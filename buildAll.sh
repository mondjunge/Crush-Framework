#!/bin/bash
#
##LOLOLOLOLOLOLOL
##
##     +
##     *+
##    *+*+
##    +*++*
##   **++*++
##   * *+**+*
## *+* ~ ~ *+*+
## **{  '  }*+
##     ---
##      V
##  >––––––––<
## WIZAR DRAZIW
## 
Write () { 
	
	DATE=`date +%Y-%m-%d_%H:%M:%S`
	echo "$DATE $1"
}

Write "creating build folder..."
rm -rf ./dist/build
rm -rf ./build
mkdir -m 0755 ./build

Write "creating cache folders..."
mkdir -m 0777 ./build/cache
mkdir -m 0777 ./build/cache/thumbs
mkdir -m 0777 ./build/cache/thumbs/src
mkdir -m 0777 ./build/cache/html
mkdir -m 0777 ./build/cache/update

Write "creating upload folders..."
mkdir -m 0777 ./build/upload
mkdir -m 0777 ./build/upload/files
mkdir -m 0777 ./build/upload/images
mkdir -m 0777 ./build/upload/logs
mkdir -m 0777 ./build/upload/messages
mkdir -m 0777 ./build/upload/users
cp upload/.htaccess build/upload/.htaccess
cp upload/logs/.htaccess build/upload/logs/.htaccess
cp upload/messages/.htaccess build/upload/messages/.htaccess

#copy core files to build
sh ./buildCore.sh

#copy application files to build, delete custo module config files
sh ./buildApplication.sh


Write "zipping full release..."
cd build
zip -9 -rq CrushFrameworkFull.zip * .htaccess .user.ini
cd ../

Write "creating distribution dir..."
rm -r ./dist
mkdir -m 0755 ./dist

Write "moving full release zip to dist..."
mv ./build/CrushFrameworkFull.zip ./dist/CrushFramework.zip

Write "creating update packages..."
#create update files
sh ./buildUpdate.sh


Write "moving update to dist..."
mv ./update ./dist

Write "removing empty build folder..."
rm -r ./build 

cp ./distIndex.php ./dist/index.php
cp ./downloadAndExtract.php ./dist/downloadAndExtract.php

DATE=`date +%Y-%m-%d_%H:%M:%S`
echo "build time: $DATE $1" > ./dist/build.log

DIR="$( cd "$( dirname "$0" )" && pwd )"
Write "Find your release files in dist: ${DIR}/dist"

Write "Build complete! Have Fun!"