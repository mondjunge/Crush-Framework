<?php

/**
 *  Copyright © tim 20.11.2018
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 *	$Id: $
 *	Files Purpose: dynamic replacements in serviceworker.js file.
 */

require_once 'core/Config.php';

$allExtArray = array('png', 'gif', 'jpg', 'jpeg', 'svg', 'ico');
$ext = '';
Config::createConfig();

function rglob($pattern, $flags = 0) {
    $files = glob($pattern, $flags);
    foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
        $files = array_merge($files, rglob($dir . '/' . basename($pattern), $flags));
    }
    return $files;
}

function getAppIcons(){
    $return = "";
    $root = __DIR__;

    $array = rglob("$root/theme/".Config::get('siteStyle')."/images/appIcons/*.svg");

    foreach ($array as $f) {
        $return .= "\t'./" . str_replace($root."/", '', $f) . "',\n";
    }
    return $return;
}

function getSvgSprite(){
    return "\t'./theme/" . Config::get('siteStyle') ."/images/icons/open-iconic/sprite/sprite.min.svg',\n";
}

function getOfflineSite(){ // for assets to precache
    return "\t'./theme/" . Config::get('siteStyle') ."/offline.html'";
}

function getOfflineUrl(){ // for offline state to load from cache
    return  Config::get('relativeUrl') ."theme/" . Config::get('siteStyle') ."/offline.html";
}

function getServiceIcon(){
    return "theme/" . Config::get('siteStyle') . "/images/serviceIcon.png";
}
function getServiceBadge(){
    return "theme/" . Config::get('siteStyle') . "/images/serviceBadge.png";
}

function getServerUrl (){
    return $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/".Config::get('relativeUrl');
}

$serviceworkerJs = file_get_contents('serviceworker.js');


$string = str_replace('{insertResourcesToPreCache}', getSvgSprite().getAppIcons().getOfflineSite(), $serviceworkerJs);
$string = str_replace('{serverUrl}', getServerUrl(), $string);

$string = str_replace('{serviceName}', Config::get('pwaName'), $string);
$string = str_replace('{serviceIcon}', getServiceIcon(), $string);
$string = str_replace('{serviceBadge}', getServiceBadge(), $string);

header("Content-type: text/javascript");
echo str_replace('{insertOfflineUrl}', getOfflineUrl(), $string);