<?php
include_once 'Updater.php';
include_once '../core/helper/Input.php';
require_once '../core/helper/Session.php';

$updater = new Updater();



$hash =  filter_input(INPUT_GET, 'sh', FILTER_UNSAFE_RAW);
if(!$updater->checkAuthorization($hash)){
    die("You are not authorized to update this fine piece of software. Doh!");
}

if(filter_has_var(INPUT_GET, 'core')){
    try{
        $updater->updateCore();
        header('Location: ../de/site/admin?manageUpdates=core&success');
        exit;
    } catch (Exception $ex) {
        header('Location: ../de/site/admin?manageUpdates=core&failure='.url_encode($ex->getMessage()));
        exit;
    }
}

$module = filter_input(INPUT_GET, 'module', FILTER_UNSAFE_RAW);
if(isset($module) && $module != ''){
    try{
        $updater->downloadAndUpdateModule($module);
        header('Location: ../de/site/admin?manageUpdates='.$module.'&success');
        exit;
    } catch (Exception $ex) {
        header('Location: ../de/site/admin?manageUpdates='.$module.'&failure='.url_encode($ex->getMessage()));
        exit;
    }
}else{
    header("HTTP/1.1 404 Not Found");
    exit;
}


