<?php

/**
 *  Copyright © tim 15.02.2018
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
require_once '../config/config.php';
require_once '../config/useConfig.php';
require_once '../core/Config.php';
require_once '../core/helper/Session.php';

class Updater {

    private $coreFilesAndDirs = array('config/', 'core/', 'extras/', 'javascript/', 'lang/', 'theme/', '.user.ini', 
        '.htaccess', 'index.php', 'robots.txt', 'serviceworker.js', 'serviceworker.php', 'manifest-json.php');

    public function checkAuthorization($hash) {
        Session::init();

        if (!Session::isValid('updateSecurityHash', false)) {
            die("No hash." . Session::get('updateSecurityHash', false));
            return false;
        }
        if (Session::get('updateSecurityHash', false) !== $hash) {
            die("invalid hash.");
            return false;
        }
        Session::del('updateSecurityHash', false);
        Session::writeClose();

        return true;
    }

    public function updateCore() {
        if ($this->backupOldCore()) {
            if ($this->copyNewCoreInPlace()) {
                if ($this->copyCustomConfigurationAndStuff()) {
                    return;
                } else {
                    throw new Exception("Could not copy custom configurations in place! Check your servers permissions.");
                }
            } else {
                throw new Exception("Could not copy new version in place! Check your servers permissions.");
            }
        } else {
            throw new Exception("Could not backup old core! Check your servers permissions.");
        }
    }

    private function copyCustomConfigurationAndStuff() {
        /**
         * - config files
         * - extra themes
         */
        $corePath = dirname(__FILE__) . "/../";
        $coreBackupPath = dirname(__FILE__) . "/../cache/updates/core_backup/";

        try {
            // copy custom configuration
            $files = array_diff(scandir($coreBackupPath . "config/"), array('.', '..', 'config.php', '.htaccess'));
            foreach ($files as $file) {
                copy($coreBackupPath . "config/" . $file, $corePath . "config/" . $file);
            }

            // copy custom themes
            /**
             * DONE: one cannot copy folders!
             */
            $files = array_diff(scandir($coreBackupPath . "theme/"), array('.', '..', 'default', 'Theme.php'));
            foreach ($files as $file) {
                if (is_dir($coreBackupPath . "theme/" . $file)) {
                    rename($coreBackupPath . "theme/" . $file, $corePath . "theme/" . $file);
                } else {
                    copy($coreBackupPath . "theme/" . $file, $corePath . "theme/" . $file);
                }
            }

            // move back piwik, if existant
            if (is_dir($coreBackupPath . "extras/piwik")) {
                rename($coreBackupPath . "extras/piwik", $corePath . "extras/piwik");
            }

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    private function copyNewCoreInPlace() {
        $corePath = dirname(__FILE__) . "/../";
        $coreUpdatePath = dirname(__FILE__) . "/../cache/updates/core/";

        $coreFiles = $this->coreFilesAndDirs; //array('config/', 'core/', 'extras/', 'javascript/', 'lang/', 'theme/', '.user.ini', '.htaccess', 'index.php', 'robots.txt');
        foreach ($coreFiles as $cf) {
            if (file_exists($coreUpdatePath . $cf)) {
                if (!rename($coreUpdatePath . $cf, $corePath . $cf)) {
                return false;
            }
            }
        }
        return true;
    }

    private function backupOldCore() {
        $corePath = dirname(__FILE__) . "/../";
        $coreBackupPath = dirname(__FILE__) . "/../cache/updates/core_backup/";
        if (is_dir($coreBackupPath)) {
            // delete old backup before creating new one
            self::delTree($coreBackupPath);
        }
        if (!is_dir($coreBackupPath)) {
            mkdir($coreBackupPath, 0777, true);
        }

        $coreFiles = $this->coreFilesAndDirs; //array('config/', 'core/', 'extras/', 'javascript/', 'lang/', 'theme/', '.user.ini', '.htaccess', 'index.php', 'robots.txt');
        foreach ($coreFiles as $cf) {
            if (file_exists($corePath . $cf)) {
                if (!rename($corePath . $cf, $coreBackupPath . $cf)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function downloadAndUpdateModule($module) {
        /**
         * DONE:
         * - download
         * - unzip
         * - copy old special module config (if any)
         * - backup old module
         * - copy/move new version in place
         * - run db-update (must be done in Site.php over Installer.php)    
         */
        if ($this->downloadModuleNewVersion($module)) {
            if ($this->unzipModuleNewVersion($module)) {
                $this->copySpecialModuleConfigOnUpdate($module);
                if ($this->backupOldModule($module)) {
                    if ($this->copyNewModuleVersionInPlace($module)) {
                        return;
                    } else {
                        throw new Exception("Could not copy new version in place! Check your servers permissions.");
                    }
                } else {
                    throw new Exception("Could not backup old module! Check your servers permissions.");
                }
            } else {
                throw new Exception("Extracting the update package failed! Check your servers permissions.");
            }
        } else {
            throw new Exception("Something went wrong with the download! Please try again later.");
        }
    }

    private function copyNewModuleVersionInPlace($module) {
        $savePath = dirname(__FILE__) . "/../cache/updates/";
        $modCachePath = $savePath . $module . "/";
        $modAppPath = dirname(__FILE__) . "/../" . $this->getConfigKey("applicationDirectory") . "/" . $module . "/";
        return rename($modCachePath, $modAppPath);
    }

    private function backupOldModule($module) {
        $modAppPath = dirname(__FILE__) . "/../" . $this->getConfigKey("applicationDirectory") . "/" . $module . "/";
        $modAppBackupPath = dirname(__FILE__) . "/../" . $this->getConfigKey("applicationDirectory") . "/" . $module . "_backup/";
        if (is_dir($modAppBackupPath)) {
            // delete old backup before creating new one
            self::delTree($modAppBackupPath);
        }
        return rename($modAppPath, $modAppBackupPath);
    }

    private static function delTree($dir) {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    private function copySpecialModuleConfigOnUpdate($module) {
        $savePath = dirname(__FILE__) . "/../cache/updates/";
        $modCacheConfigPath = $savePath . $module . "/config/";

        $modAppConfigPath = dirname(__FILE__) . "/../" . $this->getConfigKey("applicationDirectory") . "/" . $module . "/config/";
        if (is_dir($modAppConfigPath)) {
            $d = dir($modAppConfigPath);

            while (false !== ($entry = $d->read())) {
                if (basename($entry) !== "moduleConfig.php" && $entry !== "." && $entry !== "..") {
                    copy($modAppConfigPath . $entry, $modCacheConfigPath . $entry);
                }
            }
        }
    }

    private function unzipModuleNewVersion($module) {
        $savePath = dirname(__FILE__) . "/../cache/updates/";
        $fileCachePath = $savePath . $module . ".zip";
        if (is_file($fileCachePath)) {
            $zip = new ZipArchive;
            $res = $zip->open($fileCachePath);
            if ($res === TRUE) {
                $zip->extractTo($savePath);
                $zip->close();
                unlink($fileCachePath);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function downloadModuleNewVersion($module) {
        if(str_contains($module, "../")){
            exit; die;
        }
        $savePath = dirname(__FILE__) . "/../cache/updates/";
        if (!is_dir($savePath)) {
            mkdir($savePath, 0777, true);
        }
        $fileCachePath = $savePath . $module . ".zip";

        if (is_file($fileCachePath)) {
            unlink($fileCachePath);
        }

        $url = $this->getConfigKey("updateEndpoint");
        $url .= "?downloadLatestModule=$module";

        $ch = curl_init($url);
        $fp = fopen($fileCachePath, 'x');

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);

        fclose($fp);
        if (is_readable($fileCachePath)) {
            return true;
        } else {
            return false;
        }
    }

    private function getConfigKey($string) {
        global $aConfig, $useConfig;
        //echo $useConfig;
        if (isset($useConfig) && $useConfig != '' && is_readable('../config/config_' . $useConfig . '.php')) {
            $bConfig = $aConfig;
            require_once '../config/config_' . $useConfig . '.php';
            foreach ($bConfig as $key => $value) {
                $bConfig[$key] = $aConfig[$key];
            }
            // haha lol. switch again so only variables in the main config are present.
            $aConfig = $bConfig;
        }
        //print_r($aConfig);
        return $aConfig[$string];
    }
    
}
