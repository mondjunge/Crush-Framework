<!-- Matomo Image Tracker-->
<noscript><img src="https://<?php echo $_SERVER['HTTP_HOST'];?>/extras/piwik/matomo.php?idsite=1&amp;rec=1" style="border:0" alt="" /></noscript>
<!-- Matomo Script Tracker -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//<?php echo $_SERVER['HTTP_HOST'];?>/extras/piwik/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->

