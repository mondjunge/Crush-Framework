/* global modules, base */

/**
 * Handles all kinds of menus and buttons for site navigation
 */
use_package('modules.theme');
modules.theme.MenuHandler = new function () {
    base.main.ModuleRegistry.registerModule(this);

    this.init = function () {
        modules.theme.MenuHandler.handleMenuButtonClick();
        modules.theme.MenuHandler.handleAdminNav();
        modules.theme.MenuHandler.handleMenuHide();
    };

    this.handleMenuHide = function () {
        // hide slidedown menues when user clicks somewhere else in the page
        $(document).off('click');
        $(document).on("click", function (e) {
            console.log('click somewhere..' + $(e.target).attr('id'));
            if (!$(e.target).closest('#notifications').length) {
                $("#notifications").addClass('hidden');
            }

            if (!$(e.target).closest('#navLinks').length && $("#mobileMenuButton:visible").length) {
                if ($("#navigation").hasClass('navOpen')) {
                    $("#navigation").removeClass('navOpen');
                    $("#navigation").addClass('navClose');
                }
            }

            if (!$(e.target).closest('#adminBar').length) {
                if ($("#adminBar").hasClass('navOpen')) {
                    $("#adminBar").removeClass('navOpen');
                    $("#adminBar").addClass('navClose');
                }
            }

        });
    };

    this.handleMenuButtonClick = function () {
        $('#mobileMenuButton').on("click", function () {
            var top = $("#header").outerHeight();
            $("#navigation").css('top', top - 1);
            if ($("#navigation").hasClass('navOpen')) {
                $("#navigation").removeClass('navOpen');
                $("#navigation").addClass('navClose');
            } else if ($("#navigation").hasClass('navClose')) {
                $("#navigation").removeClass('navClose');
                $("#navigation").addClass('navOpen');
            } else {
                $("#navigation").addClass('navOpen');
            }

            return false;
        });
    };

    this.handleAdminNav = function () {
        $("#settingsButton").on("click", function () {
            var top = $("#header").outerHeight();
            $("#adminBar").css('top', top - 1);
            //$("#adminBar").toggle();
            if ($("#adminBar").hasClass('navOpen')) {
                $("#adminBar").removeClass('navOpen');
                $("#adminBar").addClass('navClose');
            } else if ($("#adminBar").hasClass('navClose')) {
                $("#adminBar").removeClass('navClose');
                $("#adminBar").addClass('navOpen');
            } else {
                $("#adminBar").addClass('navOpen');
            }

            return false;
        });
    };
};