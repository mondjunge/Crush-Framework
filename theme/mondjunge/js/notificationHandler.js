/* global modules, base, reLangUrl */

/**
 * Handles fade out of system messages aka success and error messages after actions
 */
use_package('modules.theme');
modules.theme.NotificationHandler = new function () {
    base.main.ModuleRegistry.registerModule(this);

    var notificationCount = 0;
    var oldSideTitle = document.title;

    this.checkForNotificationsFirstRun = true;

    this.init = function () {
        modules.theme.NotificationHandler.handleNotificationButtonClick();
        modules.theme.NotificationHandler.checkForNewNotifications();
    };

    this.playNotificationSound = function () {
        document.getElementById('notificationSound').play();
    };

    this.checkForNewNotifications = function () {
        var notificationUrl = reLangUrl + "notifications";

        $.ajax({
            type: "POST",
            url: notificationUrl,
            async: true,
            timeout: 4000,
            data: {
                mode: "getUnreadNotificationsCount"
            },
            success: function (count) {
                console.log("notification count" + count);
                if (count.trim() === "disabled") {
                    console.log("notifications are disabled");
                    return;
                }
                if (count > notificationCount) {

                    if (!modules.theme.NotificationHandler.checkForNotificationsFirstRun) {
                        modules.theme.NotificationHandler.playNotificationSound();
                    }

                    notificationCount = count.trim();

                    $("#notificationCount").html(notificationCount);
                    $(".notificationBellHighlight")[0].classList.remove('hidden');
                    $(".notificationBellNormal")[0].classList.add('hidden');
                    $('title').html("(" + notificationCount + ") " + oldSideTitle);
                    //$('title').html(document.title.prepend('('+count+')'));

                } else {

                    //                    $('title').html(document.title.replace('*',''));
                    //                    $('[href^="'+chatUrl+'"]').css('color', '');
                    //                    
                    //                    //Contact Lists css
                    //                    $('[id^="active_contact_"]').parent().css('font-weight', 'normal');
                    //                    $('[id^="active_contact_"]').hide();
                }
                modules.theme.NotificationHandler.checkForNotificationsFirstRun = false;
                console.log("started checking for new Notifications..");
                setTimeout('modules.theme.NotificationHandler.checkForNewNotifications()', 12000);

            }
        });
    };

    this.handleNotificationButtonClick = function () {

        $('#notificationIcon').on("click", function () {
            if ($("#notifications").hasClass('hidden')) {
                var notificationUrl = reLangUrl + "notifications";
                $.ajax({
                    type: "POST",
                    url: notificationUrl,
                    async: true,
                    timeout: 4000,
                    data: {
                        mode: "getNotificationsHTML"
                    },
                    success: function (html) {

                        if (html != '') {
                            //notifications
                            $("#notifications").html(html);
                            var top = $("#header").outerHeight();
                            $("#notifications").css('top', top - 1);

                            modules.theme.NotificationHandler.handleNotificationClickMarkAsRead();

                            $("#notifications").removeClass('hidden');
                            console.log("got Notification HTML..");
                        } else {
                            console.log("fail getting Notification HTML..");
                        }


                    }
                });

            } else {
                $("#notifications").addClass('hidden');
            }
            return false;
        });
    };

    this.handleNotificationClickMarkAsRead = function () {

        $('#markAllNotificationsAsRead').off('click');
        $('#markAllNotificationsAsRead').on('click', function (e) {
            e.preventDefault();
            var notificationUrl = reLangUrl + "notifications";

            $.ajax({
                type: "POST",
                url: notificationUrl,
                data: {
                    markAllAsRead: "1"
                },
                async: true,
                timeout: 4000,
                success: function (html) {
                    console.log("marked all notification as read: ");
                    //$("#notifications").html(html);
                    $('.notification').addClass('hidden');
                    $('.notificationBellHighlight').addClass('hidden');
                    $('.notificationBellNormal').removeClass('hidden');
                    $('#notificationCount').html('');
                    $('title').html(oldSideTitle);
                }
            });
            return false;
        });


        $('.notification').off('click');
        $('.notification').on("click", function () {
            if ($(this).hasClass('unread')) {
                var notificationUrl = reLangUrl + "notifications";
                var notificationId = $(this).find(".notificationLink").data('notificationId');
                console.log("clicked notification: " + notificationId);
                $.ajax({
                    type: "POST",
                    url: notificationUrl,
                    async: true,
                    timeout: 4000,
                    data: {
                        markAsRead: notificationId
                    },
                    success: function (html) {
                        console.log("marked notification as read: " + notificationId);
                        $("#notifications").addClass('hidden');

                    }
                });
            }
        });
    };
};