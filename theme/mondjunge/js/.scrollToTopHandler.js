/* global modules, base */

/**
 * Handles scrollToTop button behaviour
 * this is a button on the bottom right corner for fast scolling to top on long pages
 * 
 * is deprecated and deactivated for its negative impact on browser performance on scrolling
 */
use_package('modules.theme');
modules.theme.ScrollToTopHandler = new function () {
    base.main.ModuleRegistry.registerModule(this);

    this.init = function () {
        modules.theme.ScrollToTopHandler.handleScrollToTop();
    };
    
    this.handleScrollToTop = function(){
        (function ($) {
            $.fn.scrollToTop = function () {
                $(this).hide().removeAttr("href");
                var scrollDiv = $(this);
                var timeoutId = null;
                $(window).scroll(function () {
                    if (timeoutId) clearTimeout(timeoutId);
                    timeoutId = setTimeout(fadeIn, 200);
                });
                var fadeIn = function(){
                    if ($(window).scrollTop() < "1000" && typeof $(scrollDiv).data("fadeout") === "undefined") {
                        $(scrollDiv).fadeOut("slow");
                        $(scrollDiv).data("fadeout", "true");
                        $(scrollDiv).removeData("fadein");
                    } else if ($(window).scrollTop() > "1000" && typeof $(scrollDiv).data("fadein") === "undefined") {
                        $(scrollDiv).fadeIn("slow");
                        $(scrollDiv).data("fadein", "true");
                        $(scrollDiv).removeData("fadeout");
                    }
                };
                $(this).on("click", function () {
                    $("html, body").animate({
                        scrollTop: 0
                    }, "slow");
                });
            };

        }(jQuery));
        // Helper for long pages
        $("#toTop").scrollToTop();
    };
    
};