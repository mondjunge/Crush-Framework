/**
 * cookieWarning
 */

use_package('modules');
modules.userConsentQuery = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    this.init = function () {
        //localStorage.removeItem(siteStyle + "_cookiesAccepted");
        handleCookieAcceptButton();
    };

    var handleCookieAcceptButton = function () {
        console.log("cookieWarningButton Click!");
        var cookiesAccepted = localStorage.getItem(siteStyle + "_cookiesAccepted");
        console.log("cookiesAccepted", cookiesAccepted);
        if ( cookiesAccepted == null || cookiesAccepted == false) {
            
            $('#cookieWarningContainer').css("display", "flex");
            
            $('#cookieWarningButton').click(function () {
                console.log("cookieWarningButton Click!");
                localStorage.setItem(siteStyle + "_cookiesAccepted", "true");
                $('#cookieWarningContainer').css("display", "none");
            });
            console.log("run code cookies");
        }

    };

//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


};