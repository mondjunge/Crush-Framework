/* global modules, base, isLoggedIn, siteStyle, reLangUrl */

/**
 * Handles fade out of system messages aka success and error messages after actions
 */
use_package('modules.theme');
modules.theme.CheckUserMessageHandler = new function () {
    base.main.ModuleRegistry.registerModule(this);

    this.init = function () {
        if (isLoggedIn) {
            modules.theme.CheckUserMessageHandler.globalCheckForNewMessages();
        }
    };

    /**
     * checks for messages menü...
     */
    this.globalCheckForNewMessages = function () {
        var globalCheckOn = localStorage.getItem(siteStyle + "_globalmessagecheckon");

        if (globalCheckOn === null) {
            localStorage.setItem(siteStyle + "_globalmessagecheckon", true);
        }
        if (globalCheckOn === false) {
            return;
        }

        /**
         *exit if this is messages app to avoid conflicts with other scripts
         */
        var chatUrl = reLangUrl + "messages";
        var loginUrl = reLangUrl + "login";

        if (document.URL.substring(0, loginUrl.length) === loginUrl) {
            return;
        }
        //alert(chatUrl);
        console.log("started checking for new Messages..");
        $.ajax({
            type: "POST",
            url: chatUrl,
            async: true,
            timeout: 4000,
            data: {
                mode: "checkForNewMessages"
            },
            success: function (html) {
                html = jQuery.trim(html);
                console.log(html);
                if (html.trim() === 'disabled') {
                    console.log('checkForNewMessages is disabled..');
                    return;
                }
                if (html !== '' && html !== 'disabled') {
                    console.log("newMessage=" + html);


                    if (document.title.substring(0, 1) !== "*") {
                        modules.theme.NotificationHandler.playNotificationSound();
                    }

                    $('[href^="' + chatUrl + '"]').css('color', '#ff3333');

                    if (document.title.substring(0, 3) === "***") {
                        $('title').html("*" + document.title.substring(3, document.title.length));
                    } else {
                        $('title').html("*" + document.title);
                    }

                    var arr = html.split(",");

                    // contact Lists css..
                    for (var i = 0; i < arr.length; i++) {
                        $("#active_contact_" + arr[i]).show();
                        $("#active_contact_" + arr[i]).css('color', 'red');
                        $("#active_contact_" + arr[i]).parent().css('font-weight', 'bold');
                    }
                    //                
                } else {
                    $('title').html(document.title.replace('*', ''));
                    $('[href^="' + chatUrl + '"]').css('color', '');

                    //Contact Lists css
                    $('[id^="active_contact_"]').parent().css('font-weight', 'normal');
                    $('[id^="active_contact_"]').hide();
                }

                setTimeout('modules.theme.CheckUserMessageHandler.globalCheckForNewMessages()', 15000);

            }
        });
    };

};