/**
 * theme.js
 */

use_package('modules');
modules.theme = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);
    this.notificationPermission = false;
    var notificationCount = 0;
    var oldSideTitle = document.title;
    this.checkForNotificationsFirstRun = true;

    this.init = function () {

        (function ($) {
            $.fn.scrollToTop = function () {
                $(this).hide().removeAttr("href");
                //    if ($(window).scrollTop() > "100") {
                //        $(this).fadeIn("slow")
                //    }
                var scrollDiv = $(this);
                $(window).scroll(function () {
                    if ($(window).scrollTop() < "1000") {
                        $(scrollDiv).fadeOut("slow")
                    } else {
                        $(scrollDiv).fadeIn("slow")
                    }
                });
                $(this).on("click",function () {
                    $("html, body").animate({
                        scrollTop: 0
                    }, "slow")
                });
            };

        }(jQuery));


        //modules.theme.toggleAdminbar();
        modules.theme.handleMenuButtonClick();
        modules.theme.toggleAdminNav();
        setTimeout('modules.theme.handleSystemErrorMessageFadeOut()', 8000);
        /**
         * Only check for mew Chat mesages, when logged in...
         */
        utils.ajax.get('login?amILoggedIn', function (answer) {
            answer = $.trim(answer);
            console.log("loggedIn? : " + answer);
            if (answer == true) {
                modules.theme.globalCheckForNewMessages();
                modules.theme.showOnlineUsersHandler();
                //modules.theme.checkNotificationPermission();
                modules.theme.handleNotificationButtonClick();
                modules.theme.checkForNewNotifications();
            } 
        });

        $("#toTop").scrollToTop();

        // hide slidedown menues when user clicks somewhere else in the page
        $(document).off('click');
        $(document).on("click",function (e) {
            console.log('click somewhere..'+$(e.target).attr('id'));
            if (!$(e.target).closest('#notifications').length ){
                $("#notifications").addClass('hidden');
            }
            
            if (!$(e.target).closest('#navLinks').length && $("#mobileMenuButton:visible").length){
                $("#navLinks").hide();
            }

            if (!$(e.target).closest('#adminBar').length){
                $("#adminBar").hide();
            }
           
        });


    }

    this.handleSystemErrorMessageFadeOut = function () {

        $('.system').fadeOut('3000');
        $('.error').fadeOut('3000');
    }

    this.handleMenuButtonClick = function () {
        $('#mobileMenuButton').on("click",function () {
            var top = $("#header").outerHeight();
            $("#navigation").css('top',top-1);
            $('#navLinks').toggle();
            
            return false;
        });
    }
    this.checkNotificationPermission = function () {
        if ('Notification' in window) {
            // API supported
            if (Notification.permission == 'default') {
                Notification.requestPermission(modules.theme.permissionGranted());
            }
            if (Notification.permission == 'granted') {
                modules.theme.permissionGranted('granted');
            }
        } else {
            // API not supported
        }
    }
    this.permissionGranted = function (status) {
        if (status == 'granted') {
            modules.theme.notificationPermission = true;
            console.log('Notifications granted..');
        }
    }

    this.notifyUser = function (head, body) {
        if (Notification.permission == 'granted') {
            var notification = new Notification(head, {
                body: body
            });

            notification.onshow = function () {
                document.getElementById('notificationSound').play()
                console.log('Notification shown');
            };
        }

    }

    this.playNotificationSound = function () {
        document.getElementById('notificationSound').play();
    }

    this.toggleAdminNav = function () {
        $("#settingsButton").on("click",function () {
            var top = $("#header").outerHeight();
            $("#adminBar").css('top',top-1);
            $("#adminBar").toggle();

            return false;
        });
    }

    this.showOnlineUsersHandler = function () {

        utils.ajax.get('user?m=getOnlineNickNames&AJAX', function (answer) {
            console.log("getOnlineNickNames:" + answer);
            if (answer != '') {
                $('#onlineUsersBar').html(answer);
                //                utils.ajax.get('chat/index?m=load&ajax',function(answer){
                //                    $('#onlineUsersBar').append(answer);
                //                });

                setTimeout('modules.theme.showOnlineUsersHandler()', 15000);
                //$('#onlineUsersBar').show();
            } else {
                $('#onlineUsersBar').hide();
            }

        });

    }

    this.handleNotificationButtonClick = function () {

        $('#notificationIcon').on("click",function () {
            if ($("#notifications").hasClass('hidden')) {
                var notificationUrl = reLangUrl + "notifications";
                $.ajax({
                    type: "POST",
                    url: notificationUrl,
                    async: true,
                    timeout: 4000,
                    data: {
                        mode: "getNotificationsHTML"
                    },
                    success: function (html) {

                        if (html != '') {
                            //notifications
                            $("#notifications").html(html);
                            var top = $("#header").outerHeight();
                            $("#notifications").css('top',top-1);

                            modules.theme.handleNotificationClickMarkAsRead();

                            $("#notifications").removeClass('hidden');
                            console.log("got Notification HTML..");
                        } else {
                            console.log("fail getting Notification HTML..");
                        }


                    }
                });

            } else {
                $("#notifications").addClass('hidden');
            }
            return false;
        });
    }

    this.handleNotificationClickMarkAsRead = function () {
        
        $('#markAllNotificationsAsRead').off('click');
        $('#markAllNotificationsAsRead').on('click', function(e){
           e.preventDefault();
           var notificationUrl = reLangUrl + "notifications";
          
           $.ajax({
                type: "POST",
                url: notificationUrl,
                data: {
                    markAllAsRead: "1"
                },
                async: true,
                timeout: 4000,
                success: function (html) {
                    console.log("marked all notification as read: ");
                    //$("#notifications").html(html);
                    $('.notification').addClass('hidden');
                    $('.notificationBellHighlight').addClass('hidden');
                    $('.notificationBellNormal').removeClass('hidden');
                    $('#notificationCount').html('');
                    $('title').html( oldSideTitle);
                }
            });
            return false;
        });
        
        
        $('.notification').off('click');
        $('.notification').on("click",function () {
            if ($(this).hasClass('unread')) {
                var notificationUrl = reLangUrl + "notifications";
                var notificationId = $(this).find(".notificationLink").data('notificationId');
                console.log("clicked notification: " + notificationId);
                $.ajax({
                    type: "POST",
                    url: notificationUrl,
                    async: true,
                    timeout: 4000,
                    data: {
                        markAsRead: notificationId
                    },
                    success: function (html) {
                        console.log("marked notification as read: " + notificationId);
                        $("#notifications").addClass('hidden');
                        //                        if(html!=''){
                        //                            //notifications
                        //                            $("#notifications").html(html);
                        //                        
                        //                            $("#notifications").removeClass('hidden');
                        //                            console.log("got Notification HTML..");
                        //                        }else{
                        //                            console.log("fail getting Notification HTML..");
                        //                        }


                    }
                });
            }
        });
    }

    this.checkForNewNotifications = function () {
        /**
         * TODO: 
         */
        var notificationUrl = reLangUrl + "notifications";

        $.ajax({
            type: "POST",
            url: notificationUrl,
            async: true,
            timeout: 4000,
            data: {
                mode: "getUnreadNotificationsCount"
            },
            success: function (count) {
                console.log("notification count" + count);
                if(count===''){
                    return;
                }
                if (count > notificationCount) {

                    if (!modules.theme.checkForNotificationsFirstRun) {
                        modules.theme.playNotificationSound();
                    }

                    notificationCount = count;

                    $("#notificationCount").html(notificationCount);
                    $(".notificationBellHighlight")[0].classList.remove('hidden');
                    $(".notificationBellNormal")[0].classList.add('hidden');
                    $('title').html("(" + notificationCount + ") " + oldSideTitle);
                    //$('title').html(document.title.prepend('('+count+')'));

                } else {

                    //                    $('title').html(document.title.replace('*',''));
                    //                    $('[href^="'+chatUrl+'"]').css('color', '');
                    //                    
                    //                    //Contact Lists css
                    //                    $('[id^="active_contact_"]').parent().css('font-weight', 'normal');
                    //                    $('[id^="active_contact_"]').hide();
                }
                modules.theme.checkForNotificationsFirstRun = false;
                console.log("started checking for new Notifications..");
                setTimeout('modules.theme.checkForNewNotifications()', 12000);

            }
        });
    }

    /**
     * checks for messages menü...
     */
    this.globalCheckForNewMessages = function () {
        var globalCheckOn = localStorage.getItem(siteStyle + "_globalmessagecheckon");

        if (globalCheckOn == null) {
            localStorage.setItem(siteStyle + "_globalmessagecheckon", true);
        }
        if (globalCheckOn == false) {
            return;
        }

        /**
         *exit if this is messages app to avoid conflicts with other scripts
         */
        var chatUrl = reLangUrl + "messages";
        var loginUrl = reLangUrl + "login";

        if (document.URL.substring(0, loginUrl.length) === loginUrl) {
            return;
        }




        //alert(chatUrl);
        console.log("started checking for new Messages..");
        $.ajax({
            type: "POST",
            url: chatUrl,
            async: true,
            timeout: 4000,
            data: {
                mode: "checkForNewMessages"
            },
            success: function (html) {
                html = jQuery.trim(html);
                console.log(html);
//                if (string.prototype.trim(html) !== '') {
                if (html !== '' && html !== 'disabled') {
                    console.log("newMessage=" + html);


                    //modules.theme.notifyUser('Neue Chat Nachricht','Hier klicken!');
                    if (document.title.substring(0, 1) != "*") {
                        modules.theme.playNotificationSound();
                    }

                    $('[href^="' + chatUrl + '"]').css('color', '#ff3333');
                    //$('[href^="'+chatUrl+'"]').val('Neue Nachricht');
                    if (document.title.substring(0, 3) == "***") {
                        $('title').html("*" + document.title.substring(3, document.title.length));
                    } else {
                        $('title').html("*" + document.title);
                    }

                    var arr = html.split(",");

                    // contact Lists css..
                    for (var i = 0; i < arr.length; i++) {
                        $("#active_contact_" + arr[i]).show();
                        $("#active_contact_" + arr[i]).css('color', 'red');
                        $("#active_contact_" + arr[i]).parent().css('font-weight', 'bold');
                    }
                    //                
                } else {
                    $('title').html(document.title.replace('*', ''));
                    $('[href^="' + chatUrl + '"]').css('color', '');

                    //Contact Lists css
                    $('[id^="active_contact_"]').parent().css('font-weight', 'normal');
                    $('[id^="active_contact_"]').hide();
                }

                setTimeout('modules.theme.globalCheckForNewMessages()', 15000);

            }
        });

    }
    this.toggleAdminbar = function () {
        var adminBarOpen = localStorage.getItem(siteStyle + "_adminBarOpen");
        // Bar is closed
        console.log(adminBarOpen);
        if (adminBarOpen === 'true') {
            console.log("Admin Bar is open!");
            $("#adminBar").removeClass('hidden');
            $("#adminBar").show();

        }
        if (adminBarOpen === 'false') {
            console.log("Admin Bar is closed!");
            $("#adminBar").addClass('hidden');
        }

        // Bar is default (closed)
        if (adminBarOpen == null) {
            localStorage.setItem(siteStyle + "_adminBarOpen", "false");
            $("#adminBar").addClass('hidden');
        }


        $("#settingsButton").on("click",function () {
            if ($("#adminBar").hasClass('hidden')) {
                localStorage.setItem(siteStyle + "_adminBarOpen", "true");
                console.log("true: " + localStorage.getItem(siteStyle + "_adminBarOpen"));
                $("#adminBar").show('fast');
                $("#adminBar").removeClass('hidden');

            } else {
                localStorage.setItem(siteStyle + "_adminBarOpen", "false");
                console.log("false: " + localStorage.getItem(siteStyle + "_adminBarOpen"));
                $("#adminBar").addClass('hidden');
                $("#adminBar").hide('fast');


            }
            return false;
        });
    };
    
//    this.ajaxRefresh = function(){
//        
//    }

//    
//    this.ajaxStart = function(){
//        
//    }


}