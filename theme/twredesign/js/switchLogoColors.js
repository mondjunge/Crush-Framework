/**
 * switchLogoColors.js
 * 
 */

use_package('modules.theme');
modules.theme.SwitchLogoColors = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);
    
    var timeout;
    this.init = function () {
        
        $('#logoArea').hover(function () {
            //timeout = setTimeout(function () {
                switchColor();
            //}, '1000');
        }, function(){
            clearTimeout(timeout);
        });

    };

    var switchColor = function () {
        var redFill = $('svg#twLogo').find("#pathRed").css('fill');
        var blueFill = $('svg#twLogo').find("#pathBlue").css('fill');
        var greenFill = $('svg#twLogo').find("#pathGreen").css('fill');

        $('svg#twLogo').find("#pathRed").css('fill', blueFill);
        $('svg#twLogo').find("#pathBlue").css('fill', greenFill);
        $('svg#twLogo').find("#pathGreen").css('fill', redFill);
        timeout = setTimeout(function () {
            switchColor();
        }, '1000');
    };



//    this.ajaxRefresh = function(){
//        
//    }

//    
//    this.ajaxStart = function(){
//        
//    }


};