<!DOCTYPE html>
<html lang="<?php echo $language; ?>" xml:lang="<?php echo $language; ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo $headTag ?>
        <title><?php echo $title ?></title>
    </head>
    <body <?php echo $bodyJs ?> <?php echo $bodyBgStyle ?>>
        <a name="accessibility legend" tabindex="0" class="outOfBounds">
            <?php echo $this->ts("accesskeys: n for navigation, c for content"); ?>
        </a>
        <audio id="notificationSound">
            <source src="<?php echo $styleUrl . "sounds/gets-in-the-way.mp3" ?>">
<!--            <source src="WhiteChristmas.ogg">-->
        </audio>
        <div id="backgroundCurtain"></div>

        <div id="fullOverlay"><div class="headerOverlay" ><div id="titleOverlay"></div><a class="pure-button pure-button-error closeOverlay" href="#closeOverlay"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#x', 'svgIcon svgIcon-light overlayCloseIcon'); ?></a></div>
            <div id="fullOverlayContent"></div>
        </div>

        <div id="header">
            <div id="header_bg"></div>
            <?php echo $header ?>
            
            <a href="<?php echo $config['relativeUrl'] ?>" style="float:left;" >
                <div id="logoArea">
                    <?php echo $this->image('logo.svg', 'logo') ?>
                    <?php echo $this->image('Schrift-Logo-Vector.svg', 'logoSchrift') ?>
                </div>
            </a>
            
            <a id="mobileMenuButton" class="pure-button pure-button-nav" href="#" title="<?php $this->ts("open/close navigation") ?>">
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#menu', 'svgIcon svgIcon-light menuIcon'); ?>
<!--                    <span class="imgBtnText"><?php //echo $this->ts('Menu'); ?></span>-->
            </a>
            
            <div id="navigation" tabindex="-1" accesskey="n">

                <div id="navLinks">
                    <?php echo $navigation ?>
                </div>
            </div>
            
            <div id="login" >
                <?php echo $languageChooser; ?>
                <?php echo $search; ?>
                <?php echo $login ?>
            </div>
            
            <div style="clear:both;"></div>
        </div>
        <div id="notifications" class="hidden" ></div>

        <!--            <a id="barToggle" href="#" title="click to open/close the admin navbar!">^^</a>-->
        <?php echo $cookieWarning; ?>
        <?php if ($isLoggedIn) : ?>
            <div id="onlineUsersBar"></div>
        <?php endif; ?>
        <?php if (is_array($systemMessages) && count($systemMessages) > 0): ?>
            <div class="system message">
                <?php foreach ($systemMessages as $system): ?>
                    <?php //echo $this->icon(); ?><?php echo $system ?><br/>
                <?php endforeach; ?>

            </div>
        <?php endif; ?>

        <?php if (is_array($errorMessages) && count($errorMessages) > 0): ?>
            <div class="error message">

                <?php foreach ($errorMessages as $error): ?>
                    <?php echo $error ?><br/>
                <?php endforeach; ?>

            </div>
        <?php endif; ?>    
        <div id="content" tabindex="-1" accesskey="c">
            <?php echo $maintainanceNotice; ?>
            <!--** EXPERIMENTAL
       
            -->


            <!--** END EXPERIMENTAL
                   
            -->
            <a href='#' id='toTop' class="pure-button " style='display:none;'><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#chevron-top', 'svgIcon svgIcon-dark toTopIcon'); ?></a>


            <div id="content_bg"></div>
            <span style="font-weight:bold;color:red;"><?php echo $iewarning ?></span>

            

            <?php echo $content ?>

        </div>
        <div id="adminBar">
            <div id="adminNav" class="adminNav">
                <?php echo $adminNavi ?>
                <a class="pure-button pure-button-error admin_element" href="<?php echo $this->action('login?logout') ?>" title="logout">
                    <span class="appIcon logoutIcon">
                        <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#power-standby', 'svgIcon svgIcon-light'); ?>
                    </span>
                    <?php echo $this->ts("logout") ?>
                </a>
            </div>
        </div>
        <?php if (is_array($debugMessages) && count($debugMessages) > 0): ?>
            <br/>
            <div class="debug">
                <?php echo "<p><a href='http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "?XDEBUG_PROFILE'>XDEBUG_PROFILE</a></p>"; ?>

                <b><?php echo $this->ts('Debug Information:'); ?></b>
                <br/>
                <pre>
                    <?php foreach ($debugMessages as $debug): ?>
                        <?php echo $debug ?><br/>
                    <?php endforeach; ?>
                </pre>
            </div>
        <?php endif; ?>

        <div id="footer"><?php echo $footer ?></div>

        <?php echo $piwikTag ?>
        <?php echo $javaScriptIncludes ?>
        <?php echo $javaScriptSingleIncludes ?>


        <!--** EXPERIMENTAL
        
        -->


        <!--** END EXPERIMENTAL
               
        -->

    </body>
</html>
