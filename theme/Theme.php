<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Theme extends Output {

    public function render($data, $contentOnly = false) {
        $aConfig = Config::getConfig();
        $data['config'] = $aConfig;

        if ($this->getController()->isModuleActive('sidebar')) {
            $rs = $this->getController()->select('sidebar');
            foreach ($rs as $r) {
                if ($r['name'] == 'leftSidebar') {
                    $data['sidebarLeft'] = $r['content'];
                }
                if ($r['name'] == 'rightSidebar') {
                    $data['sidebarRight'] = $r['content'];
                }
                if ($r['name'] == 'footer') {
                    $data['footer'] = $r['content'];
                }
            }
        } else {
            $data['sidebarLeft'] = '';
            $data['sidebarRight'] = '';
            $data['footer'] = '';
        }

        /**
         * get Menu
         */
        $data['navigation'] = $this->renderNavigation();
        $data['languageChooser'] = $this->renderLanguageSelection();

        /**
         * add additional infos to debugmessages
         */
        $data['debugMessages'] = array_merge($data['debugMessages'], $this->getAdditionalDebugInfo());

        /**
         * set template path
         */
        $this->setTemplatePath('theme/' . $aConfig['siteStyle'] . '/view/');

        /**
         * fetch messages
         * already done in index.php
         */
        $data['messages'] = $this->fetch('messages', $data);

        $data['cookieWarning'] = $this->fetch('cookieWarning', $data);

        /**
         * include matomo tracking code
         */
        if (is_file('extras/piwik/index.php') && $this->getController()->isModuleActive("piwik") && !$this->getController()->checkUserRights('1')) {
            //$this->getController()->includeJS("javascript/common/matomoTrackingCode.js", "head");
            $data['piwikTag'] = $this->fetch('matomoTag');
        } else {
            $data['piwikTag'] = '';
        }
        
        $data['isRssActive'] = $this->getController()->isModuleActive("rss");

        /**
         * head Includs
         */
        $data['javaScriptIncludes'] = $this->getJavascriptIncludes(); // IS NOW IN MAIN TEMPLATE ON BODY END FOR BETTER PERFORMANCE
        $data['javaScriptHeadIncludes'] = $this->getJavascriptIncludes("head"); // includes Javascript in <head>
        $data['javaScriptSingleIncludes'] = $this->getJavascriptIncludes("single"); // will be included on body, as a single file

        $data['cssIncludes'] = "<link rel='stylesheet' title='normal' type='text/css' href='" . $this->getCssMinLink() . "' />"; //$this->getCssIncludes();
        $data['headIncludes'] = $this->getAdditionalHeadIncludes();
        
        /**
         * body Tag include
         */
        $data['bodyJs'] = $this->getRegVar('bodyJs');

        /**
         * process outer template content
         */
        if ($this->getController()->isRegValid('site_title')) {
            $data['title'] = $this->getTitle();
        } else {
            $rs2 = false;
            $rs = $this->getController()->select('site', 'id', "module='" . $this->getController()->getActiveModule() . "/" . $this->getController()->getActiveFunction() . "'");
            if ($rs == false) {
                $rs = $this->getController()->select('site', 'id', "module='" . $this->getController()->getActiveModule() . "'");
            }
            if ($rs != false) {
                $rs2 = $this->getController()->select('site_data', 'title', "site_id='{$rs[0]['id']}' AND language='" . $this->getLanguage() . "'");
            }

            if ($rs2 != false) {
                $data['title'] = $rs2[0]['title'];
            } else {
                $data['title'] = $this->getController()->ts("(no title)");
                $this->getController()->setErrorMessage($this->getController()->ts("Module not in DB! Everybody can use it!"));
            }
        }
        $data['isLoggedIn'] = $this->getController()->isLoggedIn();
        $data['styleUrl'] = $aConfig['relativeUrl'] . 'theme/' . $aConfig['siteStyle'] . '/';
        $data['language'] = ($this->getLanguage() == 'gb') ? "en" : $this->getLanguage();
        
        $data['activeModule'] = $this->getController()->getActiveModule() . "/" . $this->getController()->getActiveFunction();
        $data['headTag'] = $this->fetch('head', $data);
        $data['header'] = $this->fetch('header');

        $data['iewarning'] = $this->fetch('iewarning');

        
        $data['searchActive'] = ($this->getController()->isModuleActive('search') && $this->getController()->checkSiteRights('search'));
        $data['notificationsActive'] = ($this->getController()->isModuleActive('notifications') && $this->getController()->checkSiteRights('notifications'));
        $data['maintainanceNotice'] = ($aConfig['maintainanceMode']) ? $this->fetch('maintainance') : "";

        if ($this->getController()->isLoggedIn()) {

            /**
             * Background Image hack 
             */
            if (Session::isValid('uwallpaper') && Session::get('uwallpaper') != '') {
                if (Session::get('urepeat_wallpaper') == '0') {
                    $repeatStyle = "background-position: center center; background-repeat: no-repeat; background-attachment:fixed;";
                } else {
                    $repeatStyle = "background-position: center center; background-attachment:fixed;";
                }

                $data['bodyBgStyle'] = " style='background-image: url(\"" . Config::get('relativeUrl') . Session::get('uwallpaper') . "\");$repeatStyle' ";
            } else {
                $data['bodyBgStyle'] = "";
            }
            /**
             * END Background Image hack END
             */
            //
            $data['nick'] = Session::get('unick');
            $data['user_image'] = Config::get('relativeUrl') . Session::get('uimage');
            $data['adminNavi'] = $this->renderAdminNavigation();
            $data['login'] = $this->fetch('logininfo', $data);
            $data['search'] = $this->fetch('search', $data);
        } else {
            $data['bodyBgStyle'] = "";

            $data['login'] = $this->fetch('login', $data);
            $data['search'] = $this->fetch('search', $data);
        }
        if ($contentOnly) {
            echo $this->fetch('contentOnly', $data);
        } else {
            echo $this->fetch('main', $data);
        }
    }

    /**
     * serve templates from default theme
     * @param type $site
     * @param type $data
     * @return string
     */
    private function fetch($site, $data = array()): string {
        $returnContent = "";
        if (!is_file('theme/' . Config::get('siteStyle') . '/view/' . $site . '.tpl')) {
            $this->setTemplatePath('theme/default/view/');
            $returnContent = $this->getController()->fetch($site, $data);
            $this->setTemplatePath('theme/' . Config::get('siteStyle') . '/view/');
        } else {
            $returnContent = $this->getController()->fetch($site, $data);
        }
        return $returnContent;
    }

}
