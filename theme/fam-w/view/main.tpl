<!DOCTYPE html>
<html lang="<?php echo $language; ?>" xml:lang="<?php echo $language; ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo $headTag ?>
        <title><?php echo $title ?></title>
    </head>
    <body <?php echo $bodyJs ?> <?php echo $bodyBgStyle ?> >
        <a name="accessibility legend" tabindex="0" class="outOfBounds">
            <?php echo $this->ts("accesskeys: n for navigation, c for content"); ?>
        </a>
        <audio id="notificationSound">
            <source src="<?php echo $styleUrl . "sounds/gets-in-the-way.mp3" ?>">
<!--            <source src="WhiteChristmas.ogg">-->
        </audio>
        <div id="backgroundCurtain" class="closeOverlay"></div>

        <div id="fullOverlay"><div class="headerOverlay" ><h2 id="titleOverlay"></h2><a class="pure-button pure-button-error closeOverlay closeOverlayStaticButton" title="<?php echo $this->ts("Close overlay"); ?>" href="#closeOverlay"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#x', 'svgIcon svgIcon-light overlayCloseIcon'); ?></a></div>
            <div id="fullOverlayContent"></div>
        </div>

        <header id="header">
            <?php echo $header ?>
            <div id="logoArea">
                <a id="logoLink" href="<?php echo $config['relativeUrl'] ?>">
                    <?php echo $this->image('logo.png', 'logo') ?>
                    <?php //echo $this->image('Schrift-Logo-Vector.svg', 'logoBig') ?>
                    fam-wahrendorff.de
                </a>
            </div>



            <?php echo $search; ?>
            <div id="login" >
                <?php echo $languageChooser; ?>

                <?php echo $login ?>
            </div>

            <div style="clear:both;"></div>
            
        </header>
        <nav id="navigation" >
            <div id="navLinks">
                <?php echo $navigation ?>
            </div>
        </nav>
        <?php echo $cookieWarning; ?>
        <?php if ($isLoggedIn && false) : ?>
            <div id="onlineUsersBar"></div>
        <?php endif; ?>
        <div id="msgsContainer">
            <?php if ($messages != ""): ?>
                <?php echo $messages; ?>
            <?php endif; ?>    
        </div>
        <?php if ($sidebarLeft != ""): ?>
            <div id="sidebarLeft"><?php echo $sidebarLeft ?></div>
        <?php endif; ?>

        <main id="content" tabindex="-1" accesskey="c">
            <?php echo $maintainanceNotice; ?>
            <!--** EXPERIMENTAL
       
            -->


            <!--** END EXPERIMENTAL
                   
            -->

            <div id="content_bg"></div>
            <?php echo $content ?>

        </main>
        <div id="adminBar">
            <div id="adminNav" class="adminNav">
                <?php echo $adminNavi ?>
                <a class="pure-button pure-button-error admin_element" href="<?php echo $this->action('login?logout') ?>" title="logout">
                    <span class="appIcon logoutIcon">
                        <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#power-standby', 'svgIcon svgIcon-light'); ?>
                    </span>
                    <?php echo $this->ts("logout") ?>
                </a>
            </div>
        </div>
        <?php if ($sidebarRight != ""): ?>
            <div id="sidebarRight"><?php echo $sidebarRight ?></div>
        <?php endif; ?>

        <a href='#' id='toTop' class="pure-button " style='display:none;'><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#chevron-top', 'svgIcon svgIcon-dark toTopIcon'); ?></a>

        <?php if (is_array($debugMessages) && count($debugMessages) > 0): ?>
            <br/>
            <div class="debug">
                <?php echo "<p><a href='http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "?XDEBUG_PROFILE'>XDEBUG_PROFILE</a></p>"; ?>

                <b><?php echo $this->ts('Debug Information:'); ?></b>
                <br/>
                <pre>
                    <?php foreach ($debugMessages as $debug): ?>
                        <?php echo $debug ?><br/>
                    <?php endforeach; ?>
                </pre>
            </div>
        <?php endif; ?>

        <?php if ($footer != ""): ?>
            <div id="footer"><?php echo $footer ?></div>
        <?php endif; ?>


        <?php echo $piwikTag ?>
        <?php echo $javaScriptIncludes ?>
        <?php echo $javaScriptSingleIncludes ?>


        <!--** EXPERIMENTAL
        
        -->


        <!--** END EXPERIMENTAL
               
        -->

    </body>
</html>