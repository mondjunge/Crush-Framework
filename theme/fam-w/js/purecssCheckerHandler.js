/**
 * makes space and enter work on custom checkboxes for accessibility
 * @type {type}
 */

use_package('modules.theme');
modules.theme.PurecssCheckerHandler = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);

    this.init = function () {
        if ($('label.pure-checker').length > 0) {
            $('label.pure-checker').each(function () {
                var input = $(this).find('input[type="checkbox"]');
                var checker = input.parent('.checker');
                if(input.prop('checked')){
                    // set style class on checked checkers
                    checker.addClass('checker-checked');
                }
                $(this).on('keydown', function (e) {
                    if (e.keyCode === 32 || e.keyCode === 13) {
                        // toggle checkbox on space and enter
                        input.click();
                    }
                });
                input.change(function(){
                    // toggle style class on checkers
                    checker.toggleClass('checker-checked');
                });
            });
        }
    };

};




