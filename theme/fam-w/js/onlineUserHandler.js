/* global modules, base, utils, isLoggedIn */

/**
 * Handles fade out of system messages aka success and error messages after actions
 */
use_package('modules.theme');
modules.theme.OnlineUserHandler = new function () {
    base.main.ModuleRegistry.registerModule(this);

    this.init = function () {
        if (isLoggedIn && $('#onlineUsersBar').length > 0) {
            modules.theme.OnlineUserHandler.showOnlineUsersHandler();
        }
    };

    this.showOnlineUsersHandler = function () {

        utils.ajax.get('user?m=getOnlineNickNames&AJAX', function (answer) {
            console.log("getOnlineNickNames:" + answer);
            if (answer !== '') {
                $('#onlineUsersBar').html(answer);

                setTimeout('modules.theme.OnlineUserHandler.showOnlineUsersHandler()', 15000);
            } else {
                $('#onlineUsersBar').hide();
            }

        });

    };

};