<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo $headTag ?>
        <title><?php echo $title ?></title>
    </head>
    <body <?php echo $bodyJs ?> <?php echo $bodyBgStyle ?>>

        <div id="content">

            <a href='#' id='toTop' class="pure-button " style='display:none;'><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#chevron-top', 'svgIcon svgIcon-dark toTopIcon'); ?></a>


            <div id="content_bg"></div>
            <span style="font-weight:bold;color:red;"><?php echo $iewarning ?></span>

                <?php if (is_array($systemMessages) && count($systemMessages) > 0): ?>
                <div class="system">
                    <?php foreach ($systemMessages as $system): ?>
                        <?php //echo $this->icon(); ?><?php echo $system ?><br/>
    <?php endforeach; ?>

                </div>
            <?php endif; ?>

<?php if (is_array($errorMessages) && count($errorMessages) > 0): ?>
                <div class="error">

                    <?php foreach ($errorMessages as $error): ?>
                        <?php echo $error ?><br/>
    <?php endforeach; ?>

                </div>
            <?php endif; ?>

<?php echo $content ?>

        </div>
        <div id="footer"><?php echo $footer ?></div>

        <?php echo $piwikTag ?>
        <?php echo $javaScriptIncludes ?>
<?php echo $javaScriptSingleIncludes ?>

    </body>
</html>
