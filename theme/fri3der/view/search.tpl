<div class="searchButton">
    <div class="search_button" id="show_search_button" style="display: visible;">

        <a href="#" title="<?php echo $this->ts('Search')?>"><?php echo $this->image('search.png', '', '32', '24') ?></a>

    </div>
    <div class="search_button" id="hide_search_button" style="display: none;">

        <a href="#" title="<?php echo $this->ts('Search')?>"><?php echo $this->image('search.png', '', '32', '24') ?></a>
    </div>
</div>
<div id="searchForm" class="searchForm">
    <div class="innersearchborder">
        <form action="<?php echo $this->action('search') ?>" method="post">
            <div class="form">
                <h2><?php echo $this->ts("Search") ?></h2>
                <p>
                    <label for="searchString" ><?php echo $this->ts("Search string") ?></label>
                    <input id="searchString" name="searchString" type="text" value=""/>
                </p>
                <p>
                    <button type="submit" name="sendSearchRequest" ><?php echo $this->ts("search") ?></button> 
                </p>
            </div>
        </form>
    </div>
</div>