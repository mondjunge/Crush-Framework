<!--[if IE 6]>
<?php echo $this->ts('OMG!!! Your Internet Explorer is so old, it smells rotten in here! <a href="http://www.microsoft.com/ie/">Update now</a> or ask your Administrator to do so, ASAP.') ?>
<script type="text/javascript"> 
        /*Load jQuery if not already loaded*/ if(typeof jQuery == 'undefined'){ document.write("<script type=\"text/javascript\"   src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\"></"+"script>"); var __noconflict = true; } 
        var IE6UPDATE_OPTIONS = {
                icons_path: "<?php echo$config['relativeUrl'];?>extras/ie6update/images/"
        }
</script>
<script type="text/javascript" src="<?php echo$config['relativeUrl'];?>extras/ie6update/ie6update.js"></script>
<![endif]-->
<!--[if IE 7]>
<?php echo $this->ts('Your Internet Explorer is to old to render this site properly. I recommend <a href="http://www.microsoft.com/ie/">an update</a> urgently.') ?>
<script type="text/javascript"> 
        /*Load jQuery if not already loaded*/ if(typeof jQuery == 'undefined'){ document.write("<script type=\"text/javascript\"   src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\"></"+"script>"); var __noconflict = true; } 
        var IE6UPDATE_OPTIONS = {
                icons_path: "<?php echo$config['relativeUrl'];?>extras/ie6update/images/"
        }
</script>
<script type="text/javascript" src="<?php echo$config['relativeUrl'];?>extras/ie6update/ie6update.js"></script>
<![endif]-->
<!--[if IE 8]>
<?php echo $this->ts('There is a newer version of Internet Explorer. <a href="http://www.microsoft.com/ie/">An update</a> would not hurt.') ?>
<script type="text/javascript"> 
        /*Load jQuery if not already loaded*/ if(typeof jQuery == 'undefined'){ document.write("<script type=\"text/javascript\"   src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js\"></"+"script>"); var __noconflict = true; } 
        var IE6UPDATE_OPTIONS = {
                icons_path: "<?php echo$config['relativeUrl'];?>extras/ie6update/images/"
        }
</script>
<script type="text/javascript" src="<?php echo$config['relativeUrl'];?>extras/ie6update/ie6update.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<br/>
<?php echo $this->ts('Alternatively you can install <a href="http://www.mozilla-europe.org/">Firefox</a> or <a href="http://www.google.com/chrome/">Chrome</a>. Both browsers update automatically and even run on older machines!') ?>
<![endif]-->
<!--[if IE]>
    <style type="text/css">
#content a:hover , #content a:focus{
    color: #333;
}
    </style>
<![endif]-->