<div id="cookieWarningContainer">
    <div id="cookieWarning">
        <span class="cookieWarningImg"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#warning', 'svgIcon messageIcon errorMessageIcon'); ?></span><span class="cookieWarningText"><?php echo $this->ts('cookieWarningText'); ?></span>
        <button id="cookieWarningButton" class="pure-button"><?php echo $this->ts('cookieWarningButtonText'); ?></button>
    </div>
</div>
