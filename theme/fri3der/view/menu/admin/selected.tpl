<a class="pure-button pure-button-dark pure-button-small admin_selected" href="<?php echo $this->action($item->getLink())?>"
   title="<?php echo $item->getTitle(); ?>">
    <?php if($icon !== "") : ?>
        <?php echo $icon; ?>
    <?php endif ;?>
    <?php echo $item->getName()?>
</a>