<div id="language_chooser">
    <?php foreach ($langData as $item): ?>
        <span class="lang_element">
            <a class="pure-button pure-button-nav pure-button-language" style="" title="<?php echo $this->ts("language_".$item['lang']); ?>" href="<?php echo $item['link'] ?>"><?php echo $this->image('flags/' . $item['lang'] . '.png') ?></a>
        </span>
    <?php endforeach; ?>
</div>