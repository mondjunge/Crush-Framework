<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo $headTag ?>
        <title><?php echo $title ?></title>
    </head>
    <body <?php echo $bodyJs ?> <?php echo $bodyBgStyle ?>>
        <audio id="notificationSound">
            <source src="<?php echo $styleUrl . "sounds/gets-in-the-way.mp3" ?>">
<!--            <source src="WhiteChristmas.ogg">-->
        </audio>
        <div id="backgroundCurtain"></div>

        <div id="fullOverlay"><div class="headerOverlay" ><div id="titleOverlay"></div><a class="pure-button pure-button-error closeOverlay" href="#">X</a></div>
            <div id="fullOverlayContent"></div>
        </div>

        <div id="header">
            <div id="header_bg"></div>
            <?php echo $header ?>

            <a href="<?php echo $config['relativeUrl']; ?>" ><?php echo $this->image('logo90x60.png', 'logoMini'); ?></a>
<!--            <a href="<?php echo $config['relativeUrl']; ?>" ><?php echo $this->image('logoBig.png', 'logo'); ?></a>-->

            <a id="mobileMenuButton" class="pure-button pure-button-small pure-button-green">
                    <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#menu', 'svgIcon svgIcon-light menuIcon'); ?>
            </a>
            <div id="navigation">
                
                <div id="navLinks">
                    <?php echo $navigation ?>
                </div>
            </div>

            <div id="login" >
                <?php echo $login ?>
            </div>



            <!--                            <div style="clear:both;"></div>-->
            <div style="clear:both;"></div>
        </div>
        <?php //echo $cookieWarning; ?>

        <!--            <a id="barToggle" href="#" title="click to open/close the admin navbar!">^^</a>-->


        <div id="onlineUsersBar"></div>
        <?php if ($messages != ""): ?>
            <?php echo $messages; ?>
        <?php endif; ?>
        <div id="content">
            <?php echo $maintainanceNotice; ?>
            <!--** EXPERIMENTAL
       
            -->

            <a href='#' id='toTop' class="pure-button " style='display:none;'><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#chevron-top', 'svgIcon svgIcon-dark toTopIcon'); ?></a>

            <!--** END EXPERIMENTAL
                   
            -->

            <div id="content_bg"></div>
            <span style="font-weight:bold;color:red;"><?php echo $iewarning ?></span>


            <?php echo $content ?>

        </div>
        <?php if (is_array($debugMessages) && count($debugMessages) > 0): ?>
            <br/>
            <div class="debug">
                <?php echo "<p><a href='http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "?XDEBUG_PROFILE'>XDEBUG_PROFILE</a></p>"; ?>

                <b><?php echo $this->ts('Debug Information:'); ?></b>
                <br/>
                <pre>
                    <?php foreach ($debugMessages as $debug): ?>
                        <?php echo $debug ?><br/>
                    <?php endforeach; ?>
                </pre>
            </div>
        <?php endif; ?>

        <div id="footer"><?php echo $footer ?></div>

        <?php echo $piwikTag ?>
        <?php echo $javaScriptIncludes ?>
        <?php echo $javaScriptSingleIncludes ?>


        <!--** EXPERIMENTAL
        
        -->


        <!--** END EXPERIMENTAL
               
        -->

    </body>
</html>
