<?php if (is_array($systemMessages) && count($systemMessages) > 0): ?>
    <div class="message system">
        <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#check', 'svgIcon svgIcon-light messageIcon systemMessageIcon'); ?>
        <p>
            <?php foreach ($systemMessages as $system): ?>
                <?php echo $system ?><br/>
            <?php endforeach; ?>
        </p>
    </div>
<?php endif; ?>
<?php if (is_array($errorMessages) && count($errorMessages) > 0): ?>
    <div class="message error">
        <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#warning', 'svgIcon svgIcon-light messageIcon errorMessageIcon'); ?>
        <p>
            <?php foreach ($errorMessages as $error): ?>
                <?php echo $error ?><br/>
            <?php endforeach; ?>
        </p>
    </div>
<?php endif; ?>