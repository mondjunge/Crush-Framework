<a id="notificationIcon" class="pure-button pure-button-small pure-button-green">
    <span id="notificationCount"></span>
    <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#bell', 'svgIcon svgIcon-light notificationBell notificationBellHighlight hidden'); ?>
    <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#bell', 'svgIcon svgIcon-dark notificationBell notificationBellNormal'); ?>
</a>
<div id="notifications" class="hidden" >  
    
</div>
<div id="settingsButton">

<?php echo $this->ts("logged in as:") ?>&nbsp;<a  class="" href="#" onclick="return false;" ><?php echo $this->image($user_image, '', '32', '16'); ?> <span class="settingsNick"><?php echo $nick ?></span></a>
</div>
<div id="adminBar">
   
    <div id="adminNav" class="adminNav">
        <?php echo $adminNavi ?>
        <a class="pure-button pure-button-small pure-button-error admin_element" href="<?php echo $this->action('login?logout') ?>"><?php echo $this->ts("logout") ?></a>
       
    </div>
</div>