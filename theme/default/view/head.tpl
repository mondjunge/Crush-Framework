<?php echo $cssIncludes ?>
<!--<meta name="apple-mobile-web-app-capable" content="yes" />-->
<meta name="mobile-web-app-capable" content="yes" />
<link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo $styleUrl ?>favicon.ico">
<link rel='shortcut icon' href='<?php echo $styleUrl ?>images/touch-icons/128.png' type='image/png' />
<link rel='apple-touch-icon' href='<?php echo $styleUrl ?>images/touch-icons/128.png'/>
<?php if($isRssActive): ?>
    <link rel='alternate' type='application/rss+xml' title='RSS-Feed' href='<?php echo Config::get('reLangUrl'); ?>rss' />
<?php endif; ?>
<link rel="manifest" type="application/manifest+json" href="<?php echo Config::get('relativeUrl'); ?>manifest-json.php?module=<?php echo $activeModule; ?>&title=<?php echo $title; ?>" />
<script>
<!--
     const relativeUrl = '<?php echo $config['relativeUrl']; ?>';
    const applicationDir = '<?php echo $config['applicationDirectory']; ?>';
    const siteStyle = '<?php echo $config['siteStyle']; ?>';
    const configPrefix = '<?php echo Config::get('dbPrefix'); ?>';
    var reLangUrl = '<?php echo $config['reLangUrl']; ?>';
    var language = '<?php echo $language ?>';
    var isLoggedIn = '<?php echo $isLoggedIn ?>';
    var DEBUG = <?php echo ($config['debugMode']) ? 'true' : 'false'; ?>;
    <?php if (Session::isValid('uid')): echo "var userId = '" . Session::get('uid') . "';"; endif; ?>
//-->
</script>
<?php echo $headIncludes ?>
<?php echo $javaScriptHeadIncludes; ?>
