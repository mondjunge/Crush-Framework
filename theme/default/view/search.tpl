<?php if ($searchActive) : ?>
    <div id="search">
        <form class="pure-form searchField" action="<?php echo $this->action('search') ?>" method="get">
            <input id="s" autocomplete="off" name="s" type="text"  value="" placeholder="<?php echo $this->ts('search site'); ?>"/>
            <button type="submit" class="pure-button pure-button-primary searchButton" >
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#magnifying-glass', 'svgIcon svgIcon-light searchButtonImg'); ?>
            </button> 
        </form>
    </div>
<?php endif; ?>