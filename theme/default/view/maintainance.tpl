<div id="maintainanceNotice">
    <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#warning', 'svgIcon svgIcon-light warnIcon'); ?>
     <p>
         <?php echo $this->ts("Maintainance Mode. Unless you are an Superadmin, you can not use any functions until maintainance ends. <br/>Please come back later."); ?>  
    </p>
</div>
