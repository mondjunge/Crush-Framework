<!DOCTYPE html>
<html lang="<?php echo $language; ?>" xml:lang="<?php echo $language; ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo $headTag ?>
        <title><?php echo $title ?></title>
    </head>
    <body <?php echo $bodyJs ?> <?php echo $bodyBgStyle ?>>
        <audio id="notificationSound">
            <source src="<?php echo $styleUrl . "sounds/gets-in-the-way.mp3" ?>">
<!--            <source src="WhiteChristmas.ogg">-->
        </audio>
        <div id="backgroundCurtain"></div>

        <div id="fullOverlay"><div class="headerOverlay" ><div id="titleOverlay"></div><a class="pure-button pure-button-error closeOverlay" href="#closeOverlay"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#x', 'svgIcon svgIcon-light overlayCloseIcon'); ?></a></div>
            <div id="fullOverlayContent"></div>
        </div>

        <?php if (is_array($systemMessages) && count($systemMessages) > 0): ?>
            <div class="system">
                <?php foreach ($systemMessages as $system): ?>
                    <?php echo $system ?><br/>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <?php if (is_array($errorMessages) && count($errorMessages) > 0): ?>
            <div class="error">

                <?php foreach ($errorMessages as $error): ?>
                    <?php echo $error ?><br/>
                <?php endforeach; ?>

            </div>
        <?php endif; ?>    
        <div id="content">
            <?php echo $maintainanceNotice; ?>

            <div id="content_bg"></div>
            <span style="font-weight:bold;color:red;"><?php echo $iewarning ?></span>

            <?php echo $content ?>
        </div>

        <?php if (is_array($debugMessages) && count($debugMessages) > 0): ?>
            <br/>
            <div class="debug">
                <?php echo "<p><a href='http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "?XDEBUG_PROFILE'>XDEBUG_PROFILE</a></p>"; ?>

                <b><?php echo $this->ts('Debug Information:'); ?></b>
                <br/>
                <pre>
                    <?php foreach ($debugMessages as $debug): ?>
                        <?php echo $debug ?><br/>
                    <?php endforeach; ?>
                </pre>
            </div>
        <?php endif; ?>

        <div id="footer"><?php echo $footer ?></div>

        <?php echo $piwikTag ?>
        <?php echo $javaScriptIncludes ?>
        <?php echo $javaScriptSingleIncludes ?>

    </body>
</html>
