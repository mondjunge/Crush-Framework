<?php if($notificationsActive) : ?>
<a id="notificationIcon" class="pure-button pure-button-nav" href="#" title="<?php echo $this->ts("notifications") ?>">
    <span id="notificationCount"></span>
    <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#bell', 'svgIcon svgIcon-light notificationBell notificationBellHighlight hidden'); ?>
    <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#bell', 'svgIcon svgIcon-dark notificationBell notificationBellNormal'); ?>
</a>

<?php endif ?>
<a id="settingsButton" class="pure-button pure-button-nav" href="#" title="<?php echo $this->ts("open/close user-navigation") ?>"><?php echo $this->image($user_image, '', '32', '15') ?> <span class="settingsNick"><?php echo $nick ?></span></a>
