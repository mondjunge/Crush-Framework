/* global modules, base */

/**
 * Handles fade out of system messages aka success and error messages after actions
 * 
 * remade as css3 animation, so the init function is deactivted
 */
use_package('modules.theme');
modules.theme.SystemMessageHandler = new function () {
    base.main.ModuleRegistry.registerModule(this);
    base.main.ModuleRegistry.registerAjaxRefreshModule(this);

    this.init = function () {
        // is now a css3 animation
        //this.handleSystemErrorMessageFadeOut();
    };

    this.ajaxRefresh = function () {

        /**
         * finds system / error Messages in content and places them in body before fading out.
         * resolves conflicts with ajax actions returning errors or system messages.
         */
        if($('#content > .system').length > 0){
            $('#content > .system').prependTo('body');
            $('body > .system').removeClass('hidden');
            //setTimeout('modules.theme.SystemMessageHandler.handleSystemMessageFadeOut()', 10000);
        }
        if($('#content > .error').length > 0){
            $('#content > .error').prependTo('body');
            $('body > .error').removeClass('hidden');
            //setTimeout('modules.theme.SystemMessageHandler.handleErrorMessageFadeOut()', 10000);
        }
        
    };

    this.handleSystemErrorMessageFadeOut = function () {
        if ($('.system').length > 0) {
            setTimeout('modules.theme.SystemMessageHandler.handleSystemMessageFadeOut()', 10000);
        }

        if ($('.error').length > 0) {
            setTimeout('modules.theme.SystemMessageHandler.handleErrorMessageFadeOut()', 10000);
        }
    };
    this.handleSystemMessageFadeOut = function () {
        console.log("fading out .system..");
        $('.system').fadeOut('3000');
    };
    this.handleErrorMessageFadeOut = function () {
        console.log("fading out .error..");
        $('.error').fadeOut('3000');
        
    };

};