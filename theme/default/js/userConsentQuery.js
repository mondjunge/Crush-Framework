/**
 * cki
 */

use_package('modules');
modules.userConsentQuery = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    this.init = function () {
        var cookiesAccepted = getCookie("cookiesAccepted");
        console.log("cookiesAccepted:",cookiesAccepted);
        if ( cookiesAccepted == null || cookiesAccepted == false) {
            $('#cookieWarningContainer').css("display", "flex");
            $('#cookieWarningButton').click(function () {
                console.log("cookieWarningButton Click!");
                setCookie("cookiesAccepted", "true");
                $('#cookieWarningContainer').css("display", "none");
            });
        }
    };
    
    var setCookie = function(name, value){
        var cookie = configPrefix + name + '=' + value + '; path=' + relativeUrl + '; SameSite=Lax; Secure';  
    	// Der Path soll immer nur den Servernamen enthalten.
        document.cookie = cookie;
    };
    
    var getCookie = function(name) {
        var nameEQ = configPrefix + name + '=',
            ca = document.cookie.split(';'),
            i = 0;
        for (; i < ca.length; i++) {
            var c = ca[i];
            while (c && c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) == 0) {
                return c.substring(nameEQ.length, c.length);
            }
        }
        return null;
    };

//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


}