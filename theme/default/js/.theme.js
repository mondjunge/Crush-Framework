/* global modules, base, isLoggedIn, utils, reLangUrl, siteStyle, Notification */

/**
 * DONE:
 * unclutter for better modularity
 * 
 * this is just the waste for later use. 
 * You need notificationPermissions for Web-Push ;)
 */

use_package('modules.theme');
modules.theme.Main = new function () {
    base.main.ModuleRegistry.registerModule(this);

    this.notificationPermission = false;

    this.init = function () {

        /**
         * Only check for new Chat mesages, when logged in...
         */
        if (isLoggedIn) {
            //modules.theme.Main.globalCheckForNewMessages();
            //modules.theme.Main.checkNotificationPermission();
        }
    };

    this.checkNotificationPermission = function () {
        if ('Notification' in window) {
            // API supported
            if (Notification.permission === 'default') {
                Notification.requestPermission(modules.theme.Main.permissionGranted());
            }
            if (Notification.permission === 'granted') {
                modules.theme.Main.permissionGranted('granted');
            }
        } else {
            // API not supported
        }
    };
    this.permissionGranted = function (status) {
        if (status === 'granted') {
            modules.theme.Main.notificationPermission = true;
            console.log('Notifications granted..');
        }
    };

    this.notifyUser = function (head, body) {
        if (Notification.permission === 'granted') {
            var notification = new Notification(head, {
                body: body
            });

            notification.onshow = function () {
                document.getElementById('notificationSound').play();
                console.log('Notification shown');
            };
        }

    };
};