/**
 * initialises summernote editor
 */

use_package('modules.component');
modules.component.SummernoteEditor = new function () {
    base.main.ModuleRegistry.registerModule(this);
    base.main.ModuleRegistry.registerAjaxRefreshModule(this);

    this.init = function () {
        console.log("init summernote");
        $('.text,.summernote').summernote({
            minHeight: 300,             	// set minimum height of editor
            maxHeight: null,			// set maximum height of editor
            tabDisable: false,
            lang: "de-DE",
            toolbar: [['view', ['codeview']],
                        ['insert', ['link', 'picture', 'video']],
                        ['table', ['table']],
                        ['view', ['fullscreen', 'help']],
                        ['break'],
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        //['fontname', ['fontname']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['color', ['color']]
                    ],
            codemirror: {
                lineNumbers: true,
                lineWrapping: true,
                mode: "text/html"
            },
            styleTags: [
                    'p', 'h2', 'h3', 'h4', 'h5', 'h6', 'pre'
            ],
        });
    };
    this.ajaxRefresh = function(){
        modules.component.SummernoteEditor.init();
    }

};




