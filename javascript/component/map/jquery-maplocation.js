/**
 * jquery-maplocation.js
 * simple jQuery plugin to show a location on openstreetmaps. Adress is url encoded by this script.
 */

(function ($) {
    $.fn.mapLocation = function () {
        $(this).each(function(i, e){
            var elem = $(e);
            if(elem.data('mapified')!=='true'){
                elem.data('mapified','true');
            } else {
                return;
            }
            var address = elem.data('address');
            var height = elem.data('height');
            var width = elem.data('width');
            if(typeof height === 'undefined'){
                height = '400px';
            }
            if(typeof width === 'undefined'){
                width = '100%';
            }
            if(typeof address === 'undefined'){
                address = utils.string.stripHtml(elem.html()).replace(/(\r\n|\n|\r)/gm, ", ");;
            }
            console.log(address);
            //var url = "https://nominatim.openstreetmap.org/search?q=" + address + "&format=json&polygon=1&addressdetails=1";
            $.ajax({
                url: reLangUrl+"site?getAddressJSON="+encodeURIComponent(address),
                success: function (response) {
                    var json = JSON.parse(decodeURIComponent(response));
                    //console.log("JSON:",json[0]); // server response
                    var iframeUrl = "https://www.openstreetmap.org/export/embed.html?bbox="+json[0].boundingbox[2]+"%2C"+json[0].boundingbox[0]+"%2C"+json[0].boundingbox[3]+"%2C"+json[0].boundingbox[1]+"&amp;layer=mapnik&amp;marker="+json[0].lat+"%2C"+json[0].lon+""
                    var iframe = '<iframe class="locationMapIframe" width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" '
                            +'src="'+iframeUrl+'" '
                            +'</iframe>';
                    var url = '<a href="https://www.openstreetmap.org/search?query='+encodeURIComponent(address)+'" target="_blank">'+address+'</a>';
                    elem.html(url+"<br/>"+iframe);
                    //$(iframe).insertAfter(mapContainer);
                    //return response;
                }
            });
        });
    };

}(jQuery));