window.onload = function () {
    $('.text').each(function () {
        CKEDITOR.replace(this, {
            customConfig: relativeUrl + 'javascript/ck_config/tims_config.js',
            height: '200',
            toolbarGroups: [
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                {name: 'insert', groups: ['insert']},
                {name: 'others', groups: ['others']},
                {name: 'tools', groups: ['tools']},

                {name: 'colors', groups: ['colors']},
                {name: 'links', groups: ['links']},

                {name: 'about', groups: ['about']}
            ],
            removeButtons: 'qrc,Link,Unlink,Youtube,Video,Save,NewPage,Print,Preview,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Checkbox,Scayt,FontSize,Font,Format,About,ShowBlocks,Flash,SelectAll,PasteFromWord,PasteText,Paste,Outdent,Indent,Source,Templates,Cut,Copy,Undo,Redo,Replace,Find,Anchor,Image,Table,HorizontalRule,SpecialChar,PageBreak,Iframe,Maximize,Smiley,BidiLtr,BidiRtl,Language,JustifyRight,JustifyBlock,JustifyCenter,JustifyLeft,Blockquote,CreateDiv,NumberedList,BulletedList,CopyFormatting,Underline,Strike,Subscript,Superscript,Styles',
            templates_files: [relativeUrl + 'javascript/ck_config/tims_templates.js'],
            on: {
                instanceReady: function (ev) {
                    // fix for divarea plugin content area not setting focus on click
                    if ($(".cke_contents").data('clickhandled') !== 'true') {
                        console.warn("editor",ev.editor);
                        $(".cke_contents").css('cursor', 'text');
                        $(".cke_contents").off('click').click(function () {
                            $(this).find(".cke_wysiwyg_div").focus();
                        });
                        $(".cke_contents").data('clickhandled', 'true');
                    }
                }
            }
        });
    });
};
