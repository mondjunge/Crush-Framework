window.onload = function(){
    CKEDITOR.replace( 'text',{
        customConfig : relativeUrl+'javascript/ck_config/tims_config.js',
        height: '300',
        uiColor: '#eeeeee',
        templates_files: [relativeUrl + 'javascript/ck_config/tims_templates.js']
    });
}
