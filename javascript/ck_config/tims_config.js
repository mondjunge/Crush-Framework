/*
 Copyright (c) 2003-2009, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config)
{

    config.skin = 'moono-lisa-crush';
    //config.filebrowserBrowseUrl = relativeUrl+'extras/kcfinder-3/browse.php?type=files';
    //config.filebrowserImageBrowseUrl = relativeUrl+'extras/kcfinder-3/browse.php?type=images';
    config.filebrowserBrowseUrl = reLangUrl + 'files?contentOnly&fileType=file';
    config.filebrowserImageBrowseUrl = reLangUrl + 'files?contentOnly&fileType=image';
    config.filebrowserFlashBrowseUrl = reLangUrl + 'files?contentOnly&fileType=video';
    config.filebrowserVideoBrowseUrl = reLangUrl + 'files?contentOnly&fileType=video';
    
    // Define changes to default configuration here. For example:
    config.language = '' + language;
    //config.uiColor = '#eee';
    //http://localhost/Crush-Framework/min/?f=theme/default/css/extra/apure0.3.css,theme/default/css/abase_style.css,theme/default/css/style.css
    //config.contentsCss = relativeUrl + 'css/ckeditor/13218168';//relativeUrl+'js/?js=theme/'+siteStyle+'/css/extra/pure-legacy-0.3.css,theme/'+siteStyle+'/css/extra/pure-min-1.0.0.css,theme/'+siteStyle+'/css/base_style.css,theme/'+siteStyle+'/editorarea.css,theme/'+siteStyle+'/css/main_style.css';//editorarea.css';

    // Load the 'mystyles' styles set from a relative URL.
    config.stylesSet = 'default:' + relativeUrl + 'javascript/ck_config/tims_styles.js';
    //config.height = 300;

    config.shiftEnterMode = CKEDITOR.ENTER_BR;
    config.enterMode = CKEDITOR.ENTER_P;
    config.FillEmptyBlocks = false;
    config.FormatOutput = true;
    config.image_removeLinkByEmptyURL = false;
    config.entities = false;
    config.resize_enabled=false;
    config.allowedContent = true;
    config.disallowedContent = 'script;';
//    config.allowedContent = {
//        $1: {
//            // Use the ability to specify elements as an object.
//            elements: CKEDITOR.dtd,
//            attributes: true,
//            styles: true,
//            classes: true
//        }
//    };
    //config.disallowedContent = 'img{width,height};img[width,height]';
    //config.basicEntities = false;

    /** keep empty anchor tags for flowplayer */
    //config.protectedSource.push( /<a[\s\S]*?a>/g );

   // config.toolbar = 'Default';
    
    //config.toolbarCanCollapse = true;

    //config.extraPlugins = 'templates,dialog,font,video,codemirror,smiley,colorbutton,divarea,autogrow,ajax,autocomplete,panelbutton,textmatch,textwatcher,xml,emoji';//,dialog,widget,lineutils,clipboard,oembed';

    // 4.14.0
    config.extraPlugins = 'autogrow,video,codemirror';
    
    config.templates_replaceContent = false;

    config.image_prefillDimensions = false;

    //config.justifyClasses = [ 'AlignLeft', 'AlignCenter', 'AlignRight', 'AlignJustify' ];
    config.autoGrow_onStartup = true;
//    config.autoGrow_topSpace = '5px';
//    config.autoGrow_bottomSpace = '25px';
    config.autoGrow_minHeight = '200px';
    config.autoGrow_maxHeight = '100vh';

    config.codemirror = {

        // Set this to the theme you wish to use (codemirror themes)
        theme: 'default',

        // Whether or not you want to show line numbers
        lineNumbers: true,

        // Whether or not you want to use line wrapping
        lineWrapping: true,

        // Whether or not you want to highlight matching braces
        matchBrackets: true,

        // Whether or not you want tags to automatically close themselves
        autoCloseTags: true,

        // Whether or not you want Brackets to automatically close themselves
        autoCloseBrackets: true,

        // Whether or not to enable search tools, CTRL+F (Find), CTRL+SHIFT+F (Replace), CTRL+SHIFT+R (Replace All), CTRL+G (Find Next), CTRL+SHIFT+G (Find Previous)
        enableSearchTools: true,

        // Whether or not you wish to enable code folding (requires 'lineNumbers' to be set to 'true')
        enableCodeFolding: true,

        // Whether or not to enable code formatting
        enableCodeFormatting: true,

        // Whether or not to automatically format code should be done when the editor is loaded
        autoFormatOnStart: true,

        // Whether or not to automatically format code should be done every time the source view is opened
        autoFormatOnModeChange: true,

        // Whether or not to automatically format code which has just been uncommented
        autoFormatOnUncomment: true,

        // Define the language specific mode 'htmlmixed' for html including (css, xml, javascript), 'application/x-httpd-php' for php mode including html, or 'text/javascript' for using java script only
        mode: 'htmlmixed',

        // Whether or not to show the search Code button on the toolbar
        showSearchButton: true,

        // Whether or not to show Trailing Spaces
        showTrailingSpace: true,

        // Whether or not to highlight all matches of current word/selection
        highlightMatches: true,

        // Whether or not to show the format button on the toolbar
        showFormatButton: true,

        // Whether or not to show the comment button on the toolbar
        showCommentButton: true,

        // Whether or not to show the uncomment button on the toolbar
        showUncommentButton: true,

        // Whether or not to show the showAutoCompleteButton button on the toolbar
        showAutoCompleteButton: true,

        // Whether or not to highlight the currently active line
        styleActiveLine: true
    };
    
    // for reference
//    config.toolbar = [
//            { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
//            { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
//            { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
//            { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
//            '/',
//            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
//            { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
//            { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
//            { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
//            '/',
//            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
//            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
//            { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
//            { name: 'about', items: [ 'About' ] }
//    ];
    
    //Default
    config.toolbarGroups = [
            { name: 'document', groups: [ 'mode', 'document', 'doctools'  ] },
            { name: 'links', groups: [ 'links' ] },
            { name: 'forms', groups: [ 'forms' ] },
            { name: 'insert', groups: [ 'insert' ] },
             { name: 'others', groups: [ 'others' ] },
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
            { name: 'tools', groups: [ 'tools' ] },
            '/',
            { name: 'styles', groups: [ 'styles' ] },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'colors', groups: [ 'colors' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            
            { name: 'about', groups: [ 'about' ] }
    ];

    config.removeButtons = 'AutoComplete,UncommentSelectedRange,CommentSelectedRange,autoFormat,searchCode,Save,NewPage,Print,Preview,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Checkbox,FontSize,Font,Format,About,ShowBlocks,Flash,SelectAll,PasteFromWord,PasteText,Paste,Outdent,Indent,codemirrorhelp';

    config.smiley_path = relativeUrl + 'theme/' + siteStyle + '/images/smileys/green/';
    config.smiley_images = ['angry.gif', 'cool.gif', 'emm.gif', 'grin.gif', 'heul.gif', 'hmm.gif', 'holy.gif', 'huuh.gif', 'lol.gif', 'muede.gif', 'no.gif', 'noemote.gif', 'noway.gif', 'omg.gif', 'rofl.gif', 'rolleyes.gif', 'sad.gif', 'sleep.gif', 'smile.gif', 'tongue.gif', 'twink.gif', 'twinkle.gif', 'wtf.gif', 'yes.gif', 'smoke.gif',  'nr2.gif', 'argue.gif', 'baaah.gif', 'clap.gif', 'cuddl.gif', 'freu.gif', 'goodbye.gif', 'hello.gif', 'hug.gif', 'insel.gif', 'king.gif', 'laola.gif', 'meditation.gif', 'nofascho.gif', 'nonono.gif', 'nr1.gif', 'rofl.gif', 'rtfm.gif', 'thumbdown.gif', 'thumbup.gif'];
};
