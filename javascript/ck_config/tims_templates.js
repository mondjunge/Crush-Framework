/*
 Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
*/
CKEDITOR.addTemplates("default",{
    imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates")+"../../../../"),
    templates:[{
        title:"3/3 Spalten, responsive",
        image: "javascript/ck_config/images/Startseite.gif",
        description:"3  1/3 Spalten. Responsive.",
        html:'<div class="pure-g pure-g-r"><div class="pure-u-1-3"><p>1/3</p></div><div class="pure-u-1-3"><p>1/3</p></div><div class="pure-u-1-3"><p>1/3</p></div></div>'
    },{
        title:"2/3 1/3 Template",
        image: "javascript/ck_config/images/zweidrittel.gif",
        description:"Zwei Spalten, die linke 2/3 groß, die rechte 1/3 groß. Responsive.",
        html:'<div class="pure-g pure-g-r"><div class="pure-u-2-3"><p>2/3</p></div><div class="pure-u-1-3"><p>1/3</p></div></div>'
    },{
        title:"1/2 Template",
        image: "",
        description:"Zwei Spalten, beide 1/2 groß. Responsive.",
        html:'<div class="pure-g pure-g-r"><div class="pure-u-1-2"><p>1/2</p></div><div class="pure-u-1-2"><p>1/2</p></div></div>'
    },{
        title:"Bild mit Untertitel, zentriert",
        image: "",
        description:"Bild mit Rahmen und Untertitel. Responsive.",
        html:'<div class="pure-g pure-g-r"><div class="pure-u-1-5">&nbsp;</div><div class="pure-u-3-5"><div class="imgBox"><img alt="Bild" src="'+relativeUrl +'javascript/ck_config/images/placeholder.gif" title="Ein Foto" /><span class="imgSub">Ein Untertitel für das Bild</span></div></div><div class="pure-u-1-5">&nbsp;</div></div>'
    },{
        title:"Bild float:right",
        image: "",
        description:"Bild mit Rahmen und Untertitel. 25% breit. Rechtsbündig floating.",
        html:'<div class="imgBox" style="float:right; width:25%;"><img alt="Bild" src="'+ relativeUrl +'javascript/ck_config/images/placeholder.gif" title="Ein Foto" /><span class="imgSub">Ein Untertitel für das Bild</span></div>'
    },{
        title:"MP4 Video mit Untertitel, zentriert",
        image: "",
        description:"Video mit Rahmen und Untertitel. Responsive.",
        html:'<div class="pure-g pure-g-r"><div class="pure-u-1-5">&nbsp;</div><div class="pure-u-3-5 imgBox" style="box-sizing: border-box; margin:0px"><video controls="" style="width:100%; max-height: 80vh;"><source src="/upload/images/blog/lenilaeuft/lenilaeuft.mp4" type="video/mp4" />Your browser doesn\'t support video.<br />Please download the file: <a href="/upload/images/blog/lenilaeuft/lenilaeuft.mp4">video/mp4</a>&nbsp;</video><span class="imgSub">Ein Untertitel für das Video</span></div><div class="pure-u-1-5">&nbsp;</div></div>'
    },{
        title:"3 Bilder",
        image: "javascript/ck_config/images/3Bilder.gif",
        description:"3 Bilder horizontal nebeneinander angeordnet. Gut als Trenner oder Abschluß. Responsive.",
        html:'<div class="pure-g pure-g-r"><div class="pure-u-1-3"><div class="imgBox"><img alt="Ein Bild" src="'+relativeUrl +'javascript/ck_config/images/placeholder.gif" /><span class="imgSub">Ein Untertitel für das Bild</span></div></div><div class="pure-u-1-3"><div class="imgBox"><img alt="Ein Bild" src="'+relativeUrl +'javascript/ck_config/images/placeholder.gif" /><span class="imgSub">Ein Untertitel für das Bild</span></div></div><div class="pure-u-1-3"><div class="imgBox"><img alt="Ein Bild" src="'+relativeUrl +'javascript/ck_config/images/placeholder.gif" /><span class="imgSub">Ein Untertitel für das Bild</span></div></div></div>'
    },{
        title:"Slider",
        image: "javascript/ck_config/images/3Bilder.gif",
        description:"Slider mit 3 Bildern. Anzahl variabel.",
        html:"<div class='imageRotator' data-controls='false' data-height='auto' data-timeout='5000'><div class='rotateContainer '><img alt='Ein Bild' src='"+relativeUrl +"javascript/ck_config/images/banner3_1.png' /><div class='imgSub'><br />Ein Untertitel für das Bild<br />&nbsp;</div></div><div class='rotateContainer '><img alt='Ein Bild' src='"+relativeUrl +"javascript/ck_config/images/banner2_3_1.png' /><div class='imgSub'><br />Ein Untertitel für das Bild<br />&nbsp;</div></div><div class='rotateContainer'><img alt='Ein Bild' src='"+relativeUrl +"javascript/ck_config/images/banner3_3_1.png' /><div class='imgSub'><br />Ein Untertitel für das Bild<br />&nbsp;</div></div></div>"
    },{
        title:"Karteneinbindung",
        image: "",
        description:"Adresse mit automatischer OpenStreetMap Karteneinbindung",
        html:"<p class='mapLocation'>Dresdener Straße 18<br />32423 Minden<br/></p>"
    }]
    });
    
