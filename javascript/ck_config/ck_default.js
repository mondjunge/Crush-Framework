window.onload = function(){
    $('.text').each(function(){
        CKEDITOR.replace( this,{
            customConfig : relativeUrl+'javascript/ck_config/tims_config.js',
            uiColor: 'transparent',
            toolbarCanCollapse : true,
            templates_files: [relativeUrl + 'javascript/ck_config/tims_templates.js'],
//            toolbarGroups: [
//                { name: 'code' },
//                { name: 'styles' },
//                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
//                { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
//                { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
//                {name: 'insert', groups: ['insert']},
//                {name: 'others', groups: ['others']},
//                {name: 'tools', groups: ['tools']},
//
//                {name: 'colors', groups: ['colors']},
//                {name: 'links', groups: ['links']},
//
//                {name: 'about', groups: ['about']}
//            ],
            removeButtons: 'Link,Unlink,Save,NewPage,Print,Preview,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Checkbox,Scayt,FontSize,Format',
            on: {
                instanceReady: function (ev) {
                    // fix for divarea plugin content area not setting focus on click
                    if ($(".cke_contents").data('clickhandled') !== 'true') {
                        console.warn("editor",ev.editor);
                        $(".cke_contents").css('cursor', 'text');
                        $(".cke_contents").off('click').click(function () {
                            $(this).find(".cke_wysiwyg_div").focus();
                        });
                        $(".cke_contents").data('clickhandled', 'true');
                    }
                }
            }
        });
    });
    
};
