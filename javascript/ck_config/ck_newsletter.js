window.onload = function(){
    CKEDITOR.replace( 'text',{
        customConfig : relativeUrl+'javascript/ck_config/tims_config.js',
        uiColor: 'transparent',
        toolbarCanCollapse : true,
        templates_files: [relativeUrl + 'javascript/ck_config/tims_templates.js'],
        on: {
            instanceReady: function (ev) {
                // fix for divarea plugin content area not setting focus on click
                if ($(".cke_contents").data('clickhandled') !== 'true') {
                    console.warn("editor",ev.editor);
                    $(".cke_contents").css('cursor', 'text');
                    $(".cke_contents").off('click').click(function () {
                        $(this).find(".cke_wysiwyg_div").focus();
                    });
                    $(".cke_contents").data('clickhandled', 'true');
                }
            }
        }
    });
};
