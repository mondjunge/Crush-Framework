/* global utils */

/**
 * utils
 */

use_package("utils");

utils.overlay = new function () {

    /**
     * Trap the focus in overlay under all circumstances.
     */
    var bindFocusTrap = function () {

        console.log("Overlay: Focus Trap start");

        var overlay = $("#fullOverlay");

        overlay.bind('keydown', function (e) {
            if (e.keyCode === 27) {
                //ESC
                utils.overlay.hide();
            }
            if (e.keyCode === 9) {
                // TAB
                if (e.shiftKey) {
                    // tab and Shift pressed...
                    if ($(document.activeElement).attr("firstmodal") === "true") {
                        e.preventDefault();
                        overlay.find("[lastmodal]").focus();
                    }
                    return;
                }
                // tab without shift pressed
                if ($(document.activeElement).attr("lastmodal") === "true") {
                    e.preventDefault();
                    overlay.find("[firstmodal]").first().focus();
                }
            }
        });

        $("[firstfocusableelement]").off("focus");
        $("[firstfocusableelement]").on('focus', function () {

            if (overlay && overlay.length > 0) {
                overlay.find("[firstmodalfield]").focus();
            }
        });

        $("[lastfocusableelement]").off("focus");
        $("[lastfocusableelement]").on('focus', function () {

            if (overlay && overlay.length > 0) {
                overlay.find("[lastmodal]").focus();
            }
        });

        $(".overlayBackground").off("focus");
        $(".overlayBackground").on('focus', function () {

            if (overlay && overlay.length > 0) {
                overlay.find("[firstmodal]").focus();
            }
        });


    };
    /**
     * remove the focus Trap
     */
    var unbindFocusTrap = function () {
        console.log("Overlay: Remove focus Trap");
        $("[firstfocusableelement]").off("focus");
        $("[lastfocusableelement]").off("focus");
        $("#fullOverlay").off("keydown");
    };

    var setPageMarkers = function () {
        // set Page Markers for modal focus trap
        var focusablePageElements = $(document).find(':focusable');
        focusablePageElements.first().attr("firstfocusableelement", true);
        focusablePageElements.last().attr("lastfocusableelement", true);

        var focusableOverlayElements = $("#fullOverlay").find(':focusable');
        focusableOverlayElements.first().attr("firstmodal", true);
        focusableOverlayElements.last().attr("lastmodal", true);
    };

    /**
     * Shows an overlay from within JS
     * @param {type} html - the html to be shown in the overlay
     * @param {type} clickedElement - this is either an Element containing a data-title attribute or a String, to be used as heading for the overlay.
     * @param {type} callback - a function to be called after selecting/clicking a picture 
     * @param {type} cssClass - a cssClass to be set on the overlay container.
     * @return {undefined}
     */
    this.show = function (html, clickedElement, callback, cssClass) {


        $("#backgroundCurtain").show();
        var title = "";
        //alert(typeof clickedElement);
        if (typeof clickedElement === "string") {
            title = clickedElement;
        } else {
            title = $(clickedElement).data('title');
        }


        $("#titleOverlay").html(title);
        $("#fullOverlayContent").html(html);
        $("#fullOverlay").show();
        $("#fullOverlay").addClass(cssClass);
        $("body").css('overflow-y', 'hidden');
        $("#fullOverlay").find(':focusable').first().focus();
        setPageMarkers();
        bindFocusTrap();

        if (typeof callback !== 'undefined') {
            //eval(callback);
            window[callback];
        }
        $('.closeOverlay').on("click", function () {
            utils.overlay.hide();
            return false;
        });
    };

    /**
     * Hide the overlay from within JS
     * @return {undefined}
     */
    this.hide = function () {
        $("#fullOverlay").hide();
        $("#backgroundCurtain").hide();
        $("body").css('overflow-y', 'scroll');
        unbindFocusTrap();
    };

    /**
     * Set a title for the current shown overlay
     * @param {type} title
     * @return {undefined}
     */
    this.setTitle = function (title) {
        $("#titleOverlay").html(title);
    };

};

utils.waiting = new function () {
    this.startWait = function () {

        var html = "<div id='waitingCurtain' style='text-align:center; position:fixed; top:0;bottom:0;left:0;right:0; z-index:1100'> "
                + "<div style='text-align:center; position:absolute; top:0;bottom:0;left:0;right:0; background-color:#333; opacity:0.3'></div>"
                + '<div class="loadingSpinner ">'
                + '<div class="sk-spinner sk-spinner-wandering-cubes">'
                + '<div class="sk-cube1"></div>'
                + '<div class="sk-cube2"></div>'
                + '</div>'
                + '</div>'
                + "</div>";
        $('body').append(html);
    };

    this.stopWait = function () {
        console.log("stop waiting");
        $('#waitingCurtain').remove();
    };
};

utils.ajax = new function () {
    /**
     * 
     * @param {String} url
     * @param {function} callback
     * @returns {undefined}
     */
    this.get = function (url, callback) {
        $.ajax({
            type: "GET",
            url: reLangUrl + "" + url,
            async: true,
            timeout: 5000,
            success: callback
        });
    };
    /**
     * 
     * @param {String} url - for url
     * @param {List} data - list {data:"string",[...]} form data list
     * @param {function} callback - function or name of a function to call after success..
     * @returns void
     */
    this.post = function (url, data, callback) {
        $.ajax({
            type: "POST",
            url: reLangUrl + "" + url,
            async: true,
            timeout: 5000,
            data: data,
            success: callback
        });
    };
};

utils.string = new function () {
    this.stripHtml = function (html)
    {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }
    this.escapeHtml = function (text) {
        return text
                .replace(/&/g, "&amp;")
                .replace(/</g, "&lt;")
                .replace(/>/g, "&gt;")
                .replace(/"/g, "&quot;")
                .replace(/'/g, "&#039;");
    };
    this.rot13 = function (s) {
        return s.replace(/[A-Za-z]/g, function (c) {
            return String.fromCharCode(c.charCodeAt(0) + (c.toUpperCase() <= "M" ? 13 : -13));
        });
    };
    this.showEmailHandler = function () {
        console.log("click email handler init");
        $('.emailLink').click(function () {
            console.log("click email");
            let encodedEmail = $(this).next('span').data('hash');
            console.log("click email" + encodedEmail);
            let decodedEmail = utils.string.rot13(atob(encodedEmail).split("").reverse().join(""));
            this.href = "mailto:" + decodedEmail;
            this.text = decodedEmail;
            $(this).off('click');
        });
    };
    /**
     * @deprecated use html5 placeholder attribute instead.
     * @param {type} element
     * @param {type} phString
     * @returns {undefined}
     */
    this.hidePlaceholder = function (element, phString) {
        // hideplaceholder onfocus
        if ($(element).val() === phString) {
            $(element).val('');
            //$(element).css('color','');
            $(element).removeClass('placeholder');
        }
        // read placeholder on blur
        $(element).blur(function () {
            if ($(element).val() === '') {
                $(element).val(phString);
                $(element).addClass('placeholder');
            }
        });
    };
    this.urlBase64ToUint8Array = function (base64String) {
        const padding = '='.repeat((4 - base64String.length % 4) % 4);
        const base64 = (base64String + padding)
                .replace(/\-/g, '+')
                .replace(/_/g, '/');

        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);

        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    };
};

utils.check = new function () {
    this.deleteCheck = function (text) {
        return confirm(text);
    };
};

utils.localStorage = new function () {
    this.set = function (key, value) {
        console.log("set localStorage: " + key + " : " + value);
        localStorage.setItem(configPrefix  + key, value);
    };
    this.get = function (key) {
        console.log("get localStorage:", key);
        return localStorage.getItem(configPrefix  + key);
    };
    this.remove = function (key) {
        console.log("get localStorage:", key);
        localStorage.removeItem(configPrefix  + key);
    };
};

utils.webpush = new function () {
    var self = this;
    this.appServerKey = $("input[name='appServerKey']").val();
    this.permissionsGranted = false;
    if(typeof userId === 'undefined'){
        return;
    }
    if( utils.localStorage.get(userId + '_webpush_endpoint') !== null ){
        this.pushNotificationsEnabled = true;
    } else {
        this.pushNotificationsEnabled = false;
    }

    this.askNotificationPermission = function () {
        return new Promise(function (resolve, reject) {
            const permissionResult = Notification.requestPermission(function (result) {
                resolve(result);
            });

            if (permissionResult) {
                permissionResult.then(resolve, reject);
            }
        }).then(function (permissionResult) {
            if (permissionResult !== 'granted') {
                throw new Error('We weren\'t granted permission.');
            }
            this.permissionsGranted = true;
            utils.eventRegistry.fireEvent("notificationPermissionGranted");
            return permissionResult;
        }).catch(function (err) {
            console.log('Unable to get permission to notify.', err);
             utils.eventRegistry.fireEvent("notificationPermissionError");
        });
    };
    this.subscribe = function () {
        /**
         * ask permission and listen for persmission granted 
         * if permission granted: subscribe
         * 
         */

        utils.eventRegistry.addListener("notificationPermissionGranted", function () {
            return navigator.serviceWorker.ready.then(function (registration) {
                //console.log('reg Object: ', registration);
                console.log('subscribing using ask: ', self.appServerKey);
                const subscribeOptions = {
                    userVisibleOnly: true,
                    applicationServerKey: utils.string.urlBase64ToUint8Array(
                            "" + self.appServerKey
                            )
                };

                // console.log('Received subscriptionOptions: ',
                // JSON.stringify(subscribeOptions));
                return registration.pushManager.subscribe(subscribeOptions);

            }).catch(function (error) {
                console.error("Error subscribing push: ", error);
                if (self.appServerKey.length < 64) {
                    console.error("You seem to have mixed up the private/public VAPID keys in web-push configuration. Please check.");
                } else {
                    console.error("Is there an internet connection on both, the server and your client? Have you configured web-push correctly?");
                }

                utils.eventRegistry.fireEvent("subscriptionFailed");
                return;
            }).then(function (pushSubscription) {
                if (typeof pushSubscription === 'undefined') {
                    return;
                }
                console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
                // send subscription details to server
                console.log('endpoint: ' + pushSubscription.endpoint);
                console.log('key p256dh: ' + pushSubscription.getKey('p256dh'));
                console.log('key auth: ' + pushSubscription.getKey('auth'));
                

                if (self.sendSubscriptionToServer(pushSubscription)) {
                    self.pushNotificationsEnabled = true;
                    utils.localStorage.set(userId + '_webpush_endpoint', encodeURIComponent(pushSubscription.endpoint));
                    utils.eventRegistry.fireEvent("subscriptionCompleted");
                }

                return pushSubscription;
            });
        },configPrefix,true);

        self.askNotificationPermission();
    };
    this.unsubscribe = function () {
        self.pushNotificationsEnabled = false;
        var localEndpoint = utils.localStorage.get(userId + '_webpush_endpoint');
        console.log("Unsubscribe Endpoint " + localEndpoint);

       // if (typeof localEndpoint !== 'undefined' && localEndpoint !== null) {
            console.log("Removing Endpoint " + localEndpoint + " from db");
            jQuery.ajax({
                type: 'POST',
                url: reLangUrl + '/devices?unsubscribeWebPushSubscription',
                data: {
                    endpoint: localEndpoint
                },
                success: function (response) {
                     utils.localStorage.remove(userId + '_webpush_endpoint');
                     utils.eventRegistry.fireEvent("unsubscribeComplete");
                     return true;
                },
                error: function (response, b, c) {
                    console.error("unsubscribe Error: " + c);
                    return false;
                }
            });
       // }
        
        // ???
        //this.removeEndpointNextCheck();

        return true;

    };
    this.sendSubscriptionToServer = function (pushSubscription) {
        console.log("subscribeOnWebsite called");

        var jsonString = JSON.stringify(pushSubscription);
        var subscriptionObject = JSON.parse(jsonString);

        console.log("endpoint: " + encodeURIComponent(subscriptionObject.endpoint));
        console.log("p256dh: " + subscriptionObject.keys.p256dh);
        console.log("auth: " + subscriptionObject.keys.auth);

        return jQuery.ajax({
            type: 'POST',
            url: reLangUrl + '/devices?registerWebPushSubscription',
            cache: false,
            //contentType: "multipart/form-data",
            //processData: false,
            data: {
                endpoint: encodeURIComponent(subscriptionObject.endpoint),
                p256dh: subscriptionObject.keys.p256dh,
                auth: subscriptionObject.keys.auth,
                useragent: encodeURIComponent(navigator.userAgent)
            },
            success: function (response) {

                console.log("registerPushIdentifier Result: ", response);

                if (response === 'true') {
                    console.log("Successful send subscription to server.");
                    return true;
                } else {
                    console.log("Server Error sending subscription to server.");
                    return false;
                }
            },
            error: function (response, b, c) {
                console.error("registerPushIdentifier Error: ", c);
                return false;
            }

        });
    };
};

utils.messages = new function () {
    
    this.success = function(msg){
        var icon = '<svg viewBox="0 0 8 8" class="icon svgIcon svgIcon-light messageIcon systemMessageIcon">' +
                '<use xlink:href="'+relativeUrl+'/theme/default/images/icons/open-iconic/sprite/sprite.min.svg#check"></use>'+
                '</svg>';
        $('#msgsContainer').append('<div class="message system">'+icon+'<p>'+msg+'</p></div>');
    };
    this.error = function(msg){
         var icon = '<svg viewBox="0 0 8 8" class="icon svgIcon svgIcon-light messageIcon errorMessageIcon">' +
                '<use xlink:href="'+relativeUrl+'/theme/default/images/icons/open-iconic/sprite/sprite.min.svg#warning"></use>'+
                '</svg>';
        $('#msgsContainer').append('<div class="message system">'+icon+'<p>'+msg+'</p></div>');
    };
    
};

/**
 * Register listeners and fire custom events
 */
utils.eventRegistry = new function () {
    var eventReg = {};
    var eventCounter = 0;	// increased with each event registered

    /**
     * @param eventName - string
     * @param func - function to be called when eventName is fired
     * @param scope - object - optional
     * @param destoryWhenFired - boolean - optional - default false : when the listener is fired once then it is destroyed
     */
    this.addListener = function (eventName, func, scope, destoryWhenFired) {
        if (!eventReg[eventName]) {
            eventReg[eventName] = [];
        }

        if (func) {
            console.log("registering event: ", eventName);
            func.eventId = eventCounter++;	// give the function an eventId
            func.eventName = eventName;	// register eventName too
            console.log("listener registered for event", func.eventName, "with Id", func.eventId);
            eventReg[eventName].push({
                func: func,
                scope: scope,
                destoryWhenFired: destoryWhenFired
            });
        } else {
            console.warn("no function defined when adding a listener", eventName);
        }
    };

    // fire events by name across the application
    this.fireEvent = function (eventName) {
        console.log("fired event: ", eventName);
        var events = eventReg[eventName] || [],
                value;
        for (var i = 0, l = events.length; i < l; i++) {
            value = events[i];
            if (value && value.func) {
                if (value.scope) {
                    value.func.apply(value.scope);
                } else {
                    value.func.call();
                }

                if (value.destoryWhenFired === true) {
                    this.destroyListener(value.func);
                }
            }

        }
    };

    /**
     * @param func = function itself
     */
    this.destroyListener = function (func) {
        if (func) {
            var eventName = func.eventName,
                    events = eventReg[eventName], //array of objects {} see addListener for more details
                    eventId = func.eventId;
            console.log("destroy listener registered for event", eventName, "with Id", eventId);
            // iterate all events for given eventName;
            for (var i = 0, l = events.length; i < l; i++) {
                //check to see if the iterated event has the same eventId as the parameter func
                if (events[i].func.eventId == eventId) {
                    eventReg[eventName].splice(i, 1);
                    break;
                }
            }
        }
    };

    /**
     * @param - eventName (string), destroys all listeners for the given eventName
     * beware! this could destroy system based events - use destroyListener() instead and destroy each listener at a time
     */
    this.destroyEvent = function (eventName) {
        delete eventReg[eventName];
    };

};