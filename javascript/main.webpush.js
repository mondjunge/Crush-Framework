
use_package("main");

main.webpush = new function(){
    base.main.ModuleRegistry.registerModule(this);
    var self = this;
    
    this.init = function(){
        //main.webpush.requestNotificationsPermissions(false);
    };
    this.requestNotificationsPermissions = function (checkOnly){
    	
    	console.log('Request Permission for Notifications.');
    	
    	if (Notification.permission === 'denied') {
    		self.unsubscribe();
    		console.log('permission denied.');
            return;
        }
    	
		return new Promise(function(resolve, reject) {
				const permissionResult = Notification.requestPermission(function(result) {
				resolve(result);
			});
			
			if (permissionResult) {
				permissionResult.then(resolve, reject);
			}}).then(function(permissionResult) {
				if (permissionResult !== 'granted') {
					if(checkOnly){
						self.unsubscribe();
						self.permissionsGranted = false;
						self.showButtonHandler();
					}
					throw new Error('We weren\'t granted permission.');
				}
			 	self.permissionsGranted = true;
			 	if(!checkOnly){
			 		//eventRegistry.fireEvent("notifyPermissionsGranted");
			 	}
			 	return permissionResult;
			}).catch(function(err) {
	    	  console.log('Unable to get permission to notify.', err);
	    	  self.unsubscribe();
	    	  self.permissionsGranted = false;
	    	  self.showButtonHandler();
    	});
    };
    
    this.subscribe = function(){
        
    };
    
    this.unsubscribe = function(){
        
    };
    
};
