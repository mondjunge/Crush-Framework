/* global base, utils, reLangUrl, relativeUrl */

/**
 * Initializes JS functionality.
 * 
 * Modules need to register themself, example for all, exclude what you do not need:
 * base.main.ModuleRegistry.registerModule(this);
 * base.main.ModuleRegistry.registerAjaxRefreshModule(this);
 * base.main.ModuleRegistry.registerAjaxStartModule(this);
 * 
 * Modules to be initialized need a method 'init'. 
 * Modules to be refreshed after AJAX requests need a 'ajaxRefresh' method.
 * Modules to be started on ajax AJAX requests need a 'ajaxStart' method.
 */

//var DEBUG = false;
if (typeof DEBUG === 'undefinded') {
    var DEBUG = false;
}
if (!DEBUG) {
    console.info('Console debug and log disabled.');
    if (!window.console) {
        window.console = {};
    }
    var methods = ["log", "debug"];
    for (var i = 0; i < methods.length; i++) {
        console[methods[i]] = function () {
        };
    }
}
/**
 * Helper to 'declare' packages.
 * @param packageName
 */
function use_package(packageName) {
    if (typeof (packageName) !== "string") {
        throw new Error("Can only declare packages in string format!");
    }
    var packageParts = packageName.split(".");
    currentPackage = window;
    i = 0;
    len = packageParts.length;
    for (; i < len; i++) {
        if (!currentPackage[packageParts[i]]) {
            currentPackage[packageParts[i]] = {};
        }
        currentPackage = currentPackage[packageParts[i]];
    }
}

use_package("base.main");

/**
 * $(function(){}) shortcut for $(document).on("ready", function(){});
 * 
 */
$(function () {

    /**
     * register Service Worker
     * initialize registered Javascript Modules
     */
    base.main.Serviceworker.register();
    base.main.ModuleRegistry.initializeAll();

}).ajaxStart(function () {

    /**
     * enables a progress function to be used with jQuery ajax.
     * Src: http://stackoverflow.com/questions/16722356/jquery-ajax-progress-deferred
     * @param {type} $
     * @returns {undefined}
     */
    (function addXhrProgressEvent($) {
        var originalXhr = $.ajaxSettings.xhr;
        $.ajaxSetup({
            xhr: function () {
                var req = originalXhr(), that = this;
                if (req) {
                    if (typeof req.addEventListener === "function" && that.progress !== undefined) {
                        req.addEventListener("progress", function (evt) {
                            that.progress(evt);
                        }, false);
                    }
                    if (typeof req.upload === "object" && that.progressUpload !== undefined) {
                        req.upload.addEventListener("progress", function (evt) {
                            that.progressUpload(evt);
                        }, false);
                    }
                }
                return req;
            }
        });
    })(jQuery);

    base.main.ModuleRegistry.indicateAjaxRequestStart();

}).ajaxStop(function () {

    /**
     * run registered refresh methods of JS-Modules
     */
    base.main.ModuleRegistry.refreshAll();

});

base.main.Serviceworker = new function () {
    var self = this;
    this.registration;
    this.register = function () {
        if ('serviceWorker' in navigator && 'controller' in navigator.serviceWorker) {
            console.log(navigator);
            if (navigator.serviceWorker.controller !== null && navigator.serviceWorker.controller.state === 'activated') {
                // sw already activated, return registration
                return self.registration;
            } else {

                console.log("registering service worker...");
                navigator.serviceWorker.register(relativeUrl + "serviceworker.js", {scope: relativeUrl}).then(function (registration) {
                    //alert("OK. Click test");
                    console.log(registration);
                    self.registration = registration;
                }).catch(function (error) {
                    //alert("error");
                    console.log(error);
                });
            }
        } else {
            // sw not supported
            console.log("Serviceworker not supported");
        }
    };
};

/**
 * base class to register javascript modules to execute on documentReady, ajaxEnd or ajaxStart
 */
base.main.ModuleRegistry = new function () {
    var initModules = [];
    var ajaxRefreshModules = [];
    var ajaxStartModules = [];

    this.registerModule = function (module) {
        initModules.push(module);
        return this;
    };

    this.registerAjaxRefreshModule = function (module) {
        ajaxRefreshModules.push(module);
        return this;
    };

    this.registerAjaxStartModule = function (module) {
        ajaxStartModules.push(module);
        return this;
    };

    this.initializeAll = function () {
        callModules(initModules, 'init', "Initialising JS-Module:");
    };

    this.refreshAll = function () {
        callModules(ajaxRefreshModules, 'ajaxRefresh', "Refreshing JS-Module after AJAX request:");
    };

    this.indicateAjaxRequestStart = function () {
        callModules(ajaxStartModules, 'ajaxStart', "Indicating ajax start to JS-Module:");
    };

    var callModules = function (modules, method, errorBaseMessage) {
        
        for (var i = 0, length = modules.length; i < length; i++) {
            try {
                modules[i][method]();
            } catch (e) {
                throw new Error(errorBaseMessage + " " + e.toString() + "\n## Code: \n\n " + modules[i] +" "+method);
            }
        }
    };
};


