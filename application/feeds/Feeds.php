<?php

/**
 *  Copyright © tim 12.02.2015
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
/**
 * TODO:#FIXME
 * - filter_input\([A-Z_]*,[a-zA-Z _']*\) may lead to sql injection attacks
 * 
 */
class Feeds extends AppController {

//    public function onPost($postVariable,$function){
//        $var = filter_input(INPUT_POST, $postVariable);
//        if(!$var && $var!=NULL){
//            $function($var);
//        }
//    }
    public function index() {
        $this->autoCheckRights();
        if($this->checkSiteRights('feeds/admin')){
            $vars['adminMode'] = true;
        }
        //---> experiment
//        $this->onPost('removeFeed',function($data){
//            
//        });
        //experiment
        
        // removes feed from user
        $removeFeedId = filter_input(INPUT_POST, 'removeFeed');
        if ($removeFeedId!=FALSE && $removeFeedId!=NULL) {
            $this->standalone = true;
            $this->removeFeed($removeFeedId);
            echo "success";
            return;
        }
        
        $rs = $this->select('feeds', '*','1','id');
        $c = [];
        foreach ($rs as $f) {
            if ($f['category'] == '') {
                $c['Sonstiges']['feeds'][$f['id']] = $f;
               $c['Sonstiges']['feeds'][$f['id']]['favicon'] = $this->getFavicon($f['url']);
                $c['Sonstiges']['title'] = 'Sonstiges';

                //$c['Sonstiges']['feeds'][$f['id']]['lastEntries'] = $this->getLatestEntries($f['url'], 3);
            } else {
                $c[$f['category']]['feeds'][$f['id']] = $f;
               $c[$f['category']]['feeds'][$f['id']]['favicon'] = $this->getFavicon($f['url']);
                $c[$f['category']]['title'] = $f['category'];

               // $c[$f['category']]['feeds'][$f['id']]['lastEntries'] = $this->getLatestEntries($f['url'], 3);
            }
        }

        $vars['category'] = $c;
        $this->view('feedsMain', $vars);
    }
    
    public function fetchFeed(){
        $this->standalone = true;
        if(!$this->checkSiteRights('feeds')){
            return;
        }
        
        $fetchId = filter_input(INPUT_POST, 'fetchFeed');
        if ($fetchId!=FALSE && $fetchId!=NULL) {
            echo $this->getLatestEntriesHtml($fetchId, 4);
            
        }else{
            $param[1] = $fetchId;
            echo $this->ts("Error fetching feed... id: $1",$param);
        }
        return;
    }

    public function admin() {
        //put your administrative code here
        $this->autoCheckRights();
        if (filter_has_var(INPUT_POST,'delete')) {
            $this->deleteFeed();
        }
        if (filter_has_var(INPUT_POST,'save')) {
            $this->updateFeed();
        }
        if (filter_has_var(INPUT_POST,'addFeed')) {
            $this->addFeed();
        }


        $vars['feeds'] = $this->select('feeds', '*', '', 'id desc');

        $this->view('adminEdit', $vars);
    }
    
    private function removeFeed($id) {
        
        

        $userFeeds = $this->select('feeds_user', '*', "user_id='".Session::get('uid')."'");
        $feeds = explode(",", $userFeeds[0]['feed_ids']);
        // array_push($feeds, $newId);
        $key = array_search($id, $feeds);
        unset($feeds["$key"]);
        array_multisort($feeds);
        print_r($feeds);
        $feedsUnique = array_unique($feeds);

        $feedsString = implode(",", $feedsUnique);
        // $feedsString = "1,3";

        $fieldsValueArray = array(
            'feed_ids' => "" . $feedsString . ""
        );
        $this->updatePrepare('feeds_user', $fieldsValueArray, "user_id=:id", array('id' => Session::get('uid')));
        exit;
    }

    private function getLatestEntriesHtml($id, $count) {
        $feed = $this->select('feeds', 'url', "id='".$id."'");
        $vars['latestEntries'] = $this->getLatestEntries($feed[0]['url'], $count);
        return $this->fetch('itemLoop', $vars);
    }

    private function getLatestEntries($url, $count) {
        require_once('extras/simplepie-1.8.0/autoloader.php');
        require_once('extras/simplepie-1.8.0/idn/idna_convert.class.php');

        // Initialize some feeds for use.
        $feed = new SimplePie();

        $feed->set_feed_url($url);

        // force to make faulty rss feeds work
        $feed->force_feed($url);
        $feed->enable_cache(true);
// When we set these, we need to make sure that the handler_image.php file is also trying to read from the same cache directory that we are.
        // $feed->set_favicon_handler('extras/simplepie-131/handler_image.php');
        //$feed->set_image_handler('extras/simplepie-131/handler_image.php');
        // Initialize the feed.
        $feed->init();

        $feed->handle_content_type();
        //$items = array();
        $items = $feed->get_items(0, $count);
//        foreach ($feed as $item){
//            $item->get_title()
//        }

        return $items;
    }

    private function deleteFeed() {
        $id = filter_input(INPUT_POST, 'id');
        $t = filter_input(INPUT_POST, 'title');
        $this->delete('feeds', 'id=' . $id);
        $this->setSystemMessage($this->ts("deleted $1 successfully.", array('1' => $t)));
    }

    private function updateFeed() {
        $id = filter_input(INPUT_POST, 'id');
        $t = filter_input(INPUT_POST, 'title');
        $u = filter_input(INPUT_POST, 'url');
        $c = filter_input(INPUT_POST, 'category');

        $fieldsValueArray = array(
            'title' => $t,
            'url' => $u,
            'category' => $c
        );
        $this->updatePrepare('feeds', $fieldsValueArray, 'id=:id', array('id' => $id));

        $this->setSystemMessage($this->ts("updated $1 successfully.", array('1' => $t)));
    }

    private function addFeed() {
        //$t = filter_input(INPUT_POST, 'title');
        $u = filter_input(INPUT_POST, 'url');
        $c = filter_input(INPUT_POST, 'category');
        $t = filter_var($this->getTitleCurl($u));

        $fieldsValueArray = array(
            'title' =>  $t ,
            'url' => $u ,
            'category' =>  $c 
        );
        $this->insert('feeds', $fieldsValueArray);
        $this->setSystemMessage($this->ts("inserted $1 successfully.", array('1' => $t)));
    }

    /**
     * TODO: insert caching, improve for feed urls
     * @param string $url
     * @return boolean
     */
    private function getFavicon($url) {
        //$this->checkSiteRights('feeds');

        $urlParsed = parse_url($url);

        // check for standard favicon
        $userPath = $urlParsed['host'];
        
        // DIRTY Fix for kicker ... wtf..
        if($userPath=='photodb.kicker.de'){
            $path = 'https://www.kicker.de/favicon.ico';
        } else {
            $path = "https://" . $userPath . "/favicon.ico";
        }
        
        $header = get_headers($path);
        if (preg_match("|200|", $header[0])) {
            return $this->imageHtml($path, 'ext_fav', '16px'); //$this->imageHtml();//'<img src="'..'" />';
        }


        # make the URL simpler
        $elems = parse_url($url);
        $url = $elems['scheme'] . '://' . $elems['host'];

        # load site
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
        curl_close($ch);

        # look for the shortcut icon inside the loaded page
        $matches = array();
        $regex_pattern = "/rel=\"shortcut icon\" (?:href=[\'\"]([^\'\"]+)[\'\"])?/";
        preg_match_all($regex_pattern, $output, $matches);

        if (isset($matches[1][0])) {
            $favicon = $matches[1][0];

            # check if absolute url or relative path
            $favicon_elems = parse_url($favicon);

            # if relative
            if (!isset($favicon_elems['host']) && substr($favicon, 0, 2) != "//") {
                $favicon = $url . '/' . $favicon;
            }
            return $this->imageHtml($favicon, 'ext_fav', '16px'); //'<img src="'..'" />';
            //return "<img src='".$favicon."' />";
        }

        return false;
    }
    
    private function getTitleCurl($url) {
        //$this->checkSiteRights('profilpage/dash');

        $html = $this->getUrlContent($url);
        if($html == ''){
            return '';
        }
        // get title
        $dom = new DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        $titleTagElement = $dom->documentElement->getElementsByTagName('title');
        $titleRaw = $titleTagElement->item('0')->textContent;
        $title = trim(empty($titleRaw) ? 'Untitled' : $titleRaw);

        // convert title to utf-8 character encoding
        $match = [];
        if ($title != 'Untitled') {
            preg_match('/<charset\=(.+)\"/i', $html, $match);
            if (!empty($match[0])) {
                $charset = str_replace('"', '', $match[0]);
                $charset2 = str_replace("'", '', $charset);
                $charset3 = strtolower(trim($charset2));
                if ($charset3 != 'utf-8') {
                    $title = iconv($charset3, 'utf-8', $title);
                }
            }
        }

        return $title;
    }

    /**
     * TODO: Check now and then if cache is expired...
     * @param string $url
     * @return string
     */
    private function getUrlContent($url) {
        // get html via url
        $savePath = dirname(__FILE__) . "/../../cache/html/" . md5($url) . ".html";
        if (!is_dir(dirname(__FILE__) . "/../../cache/html/")) {
            mkdir(dirname(__FILE__) . "/../../cache/html/", 0755);
        }
        if (is_file($savePath)) {
            //echo "ist schon da!";
            return file_get_contents($savePath);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
//        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 0);
        $html = curl_exec($ch);
        curl_close($ch);
//        fclose($fp);
//        chmod($savePath, 0644);
        file_put_contents($savePath, $html);

        return $html;
    }

}
