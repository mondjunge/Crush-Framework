<?php if ($adminMode): ?>
    <a class="pure-button pure-button-primary floatRight" href="<?php echo $this->action("feeds/admin");?>"><?php echo $this->ts('Manage feeds');?></a>
<?php endif; ?>
<div id="subMenu">
    <a class="pure-button pure-button-active tag"><?php echo $this->ts("all"); ?></a>
    <?php foreach ($category as $c): ?>
        <a class="pure-button tag"><?php echo $c['title']; ?></a>
    <?php endforeach; ?>
</div>
<?php
foreach ($category as $c):
    // show categories 
    ?>
    <div id="<?php echo $c['title']; ?>" class="categoryTile">

        <h2><?php echo $c['title']; ?></h2>
        <a id="toggle<?php echo $c['title']; ?>" href="#" class="toggleCategory" ><?php echo $this->ts("toggle"); ?></a>

        <div class="pure-g pure-g-r category">
            <?php
            foreach ($c['feeds'] as $f):
                // show category feeds 
                ?>
                <div class="pure-u-1-3">

                    <div class="feedTile" data-feedid="<?php echo $f['id']; ?>">
                        <h3 ><a href="<?php $url_info = parse_url($f['url']); echo "https://".$url_info['host'];?>" target="_blank"><?php echo $f['favicon']; ?><?php echo $f['title']; ?></a></h3>
                        <div class="feedBox">
                            <div class="sk-spinner sk-spinner-wandering-cubes">
                                <div class="sk-cube1"></div>
                                <div class="sk-cube2"></div>
                            </div>
                        </div>
                    </div>


                </div>

                <?php
            endforeach;
            // end category feeds
            ?>
        </div>

    </div>
    <?php
endforeach;
// end categories