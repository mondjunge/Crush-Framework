<div>
<form action="<?php echo $this->action('feeds/admin') ?>" method="POST" class="pure-form">
        <fieldset>
            <input id="url" name="url" type="text" value="" autocompleteoff="" placeholder="<?php echo $this->ts("http://something.com/rss.xml") ?>"/>
            <input id="title" name="title" type="text" readonly="readonly" autocompleteoff="" value=""/>
            <input id="category" name="category" type="text" value="" placeholder="<?php echo $this->ts("category") ?>"/>
            
            <button id="addFeed" class="pure-button pure-button-primary pure-button-xsmall" type="submit" name="addFeed" ><?php echo $this->ts("add feed") ?></button>
            
        </fieldset>

    </form>
</div>

<br/>

<div>
<?php foreach ($feeds as $f): ?>
    <form action="<?php echo $this->action('feeds/admin') ?>" method="POST" class="pure-form">
        <fieldset>

            <input id="id_<?php echo $f['id'] ?>" name="id" type="text" readonly="readonly" size="3" value="<?php echo $f['id'] ?>"/>
            <input id="title_<?php echo $f['id'] ?>" name="title" type="text" value="<?php echo $f['title'] ?>"/>
            <input id="url_<?php echo $f['id'] ?>" name="url" type="text" value="<?php echo $f['url'] ?>"/>
            <input id="category_<?php echo $f['id'] ?>" name="category" type="text" value="<?php echo $f['category'] ?>"/>

            <button class="pure-button pure-button-primary pure-button-xsmall" type="submit" name="save" ><?php echo $this->ts("save") ?></button>
            <button class="pure-button pure-button-error pure-button-xsmall" onclick="return utils.check.deleteCheck('<?php echo $this->ts("Do you really want to delete this feed?") ?>');" type="submit" name="delete" ><?php echo $this->ts("X") ?></button>
        </fieldset>
    </form>
<?php endforeach; ?>

</div>