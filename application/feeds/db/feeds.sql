CREATE TABLE IF NOT EXISTS `{dbprefix}feeds` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `url` text NOT NULL,
  `category` text NOT NULL,
  PRIMARY KEY (`id`)
);