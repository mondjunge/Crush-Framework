/**
 * feedsFetch
 */

use_package('modules');
modules.feedsFetch = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    this.init = function () {

        handleToggle();
        handleTags();
        handleFeeds();
        
    };

    var handleTags = function () {
        $('.tag').on("click",function () {
            var tag = $(this);
            tag.siblings(".tag").removeClass('pure-button-active');
            tag.addClass('pure-button-active');
            var catTitle = tag.html();
            $('.categoryTile').each(function(){
                var catTile = $(this);
                if(catTitle === catTile.attr('id') || catTitle==='all' || catTitle==='Alle'){
                     catTile.show();
                }else{
                    catTile.hide();
                }
            });
        });
    };
    
    var handleFeeds = function () {
        $(".feedTile").each(function () {
            var feedTile = $(this);
            var id = feedTile.attr('data-feedid');
            //console.log(reLangUrl);
            fetchFeed(feedTile, id);
        });
    };

    var handleToggle = function () {
        $(".toggleFeedBox").on("click",function () {
            $(this).siblings(".feedBox").toggle();
            return false;
        });
        $(".toggleCategory").on("click",function () {
            $(this).siblings(".category").toggle();
            return false;
        });
    };

    var fetchFeed = function (feedTile, id) {
        $.ajax({
            url: reLangUrl + "feeds/fetchFeed",
            type: 'POST',
            async: true,
            timeout: 20000,
            data: {
                fetchFeed: id
            },
            success: function (html) {
                feedTile.find(".feedBox").html(html);
            },
            error: function (e) {
                console.error(e);
                feedTile.find(".feedBox").html("Ouch... please <a id='reload" + id + "' class='reload' href='#'>reload</a>.").ready(function () {
                    handleReload(feedTile, id);
                });
            }

        });
    };

    var handleReload = function (feedTile, id) {
        //var feedTile = $(".feedTile[data-feedid='"+id+"']");
        $("#reload" + id).on("click",function () {
            fetchFeed(feedTile, id);
            return false;
        });

    };

    this.ajaxRefresh = function () {

    };


//    
//    this.ajaxStart = function(){
//        
//    }


};