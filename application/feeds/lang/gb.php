<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Insufficient rights!' =>
	'Insufficient rights!'
	,
	'save'=>
	'save'
	,
	'X'=>
	'X'
	,
	'Do you really want to delete this feed?'=>
	'Do you really want to delete this feed?'
	,
	'updated $1 successfully.'=>
	'updated $1 successfully.'
	,
	'deleted $1 successfully.'=>
	'deleted $1 successfully.'
	,
	'add feed'=>
	'add feed'
	,
	'http://something.com/rss.xml'=>
	'http://something.com/rss.xml'
	,
	'category'=>
	'category'
	,
	'inserted $1 successfully.'=>
	'inserted $1 successfully.'
	,
	'Error fetching feed...'=>
	'Error fetching feed...'
	,
	'Error fetching feed...1'=>
	'Error fetching feed...1'
	,
	'Error fetching feed...7'=>
	'Error fetching feed...7'
	,
	'Error fetching feed...11'=>
	'Error fetching feed...11'
	,
	'Error fetching feed...28'=>
	'Error fetching feed...28'
	,
	'Error fetching feed...37'=>
	'Error fetching feed...37'
	,
	'Error fetching feed...22'=>
	'Error fetching feed...22'
	,
	'Error fetching feed...2'=>
	'Error fetching feed...2'
	,
	'Error fetching feed...5'=>
	'Error fetching feed...5'
	,
	'Error fetching feed...4'=>
	'Error fetching feed...4'
	,
	'Error fetching feed...8'=>
	'Error fetching feed...8'
	,
	'Error fetching feed...19'=>
	'Error fetching feed...19'
	,
	'Error fetching feed...18'=>
	'Error fetching feed...18'
	,
	'Error fetching feed...14'=>
	'Error fetching feed...14'
	,
	'Error fetching feed...15'=>
	'Error fetching feed...15'
	,
	'Error fetching feed...16'=>
	'Error fetching feed...16'
	,
	'Error fetching feed...32'=>
	'Error fetching feed...32'
	,
	'Error fetching feed...36'=>
	'Error fetching feed...36'
	,
	'Error fetching feed...13'=>
	'Error fetching feed...13'
	,
	'Error fetching feed...10'=>
	'Error fetching feed...10'
	,
	'Error fetching feed...27'=>
	'Error fetching feed...27'
	,
	'Error fetching feed...24'=>
	'Error fetching feed...24'
	,
	'Error fetching feed...29'=>
	'Error fetching feed...29'
	,
	'Error fetching feed...34'=>
	'Error fetching feed...34'
	,
	'Error fetching feed...31'=>
	'Error fetching feed...31'
	,
	'Error fetching feed...35'=>
	'Error fetching feed...35'
	,
	'Error fetching feed...30'=>
	'Error fetching feed...30'
	,
	'Error fetching feed...33'=>
	'Error fetching feed...33'
	,
	'toggle'=>
	'toggle'
	,
	'all'=>
	'all'
	,
	'Manage feeds'=>
	'Manage feeds'
);