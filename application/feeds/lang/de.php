<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Insufficient rights!' =>
	'Keine Rechte'
	,
	'save'=>
	'speichern'
	,
	'X'=>
	'X'
	,
	'Do you really want to delete this feed?'=>
	'Soll der Feed wirklich gelöscht werden?'
	,
	'updated $1 successfully.'=>
	'$1 aktualisiert'
	,
	'deleted $1 successfully.'=>
	'$1 gelöscht'
	,
	'add feed'=>
	'hinzufügen'
	,
	'http://something.com/rss.xml'=>
	'http://something.com/rss.xml'
	,
	'category'=>
	'Kategorie'
	,
	'inserted $1 successfully.'=>
	'$1 hinzugefügt'
	,
	'Error fetching feed... id: $1'=>
	'Konnte $1 nicht laden...'
	,
	'toggle'=>
	'aus-/einblenden'
	,
	'all'=>
	'Alle'
	,
	'Manage feeds'=>
	'Verwalte Feeds'
);