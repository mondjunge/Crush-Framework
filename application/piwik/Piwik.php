<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Piwik extends AppController {

    private $modConf;

    public function __construct() {
        //echo "Piwik Mod: " . $this->getActiveModule();
        $this->modConf = $this->loadModuleConfiguration();
        //print_r($this->modConf);
    }

    public function index() {
        $this->autoCheckRights();
        
        if(!is_file(dirname(__FILE__) . "/../../extras/piwik/index.php")){
            $this->installPiwik();
        }
        
        
        if (filter_has_var(INPUT_GET, 'seo')) {
            $tpl = 'seo';
        } elseif (filter_has_var(INPUT_GET, 'sites')) {
            $tpl = 'sites';
        } elseif (filter_has_var(INPUT_GET, 'users')) {
            $tpl = 'users';
        } else {
            $tpl = 'dash';
        }
        if (filter_has_var(INPUT_GET, 'period')) {
            $data['period'] = filter_input(INPUT_GET, 'period');
        } else {
            $data['period'] = 'month';
        }
        $data['tpl'] = $tpl;

        //echo $this->modConf['apiToken']; //exit;


        $data['relativeUrl'] = Config::get('relativeUrl');
        $data['apiToken'] = $this->modConf['apiToken'];
        $this->view('menu', $data);
        $this->view($tpl, $data);
    }

    private function installPiwik() {
        try {
            $this->downloadPiwik();
            $this->extractPiwik();
            return;
        } catch (Exception $ex) {
            $this->setErrorMessage($ex->getMessage());
        }
    }

    private function extractPiwik() {
        $savePath = dirname(__FILE__) . "/../../cache/install/";
        $targetPath = dirname(__FILE__) . "/../../extras/";
        $fileCachePath = $savePath . "piwik.zip";
        
        if (is_file($fileCachePath)) {
            $zip = new ZipArchive;
            $res = $zip->open($fileCachePath);
            if ($res === TRUE) {
                $zip->extractTo($targetPath);
                $zip->close();
                unlink($fileCachePath);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function downloadPiwik() {
        $savePath = dirname(__FILE__) . "/../../cache/install/";
        if (!is_dir($savePath)) {
            mkdir($savePath, 0777, true);
        }
        $fileCachePath = $savePath . "piwik.zip";

        if (is_file($fileCachePath)) {
            unlink($fileCachePath);
        }

        $url = "https://builds.matomo.org/piwik.zip";

        $ch = curl_init($url);
        $fp = fopen($fileCachePath, 'x');

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);

        fclose($fp);
        if (is_readable($fileCachePath)) {
            return true;
        } else {
            throw new Exception($this->ts("Cannot download latest matomo(piwik)."));
        }
    }

}
