<h2><?php echo $this->ts("Visitors") ?></h2>
<div class="widgetIframe">
    <iframe width="100%" height="260" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>

<h2><?php echo $this->ts("Visitor behavior") ?></h2>
<div style='float:right;width:50%;'>
    <div class="widgetIframe">
        <iframe width="100%" height="400" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
    </div>
</div>
<div class="widgetIframe" style="width:50%">
    <iframe width="100%" height="400" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitFrequency&actionToWidgetize=getSparklines&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>


