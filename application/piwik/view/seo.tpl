<h2><?php echo $this->ts("SEO") ?></h2>

<div class="widgetIframe">
    <iframe width="100%" height="220" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getRefererType&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>

<div style='float:right;width:50%'>
    <div class="widgetIframe">
        <iframe width="100%" height="450" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getKeywords&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
    </div>
</div>

<div class="widgetIframe" style="width:50%">
    <iframe width="100%" height="450" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Referers&actionToWidgetize=getSearchEngines&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>