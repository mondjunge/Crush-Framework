<h2><?php echo $this->ts("Visitor origin") ?></h2>
<div style='float:right;width:50%'>
    <div id="widgetIframe">
        <iframe width="100%" height="350" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserCountry&actionToWidgetize=getCountry&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
    </div>
</div>

<div id="widgetIframe" style="width:50%">
    <iframe width="100%" height="350" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserCountry&actionToWidgetize=getContinent&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>


<h2><?php echo $this->ts("Visitor settings") ?></h2>
<div style='float:right;width:50%'>

    <div id="widgetIframe">
        <iframe width="100%" height="350" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserSettings&actionToWidgetize=getResolution&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
    </div>
</div>
<div id="widgetIframe" style="width:50%">
    <iframe width="100%" height="350" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserSettings&actionToWidgetize=getBrowser&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>

<div style='float:right;width:50%'>
    <div id="widgetIframe">
        <iframe width="100%" height="350" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserSettings&actionToWidgetize=getConfiguration&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
    </div>

</div>

<div id="widgetIframe" style="width:50%">
    <iframe width="100%" height="350" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=UserSettings&actionToWidgetize=getOS&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>