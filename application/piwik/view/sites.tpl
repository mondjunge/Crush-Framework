<h2><?php echo $this->ts("Pagecount") ?></h2>
<div id="widgetIframe">
    <iframe width="100%" height="200" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=VisitorInterest&actionToWidgetize=getNumberOfVisitsPerPage&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>

<h2><?php echo $this->ts("Requested sites") ?></h2>
<div id="widgetIframe">
    <iframe width="100%" height="500" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getPageUrls&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>

<h2><?php echo $this->ts("Incoming") ?></h2>
<div id="widgetIframe">
    <iframe width="100%" height="500" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getEntryPageUrls&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>

<h2><?php echo $this->ts("Outgoing") ?></h2>
<div id="widgetIframe">
    <iframe width="100%" height="350" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getOutlinks&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>

<h2><?php echo $this->ts("Downloads") ?></h2>
<div id="widgetIframe">
    <iframe width="100%" height="350" src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getDownloads&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&disableLink=1&token_auth=<?php echo $apiToken ?>" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>