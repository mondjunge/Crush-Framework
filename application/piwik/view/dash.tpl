<h2><?php echo $this->ts("Dashboard") ?></h2>

<div class="widgetIframe">
    <iframe src="<?php echo $relativeUrl ?>extras/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite=1&period=<?php echo $period ?>&date=<?php echo date('Y-m-d') ?>&token_auth=<?php echo $apiToken ?>" frameborder="0" marginheight="0" marginwidth="0" width="100%" height="500px"></iframe>
</div>