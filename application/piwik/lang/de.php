<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Visitors' =>
	'Besucher'
	,
	'Visitor behavior'=>
	'Besucher Verhalten'
	,
	'Visitor origin'=>
	'Besucher Herkunft'
	,
	'Visitor settings'=>
	'Besucher Einstellungen'
	,
	'SEO'=>
	'Suchmaschinen'
	,
	'main'=>
	'Start'
	,
	'seo'=>
	'Seo'
	,
	'users'=>
	'Besucher'
	,
	'sites'=>
	'Seiten'
	,
	'Incoming'=>
	'Einkommend'
	,
	'Requested sites'=>
	'Angefragte Seiten'
	,
	'Outgoing'=>
	'Ausgehend'
	,
	'Downloads'=>
	'Downloads'
	,
	'Pagecount'=>
	'Anzahl der Seiten'
	,
	'year'=>
	'Jahr'
	,
	'month'=>
	'Monat'
	,
	'week'=>
	'Woche'
	,
	'day'=>
	'Tag'
	,
	'Insufficient rights!'=>
	'Unzureichende Rechte!'
	,
	'apiToken_helpText'=>
	'Hier den Piwik API Token eintragen um Statistiken aus Piwik ohne vorherige Anmeldung im Plugin sehen zu können.'
	,
	'Dashboard'=>
	'Übersicht'
);