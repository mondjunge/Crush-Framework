<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Visitors' =>
	'Visitors'
	,
	'Visitor behavior'=>
	'Visitor behavior'
	,
	'Visitor origin'=>
	'Visitor origin'
	,
	'Visitor settings'=>
	'Visitor settings'
	,
	'SEO'=>
	'SEO'
	,
	'main'=>
	'main'
	,
	'seo'=>
	'seo'
	,
	'users'=>
	'users'
	,
	'sites'=>
	'sites'
	,
	'Incoming'=>
	'Incoming'
	,
	'Requested sites'=>
	'Requested sites'
	,
	'Outgoing'=>
	'Outgoing'
	,
	'Downloads'=>
	'Downloads'
	,
	'Pagecount'=>
	'Pagecount'
	,
	'year'=>
	'year'
	,
	'month'=>
	'month'
	,
	'week'=>
	'week'
	,
	'day'=>
	'day'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'apiToken_helpText'=>
	'Enter Piwik API Token to access statistics data without login into Piwik every time.'
	,
	'Dashboard'=>
	'Dashboard'
);