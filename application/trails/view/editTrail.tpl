
<form method="POST" class="pure-form pure-form-stacked" action="<?php echo $this->action('trails'); ?>">
    <a class="pure-button pure-button-small" href="<?php echo $this->action('trails?c='.$t['category_id']); ?>"><?php echo $this->ts('back'); ?></a>

    <input name="updateTrail" type="submit" class="pure-button pure-button-primary pure-button-small" value="<?php echo $this->ts('update'); ?>"/>

<!--    <select>
        
    </select>-->
    <input type="hidden" name="catId" value="<?php echo $t['category_id']; ?>"/>
    <input type="hidden" name="id" value="<?php echo $t['id']; ?>"/>
    
    <input type="text" size="50" maxlength="255" name="title" placeholder="<?php echo $this->ts('Name of trail'); ?>" value="<?php echo $t['title']; ?>"/>
    <span class="help"><?php echo $this->ts('Choose color for trail heading:'); ?></span>
    <input type="color" name="color" placeholder="<?php echo $this->ts('blue, red, black or Hexvalue'); ?>" value="<?php echo $t['color'] ?>" />
    <input type="text" size="50" maxlength="255" name="tags" placeholder="<?php echo $this->ts('tag1,tag2,tag3'); ?>" value="<?php echo $t['tags'] ?>"/>
    <span class="help"><?php echo $this->ts('format tags comma seperated!'); ?></span>
    <div class="pure-controls">
        <span class=""><?php echo $this->ts("Tags in use") ?>:</span>
        <?php foreach ($tags as $tag) : ?>
            <a class="pure-button pure-button-xsmall pure-button-tag editTag" href="#" onclick="return false;"><?php echo $tag ?></a>
        <?php endforeach; ?>
    </div>
    
    <textarea name="text"><?php echo $t['description']; ?></textarea>
    <input name="updateTrail" type="submit" class="pure-button pure-button-primary pure-button-small" value="<?php echo $this->ts('update'); ?>"/>
</form>