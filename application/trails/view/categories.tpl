
<div class="pure-g pure-g-r">
    <?php if ($adminMode) : ?>

        <div class="pure-u-1-4">
            <div class="categoryTile">
                <form method="POST" class="pure-form pure-form-stacked">
                    <legend><?php echo $this->ts('+ add category'); ?></legend>
                    <input class="pure-input-1" type="text" name="title" placeholder="<?php echo $this->ts('title'); ?>"/>
                    <input class="pure-input-1 fileSelect" data-type="image" type="text" name="image" placeholder="<?php echo $this->ts('image'); ?>" autocomplete="off"/>
                    <input name="addCategory" type="submit" class="pure-button pure-button-primary pure-button-small" value="<?php echo $this->ts('add'); ?>"/>
                </form>
            </div>
        </div>
    <?php endif; ?>

    <?php foreach ($categories as $c) : ?> 
        <div class="pure-u-1-4">


            <div class="categoryTile">
                
                <a class="categoryLink" href="<?php echo $this->action('trails?c=' . $c['title']); ?>">
                    <span class="categoryName"><?php echo $c['title']; ?></span>
                    <span class="trailsCount"><?php echo $c['trailsCount']; echo " ".$this->ts('trails')?></span>
                    <?php echo $this->image($c['image'], 'categoryImage', "768", "768", $c['title'].'_image'); ?>
                </a>
                <?php if ($adminMode) : ?>
                    <div class="editButtons">
                        <a class="pure-button pure-button-small editButton" href="<?php echo $this->action('trails?editC=' . $c['id']); ?>">✎</a>
                        <a class="pure-button pure-button-error pure-button-small" onclick="return utils.check.deleteCheck('<?php echo $this->ts('Do you really want to delete this category? All trails in it will be lost.'); ?>');" href="<?php echo $this->action('trails?deleteC=' . $c['id']); ?>">X</a>
                    </div>
                <?php endif; ?>
            </div>

        </div>

    <?php endforeach; ?>
</div>