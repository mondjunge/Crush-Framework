<a class="pure-button pure-button-small" href="<?php echo $this->action('trails?c=' . $categoryId); ?>"><?php echo $this->ts('back'); ?></a>

<form method="POST" class="pure-form pure-form-stacked" action="<?php echo $this->action('trails'); ?>">

    <input type="hidden" name="catId" value="<?php echo $categoryId; ?>"/>
    <input type="text" size="50" maxlength="255" name="title" placeholder="<?php echo $this->ts('Name of trail'); ?>"/>
    <span class="help"><?php echo $this->ts('Choose color for trail heading:'); ?></span>
    <input type="color" name="color" placeholder="<?php echo $this->ts('blue, red, black or Hexvalue'); ?>" value="<?php echo $t['color'] ?>" /> 
    <input type="text" size="50" maxlength="255" name="tags" placeholder="<?php echo $this->ts('tag1,tag2,tag3'); ?>"/>
    <span class="help"><?php echo $this->ts('format tags comma seperated!'); ?></span>
    <div class="pure-controls">
        <span class=""><?php echo $this->ts("Tags in use") ?>:</span>
        <?php foreach ($tags as $tag) : ?>
            <a class="pure-button pure-button-xsmall pure-button-tag editTag" href="#" onclick="return false;"><?php echo $tag ?></a>
        <?php endforeach; ?>
    </div>

    <textarea name="text"></textarea>
    <input name="insertTrail" type="submit" class="pure-button pure-button-primary pure-button-small" value="<?php echo $this->ts('save'); ?>"/>
</form>