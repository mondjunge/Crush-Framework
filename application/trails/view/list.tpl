<a class="pure-button pure-button-small" href="<?php echo $this->action('trails'); ?>"><?php echo $this->ts('back'); ?></a>
<?php if ($adminMode) : ?>
    <a class="pure-button pure-button-primary pure-button-small" href="<?php echo $this->action('trails?catId=' . $categoryId . '&addTrail'); ?>"><?php echo $this->ts('+ add trail'); ?></a>
<?php endif; ?>


<h1><?php echo $this->image($categoryImage, 'categoryImage', "32", "32", $category . '_image'); ?> <?php echo $category; ?></h1>

<a id="toogleLegend" class="pure-button pure-button-small" onclick="return false;" href="<?php echo $this->action('trails?catId=' . $categoryId); ?>"><?php echo $this->ts('hide/show legend'); ?></a>


<?php if ($adminMode) : ?>
    <div class="floatRight">
        <a class="pure-button pure-button-primary pure-button-small editLegendButton" onclick="return false;" href="<?php echo $this->action('trails?catId=' . $categoryId . '&editLegend'); ?>"><?php echo $this->ts('✎ edit legend'); ?></a>
    </div>
<?php endif; ?>
<div id="legend">
    <?php echo $categoryLegend; ?>
</div>

<?php if ($adminMode) : ?>
    <form id="legendEditForm" class="pure-form pure-form-stacked">
        <input name="updateLegend" onclick="return false;" type="submit" class="pure-button pure-button-primary pure-button-small updateLegendButton" value="<?php echo $this->ts('update'); ?>"/>

        <textarea name="text"><?php echo $categoryLegend; ?></textarea>
        <input name="categoryId" type="hidden" value="<?php echo $categoryId; ?>" />
        <input name="updateLegend" onclick="return false;" type="submit" class="pure-button pure-button-primary pure-button-small updateLegendButton" value="<?php echo $this->ts('update'); ?>"/>
    </form>
<?php endif; ?>
<a name="tags" />
<div class="tags">
    <?php /* $tags[]="Alle"; */ foreach ($tags as $tag) : ?> 
        <?php if ((isset($_GET['tag']) && urldecode($_GET['tag']) == $tag) || (!isset($_GET['tag']) && $tag == "Alle")): ?>
            <a class="pure-button pure-button-primary pure-button-xsmall tags_selected" href="<?php echo $this->action('trails?c=' . $categoryId . '&tag=' . urlencode($tag) . '#tags') ?>" ><?php echo $tag ?></a>
        <?php else: ?>
            <a class="pure-button pure-button-xsmall tags" href="<?php echo $this->action('trails?c=' . $categoryId . '&tag=' . urlencode($tag) . '#tags') ?>"><?php echo $tag ?></a>
        <?php endif; ?>
    <?php endforeach; ?>
</div>

<?php foreach ($trails as $t) : ?> 

    <div class="trail">

        <h2 class="trailName" style="color: #eee;background-color:<?php echo $t['color']; ?>;"><?php echo $t['title']; ?></h2>
        <?php if ($adminMode) : ?>
            <div class="editButtons">
                <a class="pure-button pure-button-small editButton" href="<?php echo $this->action('trails?editT=' . $t['id']); ?>">✎</a>
                <a class="pure-button pure-button-error pure-button-small" onclick="return utils.check.deleteCheck('<?php echo $this->ts('Do you really want to delete this trail?'); ?>');" href="<?php echo $this->action('trails?deleteT=' . $t['id']); ?>">✘</a>
            </div>
        <?php endif; ?>
        <?php foreach ($t['tags'] as $tt) : ?> 
            <a class="pure-button pure-button-primary pure-button-xsmall tags" href="<?php echo $this->action('trails?c=' . $categoryId . '&tag=' . urlencode($tt) . '#tags') ?>"><?php echo $tt ?></a>
        <?php endforeach; ?>
        <div class="trailDescription">
            <?php echo $t['description']; ?>
        </div>
    </div>

<?php endforeach; ?>