<form method="POST" class="pure-form pure-form-stacked" action="<?php echo $this->action('trails'); ?>">
    <legend><?php echo $this->ts('edit category'); ?></legend>
    <input type="hidden" name="id" value="<?php echo $c['id']; ?>"/>
    <input type="text" name="title" placeholder="<?php echo $this->ts('title'); ?>" value="<?php echo $c['title']; ?>"/>
    <input class="fileSelect" data-type="image" type="text" name="image" placeholder="<?php echo $this->ts('image'); ?>" value="<?php echo $c['image']; ?>" />
    <input name="updateCategory" type="submit" class="pure-button pure-button-primary pure-button-small" value="<?php echo $this->ts('update'); ?>"/>
</form>