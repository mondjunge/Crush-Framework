/**
 * trails
 */

use_package('modules');
modules.trails = new function(){
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);
    
    this.init = function(){
        modules.trails.legendHandler();
        modules.trails.tagHandler();
    }
    
    this.legendHandler = function(){
        $('.editLegendButton').on("click",function(){
            if($('#legend').hasClass('hidden')){
                
            }else{
                $('#legend').addClass('hidden');
            }
            if($('#legendEditForm').hasClass('show')){
                $('#legendEditForm').removeClass('show');
                $('#legend').removeClass('hidden');
            }else{
                $('#legendEditForm').addClass('show');
                 
            }
            $('#legendEditForm').toggle('fast');
        });
        $('#toogleLegend').on("click",function(){
            if($('#legend').hasClass('hidden')){
                $('#legend').removeClass('hidden');
            }else{
                $('#legend').addClass('hidden');
            }
            
        //$('#legendEditForm').toggle('fast');
        });
        
        $('.updateLegendButton').on("click",function(){
            
            $("textarea[name='text']").each(function () {
                /**
                 * make textareas with CKeditor work for AJAX...
                 * Update textarea with Data from CKEeditor 
                 */
                var $textarea = $(this);
                $textarea.val(CKEDITOR.instances[$textarea.attr('name')].getData());
                console.log($textarea.val());
                
                var text = $textarea.val();
                var categoryId = $("input[name='categoryId']").val();
                
                $.ajax({
                    type: "POST",
                    url: reLangUrl+"trails",
                    async: true,
                    timeout: 5000,
                    data: {
                        updateLegend: categoryId,
                        text: text
                    },
                    success: function(legend){
                        $('.editLegendButton').trigger("click");
                        $('#legend').html(legend);
                    //handleDeleteComment();
                    },
                    error: function(){
                        alert('something went wrong... Check your internet connection...');
                    }
                });
            });
           
            
            return false;
        });
        
    }
    this.tagHandler = function(){
        $(".editTag").on("click",function(){
            var tag = $(this).text();
            console.log('tag clicked: '+tag);
            var newvalue = "";
            var oldvalue = $("input[name='tags']").val();
            var ova = oldvalue.split(",");
            if(valueInArray(tag,ova)){
                for(var i=0; i<ova.length;i++){
                    if(ova[i]!=tag){
                        if(newvalue=="")
                            newvalue=ova[i];
                        else
                            newvalue+=","+ova[i];
                    }
               
                }
            }else{
                if(oldvalue=="")
                    newvalue = tag;
                else
                    newvalue = oldvalue + "," + tag;
            }
            $("input[name='tags']").val(newvalue);
        });
        
    }
    
//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }
    
    
}

/*
 * helper function
 */
function valueInArray(value,array){
    var inArray = false;
    for(var i=0; i<array.length;i++){
        if(array[i]==value)
            inArray=true;
    }
    return inArray;
}