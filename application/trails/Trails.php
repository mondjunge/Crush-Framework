<?php

/**
 *  Copyright © tim 19.03.2014
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Trails extends AppController {

    public function index() {
        //put your code here
        $this->autoCheckRights();

        $data['adminMode'] = $this->checkSiteRights('trails/admin');
        if ($data['adminMode']) {
            $this->includeJs('application/trails/js/.trailsAdmin.js');
            $this->includeJs(Config::get('applicationDirectory').'/files/js/.files.js');
        }

        if (filter_has_var(INPUT_GET,'c')) {
            if ($data['adminMode']) {
                $this->includeCkeditor('javascript/ck_config/ck_trails.js');
            }
            $this->viewList(filter_input(INPUT_GET, 'c'), $data);
            return;
        }
        if ($data['adminMode']) {
            if (filter_has_var(INPUT_GET,'editC')) {
                $this->editCategory();
                return;
            }
            if (filter_has_var(INPUT_GET,'editT')) {
                $this->editTrail();
                return;
            }
            if (filter_has_var(INPUT_GET,'addTrail')) {
                $this->addTrail();
                return;
            }
            if (filter_has_var(INPUT_POST,'insertTrail')) {
                $this->insertTrail();
                $this->jumpTo('trails?c=' . filter_input(INPUT_POST, 'catId'));
            }
            if (filter_has_var(INPUT_POST,'updateTrail')) {
                $this->updateTrail();
                $this->jumpTo('trails?c=' . filter_input(INPUT_POST, 'catId'));
            }

            if (filter_has_var(INPUT_POST,'updateLegend')) {
                $this->standalone = true;
                echo $this->updateLegend();
                return;
                //$this->jumpTo('trails');
            }
            if (filter_has_var(INPUT_POST,'updateCategory')) {
                $this->updateCategory();
                $this->jumpTo('trails');
            }
            if (filter_has_var(INPUT_POST,'addCategory')) {
                $this->addCategory();
                $this->jumpTo('trails');
            }
            if (filter_has_var(INPUT_GET,'deleteC')) {
                $this->deleteCategory();
                $this->jumpTo('trails');
            }
            if (filter_has_var(INPUT_GET,'deleteT')) {
                $catId = $this->deleteTrail();
                $this->jumpTo('trails?c=' . $catId);
            }
        }


        $this->main($data['adminMode']);
    }

    public function admin() {
        //put your administrative code here
        $this->autoCheckRights();
    }

    private function main($adminMode) {
        $data['adminMode'] = $adminMode;

        $data['categories'] = $this->getCategories();

        $n = 0;
        foreach ($data['categories'] as $c) {
            $catId = $c['id'];
            $count = $this->count('trails', "category_id='$catId'");
            $data['categories'][$n]['trailsCount'] = $count;
            ++$n;
        }

        $this->view('categories', $data);
    }

    private function viewList($category, $data) {

        if (!is_numeric($category)) {
            $data['category'] = $category;
            $rs = $this->select('trails_category', '*', "title='$category'");
            $data['categoryId'] = $rs[0]['id'];
        } else {
            $data['categoryId'] = $category;
            $rs = $this->select('trails_category', '*', "id='$category'");
            $data['category'] = $rs[0]['title'];
        }

        $data['categoryImage'] = $rs[0]['image'];
        $data['categoryLegend'] = $rs[0]['legend'];

        $tag = "Alle";
        if (filter_has_var(INPUT_GET,'tag') && filter_input(INPUT_GET, 'tag') != $tag) {
            $tag = filter_input(INPUT_GET, 'tag');
            $data['trails'] = $this->getTrails($data['categoryId'], $tag);
        } else {
            $data['trails'] = $this->getTrails($data['categoryId']);
        }
        $data['selected_tag'] = $tag;

        $data['tags'][] = "Alle";
        $data['tags'] = array_merge($data['tags'], $this->getTags($data['categoryId']));

        $this->view('list', $data);
    }

    private function getTrails($catId, $tag = '') {

        if ($tag == '') {
            $rsTrails = $this->select('trails', '*', "category_id='$catId'", 'title ASC');
        } else {
            $rsTrails = $this->select('trails', '*', "category_id='$catId' AND (tags LIKE '%,$tag%' OR tags LIKE '%$tag,%' OR tags='$tag')", 'title ASC');
        }
        $return = array();
        foreach ($rsTrails as $t) {
            if($t['tags']==''){
                $t['tags'] = array();
            }else{
                $t['tags'] = explode(',', $t['tags']);
            }
            
            $return[] = $t;
        }

        return $return;
    }

    private function addTrail() {
        $this->includeCkeditor('javascript/ck_config/ck_trails.js');
        $data['categoryId'] = filter_input(INPUT_GET, 'catId');
        $data['categories'] = $this->getCategories();

        $data['tags'] = array(); //"Alle";
        $data['tags'] = array_merge($data['tags'], $this->getTags());

        $this->view('addTrail', $data);
    }

    private function insertTrail() {
        
        $fieldsValueArray['user_id'] = Session::get('uid');

        $catId = filter_input(INPUT_POST, 'catId');
        $title = filter_input(INPUT_POST, 'title');
        $tags = filter_input(INPUT_POST, 'tags');
        $description = filter_input(INPUT_POST, 'text');
        $color = filter_input(INPUT_POST, 'color');

        $fieldsValueArray['color'] = "'$color'";
        $fieldsValueArray['title'] = "'$title'";
        $fieldsValueArray['tags'] = "'$tags'";
        $fieldsValueArray['description'] = "'$description'";
        $fieldsValueArray['category_id'] = "'$catId'";

        $this->insert('trails', $fieldsValueArray);
    }

    private function editTrail() {
        $this->includeCkeditor('javascript/ck_config/ck_trails.js');

        $id = filter_input(INPUT_GET, 'editT');
        $rs = $this->select('trails', '*', "id='$id'");
        $data['t'] = $rs[0];

        $data['tags'] = array(); //"Alle";
        $data['tags'] = array_merge($data['tags'], $this->getTags());

        $this->view('editTrail', $data);
    }

    private function updateTrail() {
        
        $fieldsValueArray['user_id'] = Session::get('uid');

        $id = filter_input(INPUT_POST, 'id');
        $catId = filter_input(INPUT_POST, 'catId');
        $title = filter_input(INPUT_POST, 'title');
        $tags = filter_input(INPUT_POST, 'tags');
        $color = filter_input(INPUT_POST, 'color');

        $description = filter_input(INPUT_POST, 'text');

        $fieldsValueArray['title'] = "$title";
        $fieldsValueArray['tags'] = "$tags";
        $fieldsValueArray['color'] = "$color";
        $fieldsValueArray['description'] = "$description";
        $fieldsValueArray['category_id'] = "$catId";

        $this->update('trails', $fieldsValueArray, "id='$id'");
    }

    private function deleteTrail() {
        $id = filter_input(INPUT_GET, 'deleteT');
        $trail = $this->select('trails', 'category_id', "id='$id'");
        $this->delete('trails', "id='$id'");
        return $trail[0]['category_id'];
    }

    private function editCategory() {
        $id = filter_input(INPUT_GET, 'editC');
        $rs = $this->select('trails_category', '*', "id='$id'");
        $data['c'] = $rs[0];

        $this->view('editCategory', $data);
    }

    private function updateCategory() {
        
        $id = filter_input(INPUT_POST, 'id');
        $title = filter_input(INPUT_POST, 'title');
        $image = filter_input(INPUT_POST, 'image');

        $fieldsValueArray['title'] = "$title";
        $fieldsValueArray['image'] = "$image";
        $fieldsValueArray['user_id'] = Session::get('uid');
        $this->update('trails_category', $fieldsValueArray, "id='$id'");
    }

    private function updateLegend() {
        
        $id = filter_input(INPUT_POST, 'updateLegend');
        $legend = filter_input(INPUT_POST, 'text');
        //$image = filter_input(INPUT_POST, 'image');

        $fieldsValueArray['legend'] = "$legend";
        $fieldsValueArray['user_id'] = Session::get('uid');
        $this->update('trails_category', $fieldsValueArray, "id='$id'");
        return $legend;
    }

    private function deleteCategory() {
        //
        $id = filter_input(INPUT_GET, 'deleteC');
        $this->delete('trails_category', "id='$id'");
    }

    private function getCategories() {
        $rs = $this->select('trails_category', '*', '1=1', 'title ASC');

        return $rs;
    }

    private function addCategory() {
        
        $title = filter_input(INPUT_POST, 'title');
        $image = filter_input(INPUT_POST, 'image');
        $fieldsValueArray['title'] = "'$title'";
        $fieldsValueArray['image'] = "'$image'";
        $fieldsValueArray['user_id'] = Session::get('uid');
        $this->insert('trails_category', $fieldsValueArray);
        return;
    }

    private function getTags($catId = '') {
        $tags = array();
        if ($catId == '') {
            $rs = $this->select('trails', 'tags', '', 'time asc');
        } else {
            $rs = $this->select('trails', 'tags', "category_id='$catId'", 'time asc');
        }


        if (is_array($rs)) {
            foreach ($rs as $trail) {


                $a = explode(',', $trail['tags']);
                // print_r($a);
                foreach ($a as $tag) {
                    // print_r($tag);
                    if ($tag != '')
                        $tags[] = $tag;
                }
            }
            $tags = array_unique($tags);
            array_multisort($tags, SORT_STRING);
        }
        // print_r($tags);
        return $tags;
    }

}
