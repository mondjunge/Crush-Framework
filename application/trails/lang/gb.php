<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'add category' =>
	'add category'
	,
	'+ add category'=>
	'+ add category'
	,
	'title'=>
	'title'
	,
	'image'=>
	'image'
	,
	'add'=>
	'add'
	,
	'back'=>
	'back'
	,
	'Do you really want to delete this category? All trails in it will be lost.'=>
	'Do you really want to delete this category? All trails in it will be lost.'
	,
	'update'=>
	'update'
	,
	'edit category'=>
	'edit category'
	,
	'+ add trail'=>
	'+ add trail'
	,
	'Do you really want to delete this trail?'=>
	'Do you really want to delete this trail?'
	,
	'Name of trail'=>
	'Name of trail'
	,
	'save'=>
	'save'
	,
	'trails'=>
	'trails'
	,
	'blue'=>
	'blue'
	,
	'red'=>
	'red'
	,
	'black'=>
	'black'
	,
	'white'=>
	'white'
	,
	'edit legend'=>
	'edit legend'
	,
	'✎ edit legend'=>
	'✎ edit legend'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'hide/show legend'=>
	'hide/show legend'
	,
	'Tags'=>
	'Tags'
	,
	'Tags in use'=>
	'Tags in use'
	,
	'tag1,tag2,tag3'=>
	'tag1,tag2,tag3'
	,
	'format tags comma seperated!'=>
	'format tags comma seperated!'
	,
	'blue, red or black'=>
	'blue, red or black'
	,
	'blue, red, black or Hexvalue'=>
	'blue, red, black or Hexvalue'
	,
	'Choose color for trail heading.'=>
	'Choose color for trail heading.'
	,
	'Choose color for trail heading:'=>
	'Choose color for trail heading:'
);