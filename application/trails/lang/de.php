<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'+ add category'=>
	'Gebiet hinzufügen'
	,
	'title'=>
	'Name des Gebiets'
	,
	'image'=>
	'Bild (klicken zum auswählen)'
	,
	'add'=>
	'hinzufügen'
	,
	'back'=>
	'zurück'
	,
	'Do you really want to delete this category? All trails in it will be lost.'=>
	'Möchtest Du wirklich dieses Gebiet löschen? Alle Strecken in diesem werden dann auch gelöscht.'
	,
	'update'=>
	'aktualisieren'
	,
	'edit category'=>
	'Gebiet bearbeiten'
	,
	'+ add trail'=>
	'Strecke hinzufügen'
	,
	'Do you really want to delete this trail?'=>
	'Möchtest Du diese Strecke wirklich löschen?'
	,
	'Name of trail'=>
	'Name der Strecke'
	,
	'save'=>
	'hinzufügen'
	,
	'trails'=>
	'Strecken'
	,
	'✎ edit legend'=>
	'Legende bearbeiten'
	,
	'hide/show legend'=>
	'Legende öffnen/schließen'
	,
	'Tags'=>
	'Schlagwörter'
	,
	'Tags in use'=>
	'Schlagwörter in Gebrauch'
	,
	'tag1,tag2,tag3'=>
	'Schlagwort1,Schlagwort2,wort3'
	,
	'format tags comma seperated!'=>
	'Schlagwörter bitte mit Komma trennen!'
	,
	'blue, red, black or Hexvalue'=>
	'blue, red, black oder ein Hexadezimalwert.'
	,
	'Choose color for trail heading:'=>
	'Wähle eine Farbe für die Überschrift:'
);