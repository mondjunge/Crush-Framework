CREATE TABLE IF NOT EXISTS `{dbprefix}trails_category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `image` varchar(191) NOT NULL DEFAULT '',
  `title` text NOT NULL DEFAULT '', 
  `legend` text NOT NULL DEFAULT '', 
  PRIMARY KEY (`id`)
);