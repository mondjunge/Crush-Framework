CREATE TABLE IF NOT EXISTS `{dbprefix}trails` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `category_id` bigint NOT NULL,
  `color` varchar(191) NOT NULL DEFAULT 'blue',
  `user_id` bigint NOT NULL,
  `title` varchar(191) NOT NULL DEFAULT '',
  `tags` varchar(191) NOT NULL DEFAULT '',
  `image` varchar(191) NOT NULL DEFAULT '',
  `komoot_link` varchar(191) NOT NULL DEFAULT '',
  `description` text NOT NULL DEFAULT '', 
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);