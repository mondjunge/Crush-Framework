<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
    'devices' =>
    'My devices'
    ,
    'Y-m-d H:i'=>
    'Y-m-d H:i'
    ,
    'Device Type'=>
    'Device Type'
    ,
    'Browser'=>
    'Browser'
    ,
    'Operating System'=>
    'Operating System'
    ,
    'Last Login'=>
    'Last Login'
    ,
    'remove device'=>
    'remove device'
    ,
    'useragent.desktop'=>
    'Laptop/Desktop PC'
    ,
    'useragent.mobile'=>
    'Smartphone'
    ,
    'useragent.tablet'=>
    'Tablet'
    ,
    'useragent.os.unknown'=>
    'Unknown'
    ,
    'Chrome'=>
    'Chrome'
    ,
    'Linux'=>
    'Linux'
    ,
    'Opera'=>
    'Opera'
    ,
    'Firefox'=>
    'Firefox'
    ,
    'Windows'=>
    'Windows'
    ,
    'Mac OS'=>
    'Mac OS'
    ,
    'Safari'=>
    'Safari'
    ,
    'Edge'=>
    'Edge'
    ,
    'iPhone OS'=>
    'iPhone OS'
    ,
    'iPad OS'=>
    'iPad OS'
    ,
    'iPod OS'=>
    'iPod OS'
    ,
    'Android'=>
    'Android'
    ,
    'BlackBerry'=>
    'BlackBerry'
    ,
    'Windows Phone OS'=>
    'Windows Phone OS'
    ,
    'h'=>
    'h'
    ,
    'Push Active'=>
    'Push Active'
    ,
    'Enable Push-Notifications on this device.'=>
    'Enable Push-Notifications on this device.'
    ,
    'Current device'=>
    'Current device'
    ,
    'Disable Push-Notifications on this device.'=>
    'Disable Push-Notifications on this device.'
    ,
    'These devices have sign in previously'=>
    'These devices have sign in previously'
    ,
    'Removing them disables permanent login on these devices.'=>
    'Removing them disables permanent login on these devices.'
    ,
    'Device deleted.'=>
    'Device removed.'
	,
	'Push-Notifications disabled.'=>
	'Push-Notifications disabled.'
	,
	'Push-Notifications enabled.'=>
	'Push-Notifications enabled.'
	,
	''=>
	''
);