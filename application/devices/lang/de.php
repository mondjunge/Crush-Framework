<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
    'devices' =>
    'Meine Geräte'
    ,
    'Y-m-d H:i'=>
    'd.m.Y H:i'
    ,
    'Device Type'=>
    'Geräteart'
    ,
    'Browser'=>
    'Browser'
    ,
    'Operating System'=>
    'Betriebssystem'
    ,
    'Last Login'=>
    'Letztes Login'
    ,
    'remove device'=>
    'Entferne Gerät'
    ,
    'useragent.desktop'=>
    'Laptop/Desktop PC'
    ,
    'useragent.mobile'=>
    'Smartphone'
    ,
    'useragent.tablet'=>
    'Tablet'
    ,
    'useragent.os.unknown'=>
    'Unbekannt'
    ,
    'Chrome'=>
    'Chrome'
    ,
    'Linux'=>
    'Linux'
    ,
    'Opera'=>
    'Opera'
    ,
    'Firefox'=>
    'Firefox'
    ,
    'Windows'=>
    'Windows'
    ,
    'Mac OS'=>
    'Mac OS'
    ,
    'Safari'=>
    'Safari'
    ,
    'Edge'=>
    'Edge'
    ,
    'iPhone OS'=>
    'iOS'
    ,
    'iPad OS'=>
    'iOS'
    ,
    'iPod OS'=>
    'iOS'
    ,
    'Android'=>
    'Android'
    ,
    'BlackBerry'=>
    'BlackBerry'
    ,
    'Windows Phone OS'=>
    'Windows Phone OS'
    ,
    'h'=>
    'Uhr'
    ,
    'These devices have sign in previously. Removing them disables autologin on these devices.'=>
    'Mit diesen Geräten hast Du Dich bereits eingeloggt. Sie können entfernt werden um das dauerhafte Login auf diesen Geräten zu deaktivieren.'
    ,
    'Push Active'=>
    'Push Aktiv'
    ,
    'Enable Push-Notifications on this device.'=>
    'Aktiviere Push-Benachrichtigungen auf diesem Gerät'
    ,
    'Disable Push-Notifications on this device.'=>
    'De-aktiviere Push-Benachrichtigungen auf diesem Gerät'
    ,
    'Current device'=>
    'Aktuelles Gerät'
    ,
    'These devices have sign in previously'=>
    'Mit diesen Geräten wurde sich bereits eingeloggt'
    ,
    'Removing them disables permanent login on these devices.'=>
    'Löschen der Einträge deaktiviert permanentes Einloggen und Push-Benachrichtigungen auf diesen Geräten.'
    ,
    'Device deleted.'=>
    'Gerät entfernt.'
    ,
    'Push-Notifications disabled.'=>
    'Push-Benachrichtigungen wurden für dieses Gerät deaktiviert.'
    ,
    'Push-Notifications enabled.'=>
    'Glückwunsch! Push-Benachrichtigungen dieser Webseite können nun auf deinem Gerät empfangen werden!'
);