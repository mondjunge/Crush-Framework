/* global utils, modules, base */

/**
 * devices
 */

use_package('modules');
modules.devices = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    this.init = function () {
        handlePushActivation();
    };
    var getTranslation = function (key) {
        return $("#translations").data(key);
    };

    var handlePushActivation = function () {

        var subscriptionErrorHandling = function () {
            console.log("parent", $("#pushActive").parent().parent());
            if ($("#pushActive").is(":checked")) {
                $("#pushActive").prop('checked', false);
            }
            var label = $("#pushActive").parent().parent();
            label.attr("title", getTranslation("enabledevice"));
            label.find("span[class='label']").html(getTranslation("enabledevice"));
        };
        utils.eventRegistry.addListener("notificationPermissionError", function () {
            subscriptionErrorHandling();
        });

        utils.eventRegistry.addListener("subscriptionFailed", function () {
            subscriptionErrorHandling();
        });

        utils.eventRegistry.addListener("unsubscribeComplete", function () {
            subscriptionErrorHandling();
            utils.messages.success(getTranslation("pushdisabled"));
        });


        utils.eventRegistry.addListener("subscriptionCompleted", function () {
            if (!$("#pushActive").is(":checked")) {
                $("#pushActive").prop('checked', true);
            }
            var label = $("#pushActive").parent().parent();
            label.attr("title", getTranslation("disabledevice"));
            label.find("span[class='label']").html(getTranslation("disabledevice"));
            utils.messages.success(getTranslation("pushenabled"));
        });

        $("#pushActive").on('change', function () {
            console.log($(this));
            if ($(this).is(":checked")) {
                utils.webpush.subscribe();
            } else {
                utils.webpush.unsubscribe();
            }
            ;
        });
    };

//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


}