CREATE TABLE IF NOT EXISTS `{dbprefix}devices` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `useragent` varchar(191) NOT NULL,
  `fingerprint` varchar(191) NOT NULL,
  `endpoint` text,
  `p256dh` varchar(88),
  `auth` varchar(24),
  `last_login` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_ipv4` text,
  `last_ipv6` text,
  PRIMARY KEY (`id`)
);