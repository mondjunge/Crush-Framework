<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */

/**
 * Description of Devices and device based things
 *
 * @author wahrendorff
 */
class Devices extends AppController {

    public function index() {
        $this->autoCheckRights();
        if (filter_has_var(INPUT_GET, "registerWebPushSubscription")) {
            $this->standalone = true;
            if ($this->registerWebPushSubscription()) {
                echo "true";
            } else {
                echo "false";
            }
            return;
        }
        if (filter_has_var(INPUT_GET, "unsubscribeWebPushSubscription")) {
            $this->standalone = true;
            if ($this->unsubscribeWebPushSubscription()) {
                echo "true";
            } else {
                echo "false";
            }
            return;
        }

        $userAgentUtil = new UserAgent();
        //print_r($userAgentUtil->getReadableUserAgent($_SERVER['HTTP_USER_AGENT']));

        if (filter_has_var(INPUT_GET, 'deleteDevice')) {
            $devId = filter_input(INPUT_GET, 'deleteDevice', FILTER_SANITIZE_NUMBER_INT);
            $this->delete('devices', "id=" . $devId . " AND user_id=" . SESSION::get('uid'));
            $this->setSystemMessage($this->ts("Device deleted."));
            $this->jumpTo('devices');
        }

        /**
         * TODO:
         * get id of current user
         * get devices for all current users
         * show all devices for current user in table
         * make devices deleteable
         */
        $rs = $this->select('devices', '*', 'user_id=' . SESSION::get('uid'));
        $devices = array();
        //$data['currentDevice'] = array();
        foreach ($rs as $dev) {
            $dev['rua'] = $userAgentUtil->getReadableUserAgent($dev['useragent']);
            if (Session::get("currentDeviceFingerprint") == $dev['rua']->fingerprint) {
                // remember current device
                $data['currentDevice'] = $dev;
            }
            // print_r($dev);
            $devices[] = $dev;
        }
        $data['appServerKey'] = Config::get('publicPushApplicationKey');
        $data['devices'] = $devices;

        $this->view('devices', $data);
    }

    private function registerWebPushSubscription() {
        //$this->autoCheckRights();
        $endpoint = filter_input(INPUT_POST, 'endpoint', FILTER_SANITIZE_URL);
        $p256dh = filter_input(INPUT_POST, 'p256dh', FILTER_UNSAFE_RAW);
        $auth = filter_input(INPUT_POST, 'auth', FILTER_UNSAFE_RAW);
        //$useragent = filter_input(INPUT_POST, 'useragent', FILTER_UNSAFE_RAW);

        $fieldsValueArray = array(
            'endpoint' => $endpoint,
            'p256dh' => $p256dh,
            'auth' => $auth,
        );

        return $this->updatePrepare('devices', $fieldsValueArray, "user_id=:uid AND fingerprint=:fingerprint", array('uid' => Session::get("uid"), 'fingerprint' => Session::get("currentDeviceFingerprint")));
    }

    private function unsubscribeWebPushSubscription() {
        $this->autoCheckRights();
        //$endpoint = filter_input(INPUT_POST, 'endpoint', FILTER_SANITIZE_URL);

        $fieldsValueArray = array(
            'endpoint' => null,
            'p256dh' => null,
            'auth' => null,
        );

        return $this->updatePrepare('devices', $fieldsValueArray, "user_id=:uid AND fingerprint=:fingerprint", array('uid' => Session::get("uid"), 'fingerprint' => Session::get("currentDeviceFingerprint")));
    }

}
