use_package("de.his.webpush");
/**
 * Handles Data for webpush, data is in webpushJavascript.xhtml
 */
de.his.webpush.DataHolder = new function() {

    var context;

    this.get = function(property) {
    	if (!context) {
            context = jQuery("#webpushDataHolder");
    	}
        return context.data(property);
    };
    
};
/**
 * Handles Web-Push Subscriptions and ServiceWorker registration
 */
de.his.webpush.Messaging = new function() {
    de.his.modules.ModuleRegistry
        .registerModule(this)
    	.registerAjaxRefreshModule(this);
    this.moduleName = "de.his.webpush.Messaging";
    
    this.permissionsGranted = false;
    this.isPushSupported = true;
    this.isPushEnabled = false;
    //this.serviceWorkerRegistered = true;
   // this.swRegistration = null;
    
    this.contextPath = ""+de.his.common.ContextInformation.get('context-path');
    this.appServerKey = ""+de.his.webpush.DataHolder.get('webpush-applicationserverkey-public');
    this.webpushGlobalActive = ""+de.his.webpush.DataHolder.get('webpush-active');
    this.askForPermisson = ""+de.his.webpush.DataHolder.get('webpush-ask-for-permisson');
    this.personId = ""+de.his.webpush.DataHolder.get('webpush-personid');
    
    // map class instance to self, since the context of "this" can change.
    var self = this;
    
    this.init = function() {
    	// disable Buttons if push is not configured or serviceWorkers are not supported
    	if(self.appServerKey==='' || self.webpushGlobalActive !== "true" || !('serviceWorker' in navigator) || !('PushManager' in window)){
    		if(!('serviceWorker' in navigator)){
    			console.error('Service workers aren\'t supported in this browser.');  
    		}
    		// Check is push API is supported
    	    if (!('PushManager' in window)) {
    	        console.error('Push messaging isn\'t supported in this browser.');
    	    }
    		this.disablePushNotificationButton();
    		return;
    	}
    	
    	// register Listener Events
    	self.registerEvents();
    	
    	//self.unregisterServiceWorker();
    	
    	// check if the service worker for push notifications is registered
		// is always registerd since 2021.12

    	//self.serviceWorkerRegistered = de.his.serviceworker.registration.isServiceWorkerRegistered();
    	if(self.getLocalStorage('isPushEnabled') == 'true'){
    		self.isPushEnabled = true;
    	} else {
    		self.isPushEnabled = false;
    	}
    	
    	/**
    	 * When Permission to show Notifications is blocked by the user in the browser, unsubscribe.
    	 */
    	if(self.isPushEnabled){
    		self.checkNotificationsPermissions();
    	}
    	
    	console.log("sw: ", navigator);
    	if(self.isPushEnabled){
    		console.log("sw: ", navigator.serviceWorker);
        	console.log("sw: ", navigator.serviceWorker.controller);
    	}
    	
    	console.log("Push Enabled? : " +self.isPushEnabled);
    	
    	self.showButtonHandler();
    	
    	/**
    	 * initial user Query
    	 */
    	if(!self.isPushEnabled && self.askForPermisson==="true" && self.getLocalStorage('webpush-askPermisson')!=="false"){
    		
    		self.askUserForPushActivation();
    	}
    	
    };
    
    this.askUserForPushActivation = function(){
    	/**
    	 * DONE: Do more...
    	 */
    	jQuery("#askForNotificationPermission").removeClass("unsichtbar");
    	jQuery("#askForNotificationPermissionBackground").show();
    	
    	jQuery(".noNotifications").click(function(e){
    		e.preventDefault();
    		self.setLocalStorage('webpush-askPermisson', 'false');
    		jQuery("#askForNotificationPermission").addClass("unsichtbar");
    		jQuery("#askForNotificationPermissionBackground").hide();
    		return false;
    	});
    	
    	jQuery(".yesNotifications").click(function(e){
    		e.preventDefault();
    		//self.setLocalStorage('webpush-askPermisson', 'false');
    		de.his.webpush.Messaging.requestNotificationsPermissions(false);
    		jQuery("#askForNotificationPermission").addClass("unsichtbar");
    		jQuery("#askForNotificationPermissionBackground").hide();
    		return false;
    	});
    	
    	//de.his.webpush.Messaging.requestNotificationsPermissions();
    };
    
    this.checkIfEndpointIsStillValid = function(){
    	var d = new Date();
    	var nextCheck = self.getLocalStorage('webpush-endpoint-is-valid-next-check')
    	if(nextCheck > d.getTime()){
    		var checkDate = new Date();
    		checkDate.setTime(nextCheck);
    		console.log("Next Endpoint validity check on "+checkDate+"");
    		//self.isPushEnabled = true;
    		self.showButtonHandler();
    		return true;
    	}
    	return jQuery.ajax({
			type: 'POST',
			url: self.contextPath+'/api/v1/cs/sys/isEndpointValid',
			cache: false,
		    //contentType: "multipart/form-data",
		    //processData: false,
			data: {
				endpoint: encodeURIComponent(self.getLocalEndpoint())
			},
			success : function(response){
				
				console.log("isEndpointValid Result: "+ response);
				
				if(response==='true'){
					console.log("Endpoint is Valid.");
					//self.isPushEnabled = true;
					self.showButtonHandler();
					self.setEndpointNextCheck();
					return true;
				}else{
					console.log("Endpoint is NOT Valid anymore :(.");
					//self.unregisterServiceWorker();
					self.removeLocalEndpoint();
					self.isPushEnabled = false;
				   // self.serviceWorkerRegistered = false;
				    //self.swRegistration = null;
				    self.showButtonHandler();
					return false;
				}
				
			}, 
			error : function(response,b,c){
				console.error("isEndpointValid Error: " + c);
				self.showButtonHandler();
				return true;
			}
			
		}); 
    	
    };
    
    this.setEndpointNextCheck = function(){
    	var d = new Date();
    	d.setDate(d.getDate() + 1); // half a year or so would be ok, but then the "last-active" column in the db would never be updated.
    	self.setLocalStorage('webpush-endpoint-is-valid-next-check', d.getTime());
    };
    
    this.removeEndpointNextCheck = function(){
    	self.removeLocalStorage('webpush-endpoint-is-valid-next-check');
    };
    
	this.registerEvents = function(){
    	de.his.modules.eventRegistry.addListener("notifyPermissionsGranted", function(){
    		// self.registerServiceWorker();
    		if(!self.isPushEnabled){
    			self.subscribe();
    		}
    		//self.subscribe();
    		self.setLocalStorage('webpush-askPermisson', 'false');
		});
		de.his.modules.eventRegistry.addListener("subscriptionCompleted", function(){
			self.setLocalStorage('isPushEnabled','true');
			self.isPushEnabled = true;
    		self.showButtonHandler();
    		self.addToUsersSubscribed();
    		// alert("Push Notifications activated");
    		
    		de.his.component.MessagesInfobox.showMessagesInfobox(self.getTranslation('enable-success'),"success");

		});
		// 
		de.his.modules.eventRegistry.addListener("unsubscribedPushNotifications", function(){
			self.removeLocalStorage('isPushEnabled');
			self.removeLocalStorage('webpush-askPermisson');
			self.isPushEnabled = false;
			self.removeFromUsersSubscribed();
    		self.showButtonHandler();
    		if(!self.areUsersSubscribed()){
    			//self.unregisterServiceWorker();
    			//self.serviceWorkerRegistered = false;
    		}
    		if(self.permissionsGranted){
    			de.his.component.MessagesInfobox.showMessagesInfobox(self.getTranslation('disable-success'),"success");
    		}
		});
//		de.his.modules.eventRegistry.addListener("serviceWorkerRemoved", function(){
//			self.removeLocalStorage('webpush-askPermisson');
//			self.removeLocalStorage('isPushEnabled');
//		 	this.isPushEnabled = false;
//		    this.serviceWorkerRegistered = false;
//		    this.swRegistration = null;
//    		self.showButtonHandler();
//
//		});
		de.his.modules.eventRegistry.addListener("subscriptionFailed", function(){
			//self.unregisterServiceWorker();
			de.his.component.MessagesInfobox.showMessagesInfobox(self.getTranslation('subscription-failed'),"error");
		});
		
    };
    
    this.getTranslation = function(string){
    	return jQuery('.pushSubscriptionTranslations').data(string);
    };
    
    this.disablePushNotificationButton = function(){
    	this.isPushSupported = false;
    	jQuery(".activateNotifications").addClass("unsichtbar");
		jQuery(".deactivateNotifications").addClass("unsichtbar");
    };
    
    this.showButtonHandler = function(){
    	if(!this.isPushSupported){
    		return;
    	}
    	console.log("HandlingButtons for state this.isPushEnabled: ", this.isPushEnabled);
    	if(this.isPushEnabled){
    		jQuery(".activateNotifications").addClass("unsichtbar");
			jQuery(".deactivateNotifications").removeClass("unsichtbar");
			
			jQuery(".deactivateNotifications").unbind("click");
			jQuery(".deactivateNotifications").click(function(e){
				e.preventDefault();
	    		de.his.webpush.Messaging.disableNotificationsPermissions();
	    		return false;
	    	});
		}else{
			jQuery(".deactivateNotifications").addClass("unsichtbar");
			jQuery(".activateNotifications").removeClass("unsichtbar");
			
			jQuery(".activateNotifications").unbind("click");
	    	jQuery(".activateNotifications").click(function(e){
	    		e.preventDefault();
	    		de.his.webpush.Messaging.requestNotificationsPermissions(false);
	    		return false;
	    	});
		}
	};
    
    this.requestNotificationsPermissions = function (checkOnly){
    	
    	console.log('Request Permission for Notifications.');
    	
    	if (Notification.permission === 'denied') {
    		self.unsubscribe();
    		de.his.component.MessagesInfobox.showMessagesInfobox(self.getTranslation('permission-denied-by-browser'),"error");
            return;
        }
    	
		return new Promise(function(resolve, reject) {
				const permissionResult = Notification.requestPermission(function(result) {
				resolve(result);
			});
			
			if (permissionResult) {
				permissionResult.then(resolve, reject);
			}}).then(function(permissionResult) {
				if (permissionResult !== 'granted') {
					if(checkOnly){
						self.unsubscribe();
						de.his.component.MessagesInfobox.showMessagesInfobox(self.getTranslation('permission-denied-by-browser'),"error");
						self.permissionsGranted = false;
						self.showButtonHandler();
					}
					throw new Error('We weren\'t granted permission.');
				}
			 	self.permissionsGranted = true;
			 	if(!checkOnly){
			 		de.his.modules.eventRegistry.fireEvent("notifyPermissionsGranted");
			 	}
			 	return permissionResult;
			}).catch(function(err) {
	    	  console.log('Unable to get permission to notify.', err);
	    	  self.unsubscribe();
	    	  de.his.component.MessagesInfobox.showMessagesInfobox(self.getTranslation('permission-denied-by-browser'),"error");
	    	  self.permissionsGranted = false;
	    	  self.showButtonHandler();
    	});
    };
    
    this.checkNotificationsPermissions = function(){
    	self.requestNotificationsPermissions(true);
    }
	
// 	this.registerServiceWorker = function(){
// 		if(self.serviceWorkerRegistered && self.swRegistration != null){
//     		console.log('ServiceWorker already registered');
//     		de.his.modules.eventRegistry.fireEvent("serviceWorkerRegistered");
//     		return self.swRegistration;
//     	}
//     	
// 		return navigator.serviceWorker.register(de.his.common.ContextInformation.get('context-path') + "/serviceworker.js", { scope: de.his.common.ContextInformation.get('context-path') + "/" })
// 		.then(function(registration) {
// 			self.swRegistration = registration;
// 			de.his.modules.eventRegistry.fireEvent("serviceWorkerRegistered");
// 			console.log('Service worker successfully registered.');
// 			return registration;
// 		}, function(err) {
// 				// registration failed :(
// 				console.log('ServiceWorker registration failed: ', err);
// 		}).catch(function(err) {
// 			console.error('Unable to register service worker.', err);
// 		});
//     };
    
    this.subscribe = function(){
    	
		console.log("navigator.serviceWorker", navigator.serviceWorker)
    	//var registration = de.his.serviceworker.registration.registerServiceWorker();
    	
    	return navigator.serviceWorker.ready.then(function(registration) {
    		//console.log('reg Object: ', registration);
			console.log('subscribing using ask: ', self.appServerKey);
			const subscribeOptions = {
				userVisibleOnly: true,
				applicationServerKey: self.urlBase64ToUint8Array(
					""+self.appServerKey
				)
			};
			
			// console.log('Received subscriptionOptions: ',
			// JSON.stringify(subscribeOptions));
			return registration.pushManager.subscribe(subscribeOptions);
			
		}).catch(function(error){
    		console.error("Error subscribing push: ", error);
    		if(self.appServerKey.length < 64){
    			console.error("You seem to have mixed up the private/public VAPID keys in web-push configuration. Please check.");
    		} else {
    			console.error("Is there an internet connection on both, the server and your client? Have you configured web-push correctly?");
    		}
    		
    		de.his.modules.eventRegistry.fireEvent("subscriptionFailed");
    		return;
    	}).then(function(pushSubscription) {
    		if(typeof pushSubscription === 'undefined'){
    			return;
    		}
			console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
			// send subscription details to server
 			// console.log('endpoint: '+pushSubscription.endpoint);
 			// console.log('key p256dh: '+ pushSubscription.getKey('p256dh'));
 			// console.log('key auth: '+ pushSubscription.getKey('auth'));
			
			if(self.sendSubscriptionToServer(pushSubscription)){
				self.storeLocalEndpoint(pushSubscription.endpoint);
				de.his.modules.eventRegistry.fireEvent("subscriptionCompleted");
			}else{
				//self.unregisterServiceWorker();
			}
			
			return pushSubscription;
		});
    	
    };
    
    this.storeLocalEndpoint = function (endpoint){
    	this.removeLocalEndpoint();
    	self.setLocalStorage('webpush-endpoint', endpoint);
    };
	
	this.getLocalEndpoint = function (){
    	return self.getLocalStorage("webpush-endpoint");
    };
    
    this.removeLocalEndpoint = function (){
    	self.removeLocalStorage("webpush-endpoint");
    };
    
    this.sendSubscriptionToServer = function (pushSubscription) {
		console.log("subscribeOnWebsite called");
		
		var jsonString = JSON.stringify(pushSubscription);
  	  	var subscriptionObject = JSON.parse(jsonString); 
  	  	
  	  	console.log("endpoint: "+encodeURIComponent(subscriptionObject.endpoint));
	  	console.log("p256dh: "+subscriptionObject.keys.p256dh);
	  	console.log("auth: "+subscriptionObject.keys.auth);
		
//		var data = new FormData();
//		data.append( "endpoint", ""+subscriptionObject.endpoint );
//		data.append( "p256dh", ""+subscriptionObject.keys.p256dh );
//		data.append( "auth", ""+subscriptionObject.keys.auth );
		
		return jQuery.ajax({
			type: 'POST',
			url: self.contextPath+'/api/v1/cs/sys/registerPushIdentifier',
			cache: false,
		    //contentType: "multipart/form-data",
		    //processData: false,
			data: {
				endpoint: encodeURIComponent(subscriptionObject.endpoint),
				p256dh: subscriptionObject.keys.p256dh,
				auth: subscriptionObject.keys.auth,
				useragent: encodeURIComponent(navigator.userAgent)
			},
			success : function(response){
				
				console.log("registerPushIdentifier Result: "+ response);
				
				if(response==='true'){
					console.log("Successful send subscription to server.");
					return true;
				}else{
					console.log("Server Error sending subscription to server.");
					//self.unregisterServiceWorker();
					self.showButtonHandler();
					return false;
				}
				
			}, 
			error : function(response,b,c){
				console.error("registerPushIdentifier Error: " + c);
				//self.unregisterServiceWorker();
				self.showButtonHandler();
				return false;
			}
			
		}); 
	};
    
    function ab2str(buf) {
        var dataView = new DataView(buf);
        var decoder = new TextDecoder('utf-8');
        return decoder.decode(dataView);
    };
    
    this.disableNotificationsPermissions = function (){
    	console.log('Push disabled.');
    	this.isPushEnabled = false;
    	self.removeLocalStorage('isPushEnabled');
    	self.unsubscribe();
    	return false;
    };
    
//     this.unregisterServiceWorker = function(){
// 		navigator.serviceWorker.getRegistrations().then(function(r) {
// 			console.log('Removing ServiceWorker.');
// 			self.swRegistration = null;
// 			return Promise.all(r.map(function(reg){reg.unregister()}));
// 		}).catch(function(err){
// 			console.log('Removing ServiceWorker failed with error: ', err);
// 		});
//     };
    
    this.unsubscribe = function(){
    	self.isPushEnabled = false;
    	self.removeLocalStorage('isPushEnabled');
    	var localEndpoint = self.getLocalEndpoint();
    	console.log("Unsubscribe Endpoint "+ localEndpoint);
    	
     	if(typeof localEndpoint !== 'undefined' && localEndpoint !== null){
     		console.log("Removing Endpoint "+localEndpoint+" from db");
 			jQuery.ajax({
 				type: 'POST',
 				url: self.contextPath+'/api/v1/cs/sys/unsubscribeEndpoint',
 				data: {     
 					endpoint: encodeURIComponent(localEndpoint)  
 				} 
 			}); 
 		}
		
		//self.unregisterServiceWorker();
		//self.serviceWorkerRegistered = false;
		
		self.removeEndpointNextCheck();
		self.removeLocalEndpoint();
		de.his.modules.eventRegistry.fireEvent("unsubscribedPushNotifications");
		return true;
    
    };
    
//     this.isServiceWorkerRegistered = function(){
// 	    if ('serviceWorker' in navigator && navigator.serviceWorker !== null && typeof navigator.serviceWorker !== 'undefined') {
// 	    	if('controller' in navigator.serviceWorker && navigator.serviceWorker.controller !== null && typeof navigator.serviceWorker.controller !== 'undefined'){
// 	    		if( navigator.serviceWorker.controller.state === 'activated'){
// 	    			return true;
// 	    		}
// 	    	}else{
// 	    		return false;
// 	    	}
// 		} else{
// 			return false;
// 		}
//     };
    
    this.urlBase64ToUint8Array = function (base64String) {
		const padding = '='.repeat((4 - base64String.length % 4) % 4);
		const base64 = (base64String + padding)
		.replace(/\-/g, '+')
		.replace(/_/g, '/');
		
		const rawData = window.atob(base64);
		const outputArray = new Uint8Array(rawData.length);
		
		for (let i = 0; i < rawData.length; ++i) {
			outputArray[i] = rawData.charCodeAt(i);
		}
		return outputArray;
	};
	
	this.setLocalStorage = function(key,value){
		console.log("set localStorageKey "+""+self.personId+"_"+key);
    	localStorage.setItem(""+self.personId+"_"+key, value);
    };
    this.getLocalStorage = function(key){
    	console.log("get localStorageKey "+""+self.personId+"_"+key);
    	return localStorage.getItem(""+self.personId+"_"+key);
    };
    this.removeLocalStorage = function(key){
    	console.log("remove localStorageKey "+""+self.personId+"_"+key);
    	localStorage.removeItem(""+self.personId+"_"+key);
    };
    
    this.addToUsersSubscribed = function(){
    	/**
    	 * load csv from localStorage
    	 * add id
    	 * save csv to loacStorage
    	 */
    	var cvs = localStorage.getItem('subscribedUsers');
    	var data = [];
    	if(cvs != null){
    		data = cvs.split(',');
    	}
    	
    	data.push(self.personId);
    	
    	var csvContent = data.join(",");
    	console.log("cvsContent: "+csvContent);
    	localStorage.setItem('subscribedUsers',csvContent);
    };
    
    this.removeFromUsersSubscribed = function(){
    	/**
    	 * load csv from localStorage
    	 * remove id
    	 * save csv to localStorage, remove if empty
    	 */
    	var cvs = localStorage.getItem('subscribedUsers');
    	console.log("cvs: "+cvs);
    	var data = [];
    	if(cvs != null){
    		data = cvs.split(',');
    	}
    	
    	var i = data.indexOf(self.personId);
    	if(i != -1) {
    		data.splice(i, 1);
    	}
    	if(data.length < 1){
    		localStorage.removeItem('subscribedUsers');
    	}else{
    		var csvContent = data.join(",");
    		console.log("cvsContent: "+csvContent);
        	localStorage.setItem('subscribedUsers',csvContent);
    	}
    };
    
    this.areUsersSubscribed = function(){
    	/**
    	 * check if localStorage is Empty or not.
    	 */
    	var cvs = localStorage.getItem('subscribedUsers');
    	console.log("cvs: "+cvs);
    	if(cvs == null || cvs == ''){
    		return false;
    	}else{
    		return true;
    	}
    };
    
	this.refresh = function() {
		console.log("refreshing");
		this.showButtonHandler();
		//self.registerEvents();
	};
    
};
