<h2><?php echo $this->ts('devices'); ?></h2>
<!--

-->
<span id="translations" class="hidden" 
      data-pushdisabled="<?php echo $this->ts("Push-Notifications disabled."); ?>"
      data-pushenabled="<?php echo $this->ts("Push-Notifications enabled."); ?>"
      data-disabledevice="<?php echo $this->ts("Disable Push-Notifications on this device."); ?>"
      data-enabledevice="<?php echo $this->ts("Enable Push-Notifications on this device."); ?>"></span>
<input type="hidden" name="appServerKey" value="<?php echo $appServerKey; ?>" />
<!--<h4>: </h4>-->
    
<div class="boxBorder">
    <div><?php echo $this->ts('Current device'); ?></div>
    <h3><?php echo $this->ts($currentDevice['rua']->deviceType). ", ".$currentDevice['rua']->browser .", ".$currentDevice['rua']->os ; ?></h3>
    <div class="pure-controls">
        <label for="pushActive" class="pure-checker" tabindex="0" title="<?php if ($currentDevice['endpoint'] != null) :  echo $this->ts("Disable Push-Notifications on this device."); else: echo $this->ts("Enable Push-Notifications on this device."); endif; ?>">
            <div class="checker">
                <input id="pushActive" class="checkbox" name="pushactive" type="checkbox" <?php if ($currentDevice['endpoint'] != null) :  echo 'checked="checked"'; else: echo''; endif; ?>/>
                <div class="check-bg"></div>
                <div class="checkmark">
                    <svg viewBox="0 0 100 100">
                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
            </div>
            <span class="label"><?php if ($currentDevice['endpoint'] != null) :  echo $this->ts("Disable Push-Notifications on this device."); else: echo $this->ts("Enable Push-Notifications on this device."); endif; ?></span>
        </label>
    </div>
</div>
    
<h4> <?php echo $this->ts('These devices have sign in previously').":"; ?></h4>
<p class="help"> <?php echo $this->ts('Removing them disables permanent login on these devices.'); ?></p>
<div class="tableWrapper">
    <table class="pure-table pure-table-striped" summary="devices overview">
        <thead>
            <tr>
<!--                <th><?php echo '#' ?></th>-->
                <th><?php echo $this->ts('Push Active') ?></th>
                <th><?php echo $this->ts('Device Type') ?></th>
                <th><?php echo $this->ts('Browser') ?></th>
                <th><?php echo $this->ts('Operating System') ?></th>
                <th><?php echo $this->ts('Last Login') ?></th>
                
            </tr>
        </thead>
        <tbody>
            <?php foreach ($devices as $d) : ?>
                <tr>
<!--                    <td><?php echo $d['id']; ?></td>-->
                    <td>
                        <label for="pushActive_<?php echo $d['id']; ?>" class="pure-checker" tabindex="0" title="">
                            <div class="checker">
                                <input class="checkbox" name="pushActive_<?php echo $d['id']; ?>" type="checkbox" <?php if ($d['endpoint'] != null) :  echo 'checked="checked"'; else: echo''; endif; ?> disabled/>
                                <div class="check-bg"></div>
                                <div class="checkmark">
                                    <svg viewBox="0 0 100 100">
                                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                </div>
                            </div>
                            <span class="label"></span>
                        </label>
                    </td>
                    <td><?php echo $this->ts($d['rua']->deviceType); ?></td>
                    <td><?php echo $this->ts($d['rua']->browser); ?></td>
                    <td><?php echo $this->ts($d['rua']->os); ?></td>
                    <td><?php echo date($this->ts('Y-m-d H:i'), strtotime($d['last_login'])); echo $this->ts("h"); ?></td>
                    
                    <td>
                        <a class="pure-button" href="?deleteDevice=<?php echo $d['id'] ?>" ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon-small svgIcon-dark'); echo $this->ts("remove device") ?></a>
                    </td>
                </tr>  
            <?php endforeach; ?> 
        </tbody>
    </table>
</div>