<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Insert new user' =>
	'Insert new user'
	,
	'Username'=>
	'Username'
	,
	'Passphrase'=>
	'Passphrase'
	,
	'Email'=>
	'Email'
	,
	'Repeat'=>
	'Repeat'
	,
	'Register as user'=>
	'Register as user'
	,
	'User inserted successfully'=>
	'User inserted successfully'
	,
	'Cannot register user!'=>
	'Cannot register user!'
        ,
	'Add user'=>
	'Add user'
	,
	'Users and Roles'=>
	'Users and Roles'
	,
	'Roles'=>
	'Roles'
	,
	'Active'=>
	'Active'
	,
	'Registration Time'=>
	'Registration Time'
	,
	'Role'=>
	'Role'
	,
	'Description'=>
	'Description'
	,
	'Users'=>
	'Users'
	,
	'Edit'=>
	'Edit'
	,
	'edit'=>
	'edit'
	,
	'Create new Role'=>
	'Create new Role'
	,
	'Create Role'=>
	'Create Role'
	,
	'Rolename'=>
	'Rolename'
	,
	'You have to '=>
	'You have to '
	,
	'User registered successfully!'=>
	'User registered successfully!'
	,
	'Password have to be at least 5 characters long!'=>
	'Password have to be at least 5 characters long!'
	,
	'Role created successfully!'=>
	'Role created successfully!'
	,
	'Cannot create user!'=>
	'Cannot create user!'
	,
	'Cannot create role without name!'=>
	'Cannot create role without name!'
	,
	'Unsufficiant rights.'=>
	'Unsufficiant rights.'
	,
	'Edit user'=>
	'Edit user'
	,
	'Change Password'=>
	'Change Password'
	,
	'New passphrase'=>
	'New passphrase'
	,
	'Repeat new passphrase'=>
	'Repeat new passphrase'
	,
	'Old passphrase'=>
	'Old passphrase'
	,
	'Save changes'=>
	'Save changes'
	,
	'User active'=>
	'User active'
	,
	'User updated successfully'=>
	'User updated successfully'
	,
	'Delete user'=>
	'Delete user'
	,
	'User deleted successfully'=>
	'User deleted successfully'
	,
	'Password & User updated successfully'=>
	'Password & User updated successfully'
	,
	'This E-Mail address is already registered!'=>
	'This E-Mail address is already registered!'
	,
	'Not an valid E-Mail address!'=>
	'Not an valid E-Mail address!'
	,
	'Nick have to be at least 3 characters long!'=>
	'Nick have to be at least 3 characters long!'
	,
	'Account'=>
	'Account'
	,
	'Nickname'=>
	'Nickname'
	,
	'Changing your E-mail will log you out'=>
	'Changing your E-mail will log you out'
	,
	'Changing your nickname will log you out'=>
	'Changing your nickname will log you out'
	,
	'You need to log in again.'=>
	'You need to log in again.'
	,
	'Background image'=>
	'Background image'
	,
	'File upload has gone wrong.'=>
	'File upload has gone wrong.'
	,
	'File has been uploaded succesfully.'=>
	'File has been uploaded succesfully.'
	,
	'Old file could not be deleted.'=>
	'Old file could not be deleted.'
	,
	'Logged in successfully!'=>
	'Logged in successfully!'
	,
	'Password does not match with the repetition.'=>
	'Password does not match with the repetition.'
	,
	'Warning: Not a good idea!.'=>
	'Warning: Not a good idea!.'
	,
	'Password have to be at least 5 characters long and not longer than 64 characters!'=>
	'Password have to be at least 5 characters long and not longer than 64 characters!'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'Username changes will log you out automatically.'=>
	'Username changes will log you out automatically.'
	,
	'Your User Account'=>
	'Your User Account'
	,
	'Delete account'=>
	'Delete account'
	,
	'User image'=>
	'User image'
	,
	'This is not allowed!'=>
	'This is not allowed!'
	,
	'Something went wrong while updating the user'=>
	'Something went wrong while updating the user'
	,
	'Hold down Ctrl-Key to select multiple roles.'=>
	'Hold down Ctrl-Key to select multiple roles.'
	,
	'Change user image'=>
	'Change user image'
	,
	'You will get notifications on this address.'=>
	'You will get notifications on this address.'
	,
	'Delete role'=>
	'Delete role'
	,
	'back'=>
	'back'
	,
	'Cannot update role!'=>
	'Cannot update role!'
	,
	'Role updated successfully.'=>
	'Role updated successfully.'
	,
	'What roles shall the new user get?'=>
	'What roles shall the new user get?'
	,
	'Which roles shall the new user get?'=>
	'Which roles shall the new user get?'
	,
	'Insert E-Mail address of the new user.'=>
	'Insert E-Mail address of the new user.'
	,
	'Invitation will be send to this address.'=>
	'Invitation will be send to this address.'
	,
	'Invite user'=>
	'Invite user'
	,
	'Invite new user'=>
	'Invite new user'
	,
	'You have been invited to join the community on $1'=>
	'You have been invited to join the community on $1'
	,
	'Hello $1,'=>
	'Hello $1,'
	,
	'you have been invited to join the community by $2.'=>
	'you have been invited to join the community by $2.'
	,
	'Login with your e-mail address under $3, and be a part of the community!'=>
	'Login with your e-mail address under $3, and be a part of the community!'
	,
	'Please change your temporary password as soon as you can.'=>
	'Please change your temporary password as soon as you can.'
	,
	'If you have any questions regarding this e-mail, contact $5 for more information.'=>
	'If you have any questions regarding this e-mail, contact $5 for more information.'
	,
	'Invitation e-mail was send to $1:'=>
	'Invitation e-mail was send to $1:'
	,
	'Login with your e-mail address under $3, and be a part of the community! Your friends are already waiting for you!'=>
	'Login with your e-mail address under $3, and be a part of the community! Your friends are already waiting for you!'
	,
	'Invite new users'=>
	'Invite new users'
	,
	'Nick is already taken from another user. Sorry.'=>
	'Nick is already taken from another user. Sorry.'
	,
	'Currently online'=>
	'online'
	,
	'image'=>
	'image'
	,
	'Change wallpaper'=>
	'Change wallpaper'
	,
	'Repeat wallpaper'=>
	'Repeat wallpaper'
	,
	'click here to select an image'=>
	'click here to select an image'
	,
	'X'=>
	'X'
	,
	'remove image'=>
	'remove image'
	,
	'remove wallpaper'=>
	'remove wallpaper'
	,
	'The desciption is going to show up on top of your profilepage.'=>
	'The desciption is going to show up on top of your profilepage.'
	,
	'key not set!'=>
	'key not set!'
	,
	'You are not logged in.'=>
	'You are not logged in.'
	,
	'Batch invite new users'=>
	'Batch invite new users'
	,
	'Which roles shall all the new users get?'=>
	'Which roles shall all the new users get?'
	,
	'Subject'=>
	'Subject'
	,
	'Last Online'=>
	'Last Online'
	,
	'E-mail adresses:'=>
	'E-mail adresses:'
	,
	'Seperate e-mail adresses with ;.'=>
	'Seperate e-mail adresses with ;.'
	,
	'Password forgotten?'=>
	'Password forgotten?'
	,
	'E-Mail'=>
	'E-Mail'
	,
	'Password'=>
	'Password'
	,
	'Change your password after first login:'=>
	'Change your password after first login:'
	,
	'inviteSubject'=>
	'inviteSubject'
	,
	'inviteText'=>
	'inviteText'
);