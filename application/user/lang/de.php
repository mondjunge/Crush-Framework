<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Insert new user' =>
	'Neuen Nutzer anlegen'
	,
	'Username'=>
	'Benutzername'
	,
	'Passphrase'=>
	'Passwort'
	,
	'Email'=>
	'E-Mail'
	,
	'Repeat'=>
	'Wiederholen'
	,
	'Register as user'=>
	'Als Benutzer registrieren'
	,
	'User inserted successfully'=>
	'Nutzer wurde erfolgreich hinzugefügt'
	,
	'Cannot register user!'=>
	'Nutzer konnte nicht registriert werden'
	,
	'Add user'=>
	'füge Nutzer hinzu'
	,
	'Users and Roles'=>
	'Benutzer und Rollen'
	,
	'Roles'=>
	'Rollen'
	,
	'Active'=>
	'Aktiv'
	,
	'Registration Time'=>
	'Zeit der Registrierung'
	,
	'Role'=>
	'Rolle'
	,
	'Description'=>
	'Beschreibung'
	,
	'Users'=>
	'Benutzer'
	,
	'Edit'=>
	'Bearbeiten'
	,
	'edit'=>
	'bearbeiten'
	,
	'Create new Role'=>
	'Erstelle eine neue Rolle'
	,
	'Create Role'=>
	'erstelle Rolle'
	,
	'Rolename'=>
	'Rollenname'
	,
	'You have to '=>
	'Du musst'
	,
	'User registered successfully!'=>
	'Benutzer erfolgreich registriert!'
	,
	'Password have to be at least 5 characters long!'=>
	'Das Passwort muss mindestens 5 Zeichen haben'
	,
	'Role created successfully!'=>
	'Rolle wurde erfolgreich angelegt'
	,
	'Cannot create user!'=>
	'Benutzer konnte nicht erstellt werden'
	,
	'Cannot create role without name!'=>
	'Es kann keine Rolle ohne Namen angelegt werden!'
	,
	'Unsufficiant rights.'=>
	'Keine Berechtigung.'
	,
	'Edit user'=>
	'Bearbeite Nutzer'
	,
	'Change Password'=>
	'Passwort ändern'
	,
	'New passphrase'=>
	'Neues Passwort'
	,
	'Repeat new passphrase'=>
	'Wiederholung neues PW'
	,
	'Old passphrase'=>
	'Altes Passwort'
	,
	'Save changes'=>
	'Änderungen speichern'
	,
	'User active'=>
	'Account aktiv'
	,
	'User updated successfully'=>
	'Nutzerdaten erfolgreich geändert'
	,
	'Delete user'=>
	'Nutzer löschen'
	,
	'User deleted successfully'=>
	'Nutzer wurde gelöscht'
	,
	'Password & User updated successfully'=>
	'Passwort und Nutzerdaten wurden erfolgreich geändert'
	,
	'This E-Mail address is already registered!'=>
	'Diese E-Mail Adresse ist bereits registriert.'
	,
	'Not an valid E-Mail address!'=>
	'Das ist keine gültige E-Mail Adresse.'
	,
	'Nick have to be at least 3 characters long!'=>
	'Der Benutzername muß mindestens 3 Zeichen lang sein.'
	,
	'Account'=>
	'Nutzerkonto'
	,
	'Nickname'=>
	'Benutzername'
	,
	'Changes will log you out automatically.'=>
	'Änderungen bewirken eine automatische Abmeldung.'
	,
	'Changing your nickname will log you out'=>
	'Bei Änderungen des Benutzernamens wird man automatisch abgemeldet.'
	,
	'You need to log in again.'=>
	'Du musst dich wieder anmelden um weiter zu machen.'
	,
	'Background image'=>
	'Hintergrund Bild hochladen'
	,
	'File upload has gone wrong.'=>
	'Beim Datei Upload ist etwas schief gelaufen. Entschuldigung.'
	,
	'File has been uploaded succesfully.'=>
	'Die Datei wurde hoch geladen.'
	,
	'Old file could not be deleted.'=>
	'Die alte Datei konnte nicht gelöscht werden.'
	,
	'Logged in successfully!'=>
	'Erfolgreich angemeldet'
	,
	'Password does not match with the repetition.'=>
	'Passwort und Passwortwiederholung stimmen nicht überein.'
	,
	'Warning: Not a good idea!.'=>
	'Achtung: Den eigenen Account zu deaktivieren ist keine gute Idee!'
	,
	'Password have to be at least 5 characters long and not longer than 64 characters!'=>
	'Das Passwort muß mindestens aus 5 Zeichen, aber weninger als 64 Zeichen bestehen!'
	,
	'Insufficient rights!'=>
	'Du hast keine ausreichenden Rechte für diese Aktion.'
	,
	'Your User Account'=>
	'Dein Nutzerkonto'
	,
	'Delete account'=>
	'Nutzerkonto löschen'
	,
	'User image'=>
	'Nutzerbild'
	,
	'Username changes will log you out automatically.'=>
	'Bei einer Änderung des Benutzernamen wirst Du automatisch ausgeloggt!'
	,
	'Something went wrong while updating the user'=>
	'Irgendwas ist beim speichern schief gelaufen. Informiere den Admin, wenn möglich.'
	,
	'Hold down Ctrl-Key to select multiple roles.'=>
	'Halte die [Strg]-Taste gedrückt, um mehrere Rollen auf einmal aus zu wählen.'
	,
	'Change user image'=>
	'Nutzerbild ändern'
	,
	'This is not allowed!'=>
	'Diese Aktion ist verboten.'
	,
	'You will get notifications on this address.'=>
	'Benachrichtigungen jeglicher Art werden an diese E-Mail Adresse geschickt.'
	,
	'Delete role'=>
	'Rolle löschen'
	,
	'back'=>
	'zurück'
	,
	'Cannot update role!'=>
	'Aktualisieren der Rolle fehlgeschlagen'
	,
	'Role updated successfully.'=>
	'Rolle wurde erfolgreich aktualisiert'
	,
	'Which roles shall the new user get?'=>
	'Welche Rollen soll der neue Nutzer erhalten?'
	,
	'Insert E-Mail address of the new user.'=>
	'Gib die E-Mail Adresse des neuen Nutzers ein. Eine E-Mail mit weiteren Instruktionen wird an diese Adresse geschickt.'
	,
	'Invitation will be send to this address.'=>
	'Die Einladung wird an diese Adresse gesendet.'
	,
	'Invite new user'=>
	'Neuen Nutzer einladen'
	,
	'You have been invited to join the community on $1'=>
	'Du wurdest auf $1 eingeladen'
	,
	'Hello $1,'=>
	'Hallo $1,'
	,
	'you have been invited to join the community by $2.'=>
	'du wurdest von $2 eingeladen der Community bei zu treten.'
	,
	'Login with your e-mail address under $3, and be a part of the community! Your friends are already waiting for you!'=>
	'Melde dich mit Deiner E-Mail Adresse unter $3 an und sei ein Teil der Community! Deine Freunde warten schon auf Dich!'
	,
	'Your temporary password is: $4'=>
	'Dein vorläufiges Passwort ist: $4'
	,
	'Please change your temporary password under $6 as soon as you can.'=>
	'Bitte ändere Dein vorläufiges Passwort unter $6 sobald Du kannst.'
	,
	'If you have any questions regarding this e-mail, contact $5 for more information.'=>
	'Falls Du irgendwelche Fragen bezüglich dieser E-Mail hast, kontaktiere $5 für mehr Informationen.'
	,
	'Invitation e-mail was send to $1:'=>
	'Einladung wurde an $1 versendet!'
	,
	'Invite new users'=>
	'Lade neue Nutzer ein'
	,
	'Nick is already taken from another user. Sorry.'=>
	'Der Nutzername is bereits an einen anderen Nutzer vergeben. Entschuldige bitte.'
	,
	'Currently online'=>
	'online'
	,
	'image'=>
	'Bild'
	,
	'Change wallpaper'=>
	'Wallpaper ändern'
	,
	'Repeat wallpaper'=>
	'Wallpaper wiederholen'
	,
	'click here to select an image'=>
	'Klicke hier um ein Bild auszuwählen'
	,
	'remove image'=>
	'Nutzerbild entfernen'
	,
	'remove wallpaper'=>
	'Wallpaper entfernen'
	,
	'The desciption is going to show up on top of your profilepage.'=>
	'Die Beschreibung wird gleich neben dem Profilbild auf Deiner Profilseite angezeigt.'
	,
	'key not set!'=>
	'Schlüssel nicht gesetzt!'
	,
	'You are not logged in.'=>
	'Du bist nicht angemeldet.'
	,
	'E-mail adresses:'=>
	'E-Mail Adressen:'
	,
	'Seperate e-mail adresses with ;.'=>
	'Mehrere E-mail Adressen bitte mit ; trennen.'
	,
	'Batch invite new users'=>
	'Lade mehrere Nutzer auf einmal ein'
	,
	'Which roles shall all the new users get?'=>
	'Welche Rollen sollen die neuen Nutzer bekommen?'
	,
	'Subject'=>
	'Betreff'
	,
	'Last Online'=>
	'Zuletzt Online'
	,
	'Password forgotten?'=>
	'Passwort vergessen?'
	,
	'E-Mail'=>
	'E-Mail'
	,
	'Password'=>
	'Passwort'
	,
	'Change your password after first login:'=>
	'Ändere Dein Passwort nach dem ersten Login:'
	,
	'inviteSubject'=>
	'Dieser Blog wird Dein Leben verändern!'
	,
	'inviteText'=>
	'Hallo,<br/>es wurde ein Nutzerkonto unter folgender Adresse für Dich generiert: '
);