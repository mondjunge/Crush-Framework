CREATE TABLE IF NOT EXISTS `{dbprefix}user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nick` varchar(191) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(191) NOT NULL DEFAULT '',
  `roles` varchar(191) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `registered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastsid` varchar(32) NULL,
  `last_online` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(191) NOT NULL DEFAULT '',
  `description` text NOT NULL DEFAULT '', 
  `wallpaper` varchar(191) NOT NULL DEFAULT '',
  `friends` text NOT NULL DEFAULT '',
  `repeat_wallpaper` tinyint(1) NOT NULL DEFAULT '0',
  `background_color` VARCHAR(9) DEFAULT '',
  PRIMARY KEY (`id`), UNIQUE (`nick`)
);