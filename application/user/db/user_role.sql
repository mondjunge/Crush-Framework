CREATE TABLE IF NOT EXISTS `{dbprefix}user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(191) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`), UNIQUE (`name`)
);

INSERT INTO `{dbprefix}user_role` (`id` ,`name` ,`description`)
VALUES (1 , 'Superuser', 'Superusers can access all functions in this system. This role is not deleteable! ');

INSERT INTO `{dbprefix}user_role` (`id` ,`name` ,`description`)
VALUES (2 , 'User', 'Users can access non administrative functions. ');