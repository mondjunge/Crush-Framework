IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = '{dbname}' 
                 AND  TABLE_NAME = '{dbprefix}role'))
BEGIN
    RENAME TABLE `{dbprefix}role` TO `{dbprefix}user_role`;
END