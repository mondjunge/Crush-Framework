/**
 * user
 */

use_package('modules');
modules.user = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    this.init = function () {
        handleRemoveImage();
        replaceTextarea();
        handleTableSorter();
    };
    var handleTableSorter = function() {
        $("#userTable").tablesorter({widthFixed: true, widgets: ['zebra']});
        $("#roleTable").tablesorter({widthFixed: true, widgets: ['zebra']});
    };
    var replaceTextarea = function () {
        //alert($("textarea[name='description']").length);
        if ($("textarea[name='description']").length !== 0) {
            
            CKEDITOR.replace('description', {
                customConfig: relativeUrl + 'javascript/ck_config/tims_config.js',
                uiColor: '#eeeeee',
                toolbar: 'Default',
                height: '200px;'
            });
        }

    };
    var handleRemoveImage = function () {
        $('.removeImage').on("click",function () {
            $('#image').val(relativeUrl+'theme/'+siteStyle+'/images/profil.png');
            $('img.imagePreview').attr('src', relativeUrl+'theme/'+siteStyle+'/images/profil.png');
            return false;
        });
        $('.removeWallpaper').on("click",function () {
            $('#wallpaper').val('');
            return false;
        });

    };
    this.selectNewUserImage = function () {
        //alert('Image selected');
        //alert(modules.files.fileSelectElement.val());

        $('img.imagePreview').attr('src', modules.files.fileSelectElement.data("path"));
        $('#image').val(modules.files.fileSelectElement.data("path"));
    };
    this.selectNewWallpaper = function () {
         $('#wallpaper').val(modules.files.fileSelectElement.data("path"));
    };
//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


}