function deleteCheckUser(){
    var confirmText;
    if(language == 'gb')
        confirmText = 'Do you really want to delete this user? You can not undo this action!';
        
    if(language == 'de')
        confirmText = 'Soll der User wirklich gelöscht werden? Der Vorgang kann nicht rückgängig gemacht werden!';
        
        
    return confirm(confirmText);
}
function deleteCheckRole(){
    var confirmText;
    if(language == 'gb')
        confirmText = 'Do you really want to delete this role? You can not undo this action!';
        
    if(language == 'de')
        confirmText = 'Soll die Rolle wirklich gelöscht werden? Der Vorgang kann nicht rückgängig gemacht werden!';
        
        
    return confirm(confirmText);
}