<?php if (!$adminMode) : ?>
    <form class="pure-form pure-form-aligned" method="POST">
        <fieldset>
            <legend><?php echo $this->ts("Invite new users") ?></legend>
            <?php if ($adminMode) : ?>
                <div class="help"><?php echo $this->ts("Which roles shall the new user get?") ?></div>

                <span class="help"><?php echo $this->ts("Hold down Ctrl-Key to select multiple roles.") ?></span>
                <div class="pure-control-group">
                    <label for="roles" ><?php echo $this->ts("Roles") ?></label>

                    <select id="roles" name='roles[]' multiple='multiple' size='5' style='width:100%'>
                        <?php
                        foreach ($roles as $role) : $check = '';
                            if ($role['id'] == '2') {
                                $check = "selected='selected'";
                            }
                            ?>

                            <option value="<?php echo $role['id'] ?>" <?php echo $check ?>><?php echo $role['name'] ?> - <?php echo $role['description'] ?></option>
                        <?php endforeach; ?>
                    </select>

                </div>
            <?php endif; ?>
            <div class="help"><?php echo $this->ts("Insert E-Mail address of the new user.") ?></div>

            <div class="pure-control-group">
                <label for="email" ><?php echo $this->ts("Email") ?></label>
                <input id="email" name="email" type="text" size="26" autocomplete="OFF"/>
                <span class="help"><?php echo $this->ts("Invitation will be send to this address."); ?> </span>
            </div>

            <div class="pure-controls">
                <button type="submit" class="pure-button pure-button-primary" name="inviteUser" ><?php echo $this->ts("Invite new user") ?></button>
            </div>

        </fieldset>

    </form>
<?php endif; ?>
<?php if ($adminMode) : ?>
    <form class="pure-form pure-form-aligned" method="POST">
        <fieldset>
            <legend><?php echo $this->ts("Batch invite new users") ?></legend>
            <div class="pure-control-group">
                <label for="emails" ><?php echo $this->ts("E-mail adresses:") ?></label>
                <input id="emails" name="emails" type="text" size="50" autocomplete="OFF" placeholder="first@some.de;foo@bar.com;hello@world.de" />
                <span class="help"><?php echo $this->ts("Seperate e-mail adresses with ;."); ?> </span>
            </div>
            <div class="help"><?php echo $this->ts("Which roles shall all the new users get?") ?></div>

            <span class="help"><?php echo $this->ts("Hold down Ctrl-Key to select multiple roles.") ?></span>
            <div class="pure-control-group">

                <label for="roles" ><?php echo $this->ts("Roles") ?></label>
                <select id="roles" name='roles[]' multiple='multiple' size='5' style='width:100%'>
                    <?php
                    foreach ($roles as $role) : $check = '';
                        if ($role['id'] == '2') {
                            $check = "selected='selected'";
                        }
                        ?>

                        <option value="<?php echo $role['id'] ?>" <?php echo $check ?>><?php echo $role['name'] ?> - <?php echo $role['description'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="pure-control-group">
                <label for="subject" ><?php echo $this->ts("Subject") ?></label>
                <input class="pure-u-1" type="text" name="subject" placeholder="<?php echo $this->ts('Subject'); ?>" value="<?php echo $this->ts('inviteSubject');?>"/>
                <textarea id="text" name="text">
                    <?php echo $this->ts('inviteText');?>
                    <br/><br/>
                    <?php echo ($_SERVER['SERVER_PORT'] == '80' ? 'http' : 'https') . "://" . $_SERVER['SERVER_NAME']; ?>
                    <br/><br/>
                    <?php echo $this->ts('E-Mail');?> : {email}<br />
                    <?php echo $this->ts('Password');?> : {pass}<br />
                    <br/><br/>
                    <?php echo $this->ts('Change your password after first login:');?> {changePassLink}
                </textarea>
            </div>
            <div class="pure-controls">
                <button type="submit" class="pure-button pure-button-primary" name="batchInviteUsers" ><?php echo $this->ts("Batch invite new users") ?></button> 
            </div>
        </fieldset>
    </form>     
<?php endif; ?>