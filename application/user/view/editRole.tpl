<a class="pure-button" href="<?php echo $this->action("user/admin")?>" ><?php echo $this->ts("back")?></a>
<form class="pure-form pure-form-aligned" action="" method="post" enctype="multipart/form-data">
    <!--    <div class="form">-->
    <fieldset>
        <legend><?php echo $this->ts('Role') . " " . $role['name'] ?></legend>
        <!--        <div class="pure-control-group">
                    <label for="username" class="label_100"><?php echo $this->ts("Username") ?></label>
                    <input id="username" name="username" type="text" value="" size="26"/>
                </div>
                
                <p class="help"><?php echo $this->ts("Username changes will log you out automatically."); ?> </p>-->
        <input id="id" name="id" type="hidden" value="<?php echo $role['id'] ?>" />
        
        <div class="pure-control-group">
            <label for="name" ><?php echo $this->ts("Role") ?></label>
            <input id="name" name="name" type="text" size="26" value="<?php echo $role['name'] ?>" autocomplete="OFF"/>
        </div>

        <div class="pure-control-group">
            <label for="roleDescription" ><?php echo $this->ts("Description") ?></label>
            <textarea name="roleDescription" width="15" row="5"><?php echo $role['description'] ?></textarea>
               </div>

        <div class="pure-controls">
            <button type="submit" class="pure-button pure-button-primary" name="updateRole"><?php echo $this->ts("Save changes") ?></button> 
            <button type="submit" class="pure-button pure-button-error" name="deleteRole" onclick="return deleteCheckRole();" ><?php echo $this->ts("Delete role") ?></button>
        </div>
    </fieldset>
    <!--    </div>-->
</form>