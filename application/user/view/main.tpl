<h1><?php echo $this->ts('Users and Roles') ?></h1>
<div class="pure-g pure-g-r">
    <div class="pure-u-1">
        <h2><?php echo $this->ts('Users') ?></h2>
        <div class="tableWrapper">
            <table id="userTable" class="pure-table pure-table-striped" summary="user overview">
                <thead>
                    <tr>
                        <th><?php echo '#' ?></th>
                        <th><?php echo $this->ts('Username') ?></th>
                        <th><?php echo $this->ts('Email') ?></th>
                        <th><?php echo $this->ts('Roles') ?></th>
                        <th><?php echo $this->ts('Registration Time') ?></th>
                        <th><?php echo $this->ts('Last Online') ?></th>
                        <th><?php echo $this->ts('Active') ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <td><?php echo ($user['id']) ?></td>
                            <td><?php echo ($user['nick']) ?></td>
                            <td><?php echo $user['email'] ?></td>
                            <td><?php echo ($user['roles']) ?></td>
                            <td><?php echo ($user['registered']) ?></td>
                            <td><?php echo ($user['last_online']) ?></td>
                            <td><?php echo ($user['active']) ?></td>
                            <td>
                                <a class="pure-button" href="?editUser=<?php echo $user['id'] ?>" ><?php echo $this->ts("edit") ?></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>


                </tbody>
            </table>
        </div>
     <div id="userPager" class="pager">
<!--                    <form action="">
        <?php echo $this->image('first.png', 'first') ?>
        <?php echo $this->image('prev.png', 'prev') ?>
        
                        <input type="text" class="pagedisplay"/>
        <?php echo $this->image('next.png', 'next') ?>
        <?php echo $this->image('last.png', 'last') ?>
                        <select class="pagesize">
                            <option selected="selected"  value="10">10</option>
        
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option  value="40">40</option>
                        </select>
                    </form>-->
                </div>
        <div class="insertUser">
            <form class="pure-form pure-form-aligned" action="<?php echo $this->action('user/admin') ?>" method="post">
                <fieldset>
                    <legend><?php echo $this->ts('Insert new user') ?></legend>
                    <div class="pure-control-group">
                        <label for="username" class="label_100"><?php echo $this->ts("Username") ?></label>
                        <input id="username" name="username" type="text" value="" size="26" autocomplete="new-password"/>
                    </div>
                    <div class="pure-control-group">
                        <label for="password" class="label_100"><?php echo $this->ts("Passphrase") ?></label>
                        <input id="password" name="password" type="password" size="26"  autocomplete="new-password"/>
                    </div>
                    <div class="pure-control-group">
                        <label for="passrepeat" class="label_100"><?php echo $this->ts("Repeat") ?></label>
                        <input id="passrepeat" name="passrepeat" type="password" size="26"  autocomplete="new-password"/>
                    </div>
                    <div class="pure-control-group">
                        <label for="email" class="label_100"><?php echo $this->ts("Email") ?></label>
                        <input id="email" name="email" type="text" size="26" autocomplete="new-password"/>
                    </div>

                    <div class="pure-controls">
                        <button class="pure-button pure-button-primary" type="submit" name="insertUser" ><?php echo $this->ts("Add user") ?></button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="pure-u-1">
        <h2><?php echo $this->ts('Roles') ?></h2>
        <div class="tableWrapper">
            <table id="roleTable" class="pure-table pure-table-striped" summary="user overview">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $this->ts('Rolename') ?></th>
                        <th><?php echo $this->ts('Description') ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($roles as $role) : ?>
                        <tr>
                            <td><?php echo ($role['id']) ?></td>
                            <td><?php echo ($role['name']) ?></td>
                            <td><?php echo ($role['description']) ?></td>
                            <td>
                                <a class="pure-button" href="?editRole=<?php echo $role['id'] ?>" ><?php echo $this->ts("edit") ?></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>


                </tbody>
            </table>
        </div>
        <!--        <div id="rolePager" class="pager">
                    <form action="">
        <?php echo $this->image('first.png', 'first') ?>
        <?php echo $this->image('prev.png', 'prev') ?>
        
                        <input type="text" class="pagedisplay"/>
        <?php echo $this->image('next.png', 'next') ?>
        <?php echo $this->image('last.png', 'last') ?>
        
                        <select class="pagesize">
                            <option selected="selected"  value="10">10</option>
        
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option  value="40">40</option>
                        </select>
                    </form>
        
                </div>-->
        <div class="insertRole">

            <form class="pure-form pure-form-aligned" action="<?php echo $this->action('user/admin') ?>" method="post">
                <fieldset>
                    <legend><?php echo $this->ts('Create new Role') ?></legend>
                    <div class="pure-control-group">
                        <label for="rolename" class="divlabel"><?php echo $this->ts("Rolename") ?></label>
                        <input id="rolename" name="rolename" type="text" value="" size="26"/>
                    </div>
                    <div class="pure-control-group">
                        <label for="roleDescription" class="divlabel"><?php echo $this->ts("Description") ?></label>
                        <textarea id="roleDescription" name="roleDescription" rows="10" cols="25"></textarea>
                    </div>
                    <div class="pure-controls">
                        <button class="pure-button pure-button-primary" type="submit" name="createRole" ><?php echo $this->ts("Create Role") ?></button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div style="clear:both"></div>