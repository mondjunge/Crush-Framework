<?php if ($isAdmin) : ?>
<!--    <a class="pure-button" href="<?php echo $this->action("user/admin") ?>" ><?php echo $this->ts("back") ?></a>-->
<?php else: ?>

<?php endif; ?>
<form autocomplete="off" class="pure-form pure-form-aligned" action="<?php echo $this->action('user?m=updateAccount') ?>" method="post" enctype="multipart/form-data">
    <!--    <div class="form">-->
    <fieldset>
        <div class="pure-controls">
            <button type="submit" class="pure-button pure-button-primary" name="updateUser" ><?php echo $this->ts("Save changes") ?></button> 
            <?php if ($isAdmin) : ?>
                <button type="submit" class="pure-button pure-button-error" name="deleteUser" onclick="return deleteCheckUser();"><?php echo $this->ts("Delete account") ?></button> 
            <?php endif; ?>
        </div>
        <br/>
        <legend><?php echo $this->ts('Account') . " " . $user['nick'] ?></legend>

        <?php if ($isAdmin) : ?>
            <div class="pure-control-group">
                <label for="active" class="pure-checker">
                    <div class="checker">
                        <input id="active" class="checkbox" name="active" <?php echo $user['active'] ?> type="checkbox">
                        <div class="check-bg"></div>
                        <div class="checkmark">
                            <svg viewBox="0 0 100 100">
                            <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </div>
                    </div>
                    <span class="label"><?php echo $this->ts("User active") ?></span>
                </label>
                <div class="help help-margin warning"><?php echo $this->ts("Warning: Not a good idea!."); ?> </div>
            </div>
            <div class="pure-control-group">
                <label for="roles" ><?php echo $this->ts("Roles") ?></label>
                <select id="roles" name='roles[]' multiple='multiple' size='5' style='width:150px'>
                    <?php
                    foreach ($roles as $role) : $check = '';
                        if (in_array($role['id'], $user['roles'])) {
                            $check = "selected='selected'";
                        }
                        ?>

                        <option value="<?php echo $role['id'] ?>" <?php echo $check ?>><?php echo $role['name'] ?></option>
                    <?php endforeach; ?>
                </select>
                <span class="help"><?php echo $this->ts("Hold down Ctrl-Key to select multiple roles.") ?></span>
            </div>

        <?php else: ?>
            <div class="pure-control-group">
                <label for="roles" ><?php echo $this->ts("Roles") ?></label>

                <?php
                $userRolesString = "";
                foreach ($roles as $role) :
                    if (in_array($role['id'], $user['roles'])) {
                        if ($userRolesString == "") {
                            $userRolesString = $role['name'];
                        } else {
                            $userRolesString .= "," . $role['name'];
                        }
                    }
                    ?>

                <?php endforeach; ?>
                <input id="roles" name="roles" value="<?php echo $userRolesString ?>" readonly="readonly"/>     
            </div>
        <?php endif; ?>

        <input id="id" name="id" type="hidden" value="<?php echo $user['id'] ?>" />
        <input type="text" style="display:none">
        <input type="password" style="display:none">

        <div class="pure-control-group">
            <label for="imagePreview" ><?php echo $this->ts("User image") ?></label>
            <?php echo $this->image($user['image'], 'imagePreview pure-u-1-5') ?>
            <?php echo $this->image($user['image'], 'imagePreview image32') ?>
            <?php echo $this->image($user['image'], 'imagePreview image16') ?>
            <br/>
        </div>

        <div class="pure-control-group">
            <label for="image" ><?php echo $this->ts("Change user image") ?></label>
            <input id="image" class="" type="text" name="image" readonly="" value="<?php if ($user['image'] != "profil.png") echo $user['image']; ?>" />
            <button id="selectImageButton" class="pure-button pure-button-xsmall fileSelect" data-type="image" data-subdir="profil" data-callback="modules.user.selectNewUserImage" type="submit" ><?php echo $this->ts('click here to select an image'); ?></button>
            <a class="pure-button pure-button-xsmall pure-button-secondary removeImage" href="#"><?php echo $this->ts('remove image'); ?></a>
<!--            <input class="pure-button" id="image" name="image" type="file"/>-->
        </div>

        <div class="pure-control-group">
            <label for="nick" ><?php echo $this->ts("Nickname") ?></label>
            <input id="nick" name="nick" type="text" size="26" value="<?php echo $user['nick'] ?>" autocomplete="OFF"/>
            <span class="help"><?php echo $this->ts("Username changes will log you out automatically."); ?></span>
        </div>

        <div class="pure-control-group">
            <label for="email" ><?php echo $this->ts("Email") ?></label>
            <input id="email" name="email" type="text" size="26" value="<?php echo $user['email'] ?>" autocomplete="OFF"/>
            <span class="help"><?php echo $this->ts("You will get notifications on this address."); ?> </span>
        </div>


        <div class="pure-control-group">
            <label for="wallpaper" ><?php echo $this->ts("Change wallpaper") ?></label>
            <input id="wallpaper" class="" autocomplete="off"  type="text" name="wallpaper" readonly="" value="<?php echo $user['wallpaper']; ?>" />
            <button id="selectWallpaperButton" class="pure-button pure-button-xsmall fileSelect" data-type="image" data-subdir="wallpaper" data-callback="modules.user.selectNewWallpaper" type="submit" ><?php echo $this->ts('click here to select an image'); ?></button>
            <a class="pure-button pure-button-xsmall pure-button-secondary removeWallpaper" href="#"><?php echo $this->ts('remove wallpaper'); ?></a>

        </div>
        <div class="pure-control-group">
            <label for="repeat_wallpaper" ><?php echo $this->ts("Repeat wallpaper") ?></label>
            <input id="repeat_wallpaper" name="repeat_wallpaper" type="checkbox" <?php echo $user['repeat_wallpaper'] ?>/>
        </div>
        <?php if (isset($isProfilpageActive) && $isProfilpageActive) : ?>
            <div class="pure-control-group">
                <label class="pure-top-label" for="description"><?php echo $this->ts('Description'); ?></label>


                <div class="" style="display: inline-block;">
                    <textarea id="description"  name="description" ><?php echo $user['description'] ?></textarea>
                    <span class="help"><?php echo $this->ts("The desciption is going to show up on top of your profilepage.") ?></span>
                </div>


            </div>
        <?php endif; ?>
        <legend><?php echo $this->ts('Change Password') ?></legend>

        <div class="pure-control-group">
            <label for="old_password" ><?php echo $this->ts("Old passphrase") ?></label>
            <input id="old_password" name="old_password" type="password" size="26" maxlength="64" autocomplete="OFF"/>
            <a href="<?php echo $this->action('login/retreivePassword'); ?>" ><?php echo $this->ts("Password forgotten?") ?></a>
        </div>

        <div class="pure-control-group">
            <label for="new_password" ><?php echo $this->ts("New passphrase") ?></label>
            <input id="new_password" name="new_password" type="password" size="26" maxlength="64" autocomplete="OFF"/>
        </div>

        <div class="pure-control-group">
            <label for="new_passrepeat" ><?php echo $this->ts("Repeat new passphrase") ?></label>
            <input id="new_passrepeat" name="new_passrepeat" type="password" size="26" maxlength="64" autocomplete="OFF"/>
        </div>

        <div class="pure-controls">
            <button type="submit" class="pure-button pure-button-primary" name="updateUser" ><?php echo $this->ts("Save changes") ?></button> 
            <?php if ($isAdmin) : ?>
                <button type="submit" class="pure-button pure-button-error" name="deleteUser" onclick="return deleteCheckUser();"><?php echo $this->ts("Delete account") ?></button> 
            <?php endif; ?>
        </div>
    </fieldset>
    <!--    </div>-->
</form>