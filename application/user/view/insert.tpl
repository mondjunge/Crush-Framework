<h3><?php echo$this->ts('Insert new user')?></h3>
<form action="<?php echo$this->action()?>user" method="post">
    <div class="form">
        <p>
            <label for="username" class="divlabel"><?php echo $this->ts("Username") ?></label>
            <input id="username" name="username" type="text" value="" size="26"/>
        </p>
        <p>
            <label for="password" class="divlabel"><?php echo $this->ts("Passphrase") ?></label>
            <input id="password" name="password" type="password" size="26"/>
        </p>
        <p>
            <label for="passrepeat" class="divlabel"><?php echo $this->ts("Repeat") ?></label>
            <input id="passrepeat" name="passrepeat" type="password" size="26"/>
        </p>
        <p>
            <label for="email" class="divlabel"><?php echo $this->ts("Email") ?></label>
            <input id="email" name="email" type="text" size="26"/>
        </p>
       
        <p>
            <button type="submit" name="insertUser" ><?php echo $this->ts("Add user") ?></button>
        </p>
    </div>
</form>