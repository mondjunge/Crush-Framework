<!-- IS NOT USED ANYMORE; TAKE ALOOK AT ACCOUNT!!! -->
<h2><?php echo $this->ts('Edit user') . " " . $user['nick'] ?></h2>
<form action="<?php echo $this->action('user/admin') ?>" method="post">
    <div class="form">
        <input id="id" name="id" type="hidden" value="<?php echo $user['id'] ?>" />
        <p>
            <label for="active" class="label_200"><?php echo $this->ts("User active") ?></label>
            <input id="active" name="active" type="checkbox" <?php echo $user['active'] ?>/>
        </p>
        <p>
            <label for="email" class="label_200"><?php echo $this->ts("Email") ?></label>
            <input id="email" name="email" type="text" size="26" value="<?php echo $user['email'] ?>" autocomplete="OFF"/>
        </p>

        <p>
            <label for="roles" class="label_200"><?php echo $this->ts("Roles") ?></label>
            <select id="roles" name='roles[]' multiple='multiple' size='5' style='width:150px'>
                <?php
                foreach ($roles as $role) : $check = '';
                    if (in_array($role['id'], $user['roles'])) {
                        $check = "selected='selected'";
                    }
                    ?>

                    <option value="<?php echo $role['id'] ?>" <?php echo $check ?>><?php echo $role['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </p>


        <h3><?php echo $this->ts('Change Password') ?></h3>

        <p>
            <label for="new_password" class="label_200"><?php echo $this->ts("New passphrase") ?></label>
            <input id="new_password" name="new_password" type="password" size="26" autocomplete="OFF"/>
        </p>
        <p>
            <label for="new_passrepeat" class="label_200"><?php echo $this->ts("Repeat new passphrase") ?></label>
            <input id="new_passrepeat" name="new_passrepeat" type="password" size="26" autocomplete="OFF"/>
        </p>
        <p>
            <label for="old_password" class="label_200"><?php echo $this->ts("Old passphrase") ?></label>
            <input id="old_password" name="old_password" type="password" size="26" autocomplete="OFF"/>
        </p>


        <p>
            <button type="submit" name="updateUser" ><?php echo $this->ts("Save changes") ?></button> 
            <button type="submit" name="deleteUser" onclick="return deleteCheckUser();" ><?php echo $this->ts("Delete user")?></button> 
        </p>
    </div>
</form>