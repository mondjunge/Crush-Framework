<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class User extends AppController {

    public function index() {
        if(!$this->isLoggedIn()){
            $this->jumpToLogin();
            return;
        }

        if (filter_has_var(INPUT_GET, 'm')) {
            $mode = filter_input(INPUT_GET, 'm');
        } else {
            $mode = '';
        }

        // let User delete himself and admin delete anyone.

        if ((filter_has_var(INPUT_POST, 'deleteUser') && filter_input(INPUT_POST, 'id') == Session::get("uid")) || (filter_has_var(INPUT_POST, 'deleteUser') && $this->checkSiteRights("user/admin"))) {

            $this->deleteUser();
            if (filter_input(INPUT_POST, 'id') == Session::get("uid")) {
                Link::jumpTo("login?logout");
            } else {
                Link::jumpTo("user/admin");
            }
        }

        if (!$this->isLoggedIn()) {
            return;
        }

        switch ($mode) {
            case 'updateAccount':
                $this->selfUpdateUser();
                //$this->logout();
                //$this->setSystemMessage($this->ts("You need to log in again."));
                //$this->jumpToLogin();
                Link::jumpToLast();
                break;
            case 'getOnlineNickNames':
                $this->getOnlineNickNames();
                break;
            case 'registerUser':
                $this->registerUser();
                break;
        }


        if (filter_has_var(INPUT_GET, 'addFriend')) {
            $this->standalone = true;
            $this->addFriend(filter_input(INPUT_GET, 'addFriend', FILTER_UNSAFE_RAW));
            echo "true";
            return;
        }
        if (filter_has_var(INPUT_GET, 'removeFriend')) {
            $this->standalone = true;
            $this->removeFriend(filter_input(INPUT_GET, 'removeFriend', FILTER_UNSAFE_RAW));
            echo "true";
            return;
        }
    }

    public function admin() {
        $this->autoCheckRights();


        if (filter_has_var(INPUT_POST, 'deleteUser')) {
            $this->deleteUser();
        } elseif (filter_has_var(INPUT_POST, 'updateUser')) {
            $this->updateUser();
        } elseif (filter_has_var(INPUT_POST, 'insertUser')) {
            $this->insertUser();
        } elseif (filter_has_var(INPUT_POST, 'createRole')) {
            $this->insertRole();
        } elseif (filter_has_var(INPUT_GET, 'editUser')) {
            $this->includeCkeditor();
            $this->includeJs(Config::get('applicationDirectory') . '/files/js/.files.js');
            $this->editUser();
            return;
        } elseif (filter_has_var(INPUT_GET, 'editRole')) {
            $this->editRole();
            return;
        }
        $this->main();
    }

    /**
     * @Module(type="a" , description="" , hide="" , role_need=" )
     */
    public function adminOwnData() {
        $this->autoCheckRights();

        $this->includeCkeditor();
        $this->includeJs(Config::get('applicationDirectory') . '/files/js/.files.js');

        $user = $this->select('user', '*', "id='" . Session::get('uid') . "'");
        $data['user'] = $user[0];
        if ($data['user']['active'] == '1') {
            $data['user']['active'] = "checked='checked'";
        } else {
            $data['user']['active'] = "";
        }
        if ($data['user']['repeat_wallpaper'] == '1') {
            $data['user']['repeat_wallpaper'] = "checked='checked'";
        } else {
            $data['user']['repeat_wallpaper'] = "";
        }

        $data['user']['roles'] = explode(',', $data['user']['roles']);
        if ($data['user']['image'] == "" || $data['user']['image'] == "profil.png") {
            $data['user']['image'] = "profil.png";
        } else {
            $data['user']['image'] = Config::get('relativeUrl') . $data['user']['image'];
        }
        if ($data['user']['wallpaper'] != "") {
            $data['user']['wallpaper'] = Config::get('relativeUrl') . $data['user']['wallpaper'];
        } else {
            $data['user']['wallpaper'] = "";
        }
        $data['isProfilpageActive'] = $this->isModuleActive("profilpage");
        $data['roles'] = $this->getRoles('50');
        $data['isAdmin'] = $this->checkSiteRights("user/admin");
        $this->view('account', $data);
    }

    public function adminInviteUser() {
        $this->autoCheckRights();
        $this->includeCkeditor();
        $data['adminMode'] = $this->checkSiteRights('user/admin');
        $this->includeCkeditor('javascript/ck_config/ck_newsletter.js');

        if (filter_has_var(INPUT_POST, 'batchInviteUsers')) {
            $this->batchInviteUsers();
        }

        if (filter_has_var(INPUT_POST, 'inviteUser')) {

            $splitEmail = explode('@', filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL));
            $nick = $splitEmail[0];
            $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

            if ($email == '') {
                $data['roles'] = $this->getRoles();
                $this->view('inviteUser', $data);
                return;
            }

            $rs = $this->select('user', '*', "email='$email'");
            if (sizeof($rs) > 0) {
                $this->setErrorMessage("A user with this e-mail address is already registered.");
            } else {

                $active = 1;
                if ($data['adminMode'] && !filter_has_var(INPUT_POST, 'roles')) {
                    $this->setErrorMessage("You have not selected any role. The User will have no rights to do anything. Please repeat.");
                } else {


                    if ($data['adminMode']) {
                        $roles = join(',', filter_input(INPUT_POST, 'roles', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));
                    } else {
                        $roles = "2";
                    }



                    $pass = $this->generateRandomString(6);

                    $umlautUrl = str_replace("xn--", "ü", Config::get('rootUrl'));
                    $umlautUrl = str_replace("-jva", "", $umlautUrl);

                    $subject = $this->ts('You have been invited to join the community on $1');
                    $subject = str_replace('$1', $umlautUrl, $subject);

                    $message = $this->ts("Hello $1,");
                    $message .= "<br/><br/>\n\r\n\r";
                    $message .= $this->ts("you have been invited to join the community by $2.");
                    $message .= "<br/><br/>\n\r\n\r";
                    $message .= $this->ts("Login with your e-mail address under $3, and be a part of the community! Your friends are already waiting for you!");
                    $message .= "<br/><br/>\n\r\n\r";
                    $message .= $this->ts("Your temporary password is: $4");
                    $message .= "<br/><br/>\n\r\n\r";
                    $message .= $this->ts("Please change your temporary password under $6 as soon as you can.");
                    $message .= "<br/>\n\r";
                    $message .= $this->ts("If you have any questions regarding this e-mail, contact $5 for more information.");

                    $message = str_replace('$1', $nick, $message);
                    $message = str_replace('$2', Session::get('unick'), $message);
                    $message = str_replace('$3', "<a href='" . Config::get('rootUrl') . Config::get('reLangUrl') . "login'>" . $umlautUrl . Config::get('reLangUrl') . "login</a>", $message);
                    $message = str_replace('$4', $pass, $message);
                    $message = str_replace('$6', "<a href='" . Config::get('rootUrl') . Config::get('reLangUrl') . "user/adminOwnData'>" . $umlautUrl . Config::get('reLangUrl') . "user/adminOwnData</a>", $message);


                    $fromEmail = $this->select('user', 'email', "id='" . Session::get('uid') . "'");
                    if ($fromEmail[0]['email'] != '') {
                        $message = str_replace('$5', $fromEmail[0]['email'], $message);
                    } else {
                        $message = str_replace('$5', Config::get('noticeEmail'), $message);
                    }


                    if (!$this->mail_utf8($email, $subject, $message)) {
                        $this->setErrorMessage($this->ts('Something went wrong, while sending the e-mail. Please check the e-mail address.'));
                    } else {
                        $notice = str_replace('$1', $email, $this->ts("Invitation e-mail was send to $1:"));
                        $this->insertUserData($nick, $pass, $email, $active, $roles);
                        $this->setSystemMessage($notice);
                    }
                }
            }
        }
        if ($data['adminMode']) {
            $data['roles'] = $this->getRoles();
        }


        $this->view('inviteUser', $data);
    }

    private function addFriend($userId) {

        $rs = $this->selectPrepare('user', 'friends', "id=:id", array('id' => Session::get('uid')) );

        $csv = $this->addToCsvString($userId, $rs[0]['friends']);

        $fieldsValueArray = array(
            'friends' => $csv
        );
        $this->updatePrepare('user', $fieldsValueArray, "id=:id", array('id' => Session::get('uid')) );
    }

    private function removeFriend($userId) {

        $rs = $this->selectPrepare('user', 'friends', "id=:id", array('id' => Session::get('uid')) );

        $csv = $this->removeFromCsvString($userId, $rs[0]['friends']);

        $fieldsValueArray = array(
            'friends' => $csv
        );
        $this->updatePrepare('user', $fieldsValueArray, "id=:id", array('id' => Session::get('uid')) );
    }

    private function batchInviteUsers() {
        $this->includeCkeditor('javascript/ck_config/ck_newsletter.js');
        $emails = filter_input(INPUT_POST, 'emails', FILTER_UNSAFE_RAW);
        $subject = filter_input(INPUT_POST, 'subject', FILTER_UNSAFE_RAW);
        $message = filter_input(INPUT_POST, 'text', FILTER_UNSAFE_RAW);
        $emArray = explode(";", $emails);
        if ($this->checkSiteRights('user/admin')) {
            $roles = join(',', filter_input(INPUT_POST, 'roles', FILTER_UNSAFE_RAW, FILTER_REQUIRE_ARRAY));
        } else {
            $roles = "2";
        }

        if ($emails == '') {
            $data['roles'] = $this->getRoles();
            $this->view('inviteUser', $data);
            return;
        }
        $notice = "";
        foreach ($emArray as $e) {
            $e = trim($e);
            $splitEmail = explode('@', $e);
            $nick = $splitEmail[0];
            $email = $e;
            $pass = $this->generateRandomString(6);
            $changePassString = "<a href='" . Config::get('rootUrl') . Config::get('reLangUrl') . "user/adminOwnData'>" . Config::get('rootUrl') . Config::get('reLangUrl') . "user/adminOwnData</a>";
            
            $message = str_replace('<br/>','<br/>\n\r',$message);
            $message2 = str_replace('{pass}', $pass, $message);
            $message3 = str_replace('{email}', $email, $message2);
            $message4 = str_replace('{changePassLink}', $changePassString, $message3);

            //$message .= "<br/>Ändere Dein Passwort und Nutzernamen unter " . $changePassString;

            if (!$this->mail_utf8($email, $subject, $message4)) {
                $this->setErrorMessage($this->ts('Something went wrong, while sending the e-mail. Please check the e-mail address.'));
            } else {
                $notice = str_replace('$1', $email, $this->ts("Invitation e-mail was send to $1:") . "<br/>");
                $this->insertUserData($nick, $pass, $email, '1', '2');
                $this->setSystemMessage($notice);
            }
        }
    }

    private function showFriends() {
        
    }

    private function showFollowers() {
        
    }

    /**
     * 
     */
    private function getOnlineNickNames() {
        $this->standalone = true;


        if (!$this->isLoggedIn()) {
            echo $this->ts("You are not logged in.");
            return;
        }
        //$this->autoCheckRights();

        $this->updateOnlineTimestamp();

        $onlineUsersRs = $this->select('user', '*', "last_online > '" . date("Y-m-d H:i:s", mktime(date("H"), date("i") - 3)) . "'", "last_online DESC");
        $html = "";
        foreach ($onlineUsersRs as $ou) {
            $data['user'] = $ou;
            $html .= $this->fetch('onlineUser', $data);
        }
        echo $this->ts("Currently online") . ": " . $html;
    }

    private function main() {
        $aConfig = Config::getConfig();
        $data['conf'] = $aConfig;
        $data['insertUserHtml'] = $this->fetch('insert', $data);
        $users = $this->select('user', '*', '1=1', 'id asc');

        foreach ($users as $user) {
            if (strlen($user['nick']) > 13) {
                $user['nick'] = substr($user['nick'], 0, 10) . "...";
            }
            if (strlen($user['email']) > 18) {
                $user['email'] = substr($user['email'], 0, 15) . "...";
            }
            $data['users'][] = $user;
        }

//        $roles = $this->select('role', '*', '1=1', 'id asc');
//
//        foreach ($roles as $role) {
//            if (strlen($role['description']) > 53) {
//                $role['description'] = substr($role['description'], 0, 50) . "...";
//            }
//            $data['roles'][] = $role;
//        }
        $data['roles'] = $this->getRoles('50');

        $this->view('main', $data);
    }

    private function deleteUser() {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

        if ($this->delete('user', "id='$id'")) {
            $this->setSystemMessage($this->ts('User deleted successfully'));
        } else {
            $this->setErrorMessage($this->ts('Cannot delete User'));
        }
    }

    private function updateUser() {

        /**
         * TODO:
         * - fix possible to long password attack scenario. 
         */
        $currentUid = Session::get('uid');
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $nick = filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW);
        $image = filter_input(INPUT_POST, 'image', FILTER_SANITIZE_URL);
        $wallpaper = filter_input(INPUT_POST, 'wallpaper', FILTER_SANITIZE_URL);

        $description = filter_input(INPUT_POST, 'description', FILTER_UNSAFE_RAW);

        if ($currentUid != $id) {
            if (!$this->checkSiteRights("user/admin")) {
                print_r($currentUid);
                die; // lol
            }
        }

        /**
         * active and role changes only from super admins!! 
         */
        if ($this->checkSiteRights("user/admin")) {
            $active = '0';
            $roles = '0';
            if (filter_has_var(INPUT_POST, 'active')) {
                $active = '1';
            }

            if (filter_has_var(INPUT_POST, 'roles')) {
                $roles = join(',', filter_input(INPUT_POST, 'roles', FILTER_UNSAFE_RAW, FILTER_REQUIRE_ARRAY));
            }

            $fieldsValueArray = array(
                'active' => $active,
                'roles' => $roles
            );
        }

        if (Config::get('relativeUrl') != '/') {
            $userImagePath = str_replace(Config::get('relativeUrl'), '', $image); //$this->uploadUserImage($id);

            $wallpaperImagePath = str_replace(Config::get('relativeUrl'), '', $wallpaper); //$this->uploadUserImage($id);
        }
        if (Config::get('relativeUrl') == '/') {
            if ($image != '') {
                $userImagePath = substr($image, 1);
            } else {
                $userImagePath = '';
            }
            if ($wallpaper != '') {
                $wallpaperImagePath = substr($wallpaper, 1);
            } else {
                $wallpaper = '';
            }
        }
        if ($userImagePath !== false) {
            $fieldsValueArray['image'] = $userImagePath;
        }
        if ($id == $currentUid) {
            if ($userImagePath == "") {
                Session::set('uimage', "theme/" . Config::get("siteStyle") . "/images/profil.png", true);
            } else {
                Session::set('uimage', $userImagePath, true);
            }
        }
        if ($wallpaperImagePath !== false) {
            $fieldsValueArray['wallpaper'] = $wallpaperImagePath;
            if ($id == $currentUid) {
                Session::set('uwallpaper', $wallpaperImagePath, true);
            }
        }
        $repeat_wallpaper = '0';
        if (filter_has_var(INPUT_POST, 'repeat_wallpaper')) {
            $repeat_wallpaper = '1';
        }
        Session::set('urepeat_wallpaper', $repeat_wallpaper, true);

        $fieldsValueArray['repeat_wallpaper'] = $repeat_wallpaper;

        $fieldsValueArray['email'] = $email;
        $fieldsValueArray['nick'] = $nick;
        $fieldsValueArray['description'] = $description == null ? '': $description;


        if (filter_input(INPUT_POST, 'new_password', FILTER_UNSAFE_RAW) != '' && filter_input(INPUT_POST, 'new_passrepeat', FILTER_UNSAFE_RAW) != '' && filter_input(INPUT_POST, 'old_password', FILTER_UNSAFE_RAW) != '' && filter_input(INPUT_POST, 'new_password', FILTER_UNSAFE_RAW) == filter_input(INPUT_POST, 'new_passrepeat', FILTER_UNSAFE_RAW)) {


            $rs = $this->select('user', 'password', "id=$id");
            $dbhash = $rs[0]['password'];


            if (strlen($dbhash) == 64) {
                /*
                 *  fallback code for old  password storage without salt
                 */
                $oldhash = hash('sha256', filter_input(INPUT_POST, 'old_password', FILTER_UNSAFE_RAW));
            } else {
                $pwdSalt = substr($dbhash, 0, 64);
                $oldhash = $pwdSalt . hash('sha512', $pwdSalt . filter_input(INPUT_POST, 'old_password', FILTER_UNSAFE_RAW));
            }

            if ($oldhash != $dbhash) {
                $this->setErrorMessage($this->ts('Wrong password.'));
            } else {
                $randomSalt = $this->generateRandomString();
                $newhash = $randomSalt . hash('sha512', $randomSalt . filter_input(INPUT_POST, 'new_password', FILTER_UNSAFE_RAW));

                $fieldsValueArray['password'] = $newhash;
                if ($this->updatePrepare('user', $fieldsValueArray,  "id=:id", array('id' => $id))) {
                    $this->setSystemMessage($this->ts('Password & User updated successfully'));
                } else {
                    $this->setErrorMessage($this->ts('Something went wrong while updating the user'));
                }
            }
        } else {

            if ($this->updatePrepare('user', $fieldsValueArray,  "id=:id", array('id' => $id) )) {
                $this->setSystemMessage($this->ts('User updated successfully'));
            } else {
                $this->setErrorMessage($this->ts('Something went wrong while updating the user'));
            }
        }
    }

    private function editUser() {
        $id = filter_input(INPUT_GET, 'editUser', FILTER_SANITIZE_NUMBER_INT);

        $user = $this->select('user', '*', "id='" . $id . "'");
        $data['user'] = $user[0];
        if ($data['user']['active'] == '1') {
            $data['user']['active'] = "checked='checked'";
        } else {
            $data['user']['active'] = "";
        }
        if ($data['user']['repeat_wallpaper'] == '1') {
            $data['user']['repeat_wallpaper'] = "checked='checked'";
        } else {
            $data['user']['repeat_wallpaper'] = "";
        }

        $data['user']['roles'] = explode(',', $data['user']['roles']);
        if ($data['user']['image'] == "" || $data['user']['image'] == "profil.png") {
            $data['user']['image'] = "profil.png";
        } else {
            $data['user']['image'] = Config::get('relativeUrl') . $data['user']['image'];
        }

        $data['isProfilpageActive'] = $this->isModuleActive("profilpage");
        $data['roles'] = $this->getRoles('50');
        $data['isAdmin'] = $this->checkSiteRights("user/admin");

        $this->view('account', $data);
    }

    private function editRole() {

        if (filter_has_var(INPUT_POST, 'updateRole')) {
            $this->updateRole();
        }


        $id = filter_input(INPUT_GET, 'editRole', FILTER_SANITIZE_NUMBER_INT);
        $role = $this->select('user_role', '*', "id='$id'", 'id asc', '1');
        $data['role'] = $role[0];
        $this->view('editRole', $data);
    }

    private function updateRole() {
        $name = filter_input(INPUT_POST, 'name', FILTER_UNSAFE_RAW);
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $description = filter_input(INPUT_POST, 'roleDescription', FILTER_UNSAFE_RAW);
        $fieldsValueArray = array(
            'name' => "$name",
            'description' => "$description"
        );
        if (!$this->updatePrepare('user_role', $fieldsValueArray,  "id=:id", array('id' => $id))) {
            $this->setErrorMessage($this->ts("Cannot update role!"));
        } else {
            $this->setSystemMessage($this->ts("Role updated successfully."));
        }
    }

//    private function viewInsertUser() {
//        $aConfig = Config::getConfig();
//        $data['conf'] = $aConfig;
//        $this->view('insert', $data);
//    }    
//    
//    filter_input\([A-Z_]*,[a-zA-Z _']*\)


    private function insertUser() {

        $roles = '';
        if (filter_has_var(INPUT_POST, 'roles')) {
            $roles = join(',', filter_input(INPUT_POST, 'roles', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));
        }

        if (filter_input(INPUT_POST, 'username') == '' || strlen(filter_input(INPUT_POST, 'username')) < 3) {
            $this->setErrorMessage($this->ts("Nick have to be at least 3 characters long!"));
            return;
        }

        if (filter_input(INPUT_POST, 'password') == '' || strlen(filter_input(INPUT_POST, 'password')) < 5 || strlen(filter_input(INPUT_POST, 'password')) > 64) {
            $this->setErrorMessage($this->ts("Password have to be at least 5 characters long and not longer than 64 characters!"));
            return;
        }

        if (filter_input(INPUT_POST, 'password') != filter_input(INPUT_POST, 'passrepeat')) {
            $this->setErrorMessage($this->ts("Password does not match with the repetition."));
            return;
        }
        $newId = $this->insertUserData(filter_input(INPUT_POST, 'username', FILTER_UNSAFE_RAW), filter_input(INPUT_POST, 'password', FILTER_UNSAFE_RAW), filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL), '1', $roles);
        if (!$newId) {
            $this->setErrorMessage($this->ts("Cannot register user!"));
        } else {
            $this->setSystemMessage($this->ts("User registered successfully!"));
        }
    }

    private function insertUserData($nick, $pass, $email, $active, $roles = '0') {

        $randomSalt = $this->generateRandomString();
        $fieldsValueArray = array(
            'nick' => $nick,
            'password' =>  $randomSalt . hash('sha512', $randomSalt . $pass) ,
            'email' => $email,
            'active' => $active,
            'roles' => $roles
        );
        $newUserId = $this->insert('user', $fieldsValueArray);

        if ($newUserId === FALSE) {
            return $newUserId;
        } else {

            // create notification settings
            $rs = $this->select('notifications_settings', '*', "user_id='$newUserId'");

            if ($rs === FALSE || sizeof($rs) == 0) {
                $fieldsValueArray = array();
                $fieldsValueArray['user_id'] = $newUserId;
                $this->insert('notifications_settings', $fieldsValueArray);
            }

            return $newUserId;
        }
    }

    private function insertRole() {
        if (!filter_has_var(INPUT_POST, 'rolename') || filter_input(INPUT_POST, 'rolename') == '') {
            $this->setErrorMessage($this->ts("Cannot create role without name!"));
            return;
        }

        $name = filter_input(INPUT_POST, 'rolename', FILTER_UNSAFE_RAW);
        $description = filter_input(INPUT_POST, 'roleDescription', FILTER_UNSAFE_RAW);
        $newId = $this->insertRoleData($name, $description);
        if (!$newId) {
            $this->setErrorMessage($this->ts("Cannot create role!"));
        } else {
            $this->setSystemMessage($this->ts("Role created successfully!"));
        }
    }

    private function insertRoleData($name, $description) {
        $fieldsValueArray = array(
            'name' => $name,
            'description' => $description
        );
        return $this->insert('user_role', $fieldsValueArray);
    }

    private function selfUpdateUser() {
        /**
         * TODO: Refactor
         * saves also other accounts, when in admin mode... what a mess.. :((( 
         */
        if (filter_input(INPUT_POST, 'id') != Session::get('uid') && $this->checkSiteRights("user/admin") === false) {
            $this->setErrorMessage($this->ts("This is not allowed!"));
        }

        if (filter_input(INPUT_POST, 'id') == Session::get('uid') && filter_input(INPUT_POST, 'nick') != Session::get('unick')) {
            $rs = $this->select('user', '*', "nick='" . filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW) . "'");
            if (sizeof($rs) > 0) {
                $this->setErrorMessage($this->ts('Nick is already taken from another user. Sorry.'));
                return;
            }
        }


        //$this->uploadBg();
        $this->updateUser();
        /**
         * logout if user updates his or her nickname
         */
        if (filter_input(INPUT_POST, 'id') == Session::get('uid') && filter_input(INPUT_POST, 'nick') != Session::get('unick')) {

            $this->logout();
            $this->jumpToLogin();
        }
    }

    private function registerUser() {

        $error = false;
        if (Input::check(filter_input(INPUT_POST, 'email'), 'email') == false || strlen(filter_input(INPUT_POST, 'email')) < 6) {
            $this->setErrorMessage($this->ts("Not an valid E-Mail address!"));
            $error = true;
        }

        if (filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW) == '' || strlen(filter_input(INPUT_POST, 'nick')) < 3) {
            $this->setErrorMessage($this->ts("Nick have to be at least 3 characters long!"));
            $error = true;
        }

        $user = $this->select('user', '*', "email='" . filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL) . "'");
        if (sizeof($user) > 0) {
            $this->setErrorMessage($this->ts("This E-Mail address is already registered!"));
            $error = true;
        }

        if (filter_input(INPUT_POST, 'pass') == '' || strlen(filter_input(INPUT_POST, 'pass')) < 5) {
            $this->setErrorMessage($this->ts("Password have to be at least 5 characters long!"));
            $error = true;
        }

        if (filter_input(INPUT_POST, 'pass') != filter_input(INPUT_POST, 'passrepeat')) {
            $this->setErrorMessage($this->ts("Password does not match with the repetition."));
            $error = true;
        }

        if (!$error) {
            $newId = $this->insertUserData(filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW), filter_input(INPUT_POST, 'pass', FILTER_UNSAFE_RAW), filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL), '1', '2');
            if (!$newId) {
                $this->setErrorMessage($this->ts("Cannot register user!"));
            } else {
                $this->setSystemMessage($this->ts("User registered successfully!"));
                if ($this->loginNow()) {
                    $this->setSystemMessage($this->ts("Logged in successfully!"));
                } else {
                    $this->setErrorMessage($this->ts("Wrong Username or Password"));
                }
            }
        }
        Link::jumpToLast();
    }

    /**
     * adds a userId o other String to a csv or empty String
     * user for voting and adding friends
     * @param type $userId
     * @param type $csvString
     * @return type 
     */
    private function addToCsvString($userId, $csvString) {
        if ($csvString == "") {
            $fString = $userId;
        } else {
            $fArr = explode(",", $csvString);
            if (in_array($userId, $fArr)) {
                // userId is already in string
                return $csvString;
            }
            array_push($fArr, $userId);

            $fUnique = array_unique($fArr);

            $fString = implode(",", $fUnique);
        }
        return $fString;
    }

    /**
     * removes a user id or other string from a csv String
     * @param type $userId
     * @param type $csvString
     * @return type 
     */
    private function removeFromCsvString($userId, $csvString) {
        if ($csvString == "") {
            $fString = "";
        } else {
            $fArr = explode(",", $csvString);

            foreach ($fArr as $i => $v) {
                if ($v == $userId) {
                    array_splice($fArr, $i, 1);
                    //return $i;
                }
            }

            $fString = implode(",", $fArr);
        }

        return $fString;
    }

    private function uploadUserImage($id) {
        // only run if uploaded file exists
        if (isset($_FILES['image']) && is_file($_FILES['image']['tmp_name'])) {

            $target_dir = "upload/users/" . $id . "/images/profil"; //Session::get('uid');
            $target_path = $target_dir . "/";

            // create user dir, if it does not exist
            if (!is_dir($target_path)) {

                mkdir($target_path, 0777, true);
                chmod($target_path, 0777);
            }

            /**
             * phpThumb magic
             */
            $userfile_name = $_FILES['image']['name'];
            $userfile_tmp = $_FILES['image']['tmp_name'];
            $userfile_size = $_FILES['image']['size'];
            $userfile_type = $_FILES['image']['type'];

            $filename = basename($_FILES['image']['name']);
            $file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));

            $thumbnail_width = '300';
            $thumbnail_height = '300';

            //$target_file = $filename;
            $target_file_path = dirname(__FILE__) . '/../../' . $target_path . $filename;

            require_once(dirname(__FILE__) . '/../../extras/phpThumb-1.7.13/phpthumb.class.php');

            /**
             *  create phpThumb object
             */
            $phpThumb = new phpThumb();

            $phpThumb->setSourceData(file_get_contents($userfile_tmp));

            if ($thumbnail_width != '')
                $phpThumb->setParameter('w', $thumbnail_width);
            if ($thumbnail_height != '')
                $phpThumb->setParameter('h', $thumbnail_height);

            $phpThumb->setParameter('config_output_format', $file_ext);

            if ($phpThumb->GenerateThumbnail()) { // this line is VERY important, do not remove it!
                if ($phpThumb->RenderToFile($target_file_path)) {
                    // do something on success

                    return $target_path . $filename;
                } else {
                    // do something with debug/error messages
                    return false; //'Failed:<pre>' . implode("\n\n", $phpThumb->debugmessages) . '</pre>';
                }
                $phpThumb->purgeTempFiles();
            } else {
                // do something with debug/error messages
                return false; //echo 'Failed:<pre>' . $phpThumb->fatalerror . "\n\n" . implode("\n\n", $phpThumb->debugmessages) . '</pre>';
            }
        } else {
            return false;
        }
    }

    private function uploadBg() {
        // only run if uploaded file exists
        if (isset($_FILES['bg']) && is_file($_FILES['bg']['tmp_name'])) {

            $target_path = "upload/users/" . Session::get('uid') . "/";

            if (!is_dir($target_path)) {
                mkdir($target_path);
            }

            $target_path = "upload/users/" . Session::get('uid') . "/" . $_FILES['bg']['tmp_name'];
            if (is_file($target_path)) {
                if (!unlink($target_path)) {
                    $this->setErrorMessage($this->ts('Old file could not be deleted.'));
                }
            }
            if (move_uploaded_file($_FILES['bg']['tmp_name'], $target_path)) {
                $this->setSystemMessage($this->ts('File has been uploaded succesfully.'));
                // echo "The file has been uploaded";
            } else {
                $this->setErrorMessage($this->ts('File upload has gone wrong.'));
            }
        }
    }

}
