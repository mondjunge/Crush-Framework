TODO:
<ul>
    <li>DB Infos:</li>
    <li>version</li>
    <li>DB Kollation (utf8 or utf8mb4)</li>
    <li>DB Kollation Update (utf8 to utf8mb4)</li>
    <li>DatenBackup Tabellen downloaden/anlegen</li>
    <li>DatenBackup Tabellen hochladen/einspielen</li>
    <li>DatenBackup löschen</li>
    <li>Daten Tabelle löschen</li>
    <li>Tabelle dropen und neu aufbauen</li>
</ul>



<div>
    <form class="pure-form pure-form-aligned" method="POST" >
        Collation: <?php echo $dbCollation; ?>
        <button name="mb4updateDB" type="submit" class="pure-button" ><?php echo $this->ts('mb4updateDatabase'); ?></button>
    </form>
    <?php foreach ($tables as $t) : ?>
        <form class="pure-form pure-form-aligned" method="POST" >
            <input value="<?php echo $tablesCollations[$t]; ?>" readonly="readonly" name="collation" />
            <input readonly="readonly" value="<?php echo $t ?>" name="table" />
            <button name="dumpTable" type="submit" class="pure-button" ><?php echo $this->ts('dump'); ?></button>
            <button name="mb4updateTable" type="submit" class="pure-button" ><?php echo $this->ts('mb4updateTable'); ?></button>
        </form>
    <?php endforeach; ?>
</div>

