<?php

/**
 *  Copyright © Tim Wahrendorff 29.08.2019
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Dbmanager extends AppController {

    public function index() {
        if (filter_has_var(INPUT_POST, 'dumpTable')) {
            $this->dumpTableData();
            return;
        }
        if (filter_has_var(INPUT_POST, 'mb4updateTable')) {
            $this->utf8mb4UpdateTable();
        }
        if (filter_has_var(INPUT_POST, 'mb4updateDB')) {
            $this->utf8mb4UpdateDB();
        }
        $data['dbCollation'] = $this->getDbCollation();
        $data['tables'] = $this->getTables();
        $data['tablesCollations'] = $this->getTablesCollations($data['tables']);

        $this->view('dbmanager', $data);
    }
    
    private function getTablesCollations($tables){
        $tableCollation = array();
        foreach($tables as $table){
            $tableCollation[$table] = $this->getCollation($table);
        }
        return $tableCollation;
    }

    /**
     * returns tablenames in the configured database
     * @global array $aConfig
     * @return array $array(0=>'tablename',1=>'... ) 
     */
    private function getTables() {
        $aConfig = Config::getConfig();

        $row = $this->query("SHOW TABLES FROM {$aConfig['dbName']} LIKE '{$aConfig['dbPrefix']}%' ",true);
        //$row = mysqli_fetch_row($rs);
        foreach ( $row as $table ) {
            if ($aConfig['dbPrefix'] == substr($table[0], 0, strlen($aConfig['dbPrefix']))) {
                $tabA[] = $table[0];
            }
        }
        return $tabA;
    }

    /**
     * returns tablenames of a given table or FALSE if table does not exist
     * Caution: does not add or remove a prefix.
     * @param String $tablename
     * @return Array $array( [0] => Array ( [Field] => id [Type] => int(11) [Null] => NO [Key] => PRI [Default] => [Extra] => auto_increment ) 
     *                       [1] => Array ( [Field] => ...
     */
    private function getColumns($tablename) {
        $result = $this->query("SHOW COLUMNS FROM $tablename", true);
        return $result;
    }

    private function getDbCollation() {
        $result = $this->query('show variables like "character_set_database"', true);
        print_r($result);
        return $result[1];
    }
    
    private function getCollation($table) {
        $result = $this->query("SHOW TABLE STATUS WHERE NAME LIKE '$table'", true);
        
        return $result[14];
    }

    private function utf8mb4UpdateTable() {
        $table = filter_input(INPUT_POST, 'table', FILTER_UNSAFE_RAW);
        $this->utf8mb4Update($table);
    }

    private function utf8mb4UpdateDB() {
        $sql = "ALTER DATABASE `" . Config::get('dbName') . "` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
        $this->query($sql);
    }

    private function utf8mb4Update($table) {
        /**
         * TODO:
         * dump table data
         * drop table
         * rebuild table from module scheme
         * (optional: dump table scheme, replace varchar(255) and similar with mb4 compatible types
         * reinsert dumped data
         */
        $tableParts = explode('_', $table);

        $module = $tableParts[1]; // as long as the module dbs following convention
        $tableData = $this->dumpData($table);

        if (!$this->dropTable($table)) {
            echo "Table could not be dropped: " . $table;
            return;
        }

        $inst = new Installer();
        $inst->installModulesDb(array(0 => $module));

        $datas = explode(";;;", $tableData);
        foreach ($datas as $d) {
            if(trim($d)==''){
                continue;
            }
            if (!$this->query($d)) {
                echo "Query cannot be executed: " . $d;
            }
        }
    }

    private function dropTable($tablename) {
        $def = "";
        $def .= "DROP TABLE `$tablename` ";
        $result = $this->query($def);

        if (!$result)
            return false;
        else
            return true;
    }

    private function dumpTableData() {
        $this->standalone = true;
        $table = filter_input(INPUT_POST, 'table', FILTER_UNSAFE_RAW);
        header("Content-Type: text/plain;");
        header("Content-Transfer-Encoding: binary;");
        Header("Content-Disposition: attachment; filename=$table.sql;");
        Header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        echo str_replace(Config::get('dbPrefix'), "{dbprefix}", $this->dumpData($table));
    }

    private function dumpData($table) {
        
        /**
         * TODO: include column names into dumps
         * 
         * 
         */

        $result = $this->query('SELECT * FROM ' . $table);
        $fields_amount = $result->field_count;
        $rows_num = $result->num_rows;
        $res = $this->query('SHOW CREATE TABLE ' . $table);
        $TableMLine = $res->fetch_row();
        $content = ''; //(!isset($content) ? '' : $content) . "\n\n" . $TableMLine[1] . ";;;\n\n";

        for ($i = 0, $st_counter = 0; $i < $fields_amount; $i++, $st_counter = 0) {
            foreach ( $result->fetch_row() as $row) { //when started (and every after 100 command cycle):
                if ($st_counter % 100 == 0 || $st_counter == 0) {
                    $content .= "\nINSERT INTO " . $table . " VALUES";
                }
                $content .= "\n(";
                for ($j = 0; $j < $fields_amount; $j++) {
                    $row[$j] = str_replace("\n", "\\n", addslashes($row[$j]));
                    if (isset($row[$j])) {
                        $content .= '\'' . $row[$j] . '\'';
                    } else {
                        $content .= '\'\'';
                    }
                    if ($j < ($fields_amount - 1)) {
                        $content .= ',';
                    }
                }
                $content .= ")";
                //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                if ((($st_counter + 1) % 100 == 0 && $st_counter != 0) || $st_counter + 1 == $rows_num) {
                    $content .= ";;;";
                } else {
                    $content .= ",";
                }
                $st_counter = $st_counter + 1;
            }
        } $content .= "\n\n\n";
        return $content;
    }

    /**
     * 
     * 
     * 
     * DANGER, totally old and broken code ahead..!!
     * #############################################
     * 
     */

    /**
     *
     * @param $wo relativer Pfad zum document root ( "../../" oder gar "" meist ist $wo schon initialisiert)
     * @param $toFile (true oder false, in admin/install/dump/ als sql file speichern oder nur ausgeben)
     * @return unknown_type
     */
    private function dump($wo, $dbCon, $config, $toFile = false) {

        $db = $config['db_name'];
        //$dbCon->init($config);
        $con = $dbCon->con;
        //		include($wo."core/variablen.php");
        //		include($wo."core/connect.php");

        $sql = "SHOW TABLES FROM {$config['db_name']} LIKE '{$config['db_prefix']}%' ";
        $tables = $dbCon->query($sql);
        //print_r($tables);
        $export = "";
        foreach ($dbCon->fr($tables) as $table) {
            //print_r($table);
            if ($config['db_prefix'] == substr($table[0], 0, strlen($config['db_prefix']))) {
                $export .= $this->defdump($table[0], $wo, $dbCon, $config);
                $export .= $this->datadump($table[0], $wo, $dbCon, $config);
            }
        }

        $file_name = date('Y-m-d_His') . "_{$db}.sql";
        //echo $file_name; exit;
        //	header("Content-Type: application/octet-stream; ");
        //	header("Content-Transfer-Encoding: binary");
        //header("Content-Length: " . filesize($file) ."; ");
        //readfile($export);
        if ($toFile === true) {
            file_put_contents($wo . "admin/install/dump/" . $file_name, str_replace($config['db_prefix'], "{dbprefix}", $export));
        } else {//if(!$toFile===true)
            header("Content-Type: text/plain;");
            header("Content-Transfer-Encoding: binary;");
            Header("Content-Disposition: attachment; filename=$file_name;");
            Header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            echo str_replace($config['db_prefix'], "{dbprefix}", $export);
        }
        return;
    }

    private function defdump($tablename, $wo, $dbCon, $config) {
        //include($wo."core/variablen.php");
        //		include($wo."core/connect.php");
        $def = "";
        $def .= "CREATE TABLE $tablename (" . "\n";
        $result = mysqli_query("SHOW COLUMNS FROM $tablename") or die("Table $tablename not existing in database");
        foreach (mysqli_fetch_row($result) as $row) {

            //foreach($row as $field)
            $def .= "    `$row[0]` $row[1]";
            //print_r($row);
            if ($row[2] != "YES")
                $def .= " NOT NULL ";
            elseif (is_null($row[4]))
                $def .= " DEFAULT NULL ";;

            switch ($row[4]) {

                case "CURRENT_TIMESTAMP" :
                    $def .= " DEFAULT $row[4] ";

                    break;
                //				case "NULL" :
                //					$def .= " DEFAULT $row[4] ";
                //
                //				break;
                default :
                    if (!is_null($row[4]) && strpos($row[1], "int") !== FALSE) {
                        $def .= " DEFAULT $row[4] ";
                    } elseif (!is_null($row[4]))
                        $def .= " DEFAULT '$row[4]'";
                    break;
            }
            //if (!is_null($row[4])) $def .= " DEFAULT '$row[4]'";

            if ($row[5] != "")
                $def .= " $row[5]";
            $def .= ",\n";
        }
        $def = preg_replace("/,\n$/", "", $def);

        $result = mysqli_query("SHOW KEYS FROM $tablename");
        foreach (mysqli_fetch_row($result) as $row) {
            //print_r($row);echo "<br/><br/><br/>";
            //			PRIMARY KEY (`id`),
            //  		UNIQUE KEY `nick` (`nick`)

            $kname = $row[2];
            if (($kname != "PRIMARY") && ($row[4] == $kname)) {
                $def .= ",\n";
                $def .= "UNIQUE KEY `$kname` (`$kname`)";
            }
            if (($kname == "PRIMARY")) {
                $def .= ",\n";
                $def .= "PRIMARY KEY (`$row[4]`) ";
            }


            /**
             * TODO:
             * Evtl. gibt es mehr als einen Unique in einer Tabelle, bis jetzt aber noch nciht...
             */
            //if(!isset($index[$kname])) $index[$kname] = array();
            //			if(($kname != "PRIMARY") && ($row[4] == $kname))
            //			{
            //				$index["UNIQUE"][] = $row[4];
            //			}
            //			$index[$kname][] = $row["Column_name"];
            //print_r($index);echo "<br/><br/><br/>";
        }


        $def .= "\n)\n";
        $def .= "CHARACTER SET utf8 COLLATE utf8_general_ci;;;\n\n\n ";

        //include($wo."core/disconnect.php");
        return (stripslashes($def));
    }

    private function dropDatabase($wo, $dbCon, $config) {

        //		include($wo."core/variablen.php");
        //		include($wo."core/connect.php");

        $tables = $dbCon->listTables();
        if (!$tables) {
            return false;
        }
        //print_r($tables);
        $export = "";
        foreach ($dbCon->fr($tables) as $table) {
            //print_r($table);
            if ($config['db_prefix'] == substr($table[0], 0, strlen($config['db_prefix']))) {
                if (!$this->dropTable($table[0], $wo, $dbCon, $config)) {
                    //if something happens, return false
                    //return false;
                }
            }
        }
        //include($wo."core/disconnect.php");
        return true;
        //exit;
    }

}
