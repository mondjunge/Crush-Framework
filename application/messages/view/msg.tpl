<div class="entry">
    <div class="entry_img">
         <?php echo $this->image($entry['image'], 'profile_pic', '32', '32', "{$entry['nick']}"); ?>
    </div>
    <span class="entry_nick"><?php echo $entry['nick'] ?></span> <span class="entry_date"><?php echo $entry['time_formated']; ?><?php echo $this->ts("Uhr"); ?></span>
    <div class="entry_text">
        <?php echo $entry['text'] ?>
    </div>
    <div style="clear:both;"></div>
</div>