
<div id="chatDetail">
    <span><?php echo $this->ts("Private conversation with"); ?>&nbsp;<?php echo $secondUserNick ?></span>
</div>    

<div id="msgContainer"><?php echo $logContent; ?></div>
<div id="chatInput">
<!--        <input id="msg" name="msg" type="text" autocomplete="off" /> -->

    <div class="inputContainer">
        <form class="pure-form">
            <fieldset>

                <div id="msgCon">
                    <textarea id="msg" name="msg" ></textarea>

                </div>
                <div id="subCon">
                    <button id="msgSubmit" class="pure-button bigSubmit" type="submit" ><?php echo $this->ts("send"); ?></button>

                </div>


                <input id="secondUserId" name="secondUserId" type="hidden" value="<?php echo $secondUserId ?>" />
            </fieldset>
        </form>
    </div>   
    <div class="help messageHelp"><img id="activateSmilies" src="<?php echo $themeUrl ?>/images/smileys/green/smile.gif" alt=":)" title=":)" class="smilie activeSelect" /> <?php echo $this->ts("send your message with [shift]+[enter]."); ?></div>

</div>
<?php echo $smilieHtml?>