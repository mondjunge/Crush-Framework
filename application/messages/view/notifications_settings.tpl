<!-- 

- Kontakt schreibt neuen Beitrag
- Jemand kommentiert eigenen Beitrag
- Jemand kommentiert fremden Beitrag den man zuvor auch kommentiert hat
- Man wird im Chat angeschrieben.
- neuer Event wird eingetragen

Optional
- neuer Kontakt

-->
<fieldset>
    <legend><?php echo $this->ts("private chat") ?></legend>
    <div class="pure-controls">
         <label for="chat" class="pure-checkbox" title="<?php echo $this->ts('contact writes you a chat message, but you are offline.');?>">
            <div class="checker">
                <input id="chat" class="checkbox" name="chat" <?php echo $s['chat'] ?> type="checkbox"> 
            <div class="check-bg"></div>
                <div class="checkmark">
                    <svg viewBox="0 0 100 100">
                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
            </div>
            <span class="label"><?php echo $this->ts('contact writes you a chat message, but you are offline.');?></span>
        </label>
    </div>
</fieldset>