CREATE TABLE IF NOT EXISTS `{dbprefix}messages` (
    `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `user_id_second` bigint NOT NULL,
  `last_message` text NOT NULL,
  `last_message_second` text NOT NULL,
  `last_online_first` datetime NOT NULL,
  `last_online_second` datetime NOT NULL,
  `first_ts` datetime NOT NULL,
  `second_ts` datetime NOT NULL,
  PRIMARY KEY (`id`)
);