//var activityStatus = false;
//var firstRun = true;

// first load only... 
// elements added after first load have to be modified after loading them!!!
$(document).ready(function () {


    getData("load");
    //checkForNewMessages();
    bindSendMsg();
    getOnlineFriends();

    handleShortcut();
    handleSmilieSelect();
    handleInsertSmilies();
    handleMsgFocus();
//autoScrollMsgContainer();

});

/**
 * Binds sendMsg to submit Button
 */
var bindSendMsg = function () {
    $('#msgSubmit').on("click", sendMsg);
}
var handleMsgFocus = function () {
    $('#msg').focus(function () {
        var smilieSelect = $("#smilieSelect");
        if (smilieSelect.hasClass("activeSelect")) {
            smilieSelect.hide();
            smilieSelect.removeClass("activeSelect");
        }
    });
}
/**
 * Handles shift-Return keybinding
 */
var handleShortcut = function () {
    /**
     * working shortcut for shift enter..
     */
    $('#msg').on('keypress', function (e) {
        if (e.keyCode == 13) {
            // 13 is enter/return key
            if (e.shiftKey) {
                // Enter and Shift pressed... do anything here...
                //alert(""+e.keyCode);
                sendMsg();
                return false;
            }

        }
    });
    $("#msg").focus();

}
/**
 * Send message function sends the msg textarea value to the server via ajax 
 */
var sendMsg = function () {
    var clientmsg = $("#msg").val();
    var secondUserId = $("#secondUserId").val();
    if (clientmsg.length > 0) {

        $.ajax({
            type: "POST",
            url: reLangUrl + "messages",
            async: true,
            timeout: 5000,
            data: {
                mode: 'send',
                text: clientmsg,
                secondUserId: secondUserId
            },
            success: function (html) {

                $("#msgContainer").html(html);

                autoScrollMsgContainer();

            }
        });
    }
    // set hidden value
    $("#msg").prop("value", "").focus();
    return false;
}
/**
 * handles selectbox for smilies
 */
var handleSmilieSelect = function () {

    $("#smilieSelect").hide();
    $("#activateSmilies").on("click", function () {
        var smilieSelect = $("#smilieSelect");

        if (smilieSelect.hasClass("activeSelect")) {
            smilieSelect.hide();
            smilieSelect.removeClass("activeSelect");
            $("#msg").focus();
        } else {
            smilieSelect.show();
            smilieSelect.addClass("activeSelect");

        }

    });

}
var handleInsertSmilies = function () {
    $(".smilieContainer").find("img").on("click", function () {
        $("#msg").val($("#msg").val() + $(this).prop("title"));
        $("#smilieSelect").hide();
        $("#smilieSelect").removeClass("activeSelect");
        $("#msg").focus();
    })
}
/**
 * get updates on current chat regulary
 */
var getData = function (mode) {
    var secondUserId = $("#secondUserId").val();
    if (secondUserId === null) {
        return;
    }
    var chatUrl = reLangUrl + "messages";
    //alert(chatUrl);
    $.ajax({
        type: "POST",
        url: chatUrl,
        async: true,
        timeout: 5000,
        data: {
            mode: mode,
            secondUserId: secondUserId
        },
        success: function (html) {
            html = $.trim(html);
            if (html !== '') {
                $("#msgContainer").html(html);
                autoScrollMsgContainer();
                if (mode === 'idle') {
                    modules.theme.notifyUser('Neue Nachricht', 'hier klicken um sie zu lesen...');
                }

            }
            isUserOnline();
            setTimeout('getData(\'idle\')', 5000);

        }
    });

}
var autoScrollMsgContainer = function () {
    //auto scroll	
    if ($('#msgContainer').length > 0) {
        $('#msgContainer').animate({
            scrollTop: $('#msgContainer').get(0).scrollHeight
        }, 1500);
    }

}
/**
 * Check if specific User is online
 *
 */
var isUserOnline = function () {
    var secondUserId = $("#secondUserId").val();
    $.ajax({
        type: "POST",
        url: reLangUrl + "messages",
        async: true,
        timeout: 5000,
        data: {
            mode: 'isUserOnline',
            secondUserId: secondUserId
        },
        success: function (html) {
            if (html === "true") {
                // alert("true");
                $("#chatDetail").css('color', 'green');
            } else {
                // alert("false");
                $("#chatDetail").css('color', '#cc3333');
            }

        }
    });
};
var getOnlineFriends = function () {
    $.ajax({
        type: "POST",
        url: reLangUrl + "messages",
        async: true,
        timeout: 5000,
        data: {
            mode: 'getOnlineFriends'
        },
        success: function (csv) {
            csv = jQuery.trim(csv);
            //alert(csv);
            console.log("onlineFriends: " + csv);
            var arr = csv.split(",");
            //alert(arr);

            $('[id^="contact_"]').children(".status_off").show();
            $('[id^="contact_"]').children(".status_on").hide();

            for (var i = 0; i <= arr.length; i++) {
                //console.log(arr[i]);
                $("#contact_" + arr[i]).children(".status_off").hide();
                $("#contact_" + arr[i]).children(".status_on").show();
            }


        }
    });

    setTimeout('getOnlineFriends()', 10000);
}
/**
 * Now in theme.js, for better notifications and lower resource use...
 */
//var checkForNewMessages = function(){
//
//    var chatUrl = reLangUrl+"messages";
//    //alert(chatUrl);
//    $.ajax({
//        type: "POST",
//        url: chatUrl,
//        async: true,
//        timeout: 5000,
//        data: {
//            mode: "checkForNewMessages" 
//        },
//        success: function(html){
//            
//            if(html!=''){
//                var arr = html.split(",");
//                for(var i=0; i<arr.length; i++){
//                    $("#active_contact_"+arr[i]).show();
//                    $("#active_contact_"+arr[i]).css('color', 'red');
//                    $("#active_contact_"+arr[i]).parent().css('font-weight', 'bold');
//                }
//            }else{
//                $('[id^="active_contact_"]').parent().css('font-weight', 'normal');
//                $('[id^="active_contact_"]').hide();
//            }
//            setTimeout('checkForNewMessages()', 5000);
//                
//        }
//    });
//
//}

/**
 *
 *
 * OLD CHAT STUFF
 *
 */


//function changeActivity(activity){
//    //alert(chatStatus);
//
//    if (activity == true) {
//        //alert(oldTitleText);
//        activityStatus = true;
//        if (!$("#userMsg").is(":focus")) {
//            // change message if document has no focus
//            if(document.title == "-> Aktivität!"){
//                document.title = "Chat";
//            }else{
//                document.title = "-> Aktivität!";
//            }
//        }else{
//            document.title = "Chat";
//            activityStatus = false;
//        }
//    } else {
//        activityStatus = false;
//        if ($("#userMsg").is(":focus")) {
//            document.title = "Chat";
//        }
//    }  
//    
//}
//function getData() {
//    var oldscrollHeight = $("#msgContainer").attr("scrollHeight") - 20;
//    var room = $("#chatRoom").val();
//    var chatUrl = "?m=load&room="+room;
//    //var oldChatWindow = $("#chatWindow").html();
//    if(!firstRun){
//        //alert("something");
//        chatUrl = "?m=idle&room="+room;
//    }       
//    //alert(chatUrl);
//    $.ajax({
//        type: "POST",
//        url: chatUrl,
//        async: true,
//        timeout: 30000,
//        data: "get=true",
//        success: function(html){
//            var htmlArr = html.split('<!--USERLIST-->');
//                
//            /**
//             * Userlist
//             */
//            //alert(htmlArr[1]);
//            $("#friendsList").html(htmlArr[1]);
//            
//            //Private Chat click on username
//            //            $("a.private").on("click",function(){
//            //                addRoom(""+this.text+"",this.id);
//            //                //changeRoom(this.id);
//            //                $("a.rooms").on("click",function(){
//            //                    changeRoom(this.id);
//            //                    return false;
//            //                });
//            //                
//            //                $("#msgContainer").attr("style", "background-color:#333; color:#fff;");
//            //                $("#userMsg").focus();
//            //                return false;
//            //            });
//            /** */
//            
//            if(firstRun){
//                $("#msgContainer").html(htmlArr[0]);
//                firstRun = false;
//            }else{
//                if(htmlArr[0]!=''){
//                    $("#msgContainer").append(htmlArr[0]);
//                    changeActivity(true);
//                }
//            }
//            
//            if(activityStatus){
//                changeActivity(true);
//            }
//            
//            //auto scroll				
//            var newscrollHeight = $("#msgContainer").attr("scrollHeight") - 20;
//            if(newscrollHeight > oldscrollHeight){
//                $("#msgContainer").animate({
//                    scrollTop: newscrollHeight
//                }, 'normal'); //Autoscroll to bottom of div
//            }
//            
//            setTimeout("getData()", 2000);
//                
//        }
//    });
//}
function addRoom(desc, id) {
    if (roomExists(id)) {
        // do nothing if room exists
        return false;
    } else {
        // build link for room
        var link = "<a id=" + id + " class='room' href='" + id + "'>" + desc + "</a>";
        // temporary removal of id... its a workaround, not pretty
        $("a#" + id).removeAttr("id");

        //add link to roomlist
        $("#roomList").append("&nbsp;" + link);

        // when link is added and ready to manipulate
        $("a#" + id).ready(function () {
            var room = new Object();
            room.id = id;
            room.desc = $("a#" + id).text();
            room.active = true;
            room.first_run = true;
            activeRooms.push(room);
            //alertRooms(activeRooms);
            //add click event to change the room... 
            $("a#" + id).on("click", function () {
                changeRoom(this.id);
                return false;
            });

        });
        //change active room to added room
        changeRoom(id);
    }
}
function writeRoomLog(id, text) {
    for (var i = 0; i < activeRooms.length; i++) {
        if (activeRooms[i].id == id) {
            activeRooms[i].log = text;
            return;
        }
    }
}
function roomExists(id) {
    for (var i = 0; i < activeRooms.length; i++) {
        if (activeRooms[i].id == id) {
            return true;
        }
    }
    return false;
}
function alertRooms(rooms) {
    var text = "";
    for (var i = 0; i < rooms.length; i++) {
        text += "[" + i + "]=>" + rooms[i].id + "\n" + rooms[i].desc + "\n" + rooms[i].active + "\n" + rooms[i].first_run + "\n\n";
    }
    alert(text);
}
function alertArray(arr) {
    var text = "";
    for (var i = 0; i < arr.length; i++) {
        text += "[" + i + "]=>" + arr[i] + "\n";
    }
    alert(text);
}

function handleRoomClickEvent(element) {
    alert(element);

}

function removeRoom(id) {
    $("#roomList").remove("#" + id);
    $(".room").ready(function () {
        var room = new Object();
        room['id'] = $(this).attr("id");
        room['desc'] = $(this).text();
        room['active'] = false;
        activeRooms.push(room);
    });
    $(".active_room").ready(function () {
        var room = new Object();
        room['id'] = $(this).attr("id");
        room['desc'] = $(this).text();
        room['active'] = true;
        activeRooms.push(room);
    });
}

function changeRoom(id) {
    // set value to hidden field

    $("#chatRoom").val(id);

    // loading message
    //$("#chatWindow").html("Chat wird neu geladen... Bitte warten...");

    //if(!firstRun){
    // change classes of room buttons
    //$("a.active_room").addClass("room");
    $("a.room").removeClass("active_room");
    $("a#" + id).addClass("active_room");
    //$("a#"+id).removeClass("room");
    // }
    $("a.active_room").ready(function () {
        $("a.active_room").on("click", function () {
            return false;
        });
    });

    var array = id.split("-");
    // set colors for private chat or global
    //    if(array[0]=="rooms"){
    //        $("#chatWindow").attr("style", "background-color:#fff; color:#333;");
    //    }
    //    
    //    if(array[0]=="private"){
    //        $("#chatWindow").attr("style", "background-color:#333; color:#fff;");
    //    }
    //$("#roomList").html("Globaler Chat");
    $("#userMsg").focus();
    firstRun = true;
    changeActivity(false);
};
