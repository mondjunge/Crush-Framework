<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Messages extends AppController implements INotify{

    /**
     *  
     * @var type 
     */
    private $uNick;
    private $uId;
    private $chatId;
    private $logPath = "/../../upload/messages/";
    private $logDir;
    private $logFilepath;
    private $ProfileModel;
    // if true, you need contacts in profile module, if false, you can message all users in the system.
    private $needContacts = false;

    public function __construct() {
        // initiate model 
        //include_once 'application/profile/model/ProfileModel.php';
        //$this->ProfileModel = new ProfileModel();
        //$this->ProfileModel();
        
        $this->uId = Session::get('uid');
        $this->uNick = Session::get('unick');
        //$this->includeJs("javascript/messages/messages.js");
    }

    public function index() {
        

        if (filter_has_var(INPUT_POST, 'mode')) {
            $postMode = filter_input(INPUT_POST, 'mode');
        } else {
            $this->autoCheckRights();
            /**
             * - show contact (friends) list
             * - show last messages list
             * - show detail messages list
             */
            $data['content'] = "";
            if (isset($_REQUEST['showContacts'])) {
                $data['content'] = $this->showContacts();
            }
            if (isset($_REQUEST['showLatest'])) {
                $data['content'] = $this->showLatest();
            }
            if (isset($_REQUEST['showDetail'])) {
                $data['content'] = $this->showDetail();
            }
            $data['contacts'] = $this->showContacts();
            $this->view("main", $data);
        }
        /**
         * AJAX Requests 
         */
        if (isset($postMode) && $postMode == "load") {
            $this->standalone = true;
            $chatId = $this->getChatLogId(filter_input(INPUT_POST, 'secondUserId'));
            $notice = "";
            if ($this->isUserOnline(filter_input(INPUT_POST, 'secondUserId')) == 'false') {
                $notice = "<span class='help'>" . $this->ts("User is offline. System will send him/her an E-Mail if you begin to chat now.") . "</span>";
            }
            echo $this->readLogFile($chatId) . $notice;
            return;
        }
        if (isset($postMode) && $postMode == "idle") {
            $this->standalone = true;
            $chatId = $this->getChatLogId(filter_input(INPUT_POST, 'secondUserId'));
            $notice = "";
//             if ($this->isUserOnline(filter_input(INPUT_POST, 'secondUserId')) == 'false') {
//                $notice = "<span class='help'>" . $this->ts("User is offline. System will send him/her an E-Mail if you begin to chat now.") . "</span>";
//            }
            echo $this->idleLogFile($chatId) . $notice;
            return;
        }
        if (isset($postMode) && $postMode == "send") {
            $this->standalone = true;
            echo $this->sendMessage();
            return;
        }
        if (isset($postMode) && $postMode == "checkForNewMessages") {
            $this->standalone = true;
            if(!$this->isModuleActive('messages') || !$this->checkSiteRights() || !$this->tableExists('messages')){
                echo "disabled";
                return;
            }
            $this->updateOnlineTimestamp();
            echo $this->checkForNewMessages();
            return;
        }
        if (isset($postMode) && $postMode == "updateOnlineTimestamp") {
            $this->autoCheckRights();
            $this->standalone = true;
            //$chatId = $this->getChatLogId(filter_input(INPUT_POST, 'secondUserId'));
            $this->updateOnlineTimestamp();
            return;
        }
        if (isset($postMode) && $postMode == "isUserOnline") {
            $this->autoCheckRights();
            $this->standalone = true;
            //$chatId = $this->getChatLogId(filter_input(INPUT_POST, 'secondUserId'));

            echo $this->isUserOnline(filter_input(INPUT_POST, 'secondUserId'));
            return;
        }
        if (isset($postMode) && $postMode == "getOnlineFriends") {
            $this->autoCheckRights();
            $this->standalone = true;
            //echo $this->getOnlineFriends();
            echo $this->getOnlineContacts();
            return;
        }
        if (filter_has_var(INPUT_GET,'mode') && filter_input(INPUT_GET, 'mode') == "getSmilies") {
            $this->autoCheckRights();
            $this->standalone = true;
            echo $this->viewSmilies();
            return;
        }
    }

    /**
     * get FriendsList
     * @return html 
     */
    private function showContacts() {
        $onlineUsers=array();
        /**
         * - get Friends List
         * - create html for each friend (Picture, Nick, Link to Conversation
         * - return result
         */
        if (!$this->needContacts) {
            $users = $this->select('user', 'id', '', 'last_online DESC, id ASC');
            foreach ($users as $u) {
                if ($u['id'] != Session::get('uid'))
                    $onlineUsers[] = $u['id'];
            }
        } else {
            $rs = $this->select('profile', '*', "user_id='$this->uId'", "");

            if ($rs[0]['friends'] == "") {
                /**
                 * no friends :((( :D
                 * TODO: add button to get some..
                 */
                return $this->ts("no contacts... :(");
            }
            $onlineUsers = explode(",", $rs[0]['friends']);
        }




        $html = "";
        foreach ($onlineUsers as $ou) {

            $u = $this->select('user', 'id,nick,image', "id={$ou}");
            //$user = $this->select('user', 'nick', "id={$ou}");
            if (sizeof($u) == 0) {
                /**
                 * delete user from csv 
                 */
            }
            //$u[0]['nick'] = $user[0]['nick'];
            if ($u['0']['image'] == '') {
                $u['0']['image'] = "profil.png";
            } else {
                $u['0']['image'] = Config::get("relativeUrl") . $u['0']['image'];
            }
            //$u[0]['user_id'] = $ou;
            $data['user'] = $u[0];
            //$data['user_id'] = $ou;
            //$this->setDebugMessage("Messages UserId: "+$data['user_id']);
            $html .= $this->fetch('contact', $data, true);
        }
        return $html;
    }

//    public function widget(){
//        $this->autoCheckRights();
//        $this->standalone = true;
//        return $this->showContacts();
//    }

    /**
     * GET showDetail (= userId) needed
     * @return type 
     */
    private function showDetail() {


        // check user input 
        $secondUid = $_REQUEST['showDetail'];

        if ($secondUid == $this->uId) {
            $this->setErrorMessage($this->ts('Error: You cannot chat with yourself!'));
            return;
        }

        if ($secondUid == 0 || !is_numeric($secondUid)) {
            //echo "secondUid:" . $secondUid;
            return "";
        }

        /**
         * Chat Log Data Logic 
         */
        // get chatId from db, create one if not exists
        $chatId = $this->getChatLogId($secondUid);

        // get Log Content
        $logContent = $this->readLogFile($chatId);
        //echo $logContent;
        // check $chadId for false
        if ($logContent === FALSE) {
            // no log DIR exists!!!
            // $this->createLogFile($secondUid);
            $logContent = $this->ts("Leave your friend a message...");
        }

        /**
         * Second User Data 
         */
        //$u = $this->select('user', 'image_path', "user_id={$secondUid}");
        $user = $this->select('user', 'nick,image', "id={$secondUid}");

        $data['themeUrl'] = Config::get('relativeUrl') . 'theme/' . Config::get('siteStyle');
        $data['smilieHtml'] = $this->getSmiliesHtml();
        $data['logContent'] = $logContent;
        $data['secondUserNick'] = $user[0]['nick'];
        $data['secondUserId'] = $secondUid;
        $data['secondUserImagePath'] = $user[0]['image'];
        $content = $this->fetch('detail', $data);

        return $content;
    }

    private function sendMessage() {
        // check user input 
        $secondUid = $_REQUEST['secondUserId'];
        
        $text = $_REQUEST['text'];
        $notice = '';
        // $text = $_REQUEST['text'];
        if ($secondUid == 0 || !is_numeric($secondUid)) {
            return;
        }
        // get chatId from db, create one in not exists
        $chatId = $this->getChatLogId($secondUid);

        // set, yeah chat and Log Path
        //$this->setChatIdAndLogPath($chatId);

        if ($this->isUserOnline($secondUid) == 'false') {

            $umlautUrl = str_replace("xn--", "ü", Config::get('rootUrl'));
            $umlautUrl = str_replace("-jva", "", $umlautUrl);
            // Notification chat
            $rs = $this->select('user as u, ' . Config::get('dbPrefix') . 'notifications_settings as ns'
                    , 'u.id, u.email, u.nick, ns.newentry'
                    , "u.id=ns.user_id AND ns.chat=1 AND u.id='$secondUid'");

            if ($rs !== FALSE && sizeof($rs) > 0) {
                $nick = Session::get('unick');
                $userId = Session::get('uid');
                $link = $umlautUrl . Config::get('reLangUrl') . "messages?showDetail=" . $userId;
                foreach ($rs as $r) {
                    $subject = $this->ts('New Message from $2');
                    $message = $this->ts("Hello $1,");
                    $message .= "<br/><br/>\r\n\r\n";
                    $message .= $this->ts("there is a new message from $2.");
                    //$message .= "<br/><br/>";
                    //$message .= "\"".$this->replaceUrls(stripslashes(nl2br(htmlspecialchars($text))))."\"";
                    $message .= "<br/><br/>\r\n\r\n";
                    $message .= $this->ts("Take a look now under following link:");
                    $message .= "<br/>\r\n";
                    $message .= "<a href='$link'>" . $link . "</a>";
                    $message = str_replace('$1', htmlentities($r['nick']), $message);
                    $message = str_replace('$2', htmlentities($nick), $message);
                    $subject = str_replace('$2', htmlentities($nick), $subject);

                    if (!$this->mail_utf8($r['email'], $subject, $message, $nick)) {
                        $notice = "<span class='help'>" . $this->ts('User is offline, something went wrong while sending him an E-Mail.') . "</span>";
                    } else {
                        $notice = "<span class='help'>" . $this->ts('User is offline, E-Mail was send to notice him.') . "</span>";
                    }
                }
            } else {
                $notice = "<span class='help'>" . $this->ts('User is offline, no E-Mail could be send, because user have deactivated notifications.') . "</span>";
            }
            //END Notification
//            $sUser = $this->select('user', 'nick,email', "id='$secondUid'");
//
//
//
//            if ($sUser[0]['email'] != '') {
//                if (!mail($sUser[0]['email'], $this->ts('New Message on') . " " . Config::get('rootUrl'), $this->ts("Hello") . " "
//                                . $sUser[0]['nick'] . ", \n\r"
//                                . $this->ts("You have a new message from") . " " . Session::get("unick") . ":\n\r"
//                                . $text
//                                . "\n\r"
//                                . $this->ts("Answer online by visiting following Link") . "\n"
//                                . Config::get('rootUrl') . Config::get('reLangUrl') . "messages?showDetail=" . $secondUid . "\n\r"
//                )) {
//                    $notice = "<span class='help'>" . $this->ts('User is offline, something went wrong while sending him an E-Mail.') . "</span>";
//                } else {
//                    $notice = "<span class='help'>" . $this->ts('User is offline, E-Mail was send to notice him.') . "</span>";
//                }
//            } else {
//                $notice = "<span class='help'>" . $this->ts('User is offline, no E-Mail could be send, because user have no E-Mail.') . "</span>";
//            }
            //$text = $text;
        }


        $this->writeToLog($chatId, $text);
        return $this->readLogFile($chatId) . $notice;
    }

    /**
     * @param post text
     * @param type $chatId
     * @return boolean 
     */
    private function writeToLog($chatId, $text) {
        /**
         * DONE:
         * - read template
         * - template data : datetime, nick, text 
         * - append template to log

         * 
         */
        

        // set chat and Log Path
        $this->setChatIdAndLogPath($chatId);
        $this->createLogFileIfNotExists();
        $timestamp = time();

        $data['entry']['nick'] = Session::get('unick');
        $u = $this->select('user', 'image', "id=".Session::get('uid'));
        if ($u[0]['image'] == '') {
            $u[0]['image'] = "profil.png";
        } else {
            $u[0]['image'] = Config::get("relativeUrl") . $u['0']['image'];
        }
        $data['entry']['image'] = $u[0]['image'];
        $data['entry']['time_formated'] = date("d.m.Y H:i:s", $timestamp);
        $data['entry']['text'] = $this->replaceSmilies($this->replaceUrls(stripslashes(nl2br(htmlspecialchars($text)))));

        //$room = $this->room;

        $msg = $this->fetch("msg", $data);
        /**
         * write to log 
         */
        if (file_put_contents($this->logFilepath, $msg, FILE_APPEND) !== FALSE) {
            $this->updateTimestamp($chatId, $timestamp);
            return $msg;
        } else {
            return "";
        }
    }

    /**
     * updates last viewed/send timestamp for current user
     * @param type $chatId
     * @param type $timestamp 
     */
    private function updateTimestamp($chatId, $timestamp = null) {

        $messagesData = $this->select('messages', 'user_id,user_id_second,first_ts,second_ts', "id=$chatId");

        if ($messagesData[0]['user_id'] == $this->uId) {
            if ($timestamp == null) {
                $fieldsValueArray = array(
                    'first_ts' => $messagesData[0]['second_ts']
                );
                $this->updatePrepare('messages', $fieldsValueArray, "id=:chatId", array('chatId' => $chatId));
            } else {

                $fieldsValueArray = array(
                    'first_ts' => date("Y-m-d H:i:s", $timestamp)
                        //'first_ts' => date("Y-d-m H:i:s", $timestamp)
                );
                $this->updatePrepare('messages', $fieldsValueArray, "id=:chatId", array('chatId' => $chatId));
            }
        }
        if ($messagesData[0]['user_id_second'] == $this->uId) {

            if ($timestamp == null) {
                $fieldsValueArray = array(
                    'second_ts' => $messagesData[0]['first_ts']
                );
                $this->updatePrepare('messages', $fieldsValueArray, "id=:chatId", array('chatId' => $chatId));
            } else {
                $fieldsValueArray = array(
                    'second_ts' => date("Y-m-d H:i:s", $timestamp)
                );
                $this->updatePrepare('messages', $fieldsValueArray, "id=:chatId", array('chatId' => $chatId));
            }
        }
    }

    /**
     *
     * @param type $userId 
     */
    private function isUserOnline($userId) {
        $profileData = $this->selectPrepare('user', 'last_online', "id=:userId", array('userId' => $userId));

        if ($profileData[0]['last_online'] < date("Y-m-d H:i:s", mktime(date("H"), date("i") - 3))) {
            // more than 3 Minutes ago
            return 'false';
        } else {
            return 'true';
        }
    }

    /**
     * Returns a String of all Online Friends
     * @return String csv 
     */
    private function getOnlineFriends() {

        //$af = $this->ProfileModel->getFriends();
        $ar = array();
        foreach ($af as $f) {
            if ($this->isUserOnline($f['user_id']) == 'true') {
                $ar[] = $f['user_id'];
            }
        }
        return implode(',', $ar);
    }

    private function getOnlineContacts() {

        //$af = $this->ProfileModel->getFriends();
        $af = $this->getContacts();
        $ar = array();
        foreach ($af as $f) {
            if ($this->isUserOnline($f) == 'true') {
                $ar[] = $f;
            }
        }
        return implode(',', $ar);
    }

    private function getContacts() {
        
        $users = $this->select('user', 'id', '1=1', 'last_online DESC');
        foreach ($users as $u) {
            if ($u['id'] != Session::get('uid'))
                $contacts[] = $u['id'];
        }
        return $contacts;
    }

    private function checkForNewMessages() {

        $uid = $this->uId;
        // check if some private chatroom exists
        $messagesData = $this->select('messages', 'id,first_ts,second_ts,user_id,user_id_second', "user_id='$uid' OR user_id_second='$uid'");
        $returnCsv = "";
        foreach ($messagesData as $mr) {
            if ($mr['user_id'] == $this->uId && $mr['first_ts'] < $mr['second_ts']) {
                //$returnArray[] = $mr['id'];
                $returnCsv = $this->addToCsvString($mr['user_id_second'], $returnCsv);
            }
            if ($mr['user_id_second'] == $this->uId && $mr['first_ts'] > $mr['second_ts']) {
                $returnCsv = $this->addToCsvString($mr['user_id'], $returnCsv);
            }
        }

        return $returnCsv;
    }

    /**
     * adds a userId o other String to a csv or empty String
     * user for voting and adding friends
     * @param type $userId
     * @param type $csvString
     * @return type 
     */
    private function addToCsvString($userId, $csvString) {
        if ($csvString == "") {
            $fString = $userId;
        } else {
            $fArr = explode(",", $csvString);
            if (in_array($userId, $fArr)) {
                // userId is already in string
                return $csvString;
            }
            array_push($fArr, $userId);

            $fUnique = array_unique($fArr);

            $fString = implode(",", $fUnique);
        }
        return $fString;
    }

    private function replaceUrls($string) {

        /*         * * make sure there is an http:// on all URLs ** */
        $string = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-\+%]+)/i", "$1http://$2", $string);
        /*         * * make all URLs links ** */
        $string = preg_replace("/([\w]+:\/\/[\w\-?!&;#,%~=\.\/\@\+]+[\w\/])/i", "<a target=\"_blank\" href=\"$1\">$1</a>", $string);
        /*         * * make all emails hot links ** */
        $string = preg_replace("/([\w\-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i", "<A HREF=\"mailto:$1\">$1</A>", $string);

        return $string;
    }

    /**
     * replace smilies with pictures
     * @param type $string 
     */
//    private function replaceSmilies($string) {
//
//        $icons = $this->getSmilies();
//
//        foreach ($icons as $icon => $image) {
//            $icon = preg_quote($icon);
//            $string = preg_replace("!$icon!", $image, $string);
//        }
//
//
//        return $string;
//    }
//
//    /**
//     * Returns [smilie][replacement] Array
//     * @return array 
//     */
//    private function getSmilies() {
//        $themeUrl = Config::get('relativeUrl') . 'theme/' . Config::get('siteStyle');
//        $icons = array(
//            ':)' => '<img src="' . $themeUrl . '/images/smileys/green/smile.gif" alt=":)" title=":)" class="smilie" />',
//            ':(' => '<img src="' . $themeUrl . '/images/smileys/green/sad.gif" alt=":(" title=":("  class="smilie" />',
//            ':\'(' => '<img src="' . $themeUrl . '/images/smileys/green/heul.gif" alt=":\'(" title=":\'("  class="smilie" />',
//            ';)' => '<img src="' . $themeUrl . '/images/smileys/green/twink.gif" alt=";)" title=";)"  class="smilie" />',
//            ':*' => '<img src="' . $themeUrl . '/images/smileys/green/kiss.png" alt=":*" title=":*"  class="smilie" />',
//            ':D' => '<img src="' . $themeUrl . '/images/smileys/green/grin.gif" alt=":D" title=":D"  class="smilie" />',
//            ':P' => '<img src="' . $themeUrl . '/images/smileys/green/tongue.gif" alt=":P" title=":P"  class="smilie" />',
//            ':O' => '<img src="' . $themeUrl . '/images/smileys/green/wtf.gif" alt=":O" title=":O"  class="smilie" />',
//            '8|' => '<img src="' . $themeUrl . '/images/smileys/green/omg.gif" alt="8|" title="8|"  class="smilie" />',
//            '&lt;||' => '<img src="' . $themeUrl . '/images/smileys/green/joint.gif" alt="&lt;||" title="&lt;||"  class="smilie" />',
//            'B)' => '<img src="' . $themeUrl . '/images/smileys/green/cool.gif" alt="B)" title="B)"  class="smilie" />',
//            ':horny:' => '<img src="' . $themeUrl . '/images/smileys/green/love.gif" alt=":horny:" title=":horny:"  class="smilie" />',
//            ':what:' => '<img src="' . $themeUrl . '/images/smileys/green/huuh.gif" alt=":what:" title=":what:"  class="smilie" />',
//            ':coffee:' => '<img src="' . $themeUrl . '/images/smileys/green/coffee.gif" alt=":coffee:" title=":coffee:"  class="smilie" />',
//            ':bye:' => '<img src="' . $themeUrl . '/images/smileys/green/byebye.gif" alt=":bye:" title=":bye:"  class="smilie" />',
//            ':hello:' => '<img src="' . $themeUrl . '/images/smileys/green/hello.gif" alt=":hello:" title=":hello:"  class="smilie" />',
//            ':doh:' => '<img src="' . $themeUrl . '/images/smileys/green/doh.gif" alt=":doh:" title=":doh:"  class="smilie" />',
//            ':zzz:' => '<img src="' . $themeUrl . '/images/smileys/green/sleep.gif" alt=":zzz:" title=":zzz:"  class="smilie" />',
//            ':holy:' => '<img src="' . $themeUrl . '/images/smileys/green/holy.gif" alt=":holy:" title=":holy:"  class="smilie" />',
//            ':evil:' => '<img src="' . $themeUrl . '/images/smileys/green/twisted.gif" alt=":evil:" title=":evil:"  class="smilie" />',
//            ':puke:' => '<img src="' . $themeUrl . '/images/smileys/green/puke.gif" alt=":puke:" title=":puke:"  class="smilie" />',
//            ':agree:' => '<img src="' . $themeUrl . '/images/smileys/green/agree.gif" alt=":agree:" title=":agree:"  class="smilie" />',
//            ':disagree:' => '<img src="' . $themeUrl . '/images/smileys/green/disagree.gif" alt=":disagree:" title=":disagree:"  class="smilie" />',
//            ':devil:' => '<img src="' . $themeUrl . '/images/smileys/green/devil.gif" alt=":devil:" title=":devil:"  class="smilie" />',
//            ':angel:' => '<img src="' . $themeUrl . '/images/smileys/green/engel.gif" alt=":angel:" title=":angel:"  class="smilie" />',
//            ':beer:' => '<img src="' . $themeUrl . '/images/smileys/green/bier.gif" alt=":beer:" title=":beer:"  class="smilie" />',
//            ':nonazi:' => '<img src="' . $themeUrl . '/images/smileys/green/nofascho.gif" alt=":nonazi:" title=":nonazi:"  class="smilie" />',
//            ':rofl:' => '<img src="' . $themeUrl . '/images/smileys/green/rofl_1.gif" alt=":rofl:" title=":rofl:" height="" class="smilie" />',
//            ':lol:' => '<img src="' . $themeUrl . '/images/smileys/green/lol.gif" alt=":lol:" title=":lol:"  class="smilie" />',
//            ':up:' => '<img src="' . $themeUrl . '/images/smileys/green/thumb.gif" alt=":up:" title=":up:"  class="smilie" />',
//            ':down:' => '<img src="' . $themeUrl . '/images/smileys/green/thumbdown.gif" alt=":down:" title=":down:"  class="smilie" />',
//            ':fu:' => '<img src="' . $themeUrl . '/images/smileys/green/fu.gif" alt=":fu:" title=":fu:" height="" class="smilie" />',
//            ':rock:' => '<img src="' . $themeUrl . '/images/smileys/green/rock.gif" alt=":rock:" title=":rock:"  class="smilie" />',
//            ':hack:' => '<img src="' . $themeUrl . '/images/smileys/green/hack.gif" alt=":hack:" title=":hack:"  class="smilie" />',
//            ':blush:' => '<img src="' . $themeUrl . '/images/smileys/green/blush.gif" alt=":blush:" title=":blush:"  class="smilie" />',
//            ':hug:' => '<img src="' . $themeUrl . '/images/smileys/green/hug.gif" alt=":hug:" title=":hug:"  class="smilie" />',
//            ':cuddle:' => '<img src="' . $themeUrl . '/images/smileys/green/cuddle.gif" alt=":cuddle:" title=":cuddle:"  class="smilie" />',
//            ':sex:' => '<img src="' . $themeUrl . '/images/smileys/green/sex.gif" alt=":sex:" title=":sex:"  class="smilie" />',
//            ':smoke:' => '<img src="' . $themeUrl . '/images/smileys/green/smoke.gif" alt=":smoke:" title=":smoke:"  class="smilie" />',
//            ':hide:' => '<img src="' . $themeUrl . '/images/smileys/green/hide.gif" alt=":hide:" title=":hide:"  class="smilie" />',
//            '&lt;3' => '<img src="' . $themeUrl . '/images/smileys/zz_red_heart.png" alt="&lt;3" title="&lt;3" width="19" height="19" class="smilie" />'
//        );
//
//        return $icons;
//    }

    private function getSmiliesHtml() {
        $s = array();
        $s['icons'] = $this->getSmilies();
        return $this->fetch('smilies', $s);
    }

    /**
     * returns the newly created ChatId
     * @param type $secondUid
     * @return type 
     */
    private function createDbLog($secondUid) {
        $uid = $this->uId;

        $fieldsValueArray = array(
            'user_id' => $uid,
            'user_id_second' => $secondUid
        );

        $chatId = $this->insert('messages', $fieldsValueArray);

        return $chatId;
    }

    /**
     * Returns ChatLogId or false if no log exists
     * @param type $secondUid
     * @return int or false 
     */
    private function getChatLogId($secondUid) {
        $uid = $this->uId;
        // check if some private chatroom exists
        $data = $this->select('messages', 'id', "(user_id='$uid' OR user_id_second='$uid') AND (user_id='$secondUid' OR user_id_second='$secondUid')");

        if (sizeof($data) > 0) {
            return $data[0]['id'];
        } else {
            // no db entry for chat exists, create one
            return $this->createDbLog($secondUid);
        }
    }

    /**
     * sets log path and dir
     * @param int $chatId 
     */
    private function setChatIdAndLogPath($chatId) {
        $this->chatId = $chatId;
        $this->logDir = __DIR__ . $this->logPath . $this->chatId . "/";
        $this->logFilepath = $this->logDir . date("Y-m-d") . ".html";
    }

    /**
     * reads all log files
     * @return String or false if no log directory
     */
    private function readLogFile($chatId, $time = "last") {
        $this->setChatIdAndLogPath($chatId);
        // check if chat Log Directory exists
        if (!is_dir($this->logDir)) {
            // i does not exist, return false
            return false;
        }

        $dir = scandir($this->logDir);



        if ($time == "last") {
            // only read last two days a message occured
            $size = sizeof($dir);
            //$this->updateTimestamp($chatId);
            $logContent = "";

            if ($size > 1) {
                // get also second last if exists, if somebody answers to an old message
                if (is_file($this->logDir . $dir[$size - 2])) {
                    $logContent .= file_get_contents($this->logDir . $dir[$size - 2]);
                }
            }
            if ($size > 0) {
                if (is_file($this->logDir . $dir[$size - 1])) {
                    $logContent .= file_get_contents($this->logDir . $dir[$size - 1]);
                }
            }

            return $logContent;
        } else {
            $logContent = "";
            foreach ($dir as $f) {
                //echo $this->logDir.$f; // check later, maybe $f is just the filename and the path needs to be added...
                $logContent .= file_get_contents($this->logDir . $f);
            }
            //$this->updateTimestamp($chatId);
            return $logContent;
        }
    }

    /**
     * looks for changes in logfile and returns them
     * @param type $chatId
     * @return string changes or empty string
     */
    private function idleLogFile($chatId) {
        $this->setChatIdAndLogPath($chatId);
        // check if chat Log Directory exists
        if (!is_dir($this->logDir)) {
            // i does not exist, return false
            return "";
        }

        $messagesData = $this->select('messages', 'user_id,user_id_second,first_ts,second_ts', "id=$chatId");

        if ($messagesData[0]['user_id'] == $this->uId) {
            if ($messagesData[0]['first_ts'] < $messagesData[0]['second_ts']) {
                $this->updateTimestamp($chatId);
                return $this->readLogFile($chatId);
            }
        }
        if ($messagesData[0]['user_id_second'] == $this->uId) {
            if ($messagesData[0]['first_ts'] > $messagesData[0]['second_ts']) {
                $this->updateTimestamp($chatId);
                return $this->readLogFile($chatId);
            }
        }
    }

    /**
     * creates the Log File 
     */
    private function createLogFileIfNotExists() {
        //$room = $this->room;
        $logDir = dirname($this->logFilepath);
        //echo $logDir;
        if (!file_exists($logDir)) {
            mkdir($logDir, 0777, true);
        }
        $pfad = $this->logFilepath;
//        $ourFileHandle = fopen($pfad, 'w') or die("can't open file ($pfad)".__DIR__);
//        fclose($ourFileHandle);
        if (!is_file($pfad)) {
            if (false === file_put_contents($pfad, "")) {
                echo "$pfad konnte nicht angelegt werden... Entschuldigung";
            }

//            if ($this->private) {
//                if (false === file_put_contents($pfad, "")) {
//                    echo "$pfad konnte nicht angelegt werden... Entschuldigung";
//                }
//            } else {
//                if (false === file_put_contents($pfad, "")) {
//                    echo "$pfad konnte nicht angelegt werden... Entschuldigung";
//                }
//            }
        }
    }

    public function getNotificationCategories() {
        return array('chat');
    }

    public function getNotificationSettings($data) {
        return $this->fetch('notifications_settings', $data);
    }

}
