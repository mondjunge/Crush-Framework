<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'created private conversation...' =>
	'hat eine private Konversation begonnen...'
	,
	'created Chatroom...'=>
	'hat den Chatraum erstellt...'
	,
	'Uhr'=>
	' Uhr'
	,
	'Private conversation with'=>
	'Private Unterhaltung mit'
	,
	'loading messages...'=>
	'lade Nachrichten...'
	,
	'Leave your friend a message...'=>
	'Hinterlasse Deinem Kontakt eine Nachricht...'
	,
	'send your message with [shift]+[enter].'=>
	'[shift]+[enter] drücken um Nachricht zu versenden.'
	,
	'Contacts'=>
	'Kontakte'
	,
	'send'=>
	'senden'
	,
	'no contacts... :('=>
	'Du hast keine Kontakte... :('
	,
	'Warning: Using this chat on a mobile device can cause rapid battery loss!'=>
	'Achtung: Diesen Chat auf einem Handy zu nutzen kann die Akkulaufzeit extrem verkürzen!'
	,
	'Insufficient rights!'=>
	'Du musst Dich anmelden um diese Funktion zu nutzen.'
	,
	'New Message on'=>
	'Neue Nachricht auf'
	,
	'Hello'=>
	'Hallo'
	,
	'User is offline, E-Mail was send to notice him.'=>
	'Der Nutzer ist offline, es wurde eine E-Mail versendet um ihn zu benachrichtigen.'
	,
	'You have a new message from'=>
	'Du hast eine neue Nachricht von'
	,
	'Answer online by visiting following Link'=>
	'Antworte online indem Du folgenden link besuchst'
	,
	'User is offline. System will send him/her an E-Mail if you begin to chat now.'=>
	'Der Nutzer ist offline. Es wird versucht ein E-Mail zu senden, wenn Du jetzt eine Nachricht schreibst.'
	,
	'New Message from $2'=>
	'Neue private Nachricht von $2'
	,
	'Hello $1,'=>
	'Hallo $1, '
	,
	'there is a new message from $2.'=>
	'$2 hat Dir eine neue Nachricht geschrieben.'
	,
	'Take a look now under following link:'=>
	'Unter folgendem Link kannst Du die Unterhaltung einsehen:'
	,
	'User is offline, no E-Mail could be send, because user have deactivated notifications.'=>
	'Der Nutzer ist offline. Es wird keine E-Mail versendet, da der Nutzer diese Funktionalität deaktiviert hat.'
	,
	'Error: You cannot chat with yourself!'=>
	'Fehler: Du kannst nicht mit Dir selber chatten!'
	,
	'User is offline, something went wrong while sending him an E-Mail.'=>
	'Der Nutzer ist offline. Es konnte keine E-Mail an ihn gesendet werden.'
	,
	'contact writes you a chat message, but you are offline.'=>
	'Kontakt schreibt Dir eine Chat Nachricht, aber Du bist offline.'
	,
	'private chat'=>
	'Privater Chat'
);