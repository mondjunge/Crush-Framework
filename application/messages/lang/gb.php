<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Uhr' =>
	'Uhr'
	,
	'Private conversation with'=>
	'Private conversation with'
	,
	'loading messages...'=>
	'loading messages...'
	,
	'Leave your friend a message...'=>
	'Leave your friend a message...'
	,
	'send your message with [shift]+[enter].'=>
	'send your message with [shift]+[enter].'
	,
	'Contacts'=>
	'Contacts'
	,
	'send'=>
	'send'
	,
	'send your message with [ctrl]+[enter].'=>
	'send your message with [ctrl]+[enter].'
	,
	'no contacts... :('=>
	'no contacts... :('
	,
	'Warning: Using this chat on a mobile device can cause rapid battery loss!'=>
	'Warning: Using this chat on a mobile device can cause rapid battery loss!'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'User is Online.'=>
	'User is Online.'
	,
	'New Message onhttp://localhost'=>
	'New Message onhttp://localhost'
	,
	'Hello'=>
	'Hello'
	,
	'You have a new message:'=>
	'You have a new message:'
	,
	''=>
	''
	,
	'User is offline, E-Mail was send to notice him.'=>
	'User is offline, E-Mail was send to notice him.'
	,
	'You have a new message from'=>
	'You have a new message from'
	,
	'Answer online by visiting following Link:'=>
	'Answer online by visiting following Link:'
	,
	'User is offline. System will send him/her an E-Mail if you begin to chat now.'=>
	'User is offline. System will send him/her an E-Mail if you begin to chat now.'
	,
	'New Comment'=>
	'New Comment'
	,
	'User is offline, no E-Mail could be send, because user have deactivated notifications.'=>
	'User is offline, no E-Mail could be send, because user have deactivated notifications.'
	,
	'New Message'=>
	'New Message'
	,
	'Hello $1,'=>
	'Hello $1,'
	,
	'there is a new message from $2.'=>
	'there is a new message from $2.'
	,
	'Take a look now under following link:'=>
	'Take a look now under following link:'
	,
	'Error: You cannot chat with yourself!'=>
	'Error: You cannot chat with yourself!'
	,
	'User is offline, something went wrong while sending him an E-Mail.'=>
	'User is offline, something went wrong while sending him an E-Mail.'
	,
	'contact writes you a chat message, but you are offline.'=>
	'contact writes you a chat message, but you are offline.'
	,
	'private chat'=>
	'private chat'
	,
	'New Message from $2'=>
	'New Message from $2'
);