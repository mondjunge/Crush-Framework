<?php

/**
 *  Copyright © tim 17.04.2014
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Files extends AppController {

    private $targetDir = null; //initial
    private $fileType = 'image'; // default
    private $userMode = 0; // global (2) superadmin shared (1) or User specific (0) (default)
    private $subDir = "";
    private $baseUploadPath = "upload/";

    public function __construct() {
        ini_set('memory_limit', '1024M');
        ini_set('post_max_size', '1024M');
        ini_set('upload_max_filesize', '1024M');
    }

    private function setTargetDir($targetDir) {
        $this->targetDir = $targetDir;
    }

    private function setUserMode($userMode) {
        if ($this->checkUserRights(1)) {
            // only superusers may change the userMode
            $this->userMode = $userMode;
            if ($userMode == 2) {
                // when in "supermode", set filetype file to upload and access all files.
                $this->setFileType("file");
            } 
            if ($userMode == 3) {
                // when in "gallerymode", set filetype file to upload and access all files.
                $this->setFileType("file");
            } 
        } else {
            $this->userMode = 0;
        }
    }

    private function setFileType($fileType) {
        if ($fileType != "") {
            $this->fileType = $fileType;
        }
    }

    private function setSubDir($subDir) {
        if (strlen($subDir) > 0 && $subDir != "" && $subDir != "/" && substr($subDir, -1, 1) != "/") {

            $subDir = $subDir . "/";
            //$dirsA[] = "/";
        }
        if ($subDir == "/") {
            $subDir = "";
        }
        if (substr($subDir, 0, 3) == '../') {
            $subDir = "";
        }
        $this->subDir = $subDir;
    }

    private function getTargetDir() {

        if ($this->targetDir == null) {
            switch ($this->userMode) {
                case 0:
                    // user folders

                    $userId = Session::get('uid');
                    $this->setTargetDir($this->baseUploadPath . "users/" . $userId . "/" . $this->fileType . "s/" . $this->subDir);

                    break;

                case 1:
                    // shared admin folders
                    $this->setTargetDir($this->baseUploadPath . $this->fileType . "s/" . $this->subDir);

                    break;

                case 2:
                    // shared admin folders
                    $this->setTargetDir($this->baseUploadPath . $this->subDir);

                    break;

                case 3:
                    // shared gallers folders
                    $this->setTargetDir($this->baseUploadPath . "images/gallery/" . $this->subDir);

                    break;

                default:
                    // user folders

                    $userId = Session::get('uid');
                    $this->setTargetDir($this->baseUploadPath . "users/" . $userId . "/" . $this->fileType . "s/" . $this->subDir);

                    break;
            }

            if (substr($this->targetDir, -1, 1) != "/") {
                $this->targetDir .= "/";
            }
        }

        return $this->targetDir;
    }
    
    /**
     * gets all files in upload, rewrite points to this.
     * restricts access to gallary to logged in users
     * @return type
     */
    private function getFile(){
         /**
          * potential SECURITY RISK! be careful when changing
         * Hook for loading files
         * TODO: permission system, global, personal
         */
        $this->standalone = true;
        //echo "haha"; exit;
        // this loc seperates secure data from total dataloss.
        $fileurl = str_replace("../","",filter_input(INPUT_GET, 'getfile'));
        
        $fullPath = __DIR__."/../../upload/".$fileurl;
        
        if(!is_file($fullPath)){
            header("Content-type:". mime_content_type(__DIR__."/../../theme/".Config::get('siteStyle')."/images/logo.png"));
            readfile(__DIR__."/../../theme/".Config::get('siteStyle')."/images/logo.png");
            return;
        }
        
        /**
         * Only show gallery pictures if user have rights to view gallery
         */
        if(strpos($fileurl, 'gallery/') !== false && !$this->checkSiteRights('gallery')){
            header("Content-type:". mime_content_type(__DIR__."/../../theme/".Config::get('siteStyle')."/images/logo.png"));
            readfile(__DIR__."/../../theme/".Config::get('siteStyle')."/images/logo.png");
            return;
        }
        
        //read file
        header("Content-type:". mime_content_type($fullPath));
        header("Content-Length: ".filesize($fullPath));
        header('Content-disposition: filename="'.basename($fileurl).'"');
        readfile($fullPath);
        return;
    }

    public function index() {
       
        if (filter_has_var(INPUT_GET, 'getfile')) {
           $this->getFile();
           return;
        }
        //put your code here
        $this->autoCheckRights();

        $this->includeJs(Config::get('applicationDirectory') . '/files/js/.files.js');

        // hack for ckeditor integration
        $data['contentOnly'] = "";
        if (filter_has_var(INPUT_GET, 'contentOnly')) {
            $data['contentOnly'] = "&contentOnly";
        }

        // set fileType globally
        if (filter_has_var(INPUT_GET, 'fileType')) {
            $this->setFileType(filter_input(INPUT_GET, 'fileType', FILTER_UNSAFE_RAW));
        }

        if (filter_has_var(INPUT_GET, 'userMode')) {
            $this->setUserMode(filter_input(INPUT_GET, 'userMode', FILTER_UNSAFE_RAW));
        }

        if (filter_has_var(INPUT_GET, 'subdir')) {
            $this->setSubDir(filter_input(INPUT_GET, 'subdir', FILTER_UNSAFE_RAW));
        }

        if (filter_has_var(INPUT_GET, 'changeDir')) {
            $this->standalone = true;
            $this->setSubDir(filter_input(INPUT_GET, 'changeDir', FILTER_UNSAFE_RAW));
            echo $this->selectFile();
            return;
        }
        if (filter_has_var(INPUT_GET, 'createDir')) {
            $this->standalone = true;
            $this->createDir(filter_input(INPUT_GET, 'createDir', FILTER_UNSAFE_RAW));
            return;
        }
        if (filter_has_var(INPUT_GET, 'deleteDir')) {
            $this->standalone = true;
            $this->deleteDir(filter_input(INPUT_GET, 'deleteDir', FILTER_UNSAFE_RAW));
            return;
        }
        if (filter_has_var(INPUT_GET, 'deleteFile')) {
            $this->standalone = true;
            $this->deleteFile(filter_input(INPUT_GET, 'deleteFile', FILTER_UNSAFE_RAW));
            return;
        }

        if (filter_has_var(INPUT_GET, 'selectFile')) {
            $this->standalone = true;
            echo $this->selectFile();
            return;
        }
        if (filter_has_var(INPUT_GET, 'getFileHtml')) {
            $this->standalone = true;

            $path = filter_input(INPUT_GET, 'getFileHtml', FILTER_UNSAFE_RAW);
            $data['path'] = $path;

            echo "<div class=\'mediacontent\'>" . $this->fetch($this->fileType . 'Html', $data) . "</div>";
            return;
        }
        if (filter_has_var(INPUT_GET, 'uploadFile')) {
            $this->standalone = true;

            $this->setSubDir(filter_input(INPUT_POST, 'subdir', FILTER_UNSAFE_RAW));
            $this->setFileType(filter_input(INPUT_POST, 'fileType', FILTER_UNSAFE_RAW));
            $this->uploadFile();
            echo $this->selectFile();
            return;
        }
        if (filter_has_var(INPUT_GET, 'fileType')) {
            $data['filemanager'] = $this->selectFile();
        } else {
            $data['filemanager'] = $this->selectFile();
        }

        if (filter_has_var(INPUT_GET, 'CKEditorFuncNum')) {
            $data['CKEditorFuncNum'] = filter_input(INPUT_GET, 'CKEditorFuncNum', FILTER_UNSAFE_RAW);
        }
        $this->view('files', $data);
    }

    public function admin() {
        //put your administrative code here
        $this->autoCheckRights();
    }

    private function selectFile() {
        $filesA = array();
        $dirsA = array();
        if (!in_array($this->fileType, array(0 => 'video', 1 => 'image', 2 => 'file'))) {
            exit;
        }
        $userImageDirPath = __DIR__ . "/../../" . $this->getTargetDir();

        if (!is_dir($userImageDirPath)) {
            mkdir($userImageDirPath, 0777, true);
            chmod($userImageDirPath, 0777);
        }
        $dir = scandir($userImageDirPath);

        foreach ($dir as $f) {
            if (is_file($userImageDirPath . $f) && substr($f, 0, 9) != ".htaccess") {
                $fi = array();
                $fi['fileType'] = $this->getTypeOfFile($f);
                $fi['path'] = Config::get('relativeUrl') . $this->getTargetDir() . $f;
                $fi['last_modified'] = date("Y-m-d H:i:s", filemtime($userImageDirPath . $f));
                $filesize = filesize($userImageDirPath . $f);
                $sizeUnit = "Bytes";
                if ($filesize > 1024) {
                    $filesize = $filesize / 1024;
                    $sizeUnit = "KB";
                }
                if ($filesize > 1024) {
                    $filesize = $filesize / 1024;
                    $sizeUnit = "MB";
                }
                if ($filesize > 1024) {
                    $filesize = $filesize / 1024;
                    $sizeUnit = "GB";
                }

                $fi['file_size'] = number_format($filesize, 1) . $sizeUnit;
                $filesA[] = $fi;
            }
            if (is_dir($userImageDirPath . $f) && substr($f, 0, 1) != ".") {
                $dirsA[] = $f;
            }
        }

        $data['styleUrl'] = Config::get('relativeUrl') . 'theme/' . Config::get('siteStyle') . '/'; //'/' . $aConfig['relativeUrl'] . '/

        $data['files'] = $this->sortFiles($filesA, 'last_modified');
        $data['dirs'] = $dirsA;
        $data['subdir'] = $this->subDir;
        $data['fileType'] = $this->fileType;
        $data['userMode'] = $this->userMode;
        /**
         * TODO
         */
        // date("F d Y H:i:s.", filectime($filename));

        return $this->fetch('selectFile', $data);
    }

    private function getTypeOfFile($f) {

        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false !== $ext = array_search(
                $finfo->file($this->getTargetDir() . $f), array(
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif'
                ), true
                )) {
            return "image";
        }

        if (false !== $ext = array_search(
                $finfo->file($this->getTargetDir() . $f), array(
            'ogv' => 'video/ogg',
            'mp4' => 'video/mp4'
                ), true
                )) {
            return "video";
        }

        if (false !== $ext = array_search(
                $finfo->file($this->getTargetDir() . $f), array(
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'rar2' => 'application/x-rar',
                ), true
                )) {
            return "archive";
        }

        if (false !== $ext = array_search(
                $finfo->file($this->getTargetDir() . $f), array(
            'mpeg3' => 'audio/mpeg',
            'mp3' => 'audio/mp3'
                ), true
                )) {
            return "audio";
        }

        if (false !== $ext = array_search(
                $finfo->file($this->getTargetDir() . $f), array(
            'txt' => 'text/plain',
            'diff' => 'text/x-diff'
                ), true
                )) {
            return "text";
        }

        if (false !== $ext = array_search(
                $finfo->file($this->getTargetDir() . $f), array(
            'pdf' => 'application/pdf'
                ), true
                )) {
            return "pdf";
        }

        return "file";
    }

    private function deleteFile($name) {
        if(str_contains($name, "../")){
            // f*~* off
            exit; die;
        }
        $target_dir = $this->getTargetDir() . $name . ""; //Session::get('uid');
        $target_path = $target_dir;

        if (is_file($target_path)) {
            if (!unlink($target_path)) {
                //echo "<div class='error'>" . $this->ts("Could not delete file.") . "</div>";
                $this->setErrorMessage($this->ts("Could not delete file."));
            } else {
                //echo "<div class='system'>" . $this->ts("File") . " '" . $name . "' " . $this->ts("deleted..") . "</div>";
                $this->setSystemMessage($this->ts("File") . " '" . $name . "' " . $this->ts("deleted.."));
            }
        } else {
            //echo "<div class='error'>" . $this->ts("Not a file.") . ": " . $target_path . "</div>";
            $this->setErrorMessage($this->ts("Not a file.") . ": " . $target_path);
        }
        echo $this->selectFile();
        return;
    }

    private function deleteDir($name) {
        $target_dir = $this->getTargetDir() . $name . "/"; //Session::get('uid');
        $target_path = $target_dir;

        if (is_dir($target_path)) {
            if (!rmdir($target_path)) {
                //echo "<div class='error'>" . $this->ts("Could not delete directory. It needs to be empty in order to be deleted.") . "</div>";
                $this->setErrorMessage($this->ts("Could not delete directory. It needs to be empty in order to be deleted."));
            } else {
                $this->setSystemMessage($this->ts("Directory deleted.."));
                //echo "<div class='system'>" . $this->ts("Directory deleted..") . "</div>";
            }
        } else {
            //echo "<div class='error'>" . $this->ts("Not a directory") . ": " . $target_path . "</div>";
            $this->setErrorMessage($this->ts("Not a directory") . ": " . $target_path);
        }
        echo $this->selectFile();
        return;
    }

    private function createDir($name) {
        $target_dir = $this->getTargetDir() . $name; //Session::get('uid');

        $target_path = $target_dir;
        if (!is_dir($target_path)) {

            mkdir($target_path, 0777, true);
            chmod($target_path, 0777);
        }
        echo $this->selectFile();
        return;
    }

    private function uploadFile() {
        $fileType = $this->fileType;
        $resizeImgOnUpload = false;
        $n = 0;
        $filesUploaded = 0;
        while (isset($_FILES['file-' . $n]) && is_file($_FILES['file-' . $n]['tmp_name'])) {
            $currentFile = $_FILES['file-' . $n];

            if ($fileType == 'image') {
                if (false === $ext = $this->checkImage($currentFile['tmp_name'])) {
                    return;
                }
                // shall the images be resized on upload?
                if (filter_input(INPUT_POST, 'resizeImgOnUpload') == 'true') {
                    $resizeImgOnUpload = true;
                }
                //$this->setErrorMessage("RESIZE ON UPOLAD: (".$resizeImgOnUpload.")"); return;
            }
            if ($fileType == 'video') {
                if (false === $ext = $this->checkVideo($currentFile['tmp_name'])) {
                    return;
                }
            }
            if ($fileType == 'file') {
                if (false === $ext = $this->checkFile($currentFile['tmp_name'])) {
                    return;
                }
            }

            $target_dir = $this->getTargetDir();

            $target_path = $target_dir;
            if (!is_dir($target_path)) {

                mkdir($target_path, 0777, true);
                chmod($target_path, 0777);
            }

            $filename = basename($currentFile['name']);

            $target_file_path = dirname(__FILE__) . '/../../' . $target_path . $filename;

            if ($resizeImgOnUpload) {
                $maxImgWidth = filter_input(INPUT_POST, 'maxImgWidth', FILTER_SANITIZE_NUMBER_INT);
                $maxImgHeight = filter_input(INPUT_POST, 'maxImgHeight', FILTER_SANITIZE_NUMBER_INT);
                if (Thumb::Instance()->resizeImage($currentFile['tmp_name'], $target_file_path, $maxImgWidth, $maxImgHeight, $ext)) {
                    chmod($target_file_path, 0777);
                    ++$filesUploaded;
                }
            } elseif (move_uploaded_file($currentFile['tmp_name'], $target_file_path)) {
                chmod($target_file_path, 0777);
                //echo "<div class='system'>" . $this->ts("File uploaded successful") . "</div>";
                ++$filesUploaded;
            } else {
                //echo "<div class='error'>" . $this->ts("Error uploading Image.") . "</div>";
            }
            ++$n;
        }
        if ($filesUploaded == $n) {
            //echo "<div class='system'>" . $filesUploaded . " " . $this->ts("file(s) uploaded successful") . "</div>";
            $this->setSystemMessage($filesUploaded . " " . $this->ts("file(s) uploaded successful"));
        } else {
            // echo "<div class='error'>" . $this->ts("Error uploading file(s).") . "</div>";
            $this->setErrorMessage($this->ts("Error uploading file(s)."));
        }
        return;
    }

    private function checkImage($file) {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search(
                $finfo->file($file), array(
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif'
                ), true
                )) {
            //throw new RuntimeException('Invalid file format.');
            //echo "<div class='error'>" . $this->ts("Invalid file format. Only jpg, png and gif is allowed.") . "</div>";
            $this->setErrorMessage($this->ts("Invalid file format. Only jpg, png and gif is allowed."));
            unset($file);
            return false;
        } else {
            return $ext;
        }
    }

    private function checkVideo($file) {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search(
                $finfo->file($file), array(
            'ogv' => 'video/ogg',
            'mp4' => 'video/mp4',
            'mpeg' => 'video/mpeg'
                ), true
                )) {
            // throw new RuntimeException('Invalid file format.');
            //echo "<div class='error'>" . $this->ts("Invalid file format. Only ogv and mp4 is allowed.") . "</div>";
            $this->setErrorMessage($this->ts("Invalid file format. Only ogv and mp4 is allowed."));
            unset($file);
            return false;
        } else {
            return $ext;
        }
    }

    private function checkFile($file) {
        $finfo = new finfo(FILEINFO_MIME_TYPE);

        if (false === $ext = array_search(
                $finfo->file($file), array(
            'jpg' => 'image/jpeg',
            'png' => 'image/png',
            'gif' => 'image/gif',
            'ogv' => 'video/ogg',
            'mp4' => 'video/mp4',
            'mpeg' => 'video/mpeg',
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            '2.rar' => 'application/x-rar',
            'mpeg3' => 'audio/mpeg',
            'mp3' => 'audio/mp3',
            'txt' => 'text/plain',
            'diff' => 'text/x-diff',
            'pdf' => 'application/pdf'
                ), true
                )) {
            //throw new RuntimeException('Invalid file format.');
            //echo "<div class='error'>" . $this->ts("Invalid file format. Only jpg, png, ogv, mp4, zip, rar, pdf, mp3 and txt is allowed.") . "</div>";
            $this->setErrorMessage($this->ts("Invalid file format. Only jpg, png, ogv, mp4, zip, rar, pdf, mp3 and txt is allowed."));
            unset($file);
            return false;
        } else {
            return $ext;
        }
    }

    private function sortFiles($files, $key, $sort = "desc") {


        $sorter = array();
        $ret = array();
        reset($files);
        foreach ($files as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $files[$ii];
        }
        //$ret = ;
        if ($sort == "desc") {
            $ret = array_reverse($ret);
        }
        return $ret;
    }
}
