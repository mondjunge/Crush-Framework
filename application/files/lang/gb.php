<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Only jpeg, png and gif files allowed!' =>
	'Only jpeg, png and gif files allowed!'
	,
	'upload image'=>
	'upload image'
	,
	'upload'=>
	'upload'
	,
	'upload a file...'=>
	'upload a file...'
	,
	'choose a file by clicking...'=>
	'choose a file by clicking...'
	,
	'File uploaded successful'=>
	'File uploaded successful'
	,
	'+ create a new dir'=>
	'+ create a new dir'
	,
	'back'=>
	'back'
	,
	'+ upload new files'=>
	'+ upload new files'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'create'=>
	'create'
	,
	'dirname'=>
	'dirname'
	,
	'Do you really want to delete this category? All trails in it will be lost.'=>
	'Do you really want to delete this category? All trails in it will be lost.'
	,
	'Do you really want to delete this dir? All files in it will be lost.'=>
	'Do you really want to delete this dir? All files in it will be lost.'
	,
	'Not a directory'=>
	'Not a directory'
	,
	'Could not delete directory. Sorry!'=>
	'Could not delete directory. Sorry!'
	,
	'Directory deleted..'=>
	'Directory deleted..'
	,
	'Could not delete directory. It needs to be empty in order to be deleted.'=>
	'Could not delete directory. It needs to be empty in order to be deleted.'
	,
	'Do you really want to delete this directory?'=>
	'Do you really want to delete this directory?'
	,
	'Do you really want to delete this file?'=>
	'Do you really want to delete this file?'
	,
	'Not a file.'=>
	'Not a file.'
	,
	'File deleted..'=>
	'File deleted..'
	,
	'File'=>
	'File'
	,
	'deleted..'=>
	'deleted..'
	,
	'file(s) uploaded successful'=>
	'file(s) uploaded successful'
	,
	'Only ogv and mp4 files allowed!'=>
	'Only ogv and mp4 files allowed!'
	,
	'upload video'=>
	'upload video'
	,
	'upload '=>
	'upload '
	,
	'upload videos'=>
	'upload videos'
	,
	'video'=>
	'video'
	,
	'videos'=>
	'videos'
	,
	'images'=>
	'images'
	,
	'create a new dir'=>
	'create a new dir'
	,
	'upload new files'=>
	'upload new files'
	,
	'Error uploading file(s).'=>
	'Error uploading file(s).'
	,
	'Invalid file format. Only jpg, png and gif is allowed.'=>
	'Invalid file format. Only jpg, png and gif is allowed.'
	,
	'upload file'=>
	'upload file'
	,
	'Invalid file format. Only jpg, png, ogv, mp4, zip, rar, pdf, mp3 and txt is allowed, but was: '=>
	'invalid file format. Only jpg, png, ogv, mp4, zip, rar, pdf, mp3 and txt is allowed, but was: '
	,
	'resize images on upload'=>
	'resize images on upload'
	,
	'max width'=>
	'max width'
	,
	'max height'=>
	'max height'
	,
	'files'=>
	'files'
	,
	'animated gif, click to play'=>
	'animated gif, click to play'
);