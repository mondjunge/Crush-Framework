<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Only jpeg, png and gif files allowed!' =>
	'Nur jpeg, png und gif Dateien sind erlaubt!'
	,
	'upload image'=>
	'Datei(en) hochladen'
	,
	'upload'=>
	'hochladen'
	,
	'upload a file...'=>
	'lade eine Datei hoch...'
	,
	'choose a file by clicking...'=>
	'Wähle eine Datei durch klicken aus...'
	,
	'File uploaded successful'=>
	'Datei wurde erfolgreich hochgeladen.'
	,
	'create a new dir'=>
	'neues Verzeichnis anlegen'
	,
	'back'=>
	'⇦ zurück'
	,
	'upload new files'=>
	'neue Dateien hochladen'
	,
	'Insufficient rights!'=>
	'Keine Rechte für diese Aktion'
	,
	'create'=>
	'erstellen'
	,
	'dirname'=>
	'Verzeichnisname'
	,
	'Not a directory'=>
	'Kein Verzeichnis'
	,
	'Directory deleted..'=>
	'Verzeichnis gelöscht...'
	,
	'Could not delete directory. It needs to be empty in order to be deleted.'=>
	'Konnte das Verzeichnis nicht löschen. Es muß leer sein um gelöscht zu werden.'
	,
	'Do you really want to delete this directory?'=>
	'Möchtest Du wirklich dieses Verzeichnis löschen?'
	,
	'Do you really want to delete this file?'=>
	'Möchtest Du wirklich diese Datei löschen?'
	,
	'Not a file.'=>
	'Keine Datei.'
	,
	'File'=>
	'Datei'
	,
	'deleted..'=>
	'wurde erfolgreich gelöscht.'
	,
	'file(s) uploaded successful'=>
	'Datei(en) hochgeladen.'
	,
	'Only ogv and mp4 files allowed!'=>
	'Nur ogv und mp4 Dateien sind erlaubt!'
	,
	'upload video'=>
	'Videos hochladen'
	,
	'upload '=>
	'hochladen'
	,
	'upload videos'=>
	'Videos hochladen'
	,
	'videos'=>
	'Videos'
	,
	'images'=>
	'Bilder'
	,
        'files'=>
	'Dateien'
	,
	'Error uploading file(s).'=>
	'Fehler beim hochladen der Datei(en).'
	,
	'Invalid file format. Only jpg, png and gif is allowed.'=>
	'Ungültige Datei. Zulässige Dateien sind jpg, png und gif Dateien.'
	,
	'upload file'=>
	'Datei(en) hochladen'
	,
	'Invalid file format. Only zip, rar, mp3 and txt is allowed.'=>
	'Ungültige Datei. Zulässige Dateien sind zip, rar, mp3 und txt Dateien.'
	,
	'Allowed files are: jpeg, png, gif ogv, mp4, mp3, rar, zip, pdf and txt.'=>
	'Zulässig Dateien sind: jpg, png, gif, ogv, mp4, mp3, rar, zip, pdf und txt.'
	,
	'Invalid file format. Only jpg, png, ogv, mp4, zip, rar, pdf, mp3 and txt is allowed.'=>
	'Ungültige Datei. Zulässige Dateien sind jpg, png, gif, ogv, mp4, mp3, rar, zip, pdf und txt Dateien.'
	,
	'resize images on upload'=>
	'Bilder beim hochladen verkleinern'
	,
	'max width'=>
	'max. Breite'
	,
	'max height'=>
	'max. Höhe'
	,
	'animated gif, click to play'=>
	'Animiertes gif, klicken zum abspielen.'
);