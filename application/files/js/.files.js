/**
 * files
 */

use_package('modules');
modules.files = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    var fileSelectElement = null;
    var userMode = 0;
    var self = this;
    this.init = function () {
        modules.files.userMode = $("#userMode").data('value');
        console.log("userMode " + modules.files.userMode);
        modules.files.initHandler();
        modules.files.handlers();
    };

    this.fadeSystemAndErrorMessages = function () {
        //this.init();
        //setTimeout('modules.theme.SystemMessageHandler.handleSystemErrorMessageFadeOut()', 10000);
    };

    this.initHandler = function () {
        $(".fileSelect").off('click');
        $(".fileSelect").on("click",function () {

            modules.files.fileSelectElement = $(this);
            var fileType = modules.files.fileSelectElement.data('type');
            var subdir = modules.files.fileSelectElement.data('subdir');
            var url = reLangUrl + "files/index?selectFile&fileType=" + fileType;
            url += "&userMode=" + modules.files.userMode;
            if (typeof subdir != 'undefined') {
                url += "&changeDir=" + subdir;
            }
            console.log('click on select ' + fileType);
            var element = this;
            $.ajax({
                type: "GET",
                url: url,
                async: true,
                success: function (html) {
                    utils.overlay.show(html, element, "modules.files.init");
                    modules.files.handlers();
                    modules.files.selectClickHandler();
                }
            });
            return false;
        });
    };

    this.handlers = function () {
        modules.files.changeDirHandler();
        modules.files.uploadClickHandler();
        modules.files.handleResizeOnUpload();
        modules.files.createDirHandler();
        modules.files.deleteDirHandler();
        modules.files.deleteFileHandler();
    };
    
     this.handleResizeOnUpload = function(){
        
        $("input[name='resizeImgOnUpload']").off('change');
        $("input[name='resizeImgOnUpload']").change(function (){
            console.log("checkbox clicked");
            console.log(this.checked);
            if(this.checked){
                $("input[name='maxImgWidth']").removeAttr('disabled');
                $("input[name='maxImgHeight']").removeAttr('disabled');
            }else{
                $("input[name='maxImgWidth']").attr('disabled', 'disabled');
                $("input[name='maxImgHeight']").attr('disabled', 'disabled');
            }
        });
    };

    this.deleteFileHandler = function () {
        $('.delFileButton').off('click');
        $('.delFileButton').on("click",function () {
            var confirmText = $(this).data('confirm');
            if (confirmText != '') {
                confirmResult = confirm(confirmText);
            }
            if (confirmResult === false) {
                return false;
            }

//            $(".error").hide();
//            $(".system").hide();
            var subdir = $("input[name='subdir']").val();
            var file = $(this).data('file');
            var type = $("input[name='fileType']").val();
            var url = reLangUrl + "files/index?deleteFile=" + encodeURIComponent(file) + "&fileType=" + type + "&subdir=" + subdir + "&ajax";
            url += "&userMode=" + modules.files.userMode;
            console.log("delete File:" + subdir + file);
            $.ajax({
                type: "GET",
                url: url,
                async: true,
                success: function (html) {
                    //alert('muar');
                    $('#selectFileContent').replaceWith(html);
                    //                    modules.files.changeDirHandler();
                    //                    modules.files.selectClickHandler();
                    //                    modules.files.uploadClickHandler();
                    //                    modules.files.createDirHandler();
                    //                    modules.files.deleteDirHandler();
                    modules.files.handlers();
                    modules.files.selectClickHandler();
                    self.fadeSystemAndErrorMessages();//setTimeout('modules.theme.SystemMessageHandler.handleSystemErrorMessageFadeOut()', 10000);

                }

            });
            return false;
        });
    }
    this.deleteDirHandler = function () {
        $('.delDirButton').off('click');
        $('.delDirButton').on("click",function () {

            var confirmText = $(this).data('confirm');
            if (confirmText != '') {
                confirmResult = confirm(confirmText);
            }
            if (confirmResult === false) {
                return false;
            }

//            $(".error").hide();
//            $(".system").hide();

            var subdir = $("input[name='subdir']").val();
            var dir = $(this).data('dir');
            var type = $("input[name='fileType']").val();
            var url = reLangUrl + "files/index?deleteDir=" + encodeURIComponent(dir) + "&fileType=" + type + "&subdir=" + encodeURIComponent(subdir) + "&ajax";
            url += "&userMode=" + modules.files.userMode;
            console.log("delete Dir:" + subdir + dir);
            $.ajax({
                type: "GET",
                url: url,
                async: true,
                success: function (html) {
                    //alert('muar');
                    $('#selectFileContent').replaceWith(html);
                    modules.files.handlers();
                    modules.files.selectClickHandler();
                    self.fadeSystemAndErrorMessages();//setTimeout('modules.theme.SystemMessageHandler.handleSystemErrorMessageFadeOut()', 10000);

                }

            });
            return false;
        });
    }

    this.uploadClickHandler = function () {
        $(".uploadFileButton").off('click');
        $(".uploadFileButton").on("click",function () {
            console.log("upload clicked!");
            $(".uploadFilesForm").toggle('fast');
        });
        $("#uploadFile").off('click');
        $("#uploadFile").on("click",function () {
            console.log("upload clicked!");

            modules.files.uploadFile();
        });

    }

    this.uploadFile = function () {
        utils.waiting.startWait();
        var subdir = $('#subdir').val();
        var fileType = $('#fileType').val();
        var resizeImgOnUpload = $("input[name='resizeImgOnUpload']").is(':checked');
        var maxImgWidth = $("input[name='maxImgWidth']").val();
        var maxImgHeight = $("input[name='maxImgHeight']").val();

        if (resizeImgOnUpload && (maxImgWidth === '0' || maxImgHeight === '0')) {
            alert('0px is not a valid image dimension. Abort.');
            utils.waiting.stopWait();
            return;
        }

        var url = reLangUrl + "files/index?uploadFile";
        url += "&userMode=" + modules.files.userMode;

        console.log("upload clicked..");
        console.log("fileType: " + fileType);
        console.log("subdir: " + subdir);
        console.log("resizeImgOnUpload: " + resizeImgOnUpload);

        var data = new FormData();

        jQuery.each($('#file')[0].files, function (i, file) {
            data.append('file-' + i, file);
        });
        // data.append('files',$('#file')[0].files);

        data.append('subdir', subdir);
        data.append('fileType', fileType);
        data.append('resizeImgOnUpload', ""+resizeImgOnUpload);
        data.append('maxImgWidth', maxImgWidth);
        data.append('maxImgHeight', maxImgHeight);
        $.ajax({
            url: url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            timeout: 0,
            success: function (html) {
                console.log('file Upload success!');
                $('#selectFileContent').replaceWith(html);
                modules.files.handlers();
                modules.files.selectClickHandler();
                utils.waiting.stopWait();
                self.fadeSystemAndErrorMessages();//setTimeout('modules.theme.SystemMessageHandler.handleSystemErrorMessageFadeOut()', 10000);
            }
            ,
            progress: function (evt) {
                if (evt.lengthComputable) {
                    var percent = parseInt((evt.loaded / evt.total * 100), 10) + "%";
                    $('#uploadProgress > span').html(percent);
                    $('#uploadProgress').css('width', percent);
                    //console.log("Progress: " + parseInt( (evt.loaded / evt.total * 100), 10) + "%");
                } else {
                    console.log("Progress: Length not computable.");
                }
            },
            progressUpload: function (evt) {
                if (evt.lengthComputable) {
                    if ($('#uploadProgress').hasClass('hidden')) {
                        $('#uploadProgress').removeClass('hidden');
                    }
                    var percent = parseInt((evt.loaded / evt.total * 100), 10) + "%";
                    $('#uploadProgress > span').html(percent);
                    $('#uploadProgress').css('width', percent);
                    //console.log("Upload " + parseInt( (evt.loaded / evt.total * 100), 10) + "%");
                } else {
                    console.log("Upload: Length not computable.");
                }
            },
            error: function (x, t, m) {
                if (t === "timeout") {
                    alert("Timeout: Es hat zu lange gedauert die Datei hoch zu laden.");
                } else {
                    alert(t);
                }
            }
        });
    };

    this.createDirHandler = function () {
        $(".createDirButton").off('click');
        $(".createDirButton").on("click",function () {
            $(".createNewDirForm").toggle('fast');
            $("input[name='dirName']").focus();
        });

        $("#createDir").off('click');
        $("#createDir").on("click",function () {
            console.log("upload clicked!");

            modules.files.createDir();
        });
    };

    this.createDir = function () {
        var subdir = $("input[name='subdir']").val();
        var dir = $("input[name='dirName']").val();
        var type = $("input[name='fileType']").val();
        var url = reLangUrl + "files/index?createDir=" + encodeURIComponent(dir) + "&fileType=" + type + "&subdir=" + encodeURIComponent(subdir) + "&ajax";
        url += "&userMode=" + modules.files.userMode;
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            success: function (html) {
                //alert('muar');
                $('#selectFileContent').replaceWith(html);
                modules.files.handlers();
                modules.files.selectClickHandler();
                self.fadeSystemAndErrorMessages();//setTimeout('modules.theme.SystemMessageHandler.handleSystemErrorMessageFadeOut()', 10000);

            }

        });
    };

    this.changeDirHandler = function () {

        $(".dir").off('click');
        $(".dir").on("click",function () {
            console.log('click on change dir');

            var dir = $(this).find('.dirName').text();
            var subdir = $('#subdir').val();

            if (dir == '..') {
                var subdirsA = subdir.split('/');
                var subdirLength = subdirsA.length;
                var newSubDir = "";
                console.log('subdirLength:' + subdirLength);

                for (var i = 0; i < subdirLength - 2; i++) {
                    newSubDir += subdirsA[i] + "/";
                }

                dir = newSubDir;
            } else {
                if (dir != '/') {
                    dir = subdir + dir;
                }
            }



            console.log("dir:" + dir);

            var type = $('#fileType').val();
            console.log("type:" + type);

            modules.files.changeDir(dir, type);
            return false;
        });
    };

    this.changeDir = function (dir, type) {
        utils.waiting.startWait();
        var url = reLangUrl + "files/index?changeDir=" + encodeURIComponent(dir) + "&fileType=" + type + "&ajax";
        url += "&userMode=" + modules.files.userMode;
        $.ajax({
            type: "GET",
            url: url,
            async: true,
            success: function (html) {
                utils.waiting.stopWait();
                //alert('muar');
                $('#selectFileContent').replaceWith(html);
                modules.files.handlers();
                modules.files.selectClickHandler();

            }

        });
    };

    this.testSelectClickHandler = function () {
        alert('clicked!');
        return false;
    };

    this.selectClickHandler = function () {

        // if there is no fileSelectElement, we are in fileBrowser mode and do not need a selectClickHandler
        if (typeof modules.files.fileSelectElement === 'undefined' || modules.files.fileSelectElement === null) {
            return;
        }

        var selectClickHandler = modules.files.fileSelectElement.data('selecthandler');
        var callback = modules.files.fileSelectElement.data('callback');

        if (typeof selectClickHandler === 'undefined' || selectClickHandler === '') {

            /**
             * assume selector is an input and we can set a value..
             */
            $('.selectableFile').on("click",function () {
                var path = $(this).data("path");
                var fileType = $(this).data("file-type");

                modules.files.fileSelectElement.data("path", path);
                utils.overlay.hide();
                console.log(fileType);

                if (typeof callback !== 'undefined' || callback !== '') {
                    /**
                                    * TODO:
                                    * make this work without eval in profilpage.
                                    * first guess is, it does not work cause the clickhandler function 
                                    * is found later in the javascript code than this is executed
                                    */
                    console.log("callback: " + callback);
                    //window[callback]();
                    eval(callback + "()");
                }

            });


        } else {
            /**
             * TODO:
             * make this work without eval in profilpage.
             * first guess is, it does not work cause the clickhandler function 
             * is found later in the javascript code than this is executed
             */
            console.log("executing "+selectClickHandler);
            eval(selectClickHandler + "()");
            //window[selectClickHandler]();
        }
    };


//    
//    this.ajaxStart = function(){
//        
//    }


};