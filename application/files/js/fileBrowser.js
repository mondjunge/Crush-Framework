/**
 * fileBrowser
 */

use_package('modules');
modules.fileBrowser = new function () {
    base.main.ModuleRegistry.registerModule(this);
    base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    var CKEditorFuncNum = null;

    this.init = function () {
        if ($("#CKEditorFuncNum").length > 0) {
            CKEditorFuncNum = $("#CKEditorFuncNum").data('value');
            console.log("CKEditorFuncNum "+CKEditorFuncNum);
            modules.fileBrowser.handleCkEditorIntegration();
        }else{
            modules.fileBrowser.handleFileClicks();
        }
    };

    this.ajaxRefresh = function () {
        this.init();
    };
    
    this.handleCkEditorIntegration = function () {
        $('.selectableFile').off('click');
        $('.selectableFile').on("click",function () {
            var fileUrl = $(this).data("path");
            window.opener.CKEDITOR.tools.callFunction( CKEditorFuncNum, fileUrl , function() {
                
                //CKEditor callback function
                // Get the reference to a dialog window.
                var dialog = this.getDialog();
                // Check if this is the Image Properties dialog window.
                if ( dialog.getName() === 'image' ) {
                    console.log("image properties dialog found "+fileUrl);
                    // Get the reference to a text field 
                    
                    var element = dialog.getContentElement( "info", "txtAlt" );
                    if ( element ){
                        console.log("alt");
                        element.setValue( "Ein Bild" );
                    }
                    
                    var elementWidth = dialog.getContentElement( "info","txtWidth" );
                    if ( elementWidth ){
                        console.log("width");
                        elementWidth.setValue( '100%' );
                    }
                    
                    var elementHeight = dialog.getContentElement( "info","txtHeight" );
                    if ( elementHeight ){
                        console.log("height");
                        elementHeight.setValue( '100%' );
                    }
                    
                    var elementUrl = dialog.getContentElement( "info","txtUrl" );
                    if ( elementUrl ){
                        console.log("url");
                        elementUrl.setValue( fileUrl );
                    }
                    
                }
                //return false; // image plugin of ckeditor will ignore 'return false;' and change the dimension.
                // Return "false" to stop further execution. In such case CKEditor will ignore the second argument ("fileUrl")
                // and the "onSelect" function assigned to the button that called the file manager (if defined).
                // return false;
            });
            window.close();
        });
    };

    this.handleFileClicks = function () {
        $('.selectableFile').off('click');
        $('.selectableFile').on("click",function () {
            var path = $(this).data("path");
            var fileType = $(this).data("file-type");

            console.log(fileType);
            var html = "";
            /**
             * height of 100% can break layout on (mobile) safari and older IE
             */
            if (fileType == 'image') {
                html = "<div style='text-align:center;' ><img src='" + path + "' style='max-width:100%; max-height: 80vh; '/></div>";

            }
            if (fileType == 'video') {
                html = "<div style='text-align:center;' ><a href='" + path + "' target='_blank' >Download</a></div>";
                html += "<div style='text-align:center;' ><video src='" + path + "' style='max-width:100%; max-height: 85vh;' controls> </video></div>";
                
            }
            if (fileType == 'audio') {
                html = "<div style='text-align:center;' ><a href='" + path + "' target='_blank' >Download</a></div>";
                html += "<div style='text-align:center;' ><audio src='"+path+"' controls /></div>";

            }
            if(fileType == 'pdf'){
                html = "<div style='text-align:center;' ><a href='" + path + "' target='_blank' >Download</a></div>"
                html += "<div style='text-align:center;' ><iframe src='"+path+"' style='width:100%; height: 80vh;' /></div>";
            }
            if (fileType == 'file' || fileType == 'archive' || fileType == 'text') {
                html = "<div style='text-align:center;' ><a href='" + path + "' target='_blank' >Download</a></div>";

            }
            
            utils.overlay.show(html, this);


        });
    }
//    
//    this.ajaxStart = function(){
//        
//    }


}