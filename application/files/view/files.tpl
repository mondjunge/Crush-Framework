<?php
$ckeditorUrlAddition = "";
if (isset($CKEditorFuncNum)) :
    $ckeditorUrlAddition = "&CKEditorFuncNum=$CKEditorFuncNum";
    ?>

    <span id="CKEditorFuncNum" data-value="<?php echo $CKEditorFuncNum; ?>" ></span>
<?php endif; ?>
<span id="userMode" data-value="<?php echo $userMode; ?>" ></span>
<?php if($userMode < 2) : ?>
<a class="pure-button pure-button-small" href="<?php echo $this->action('files?fileType=file&viewFiles');
echo $ckeditorUrlAddition; echo "&userMode=".$userMode; echo $contentOnly; ?>" ><?php echo $this->ts('files'); ?></a>

<a class="pure-button pure-button-small" href="<?php echo $this->action('files?fileType=image&viewFiles');
echo $ckeditorUrlAddition; echo "&userMode=".$userMode; echo $contentOnly; ?>" ><?php echo $this->ts('images'); ?></a>

<a class="pure-button pure-button-small" href="<?php echo $this->action('files?fileType=video&viewFiles');
echo $ckeditorUrlAddition; echo "&userMode=".$userMode; echo $contentOnly; ?>" ><?php echo $this->ts('videos'); ?></a>
<?php endif; ?>

<?php echo $filemanager; ?>