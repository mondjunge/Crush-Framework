<div id="selectFileContent">
    <div id="fileActions">
        <a href="#" accesskey="d" class="pure-button pure-button-small pure-button-files createDirButton" data-current="<?php echo $subdir ?>">
            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#folder', 'svgIcon svgIcon-dark fileIcon'); ?>
            <span class="imgBtnText"><?php echo $this->ts('create a new dir'); ?></span></a>
        <a href="#" accesskey="u" class="pure-button pure-button-small pure-button-files uploadFileButton" data-current="<?php echo $subdir ?>">
            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#cloud-upload', 'svgIcon svgIcon-dark fileIcon'); ?>
            <span class="imgBtnText"><?php echo $this->ts('upload new files'); ?></span></a>
    </div>

    <div class="createNewDirForm" style="display:none;">
        <form class="pure-form">
<!--            <input name="subdir" type="hidden" value="<?php echo $subdir ?>" />
            <input name="fileType" type="hidden" value="<?php echo $fileType ?>" />-->
            <input type="text" name="dirName" autocomplete="off" placeholder="<?php echo $this->ts('dirname'); ?>"/>
            <button class="pure-button pure-button-primary  pure-button-small" id="createDir" onclick="return false;" name="createDir" type="submit" ><?php echo $this->ts("create") ?></button>
        </form>
    </div>
    <div class="uploadFilesForm" style="display:none;">
        <?php if ($fileType == "image"): ?>
            <div class="help">
                <?php echo $this->ts('Only jpeg, png and gif files allowed!'); ?>
            </div>
        <?php endif; ?>
        <?php if ($fileType == "video"): ?>
            <div class="help">
                <?php echo $this->ts('Only ogv and mp4 files allowed!'); ?>
            </div>
        <?php endif; ?>
        <?php if ($fileType == "file"): ?>
            <div class="help">
                <?php echo $this->ts('Allowed files are: jpeg, png, gif ogv, mp4, mp3, rar, zip, pdf and txt.'); ?>
            </div>
        <?php endif; ?>
        <form class="pure-form" action="<?php echo $this->action("files/index") ?>" method="post" enctype="multipart/form-data">

            <input id="subdir" name="subdir" type="hidden" value="<?php echo $subdir ?>" />
            <input id="fileType" name="fileType" type="hidden" value="<?php echo $fileType ?>" />
            <label for="file" ><?php echo $this->ts("upload " . $fileType) ?></label>
<!--            <input class="pure-button" id="singlefile" name="singlefile" accept="<?php echo $fileType ?>/*" type="file"/>
            -->
            <input class="pure-button" id="file" name="file[]" multiple="multiple" accept="<?php echo $fileType ?>/*" type="file"/>
            <a href="#" class="pure-button pure-button-primary" id="uploadFile" onclick="return false;" name="uploadFile" >
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#cloud-upload', 'svgIcon svgIcon-light fileIcon'); ?>
                <span class="imgBtnText"><?php echo $this->ts("upload") ?></span>
            </a>
            <?php if ($fileType == "image"): ?>
                <br/>
                <label for="resizeImgOnUpload" class="pure-checker" tabindex="0" title="<?php echo $this->ts("resize images on upload") ?>">
                    <div class="checker">
                        <input id="resizeImgOnUpload" class="checkbox" name="resizeImgOnUpload" type="checkbox" checked="checked" />
                        <div class="check-bg"></div>
                        <div class="checkmark">
                            <svg viewBox="0 0 100 100">
                            <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </div>
                    </div>
                    <span class="label"><?php echo $this->ts("resize images on upload") ?>. </span>
                </label>
                <input type="text" name="maxImgWidth" size="4" value="1920"/>px&nbsp;
                <label for="maxImgWidth"><?php echo $this->ts('max width'); ?></label>
                <input type="text" name="maxImgHeight" size="4" value="1920"/>px&nbsp;
                <label for="maxImgHeight"><?php echo $this->ts('max height'); ?></label>
                
            <?php endif; ?>
        </form>
        <div id="uploadProgress" class="hidden"><span class="percent">0%</span></div>
    </div>


    <div>
        <div class="dirTile">
            <a href="#" class="pure-button pure-button-small dir" data-file-type="<?php echo $fileType ?>"><span class="dirName">/</span></a>
            <span class="subdir"><?php echo $subdir ?></span>
        </div>
    </div>

    <div class="">
        <div class="">
            <?php if (sizeof($files) > 0) : ?>
                <?php echo $this->ts('choose a file by clicking...'); ?>
            <?php else: ?>
                <?php echo $this->ts('upload a file...'); ?>
            <?php endif; ?>
        </div>
        <?php if ($subdir != '') : ?>
            <div class="imageTile">
                <div class="imagePadding">
                    <a href="#" class="dir" >
                        <?php echo $this->image('folder.png', 'folder-icon', '128'); ?>
                        <span class="dirName">..</span>
                    </a>
                </div>
            </div>
        <?php endif; ?>
        <?php foreach ($dirs as $d) : ?>
            <div class="imageTile">
                <div class="imagePadding">
                    <a class="dir" href="#" >
                        <?php echo $this->image('folder.png', 'folder-icon', '128'); ?>
                        <span class="dirName"><?php echo $d ?></span>

                    </a>
                    <div class="editButtons">
                        <a class="delDirButton" 

                           data-confirm="<?php echo $this->ts('Do you really want to delete this directory?'); ?>"
                           data-dir="<?php echo $d; ?>" href="<?php echo $this->action('files?delDir=' . $d); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-dark fileDeleteIcon'); ?></a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

        <?php foreach ($files as $i) : ?>
            <div class="imageTile">
                <div class="imagePadding">
                    <a href="#" class="selectableFile" data-path="<?php echo $i['path'] ?>" data-file-type="<?php echo $i['fileType'] ?>">

                        <?php if ($i['fileType'] == "image"): ?>
                            <?php echo $this->image($i['path'], 'image', "144", ""); ?>
                        <?php endif; ?>
                        <?php if ($i['fileType'] == "video"): ?>
                            <video src="<?php echo $i['path'] ?>" width="100%" height="100%" >
                                <div> Schade – hier käme ein Video, wenn Ihr Browser HTML 5 
                                    Unterstützung für OGG hätte, wie z.B. der aktuelle Firefox</div>
                            </video>
                        <?php endif; ?>
                        <?php if ($i['fileType'] == "archive"): ?>
                            <?php echo $this->image('archive.png', 'folder-icon', '128'); ?>
                        <?php endif; ?>
                        <?php if ($i['fileType'] == "audio"): ?>
                            <?php echo $this->image('audio.png', 'folder-icon', '128'); ?>
                        <?php endif; ?>
                        <?php if ($i['fileType'] == "text"): ?>
                            <?php echo $this->image('text.png', 'folder-icon', '128'); ?>
                        <?php endif; ?>
                        <?php if ($i['fileType'] == "file"): ?>
                            <?php echo $this->image('file.png', 'folder-icon', '128'); ?>
                        <?php endif; ?>
                        <?php if ($i['fileType'] == "pdf"): ?>
                            <?php echo $this->image('pdf.png', 'folder-icon', '128'); ?>
                        <?php endif; ?>

                    </a>
                    <div class="editButtons">
                        <a class="delFileButton" 
                           data-confirm="<?php echo $this->ts('Do you really want to delete this file?'); ?>"
                           data-file="<?php echo basename($i['path']) ?>" href="<?php echo $this->action('files?delFile=' . basename($i['path'])); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-dark fileDeleteIcon'); ?></a>
                    </div>
                    <div class="imgdetail">
                        <span class=""><?php echo basename($i['path']); //substr(basename($i['path']), 0, 18);                 ?></span>
                        <br/>
                        <span class=""><?php echo $i['last_modified']; ?></span>
                        <br/>
                        <span class=""><?php echo $i['file_size']; ?></span>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </div>
</div>