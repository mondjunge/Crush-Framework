<div class="blog_buttons">
    <a href="<?php echo $this->action('rss') ?>"><?php echo $this->image('rss_logo.png', '', '24', '24', $this->ts('RSS Feed')) ?></a>
</div>
<div class="blog">

    <?php $n = 1;
    foreach ($tags as $tag) : ?>

        <div id="blogTab<?php echo $n ?>" class="blogTab">
            <div id="blogGroup<?php echo $n ?>" class="blogGroup ui-widget-content">
                <h3> 
                    <?php
                    if ($tag == '') {
                        echo $this->ts("Untagged Entries");
                    } else {
                        echo $tag;
                    } 
                    ?>
                </h3>

                    <?php foreach ($articles["$tag"] as $art) : ?>
                    <p>
                        <a href="<?php echo $this->action('blog/').urlencode(htmlspecialchars_decode($art['title'])); ?>">
                    <?php echo $art['title']; ?>
                        </a>
                    </p>
    <?php endforeach; ?>

            </div>
        </div>

    <?php ++$n;
endforeach; ?>
    <!--     <div id="blogTab2" >
            <div id="blogGroup2" class="ui-widget-content">2</div>
        </div>
         <div id="blogTab3" >
            <div id="blogGroup3" class="ui-widget-content">3</div>
        </div>
         <div id="blogTab4" >
            <div id="blogGroup4" class="ui-widget-content">4</div>
        </div>
         <div id="blogTab5" >
            <div id="blogGroup5" class="ui-widget-content">5</div>
        </div>
         <div id="blogTab6" >
            <div id="blogGroup6" class="ui-widget-content">6</div>
        </div>-->
    <!--    
        <div id="blogGroup2" class="ui-widget-content"></div>
        <div id="blogGroup3" class="ui-widget-content"></div>
        <div id="blogGroup4" class="ui-widget-content"></div>
        <div id="blogGroup5" class="ui-widget-content"></div>
        <div id="blogGroup6" class="ui-widget-content"></div>-->
</div>
<div style="clear:both;"></div>