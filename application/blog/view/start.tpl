<?php if ($editLink): ?>
    <p>  
        <a id="newArticleButton" class="pure-button pure-button-primary" href="<?php echo $this->action('blog/admin?new') ?>"><?php echo $this->ts('+ add new article') ?></a>
    </p>
<?php endif; ?>
<?php if ($mainArticle!=false): ?>
<div id="news-position">
    <div class="blog_article">
        <h2 class="blog_heading">
            <a href="<?php echo $this->action('blog/'). $mainArticle['dateurl'] .'/' . str_replace('%2F', '/', urlencode($mainArticle['title'])) ?>" ><?php echo $mainArticle['title']; ?></a></h2>

        <?php if ($editLink): ?>
            <div><a href="<?php echo $this->action('blog/admin?edit=') . $mainArticle['id'] ?>" ><?php echo $this->ts('edit') ?></a></div>
        <?php endif; ?>
        <div class="date"><?php echo $mainArticle['date']; ?></div>
        <div class="blog_text">
            <?php echo $mainArticle['text'] ?>
            <a class="blog_read" href="<?php echo $this->action('blog/'). $mainArticle['dateurl'] .'/' . str_replace('%2F', '/', urlencode($mainArticle['title'])) ?>" ><?php echo $this->ts('continue reading...'); ?></a>
        </div>

        <div class="tags">
            <?php echo $this->ts('Tags') ?>:
            <?php foreach ($mainArticle['tags'] as $tag) : ?>
                <a class="blog_tags" href="<?php echo $this->action('blog?tag=' . urlencode($tag)) ?>" ><?php echo $tag ?></a>
            <?php endforeach; ?>                
        </div>

    </div>
</div>

<div class="pure-g pure-g-r">
    <?php if (isset($secondaryArticles[0]) && $secondaryArticles[0]['title'] != '') : ?>
    <div class="pure-u-1-3">
        <div class="blog_article">
            <h2 class="blog_heading">
                <a href="<?php echo $this->action('blog/'). $secondaryArticles[0]['dateurl'] .'/' . str_replace('%2F', '/', urlencode($secondaryArticles[0]['title'])) ?>" ><?php echo $secondaryArticles[0]['title']; ?></a></h2>

            <?php if ($editLink): ?>
                <div><a href="<?php echo $this->action('blog/admin?edit=') . $secondaryArticles[0]['id'] ?>" ><?php echo $this->ts('edit') ?></a></div>
            <?php endif; ?>
            <div class="date"><?php echo $secondaryArticles[0]['date']; ?></div>
            <div class="blog_text">
                <?php echo $secondaryArticles[0]['text'] ?>
                <a class="blog_read" href="<?php echo $this->action('blog/'). $secondaryArticles[0]['dateurl'] .'/' . str_replace('%2F', '/', urlencode($secondaryArticles[0]['title'])) ?>" ><?php echo $this->ts('continue reading...'); ?></a>
            </div>

            <div class="tags">
                <?php echo $this->ts('Tags') ?>:
                <?php foreach ($secondaryArticles[0]['tags'] as $tag) : ?>
                    <a class="blog_tags" href="<?php echo $this->action('blog?tag=' . urlencode($tag)) ?>" ><?php echo $tag ?></a>
                <?php endforeach; ?>                
            </div>

        </div>
    </div>
    <?php endif; ?>
    <?php if (isset($secondaryArticles[1]) && $secondaryArticles[1]['title'] != '') : ?>
    <div class="pure-u-1-3">
        <div class="blog_article">
            <h2 class="blog_heading">
                <a href="<?php echo $this->action('blog/'). $secondaryArticles[1]['dateurl'] .'/' . str_replace('%2F', '/', urlencode($secondaryArticles[1]['title'])) ?>" ><?php echo $secondaryArticles[1]['title']; ?></a></h2>

            <?php if ($editLink): ?>
                <div><a href="<?php echo $this->action('blog/admin?edit=') . $secondaryArticles[1]['id'] ?>" ><?php echo $this->ts('edit') ?></a></div>
            <?php endif; ?>
            <div class="date"><?php echo $secondaryArticles[1]['date']; ?></div>
            <div class="blog_text">
                <?php echo $secondaryArticles[1]['text'] ?>
                <a class="blog_read" href="<?php echo $this->action('blog/'). $secondaryArticles[1]['dateurl'] .'/' . str_replace('%2F', '/', urlencode($secondaryArticles[1]['title'])) ?>" ><?php echo $this->ts('continue reading...'); ?></a>
            </div>

            <div class="tags">
                <?php echo $this->ts('Tags') ?>:
                <?php foreach ($secondaryArticles[1]['tags'] as $tag) : ?>
                    <a class="blog_tags" href="<?php echo $this->action('blog?tag=' . urlencode($tag)) ?>" ><?php echo $tag ?></a>
                <?php endforeach; ?>                
            </div>

        </div>
    </div>
    <?php endif; ?>
    <?php if (isset($secondaryArticles[2]) && $secondaryArticles[2]['title'] != '') : ?>
    <div class="pure-u-1-3">
        <div class="blog_article">
            <h2 class="blog_heading">
                <a href="<?php echo $this->action('blog/'). $secondaryArticles[2]['dateurl'] .'/' . str_replace('%2F', '/', urlencode($secondaryArticles[2]['title'])) ?>" ><?php echo $secondaryArticles[2]['title']; ?></a></h2>

            <?php if ($editLink): ?>
                <div><a href="<?php echo $this->action('blog/admin?edit=') . $secondaryArticles[2]['id'] ?>" ><?php echo $this->ts('edit') ?></a></div>
            <?php endif; ?>
            <div class="date"><?php echo $secondaryArticles[2]['date']; ?></div>
            <div class="blog_text">
                <?php echo $secondaryArticles[2]['text'] ?>
                <a class="blog_read" href="<?php echo $this->action('blog/'). $secondaryArticles[2]['dateurl'] .'/' . str_replace('%2F', '/', urlencode($secondaryArticles[2]['title'])) ?>" ><?php echo $this->ts('continue reading...'); ?></a>
            </div>

            <div class="tags">
                <?php echo $this->ts('Tags') ?>:
                <?php foreach ($secondaryArticles[2]['tags'] as $tag) : ?>
                    <a class="blog_tags" href="<?php echo $this->action('blog?tag=' . urlencode($tag)) ?>" ><?php echo $tag ?></a>
                <?php endforeach; ?>                
            </div>

        </div>
    </div>
    <?php endif; ?>
    <div class="pure-u-1">
        <a class="pure-button pure-button-primary" href="<?php echo $this->action('blog'); ?>" ><?php echo $this->ts('more articles...'); ?></a>
    </div>

</div>
<?php endif; ?>