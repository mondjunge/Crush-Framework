<h1><?php echo $this->ts('Create a new article') ?></h1>
<form class="pure-form pure-form-stacked" action="<?php echo $this->action('blog/admin') ?>" method="post" enctype="multipart/form-data">
    <fieldset>

        <div class="pure-g pure-g-r">

            <div class="pure-u-1-2">
                <div class="pure-control-group">
                    <label for="title" ><?php echo $this->ts('Title') ?></label>
                    <input id="title" name="title" type="text"  />
                    <span class="help"><?php echo $this->ts('Title for the article'); ?></span>
                </div>

                <div class="pure-control-group">
                    <label for="tags" ><?php echo $this->ts('Tags') ?></label>
                    <input id="tags" name="tags" type="text" value=""/>
                </div>
                <p>
                <div class="tags">
                    <span ><?php echo $this->ts("Tags in use") ?></span>
                    <?php foreach ($tags as $tag) : ?>
                        <a class="pure-button pure-button-xsmall" href="#" onclick="addTag('<?php echo $tag ?>');return false;"><?php echo $tag ?></a>
                    <?php endforeach; ?>
                </div>
                </p>
                <p>
    <!--                <label for="language" ><?php echo $this->ts('Language') ?></label>-->
                    <input id="language" name="language" type="hidden" size="3" value="de"/>
                </p>

            </div>
            <div class="pure-u-1-2">
                <div class="pure-control-group">
                    <label for="role_need" ><?php echo $this->ts("Role need") ?></label>
                    <select id="role_need" name='role_need[]' multiple='multiple' size='5' style='width:150px'>

                        <option value="0" selected="selected" ><?php echo $this->ts("Everybody") ?></option>
                        <?php foreach ($roles as $role) : ?>
                            <option value="<?php echo $role['id'] ?>"><?php echo $role['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
<!--                <div class="pure-control-group">
                    <label for="allow_comments" ><?php echo $this->ts('Allow comments') ?></label>
                    <input id="allow_comments" name="allow_comments" type="checkbox" checked="checked" />
                </div>-->
                <div class="pure-control-group">
                    <label class="pure-checker" tabindex="0" title="<?php echo $this->ts('Allow comments') ?>">
                        <div class="checker">
                            <input class="checkbox" name="allow_comments" type="checkbox" checked="checked"/>
                            <div class="check-bg"></div>
                            <div class="checkmark">
                                <svg viewBox="0 0 100 100">
                                <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <span class="label"><?php echo $this->ts('Allow comments') ?></span>
                    </label>
                </div>
            </div>
        </div>
        <!-- <div class="pure-control-group">
                        
                             <label for="active" ><?php echo $this->ts('Active') ?></label>
                             <input id="active" name="active" type="hidden" value="0" />
             
            
         </div>-->

        <div style="clear:both"></div>
        <div class="pure-u-1-1">
            <textarea id="ckeditor" class="text" name="text" cols="50" rows="10" ></textarea>
        </div>

        <div>
            <p class="float">
                <label for="author" ><?php echo $this->ts('Author') ?></label>
                <input id="author" name="author" type="text" value="<?php echo $defaultAuthor ?>"/>

                <label for="date" ><?php echo $this->ts('Date') ?></label>
                <input id="date" class="date" name="date" type="date" size="10" value="<?php echo $date ?>" />
                
            </p>
        </div>
        <div style="clear:both"></div>
        <span class="help">
            <?php echo $this->ts('A date in the future will publish this article in the future and will NOT send notification e-mails.'); ?>
            <br>
            <?php echo $this->ts('Notification E-Mails will be send to users when you publish an article.'); ?>
            <br>
            <?php echo $this->ts('You can just create an article to work on and publish it later or create and publish this article in one go.'); ?>
        </span>
        <button name="insert" class="pure-button pure-button-primary" type="submit" ><?php echo $this->ts('Create article'); ?></button>
        <button name="insertandpublish" class="pure-button pure-button-primary" type="submit" ><?php echo $this->ts('Create & publish article'); ?></button>
        
    </fieldset>
</form>
<div style="clear:both"></div>
