<div class="blog">
    <div class="tags">
        <?php /* $tags[]="Alle"; */ foreach ($tags as $tag) : ?> 
            <?php if ((filter_has_var(INPUT_GET, 'tag') && urldecode(filter_input(INPUT_GET, 'tag')) == $tag) || (!filter_has_var(INPUT_GET, 'tag') && $tag == "Alle")): ?>
                <a class="blog_tags_selected" href="<?php echo $this->action('blog' . $adminContext . '?tag=' . urlencode($tag)) ?>" ><?php echo $tag ?></a>
            <?php else: ?>
                <a class="blog_tags" href="<?php echo $this->action('blog' . $adminContext . '?tag=' . urlencode($tag)) ?>" ><?php echo $tag ?></a>
            <?php endif; ?>
        <?php endforeach; ?>

    </div>
    <div class="blog_buttons">
        <?php if($isRssActive): ?>
        <a href="<?php echo $this->action('rss') ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#rss-alt', 'svgIcon svgIcon-dark rssIcon'); ?></a>
        <?php endif; ?>
    </div>
    <?php if ($editLink): ?>
        <div>  
            <a id="newArticleButton" class="pure-button pure-button-primary pure-button-small button-blog" href="<?php echo $this->action('blog/admin?new') ?>"><?php echo $this->ts('+ add new article') ?></a>
            <div style="clear:both" ></div>
        </div>
    <?php endif; ?>


    <?php foreach ($articles as $article) : ?>
        <?php if ($article['active'] == '1' || $editLink): ?>
            <?php if ($article['active'] == '0'): $inactiveArticleCss = 'inactiveArticle'; ?>
                <div class="articleNotPublished"><?php echo $this->ts('Article is not published yet!'); ?></div>
            <?php else: $inactiveArticleCss = ''; ?>

            <?php endif; ?>
            <div class="blog_article <?php echo $inactiveArticleCss; ?>">
                <div class="date dateMainList" ><?php echo $article['date']; ?></div>
                <h2 class="blog_heading">
                    <a href="<?php echo $this->action('blog/'). $article['dateurl'] .'/'. str_replace('%2F', '/', $article['linkTitle']) ?>" ><?php echo $article['title']; ?></a>
                </h2>

                <?php if ($editLink): ?>
                    <div><a href="<?php echo $this->action('blog/admin?edit=') . $article['id'] ?>" ><?php echo $this->ts('edit') ?></a></div>
                <?php endif; ?>

                <div class="blog_text">
                    <?php echo $article['text'] ?>
                    <br/>
                    <a class="blog_read" href="<?php echo $this->action('blog/'). $article['dateurl'] .'/'. str_replace('%2F', '/', $article['linkTitle']) ?>" ><?php echo $this->ts("continue reading...") ?></a>
                    <?php echo $article['commentsCount'] ." "; if($article['commentsCount'] == 1){ echo $this->ts('comment'); }else{ echo $this->ts('comments');}?>
                </div>

                <div class="tags">
                    <?php echo $this->ts('Tags') ?>:
                    <?php foreach ($article['tags'] as $tag) : ?>
                        <a class="blog_tags" href="<?php echo $this->action('blog' . $adminContext . '?tag=' . urlencode($tag)) ?>" ><?php echo $tag ?></a>
                    <?php endforeach; ?>                
                </div>

            </div>
        <?php endif; ?>
    <?php endforeach; ?>

    <div class="pure-u-1">
        <?php
        if ($selected_tag != "") {
            $tagVar = "&tag=" . $selected_tag;
        } else {
            $tagVar = "";
        }
        ?>
        <?php if (count($articles) >= 5): ?>
            <a class="pure-button pure-button-primary" href="<?php echo $this->action('blog' . $adminContext . '?page=') . ($page + 1) . $tagVar ?>" >
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#arrow-thick-left', 'svgIcon svgIcon-light'); ?>
                <?php echo $this->ts('Older Posts'); ?>
            </a>
        <?php endif; ?>
        <?php if ($page != 0): ?>
            <a class="pure-button pure-button-primary" href="<?php echo $this->action('blog' . $adminContext . '?page=') . ($page - 1) . $tagVar ?>" >
                <?php echo $this->ts('Newer Posts'); ?>
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#arrow-thick-right', 'svgIcon svgIcon-light'); ?>
            </a>
        <?php endif; ?>
    </div>

</div>