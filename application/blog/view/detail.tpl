<div class="blog_buttons">
    <a href="<?php echo $this->action('rss') ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#rss-alt', 'svgIcon svgIcon-dark rssIcon'); ?></a>
</div>
<!--<div class="tags">
    <a class="pure-button pure-button-primary pure-button-small button-blog" href="<?php echo $this->action('blog'); ?>" ><?php echo $this->ts('back'); ?></a>
</div>     -->
<h1 id="blogHeader"><?php echo $article['title']; ?></h1>

<?php if ($editLink): ?>
    <div><a href="<?php echo $this->action('blog/admin?edit=') . $article['id'] ?>" ><?php echo $this->ts('edit') ?></a></div>
<?php endif; ?>

<div class="blog">
    <div id="blogArticle">
        <?php echo $article['text']; ?>
    </div>
    <div id="authorAndDate">
        <div class="author"><?php echo $article['author']; ?></div> <div class="date"><?php echo $article['date']; ?></div>
    </div>
</div>
<div style="clear:both"><br/></div>
<div class="tags">
    <?php if (isset($lastNext['0'])) : ?>
        <a class="pure-button pure-button-primary pure-button-small button-blog" href="<?php echo $this->action('blog/'). $lastNext['0']['dateurl'] .'/' . $lastNext['0']['linkTitle']; ?>">
            <?php echo "&lt;&nbsp;" . $lastNext['0']['title']; ?>
        </a>
        &nbsp;
    <?php endif; ?>
    <?php if (isset($lastNext['1'])) : ?>
        <a class="pure-button pure-button-primary pure-button-small button-blog" href="<?php echo $this->action('blog/'). $lastNext['1']['dateurl'] .'/' . $lastNext['1']['linkTitle']; ?>">
            <?php echo $lastNext['1']['title'] . "&nbsp;&gt;"; ?>
        </a>
    <?php endif; ?>
</div>  

<div style="clear:both"></div>
<?php if ($article['allow_comments'] == '1') : ?>

    <div id="comments">
        <?php if (isset($comments) && count($comments) > 0) : ?>
            <h2><?php
                echo $this->ts('Comments');
                $n = 0;
                ?></h2>
            <?php foreach ($comments as $comment): ++$n; ?>

                <div  class="bubble_min">
                    <a style="float:right" name="comment<?php echo $comment['id'] ?>" href="#comment<?php echo $comment['id'] ?>" >#<?php echo $n ?></a>
                    <?php echo $comment['text'] ?>
                </div>
                <div class="after_bubble_before">
                    <div class="after_bubble_min"></div>
                    <div class="after_bubble_after_min"></div>

                </div>
                <div class="bubble_author">
                    <span class="comment_gra"><img src="<?php echo $comment['graUrl'] ?>" alt="Gravatar" /></span>
                    <span class="comment_name"><?php echo $comment['author'] ?></span> <span class="comment_date"><?php echo $comment['date'] ?></span>
                    <?php if ($editLink) : ?>
                        <span class="comment_name"><?php echo $comment['email'] ?></span>
                        <a href="<?php echo $this->action('blog/admin?comDel=' . $comment['id']) ?>" onclick="return deleteCheck();"><?php echo $this->ts('delete') ?></a>
                    <?php endif; ?>
                </div>
            <?php endforeach ?>
        <?php endif; ?>

    </div>
    <div id="entercomment" class="form">
        <a name="enterComment" ></a>
        <div  class="bubble">
            <noscript><em><?php echo $this->ts('JavaScript needs to be enabled in order to post comments'); ?></em></noscript>
            <h2><?php echo $this->ts('Write Comment'); ?></h2>
            <form class="pure-form pure-form-aligned" action="<?php echo $this->action('blog') ?>" method="post">
                <fieldset>
                    <input type="hidden" name="id" value="<?php echo $article['id'] ?>" />
                    <input type="hidden" name="title" value="<?php echo $article['title'] ?>" />
                    <div class="pure-control-group">
                        <label for="name" ><?php echo $this->ts('Name'); ?> *</label>
                        <input id="name" type="text" name="name" value="<?php echo $c_name ?>" maxlength="255"/>
                    </div>
                    <div class="pure-control-group">
                        <label for="email"  ><?php echo $this->ts('Email address'); ?></label>
                        <input id="email" type="text" name="email" value="<?php echo ($c_email != null ? $c_email : '') ?>" maxlength="255"/> <span class="help"><a href="http://www.gravatar.com" target="_blank" title="Gravatar Service">Gravatar</a> <?php echo $this->ts('support'); ?></span>
                    </div>
                    <p style="display: none;">
                        <label for="email2"  ><?php echo $this->ts('Repeat Email address'); ?></label>
                        <input id="email2" type="text" name="email2" value="" maxlength="255"/>
                    </p> 
                    <div class="comment pure-u-1-1">
                        <label for="text"><?php echo $this->ts('Comment'); ?> *</label>

                        <textarea id="text" name="text" class="text"><?php echo $c_text ?></textarea>
                    </div>  
                    <p>
                        <em> * <?php echo $this->ts('required'); ?></em>
                    </p>
                    <p>
                        <span id="commentButton" data-text="<?php echo $this->ts('Send comment'); ?>"></span>
                        <br/>
                        <em><?php echo $this->ts('I might answer your questions via email if you provide your address. I will not send spam, neither sell your address to someone else.'); ?></em>
                    </p>
                </fieldset>
            </form>
        </div>
        <div class="after_bubble_before">
            <div class="after_bubble"></div>
            <div class="after_bubble_after"></div>
        </div>
    </div>
<?php else: ?>
    <p>
        <em><?php echo $this->ts('Comments are deactivated for this article.'); ?></em>
    </p>
<?php endif; ?>