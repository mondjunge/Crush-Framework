<h1><?php echo $this->ts('Edit Article') ?></h1>


<form class="pure-form pure-form-stacked" action="<?php echo $this->action('blog/admin') ?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <input type="hidden" name="id" value="<?php echo $article['id'] ?>" />
        <div class="pure-g pure-g-r">
            <div class="pure-u-1-2">
                <div class="pure-control-group">
                    <label for="title" ><?php echo $this->ts('Title') ?></label>
                    <input id="title" name="title" type="text" value="<?php echo $article['title'] ?>"/>
                    <span class="help"><?php echo $this->ts('Title for the article'); ?></span>
                </div>

                <div class="pure-control-group">
                    <label for="tags" ><?php echo $this->ts('Tags') ?></label>
                    <input id="tags" name="tags" type="text" value="<?php echo $article['tags'] ?>"/>
                    <span class="help"><?php echo $this->ts('Seperate tags with comma.'); ?></span>
                </div>
                <p>
                <div class="tags">
                    <span ><?php echo $this->ts("Tags in use") ?></span>
                    <?php foreach ($tags as $tag) : ?>
                        <a class="pure-button pure-button-xsmall" href="#" onclick="addTag('<?php echo $tag ?>');return false;"><?php echo $tag ?></a>
                    <?php endforeach; ?>
                </div>
                </p>
                <p>
<!--                    <label for="language" ><?php echo $this->ts('Language') ?></label>-->
                    <input id="language" name="language" type="hidden" size="3" value="<?php echo $article['language'] ?>"/>
                </p>

            </div>
            <div class="pure-u-1-2">
                <div class="pure-control-group">
                    <label for="role_need" ><?php echo $this->ts("Role need") ?></label>
                    <select id="role_need" name='role_need[]' multiple='multiple' size='5' style='width:150px'>


                        <?php
                        $check = '';
                        if (in_array('0', $rnArray)) {
                            $check = "selected='selected'";
                        }
                        ?>
                        <option value="0" <?php echo $check ?> ><?php echo $this->ts("Everybody") ?></option>
                        <?php
                        foreach ($roles as $role) :
                            $check = '';
                            if (in_array($role['id'], $rnArray)) {
                                $check = "selected='selected'";
                            }
                            ?>

                            <option value="<?php echo $role['id'] ?>" <?php echo $check ?>><?php echo $role['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="pure-control-group">
                    <label class="pure-checker" tabindex="0" title="<?php echo $this->ts('Allow comments') ?>">
                        <div class="checker">
                            <input class="checkbox" name="allow_comments" type="checkbox" <?php echo $article['allow_comments'] ? 'checked="checked"': '' ?>/>
                            <div class="check-bg"></div>
                            <div class="checkmark">
                                <svg viewBox="0 0 100 100">
                                <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <span class="label"><?php echo $this->ts('Allow comments') ?></span>
                    </label>
                </div>
                <!--                    <div class="pure-control-group">
                                            <label for="active" ><?php echo $this->ts('Active') ?></label>
                                            <input id="active" name="active" type="checkbox"  />
                                    </div>-->
                <input id="active" name="active" type="hidden" value="<?php echo $article['active'] ?>" />
            </div>
        </div>
        <div style="clear:both"></div>
        <button name="update" type="submit" class="pure-button pure-button-primary" ><?php echo $this->ts('Update article') ?></button>
        <?php if (!$article['active']) : ?>
        <button name="updateandpublish" class="pure-button pure-button-primary" type="submit" ><?php echo $this->ts('Publish article'); ?></button>
        <?php else: ?>
        <button name="unpublish" class="pure-button pure-button-error" type="submit" ><?php echo $this->ts('Unpublish article'); ?></button>
        <?php endif; ?>
        <div style="clear:both"></div>

        <div>
            <textarea id="blog_article_content" class="text" name="text" cols="50" rows="10" ><?php echo $article['text'] ?></textarea>
        </div>

        <div>
            <p class="float">
                <label for="author"><?php echo $this->ts('Author') ?></label>
                <input id="author" name="author" type="text" value="<?php echo $article['author'] ?>"/>

                <label for="date"><?php echo $this->ts('Date') ?></label>
                <input id="date" class="date" name="date" type="date" size="10" value="<?php echo $article['date'] ?>" /><span class="help"><?php echo $this->ts('A date in the future will publish this article in the future and will NOT send notification e-mails.'); ?></span>
            </p>
        </div>
        <div style="clear:both"></div>
        <button name="update" type="submit" class="pure-button pure-button-primary" ><?php echo $this->ts('Update article') ?></button>
        <?php if (!$article['active']) : ?>
        <button name="updateandpublish" class="pure-button pure-button-primary" type="submit" ><?php echo $this->ts('Publish article'); ?></button>
        <?php else: ?>
        <button name="unpublish" class="pure-button pure-button-error" type="submit" ><?php echo $this->ts('Unpublish article'); ?></button>
        <?php endif; ?>
        <button name="delete" type="submit" class="pure-button pure-button-error" onclick="return deleteCheck();" ><?php echo $this->ts('Delete article') ?></button>
        <br/>
        <span class="help"><?php echo $this->ts('Notification E-Mails will be send to users when you publish an article.'); ?></span>
    </fieldset>
</form>
<div style="clear:both"></div>
