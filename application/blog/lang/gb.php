<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Untagged Entries' =>
	'Untagged Entries'
	,
	'Tag Group View'=>
	'Tag Group View'
	,
	'Untagged Entries'=>
	'Untagged Entries'
	,
	'read more...'=>
	'read more...'
	,
	'Older Posts'=>
	'Older Posts'
	,
	'Newer Posts'=>
	'Newer Posts'
	,
	'list view'=>
	'list view'
	,
	'group view'=>
	'group view'
	,
	'previous'=>
	'previous'
	,
	'next'=>
	'next'
	,
	'Blog Administration'=>
	'Blog Administration'
	,
	'Create new article'=>
	'Create new article'
	,
	'Create a new article'=>
	'Create a new article'
	,
	'Title'=>
	'Title'
	,
	'Author'=>
	'Author'
	,
	'Date'=>
	'Date'
	,
	'Tags'=>
	'Tags'
	,
	'Role need'=>
	'Role need'
	,
	'Everybody'=>
	'Everybody'
	,
	'Unsufficiant rights.'=>
	'Unsufficiant rights.'
	,
	'Active'=>
	'Active'
	,
	'Language'=>
	'Language'
	,
	'Allow comments'=>
	'Allow comments'
	,
	'Create article'=>
	'Create article'
	,
	'edit'=>
	'edit'
	,
	'Update article'=>
	'Update article'
	,
	'Edit Article'=>
	'Edit Article'
	,
	'Article could not be updated!'=>
	'Article could not be updated!'
	,
	'Updated article successfully.'=>
	'Updated article successfully.'
	,
	'Delete article'=>
	'Delete article'
	,
	'Article deleted.'=>
	'Article deleted.'
	,
	'Article inserted successfully'=>
	'Article inserted successfully'
	,
	'd|m|Y'=>
	'd|m|Y'
	,
	'back'=>
	'back'
	,
	'Share.png'=>
	'Share.png'
	,
	'Tag group view'=>
	'Tag group view'
	,
	'List view'=>
	'List view'
	,
	'Name'=>
	'Name'
	,
	'Email address'=>
	'Email address'
	,
	'Send comment'=>
	'Send comment'
	,
	'Comment'=>
	'Comment'
	,
	'required'=>
	'required'
	,
	'Write Comment'=>
	'Write Comment'
	,
	'JavaScript needs to be enabled in order to post comments'=>
	'JavaScript needs to be enabled in order to post comments'
	,
	'I might answer your questions via email if you provide your address. This site will not send spam, neither sell your address to someone else.'=>
	'I might answer your questions via email if you provide your address. This site will not send spam, neither sell your address to someone else.'
	,
	'I might answer your questions via email if you provide your address. I will not send spam, neither sell your address to someone else.'=>
	'I might answer your questions via email if you provide your address. I will not send spam, neither sell your address to someone else.'
	,
	'Comment send successfully.'=>
	'Comment send successfully.'
	,
	'd|m|Y H:i'=>
	'd|m|Y H:i'
	,
	'supported'=>
	'supported'
	,
	'support'=>
	'support'
	,
	'delete'=>
	'delete'
	,
	'Comment deleted'=>
	'Comment deleted'
	,
	'Comments'=>
	'Comments'
	,
	'You need to insert your name.'=>
	'You need to insert your name.'
	,
	'You need to insert a comment.'=>
	'You need to insert a comment.'
	,
	'You need to insert your name. <a href="#enterComment">to comment form</a>'=>
	'You need to insert your name. <a href="#enterComment">to comment form</a>'
	,
	'You need to insert a comment. <a href="#enterComment">to comment form</a>'=>
	'You need to insert a comment. <a href="#enterComment">to comment form</a>'
	,
	'RSS Feed'=>
	'RSS Feed'
	,
	'Something went wrong while getting the article or you are a hacker.'=>
	'Something went wrong while getting the article or you are a hacker.'
	,
	'Something went wrong while getting the article. Sorry.'=>
	'Something went wrong while getting the article. Sorry.'
	,
	'Please read the article before you place a comment.'=>
	'Please read the article before you place a comment.'
	,
	'Tags in use'=>
	'Tags in use'
	,
	'No older arcticles in stock, sorry!'=>
	'No older arcticles in stock, sorry!'
	,
	'+ add new article'=>
	'+ add new article'
	,
	'No arcticles in stock, sorry!'=>
	'No arcticles in stock, sorry!'
	,
	'No articles in stock, sorry!'=>
	'No articles in stock, sorry!'
	,
	'No older articles in stock, sorry!'=>
	'No older articles in stock, sorry!'
	,
	'Repeat Email address'=>
	'Repeat Email address'
	,
	'continue reading...'=>
	'continue reading...'
	,
	'Language not supported. Sorry.'=>
	'Language not supported. Sorry.'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'more articles...'=>
	'more articles...'
	,
	'Create & publish article'=>
	'Create & publish article'
	,
	'Comments are deactivated for this article.'=>
	'Comments are deactivated for this article.'
	,
	'Article is not published yet!'=>
	'Article is not published yet!'
	,
	'Article created successfully.'=>
	'Article created successfully.'
	,
	'Publish article'=>
	'Publish article'
	,
	'Unpublish article'=>
	'Unpublish article'
	,
	'Article created successfully, but it is not published yet.'=>
	'Article created successfully, but it is not published yet.'
	,
	'You can just create an article to work on and publish it later or create and publish this article in one go.'=>
	'You can just create an article to work on and publish it later or create and publish this article in one go.'
	,
	'Notification E-Mails will be send to users when you publish an article.'=>
	'Notification E-Mails will be send to users when you publish an article.'
	,
	'A date in the future will publish this article in the future and will NOT send notification e-mails.'=>
	'A date in the future will publish this article in the future and will NOT send notification e-mails.'
	,
	'Cannot insert Comment. Sorry.'=>
	'Cannot insert Comment. Sorry.'
	,
	'Title for the article'=>
	'Title for the article.'
	,
	'key not set!'=>
	'key not set!'
	,
	'Seperate tags with comma.'=>
	'Seperate tags with comma.'
	,
	'Y-m-d'=>
	'Y-m-d'
	,
	'calendar'=>
	'calendar'
	,
	'a new blog article was created.'=>
	'a new blog article was created.'
	,
	'blog'=>
	'blog'
	,
	'Hallo {user},<br/><br/>tim@9tw.de hat einen neuen Artikel auf http://localhost/Crush-Framework/ veröffentlicht.<br/><br/>Hier kommst Du direkt zum neuen Artikel: <br/><a href=\'{link}\' >{link}</a>'=>
	'Hallo {user},<br/><br/>tim@9tw.de hat einen neuen Artikel auf http://localhost/Crush-Framework/ veröffentlicht.<br/><br/>Hier kommst Du direkt zum neuen Artikel: <br/><a href=\'{link}\' >{link}</a>'
	,
	'Hello $1,'=>
	'Hello $1,'
	,
	'click following link to go directly to the source of this notification:'=>
	'click following link to go directly to the source of this notification:'
	,
	'To view all your notifications and mark them as read, go to this page:'=>
	'To view all your notifications and mark them as read, go to this page:'
	,
	'To change your notification settings, go to this page:'=>
	'To change your notification settings, go to this page:'
	,
	'$1 hat einen neuen Artikel auf $2 veröffentlicht.'=>
	'$1 published an new article on $2.'
	,
	'Neuigkeiten auf '=>
	'News on '
	,
	'$1 hat einen neuen Artikel veröffentlicht.'=>
	'$1 published a new article.'
	,
	'comments'=>
	'comments'
	,
	'Could not insert article. Sry, this is an never expected problem. Check your database.'=>
	'Could not insert article. Sry, this is an never expected problem. Check your database.'
);