<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Untagged Entries' =>
	'Einträge ohne Tag'
	,
	'read more...'=>
	'weiterlesen...'
	,
	'Older Posts'=>
	'Ältere Einträge'
	,
	'Newer Posts'=>
	'Neuere Einträge'
	,
	'list view'=>
	'Listenansicht'
	,
	'group view'=>
	'Gruppenansicht'
	,
	'previous'=>
	'vorheriger'
	,
	'next'=>
	'nächster'
	,
	'Tag Group View'=>
	'Gruppenansicht'
	,
	'Blog Administration'=>
	'Blog Verwaltung'
	,
	'Create a new article'=>
	'Einen neuen Beitrag erstellen'
	,
	'Title'=>
	'Titel'
	,
	'Author'=>
	'Autorin'
	,
	'Date'=>
	'Datum'
	,
	'Tags'=>
	'Schlagwörter'
	,
	'Role need'=>
	'Wer soll diesen Artikel sehen können?'
	,
	'Everybody'=>
	'Jeder'
	,
	'Unsufficiant rights.'=>
	'Unzureichende Rechte'
	,
	'Active'=>
	'Aktiv'
	,
	'Language'=>
	'Sprache'
	,
	'Allow comments'=>
	'Kommentare erlauben'
	,
	'Create article'=>
	'Erstelle Beitrag'
	,
	'edit'=>
	'bearbeiten'
	,
	'Update article'=>
	'Artikel aktualisieren'
	,
	'Edit Article'=>
	'Artikel bearbeiten'
	,
	'Article could not be updated!'=>
	'Artikel konnte nicht geupdated werden!'
	,
	'Updated article successfully.'=>
	'Artikel wurde erfolgreich aktualisiert.'
	,
	'Delete article'=>
	'Artikel löschen'
	,
	'Article deleted.'=>
	'Artikel gelöscht.'
	,
	'Article inserted successfully'=>
	'Artikel erfolgreich erstellt'
	,
	'd|m|Y'=>
	'd.m.Y'
	,
	'back'=>
	'zurück'
	,
	'Share.png'=>
	'Teilen.png'
	,
	'Tag group view'=>
	'Tag Gruppierung'
	,
	'List view'=>
	'Listenansicht'
	,
	'Name'=>
	'Name'
	,
	'Email address'=>
	'E-Mail Adresse'
	,
	'Send comment'=>
	'Kommentar senden'
	,
	'Comment'=>
	'Kommentar'
	,
	'required'=>
	'benötigt'
	,
	'Write Comment'=>
	'Kommentar schreiben'
	,
	'JavaScript needs to be enabled in order to post comments'=>
	'JavaScript muss aktiviert sein um Kommentare zu senden'
	,
	'I might answer your questions via email if you provide your address. I will not send spam, neither sell your address to someone else.'=>
	'Die E-Mail Adresse wird versteckt und nur genutzt um Dein Gravatar an zu zeigen, wenn Du eins hast.'
	,
	'Comment send successfully.'=>
	'Kommentar erfolgreich gesendet'
	,
	'd|m|Y H:i'=>
	'd.m.Y H:i'
	,
	'supported'=>
	'unterstützt'
	,
	'support'=>
	'Unterstützung'
	,
	'delete'=>
	'löschen'
	,
	'Comment deleted'=>
	'Kommentar gelöscht'
	,
	'Comments'=>
	'Kommentare'
	,
	'You need to insert your name.'=>
	'Du musst einen Namen angeben.'
	,
	'You need to insert a comment.'=>
	'Du musst ein Kommentar abgeben.'
	,
	'You need to insert your name. <a href="#enterComment">to comment form</a>'=>
	'Du musst einen Namen angeben. <a href="#enterComment">zur Kommentar Eingabe</a>'
	,
	'You need to insert a comment. <a href="#enterComment">to comment form</a>'=>
	'Du musst ein Kommentar abgeben. <a href="#enterComment">zur Kommentar Eingabe</a>'
	,
	'RSS Feed'=>
	'RSS Feed'
	,
	'Something went wrong while getting the article or you are a hacker.'=>
	'Artikel konnte nicht geholt werden.'
	,
	'Something went wrong while getting the article. Sorry.'=>
	'Artikel konnte nicht geholt werden. Entschuldige bitte die Umstände.'
	,
	'Please read the article before you place a comment.'=>
	'Bitte lies den Artikel bevor Du ihn kommentierst.'
	,
	'Tags in use'=>
	'Schlagwörter in Verwendung'
	,
	'No older arcticles in stock, sorry!'=>
	'Keine älteren Artikel vorhanden.'
	,
	'+ add new article'=>
	'Erstelle einen neuen Artikel'
	,
	'No articles in stock, sorry!'=>
	'Es gibt noch keine Einträge im Blog.'
	,
	'No older articles in stock, sorry!'=>
	'Keine älteren Einträge vorhanden.'
	,
	'Repeat Email address'=>
	'Wiederhole die E-Mail Addresse'
	,
	'continue reading...'=>
	'weiterlesen & kommentieren...'
	,
	'Language not supported. Sorry.'=>
	'Die Sprache wird nicht unterstützt.'
	,
	'Insufficient rights!'=>
	'Fehlende Berechtigung!'
	,
	'more articles...'=>
	'weitere Artikel...'
	,
	'Create & publish article'=>
	'Erstellen und veröffentlichen'
	,
	'Comments are deactivated for this article.'=>
	'Kommentare sind für diesen Beitrag deaktiviert.'
	,
	'Article is not published yet!'=>
	'Artikel ist noch nicht veröffentlicht!'
	,
	'Article created successfully.'=>
	'Artikel wurde erfolgreich erstellt.'
	,
	'Publish article'=>
	'Artikel veröffentlichen'
	,
	'Unpublish article'=>
	'Artikel zurück ziehen'
	,
	'Article created successfully, but it is not published yet.'=>
	'Artikel wurde erfolgreich erstellt.'
	,
	'You can just create an article to work on and publish it later or create and publish this article in one go.'=>
	'Der Artikel wird erst für Besucher sichtbar nachdem er veröffentlicht wurde.'
	,
	'Notification E-Mails will be send to users when you publish an article.'=>
	'Benachrichtigungs E-Mails werden an Nutzer versendet, wenn ein Artikel veröffentlicht wird.'
	,
	'A date in the future will publish this article in the future and will NOT send notification e-mails.'=>
	'Bei einem Datum in der Zukunft wird der Artikel erst zum angegebenem Datum sichtbar und es wird bei Veröffentlichung KEINE Benachrichtigungs E-Mail an Nutzer versandt.'
	,
	'Cannot insert Comment. Sorry.'=>
	'Konnte den Kommentar nicht speichern.'
	,
	'Title for the article'=>
	'Titel für den Artikel.'
	,
	'Seperate tags with comma.'=>
	'Trenne Schlägwörter mit einem Komma.'
	,
	'Y-m-d'=>
	'Y-m-d'
	,
	'a new blog article was created.'=>
	'Ein neuer Blog Eintrag wurde erstellt.'
	,
	'blog'=>
	'Blog'
	,
	'Hello $1,'=>
	'Hallo $1,'
	,
	'click following link to go directly to the source of this notification:'=>
	'Klicke folgenden Link um direkt zum Artikel zu gelangen:'
	,
	'To view all your notifications and mark them as read, go to this page:'=>
	'Um alle Benachrichtigungen zu sehen, folge diesem Link: '
	,
	'To change your notification settings, go to this page:'=>
	'Ändere die Einstellungen zu Benachrichtigungen hier:'
	,
	'$1 hat einen neuen Artikel auf $2 veröffentlicht.'=>
	'$1 hat einen neuen Artikel auf $2 veröffentlicht.'
	,
	'Neuigkeiten auf '=>
	'Neuigkeiten auf '
	,
	'$1 hat einen neuen Artikel veröffentlicht.'=>
	'$1 hat einen neuen Artikel veröffentlicht.'
	,
	'comments'=>
	'Kommentare'
        ,
	'comment'=>
	'Kommentar'
	,
	'Could not insert article. Sry, this is an never expected problem. Check your database.'=>
	''
);