CREATE TABLE IF NOT EXISTS `{dbprefix}blog` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL DEFAULT '',
  `text` MEDIUMTEXT NOT NULL DEFAULT '',
  `author` varchar(191) NOT NULL DEFAULT '',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_need` varchar(191) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `allow_comments` tinyint(1) NOT NULL DEFAULT '1',
  `language` char(2) NOT NULL DEFAULT 'gb',
  `parent` bigint NULL,
  `tags` text NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
);