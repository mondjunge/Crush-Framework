CREATE TABLE `{dbprefix}blog_comments` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `author` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `email` varchar(191) NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `blog_id` bigint NULL,
  PRIMARY KEY (`id`)
);