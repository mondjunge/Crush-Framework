/**
 * blog.js
 */

use_package('modules');
modules.blog = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);
    this.init = function () {

        //handlePublish();
        //handleGroupView();
        handleImages();
        handleImageRotator();
        //handleOEmbed();

        utils.string.showEmailHandler(); // show obfuscated E-Mails

        if ($("#commentButton").length) {
            //console.log("commentButton:",$("#commentButton"));
            $("#commentButton").html('<button class="pure-button pure-button-primary" type="submit" name="comment" >' + $("#commentButton").data('text') + '</button>');
        }
    };

    var handleOEmbed = function () {
        var figure = $("figure.media")
        var url = figure.find("oembed").attr("url");
        console.log("url",url);
        if(url.includes("youtu")){
            //https://www.youtube-nocookie.com/embed/
            var noCookieDomain = "https://www.youtube-nocookie.com/embed/";
            // case 1: watch?v=
            var urlParts = url.split("watch?v=");
            url = noCookieDomain+urlParts[1];
            
        }
        figure.html("<iframe src=\""+url+"\" height=\"500\" width=\"100%\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share\" allowfullscreen/>")
        
        
//        $.getJSON(url, function (data) {
//            var items = [];
//            $.each(data, function (key, val) {
//                console.log("oEmbed", key, val);
//            });
//
//        });
    }

    var handleImages = function () {
        $('.imgBox > img').on("click", function () {
            var imgSrc = $(this).attr('src');
            utils.overlay.show('<div onclick="utils.overlay.hide()" style="cursor:pointer; text-align:center;" ><img src="' + imgSrc + '" style="max-width:100%; max-height: 90vh; "/></div>');
        });
    };

    var handleImageRotator = function () {

        $('.imageRotator').each(function () {
            var rotatorTimeout;
            var rotator = $(this);
            //configure timeout
            var controlsEnabled = rotator.data('controls');
            var timeout = rotator.data('timeout');
            var fixHeight = rotator.data('height');
            if (typeof controlsEnabled === 'undefined' || controlsEnabled === 'false') {
                controlsEnabled = false;
            }
            if (typeof timeout === 'undefined') {
                timeout = 5000;
            }
            if (typeof fixHeight !== 'undefined') {
                rotator.css({
                    'height': fixHeight,
                    'overflow': 'hidden'
                });
            }
            rotator.removeClass('pure-g-r');
            var controlHtml = '<div class="rotateControl">';
            var containers = rotator.find('.rotateContainer');
            containers.each(function (i) {

                $(this).removeClass('pure-u-1-' + containers.size);
                controlHtml += '&nbsp;<a class="" href="#">&diams;</a>&nbsp;';
                if (i > 0) {
                    //fix height of included items for smoother transition
                    $(this).find('div,span,img,p,blockquote').each(function () {
                        var imgbreite = $(this).width();
                        var imghoehe = $(this).height();
                        $(this).css({
                            'height': imghoehe,
                            'width': imgbreite
                        });

                    });
                    $(this).addClass('hiddenRotateContainer');
                    $(this).css({
                        'width': '0px'
                    });
                }

                $(this).css({
                    'float': 'right'
                });
            });
            controlHtml += '</div>';

            rotator.css({
                'display': 'block'
            });
            var rotHeight = rotator.height();
            var rotWidth = rotator.width();

            if (controlsEnabled) {
                rotator.append(controlHtml);

                var control = rotator.find('.rotateControl');
                control.css({
                    'top': rotHeight - 30,
                    'left': rotWidth / 2 - control.width() / 2
                });
                control.fadeIn();
            }

            rotatorTimeout = setTimeout(function () {
                rotateImage(containers, timeout, control);
            }, timeout);
            var rotateImage = function (images, timeout, control) {
                var size = images.size;
                console.log("images size: ", size);

                console.log("START rotate image ");

                images.each(function (i) {

                    if (!$(this).hasClass('hiddenRotateContainer')) {
                        //fix height of included items
                        $(this).find('div,span,img,p,blockquote').each(function () {
                            var imgbreite = $(this).width();
                            var imghoehe = $(this).height();
                            $(this).css({
                                'height': imghoehe,
                                'width': imgbreite
                            });

                        });
                        console.log("rotate to image: " + (i + 1));
                        $(this).css({
                            'float': 'left'
                        });

                        $(this).animate({
                            width: 0
                        });

                        $(this).addClass('hiddenRotateContainer');

                        var rotateIMG = function (element) {
                            $(element).removeClass('hiddenRotateContainer');
                            $(element).css({
                                'float': 'right'
                            });
                            $(element).animate({
                                width: '100%'
                            });
                        };

                        if (typeof images.get(i + 1) !== 'undefined') {
                            rotateIMG(images.get(i + 1));
                            return false;
                        }
                        if (typeof images.get(i + 1) === 'undefined') {
                            rotateIMG(images.get(0));
                            console.log("rotate to beginning: ");
                            return false;
                        }
                    }

                });
                rotatorTimeout = setTimeout(function () {
                    rotateImage(images, timeout, control);
                }, timeout);
                //console.log("END rotate image ");
            };
            console.log("containers size: ", containers);
        });
    };


//    var handlePublish = function(){
//        $('#insertandpublish').on("click",function(){
//            var html = '<form class="pure-form pure-form-stacked" method="post" enctype="multipart/form-data">';
//            html += '<h2>'+$('#title').val()+'</h2>';
//            
//            html += CKEDITOR.instances["ckeditor"].document.getBody().getHtml();
//            
//            html += $('#blogDate').html();
//            
//            html += '</form>';
//            
//            html = $('#newArticle').html();
//            
//            utils.overlay.show(html,this);
//            //$('#blogDate').datepicker();
//        });
//    };


//    var handleGroupView = function () {
//        /*
//         * change button and link appearence
//         */
//        //    $( "button, input:submit", "newRow" ).button();
//        //$( "button, input:submit, a", ".blog" ).on("click",function() {});
//
//        var howMany = $(".ui-widget-content").length;
//        if (howMany === 0) {
//            return;
//        }
//        var limit = 6;
//
//        //    deleteCookie("groups");
//        //    deleteCookie("tabs");
//
//        //[tabID]= groupID;
//        var tabs = readCookie("tabs").split(',', limit);
//        //[groupID]= tabID;
//        var groups = readCookie("groups").split(',', limit);
//
//        if (howMany != groups.length) {
//            var csv = '';
//            for (var i = 0; howMany > i; i++) {
//                if (i == 0) {
//                    csv += '' + (i + 1);
//                } else {
//                    csv += ',' + (i + 1);
//                }
//            }
//            writeCookie("groups", csv);
//            writeCookie("tabs", csv);
//            tabs = readCookie("tabs").split(',', limit);
//            groups = readCookie("groups").split(',', limit);
//
//        }
//
//
//        //    var blogTabsElements = $("[id|='blogTab']");
//        /**
//         * Get position for each tab and set the offset of its current group right
//         **/
//        var blogGroups = '';
//        var blogTabs = '';
//        for (var n = 0; tabs.length > n; n++) {
//            var gOffset = $("#blogTab" + (1 + n)).offset();
//            $('#blogGroup' + tabs[n]).offset(gOffset);
//            if (n > 0) {
//                blogGroups += ', #blogGroup' + (1 + n);
//                blogTabs += ', #blogTab' + (1 + n);
//            } else {
//                blogGroups += '#blogGroup' + (1 + n);
//                blogTabs += '#blogTab' + (1 + n);
//            }
//        }
//
//        $(blogGroups).draggable({
//            start: function (event, ui) {
//                /*set the z-index over all while dragging*/
//                $(this).css('z-index', 100);
//            },
//            stop: function (event, ui) {
//                /*set the z-index back*/
//                $(this).css('z-index', 'auto')
//            }
//        });
//        $(blogTabs).droppable({
//            drop: function (event, ui) {
//                /**
//                 * get the # from the id of draggable and droppable
//                 */
//                var bulletGroupId = $(ui.draggable).attr('id').substr(-1, 1);
//                var targetTabId = $(this).attr('id').substr(-1, 1);
//                var hitGroupId = tabs[targetTabId - 1];
//                var orphandTabId = groups[bulletGroupId - 1];
//
//                /**
//                 * Now keep the two arrays ordered to know which draggable is on which dropable
//                 */
//                groups[hitGroupId - 1] = orphandTabId;
//                tabs[orphandTabId - 1] = hitGroupId;
//
//                groups[bulletGroupId - 1] = targetTabId;
//                tabs[targetTabId - 1] = bulletGroupId;
//
//                /* Get Offset from the Tabs */
//                var oOffset = $("#blogTab" + orphandTabId).offset();
//                var tOffset = $("#blogTab" + targetTabId).offset();
//                /**
//                 * now make the Group with the TargetID the Group with the bulletID
//                 * and the other way around also.
//                 */
//                $('#blogGroup' + hitGroupId).offset(oOffset);
//                $('#blogGroup' + hitGroupId).height(400);
//                $('#blogGroup' + bulletGroupId).offset(tOffset);
//                $('#blogGroup' + bulletGroupId).height(400);
//
//                writeCookie("groups", groups.join(','));
//                writeCookie("tabs", tabs.join(','));
//                // alert("wrote Group Save"+groups.join(','));
//            }
//        });
//    };
//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


}
/**
 * Blog Controls
 */
var tags = '';
function initTags(value) {
    tags = value;
}
function changeTag(element, tag) {
    var aTags = tags.split(',');
    var newvalue = '';
    var deleted = false;
    for (var n = 0; n < aTags.length; n++) {
        if (aTags[n] == tag) {
            deleted = true;
        } else {
            if (n == 0)
                newvalue += aTags[n];
            else
                newvalue += ',' + aTags[n];
        }
    }
    if (!deleted) {

    }
    tags = newvalue;
    $(element).addClass("blog_tags_selected");
//element.getAttribute('class') = "blog_tags_selected";

}

/**
 * Delete check stuff 
 */

function deleteCheck() {
    var confirmText;
    if (language == 'gb')
        confirmText = 'Do you really want to delete this article? You can not undo this action!';

    if (language == 'de')
        confirmText = 'Soll der Beitrag wirklich gelöscht werden? Der Vorgang kann nicht rückgängig gemacht werden!';


    return confirm(confirmText);
}

/**
 * add Tag stuff in blog editor
 */
function addTag(tag) {
    //alert(tag);
    var newvalue = "";
    var oldvalue = document.getElementById("tags").value;
    //    var setNewTag = true;
    //    if(oldvalue==""){
    //        newvalue = tag;
    //    }else{
    //        var ova = oldvalue.split(",");
    //        for(var n = 0; n<ova.length ;n++){
    //            //alert(ova[n]);
    //            if(ova[n]==tag){
    //                setNewTag = false;
    //            }
    //        }
    //        newvalue = oldvalue + "," + tag;
    //    }
    var ova = oldvalue.split(",");
    if (valueInArray(tag, ova)) {
        for (var i = 0; i < ova.length; i++) {
            if (ova[i] != tag) {
                if (newvalue == "")
                    newvalue = ova[i];
                else
                    newvalue += "," + ova[i];
            }

        }
    } else {
        if (oldvalue == "")
            newvalue = tag;
        else
            newvalue = oldvalue + "," + tag;
    }
    document.getElementById("tags").value = newvalue;

}

function valueInArray(value, array) {
    var inArray = false;
    for (var i = 0; i < array.length; i++) {
        if (array[i] == value)
            inArray = true;
    }
    return inArray;
}
