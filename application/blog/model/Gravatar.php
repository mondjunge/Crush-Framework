<?php
/**
 * http://dennishoppe.de/artikel/21/der-gravatar
 */
class Gravatar {
  private $mail;
  private $size = 40;
  private $rating = 'pg';
  private $default = 'identicon';
 
  public function setMail ($mail){
    $mail = Trim ($mail);
    If ($mail == '') return False;
    $this->mail = StrToLower ($mail);
    return True;
  }
 
  public function setSize ($size){
    $size = IntVal ($size);
    If ($size < 1 || $size > 512) return False;
    $this->size = $size;
    return True;
  }
 
  public function setRating ($rating){
    $arr_valid = Array ('g', 'pg', 'r', 'x');
    $rating = StrToLower (Trim ($rating));
    If (Array_Search ($rating, $arr_valid) === False) return False;
    $this->rating = $rating;
    return True;
  }
 
  public function setDefault ($default){
    // Valid: identicon, monsterid, wavatar or an url
    $default = Trim ($default);
    $this->default = UrlEncode($default);
  }
 
  public function getUrl (){
    If ($this->mail == '') return False;
    $url = 'https://www.gravatar.com/avatar/'.
           hash('md5',$this->mail).'?'.
           'size='.$this->size.'&amp;'.
           'rating='.$this->rating.'&amp;'.
           'default='.$this->default;
    return $url;
  }
}