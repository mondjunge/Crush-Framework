<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class BlogModel extends Database {

    private $userRoles = array();

    /**
     *
     * @int auto_increment
     */
    private $id;

    /**
     * @varchar(255)
     */
    private $title;

    /**
     * @text
     */
    private $text;
    private $author;
    private $date;
    private $active;
    private $tags;
    private $lang;
    
    public function __construct(){
        // initiate Database
        $this->getInstance();
    }

    public function setUserRoles($roles) {
        $this->userRoles = $roles;
    }

    /**
     * returns all active blog articles
     * @return array multidimensinoal [0-n][column_nmae]
     */
    public function getAllArticles($limit = '', $admin = false) {
        if ($admin) {
            $rs = $this->selectPrepare('blog', '*', '1=1 ORDER BY date desc, id desc');
        } else {
            $rs = $this->selectPrepare('blog', '*', 'active=1 AND date < NOW() ORDER BY date desc, id desc');
        }
        $n = 0;
        if (is_array($rs))
            $return = [];
            foreach ($rs as $article) {
                if ($this->checkUserRights($article['role_need'])) {
                    $return[] = $article;
                }
            }
        if ($limit != '') {
            $art = [];
            $la = explode(',', $limit);
            for ($n = $la[0]; $n < $la[0] + $la[1]; ++$n) {
                if (isset($return[$n])){
                    $art[] = $return[$n];
                }
                    
            }
            return $art;
        } else {
            return $return;
        }
    }

    /**
     * get a particular article by id
     * @param int $id
     * @return array associative [column_name]
     */
    public function getArticleById($id) {
        $rs = $this->selectPrepare('blog', '*', 'id=:id' ,array('id' => $id ) );
        if (is_array($rs))
            foreach ($rs as $article) {
                if ($this->checkUserRights($article['role_need'])) {
                    $return[] = $article;
                }
            }
        return $return[0];
    }

//    public function getArticlesByTag($tag='', $limit='', $active='1') {
//        if ($tag == '' && $limit == '') {
//            /**
//             * get them ordered as array
//             */
//            $tags = $this->getTags();
//            foreach ($tags as $tag) {
//                $tagArticles["$tag"][] = $this->getArticles($tag);
//            }
//            return $tagArticles;
//        } elseif ($tag != '' && $limit != '') {
//            $rsA = $this->select('blog', '*', 'active=' . $active, 'date asc');
//            foreach ($rsA as $article) {
//                $articleTags = explode(",", $article['tags']);
//                if (in_array($tag, $articleTags)) {
//                    $articlesA[] = $article;
//                }
//            }
//            $limitA = explode(',', $limit);
//            $articlesA = array_slice($articlesA, $limitA[0], $limitA[1]);
//            return $articlesA;
//        } elseif ($tag == '' && $limit != '') {
//            $articlesA = $this->select('blog', '*', 'active=' . $active, 'date asc', $limit);
//
////            $limitA = explode(',', $limit);
////            $articlesA = array_slice($articlesA, $limitA[0], $limitA[1]);
//            return $articlesA;
//        }
//    }

    public function getArticleByTitle($title) {

        $rs = $this->selectPrepare('blog', '*', "title=:title ORDER BY id desc", array('title' => $title));

        if ($this->checkUserRights($rs[0]['role_need'])) {

            return $rs[0];
        } else {
            return false;
        }
    }

    /**
     * DONE: right check
     * @param type $title
     * @return type 
     */
    public function getLastNext($id) {
//        $rs = $this->select('blog', '*', "active=1 AND (title='$title')", 'id desc');
//        $date = $rs[0]['date'];
//        $id = $rs[0]['id'];
//        $tags = $rs[0]['tags'];
        $returnA = array();
        $r = $this->selectPrepare('blog', '*', "active=1 AND (id<:id) AND date < NOW() ORDER BY id desc", array('id' => $id));
        $allowed = false;
        for ($n = 0; $n < count($r) && $allowed == false; ++$n) {
            $allowed = $this->checkUserRights($r[$n]['role_need']);
            $returnA[0] = $r[$n];
        }

        $r = $this->selectPrepare('blog', '*', "active=1 AND (id>:id) AND date < NOW() ORDER BY id asc", array('id' => $id));

        $allowed = false;
        for ($n = 0; $n < count($r) && $allowed == false; ++$n) {
            $allowed = $this->checkUserRights($r[$n]['role_need']);
            $returnA[1] = $r[$n];
        }

        return $returnA;
    }

    /**
     *
     * @param type $tag - name of the tag to be found
     * @return type 
     */
    public function getArticlesByTag($tag, $limit = '', $adminMode = false) {
        if ($adminMode) {
            if ($tag == '') {
                $rs = $this->selectPrepare('blog', '*', "(tags='') ORDER BY date desc, id desc");
            } else {
                $rs = $this->selectPrepare('blog', '*', "(tags LIKE :tag1 OR tags LIKE :tag2 OR tags=:tag3) ORDER BY date desc, id desc", array('tag1' => "%,$tag%", 'tag2' => "%$tag,%", 'tag3' => $tag));
            }
        }else {
            if ($tag == '') {
                $rs = $this->selectPrepare('blog', '*', "active=1 AND date < NOW() AND (tags='') ORDER BY date desc, id desc");
            } else {
                $rs = $this->selectPrepare('blog', '*', "active=1 AND date < NOW() AND (tags LIKE :tag1 OR tags LIKE :tag2 OR tags=:tag3) ORDER BY date desc, id desc", array('tag1' => "%,$tag%", 'tag2' => "%$tag,%", 'tag3' => $tag));
            }
        }

        $return = array();
        if (is_array($rs)){
            foreach ($rs as $article) {
                if ($this->checkUserRights($article['role_need'])) {
                    $return[] = $article;
                }
            }
        }
        if ($limit != '') {
            $la = explode(',', $limit);
            $art = array();
            for ($n = $la[0]; $n < $la[0] + $la[1]; ++$n) {
                if (isset($return[$n])){
                    $art[] = $return[$n];
                }
            }
            return $art;
        } else {
            return $return;
        }
    }

    /**
     *
     * @param int $id
     * @return Array 
     */
    public function getComments($id) {
        $rs = $this->selectPrepare('blog_comments', '*', "blog_id=:id ORDER BY date asc, id desc", array('id' => $id));
        return $rs;
    }
    /**
     * counts comments on an article
     * @param type $id
     * @return type
     */
    public function countComments($id) {
        $rs = $this->count('blog_comments',"blog_id='$id'");
        return $rs;
    }

    /**
     * Returns all Tags that occure in the blog database sorted alphabetically
     * @return array 
     */
    public function getTags($adminMode=false) {
        $tags = array();
        if($adminMode){
            $rs = $this->select('blog', '*', '', 'date asc');
        }else{
            $rs = $this->select('blog', '*', 'active=1 AND date < NOW()', 'date asc');
        }
        

        if (count($rs) > 0) { //is_array($rs)
            foreach ($rs as $article) {
                if ($this->checkUserRights($article['role_need'])) {
                    $return[] = $article;
                }
            }
            if (is_array($return)) {
                foreach ($return as $article) {


                    $a = explode(',', $article['tags']);
                    // print_r($a);
                    foreach ($a as $tag) {
                        // print_r($tag);
                        if ($tag !== '') {
                            $tags[] = $tag;
                        }
                    }
                }
                $tags = array_unique($tags);
                array_multisort($tags, SORT_STRING);
            }
        }
        // print_r($tags);
        return $tags;
    }

    public function checkUserRights($roleNeed) {
        //echo $roleNeed; //exit;
        $rnA = explode(",", $roleNeed);

        // if there is a 0 in $roleNeed, the site is for everyone
        if (in_array("0", $rnA)) {
            return true;
        }
        $userRolesA = $this->userRoles;
        if (!$userRolesA) {
            return false;
        }
        // if user is superadmin, he is always allowed
        if (in_array("1", $userRolesA)) {
            return true;
        }
        if (is_array($userRolesA))
            foreach ($userRolesA as $userRole) {
                //print_r($rnA); echo $userRole; exit;
                if (in_array($userRole, $rnA)) {
                    return true;
                }
            }
        return false;
    }

}
