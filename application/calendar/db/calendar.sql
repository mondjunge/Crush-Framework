CREATE TABLE `{dbprefix}calendar` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(191) NOT NULL DEFAULT '',
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `url` varchar(191) NOT NULL DEFAULT '',
  `allDay` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `repeat_pattern` varchar(16) DEFAULT NULL,
  `repeat_end` datetime DEFAULT NULL,
  `user_id` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
);



