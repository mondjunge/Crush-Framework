<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Calendar extends AppController implements INotify {

    private $friendsEnabled = true;

    public function __construct() {
        if ($this->isModuleActive('profilpage')) {
            $this->loadModuleConfiguration('profilpage');
            if (Config::get('friendsEnabled') != null) {
                $this->friendsEnabled = Config::get('friendsEnabled');
            }
        }
    }

    public function index() {
        $this->autoCheckRights();
        //$this->includeJs("javascript/component/map/jquery-maplocation.js");
//        if ($this->checkSiteRights('calendar/admin')) {
//            Link::jumpTo('calendar/admin');
//        }
        $this->includeJs('extras/fullcalendar-1.6.4/fullcalendar/fullcalendar.min.js');
        $data['adminMode'] = $this->checkSiteRights('calendar/admin');

        if ($data['adminMode']) {
            $this->includeCkeditor();
            $this->includeJs("application/calendar/js/.calendarAdmin.js");
            if (filter_has_var(INPUT_POST, 'delete')) {
                $this->deleteEvent();
                Link::jumpTo('calendar');
            }
            if (filter_has_var(INPUT_GET, 'updateEvent')) {

                $this->updateEvent();
                Link::jumpTo('calendar');
            }

            if (filter_has_var(INPUT_GET, 'viewAddEvent')) {
                $this->standalone = true;
                $this->includeCkeditor('javascript/ck_config/ck_default.js');
                $this->viewAddEvent();
                return;
            }

            if (filter_has_var(INPUT_GET, 'addEvent')) {
                $this->addEvent();

                Link::jumpTo('calendar');
            }
            $this->includeCkeditor();
        } else {
            $this->includeJs("application/calendar/js/.calendar.js");
        }
        
         if (filter_has_var(INPUT_GET, 'getAddressJSON')) {
             $this->standalone = true;
             $address = filter_input(INPUT_GET, 'getAddressJSON');
             // + address + "&format=json&polygon=1&addressdetails=1";
            echo $this->curlUrlContent("https://nominatim.openstreetmap.org/search?q=".$address."&format=json&polygon=1&addressdetails=1");
            return;
         }


        if (filter_has_var(INPUT_GET, 'getJSONEvents')) {
            // get JSON Events
            $this->standalone = true;
            $this->getJSONEvents();
            return;
        }

        if (filter_has_var(INPUT_GET, 'detailId')) {
            if (filter_has_var(INPUT_GET, 'ajax'))
                $this->standalone = true;

            $this->viewDetail(false);
            return;
        }

        $this->viewCalendar($data['adminMode']);
    }

    public function widget() {
        if (!$this->isModuleActive('calendar')) {
            return "";
        }
        $this->autoCheckRights();
        $this->standalone = true;
//        $this->includeJs('extras/fullcalendar-1.6.4/fullcalendar/fullcalendar.js');
//        $this->includeJs("javascript/calendar/calendar.js");

        $data['nextEvents'] = $this->fetchNextEventsHTML(5);
        $data['adminMode'] = $this->checkSiteRights('calendar/admin');
        return $this->fetch('widget', $data);
    }
    
    public function admin() {
        $this->autoCheckRights();
        $this->includeJs('extras/fullcalendar-1.6.4/fullcalendar/fullcalendar.min.js');

        $this->includeJs("application/calendar/js/.dateControl.js");

//        $this->includeJs('extras/ckeditor4/ckeditor.js', "single");
//        $this->includeJs('extras/ckeditor4/adapters/jquery.js', "single");
        $this->includeJs("application/calendar/js/.calendarAdmin.js");


        if (filter_has_var(INPUT_GET, 'getJSONEvents')) {
            // get JSON Events
            $this->standalone = true;
            $this->getJSONEvents('Y', 'm', true);
            return;
        }
        if (filter_has_var(INPUT_GET, 'detailId')) {
            if (filter_has_var(INPUT_GET, 'ajax')) {
                $this->standalone = true;
            } else {
                $this->includeCkeditor('javascript/ck_config/ck_default.js');
            }

            $this->viewEditDetail(true);
            return;
        }
        if (filter_has_var(INPUT_POST, 'delete')) {
            $this->deleteEvent();
            Link::jumpTo('calendar/admin');
        }
        if (filter_has_var(INPUT_GET, 'updateEvent')) {

            $this->updateEvent();
            Link::jumpTo('calendar/admin');
        }

        if (filter_has_var(INPUT_GET, 'viewAddEvent')) {
            if (filter_has_var(INPUT_POST, 'startString')) {
                $this->standalone = true;
            } else {
                $this->includeCkeditor('javascript/ck_config/ck_default.js');
            }
            $this->viewAddEvent();
            return;
        }

        if (filter_has_var(INPUT_GET, 'addEvent')) {
            $this->addEvent();

            Link::jumpTo('calendar/admin');
        }
        $this->includeCkeditor();

        $this->viewCalendar(true);
    }

    private function viewCalendar($admin = false) {

        $data['adminMode'] = $admin;
        if ($admin) {
            $data['nextEvents'] = $this->fetchNextEventsHTML(9);
            $this->view('main', $data);
        } else {
            $data['nextEvents'] = $this->fetchNextEventsHTML(9);
            $this->view('main', $data);
        }
    }

    private function viewEditDetail($admin = false) {
        $data['adminMode'] = $admin;
        $id = Input::sanitize(filter_input(INPUT_GET, 'detailId'), 'int');
        $rs = $this->select('calendar', '*', "id='$id'");
        if (sizeof($rs) == 0 || $rs === FALSE) {
            echo "Fehler";
            return;
        }
        $data['evt'] = $rs[0];

        // true when event is dropped after drag
        if (filter_has_var(INPUT_POST, 'startString')) {
            $data['evt']['start'] = $this->escapeString(filter_input(INPUT_POST, 'startString'));
        }
        if (filter_has_var(INPUT_POST, 'endString')) {
            $data['evt']['end'] = $this->escapeString(filter_input(INPUT_POST, 'endString'));
        }
        $dateTime = explode(" ", $data['evt']['start']);
        $data['evt']['start'] = $dateTime[0];
        $startTime = explode(":", $dateTime[1]);
        $data['evt']['starthour'] = $startTime[0];
        $data['evt']['startminute'] = $startTime[1];

        $dateTime = explode(" ", $data['evt']['end']);
        $data['evt']['end'] = $dateTime[0];
        $startTime = explode(":", $dateTime[1]);
        $data['evt']['endhour'] = $startTime[0];
        $data['evt']['endminute'] = $startTime[1];

        if ($data['evt']['allDay'] == 1) {
            $data['evt']['allDay'] = "checked='checked'";
        } else {
            $data['evt']['allDay'] = "";
        }
        if ($data['evt']['active'] == 1) {
            $data['evt']['active'] = "checked='checked'";
        } else {
            $data['evt']['active'] = "";
        }

        if ($data['evt']['repeat_pattern'] == "" || $data['evt']['repeat_pattern'] == null) {
            $data['evt']['repeat'] = "";
            $data['evt']['repeat_interval'] = "";
            $data['evt']['repeat_end'] = "";
        } else {
            $repeatArray = explode("-", $data['evt']['repeat_pattern']);
            $data['evt']['repeat'] = $repeatArray['0'];
            $data['evt']['repeat_interval'] = $repeatArray['1'];

            $rEndDateTime = explode(" ", $data['evt']['repeat_end']);
            $data['evt']['repeat_end'] = $rEndDateTime[0];
        }


        $this->view('editEvent', $data);
    }

    private function viewDetail($admin = false) {
        $data['adminMode'] = $admin;

        $id = Input::sanitize(filter_input(INPUT_GET, 'detailId'), 'int');
        $rs = $this->select('calendar', '*', "id=$id AND active=1");
        if (sizeof($rs) == 0 || $rs === FALSE) {
            echo $this->ts("This Event is not available or has been deleted. Sorry.");
            return;
        }
        $data['evt'] = $rs[0];

        $start = new DateTime($data['evt']['start']);
        $data['evt']['start'] = $start->format($this->ts('Y|m|d H:i'));
        $startArray = explode(" ", $data['evt']['start']);
        $data['evt']['startDate'] = $startArray[0];
        $data['evt']['startTime'] = $startArray[1];

        $end = new DateTime($data['evt']['end']);
        $data['evt']['end'] = $end->format($this->ts('Y|m|d H:i'));
        $endArray = explode(" ", $data['evt']['end']);
        $data['evt']['endDate'] = $endArray[0];
        $data['evt']['endTime'] = $endArray[1];
        if ($this->checkSiteRights('calendar/admin')) {
            $data['adminRights'] = true;
        }
        
        $this->view('event', $data);
    }

    private function viewAddEvent() {

        $data['startDate'] = date('Y-m-d');
        $data['endDate'] = date('Y-m-d');
        if (filter_has_var(INPUT_POST, 'startString')) {
            $startArray = explode(" ", filter_input(INPUT_POST, 'startString'));
            $data['startDate'] = $startArray[0];
            $timeArray = explode(":", $startArray[1]);
            $data['startHour'] = $timeArray[0];
            $data['startMinute'] = $timeArray[1];
        } else {
            //No time selected, set default one
            $data['startHour'] = "12";
            $data['startMinute'] = "00";
        }
        if (filter_has_var(INPUT_POST, 'endString')) {
            $endArray = explode(" ", filter_input(INPUT_POST, 'endString'));
            $data['endDate'] = $endArray[0];
            $timeArray = explode(":", $endArray[1]);
            $data['endHour'] = $timeArray[0];
            $data['endMinute'] = $timeArray[1];
        } else {
            //No time selected, set default one
            $data['endHour'] = "12";
            $data['endMinute'] = "30";
        }

        $data['evt']['repeat'] = "";
        $data['evt']['repeat_interval'] = "";
        $data['evt']['repeat_end'] = "";


        $this->view('addEvent', $data);
    }

    private function fetchNextEventsHTML($count = 10) {
        //$this->standalone= true;
        $today = new DateTime();
        $eventsA = array();
        $futureEvt = array();

        $rs = $this->select('calendar', '*', "end >= '" . $today->format('Y-m-d H:i') . "' AND (repeat_pattern is null OR repeat_pattern = '')", 'start ASC', '0,' . $count);

        $eventsR = $this->loadRepeatedEvents();

        $eventsAnR = array_merge($rs, $eventsR);

        if ($eventsAnR !== FALSE) {
            foreach ($eventsAnR as $ev) {


                $ev['url'] = Config::get('reLangUrl') . 'calendar?detailId=' . $ev['id'];
                $start = new DateTime($ev['start']);
                $ev['startDate'] = $start->format($this->ts('Y|m|d'));
                $ev['startHour'] = $start->format($this->ts('H:i'));

                $end = new DateTime($ev['end']);
                $ev['endDate'] = $end->format($this->ts('Y|m|d'));
                $ev['endHour'] = $end->format($this->ts('H:i'));

                $ev['today'] = false;
                if ($today->format('Y|m|d') == $start->format('Y|m|d')) {
                    $ev['today'] = true;
                }
                $ev['now'] = false;
                if ($start->format('Y-m-d H:i') <= $today->format('Y-m-d H:i') && $end->format('Y-m-d H:i') >= $today->format('Y-m-d H:i')) {
                    $ev['now'] = true;
                }

                $eventsA[] = $ev;
            } // endforeach
        }

        $events = $this->sortEvents($eventsA, "start");
        if (count($eventsA) == 0) {
            return "";
        }
        foreach ($events as $evt) {
            if ($evt['end'] > date("Y-m-d H:i:s")) {
                $futureEvt[] = $evt;
            }
        }
        $nextEvents = array();
        for ($n = 0; $n < $count && $n < sizeof($futureEvt); ++$n) {
            $nextEvents[] = $futureEvt[$n];
        }
        $data['events'] = $nextEvents;
//            for($n=0;$n<10;++$n){
//                [] = $events[$n];
//            }

        return $this->fetch('nextEvents', $data);
    }

    private function sortEvents($events, $key) {


        $sorter = array();
        $ret = array();
        reset($events);
        foreach ($events as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $events[$ii];
        }
        return $ret;
    }

    private function getJSONEvents($year = 'Y', $month = 'm', $adminMode = false) {
        $aConfig = Config::getConfig();
        $where = '';
        if ($year == 'Y' || !($year <= 2200 && $year >= 1200)) {
            $year = date('Y');
        }
        if ($month == 'm' || !($month <= 12 && $month >= 1)) {
            $month = date('m');
        }
        if ($month < 10) {
            $month = "0" . $month;
        }
        //Events older than 3 month are not loaded for performance
        $where .= " `start` > '" . $year . "-" . ($month - 3) . "-01 00:00'";

        $where .= " AND (repeat_pattern is null OR repeat_pattern='')";

        // show admin deactivated events also 
        if (!$adminMode){
            $where .= ' AND active=1';
        }
        //include_once 'model/CalendarModel.php';

        $eventsA = array();
        $rs = $this->select('calendar', '*', $where);
        foreach ($rs as $ev) {
            if ($ev['allDay'] == 0) {
                $ev['allDay'] = false;
            }
            $ev['repeat_end'] = '';
            $ev['url'] = Config::get('reLangUrl') . 'calendar?detailId=' . $ev['id'];

            $eventsA[] = $ev;
        }


        //$eventsAnR = array_merge($eventsA, $this->loadRepeatedEvents($year, $month, $adminMode));

        $eventsR = $this->loadRepeatedEvents($year, $month, $adminMode);
        $eventsAnR = array_merge($eventsA, $eventsR);

        echo json_encode($eventsAnR);
        // example
//        echo json_encode(array(
//            array(
//                'id' => 111,
//                'title' => "Event1",
//                'start' => "$year-$month-10",
//                'url' => "http://yahoo.com/",
//                'active' => 1 // 
//            ),
//            array(
//                'id' => 222,
//                'title' => "Event2",
//                'start' => "$year-$month-20",
//                'end' => "$year-$month-22",
//                'url' => "http://yahoo.com/",
//                'active' => 0
//            )
//        ));
    }

    private function loadRepeatedEvents($year = 'Y', $month = 'm', $adminMode = false) {
        $repEvents = array();
        $where = '';
        if ($year == 'Y' || !($year <= 2200 && $year >= 1200)) {
            $year = date('Y');
        }
        if ($month == 'm' || !($month <= 12 && $month >= 1)) {
            $month = date('m');
            if ($month < 10) {
                $month = "0" . $month;
            }
        }
        //Events older than 3 month are not loaded for performance
        $where .= " (`repeat_end` > '" . $year . "-" . ($month - 3) . "-01 00:00:00' OR `repeat_end` = '0000-00-00 00:00:00') ";
        $where .= " AND repeat_pattern is not null AND repeat_pattern != ''";

        // show admin deactivated events also 
        if (!$adminMode){
            $where .= ' AND active=1';
        }
        $rs = $this->select('calendar', '*', $where);
        foreach ($rs as $ev) {
            if ($ev['allDay'] == 0) {
                $ev['allDay'] = false;
            }



            $ev['url'] = Config::get('reLangUrl') . 'calendar?detailId=' . $ev['id'];


            $explodedEvents = $this->explodeRepeatedEvent($ev, $year, $month);


            $repEvents = array_merge($repEvents, $explodedEvents);
            //$repEvents[] = $ev;
            //return $this->explodeRepeatedEvent($ev);
        }
        // $newEvents = array_merge($repEvents, $explodedEvents);

        return $repEvents;
    }

    private function explodeRepeatedEvent($ev, $year = 'Y', $month = 'm') {
        $explodedEvents = array();
        $explodedEvents[] = $ev;

        $patternArray = explode("-", $ev['repeat_pattern']);
        /**
         * TODO:
         *  - add $patternArray[1] as long as repeat_end is reached, save Event every time. 
         */
        $n = 0;
        while (($ev['start'] < $ev['repeat_end'] || $ev['repeat_end'] == '0000-00-00 00:00:00') && $ev['start'] < date("Y-m-d", mktime(0, 0, 0, date('m'), date('d'), date('Y') + 3))) {


            if ($patternArray[0] == 'd') {
                $dateTime = explode(" ", $ev['start']);
                $dateArray = explode("-", $dateTime[0]);
                $dateString = date("Y-m-d", mktime(0, 0, 0, $dateArray[1], $dateArray[2] + ($patternArray[1]), $dateArray[0]));
                $ev['start'] = $dateString . " " . $dateTime[1];


                $dateTimeEnd = explode(" ", $ev['end']);
                $dateArrayEnd = explode("-", $dateTimeEnd[0]);
                $dateStringEnd = date("Y-m-d", mktime(0, 0, 0, $dateArrayEnd[1], $dateArrayEnd[2] + ($patternArray[1]), $dateArrayEnd[0]));
                $ev['end'] = $dateStringEnd . " " . $dateTimeEnd[1];
            }
            if ($patternArray[0] == 'm') {
                $dateTime = explode(" ", $ev['start']);
                $dateArray = explode("-", $dateTime[0]);
                $dateString = date("Y-m-d", mktime(0, 0, 0, $dateArray[1] + ($patternArray[1]), $dateArray[2], $dateArray[0]));
                $ev['start'] = $dateString . " " . $dateTime[1];


                $dateTimeEnd = explode(" ", $ev['end']);
                $dateArrayEnd = explode("-", $dateTimeEnd[0]);
                $dateStringEnd = date("Y-m-d", mktime(0, 0, 0, $dateArrayEnd[1] + ($patternArray[1]), $dateArrayEnd[2], $dateArrayEnd[0]));
                $ev['end'] = $dateStringEnd . " " . $dateTimeEnd[1];
            }
            if ($patternArray[0] == 'y') {
                $dateTime = explode(" ", $ev['start']);
                $dateArray = explode("-", $dateTime[0]);
                $dateString = date("Y-m-d", mktime(0, 0, 0, $dateArray[1], $dateArray[2], $dateArray[0] + ($patternArray[1])));
                $ev['start'] = $dateString . " " . $dateTime[1];


                $dateTimeEnd = explode(" ", $ev['end']);
                $dateArrayEnd = explode("-", $dateTimeEnd[0]);
                $dateStringEnd = date("Y-m-d", mktime(0, 0, 0, $dateArrayEnd[1], $dateArrayEnd[2], $dateArrayEnd[0] + ($patternArray[1])));
                $ev['end'] = $dateStringEnd . " " . $dateTimeEnd[1];
            }
            ++$n;
            $explodedEvents[] = $ev;
        }
        return $explodedEvents;
    }

    private function addEvent() {

        $title = filter_input(INPUT_POST, 'title',FILTER_UNSAFE_RAW);
        $description = filter_input(INPUT_POST, 'text',FILTER_UNSAFE_RAW);
        $location = filter_input(INPUT_POST, 'location',FILTER_UNSAFE_RAW);


        if (filter_has_var(INPUT_POST, 'repeat') && filter_input(INPUT_POST, 'repeat') != "") {
            $repeat_pattern = $this->getRepeatPattern();
            $repeat_end = filter_input(INPUT_POST, 'repeat_end');
        } else {
            $repeat_pattern = 'NULL';
            $repeat_end = 'NULL';
        }


        if (filter_has_var(INPUT_POST, 'active'))
            $active = 1;
        else
            $active = 0;

        if (filter_has_var(INPUT_POST, 'allDay')) {
            $allDay = 1;
            $start = filter_input(INPUT_POST, 'start') . " " . "00:00:00";
            $end = filter_input(INPUT_POST, 'end') . " " . "23:59:00";
        } else {
            $allDay = 0;
            if (filter_has_var(INPUT_POST, 'starthour')) {
                if (filter_input(INPUT_POST, 'starthour') < 10) {
                    $starthour = "0" . filter_input(INPUT_POST, 'starthour');
                } else {
                    $starthour = filter_input(INPUT_POST, 'starthour');
                }
            }
            if (filter_has_var(INPUT_POST, 'endhour')) {
                if (filter_input(INPUT_POST, 'endhour') < 10) {
                    $endhour = "0" . filter_input(INPUT_POST, 'endhour');
                } else {
                    $endhour = filter_input(INPUT_POST, 'endhour');
                }
            }

            $start = filter_input(INPUT_POST, 'start') . " " . $starthour . ":" . filter_input(INPUT_POST, 'startminute') . ":00";
            $end = filter_input(INPUT_POST, 'end') . " " . $endhour . ":" . filter_input(INPUT_POST, 'endminute') . ":00";
        }

        //echo "$title $start $end $active";
        $fieldsValueArray = array(
            'title' => $title,
            'start' => $start,
            'end' => $end,
            'description' => $description,
            'allDay' => $allDay,
            'location' => $location,
            'active' => $active,
            'repeat_pattern' => $repeat_pattern,
            'repeat_end' => $repeat_end,
            'user_id' => Session::get('uid')
        );
        $newId = $this->insert('calendar', $fieldsValueArray);
        if (FALSE === $newId) {
            $this->setErrorMessage($this->ts("Cannot insert Event."));
        } else {
            $this->setSystemMessage($this->ts("Event inserted succesfully!"));
            $userId = Session::get('uid');
            // Notifications newevent
//            $umlautUrl = str_replace("xn--", "ü", Config::get('rootUrl'));
//            $umlautUrl = str_replace("-jva", "", $umlautUrl);
//            $rs = $this->select('user as u, ' . Config::get('dbPrefix') . 'notifications_settings as ns'
//                    , 'u.id, u.email, u.nick, ns.newentry'
//                    , "u.id=ns.user_id AND u.active=1 AND ns.newevent=1 AND u.id!='$userId'");
//            $category = "event";
//            $link = $umlautUrl . Config::get('reLangUrl') . "calendar?detailId=" . $newId;
            //$link = "<a href='$link'>".$link."</a>";
//            $subject = $this->ts('New event');
//            $message = $this->ts("Hello $1,");
//            $message .= "<br/><br/>";
//            $message .= $this->ts("there is a new event from $2.");
//            $message .= "<br/><br/>";
//            $message .= $this->ts("Take a look now under following link:");
//            $message .= "<br/><br/>";
//            $message .= "<a href='$link'>" . $link . "</a>";
            //$this->notifyUsers($rs, $category, $subject, $message, $link);

            /**
             * 
             */
            $followersOnly = '';
            if ($this->friendsEnabled) {
                $followersA = $this->getFollowers($userId, true);
                if ($followersA !== '') {
                    $followersOnly = "AND id IN (" . $followersA . ")";
                }
            }

            $category = "newevent";

            $usersRsToNotify = $this->select('user'
                    , 'id, email, nick'
                    , "active=1 AND id!='$userId' $followersOnly");

            $linkToSource = "calendar?detailId=" . $newId;

            $messageParams[1] = Session::get('unick');
            $message = "$1 inserted a new event";
            $tryToSendEmail = true;

            //$this->setNotification($usersRsToNotify, $linkToSource, $category, $message, $messageParams, $tryToSendEmail);
            $n = new Notification();
            $n->setCategory($category);
            $n->setMessage($message);
            $n->setMessageParams($messageParams);
            $n->setSubject("Neuer Beitrag");
            $n->setLink($linkToSource);

            $this->setNotification($n, $usersRsToNotify);
//            if ($rs !== FALSE && sizeof($rs) > 0) {
//                $nick = Session::get('unick');
//                //http://localhost/Crush-Framework/de/calendar?detailId=8
//                $link = Config::get('rootUrl') . Config::get('reLangUrl') . "calendar?detailId=" . $newId;
//                foreach ($rs as $r) {
//                    $subject = $this->ts('New event');
//                    $message = $this->ts("Hello $1,");
//                    $message .= "\n\r";
//                    $message .= $this->ts("there is a new event from $2.");
//                    $message .= "\n\r";
//                    $message .= $this->ts("Take a look now under following link:");
//                    $message .= "\n\r";
//                    $message .= $link;
//                    $message = str_replace('$1', $r['nick'], $message);
//                    $message = str_replace('$2', $nick, $message);
//
//                    mail($r['email'], $subject, $message);
//                }
//            }
        }
    }

    private function getFollowers($userId, $idsAsCsv = false) {

        if ($idsAsCsv) {
            $rsRaw = $this->select('user', 'id', "friends LIKE '%,$userId' OR friends LIKE '$userId,%' OR friends LIKE '%,$userId,%' OR friends='$userId'");

            $arr = array();

            foreach ($rsRaw as $r) {
                $arr[] = $r['id'];
            }
            $rs = implode(',', $arr);
        } else {
            $rs = $this->select('user', 'id,nick,image', "friends LIKE '%,$userId' OR friends LIKE '$userId,%' OR friends LIKE '%,$userId,%' OR friends='$userId'");
        }
        return $rs;
    }

    private function getRepeatPattern() {
        $repeat = filter_input(INPUT_POST, 'repeat'); // d(ay), m(onth) or y(ear
        $repeat_interval = filter_input(INPUT_POST, 'repeat_interval'); //(1-n

        return $repeat . "-" . $repeat_interval;
    }

    private function updateEvent() {
        $id = Input::sanitize(filter_input(INPUT_POST, 'id'), 'int');
        $title = filter_input(INPUT_POST, 'title');
        $description = filter_input(INPUT_POST, 'text');
        $location = filter_input(INPUT_POST, 'location');

        if (filter_has_var(INPUT_POST, 'active')) {
            $active = 1;
        } else {
            $active = 0;
        }

        if (filter_has_var(INPUT_POST, 'allDay')) {
            $allDay = 1;
            $start = filter_input(INPUT_POST, 'start') . " " . "00:00:00";
            $end = filter_input(INPUT_POST, 'end') . " " . "23:59:00";
        } else {
            $allDay = 0;
            if (filter_has_var(INPUT_POST, 'starthour')) {
                if (filter_input(INPUT_POST, 'starthour') < 10) {
                    $starthour = "0" . filter_input(INPUT_POST, 'starthour');
                } else {
                    $starthour = filter_input(INPUT_POST, 'starthour');
                }
            }
            if (filter_has_var(INPUT_POST, 'endhour')) {
                if (filter_input(INPUT_POST, 'endhour') < 10) {
                    $endhour = "0" . filter_input(INPUT_POST, 'endhour');
                } else {
                    $endhour = filter_input(INPUT_POST, 'endhour');
                }
            }

            $start = filter_input(INPUT_POST, 'start') . " " . $starthour . ":" . filter_input(INPUT_POST, 'startminute') . ":00";
            $end = filter_input(INPUT_POST, 'end') . " " . $endhour . ":" . filter_input(INPUT_POST, 'endminute') . ":00";
        }


        if (filter_input(INPUT_POST, 'repeat') != "") {
            $repeat_pattern = $this->getRepeatPattern();
            $repeat_end = filter_input(INPUT_POST, 'repeat_end');
        } else {
            $repeat_pattern = 'NULL';
            $repeat_end = 'NULL';
        }

        //echo "$title $start $end $active";
        $fieldsValueArray = array(
            'title' => "$title",
            'start' => "$start",
            'end' => "$end",
            'description' => "$description",
            'allDay' => $allDay,
            'location' => "$location",
            'active' => $active,
            'repeat_pattern' => "$repeat_pattern",
            'repeat_end' => "$repeat_end"
        );

        if (FALSE === $this->updatePrepare('calendar', $fieldsValueArray, 'id=:id', array('id' => $id))) {
            $this->setErrorMessage($this->ts("Cannot update Event."));
        } else {
            $this->setSystemMessage($this->ts("Event updated succesfully!"));
        }
    }

    private function deleteEvent() {
        $id = Input::sanitize(filter_input(INPUT_POST, 'id'), 'int');
        if (FALSE === $this->delete('calendar', "id=$id")) {
            $this->setErrorMessage($this->ts("Cannot delete Event."));
        } else {
            $this->setSystemMessage($this->ts("Event deleted succesfully!"));
        }
    }

    /**
     * TODO: make ready
     * Select Dates 
     * @param type $m
     * @param type $d
     * @param type $y
     * @param type $day
     * @return type 
     */
    private function getDatesOfDayFrom($date, $day = 0) {
        /**
         * days 0-6 (sun-mon) 
         */
        $days = Array();
        $add = 3;
        for ($i = 0; $i < 35; $i++) {
            if ($day == date("w", mktime(0, 0, 0, date("m"), date("d") + ($add), date("Y")))) {
                $days[] = date("\K\W W, d.m.Y", mktime(0, 0, 0, date("m"), date("d") + ($add), date("Y")));
            }
            $add++;
        }


        return $days;
    }

    public function getNotificationCategories() {
        return array('newevent');
    }

    public function getNotificationSettings($data) {
        return $this->fetch('notifications_settings', $data);
    }

}
