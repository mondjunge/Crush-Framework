<div id="editEventContainer" >
    <?php if($adminRights==true) :?>
    <a class="pure-button pure-button-primary pure-button-small" href="<?php echo $this->action('calendar/admin?detailId=').$evt['id']?>" ><?php echo $this->ts('edit event');?></a>
    <?php endif; ?>
    <h2><?php echo $evt['title']; ?></h2>
    
    <!--    <div class="locationMap">
        <iframe src="" ></iframe>
    </div>-->
    <div >
        <div >
            <?php echo $this->ts('Starts'); ?>
            <b><?php echo $evt['startDate']; ?></b>
            <?php echo $this->ts('at'); ?>
            <?php echo $evt['startTime']; ?> <?php echo $this->ts('h'); ?>
            <br/>

            <?php echo $this->ts('Ends'); ?>
            <?php if ($evt['endDate'] != $evt['startDate']): ?>
            <?php echo $this->ts('on'); ?>
                <b><?php echo $evt['endDate']; ?></b>
            <?php endif; ?>
            <?php echo $this->ts('at'); ?>
            <?php echo $evt['endTime']; ?> <?php echo $this->ts('h'); ?>
        </div>
        <div>
            <h3><?php echo $this->ts('Details'); ?></h3>
            <?php echo $evt['description']; ?>
        </div>
        <div>
            <h3><?php echo $this->ts('Location'); ?></h3>
            <div class="mapLocation"><?php echo $evt['location']; ?></div>
        </div>
        
   
    </div>
</div>