<!--<h1><?php echo $this->ts('Calendar'); ?></h1>-->

<div class="pure-g pure-g-r">

    <div  class="nextEvents pure-u-1">
        
        <h2><?php echo $this->ts('next events:'); ?></h2>
            <?php echo $nextEvents ?>

        <?php if ($adminMode) : ?>
            <div>
                <a class="pure-button pure-button-primary" href="<?php echo $this->action("calendar/admin?viewAddEvent") ?>"><?php echo $this->ts("Add event") ?></a>
            </div>
        <?php endif; ?>
    </div>
<!--    <div class="pure-u-3-4">
        <div id="calendar" ></div>

    </div>-->

</div>

