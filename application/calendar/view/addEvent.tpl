<div id="editEventContainer" >
    <h2><?php echo $this->ts('Add event'); ?></h2>
    <form action="<?php echo $this->action('calendar/admin?addEvent'); ?>" method="post" class="pure-form pure-form-aligned">
        
        <fieldset>
                    <div class="pure-control-group">
                        <label for="title"><?php echo $this->ts('Title'); ?></label>
                        <input id="title" name="title" class="pure-input-1-2" type="text" placeholder="<?php echo $this->ts('Title'); ?>">
                    </div>

                    <div class="pure-control-group">
                        <label for="location"><?php echo $this->ts('Location'); ?></label>
                        <input id="location" name="location" class="pure-input-1-2" type="text" placeholder="<?php echo $this->ts('Street Nr, City'); ?>">
                    </div>

                    <div class="pure-controls">
                        <label for="allDay" class="pure-checkbox">
                            <input id="allDay" name="allDay"  type="checkbox"> <?php echo $this->ts('All day'); ?>
                        </label>
                    </div>

                    <div class="pure-control-group">


                        <label for="start"><?php echo $this->ts('Start'); ?></label>
                        <input id="start" name="start" class="pure-input-3-5 " type="date" value="<?php echo $startDate; ?>">
                        <select id="starthour" name="starthour" class="pure-input-1-5">
                            <?php for ($nCount = 0; $nCount <= 23; $nCount++) : ?>
                            <?php if ($nCount < 10){ $nCount = "0".$nCount;} ?>
                                <?php if ($nCount == $startHour) : ?>
                                    <option selected="selected"><?php echo $nCount ?></option>
                                <?php else : ?>
                                    <option><?php echo $nCount ?></option>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </select>
                        <select id="startminute" name="startminute" class="pure-input-1-5">
                            <?php for ($nCount = 0; $nCount <= 55; $nCount = 5 + $nCount) : ?>
                            <?php if ($nCount < 10){ $nCount = "0".$nCount;} ?>
                                <?php if ($nCount == $startMinute) : ?>
                                    <option selected="selected"><?php echo $nCount ?></option>
                                <?php else : ?>
                                    <option><?php echo $nCount ?></option>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </select>

                    </div>

                    <div class="pure-control-group">
                        <div class="pure-g pure-g-r">
                            <div class="pure-u-1">
                                <label for="end"><?php echo $this->ts('End'); ?></label>
                                <input id="end" name="end" class="pure-input-3-5 " type="date" value="<?php echo $endDate; ?>">
                                <select id="endhour" name="endhour" class="pure-input-1-5">
                                    <?php for ($nCount = 0; $nCount <= 23; $nCount++) : ?>
                                    <?php if ($nCount < 10){ $nCount = "0".$nCount;} ?>
                                        <?php if ($nCount == $endHour) : ?>
                                            <option selected="selected"><?php echo $nCount ?></option>
                                        <?php else : ?>
                                            <option><?php echo $nCount ?></option>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </select>
                                <select id="endminute" name="endminute" class="pure-input-1-5">
                                    <?php for ($nCount = 0; $nCount <= 55; $nCount = 5 + $nCount) : ?>
                                    <?php if ($nCount < 10){ $nCount = "0".$nCount;} ?>
                                        <?php if ($nCount == $endMinute) : ?>
                                            <option selected="selected"><?php echo $nCount ?></option>
                                        <?php else : ?>
                                            <option><?php echo $nCount ?></option>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="pure-control-group">
                        <div class="pure-g pure-g-r">
                            <div class="pure-u-1">
                                <label for="repeat"><?php echo $this->ts('Repetition'); ?></label>
                                <select id="repeat" name="repeat" class="pure-input-1-5">
                                    <option selected="selected" value=""><?php echo $this->ts('none'); ?></option>
                                    <option value="d"><?php echo $this->ts('daily'); ?></option>
                                    <option value="m"><?php echo $this->ts('monthly'); ?></option>
                                    <option value="y"><?php echo $this->ts('yearly'); ?></option>
                                </select>
                                <label for="repeat_interval"><?php echo $this->ts('Repeat interval'); ?></label>
                                <input id="repeat_interval" name="repeat_interval" disabled="disabled" class="pure-input" type="text" size="2" value="1" />
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="pure-control-group">
                        <div class="pure-g pure-g-r">
                            <div class="pure-u-1">
                                <label for="repeat_end"><?php echo $this->ts('Repetition end'); ?></label>
                                <input id="repeat_end" name="repeat_end" disabled="disabled" class="pure-input datepicker" type="date" value="" />
                                
                            </div>
                        </div>
                    </div>

                    <div class="pure-control-group">
                        <div class="pure-g pure-g-r">
                            <div class="pure-u-1">
                                <label for="desc"><?php echo $this->ts('Description'); ?></label>
                            
                                <div class="pure-u-2-3">
                                    <textarea id="desc" name="text" class="text"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pure-controls">
                        <div class="pure-g pure-g-r">
                            <div class="pure-u-1">
                                <label for="active" class="pure-checkbox">
                                    <input id="active" name="active" type="checkbox" checked="checked"><?php echo $this->ts('Active'); ?>
                                </label>
                                <button type="submit" class="pure-button pure-button-primary"><?php echo $this->ts('Add'); ?></button>
                                <a class="pure-button" href="<?php echo$this->action('calendar/admin');?>"><?php echo $this->ts('Cancel'); ?></a>
                            </div>
                        </div>
                    </div>
        </fieldset>
    </form>
</div>