<div id="editEventContainer" >
    <h2><?php echo $this->ts('Edit event'); ?> '<?php echo $evt['title']; ?>'</h2>
    <form action="<?php echo $this->action('calendar/admin?updateEvent'); ?>" method="post" class="pure-form pure-form-aligned">
        <input id="id" name="id" type="hidden" value="<?php echo $evt['id']; ?>">
        <fieldset>
                    <div class="pure-control-group">
                        <label for="title"><?php echo $this->ts('Title'); ?></label>
                        <input id="title" name="title" class="pure-input" type="text" value="<?php echo $evt['title']; ?>">
                    </div>

                    <div class="pure-control-group">
                        <label for="location"><?php echo $this->ts('Location'); ?></label>
                        <input id="location" name="location" class="pure-input" type="text" value="<?php echo $evt['location']; ?>">
                    </div>

                    <div class="pure-controls">
                        <label for="allDay" class="pure-checkbox">
                            <input id="allDay" name="allDay" class="pure-input" type="checkbox" <?php echo $evt['allDay']; ?>><?php echo $this->ts('All day'); ?>
                        </label>
                    </div>

                    <div class="pure-control-group">


                        <label for="start"><?php echo $this->ts('Start'); ?></label>
                        <input id="start" name="start" class="pure-input-3-5 " type="date" value="<?php echo $evt['start']; ?>">
                        <select id="starthour" name="starthour" class="pure-input-1-5">
                            <?php for ($nCount = 0; $nCount <= 23; $nCount++) : ?>
                            <?php if ($nCount < 10){ $nCount = "0".$nCount;} ?>
                                <?php if ($nCount == $evt['starthour']) : ?>
                                    <option selected="selected"><?php echo $nCount ?></option>
                                <?php else : ?>
                                    <option><?php echo $nCount ?></option>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </select>
                        <select id="startminute" name="startminute" class="pure-input-1-5">
                            <?php for ($nCount = 0; $nCount <= 55; $nCount = 5 + $nCount) : ?>
                            <?php if ($nCount < 10){ $nCount = "0".$nCount;} ?>
                                <?php if ($nCount == $evt['startminute']) : ?>
                                    <option selected="selected"><?php echo $nCount ?></option>
                                <?php else : ?>
                                    <option><?php echo $nCount ?></option>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </select>

                    </div>

                    <div class="pure-control-group">
                        <div class="pure-g pure-g-r">
                            <div class="pure-u-1">
                                <label for="end"><?php echo $this->ts('End'); ?></label>
                                <input id="end" name="end" class="pure-input-3-5 " type="date" value="<?php echo $evt['end']; ?>">
                                <select id="endhour" name="endhour" class="pure-input-1-5">
                                    <?php for ($nCount = 0; $nCount <= 23; $nCount++) : ?>
                                    <?php if ($nCount < 10){ $nCount = "0".$nCount;} ?>
                                        <?php if ($nCount == $evt['endhour']) : ?>
                                            <option selected="selected"><?php echo $nCount ?></option>
                                        <?php else : ?>
                                            <option><?php echo $nCount ?></option>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </select>
                                <select id="endminute" name="endminute" class="pure-input-1-5">
                                    <?php for ($nCount = 0; $nCount <= 55; $nCount = 5 + $nCount) : ?>
                                    <?php if ($nCount < 10){ $nCount = "0".$nCount;} ?>
                                        <?php if ($nCount == $evt['endminute']) : ?>
                                            <option selected="selected"><?php echo $nCount ?></option>
                                        <?php else : ?>
                                            <option><?php echo $nCount ?></option>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                     <div class="pure-control-group">
                        <div class="pure-g pure-g-r">
                            <div class="pure-u-1">
                                <label for="repeat"><?php echo $this->ts('Repetition'); ?></label>
                                <select id="repeat" name="repeat" class="pure-input-1-5">
                                    <?php $repSel=""; if($evt['repeat']=='') $repSel="selected='selected'"?> 
                                    <option <?php echo$repSel?> value=""><?php echo $this->ts('none'); ?></option>
                                    
                                    <?php $repSel=""; if($evt['repeat']=='d') $repSel="selected='selected'"?> 
                                    <option <?php echo$repSel?> value="d"><?php echo $this->ts('daily'); ?></option>
                                    
                                    <?php $repSel=""; if($evt['repeat']=='m') $repSel="selected='selected'"?> 
                                    <option <?php echo$repSel?> value="m"><?php echo $this->ts('monthly'); ?></option>
                                    
                                    <?php $repSel=""; if($evt['repeat']=='y') $repSel="selected='selected'"?> 
                                    <option <?php echo$repSel?> value="y"><?php echo $this->ts('yearly'); ?></option>
                                
                                </select>
                                <?php $repDis=""; if($evt['repeat_interval']=='') $repDis="disabled='disabled'"?> 
                                <label for="repeat_interval"><?php echo $this->ts('Repeat interval'); ?></label>
                                <input id="repeat_interval" name="repeat_interval" <?php echo$repDis?> class="pure-input" type="text" size="2" value="<?php echo $evt['repeat_interval'] ?>" />
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="pure-control-group">
                        <div class="pure-g pure-g-r">
                            <div class="pure-u-1">
                                <?php $repDis=""; if($evt['repeat_end']=='') $repDis="disabled='disabled'"?> 
                                <label for="repeat_end"><?php echo $this->ts('Repetition end'); ?></label>
                                <input id="repeat_end" name="repeat_end" <?php echo$repDis?> class="pure-input datepicker" type="date" value="<?php echo $evt['repeat_end'] ?>" />
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="pure-control-group">
                        <div class="pure-u-1">
                            <label for="desc"><?php echo $this->ts('Description'); ?></label>

                            <div class="pure-u-2-3">
                                <textarea id="desc" name="text"  class="text"><?php echo $evt['description'] ?></textarea>
                            </div>
                        </div>
                        
                    </div>

                    <div class="pure-controls">
                        <div class="pure-g pure-g-r">
                            <div class="pure-u-1">
                                <label for="active" class="pure-checkbox">
                                    <input id="active" name="active" type="checkbox" <?php echo $evt['active']; ?>><?php echo $this->ts('Active'); ?>
                                </label>
                                
                                <button type="submit" class="pure-button pure-button-primary"><?php echo $this->ts('Update'); ?></button>
                                <button type="submit" name="delete" class="pure-button pure-button-error" onclick="return deleteCheck();"><?php echo $this->ts('Delete'); ?></button>
                                <a class="pure-button" href="<?php echo$this->action('calendar/admin');?>"><?php echo $this->ts('Cancel'); ?></a>
                                    
                            </div>
                        </div>
                    </div>


        </fieldset>
    </form>
</div>