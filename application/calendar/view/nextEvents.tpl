<!--<dl>-->
<?php foreach ($events as $ev) : ?>

    <!--    <dt>-->
    <div class="nextEvent">
        <div class="nextEventTitle">
        <a class="nextEvent_a" href="<?php echo $ev['url']; ?>"><?php echo $ev['title']; ?></a>
        </div>
        <!--    </dt>-->
        <!--    <dd>-->
        <?php if ($ev['now'] == true) : ?>
            <span class="now"><b><?php echo $this->ts("now"); ?></b></span>
            <?php echo $this->ts("till"); ?> <b><?php echo $ev['endHour']; ?><?php echo $this->ts("h"); ?></b>

        <?php elseif ($ev['today'] == true) : ?>
            <span class="today"><b><?php echo $this->ts("today"); ?></b></span>
            <?php echo $this->ts("at"); ?> <b><?php echo $ev['startHour']; ?><?php echo $this->ts("h"); ?></b>

        <?php else : ?>
            <?php echo $ev['startDate']; ?> <?php echo $this->ts("at"); ?> <?php echo $ev['startHour']; ?><?php echo $this->ts("h"); ?>

        <?php endif; ?>

        <!--    </dd>-->
    </div>
<?php endforeach; ?>
<!--</dl>-->

<?php ?>