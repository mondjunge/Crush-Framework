<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Calendar' =>
	'Calendar'
	,
	'Add event'=>
	'Add event'
	,
	'next events:'=>
	'next events:'
	,
	'Title'=>
	'Title'
	,
	'Address'=>
	'Address'
	,
	'All day'=>
	'All day'
	,
	'Start'=>
	'Start'
	,
	'YYYY-MM-DD'=>
	'YYYY-MM-DD'
	,
	'End'=>
	'End'
	,
	'Active'=>
	'Active'
	,
	'Add'=>
	'Add'
	,
	'Street Nr, City'=>
	'Street Nr, City'
	,
	'time'=>
	'time'
	,
	'Description'=>
	'Description'
	,
	'addEvent'=>
	'addEvent'
	,
	'Event inserted succesfully!'=>
	'Event inserted succesfully!'
	,
	'Location'=>
	'Location'
	,
	'Details'=>
	'Details'
	,
	'Edit event'=>
	'Edit event'
	,
	'Delete'=>
	'Delete'
	,
	'Update'=>
	'Update'
	,
	'Cannot update Event.'=>
	'Cannot update Event.'
	,
	'Event updated succesfully!'=>
	'Event updated succesfully!'
	,
	'Event deleted succesfully!'=>
	'Event deleted succesfully!'
	,
	'Y|m|d H:i'=>
	'Y|m|d H:i'
	,
	'Starts'=>
	'Starts'
	,
	'at'=>
	'at'
	,
	'h'=>
	'h'
	,
	'ends'=>
	'ends'
	,
	'on'=>
	'on'
	,
	'Cancel'=>
	'Cancel'
	,
	'Y|m|d'=>
	'Y|m|d'
	,
	'H:i'=>
	'H:i'
	,
	'today'=>
	'today'
	,
	'now'=>
	'now'
	,
	'till'=>
	'till'
	,
	'Repetition'=>
	'Repetition'
	,
	'Repetition end'=>
	'Repetition end'
	,
	'none'=>
	'none'
	,
	'daily'=>
	'daily'
	,
	'weekdays'=>
	'weekdays'
	,
	'weekly'=>
	'weekly'
	,
	'monthly'=>
	'monthly'
	,
	'yearly'=>
	'yearly'
	,
	'Repeat interval'=>
	'Repeat interval'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'Cannot insert Event.'=>
	'Cannot insert Event.'
	,
	'edit event'=>
	'edit event'
	,
	'send'=>
	'send'
	,
	'profile picture'=>
	'profile picture'
	,
	'Video'=>
	'Video'
	,
	'New event'=>
	'New event'
	,
	'Hello $1,'=>
	'Hello $1,'
	,
	'there is a new event from $2.'=>
	'there is a new event from $2.'
	,
	'Take a look now under following link:'=>
	'Take a look now under following link:'
	,
	'event'=>
	'event'
	,
	'$1 created an $2.'=>
	'$1 created an $2.'
	,
	'This Event is not available or has been deleted. Sorry.'=>
	'This Event is not available or has been deleted. Sorry.'
	,
	'$1 inserted a new event'=>
	'$1 inserted a new event'
	,
	'click following link to go directly to the source of this notification:'=>
	'click following link to go directly to the source of this notification:'
	,
	'You only get one notification per month, unless you mark your notifications as read.'=>
	'You only get one notification per month, unless you mark your notifications as read.'
	,
	'To view all your notifications and mark them as read, go to this page:'=>
	'To view all your notifications and mark them as read, go to this page:'
	,
	'To change your notification settings, go to this page:'=>
	'To change your notification settings, go to this page:'
	,
	'calendar'=>
	'calendar'
	,
	'contact creates a new event.'=>
	'contact creates a new event.'
	,
	'Ends'=>
	'Ends'
);