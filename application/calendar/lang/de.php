<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Calendar' =>
	'Kalender'
	,
	'Add event'=>
	'Termin hinzufügen'
	,
	'next events:'=>
	'nächste Termine'
	,
	'Title'=>
	'Titel'
	,
	'Address'=>
	'Adresse'
	,
	'All day'=>
	'Ganztägig'
	,
	'Start'=>
	'Start'
	,
	'End'=>
	'Ende'
	,
	'Active'=>
	'Aktiv'
	,
	'Add'=>
	'hinzufügen'
	,
	'Street Nr, City'=>
	'Straße Nr, Stadt'
	,
	'Description'=>
	'Beschreibung'
	,
	'Event inserted succesfully!'=>
	'Termin erfolgreich hinzugefügt!'
	,
	'Location'=>
	'Ort'
	,
	'Details'=>
	'Details'
	,
	'Edit event'=>
	'Termin bearbeiten'
	,
	'Delete'=>
	'löschen'
	,
	'Update'=>
	'aktualisieren'
	,
	'Cannot update Event.'=>
	'Konnte Termin nicht aktualisieren. Entschuldigung.'
	,
	'Event updated succesfully!'=>
	'Termin wurde erfolgreich aktualisiert!'
	,
	'Event deleted succesfully!'=>
	'Termin wurde erfolgreich gelöscht!'
	,
	'Y|m|d H:i'=>
	'd.m.Y H:i'
	,
	'Starts'=>
	'Am'
	,
	'at'=>
	'um'
	,
	'h'=>
	'Uhr'
	,
	'Ends'=>
	'Endet'
	,
	'on'=>
	'am'
	,
	'Cancel'=>
	'abbrechen'
	,
	'Y|m|d'=>
	'd.m.Y'
	,
	'H:i'=>
	'H:i'
	,
	'today'=>
	'Heute'
	,
	'now'=>
	'Jetzt'
	,
	'till'=>
	'bis'
	,
	'Repetition'=>
	'Wiederholung'
	,
	'Repetition end'=>
	'Wdh. Ende'
	,
	'none'=>
	'keine'
	,
	'daily'=>
	'alle x Tage'
	,
	'monthly'=>
	'alle x Monate'
	,
	'yearly'=>
	'alle x Jahre'
	,
	'Repeat interval'=>
	'Wdh. Intervall'
	,
	'Insufficient rights!'=>
	'Ungenügende Rechte'
	,
	'Cannot insert Event.'=>
	'Kann den Termin aus irgendeinem Grund nicht anlegen. Sorry. :('
	,
	'edit event'=>
	'Termin bearbeiten'
	,
	'send'=>
	'absenden'
	,
	'New event'=>
	'Neuer Termin'
	,
	'Hello $1,'=>
	'Hallo $1'
	,
	'there is a new event from $2.'=>
	'$2 hat einen neuen Termin geplant.'
	,
	'Take a look now under following link:'=>
	'Schau Dir unter folgendem Link an was wann wo abgehen soll:'
	,
	'event'=>
	'Termin'
	,
	'$1 created an $2.'=>
	'$1 hat einen $2 geplant.'
	,
	'This Event is not available or has been deleted. Sorry.'=>
	'Dieser Termin ist nicht verfügbar oder wurde gelöscht. Entschuldigung.'
	,
	'$1 inserted a new event'=>
	'$1 hat einen neuen Termin angelegt'
	,
	'click following link to go directly to the source of this notification:'=>
	'Klicke folgenden Link um zur Quelle der Benachrichtigung zu gelangen:'
	,
	'You only get one notification per month, unless you mark your notifications as read.'=>
	'Du bekommst nur eine Benachrichtigung pro Monat, solange Du alte Benachrichtigungen nicht als gelesen markierst'
	,
	'To view all your notifications and mark them as read, go to this page:'=>
	'Um alle Benachrichtigungen zu sehen und als gelesen zu markieren, klicke folgenden Link:'
	,
	'To change your notification settings, go to this page:'=>
	'Um Deine Benachrichtigungseinstellungen zu ändern gehe auf folgende Seite:'
	,
	'calendar'=>
	'Kalender'
	,
	'contact creates a new event.'=>
	'Kontakt legt einen neuen Termin im Kalender an.'
);