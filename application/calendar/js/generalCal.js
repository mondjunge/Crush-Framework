/**
 * Delete check stuff 
 */

function deleteCheck(){
    var confirmText;
    if(language == 'gb')
        confirmText = 'Do you really want to delete this event? You can not undo this action!';
        
    if(language == 'de')
        confirmText = 'Soll das Event wirklich gelöscht werden? Der Vorgang kann nicht rückgängig gemacht werden!';
        
        
    return confirm(confirmText);
}