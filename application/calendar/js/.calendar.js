/**
 * calendar initialization and functions
 */

use_package('modules');
modules.calendar = new function() {
    base.main.ModuleRegistry.registerModule(this);
//    base.main.ModuleRegistry.registerAjaxRefreshModule(this);
//    
//    this.ajaxRefresh = function(){
//        
//    }
    this.init = function() {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        
        modules.calendar.openNextEventsInOverlay();
        
        /**
         * TODO: change modus for user...
         */
        var config = this.config;
        
        $('#calendar').fullCalendar(config);
        
        $(".mapLocation").mapLocation(); 
        
    };
    
    this.openNextEventsInOverlay = function(){
        console.log('click handler register on next events');
        $(".nextEvent_a").on("click",function(){
             console.log('click on next events');
             var element = this;
            $.ajax({
                type: "GET",
                url: this.href+"&ajax",
                async: true,
                success: function(html){
                    utils.overlay.show(html, element);
                    $(".mapLocation").mapLocation(); 
                }
            });
            return false;
        });
        
    };
    
        
    this.viewAddEventOnSelect = function(start, end){
        /**
             * TODO:
             * - get addEventHtml
             */
        var startYear = start.getYear()+1900;
        var startMonth = start.getMonth()+1;
        var startDay = start.getDate();
        if(startDay<10){
            startDay = "0"+startDay;
        }
        if(startMonth<10){
            startMonth = "0"+startMonth;
        }
        var startDateString = startYear+"-"+startMonth+"-"+startDay;
            
        var endYear = end.getYear()+1900;
        var endMonth = end.getMonth()+1;
        var endDay = end.getDate();
        if(endDay<10){
            endDay = "0"+endDay;
        }
        if(endMonth<10){
            endMonth = "0"+endMonth;
        }
        var endDateString = endYear+"-"+endMonth+"-"+endDay;
        var element = this;
        $.ajax({
            type: "POST",
            url: "?viewAddEvent",
            async: true,
            data: {
                startString: startDateString,
                endString: endDateString
            //                    ,
            //                    startYear: start.getYear()+1900,
            //                    startMonth: startMonth,
            //                    startDay: startDay
            //                    ,
            //                    endYear: end.getYear()+1900,
            //                    endMonth: endMonth,
            //                    endDay: endDay
            },
            success: function(html){
                utils.overlay.show(html,element);
                $(".mapLocation").mapLocation();  
            //$("#nextEvents").html(html);
                    
            }
        });
            
        $("#calendar").fullCalendar('unselect');
            
    }
    
    this.config = {
        editable: false,
        events: reLangUrl+"calendar?getJSONEvents",
        
        eventColor: '#cccccc',
        
        eventClick: function(calEvent, jsEvent, view) {

            //            alert('Event: ' + calEvent.title);
            //            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            //            alert('View: ' + view.name);
            var element = this;
            $.ajax({
                type: "GET",
                url: calEvent.url+"&ajax",
                async: true,
                success: function(html){
                    utils.overlay.show(html,calEvent.title);
                    $(".mapLocation").mapLocation();  
                }
            });
            //utils.overlay.show(calEvent.url);
            // change the border color just for fun
            //$(this).css('border-color', 'red');
            return false;

        },
        
        header: {
            left: 'today',
            center: 'title',
            right: 'prev,next'
            
        //             left: 'month,agendaWeek,agendaDay',
        //            center: 'title',
        //            right: 'today prev,next'
        },
        
        // locale
        
        firstDay: 1,
        monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
        monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
        dayNamesShort: ['Son','Mon','Die','Mit','Don','Fr','Sam'],
        buttonText: {
            prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
            next: "<span class='fc-text-arrow'>&rsaquo;</span>",
            prevYear: "<span class='fc-text-arrow'>&laquo;</span>",
            nextYear: "<span class='fc-text-arrow'>&raquo;</span>",
            today: 'Heute',
            month: 'Monat',
            week: 'Woche',
            day: 'Tag'
        },
        allDayText: '',
        
        // time formats
        axisFormat: 'H(:mm)', 
        
        titleFormat: {
            month: 'MMMM yyyy',
            week: "d. { '-' d. MMM yyyy}",
            day: 'dddd, d. MMMM yyyy'
        },
        columnFormat: {
            month: 'ddd',
            week: 'ddd d.M',
            day: 'dddd d.M'
        },
        timeFormat: { // for event elements
            '': 'H(:mm)', // default
            agenda: 'H:mm{ - H:mm}'
        },
        timeWord: ' Uhr'
        
    }
    
//    this.adminConfig = {
//        editable: true,
//        events: reLangUrl+"calendar/admin?getJSONEvents",
//        
//        selectable: true,
//        selectHelper: true,
//        select: function(start, end, allDay) {
//            
//            modules.calendar.viewAddEventOnSelect(start, end);
//    
//        },
//        eventClick: function(calEvent, jsEvent, view) {
//
//            //            alert('Event: ' + calEvent.title);
//            //            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//            //            alert('View: ' + view.name);
//            $.ajax({
//                type: "GET",
//                url: calEvent.url+"&ajax",
//                async: true,
//                success: function(html){
//                    utils.overlay.show(html,'auto');
//                //$("#nextEvents").html(html);
//                    
//                }
//            });
//            //utils.overlay.show(calEvent.url);
//            // change the border color just for fun
//            //$(this).css('border-color', 'red');
//            return false;
//
//        },
//        
//        header: {
//            left: 'month,agendaWeek,agendaDay',
//            center: 'title',
//            right: 'today prev,next'
//        },
//            
//        // locale
//        firstDay: 1,
//        monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
//        monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
//        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
//        dayNamesShort: ['Son','Mon','Die','Mit','Don','Fr','Sam'],
//        buttonText: {
//            prev: "<span class='fc-text-arrow'>&lsaquo;</span>",
//            next: "<span class='fc-text-arrow'>&rsaquo;</span>",
//            prevYear: "<span class='fc-text-arrow'>&laquo;</span>",
//            nextYear: "<span class='fc-text-arrow'>&raquo;</span>",
//            today: 'Heute',
//            month: 'Monat',
//            week: 'Woche',
//            day: 'Tag'
//        },
//        allDayText: '',
//        
//        // time formats
//        axisFormat: 'H(:mm)', 
//        
//        titleFormat: {
//            month: 'MMMM yyyy',
//            week: "d. { '-' d. MMM yyyy}",
//            day: 'dddd, d. MMMM yyyy'
//        },
//        columnFormat: {
//            month: 'ddd',
//            week: 'ddd d.M',
//            day: 'dddd d.M'
//        },
//        timeFormat: { // for event elements
//            '': 'H(:mm)', // default
//            agenda: 'H:mm{ - H:mm}'
//        },
//        timeWord: 'Uhr'
//        
//    }
        
    
};