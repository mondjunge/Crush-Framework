/**
 * dateControl
 * 
 * - corrects End- and Startdates to something possible if not so.
 */

use_package('modules');
modules.dateControl = new function(){
    base.main.ModuleRegistry.registerModule(this);
    base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);
    /**
     * Idea:
     * - check if Form is changed?
     * - make all checks in row
     *
     */
    this.init = function(){
        $("#start").change(function(){
            modules.dateControl.checkAndCorrectEndDate();
        });
        $("#starthour").change(function(){
            modules.dateControl.checkAndCorrectEndTime();
        });
        $("#end").change(function(){
            modules.dateControl.checkAndCorrectStartDate();
        });
        $("#endhour").change(function(){
            modules.dateControl.checkAndCorrectStartTime();
        });
        $("#startminute").change(function(){
            modules.dateControl.checkAndCorrectEndTimeMin();
        });
        $("#endminute").change(function(){
            modules.dateControl.checkAndCorrectStartTimeMin();
        });
        $("#allDay").change(function(){
            modules.dateControl.checkAllDay(this);
        });
    }
    this.ajaxRefresh = function(){
        modules.dateControl.init();
    }
    
    this.checkAllDay = function(element){
        //alert($(element).is(':checked'));
        if($(element).is(':checked')){
            $("#starthour").attr('disabled','true');
            $("#endhour").attr('disabled','true');
            $("#startminute").attr('disabled','true');
            $("#endminute").attr('disabled','true');
        }else{
            $("#starthour").prop('disabled', false);
            $("#endhour").prop('disabled', false);
            $("#startminute").prop('disabled', false);
            $("#endminute").prop('disabled', false);
        }
    }
    
    this.checkAndCorrectStartDate = function(){
        //alert($(this).val());
        var starttime = modules.dateControl.getStartDate();
        var endtime = modules.dateControl.getEndDate();
        if(starttime.getTime() > endtime.getTime()){
            var year = starttime.getYear()+1900;
            var month = starttime.getMonth()+1;
            var day = starttime.getDate();
            if(day<10){
                day = "0"+day;
            }
            if(month<10){
                month = "0"+month;
            }
            //alert(starttime.getYear('YYYY')+" : "+starttime.getMonth('MM'));
            console.log("endtime changed to: "+year+"-"+month+"-"+day);
            $("#end").val(""+year+"-"+month+"-"+day);
            alert("Das Enddatum kann nicht früher sein als das Anfangsdatum!");
        }
        
    }
    
    this.checkAndCorrectEndDate = function(){
        //alert($(this).val());
        var starttime = modules.dateControl.getStartDate();
        var endtime = modules.dateControl.getEndDate();
        if(starttime.getTime() > endtime.getTime()){
            var year = starttime.getYear()+1900;
            var month = starttime.getMonth()+1;
            var day = starttime.getDate();
            if(day<10){
                day = "0"+day;
            }
            if(month<10){
                month = "0"+month;
            }
            //alert(starttime.getYear('YYYY')+" : "+starttime.getMonth('MM'));
            console.log("endtime changed to: "+year+"-"+month+"-"+day);
            $("#end").val(""+year+"-"+month+"-"+day);
        }
        
    }
    /**
     * correct endtimeHour if day is same and starthour is higher than endhour
     */
    this.checkAndCorrectEndTime = function(){
       
        var starttime = modules.dateControl.getStartDate();
        var endtime = modules.dateControl.getEndDate();
        
        if(starttime.getTime() == endtime.getTime()){
                
            var starthour = parseInt($("#starthour").val());
            var endhour = parseInt($("#endhour").val());
            if(starthour > endhour){
                $("#endhour").val(starthour);
            }
        }
        
    }
    /**
     * correct starttimeHour if day is same and starthour is higher than endhour
     */
    this.checkAndCorrectStartTime = function(){
       
        var starttime = modules.dateControl.getStartDate();
        var endtime = modules.dateControl.getEndDate();
        
        if(starttime.getTime() == endtime.getTime()){
            var starthour = parseInt($("#starthour").val());
            var endhour = parseInt($("#endhour").val());
            if(starthour > endhour){
                $("#starthour").val(endhour);
            }
        }
        
    }
    
    /**
     * correct endtimeMinute if day is same and starthour is same than endhour
     */
    this.checkAndCorrectEndTimeMin = function(){
       
        var starttime = modules.dateControl.getStartDate();
        var endtime = modules.dateControl.getEndDate();
        
        if(starttime.getTime() == endtime.getTime()){
                
            var starthour = parseInt($("#starthour").val());
            var endhour = parseInt($("#endhour").val());
            if(starthour == endhour){
                var startminute = parseInt($("#startminute").val());
                var endminute = parseInt($("#endminute").val());
                if(startminute > endminute){
                    $("#endminute").val(startminute);
                }
            }
        }
        
    }
    
    this.checkAndCorrectStartTimeMin = function(){
       
        var starttime = modules.dateControl.getStartDate();
        var endtime = modules.dateControl.getEndDate();
        
        if(starttime.getTime() == endtime.getTime()){
                
            var starthour = parseInt($("#starthour").val());
            var endhour = parseInt($("#endhour").val());
            if(starthour == endhour){
                var startminute = parseInt($("#startminute").val());
                var endminute = parseInt($("#endminute").val());
                if(startminute > endminute){
                    $("#startminute").val(endminute);
                }
            }
        }
        
    }
    
    

    
    this.getStartDate= function(){
        var newDate = $("#start").val();
        newDate = newDate.split('-');
        var startYear = parseInt(newDate[0], 10);
        var startMonth = parseInt(newDate[1], 10) - 1; // as per Residuum's comment
        var startDay = parseInt(newDate[2], 10);
        var start = new Date(startYear, startMonth, startDay);
            
        return start;
    }
    
    this.getEndDate= function(){
        var newDate = $("#end").val();
        newDate = newDate.split('-');
        var startYear = parseInt(newDate[0], 10);
        var startMonth = parseInt(newDate[1], 10) - 1; // as per Residuum's comment
        var startDay = parseInt(newDate[2], 10);
        var end = new Date(startYear, startMonth, startDay);
            
        return end;
    }
    
//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }
    
    
}