<?php

/**
 *  Copyright © tim 13.06.2017
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */

/**
 * TODO:
 * - 
 * 
 */
class Settings extends AppController {

    public function index() {
        $this->autoCheckRights();

        if (filter_has_var(INPUT_POST, 'saveOnlyConfiguration')) {
            $this->createConfiguration();
            $this->jumpTo('settings');
        }
        if (filter_has_var(INPUT_POST, 'saveConfiguration')) {
            $this->createConfiguration();
            $this->jumpTo('settings');
        }

        $this->viewConfiguration();
    }

    private function viewConfiguration() {
        $useConfig = Config::$useConfig;
        $data['originalConfig'] = Config::getConfig();
        $data['defaultConfig'] = Config::getDefaultConfig();

        $data['configNames'] = $this->getConfigurationNames();
        if (filter_has_var(INPUT_POST, 'configselect')) {
            $loadConfig = Input::sanitize(filter_input(INPUT_POST, 'configselect'), 'site');
            $this->setSystemMessage($this->ts("Configuration $1 is now loaded for editing.", array(1 => $loadConfig)));
        }
        //$this->setTitle(Configuration'));
        $ConfigWriter = new ConfigWriter();
        $editConfig = (isset($loadConfig)) ? $loadConfig : $useConfig;
        $bConfig = $ConfigWriter->readConfig('config/config_' . $editConfig . '.php');
        $data['useConfig'] = $useConfig;
        $data['editConfig'] = $editConfig;
        $data['config'] = $bConfig;
        $data['availableSiteStyles'] = $this->getAvailableSiteStyles();
        $this->view('config', $data);
    }

    private function getAvailableSiteStyles() {
        $path = $_SERVER['DOCUMENT_ROOT'] . Config::get("relativeUrl") . "theme/";
        $d = dir($path);
        $fileArray = array();
        while (false !== ($entry = $d->read())) {
            if (is_dir($path.$entry) !== FALSE && strpos($entry, '.', 0) === FALSE) {
                $fileArray[] = $entry;
            }
        }
        //print_r($fileArray);
        return $fileArray;
    }

    private function createConfiguration() {
        $ConfigWriter = new ConfigWriter();
        if (!$ConfigWriter->createConfiguration(true)) {
            // fail
            $this->setErrorMessage($this->ts("An Error occured while saving configuration. You might have to manually edit it. Sorry."));
        } else {
            $this->setSystemMessage($this->ts("Configuration saved successfully."));
        }
    }

    private function getConfigurationNames() {
        $d = dir($_SERVER['DOCUMENT_ROOT'] . Config::get("relativeUrl") . "config/");
        $fileArray = array();
        while (false !== ($entry = $d->read())) {
            //echo "../../".$basePath."/".$entry."<br/>";
            //echo is_file("../../".$basePath."/".$entry);
            /**
             * TODO: FIX is_file
             */
            if (strpos($entry, "config_") !== FALSE) {
                $fileArray[] = str_replace(".php", "", str_replace("config_", "", $entry));
            }
        }
        //$this->setDebugMessage("config Files: ".print_r($fileArray));
        return $fileArray;
    }

}
