<form class="pure-form pure-form-aligned" action="<?php $this->action('settings'); ?>" method="POST">
    <fieldset>
        <legend><?php echo $this->ts("Configuration"); ?></legend>
        <div class="help help-margin"><?php echo $this->ts('Currently active configuration is: ') . $useConfig; ?></div>
        <div class="help help-margin"><?php echo $this->ts('Currently edited configuration is: ') . $editConfig; ?></div>
        <input type="hidden" id="useConfig" value="<?php echo $useConfig; ?>" />
        <input type="hidden" id="editConfig" value="<?php echo $editConfig; ?>" />
        <br/>
        <div class="pure-control-group">
            <label style="width: 13em;" for="configselect" ><?php echo $this->ts('Select a configuration') ?></label>
            <div class="pure-u-1-2">
                <select name="configselect" size='5' style="height:auto;">
                    <?php foreach ($configNames as $confName): ?>
                        <option label="<?php echo $confName; ?>" value="<?php echo $confName; ?>" <?php echo ($confName == $editConfig) ? 'selected="selected"' : ''; ?> ><?php echo $confName; ?></option>
                    <?php endforeach; ?>>
                </select>
                <button class="pure-button pure-button-primary" type="submit" name="loadConfiguration" ><?php echo $this->ts('Load configuration'); ?></button>

            </div>
            <div class="help help-margin"><?php echo $this->ts('configselect_helpText') ?></div>

        </div>
        <div class="pure-control-group">
            <label style="width: 13em;" for="configname" ><?php echo $this->ts('Create new configuration') ?></label>
            <input class="pure-input-1-2" id="configname" name="configname" type="text" value="" />
            <div class="help help-margin"><?php echo $this->ts('configname_helpText') ?></div>
        </div>
        <?php
        foreach ($config as $key => $value) :
            if (!in_array($key, $originalConfig)) {
                continue;
            }

            $type = gettype($value);
            if ($value != $defaultConfig[$key] && $type != 'array' && $type != 'boolean') {
                echo "<span class='warning help-margin'>" . $this->ts("The following value differs from the default value!") . " (" . $defaultConfig[$key] . ")</span>";
                echo "<button class='pure-button pure-button-xsmall' name='setDefaultValue' onclick='$(\"#{$key}\").val($(\"#default_{$key}\").val()); return false;'>" . $this->icon('open-iconic/sprite/sprite.min.svg#reload', 'svgIcon svgIcon-small') . "</button>";
                echo "<input type='hidden' id='default_{$key}' name='default_{$key}' value='" . $defaultConfig[$key] . "' />";
            }
            ?>
            <?php if ($key == 'dbType') : ?>
                <legend><?php echo $this->ts("Database"); ?></legend>
            <?php endif; ?>
            <?php if ($key == 'defaultController') : ?>
                <legend><?php echo $this->ts("System parameters"); ?></legend>
            <?php endif; ?>
            <?php if ($key == 'fromName') : ?>
                <legend><?php echo $this->ts("E-Mail settings"); ?></legend>
            <?php endif; ?>
            <?php if ($key == 'pwaName') : ?>
                <legend><?php echo $this->ts("PWA Settings"); ?></legend>
            <?php endif; ?>
            <?php if ($type == 'boolean'): ?>
                <div class="pure-control-group">
                    <label for="<?php echo $key ?>" class="pure-checker" tabindex="0" title="<?php echo $key ?>">
                        <div class="checker">
                            <input id="<?php echo $key ?>" class="checkbox" name="<?php echo $key ?>" <?php echo ($value ? 'checked="checked"' : ''); ?> type="checkbox" />
                            <div class="check-bg"></div>
                            <div class="checkmark">
                                <svg viewBox="0 0 100 100">
                                <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <span class="label"><?php echo $key ?></span>
                    </label>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>

                </div>
            <?php elseif ($key == 'enabledLanguages'): ?>    
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <select name="enabledLanguages[]" multiple='multiple' size='2'>
                        <option label="deutsch" value="de" <?php echo (in_array('de', $value) ? 'selected="selected"' : ''); ?> >deutsch</option>
                        <option label="english" value="gb" <?php echo (in_array('gb', $value) ? 'selected="selected"' : ''); ?>>english</option>
                    </select>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php elseif ($key == 'siteStyle'): ?>   
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <select name="siteStyle" >
                        <?php foreach ($availableSiteStyles as $val) : ?>

                            <option label="<?php echo $val; ?>" value="<?php echo $val; ?>" <?php echo ($val == $value ? 'selected="selected"' : ''); ?>><?php echo $val ?></option>

                        <?php endforeach; ?>
                    </select>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php elseif ($type == 'array'): ?>   
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <select name="<?php echo $key ?>[]" multiple='multiple' size='2'>
                        <?php foreach ($value as $val) : ?>

                            <option label="deutsch" value="<?php echo $val ?>"  selected="selected" ><?php echo $val ?></option>

                        <?php endforeach; ?>
                    </select>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php else: ?>
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <input class="pure-input-1-2" id="<?php echo $key ?>" name="<?php echo $key ?>" value="<?php echo $value ?>" type="text" />
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <div class="pure-controls">
            <?php if ($editConfig == $useConfig): ?>
                <div class="confSaveButton">
                    <button class="pure-button pure-button-primary" type="submit" name="saveOnlyConfiguration" ><?php echo $this->ts('Save configuration'); ?></button>
                </div>
                <div class="changedConfSaveButtons hidden">
                    <button class="pure-button pure-button-primary" type="submit" name="saveConfiguration" ><?php echo $this->ts('Save and activate configuration'); ?></button>
                    <button class="pure-button pure-button-primary" type="submit" name="saveOnlyConfiguration" ><?php echo $this->ts('Save configuration only'); ?></button>
                </div>
            <?php else: ?>
                <div class="confSaveButton hidden">
                    <button class="pure-button pure-button-primary" type="submit" name="saveOnlyConfiguration" ><?php echo $this->ts('Save configuration'); ?></button>
                </div>
                <div class="changedConfSaveButtons">
                    <button class="pure-button pure-button-primary" type="submit" name="saveConfiguration" ><?php echo $this->ts('Save and activate configuration'); ?></button>
                    <button class="pure-button pure-button-primary" type="submit" name="saveOnlyConfiguration" ><?php echo $this->ts('Save configuration only'); ?></button>
                </div>
            <?php endif; ?>

        </div>
    </fieldset>
</form>
