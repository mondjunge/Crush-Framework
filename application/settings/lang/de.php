<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Save and activate configuration'=>
	'Speichern und Konfiguration aktivieren'
	,
	'Configuration'=>
	'Basis Konfiguration'
	,
	'siteStyle_helpText'=>
	'Lege das Thema der Seite fest.'
	,
	'defaultLanguage_helpText'=>
	'Die vorausgewählte Sprache in der die Seite angezeigt wird.'
	,
	'enabledLanguages_helpText'=>
	'Aktivierte Sprachen. Sind mehr als eine Sprache aktiv, so bekommt der Nutzer die Möglichkeit zwischen Sprachen zu wählen.'
	,
	'devLanguage_helpText'=>
	'Sprache in der entwickelt wird. Sollte nicht geändert werden.'
	,
	'dbType_helpText'=>
	'Datenbank Typ. Aktuell steht nur ein MySQL Connector zur Verfügung.'
	,
	'dbServer_helpText'=>
	'Datenbank Server. Ein Port kann optional mit Doppelpunkt getrennt angegeben werden. Bsp: localhost:3306.'
	,
	'dbLogin_helpText'=>
	'Zu verwendender Datenbank Nutzer.'
	,
	'dbPassword_helpText'=>
	'Passwort des obigen Datenbank Nutzers.'
	,
	'dbName_helpText'=>
	'Name der Datenbank.'
	,
	'dbPrefix_helpText'=>
	'Prefix für die Datenbank Tabellen.'
	,
	'fromName_helpText'=>
	'Der Name des Absenders von E-Mail die das System an Nutzer schickt.'
	,
	'fromEmail_helpText'=>
	'Die Absender E-Mail Adresse von der Nachrichten an die Nutzer geschickt werden.'
	,
	'noticeEmail_helpText'=>
	'An diese E-Mail Adresse verschickt das System wichtige Nachrichten für Superadmins.'
	,
	'defaultController_helpText'=>
	'Gebe hier ein Modulname, optional modul/methode an, die beim Aufruf der Seite ausgeführt werden soll. Wird \'site\' angegeben, so wird die erste im Menü auffindbare statische Seite geladen.'
	,
	'defaultControllerAfterLogin_helpText'=>
	'Gebe hier ein Modulname, optional modul/methode an, die beim Aufruf der Seite für angemeldete Nutzer ausgeführt werden soll.'
	,
	'applicationDirectory_helpText'=>
	'Verzeichnis in dem die Module liegen. Sollte nicht unbedacht geändert werden.'
	,
	'debugMode_helpText'=>
	'Aktiviert debug Mode global. Achtung, wenn aktiv, sehen alle Besucher der Seite debug Ausgaben.'
	,
	'relativeUrl_helpText'=>
	'Pfad relativ zum Apache Document Root.'
	,
	'min_errorLogger_helpText'=>
	'Aktiviert den Minify Error Logger.'
	,
	'min_allowDebugFlag_helpText'=>
	'Wenn aktiv, kann man ein flag an der URL setzen um debug informationen zu erhalten.'
	,
	'min_cachePath_helpText'=>
	'Pfad zum Ordner in dem Minify cached. Wird ignoriert, wenn APC aktiv ist. Wird automatisch ermittelt.'
	,
	'min_enableAPC_helpText'=>
	'Aktivert den Pecl APC (Alternative PHP Cache).'
	,
	'min_documentRoot_helpText'=>
	'DocumentRoot von minify. Wird automatisch ermittelt.'
	,
	'min_serveOptions_maxAge_helpText'=>
	'Bestimmt in Sekunden, wie lange die gecachten Minify Dateien gültig sind, bevor ein Browser neue anfordert.'
	,
	'You already have a configuration with that name ($1). No files were written.'=>
	'Es existiert bereits eine Konfigurationsdatei mit dem Namen $1. Es wurde keine Konfigurationsdatei angelegt.'
	,
	'Database'=>
	'Datenbank Einstellungen'
	,
	'System parameters'=>
	'System Parameter'
	,
	'E-Mail settings'=>
	'E-Mail Einstellungen'
	,
	'Minify JS/CSS settings'=>
	'Minify JS/CSS Einstellungen'
	,
	'Configuration Name'=>
	'Name der Konfiguration'
	,
	'configname_helpText'=>
	'Gebe einen Namen für eine Konfiguration an die neu erstellt werden soll. Wird der Name einer bestehenden Konfiguration gewählt, wird diese überschrieben!'
	,
	'System Configuration'=>
	'Basis Konfiguration'
	,
	'Module Configuration'=>
	'Modul Konfiguration'
	,
	'Configuration saved successfully.'=>
	'Konfiguration erfolgreich gespeichert.'
    ,
    'An Error occured while saving configuration. You might have to manually edit it. Sorry.'=>
    'Ein Fehler ist beim speichern der Konfigurationsdatei aufgetreten. Entschuldigung.'
	,
	'Insufficient rights!'=>
	'Ungenügende Rechte!'
	,
	'maintainanceMode_helpText'=>
	'Ist der Wartungsmodus aktiviert, so wird Besuchern und Nutzern lediglich ein Hinweis zur Wartung angezeigt. Nur Superadmins könne sich einloggen und die Inhalte sehen/auf Funktionen zugreifen.'
	,
	'registrationOpen_helpText'=>
	'Ist diese Option aktiviert, können sich Besucher der Seite selber als Nutzer registrieren, aka. einen Nutzeraccount anlegen.'
	,
	'Create new configuration'=>
	'Neue Konfiguration anlegen'
	,
	'configselect_helpText'=>
	'Wähle eine bestehende Konfiguration. Wird eine bestehende Konfiguration geladen, so wird sie erst aktiv nachdem gespeichert wurde.'
	,
	'Select a configuration'=>
	'Wähle eine Konfiguration'
	,
	'Load configuration'=>
	'Lade gewählte Konfiguration'
	,
	'Currently active configuration is: '=>
	'Aktuell aktive Konfiguration: '
	,
	'Currently edited configuration is: '=>
	'Zur Bearbeitung geladene Konfiguration: '
	,
	'Save configuration only'=>
	'Nur speichern, nicht aktivieren.'
	,
	'showAppIcons_helpText'=>
	'Ist diese Option aktiviert, so werden svg Grafiken aus dem Ordner /theme/{siteStyle}/images/appIcons/ als Icons zu den Menüpunkten herangezogen.'
	,
	'updateEndpoint_helpText'=>
	'Url des Update-Servers'
	,
	'cssCacheLifetime_helpText'=>
	'Bestimmt in Sekunden wie lange erzeugte css Dateien gültig sind, bis der Webserver neue Dateien generiert.'
	,
	'Save configuration'=>
	'Konfiguration speichern'
	,
	'Configuration $1 is now loaded for editing.'=>
	'Konfiguration $1 wurde zum bearbeiten geladen.'
	,
	'The following value differs from the default value!'=>
	'Der folgende Wert weicht von der Originalkonfiguration ab!'
	,
	'Set default value'=>
	'Default Wert setzen'
	,
	'PWA Settings'=>
	'Progressive Web App Einstellungen'
	,
	'pwaName_helpText'=>
	'Name der Webapp auf Mobilgeräten'
	,
	'pwaShortName_helpText'=>
	'Kurzer Name der Webapp auf Mobilgeräten'
	,
	'emailMethod_helpText'=>
	'E-Mail Methode (mail oder smtp, bei letzterem müssen die SMTP Einstellungen konfiguriert werden.)'
	,
	'emailSmtpServer_helpText'=>
	'SMTP Server'
	,
	'emailSmtpServerPort_helpText'=>
	'SMTP Port'
	,
	'emailSmtpAuth_helpText'=>
	'SMTP Auth'
	,
	'emailSmtpUser_helpText'=>
	'SMTP Nutzer'
	,
	'emailSmtpPassword_helpText'=>
	'SMTP Passwort'
	,
	'publicPushApplicationKey_helpText'=>
	'Public Key eines VAPID Keypaares für das Abonnement von Push-Benachrichtigungen. (Kostenlos zu generieren unter: https://www.stephane-quantin.com/en/tools/generators/vapid-keys)'
	,
	'privatePushApplicationKey_helpText'=>
	'Private Key eines VAPID Keypaares für das Abonnement von Push-Benachrichtigungen. (Auf jedem Fall geheim halten!)'
	,
	'dbCollation_helpText'=>
	'Kollation der Datenbank. Um Emoticons in Texten zu ermöglichen ist utf8mb4_general_ci nötig. Ab MySQL 8 kann auch utf8mb4_0900_ai_ci genutzt werden (ist performanter)'
	,
	'defaultEditor_helpText'=>
	'Editor der Anwendung für HTML Text eingaben (summernote|ckeditor). Ckeditor ist EOL hat aber die meisten Funktionen und ist an die Dateiverwaltung zum Bilderuplaod angeschlossen. Summernote ist aktiv maintained, hat aber nicht ganz so viele Funktionen zum Einfügen von Bildern, Videos und embedded Medien.'
);