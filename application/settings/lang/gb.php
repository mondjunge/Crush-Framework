<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Configuration' =>
	'Configuration'
	,
	'Configuration Name'=>
	'Configuration Name'
	,
	'configname_helpText'=>
	'Name of this Configuration. Will be used to set a suffix on a newly created configuration file.'
	,
	'Save and activate configuration'=>
	'Save and activate configuration'
	,
	'siteStyle_helpText'=>
	'The name of the theme to use. It basically is the name of a folder in theme/*.'
	,
	'defaultLanguage_helpText'=>
	'The default language for this application/website.'
	,
	'enabledLanguages_helpText'=>
	'Language changer is automatically generating links if more than one language is selected as enabled. Press and hold [Ctrl] to select more than one language.'
	,
	'devLanguage_helpText'=>
	'Language the site is developed in. This defines as what language the yystem handles developer strings, that have to be translated.'
	,
	'Database'=>
	'Database'
	,
	'dbType_helpText'=>
	'Database connection type used. Mysql is the only available option atm.'
	,
	'dbServer_helpText'=>
	'Database servers hostname. If you need to change the port, use something like \'localhost:3306\'.'
	,
	'dbLogin_helpText'=>
	'Username for database access.'
	,
	'dbPassword_helpText'=>
	'Password for database access.'
	,
	'dbName_helpText'=>
	'Name of the database to be used.'
	,
	'dbPrefix_helpText'=>
	'A prefix for the system tables. It is also used as a prefix for Cookie and Session Variables.'
	,
	'System parameters'=>
	'System parameters'
	,
	'defaultController_helpText'=>
	'Default module to load if requested module can not be found or no module is given by the user.'
	,
	'defaultControllerAfterLogin_helpText'=>
	'Module to load after a successful login. \'login\' makes sense here, since it sports the admin menu for logged in users.'
	,
	'applicationDirectory_helpText'=>
	'The application directory (where the modules are located). Should not be changed without good knowledge.'
	,
	'relativeUrl_helpText'=>
	'The URL of your site/application, relative to the servers Document Root. Should be automatically discovered.'
	,
	'E-Mail settings'=>
	'E-Mail settings (Uses default PHP sendmail. You most certainly only have to use a registered address from your Hoster as fromEmail to make this work. Ask your Hoster for configuration guidance.)'
	,
	'fromName_helpText'=>
	'Sender name for E-Mails that are send to users by the system (e.g. notifications, password retreive, etc.).'
	,
	'fromEmail_helpText'=>
	'E-Mail address used to send E-Mails from'
	,
	'noticeEmail_helpText'=>
	'E-Mail address used to send system notifications to Superadmin.'
	,
	'Minify JS/CSS settings'=>
	'Minify JS/CSS settings'
	,
	'min_errorLogger_helpText'=>
	'Set to true to log messages to FirePHP (Firefox Firebug addon).'
	,
	'min_allowDebugFlag_helpText'=>
	'To allow debug mode output, you must set this option to true. You can manually enable debugging by appending "&debug" to a URI. E.g. /min/?f=script1.js,script2.js&debug.'
	,
	'min_cachePath_helpText'=>
	'For best performance, specify your temp directory here. Should already be computed automatically.'
	,
	'min_enableAPC_helpText'=>
	'Set to true to use APC/Memcache/ZendPlatform for cache storage. min_cachePath will have no effect if this is set to true.'
	,
	'min_documentRoot_helpText'=>
	'On some servers, this value may be misconfigured or missing. If so, set this to your full document root path with no trailing slash. E.g. \'/home/accountname/public_html\' or \'c:\\\xampp\\\htdocs\''
	,
	'min_serveOptions_maxAge_helpText'=>
	'Cache-Control: max-age value sent to browser (in seconds). You may want to shorten this before making changes if it is crucial those changes are seen immediately.'
	,
	'registrationOpen_helpText'=>
	'Set to true if you want visitors to be able to register themselfs and create a Useraccount.'
	,
	'debugMode_helpText'=>
	'Enables global debug mode. Caution: Shows debug messages to all visitors.'
    ,
	'maintainanceMode_helpText'=>
	'Only Superadmins can login, use functions and view content.'
	,
	'Configuration saved successfully.'=>
	'Configuration saved successfully.'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'Create new configuration'=>
	'Create new configuration'
	,
	'configselect_helpText'=>
	'configselect_helpText'
	,
	'Select a configuration'=>
	'Select a configuration'
	,
	'Load configuration'=>
	'Load configuration'
	,
	'friendsEnabled_helpText'=>
	'friendsEnabled_helpText'
	,
	'rootUrl_helpText'=>
	'rootUrl_helpText'
	,
	'AJAXsupport_helpText'=>
	'AJAXsupport_helpText'
	,
	'Current edited configuration is: '=>
	'Current edited configuration is: '
	,
	'Currently edited configuration is: '=>
	'Currently loaded for editing is: '
	,
	'Currently active configuration is: '=>
	'Currently active configuration is: '
	,
	'Save configuration only'=>
	'Save configuration only'
	,
	'showAppIcons_helpText'=>
	'If this option is enabled, there will be Icons rendered on every navigation item, based on icons in /theme/{siteStyle}/images/appIcons/'
	,
	'updateEndpoint_helpText'=>
	'URL to the update server.'
	,
	'cssCacheLifetime_helpText'=>
	'How long generated css files are valid until the webserver generates new cache files.'
	,
	'Save configuration'=>
	'Save configuration'
	,
	'Configuration {1} is now loaded for editing.'=>
	'Configuration {1} is now loaded for editing.'
	,
	'Configuration [1] is now loaded for editing.'=>
	'Configuration [1] is now loaded for editing.'
	,
	'The value differs from the default value.'=>
	'The value differs from the default value.'
	,
	'The following value differs from the default value!'=>
	'The following value differs from the default value!'
	,
	'Set default value'=>
	'Set default value'
	,
	'Configuration $1 is now loaded for editing.'=>
	'Configuration $1 is now loaded for editing.'
	,
	'stay logged in'=>
	'stay logged in'
	,
	'name_helpText'=>
	'name_helpText'
	,
	'shortName_helpText'=>
	'shortName_helpText'
	,
	'PWA Settings'=>
	'PWA Settings'
	,
	'pwaName_helpText'=>
	'pwaName_helpText'
	,
	'pwaShortName_helpText'=>
	'pwaShortName_helpText'
	,
	'emailMethod_helpText'=>
	'emailMethod_helpText'
	,
	'emailSmtpServer_helpText'=>
	'emailSmtpServer_helpText'
	,
	'emailSmtpServerPort_helpText'=>
	'emailSmtpServerPort_helpText'
	,
	'emailSmtpAuth_helpText'=>
	'emailSmtpAuth_helpText'
	,
	'emailSmtpUser_helpText'=>
	'emailSmtpUser_helpText'
	,
	'emailSmtpPassword_helpText'=>
	'emailSmtpPassword_helpText'
	,
	'publicPushApplicationKey_helpText'=>
	'Public Key of VAPID Keypair for Push-Subscribtions. (Free to generate under: https://www.stephane-quantin.com/en/tools/generators/vapid-keys)'
	,
	'privatePushApplicationKey_helpText'=>
	'Private Key of VAPID Keypair for Push-Subscribtions. (Do not share under no circumstances)'
	,
	'dbCollation_helpText'=>
	'Database Collation should be utf8mb4_0900_ai_ci for MYSQL 8+ and utf8mb4_general_ci for MYSQL <8'
	,
	'defaultEditor_helpText'=>
	'(ckeditor|summernote) ckeditor is end of life, but has the most advanced features for an HTML Editor. Summernote is actively maintained.'
);