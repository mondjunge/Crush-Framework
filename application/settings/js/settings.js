/**
 * settings.js
 * handles save buttons on changing form for better ux
 */
use_package('modules');
modules.settings = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    this.init = function () {
        handleConfChange();
    };

    var handleConfChange = function () {
        $('#configname').keyup(function () {
            console.log("useconfig:" + $('#useConfig').val());
            console.log("configname:" + $(this).val());
            console.log("editConfig:" + $('#editConfig').val());
            if ($('#useConfig').val() !== $(this).val() && $(this).val() !== '') {
                console.log("show CHANGED conf save button");
                $('.confSaveButton').addClass('hidden');
                $('.changedConfSaveButtons').removeClass('hidden');
            } else if ($('#configname').val() === '' || $('#useConfig').val() === $(this).val()) {
                console.log("show conf save button");
                $('.changedConfSaveButtons').addClass('hidden');
                $('.confSaveButton').removeClass('hidden');
            }
        });

    };

//    this.ajaxRefresh = function(){
//        
//    };
//    
//    this.ajaxStart = function(){
//        
//    };


};