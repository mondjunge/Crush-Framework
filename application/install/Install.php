<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Install extends AppController {
    /**
     * Security Audit 
     * Datum: 25.06.2020
     * Gefährdung: hoch
     * Komplexität: niedrig
     * Status: OK
     */
    /**
     * Modules to install upon install
     * @var array 
     */
    private $installOnInstall = array(1 => 'site', 2 => 'user', 3 => 'login', 4 => 'settings', 5 => 'files', 6 => 'devices', 7 => 'userstyle');

    public function __construct(){
        $this->setTitle($this->ts('Installation'));
        ini_set("error_reporting", E_ERROR | E_PARSE);
    }
    
    public function checkConfiguration(){
        // TODO: implement methods 
        /**
         * 'dbType' => 'Mysql', 
    *'dbServer' => 'localhost', 
    *'dbLogin' => 'root', 
    *'dbPassword' => '', 
    *'dbName' => 'crushdb', 
    *'dbPrefix' => 'crush_
         */
        //filter_input(INPUT_POST, 'dbType');
    }
    /**
     * flow control
     */
    public function index() {
        
        
        if((!$this->isInstalled()) && filter_has_var(INPUT_GET, 'deleteConf')){
            //echo "reset request"; exit;
            include 'config/useConfig.php';
            unlink("config/config_".$useConfig.".php");
            unlink('config/useConfig.php');
            Link::jumpTo("install");
        }
        if((!$this->isInstalled()) && filter_has_var(INPUT_GET, 'reset')){
            //echo "reset request"; exit;
            include 'config/useConfig.php';
            $data['configFile'] = "config_".$useConfig.".php";
            $this->view("resetRequest",$data);
            return;
        }
        if ($this->isInstalled()) {
            // just show some "everything done, nothing to do" when System is installed.
            
            $this->viewMainInstallation();
            return;
        }
        
        if(filter_has_var(INPUT_POST, 'systemCheckComplete')){
            Session::set("systemCheckComplete", "true",true,false);
        }

        if (filter_has_var(INPUT_POST, 'saveConfiguration')) {
            $this->createConfiguration();
            $this->jumpTo('install');
        }

        if (filter_has_var(INPUT_POST, 'createdb') && !$this->dbExists()) {
            $this->createDB();
        }
        if (filter_has_var(INPUT_POST, 'installModules') && !$this->tableExists('site')) {
            $Installer = new Installer();
            $Installer->installModules($this->installOnInstall);
        }
        if (filter_has_var(INPUT_POST, 'insertSuperadmin') && $this->count('user') == 0) {
            $user = filter_input(INPUT_POST, 'user');
            $pass = filter_input(INPUT_POST, 'pass');
            $this->insertSuperuser($user, $pass);
            $this->additionalInits();
        }
        
        if(is_file("downloadAndExtract.php") && !is_file("buildCore.sh") && !is_file("buildApplication.sh")){
            //delete extract.php automatically on install if its not a developer environment
            unlink('extract.php');
        }
        if(Session::get("systemCheckComplete",false) == "true" || filter_has_var(INPUT_POST, 'systemCheckComplete')){
            $this->viewMainInstallation();
        }else{
            $this->viewSystemCheck();
        }
        
        //$this->viewMainInstallation();
    }
    
    private function viewSystemCheck(){
        /*
         *       TODO:
         *      - PHP Version
         *      - MYSQL Version
         *      - CURL
         *      - simpleXML
         *      - folders writable:
         *          - cache
         *          - config
         *          - upload
         *          - theme
         *          - application
         */
        
        // check for PHP version
        $data['phpVersion'] = "7.3";
        $data['phpVersionCheck'] = version_compare(PHP_VERSION, $data['phpVersion'], '>=');
        
        // check for mysqli extension
        $data['mysqliExtensionCheck'] = phpversion('mysqli')==""?false:true;
        
        // check for curl extension
        $data['curlExtensionCheck'] = phpversion('curl')==""?false:true;
        
        // check for simpleXML
        $data['simpleXMLExtensionCheck'] = phpversion('simplexml')==""?false:true;
        
        //check for imagemagick
        $data['imagickCheck'] = extension_loaded('imagick') || phpversion('gd')==""?false:true;
        
        // Schreibbare Ordner benötigt:
        $data['writableDirs'] = array('application','cache','cache/html','cache/thumbs','cache/thumbs/src','config','theme','upload');
          
        
        $this->view('systemCheck',$data);
    }
    
    private function isConfigured() {
        return is_file('config/useConfig.php');
    }

    private function viewConfiguration() {
        $ConfigWriter = new ConfigWriter();
        $aConfig = $ConfigWriter->readConfig('config/config.php');
        $data['config'] = $aConfig;
        $data['step'] = 1;
        $data['availableSiteStyles'] = $this->getAvailableSiteStyles();
        $data['wizard'] = $this->fetch('wizard', $data);
        $this->view('config', $data);
       
    }
    
     private function getAvailableSiteStyles() {
        $path = $_SERVER['DOCUMENT_ROOT'] . Config::get("relativeUrl") . "theme/";
        $d = dir($path);
        $fileArray = array();
        while (false !== ($entry = $d->read())) {
            if (is_dir($path.$entry) !== FALSE && strpos($entry, '.', 0) === FALSE) {
                $fileArray[] = $entry;
            }
        }
        //print_r($fileArray);
        return $fileArray;
    }

    private function createConfiguration() {
        try{
            $this->checkConfiguration();
        } catch (Exception $ex) {
            $this->setErrorMessage($ex->getMessage());
            return;
        }
        $ConfigWriter = new ConfigWriter();
        
        $configName = filter_input(INPUT_POST, 'configname');
        $newConfigFilePath = 'config/config_' . $configName . '.php';
        
        if (is_file($newConfigFilePath)) {
            $this->setErrorMessage($this->ts('You already have a configuration with that name ($1). No files were written.', array(1 => $newConfigFilePath)));
            return;
        }
        
        if ($ConfigWriter->createConfiguration(false) == FALSE) {
            // fail
            $this->setErrorMessage($this->ts("An Error occured while saving configuration. You might have to manually edit it. Sorry."));
        } else {
            $this->setSystemMessage($this->ts("New Configuration saved successfully."));
        }
    }

    private function viewMainInstallation() {
        
        if (!$this->isConfigured()) {
            $this->viewConfiguration();
            return;
        }
        
        if (!$this->dbExists()) {
            $data['dbName'] =  Config::get('dbName');
            $data['dbExists'] = false;
            $data['step'] = 2;
            $data['wizard'] = $this->fetch('wizard', $data);
            $this->view('install', $data);
            return;
        }
        if (!$this->tableExists('user') || !$this->tableExists('site')) {
            $data['step'] = 3;
            $data['modulesToInstall'] = join(", ", $this->installOnInstall);
            $data['wizard'] = $this->fetch('wizard', $data);
            $this->view('install', $data);
            return;
        }
        if ($this->tableExists('user') && $this->tableExists('site') && $this->count('user') == 0) {
            $data['step'] = 4;
            $data['wizard'] = $this->fetch('wizard', $data);
            $this->view('install', $data);
            return;
        }

        $data['step'] = 5; // Finished
        $data['wizard'] = $this->fetch('wizard', $data);
        $this->view('install', $data);
    }

    private function insertSuperuser($user, $pass) {

        $randomSalt = $this->generateRandomString(64);

        $passHash = $randomSalt . hash('sha512', $randomSalt . $pass);

        $fieldsValueArray = array(
            'nick' => $user,
            'password' => $passHash,
            'active' => 1,
            'roles' => '1'
        );

        if (!$this->insert('user', $fieldsValueArray)) {
            $this->setErrorMessage($this->ts('Could not add Superuser!'));
        } else {
            $this->setSystemMessage($this->ts('successfully added Superuser'));
        }
    }

    /**
     * creates the configured database
     * @global type $aConfig 
     */
    private function createDB() {
        if (!$this->query("CREATE DATABASE `" . Config::get('dbName') . "`  
            DEFAULT CHARACTER SET utf8mb4  
            DEFAULT COLLATE utf8mb4_unicode_ci ")) {
            $this->setErrorMessage($this->ts("Database could not be created!"));
        } else {
            $this->setSystemMessage($this->ts("Database created successfully"));
        }
        Link::jumpTo("install");
    }

    private function additionalInits() {
        
    }

}
