<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Create Database'=>
	'Datenbank erstellen'
	,
	'The Database \''=>
	'Die Datenbank \''
	,
	'\' does not exist.'=>
	'\' existiert nicht'
	,
	'Should it be created now?'=>
	'Soll sie jetzt erstellt werden?'
	,
	'Create database now!'=>
	'Erstelle die Datenbank!'
	,
	'Database created successfully'=>
	'Datenbank wurde erfolgreich erstellt.'
	,
	'Create Tables'=>
	'Erstelle Tabellen'
	,
	'Create selected table(s)'=>
	'Erstelle die ausgewählten Tabellen'
	,
	'All tables installed.'=>
	'Alle Tabellen sind installiert.'
	,
	'Main Installation'=>
	'Erstinstallation'
	,
	'The Database \'$1\' exists.'=>
	'Die Datenbank \'$1\' gibt es bereits.'
	,
	'Nothing to do.'=>
	'Nichts zu tun'
	,
	'The Database \'$1\' does not exist.'=>
	'Die Datenbank \'$1\' ist nicht vorhanden.'
	,
	'Check'=>
	'Überprüfung'
	,
	'Action'=>
	'Aktion'
	,
	'The Database \'$1\' will be created for you.'=>
	'Die Datenbank \'$1\' wird erstellt.'
	,
	'System tables are already there.'=>
	'Systemmodule sind bereits vorhanden.'
	,
	'Installation will be aborted.'=>
	'Installation wird nicht durchgeführt.'
	,
	'No system tables found.'=>
	'Keine Systemmodule installiert.'
	,
	'Tables \'site\' and \'user\' will be created for you.'=>
	'Die Module \'site\' und \'user\' werden installiert.'
	,
	'Please provide initial user credentials. This user will be created and given Superadmin powers upon installation.'=>
	'Bitte Benutzername und Passwort für Superadmin angeben. Der Nutzer wird während der Installation angelegt und bekommt Superadmin Kräfte.'
	,
	'username'=>
	'Benutzername'
	,
	'password'=>
	'Passwort'
	,
	'install now'=>
	'Jetzt installieren'
	,
	'Create Superadmin User'=>
	'Lege Superadmin an'
	,
	'Install System Modules'=>
	'Systemmodule installieren'
	,
	'Superadmin password have to be at least 6 characters long. Please use a save password.'=>
	'Bitte ein sicheres Passwort verwenden. Superadmin Passwort muss mindestens 6 Zeichen lang sein!'
	,
	'You need to give your Superadmin a name.'=>
	'Der Superadmin braucht einen Nicknamen.'
	,
	'Complete'=>
	'Installation abgeschlossen'
	,
	'create db'=>
	'Datenbank erstellen'
	,
	'install modules'=>
	'Basismodule installieren'
	,
	'insert Superadmin'=>
	'Superadmin anlegen'
	,
	'Installation is finished'=>
	'Installation ist fertig!'
	,
	'Nothing else to do.'=>
	'Es gibt nichts weiter zu tun.'
	,
	'There is nothing else to do. You can login with your Superadmin now.'=>
	'Es gibt nichts weiter zu tun. Du kannst Dich nun mit Deinem neuen Superadminaccount anmelden.'
	,
	'already there'=>
	'bereits vorhanden.'
	,
	'added successfully to sites'=>
	'wurde erfolgreich zu Seiten hinzugefügt'
	,
	'successfully added Superuser'=>
	'Superuser erfolgreich angelegt.'
	,
	'Go to login.'=>
	'Zur Anmeldeseite'
	,
	'save configuration'=>
	'Speicher Konfiguration'
	,
	'Configuration'=>
	'Basis Konfiguration'
	,
	'siteStyle_helpText'=>
	'Lege das Thema der Seite fest.'
	,
	'registrationOpen_helpText'=>
	'Ist diese Option aktiviert, können sich Besucher der Seite selber als Nutzer registrieren, aka. einen Nutzeraccount anlegen.'
	,
	'defaultLanguage_helpText'=>
	'Die vorausgewählte Sprache in der die Seite angezeigt wird.'
	,
	'enabledLanguages_helpText'=>
	'Aktivierte Sprachen. Sind mehr als eine Sprache aktiv, so bekommt der Nutzer die Möglichkeit zwischen Sprachen zu wählen.'
	,
	'devLanguage_helpText'=>
	'Sprache in der entwickelt wird. Sollte nicht geändert werden.'
	,
	'dbType_helpText'=>
	'Datenbank Typ. Aktuell steht nur ein MySQL Connector zur Verfügung.'
	,
	'dbServer_helpText'=>
	'Datenbank Server. Ein Port kann optional mit Doppelpunkt getrennt angegeben werden. Bsp: localhost:3306.'
	,
	'dbLogin_helpText'=>
	'Zu verwendender Datenbank Nutzer.'
	,
	'dbPassword_helpText'=>
	'Passwort des obigen Datenbank Nutzers.'
	,
	'dbName_helpText'=>
	'Name der Datenbank.'
	,
	'dbPrefix_helpText'=>
	'Prefix für die Datenbank Tabellen.'
	,
	'fromName_helpText'=>
	'Der Name des Absenders von E-Mail die das System an Nutzer schickt.'
	,
	'fromEmail_helpText'=>
	'Die Absender E-Mail Adresse von der Nachrichten an die Nutzer geschickt werden.'
	,
	'noticeEmail_helpText'=>
	'An diese E-Mail Adresse verschickt das System wichtige Nachrichten für Superadmins.'
	,
	'defaultController_helpText'=>
	'Gebe hier ein Modulname, optional modul/methode an, die beim Aufruf der Seite ausgeführt werden soll. Wird \'site\' angegeben, so wird die erste im Menü auffindbare statische Seite geladen.'
	,
	'defaultControllerAfterLogin_helpText'=>
	'Gebe hier ein Modulname, optional modul/methode an, die beim Aufruf der Seite für angemeldete Nutzer ausgeführt werden soll.'
	,
	'applicationDirectory_helpText'=>
	'Verzeichnis in dem die Module liegen. Sollte nicht unbedacht geändert werden.'
	,
	'debugMode_helpText'=>
	'Aktiviert debug Mode global. Achtung, wenn aktiv, sehen alle Besucher der Seite debug Ausgaben.'
	,
	'relativeUrl_helpText'=>
	'Pfad relativ zum Apache Document Root.'
	,
	'min_errorLogger_helpText'=>
	'Aktiviert den Minify Error Logger.'
	,
	'min_allowDebugFlag_helpText'=>
	'Wenn aktiv, kann man ein flag an der URL setzen um debug informationen zu erhalten.'
	,
	'min_cachePath_helpText'=>
	'Pfad zum Ordner in dem Minify cached. Wird ignoriert, wenn APC aktiv ist. Wird automatisch ermittelt.'
	,
	'min_enableAPC_helpText'=>
	'Aktivert den Pecl APC (Alternative PHP Cache).'
	,
	'min_documentRoot_helpText'=>
	'DocumentRoot von minify. Wird automatisch ermittelt.'
	,
	'min_serveOptions_maxAge_helpText'=>
	'Bestimmt in Sekunden, wie lange die gecachten Minify Dateien gültig sind, bevor ein Browser neue anfordert.'
	,
	'You already have a configuration with that name ($1). No files were written.'=>
	'Es existiert bereits eine Konfigurationsdatei mit dem Namen $1. Es wurde keine Konfigurationsdatei angelegt.'
	,
	'Database'=>
	'Datenbank Einstellungen'
	,
	'System parameters'=>
	'System Parameter'
	,
	'E-Mail settings'=>
	'E-Mail Einstellungen'
	,
	'Minify JS/CSS settings'=>
	'Minify JS/CSS Einstellungen'
	,
	'Configuration Name'=>
	'Name der Konfiguration'
	,
	'configname_helpText'=>
	'Gebe einen Namen für diese Konfiguration an. Dieser wird als Suffix für die neue Konfigurationsdatei genutzt, bsp. config_default.php'
	,
	'Installation'=>
	'Installation'
	,
	'New Configuration saved successfully.'=>
	'Konfiguration erfolgreich gespeichert.'
	,
	'Create Configuration'=>
	'Konfiguration anlegen'
	,
	'maintainanceMode_helpText'=>
	'Ist der Wartungsmodus aktiviert, so wird Besuchern und Nutzern leiglich ein Hinweis zur Wartung angezeigt. Nur Superadmins könne sich einloggen und die Inhalte sehen/auf Funktionen zugreifen.'
	,
	'Basic modules ($1) will be installed for your convinience.'=>
	'Basis Module ($1) werden automatisch installiert.'
	,
	'Delete configuration and start over'=>
	'Lösche Konfiguration und beginne von vorne'
	,
	'Warning: This action will delete your current configuration and you have to start over.'=>
	'Achtung: Diese Aktion wird die aktuelle Konfiguration löschen und die Einrichtung muss von vorne begonnen werden.'
	,
	'Warning: This action will delete your current configuration ($1) and you have to start over.'=>
	'Achtung: Diese Aktion wird die aktuelle Konfiguration ($1) löschen und die Einrichtung muss von vorne begonnen werden.'
	,
	'key not set!'=>
	'Schlüssel nicht gesetzt!'
	,
	'showAppIcons_helpText'=>
	'Zeige Icons vor den Navigationspunkten an oder nicht.'
	,
	'updateEndpoint_helpText'=>
	'URL des Update Servers.'
	,
	'System Check'=>
	'Systemprüfung'
	,
	'PHP Version required'=>
	'Benötigte PHP Version'
	,
	'Required PHP Extensions'=>
	'Benötigte PHP Erweiterungen'
	,
	'Writeable Directories'=>
	'Schreibbare Verzeichnisse'
	,
	'To Configuration'=>
	'Weiter zur Konfiguration'
	,
	'javascriptDebugMode_helpText'=>
	'Aktivieren um Javascript Debug Ausgaben in der Konsole zu erhalten'
	,
	'emailMethod_helpText'=>
	''
	,
	'emailSmtpServer_helpText'=>
	''
	,
	'emailSmtpServerPort_helpText'=>
	''
	,
	'emailSmtpAuth_helpText'=>
	''
	,
	'emailSmtpUser_helpText'=>
	''
	,
	'emailSmtpPassword_helpText'=>
	''
	,
	'PWA Settings'=>
	''
	,
	'pwaName_helpText'=>
	''
	,
	'pwaShortName_helpText'=>
	''
	,
	'Could not add Superuser!'=>
	''
	,
	'publicPushApplicationKey_helpText'=>
	''
	,
	'privatePushApplicationKey_helpText'=>
	''
);