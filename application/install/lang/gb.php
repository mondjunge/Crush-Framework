<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Create Database'=>
	'Create Database'
	,
	'The Database \''=>
	'The Database \''
	,
	'\' does not exist.'=>
	'\' does not exist.'
	,
	'Should it be created now?'=>
	'Should it be created now?'
	,
	'Create database now!'=>
	'Create database now!'
	,
	'Database created successfully'=>
	'Database created successfully'
	,
	'Create Tables'=>
	'Create Tables'
	,
	'Create selected table(s)'=>
	'Create selected table(s)'
	,
	'All tables installed.'=>
	'All tables installed.'
	,
	'Main Installation'=>
	'Main Installation'
	,
	'It will be created for you.'=>
	'It will be created for you.'
	,
	'\' exist.'=>
	'\' exist.'
	,
	'OK'=>
	'OK'
	,
	'The Database $1 exists\''=>
	'The Database $1 exists\''
	,
	'The Database \'$1\' exists'=>
	'The Database \'$1\' exists'
	,
	'The Database \'$1\' exists.'=>
	'The Database \'$1\' exists.'
	,
	'Nothing to do.'=>
	'Nothing to do.'
	,
	'The Database \'$1\' does not exist.'=>
	'The Database \'$1\' does not exist.'
	,
	'Check'=>
	'Check'
	,
	'Action'=>
	'Action'
	,
	'The Database \'$1\' will be created for you.'=>
	'The Database \'$1\' will be created for you.'
	,
	'System tables are already there.'=>
	'System tables are already there.'
	,
	'Installation will be aborted.'=>
	'Installation will be aborted.'
	,
	'No system tables found.'=>
	'No system tables found.'
	,
	'Tables \'site\' and \'user\' will be created for you.'=>
	'Tables \'site\' and \'user\' will be created for you.'
	,
	'Please provide initial user credentials'=>
	'Please provide initial user credentials'
	,
	'Please provide initial user credentials. This user will be created upon installation and given Superadmin rights.'=>
	'Please provide initial user credentials. This user will be created upon installation and given Superadmin rights.'
	,
	'Please provide initial user credentials. This user will be created and given Superadmin powers upon installation.'=>
	'Please provide initial user credentials. This user will be created and given Superadmin powers upon installation.'
	,
	'username'=>
	'username'
	,
	'password'=>
	'password'
	,
	'install now'=>
	'Install now'
	,
	'Create Superadmin User'=>
	'Create Superadmin User'
	,
	'Install System Modules'=>
	'Install System Modules'
	,
	'Superadmin password have to be at least 6 characters long. Please use a save password.'=>
	'Superadmin password have to be at least 6 characters long. Please use a save password.'
	,
	'You need to give your Superadmin a name.'=>
	'You need to give your Superadmin a name.'
	,
	'Complete'=>
	'Complete'
	,
	'create db'=>
	'Create db'
	,
	'install modules'=>
	'Install modules'
	,
	'insert Superadmin'=>
	'Insert Superadmin'
	,
	'Installation is finished'=>
	'Installation is finished'
	,
	'Nothing else to do.'=>
	'Nothing else to do.'
	,
	'There is nothing else to do. You can login with your Superadmin now.'=>
	'There is nothing else to do. You can login with your Superadmin now.'
	,
	'already there'=>
	'already there'
	,
	'added successfully to sites'=>
	'added successfully to sites'
	,
	'successfully added Superuser'=>
	'successfully added Superuser'
	,
	'Go to login.'=>
	'Go to login'
	,
	'Configuration'=>
	'Configuration'
    ,
	'Configuration Name'=>
	'Configuration Name'
    ,
	'configname_helpText'=>
	'Name of this Configuration. Will be used to set a suffix on a newly created configuration file.'
	,
	'save configuration'=>
	'Save configuration'
	,
	'siteStyle_helpText'=>
	'The name of the theme to use. It basically is the name of a folder in theme/*.'
	,
	'defaultLanguage_helpText'=>
	'The default language for this application/website.'
	,
	'enabledLanguages_helpText'=>
	'Language changer is automatically generating links if more than one language is selected as enabled. Press and hold [Ctrl] to select more than one language.'
	,
	'devLanguage_helpText'=>
	'Language the site is developed in. This defines as what language the yystem handles developer strings, that have to be translated.'
	,
	'Database'=>
	'Database'
	,
	'dbType_helpText'=>
	'Database connection type used. Mysql is the only available option atm.'
	,
	'dbServer_helpText'=>
	'Database servers hostname. If you need to change the port, use something like \'localhost:3306\'.'
	,
	'dbLogin_helpText'=>
	'Username for database access.'
	,
	'dbPassword_helpText'=>
	'Password for database access.'
	,
	'dbName_helpText'=>
	'Name of the database to be used.'
	,
	'dbPrefix_helpText'=>
	'A prefix for the system tables. It is also used as a prefix for Cookie and Session Variables.'
	,
	'System parameters'=>
	'System parameters'
	,
	'defaultController_helpText'=>
	'Default module to load if requested module can not be found or no module is given by the user.'
	,
	'defaultControllerAfterLogin_helpText'=>
	'Module to load after a successful login. \'login\' makes sense here, since it sports the admin menu for logged in users.'
	,
	'applicationDirectory_helpText'=>
	'The application directory (where the modules are located). Should not be changed without good knowledge.'
	,
	'relativeUrl_helpText'=>
	'The URL of your site/application, relative to the servers Document Root. Should be automatically discovered.'
	,
	'E-Mail settings'=>
	'E-Mail settings (uses default PHP sendmail. Ask your Hoster for configuration guidance. You most certainly only have to use a registered address from your Hoster as fromEmail to make this work.)'
	,
	'fromName_helpText'=>
	'Sender name for E-Mails that are send to users by the system (e.g. notifications, password retreive, etc.).'
	,
	'fromEmail_helpText'=>
	'E-Mail address used to send E-Mails from'
	,
	'noticeEmail_helpText'=>
	'E-Mail address used to send system notifications to Superadmin.'
	,
	'Minify JS/CSS settings'=>
	'Minify JS/CSS settings'
	,
	'min_errorLogger_helpText'=>
	'Set to true to log messages to FirePHP (Firefox Firebug addon).'
	,
	'min_allowDebugFlag_helpText'=>
	'To allow debug mode output, you must set this option to true. You can manually enable debugging by appending "&debug" to a URI. E.g. /min/?f=script1.js,script2.js&debug.'
	,
	'min_cachePath_helpText'=>
	'For best performance, specify your temp directory here. Should already be computed automatically.'
	,
	'min_enableAPC_helpText'=>
	'Set to true to use APC/Memcache/ZendPlatform for cache storage. min_cachePath will have no effect if this is set to true.'
	,
	'min_documentRoot_helpText'=>
	'On some servers, this value may be misconfigured or missing. If so, set this to your full document root path with no trailing slash. E.g. \'/home/accountname/public_html\' or \'c:\\\xampp\\\htdocs\''
	,
	'min_serveOptions_maxAge_helpText'=>
	'Cache-Control: max-age value sent to browser (in seconds). You may want to shorten this before making changes if it is crucial those changes are seen immediately.'
	,
	'registrationOpen_helpText'=>
	'Set to true if you want Visitors to be able to register themselfs and create a Useraccount.'
	,
	'debugMode_helpText'=>
	'Enables global debug mode. Caution: Shows Debug Messages to all Visitors.'
        ,
	'maintainanceMode_helpText'=>
	'Only Superadmins can login and use functions and view content. All others can only access the login page and see a warning about maintainance mode.'
	,
	'Installation'=>
	'Installation'
	,
	'New Configuration saved successfully.'=>
	'New Configuration saved successfully.'
	,
	'Create Configuration'=>
	'Create Configuration'
        ,
        'You already have a configuration with that name ($1). No files were written.'=>
	'You already have a configuration with that name ($1). No files were written.'
	,
	'Basic modules ($1) will be installed for your convinience.'=>
	'Basic modules ($1) will be installed for your convinience.'
	,
	'Delete configuration and start over'=>
	'Delete configuration and start over'
	,
	'Warning: This action will delete your current configuration ($1) and you have to start over.'=>
	'Warning: This action will delete your current configuration ($1) and you have to start over.'
	,
	'System Check'=>
	'System Check'
	,
	'PHP Version required'=>
	'PHP Version required'
	,
	'Required PHP Extensions'=>
	'Required PHP Extensions'
	,
	'Writeable Directories'=>
	'Writeable Directories'
	,
	'To Configuration'=>
	'To Configuration'
	,
	'javascriptDebugMode_helpText'=>
	'Activate to get debug messages in Javascript Console'
	,
	'showAppIcons_helpText'=>
	'Activate to show icons in the navigation'
	,
	'updateEndpoint_helpText'=>
	'URL of the update server'
	,
	'emailMethod_helpText'=>
	'emailMethod_helpText'
	,
	'emailSmtpServer_helpText'=>
	'emailSmtpServer_helpText'
	,
	'emailSmtpServerPort_helpText'=>
	'emailSmtpServerPort_helpText'
	,
	'emailSmtpAuth_helpText'=>
	'emailSmtpAuth_helpText'
	,
	'emailSmtpUser_helpText'=>
	'emailSmtpUser_helpText'
	,
	'emailSmtpPassword_helpText'=>
	'emailSmtpPassword_helpText'
	,
	'PWA Settings'=>
	'PWA Settings'
	,
	'pwaName_helpText'=>
	'pwaName_helpText'
	,
	'pwaShortName_helpText'=>
	'pwaShortName_helpText'
	,
	'Could not add Superuser!'=>
	'Could not add Superuser!'
	,
	'publicPushApplicationKey_helpText'=>
	'publicPushApplicationKey_helpText'
	,
	'privatePushApplicationKey_helpText'=>
	'privatePushApplicationKey_helpText'
);