<h2><?php echo $this->ts('System Check'); ?></h2>


<div class="pure-g pure-g-r systemCheck">
    <div class="pure-u-1-3">
        <?php echo $this->ts("PHP Version required")." >= ".$phpVersion; ?>
    </div>
    <div class="pure-u-2-3">
        <?php if ($phpVersionCheck) : ?>
            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#check', 'svgIcon success'); ?>
        <?php else: ?>
            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#x', 'svgIcon fail'); ?>
        <?php endif; ?>
        <b><?php echo PHP_VERSION; ?></b>
    </div>
</div>

<div class="pure-g pure-g-r systemCheck">
    <div class="pure-u-1-3">
        <?php echo $this->ts("Required PHP Extensions"); ?>
    </div>
    <div class="pure-u-2-3">
        <div>
            <?php if ($mysqliExtensionCheck) : ?>
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#check', 'svgIcon success'); ?>
            <?php else: ?>
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#x', 'svgIcon fail'); ?>
            <?php endif; ?>
            <b><?php echo "mysqli"; ?></b>
        </div>
        <div>
            <?php if ($curlExtensionCheck) : ?>
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#check', 'svgIcon success'); ?>
            <?php else: ?>
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#x', 'svgIcon fail'); ?>
            <?php endif; ?>
            <b><?php echo "curl"; ?></b>
        </div>
        <div>
            <?php if ($simpleXMLExtensionCheck) : ?>
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#check', 'svgIcon success'); ?>
            <?php else: ?>
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#x', 'svgIcon fail'); ?>
            <?php endif; ?>
            <b><?php echo "simplexml"; ?></b>
        </div>
        <div>
            <?php if ($imagickCheck) : ?>
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#check', 'svgIcon success'); ?>
            <?php else: ?>
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#x', 'svgIcon fail'); ?>
            <?php endif; ?>
            <b><?php echo "imagick || gd"; ?></b>
        </div>
    </div>
</div>

<div class="pure-g pure-g-r systemCheck">
    <div class="pure-u-1-3">
        <?php echo $this->ts("Writeable Directories"); ?>
    </div>
    <div class="pure-u-2-3">
        <?php foreach ($writableDirs as $dir) : ?>
            <div>
                <?php if (is_writeable($dir)) : ?>
                    <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#check', 'svgIcon success'); ?>
                <?php else: ?>
                    <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#x', 'svgIcon fail'); ?>
                <?php endif; ?>
                <b><?php echo $dir; ?></b>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<form class="pure-form pure-form-aligned" action="<?php echo $this->action('install'); ?>" method="POST">
    
    <div class="pure-controls">
        <input class="pure-button pure-button-primary" type="submit" name="systemCheckComplete" value="<?php echo $this->ts('To Configuration'); ?>" />
    </div>
    
</form>