<?php echo $this->ts("Warning: This action will delete your current configuration ($1) and you have to start over.", array(1 => $configFile)); ?>
<br/>
<a class="pure-button pure-button-error" href="<?php echo $this->action("install?deleteConf"); ?>"><?php echo $this->ts("Delete configuration and start over"); ?></a>

