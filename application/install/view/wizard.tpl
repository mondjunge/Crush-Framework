<div>
    <ol class="progtrckr" data-progtrckr-steps="4">
        <li class="<?php echo ($step > 1) ? 'progtrckr-done' : 'progtrckr-todo'; ?>"><?php echo $this->ts('Create Configuration'); ?></li><!--
        --><li class="<?php echo ($step > 2) ? 'progtrckr-done' : 'progtrckr-todo'; ?>"><?php echo $this->ts('Create Database'); ?></li><!--
        --><li class="<?php echo ($step > 3) ? 'progtrckr-done' : 'progtrckr-todo'; ?>"><?php echo $this->ts('Install System Modules'); ?></li><!--
        --><li class="<?php echo ($step > 4) ? 'progtrckr-done' : 'progtrckr-todo'; ?>"><?php echo $this->ts('Create Superadmin User'); ?></li>
    </ol>
</div>