<h1><?php echo $this->ts('Main Installation') ?></h1>
<?php echo $wizard; ?>

<form class="pure-form pure-form-aligned" action="<?php $this->action('install') ?>" method="POST">
    <fieldset>
        <legend><?php echo $this->ts("Configuration"); ?></legend>
        <div class="pure-control-group">
            <label style="width: 13em;"for="configname" ><?php echo $this->ts('Configuration Name') ?></label>
            <input class="pure-input-1-2" id="configname" name="configname" type="text" value="default" />
            <div class="help help-margin"><?php echo $this->ts('configname_helpText') ?></div>
        </div>
        <?php
        foreach ($config as $key => $value) :

            $type = gettype($value);
            ?>
            <?php if ($key == 'dbType') : ?>
                <legend><?php echo $this->ts("Database"); ?></legend>
            <?php endif; ?>
            <?php if ($key == 'defaultController') : ?>
                <legend><?php echo $this->ts("System parameters"); ?></legend>
            <?php endif; ?>
            <?php if ($key == 'fromName') : ?>
                <legend><?php echo $this->ts("E-Mail settings"); ?></legend>
            <?php endif; ?>
            <?php if ($key == 'pwaName') : ?>
                <legend><?php echo $this->ts("PWA Settings"); ?></legend>
            <?php endif; ?>
            <?php if ($type == 'boolean'): ?>
                <div class="pure-control-group">
                    <label for="<?php echo $key ?>" class="pure-checker" tabindex="0" title="<?php echo $key ?>">
                        <div class="checker">
                            <input id="<?php echo $key ?>" class="checkbox" name="<?php echo $key ?>" <?php echo ($value ? 'checked="checked"' : ''); ?> type="checkbox" />
                            <div class="check-bg"></div>
                            <div class="checkmark">
                                <svg viewBox="0 0 100 100">
                                <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <span class="label"><?php echo $key ?></span>
                    </label>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>

                </div>
            <?php elseif ($key == 'enabledLanguages'): ?>    
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <select name="enabledLanguages[]" multiple='multiple' size='2'>
                        <option label="deutsch" value="de" <?php echo (in_array('de', $value) ? 'selected="selected"' : ''); ?> >deutsch</option>
                        <option label="english" value="gb" <?php echo (in_array('gb', $value) ? 'selected="selected"' : ''); ?>>english</option>
                    </select>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php elseif ($key == 'siteStyle'): ?>   
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <select name="siteStyle" >
                        <?php foreach ($availableSiteStyles as $val) : ?>

                            <option label="<?php echo $val; ?>" value="<?php echo $val; ?>" <?php echo ($val == $value ? 'selected="selected"' : ''); ?>><?php echo $val ?></option>

                        <?php endforeach; ?>
                    </select>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php elseif ($type == 'array'): ?>   
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <select name="<?php echo $key ?>[]" multiple='multiple' size='2'>
                        <?php foreach ($value as $val) : ?>

                            <option label="deutsch" value="<?php echo $val ?>"  selected="selected" ><?php echo $val ?></option>

                        <?php endforeach; ?>
                    </select>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php else: ?>
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <input class="pure-input-1-2" id="<?php echo $key ?>" name="<?php echo $key ?>" value="<?php echo $value ?>" type="text" />
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <div class="pure-controls">
            <input class="pure-button pure-button-primary" type="submit" name="saveConfiguration" value="<?php echo $this->ts('save configuration'); ?>" />
        </div>
    </fieldset>
</form>
