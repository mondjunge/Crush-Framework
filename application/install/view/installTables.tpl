<h1><?php echo $this->ts('Create Tables') ?></h1>
<div class="install">
    <form action="<?php echo $this->action('install') ?>" method="post" enctype="multipart/form-data"> 
        <?php foreach ($dbFiles as $sql) : ?>
        <p>
            <input type="checkbox" name="tables[]" checked="checked" value="<?php echo $sql ?>"/>
            <label for="<?php echo $sql ?>"><?php echo $sql ?></label>
        </p>
        <?php endforeach; ?>
            
            <input type="submit" name="createtables" value="<?php echo $this->ts('Create selected table(s)');?>" />
    </form>
</div>