<h1><?php echo$this->ts('Create Database')?></h1>
<div>
    <strong><?php echo $this->ts("The Database '").$dbName.$this->ts("' does not exist.") ?></strong>
    <br/>
    <?php echo $this->ts("Should it be created now?") ?>
    <br/>
    <?php echo $linkHelper->linkFunction($this->ts("Create database now!"), "createDB") ?>
</div>