<h1><?php echo $this->ts('Main Installation') ?></h1>

<?php echo $wizard; ?>
<form class="pure-form pure-form-aligned" action="<?php echo $this->action('install'); ?>" method="POST">
    <?php if ($step == 2): ?>
        <h2><?php echo $this->ts('Create Database') ?></h2>
        <div class="pure-g pure-g-r">
            <!-- DB check -->
            <input type="hidden" name="createDb" value="1" />
            <div class="pure-u-1-2">
                <p class="boldFont"><?php echo $this->ts("The Database '$1' does not exist.", array(1 => $dbName)); ?></p>
            </div>
            <div class="pure-u-1-2">
                <p><?php echo $this->ts("The Database '$1' will be created for you.", array(1 => $dbName)); ?></p>
            </div>
            <div class="pure-u-1">
                <input class="pure-button pure-button-primary" name="createdb" type="submit" value="<?php echo $this->ts("create db"); ?>"/>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($step == 3): ?>
        <h2><?php echo $this->ts('Install System Modules') ?></h2>
        
        <div class="pure-g pure-g-r">
            <!-- Sytsem tables Check -->
            <input type="hidden" name="installSystem" value="1" />
            <div class="pure-u-1-2">
                <p class="boldFont"><?php echo $this->ts("No system tables found."); ?></p>
            </div>
            <div class="pure-u-1-2">
                <p><?php echo $this->ts("Basic modules ($1) will be installed for your convinience.", array(1=>$modulesToInstall)); ?></p>
            </div>
            <div class="pure-u-1">
                <input class="pure-button pure-button-primary" name="installModules" type="submit" value="<?php echo $this->ts("install modules"); ?>"/>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($step == 4): ?>
        <h2><?php echo $this->ts('Create Superadmin User') ?></h2>

        <div class="pure-g pure-g-r">
            <div class="pure-u-1-2">
                <p class="boldFont"><?php echo $this->ts("Please provide initial user credentials. This user will be created and given Superadmin powers upon installation."); ?></p>
            </div>
            <div class="pure-u-1-2">

                <input class="pure-input pure-input-1-2" name="user" type="text" placeholder="<?php echo $this->ts("username"); ?>" autocomplete="off"/>
                <input class="pure-input pure-input-1-2" name="pass" type="password" placeholder="<?php echo $this->ts("password"); ?>" autocomplete="off"/>

            </div>
            <div class="pure-u-1">
                <input class="pure-button pure-button-primary" name="insertSuperadmin" type="submit" value="<?php echo $this->ts("insert Superadmin"); ?>"/>
            </div>
        </div>

    <?php endif; ?>
</form>
<?php if ($step == 5): ?>
<h2><?php echo $this->ts('Installation is finished') ?></h2>
<p class="boldFont">
    <?php echo $this->ts("There is nothing else to do. You can login with your Superadmin now."); ?>
</p>
<a class="pure-button pure-button-primary" href="<?php echo $this->action("login"); ?>"><?php echo $this->ts("Go to login."); ?></a>
<?php else: ?>
<a class="pure-button pure-button-error" href="<?php echo $this->action("install?reset"); ?>"><?php echo $this->ts("Delete configuration and start over"); ?></a>
<?php endif; ?>