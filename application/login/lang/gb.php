<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Login' =>
	'Login'
	,
	'Nick'=>
	'Nick'
	,
	'Passphrase'=>
	'Passphrase'
	,
	'stay logged in'=>
	'Stay logged in'
	,
	'login'=>
	'Login'
	,
	'Username'=>
	'Username'
	,
	'Passphrase'=>
	'Passphrase'
	,
	'Wrong Nick or Password'=>
	'Wrong Nick or Password'
	,
	'Logged in successfully!'=>
	'Logged in successfully!'
	,
	'Wrong Username or Password'=>
	'Wrong Username or Password'
	,
	'You are logged in!'=>
	'You are logged in!'
	,
	'logged in as:'=>
	'logged in as:'
	,
	'Logged out successfully!'=>
	'Logged out successfully!'
	,
	'Restricted area menu'=>
	'Restricted area menu'
	,
	'logout'=>
	'Logout'
	,
	'Register'=>
	'Register now!'
	,
	'E-Mail'=>
	'E-Mail'
	,
	'Repeat Passphrase'=>
	'Repeat Passphrase'
	,
	'I understood the terms.'=>
	'I understood the terms and will use this service respectfully.'
	,
	'Nickname'=>
	'Nickname'
	,
	'E-Mail address'=>
	'E-Mail address'
	,
	'I understood the terms an will use this service respectfully.'=>
	'I understood the terms and will use this service respectfully.'
	,
	'E-Mail Address'=>
	'E-Mail Address'
	,
	'Please change your password immediatly or anyone can corrupt your system!'=>
	'Please change your password immediatly or anyone can corrupt your system!'
	,
	'Password forgotten?'=>
	'Lost your Password? Set a new one!'
	,
	'Retrieve Password'=>
	'Retreive Password'
	,
	'Enter your Username or E-Mail address to get an E-Mail with further instructions.'=>
	'Enter your Username or E-Mail address to get an E-Mail with further instructions.'
	,
	'Username or E-Mail'=>
	'Username or E-Mail'
	,
	'send'=>
	'Send'
	,
	'E-Mail was send. Please take a look in your inbox. Maybe check the SPAM folder, too.'=>
	'E-Mail was send. Please take a look in your inbox. Maybe check the SPAM folder, too.'
	,
	'User or E-Mail cannot be found.'=>
	'User or E-Mail cannot be found.'
	,
	'This Link is not correct.'=>
	'This Link is not correct.'
	,
	'Account'=>
	'Account'
	,
	'Username changes will log you out automatically.'=>
	'Username changes will log you out automatically.'
	,
	'Change Password'=>
	'Change Password'
	,
	'Old passphrase'=>
	'Old passphrase'
	,
	'New passphrase'=>
	'New passphrase'
	,
	'Repeat new passphrase'=>
	'Repeat new passphrase'
	,
	'Save changes'=>
	'Save changes'
	,
	'Delete account'=>
	'Delete account'
	,
	'This action is not allowed.'=>
	'This action is not allowed.'
	,
	'Password updated successfully!'=>
	'Password updated successfully!'
	,
	'Password retrieval on '=>
	'Password retrieval on '
	,
	'Hello $1,'=>
	'Hello $1,'
	,
	'you got this E-Mail because someone used your address with the password retrieve function.'=>
	'you got this E-Mail because someone used your address with the password retreive function.'
	,
	'To set a new password, copy following Link completly to your browser and follow instructions.'=>
	'To set a new password, copy following Link completly to your browser and follow instructions.'
	,
	'If you have not expected this mail, just delete it!'=>
	'If you have not expected this mail, just delete it!'
	,
	'If you have any questions regarding this e-mail, contact $2 for more information.'=>
	'If you have any questions regarding this e-mail, contact $2 for more information.'
	,
	'key not set!'=>
	'key not set!'
	,
	'User registered successfully!'=>
	'User registered successfully!'
	,
	'This E-Mail address is already registered!'=>
	'This E-Mail address is already registered!'
	,
	'Nick have to be at least 3 characters long!'=>
	'Nick have to be at least 3 characters long!'
	,
	'Register now'=>
	'No Account? Register now!'
	,
	'Password does not match with the repetition.'=>
	'Password does not match with the repetition.'
	,
	'Not an valid E-Mail address!'=>
	'Not an valid E-Mail address!'
	,
	'Terms.'=>
	'Terms.'
);