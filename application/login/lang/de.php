<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Login' =>
	'Anmelden'
	,
	'login'=>
	'Anmelden'
	,
	'stay logged in'=>
	'Auf diesem Rechner angemeldet bleiben'
	,
	'Username'=>
	'Benutzername'
	,
	'Passphrase'=>
	'Passwort'
	,
	'Wrong Nick or Password'=>
	'Falscher Benutzer oder Passwort'
	,
	'Logged in successfully!'=>
	'Erfolgreich angemeldet!'
	,
	'You are logged in!'=>
	'Du bist bereits angemeldet!'
	,
	'logged in as:'=>
	'Angemeldet als:'
	,
	'Logged out successfully!'=>
	'Erfolgreich abgemeldet!'
	,
	'Wrong Username or Password'=>
	'Falscher Benutzer oder Passwort'
	,
	'Restricted area menu'=>
	'Administrator Menü'
	,
	'logout'=>
	'Abmelden'
	,
	'Register'=>
	'Registrieren'
	,
	'Repeat Passphrase'=>
	'Passwort wiederholen'
	,
	'Nickname'=>
	'Nickname'
	,
	'E-Mail address'=>
	'E-Mail Adresse'
	,
	'I understood the terms an will use this service respectfully.'=>
	'Ich habe die AGB verstanden.'
	,
	'E-Mail Address'=>
	'E-Mail Adresse'
	,
	'E-Mail'=>
	'E-Mail'
	,
	'Please change your password immediatly or anyone can corrupt your system!'=>
	''
	,
	'<strong>Warning:</strong>This login is not secure! Use <a href=\'https://9tw.ssl.goneo.de\'>https://9tw.ssl.goneo.de</a> instead.'=>
	'<strong>Achtung:</strong>Dieser Login ist nicht sicher! Wechsel auf <a href=\'https://9tw.ssl.goneo.de\'>https://9tw.ssl.goneo.de</a> um auf der sicheren Seite zu sein.'
	,
	'Password forgotten?'=>
	'Passwort vergessen?'
	,
	'Retrieve Password'=>
	'Neues Passwort anfordern'
	,
	'Enter your Username or E-Mail address to get an E-Mail with further instructions.'=>
	'Bitte gib Deinen Benutzernamen oder Deine E-Mail Adresse an um eine E-Mail mit weiteren Anweisungen zu erhalten.'
	,
	'Username or E-Mail'=>
	'Benutzername oder E-Mail Adresse'
	,
	'send'=>
	'absenden'
	,
	'E-Mail was send. Please take a look in your inbox. Maybe check the SPAM folder, too.'=>
	'Es wurde eine E-Mail mit weiteren Anweisungen versendet. Schaue auch im SPAM Ordner nach, falls Du die E-mail nicht finden solltest.'
	,
	'User or E-Mail cannot be found.'=>
	'Dieser Nutzer oder E-mail Adresse wurde nicht gefunden.'
	,
	'This Link is not correct.'=>
	'Dieser Link ist nicht korrekt. Überprüfe ob Du ihn richtig kopiert hast.'
	,
	'Account'=>
	'Konto'
	,
	'Change Password'=>
	'Passwort ändern'
	,
	'New passphrase'=>
	'Neues Passwort'
	,
	'Repeat new passphrase'=>
	'Neues Passwort wiederholen'
	,
	'Save changes'=>
	'setze neues Passwort'
	,
	'This action is not allowed.'=>
	'Diese Aktion ist nicht erlaubt.'
	,
	'Password updated successfully!'=>
	'Passwort wurde erfolgreich aktualisiert!'
	,
	'Password retrieval on '=>
	'Passwort Wiederherstellung auf '
	,
	'Hello $1,'=>
	'Hallo $1'
	,
	'you got this E-Mail because someone used your address with the password retrieve function.'=>
	'Du bekommst diese E-Mail, weil Deine E-Mail Adresse/Dein Nutzername in der Passwort-Vergessen Funktion eingegeben wurde.'
	,
	'To set a new password, copy following Link completly to your browser and follow instructions.'=>
	'Um ein neues Passwort zu vergeben kopiere bitte folgenden Link vollständig in die Adressleiste Deines Browsers und folge den Anweisungen.'
	,
	'If you have not expected this mail, just delete it!'=>
	'Wenn Du diese Mail nicht erwartet hast, lösche sie einfach!'
	,
	'If you have any questions regarding this e-mail, contact $2 for more information.'=>
	'Falls Du irgendwelche Fragen bezüglich dieser E-mail hast, kontaktiere $2.'
	,
	'key not set!'=>
	''
	,
	'User registered successfully!'=>
	'Erfolgreich registriert!'
	,
	'This E-Mail address is already registered!'=>
	'Diese E-Mail Adresse ist bereits registriert.'
	,
	'Register now'=>
	'Neu registrieren'
	,
	'Password does not match with the repetition.'=>
	'Die Passwortwiederholung stimmt nicht mit dem Original überein.'
	,
	'Not an valid E-Mail address!'=>
	'Bitte gebe eine gültige E-Mail Adresse ein.'
	,
	'Terms.'=>
	'Allgemeine Geschäftsbedingungen'
        ,
        'I understood the terms.'=>
	'Ich habe die allgemeinen Geschäftsbedingungen gelesen und verstanden.'
);