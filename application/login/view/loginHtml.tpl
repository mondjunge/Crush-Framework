<div class="loginButton">
    <div class="panel_button" id="show_button" style="display: visible;">

        <a href="#"><?php echo $this->image('lock-icon.png', '24', '24') ?></a>

    </div>
    <div class="panel_button" id="hide_button" style="display: none;">
        

        <a href="#"><?php echo $this->image('lock-icon.png', '24', '24') ?></a>
    </div>
</div>
<div id="loginForm" class="loginForm">
   
    <form action="/<?php echo $conf['reLangUrl'] . 'login' ?>" method="post">
        <div class="form">
             <h2><?php echo $this->ts("Login") ?></h2>
            <p>
                <label for="nick" class="divlabel"><?php echo $this->ts("Username") ?></label>
                <input id="nick" name="nick" type="text" value=""/>
            </p>
            <p>
                <label for="pass" class="divlabel"><?php echo $this->ts("Passphrase") ?></label>
                <input id="pass" name="pass" type="password" maxlength="64" /><br/>
            </p>
            <p>
                <label for="check" class="divlabel"><?php echo $this->ts("stay logged in") ?></label>
                <input id="check" name="resident" type="checkbox" /><br/><br/>
            </p>
            <p>
                <button type="submit" name="sendLoginRequest" ><?php echo $this->ts("login") ?></button> 
            </p>
        </div>
    </form>
</div>