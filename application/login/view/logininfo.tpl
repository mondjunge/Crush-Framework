<div id="loginFor" class="loginFor">
    <div class="border">
        <div class="form">
            <b><?php echo $this->ts("logged in as:") ?></b> <br/>
            <?php echo $nick ?>
            <p>
                <a href="<?php echo $this->action("login?logout") ?>"><?php echo $this->ts("logout") ?></a>
            </p>
            <div class="loginNav">
                <?php echo $adminNavi ?>
            </div>
        </div>
    </div>
</div>