
<form class="pure-form pure-form-aligned"  action="<?php echo $this->action("login"); ?>" method="post">
    <fieldset>
        <legend><?php echo $this->ts("Login") ?></legend>
        <?php if (isset($redirect)) : ?>
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        <?php endif; ?>

        <div class="pure-control-group">
            <label for="mn" ><?php echo $this->ts("Username or E-Mail") ?></label>
            <input id="mn" name="nick" type="text" placeholder="email@example.org"/>
        </div>
        <div class="pure-control-group">
            <label for="aerg" ><?php echo $this->ts("Passphrase") ?></label>
            <input id="aerg" name="pass" type="password" maxlength="64" placeholder="********"/><br/>
        </div>
        <div class="pure-controls">
            <label for="res" class="pure-checker" tabindex="0" title="<?php echo $this->ts("stay logged in") ?>">
                <div class="checker">
                    <input id="res" class="checkbox" name="resident" type="checkbox" />
                    <div class="check-bg"></div>
                    <div class="checkmark">
                        <svg viewBox="0 0 100 100">
                        <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                </div>
                <span class="label"><?php echo $this->ts("stay logged in") ?></span>
            </label>
        </div>
        <div class="pure-controls">
            <button class="pure-button pure-button-primary" type="submit" name="sendLoginRequest" ><?php echo $this->ts("login") ?></button>
        </div>
        <div class="pure-controls">
            <a class="pure-button" href="<?php echo $this->action('login/retreivePassword'); ?>" ><?php echo $this->ts("Password forgotten?") ?></a>
        </div>
        <?php if ($registrationOpen) : ?>
            <div class="pure-controls">
                <a id="" class="pure-button" href="<?php echo $this->action("login?register"); ?>"><?php echo $this->ts("Register now") ?></a>
            </div>
        <?php endif; ?>
    </fieldset>
</form>
