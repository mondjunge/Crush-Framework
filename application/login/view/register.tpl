<form class="pure-form pure-form-aligned" action="<?php echo $this->action("login?register"); ?>" method="post">
    <fieldset>
        <legend><?php echo $this->ts("Register") ?></legend>
        <div class="pure-control-group">
            <label for="email" ><?php echo $this->ts("E-Mail address") ?></label>
            <input id="emgaergail" name="email" type="text" value="<?php echo $transientNewAccount['email']; ?>" placeholder="emil@example.org" required="true"/>
        </div>

        <div class="pure-control-group">
            <label for="nick" ><?php echo $this->ts("Username") ?></label>
            <input id="nick" name="nick" type="text" value="<?php echo $transientNewAccount['nick']; ?>" placeholder="emil" required="true"/>
        </div><!-- comment -->

        <div class="pure-control-group">
            <label for="pass" ><?php echo $this->ts("Passphrase") ?></label>
            <input id="paaegrss" name="pass" type="password" maxlength="64" placeholder="********" required="true"/>
        </div>
        <div class="pure-control-group">
            <label for="passrepeat" ><?php echo $this->ts("Repeat Passphrase") ?></label>
            <input id="pasgraesrepeat" name="passrepeat" type="password" maxlength="64" placeholder="********" required="true"/>
        </div>
        <div class="pure-controls">
            <div class="pure-control-group">
                <a id="termsButton" href="<?php echo $this->action('site/terms'); ?>"><?php echo $this->ts("Terms.") ?></a>
            </div>
        </div>
        <div class="pure-controls">
            <label for="res" class="pure-checker" tabindex="0" title="<?php echo $this->ts("I understood the terms.") ?>">
                <div class="checker">
                    <input id="res" class="checkbox" name="agb" type="checkbox" />
                    <div class="check-bg"></div>
                    <div class="checkmark">
                        <svg viewBox="0 0 100 100">
                        <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFFFFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                </div>
                <span class="label"><?php echo $this->ts("I understood the terms.") ?></span>
            </label>
        </div>
        <div class="pure-controls">
            <div class="pure-control-group">

                <button class="pure-button pure-button-primary" type="submit" name="sendRegisterRequest" ><?php echo $this->ts("Register") ?></button> 
            </div>

        </div>
    </fieldset>
</form>
