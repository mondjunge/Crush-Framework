<form class="pure-form pure-form-aligned" action="<?php echo $this->action('login/retreivePassword') ?>" method="post" enctype="multipart/form-data">
    <!--    <div class="form">-->
    <fieldset>
        <legend><?php echo $this->ts('Account') . " " . $user['nick'] ?></legend>
        <input id="id" name="id" type="hidden" value="<?php echo $user['id'] ?>" />
        <input id="security_hash" name="security_hash" type="hidden" value="<?php echo $security_hash ?>" />
        

        <legend><?php echo $this->ts('Change Password') ?></legend>

        <div class="pure-control-group">
            <label for="new_password" ><?php echo $this->ts("New passphrase") ?></label>
            <input id="new_password" name="new_password" type="password" size="26" maxlength="64" autocomplete="OFF"/>
        </div>

        <div class="pure-control-group">
            <label for="new_passrepeat" ><?php echo $this->ts("Repeat new passphrase") ?></label>
            <input id="new_passrepeat" name="new_passrepeat" type="password" size="26" maxlength="64" autocomplete="OFF"/>
        </div>

        <div class="pure-control-group">
            <button type="submit" class="pure-button pure-button-primary" name="updatePassword" ><?php echo $this->ts("Save changes") ?></button> 
        </div>
    </fieldset>
</form>