<form class="pure-form pure-form-aligned"  action="" method="post">
    <fieldset>
        <legend><?php echo $this->ts("Retrieve Password") ?></legend>
        <div class="help"><?php echo $this->ts("Enter your Username or E-Mail address to get an E-Mail with further instructions.") ?></div>
        <div class="pure-control-group">
            <label for="nick" ><?php echo $this->ts("Username or E-Mail") ?></label>
            <input id="mn" name="nick" type="text" value=""/>
        </div>
        
        <div class="pure-controls">
            <button class="pure-button pure-button-primary" type="submit" name="retrievePassword" ><?php echo $this->ts("send") ?></button> 
        </div>
    </fieldset>
</form>