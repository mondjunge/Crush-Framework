<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Login extends AppController {
    
    public function index() {
        //$this->setTitle($this->ts("Login"));

        

        if (filter_has_var(INPUT_GET, 'redirect')) {
            $data['redirect'] = urlencode(filter_input(INPUT_GET, 'redirect'));
        }

        if (filter_has_var(INPUT_POST, 'sendRegisterRequest')) {
            $this->registerUser();
            
            // if an validation error occured, 
            $data['transientNewAccount'] = array(
                'nick' => filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW),
                'email' => filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL)
            );
        } else{
            // if an validation error occured, 
            $data['transientNewAccount'] = array(
                'nick' => '',
                'email' => ''
            );
        }
        
        if (filter_has_var(INPUT_GET, 'register')) {
            // show registration page
            $this->setTitle($this->ts("Register"));
            $this->register($data);
            return;
        }
        if (filter_has_var(INPUT_GET, 'amILoggedIn')) {
            // for js that needs to know if you are logged in..
            $this->standalone = true;
            echo "" . $this->isLoggedIn();
            return;
        }

        if (filter_has_var(INPUT_POST, 'sendLoginRequest')) {
            if ($this->loginNow()) {
                
                $this->setSystemMessage($this->ts("Logged in successfully!"));
                if (filter_has_var(INPUT_POST, 'redirect')) {
                    Link::jumpTo(urldecode(filter_input(INPUT_POST, 'redirect')));
                }
                if (Config::get('defaultControllerAfterLogin') != '') {
                    Link::jumpTo(Config::get('defaultControllerAfterLogin'));
                }
            } else {
                $this->setErrorMessage($this->ts("Wrong Username or Password"));
                //$this->jumpToLast();
            }
        }
        if (filter_has_var(INPUT_GET, 'logout')) {
            $this->logout();
            $this->setSystemMessage($this->ts("Logged out successfully!"));
            Link::jumpTo('login');
        }
        if ($this->isLoggedIn()) {
//$this->setSystemMessage($this->ts("You are logged in!"));
            
            $data['nick'] = Session::get('unick');
            $data['adminNavi'] = $this->getAdminNavigation();
            $this->setTemplatePath('application/' . $this->getActiveModule() . '/view/');
            $this->view('logininfo', $data);
        } else {
            $data['registrationOpen'] = Config::get('registrationOpen');
            $this->view('login', $data);
        }
    }

//    public function admin(){
//        $this->autoCheckRights();
//        $data = array();
//        $this->view('admin');
//    }

    private function register($data) {
        if (Config::get('registrationOpen')) {
            $this->view('register',$data);
        } else {
            echo $this->ts("Registration closed.");
        }
    }

    public function retreivePassword() {
        $data = array();
        if (filter_has_var(INPUT_POST, 'updatePassword')) {
            $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $security_hash = filter_input(INPUT_POST, 'security_hash');
            $rs = $this->selectPrepare("user", "*", "password=:security_hash AND id=:id AND active=1 ORDER BY id asc LIMIT 1 ", array('security_hash' => $security_hash, 'id' => $id) );
            if ($rs == FALSE) {
                $this->setErrorMessage($this->ts("This action is not allowed."));
                //return FALSE;
            } else {
                if (filter_input(INPUT_POST, 'new_password') != '' && filter_input(INPUT_POST, 'new_passrepeat') != '' && filter_input(INPUT_POST, 'new_password') == filter_input(INPUT_POST, 'new_passrepeat')) {

                    $randomSalt = $this->generateRandomString();
                    $newhash = $randomSalt . hash('sha512', $randomSalt . filter_input(INPUT_POST, 'new_password'));

                    $fieldsValueArray['password'] = $newhash;
                    if ($this->updatePrepare('user', $fieldsValueArray, "id=:id", array('id' => $id))) {
                        $this->setSystemMessage($this->ts('Password updated successfully!'));
                        $this->jumpToLogin();
                    } else {
                        $this->setErrorMessage($this->ts('Something went wrong while updating the password'));
                    }
                }
            }
        }

        if (filter_has_var(INPUT_GET, 'sh')) {
            $rs = $this->selectPrepare("user", "*", "password=:sh AND active=1 ORDER BY id asc LIMIT 1", array('sh' => filter_input(INPUT_GET, 'sh')));
            if ($rs == FALSE) {
                $this->setErrorMessage($this->ts("This Link is not correct."));
                //return FALSE;
            } else {
                $data['security_hash'] = filter_input(INPUT_GET, 'sh');
                $data['user'] = $rs[0];
                $this->view('resetPassword', $data);
                return;
            }
        }

        if (filter_has_var(INPUT_POST, 'retrievePassword') && filter_input(INPUT_POST, 'nick') != "") {
            $nick = filter_input(INPUT_POST, 'nick');
            $rs = $this->selectPrepare("user", "*", "(nick=:nick OR email=:nick) AND active=1 ORDER BY id asc LIMIT 1", array('nick' => $nick));
            if ($rs == FALSE) {
                $this->setErrorMessage($this->ts("User or E-Mail cannot be found."));
                //return FALSE;
            } else {
                $email = $rs[0]['email'];
                $nick = $rs[0]['nick'];
                $passHash = $rs[0]['password'];

                $this->sendPasswordResetMail($email, $nick, $passHash);
            }
        }

        $this->view('retrievePassword', $data);
    }

    private function sendPasswordResetMail($to, $nick, $security_hash) {

        $umlautUrl = str_replace("xn--", "ü", Config::get('rootUrl'));
        $umlautUrl = str_replace("-jva", "", $umlautUrl);

        $resetLink = Config::get('rootUrl') . Config::get('reLangUrl') . "login/retreivePassword?sh=" . $security_hash;
        $subject = $this->ts("Password retrieval on ") . $umlautUrl;
        
        $message = $this->ts("Hello $1,");
        $message .= "<br/><br/>";
        $message .= $this->ts("you got this E-Mail because someone used your address with the password retrieve function.");
        $message .= "<br/><br/>";
        $message .= $this->ts("To set a new password, copy following Link completly to your browser and follow instructions.");
        $message .= "<br/><br/>";
        $message .= "<a href='" . $resetLink . "'>" . $resetLink . "</a>";
        $message .= "<br/><br/>";
        $message .= $this->ts("If you have not expected this mail, just delete it!");
        $message .= "<br/>";
        $message .= $this->ts("If you have any questions regarding this e-mail, contact $2 for more information.");

        $messageFinal = str_replace('$2', Config::get('noticeEmail'), str_replace('$1', $nick, $message));

        if (!$this->mail_utf8($to, $subject, $messageFinal)) {
            // failed
            $this->setErrorMessage($this->ts("E-Mail could not be send. Sorry."));
        } else {
            $this->setSystemMessage($this->ts("E-Mail was send. Please take a look in your inbox. Maybe check the SPAM folder, too."));
        }
    }

    /**
     * TODO: duplicated code (User.php)
     * - unsecure code revise
     */
    private function registerUser() {
        
        if (!Config::get('registrationOpen')){
            Link::jumpToLast();
            return;
        }
        $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
        $nick = filter_input(INPUT_POST, 'nick');
        
        $error = false;
        if (!Input::check($email, 'email') || strlen($email) < 6) {
            $this->setErrorMessage($this->ts("Not an valid E-Mail address!"));
            $error = true;
        }
        
        

//        if (filter_input(INPUT_POST, 'nick') == '' || strlen(filter_input(INPUT_POST, 'nick')) < 3) {
//            $this->setErrorMessage($this->ts("Nick have to be at least 3 characters long!"));
//            $error = true;
//        }


        $user = $this->selectPrepare('user', 'id', "email=:email", array('email' => $email));
        if (sizeof($user) > 0) {
            $this->setErrorMessage($this->ts("This E-Mail address is already registered!"));
            $error = true;
        }

        if (filter_input(INPUT_POST, 'pass') == '' || strlen(filter_input(INPUT_POST, 'pass')) < 5) {
            $this->setErrorMessage($this->ts("Password have to be at least 5 characters long!"));
            $error = true;
        }

        if (filter_input(INPUT_POST, 'pass') != filter_input(INPUT_POST, 'passrepeat')) {
            $this->setErrorMessage($this->ts("Password does not match with the repetition."));
            $error = true;
        }

        if (!$error) {
            $newId = $this->insertUserData($nick, filter_input(INPUT_POST, 'pass'), $email, '1', '2');
            //echo "newId:".$newId; die;
            if (!$newId) {
                //echo "no new id:".$newId; die;
                $this->setErrorMessage($this->ts("Cannot register user!"));
                Link::jumpTo("login?register");
            } else {
                $this->setSystemMessage($this->ts("User registered successfully!"));
                if ($this->loginNow()) {
                    $this->setSystemMessage($this->ts("Logged in successfully!"));
                     Link::jumpTo("");
                } else {
                    //echo "error:".$error; die;
                    $this->setErrorMessage($this->ts("Wrong Username or Password"));
                }
            }
        } 
        
       
    }

    /**
     * TODO: duplicated code (User.php)
     * @param type $nick
     * @param type $pass
     * @param type $email
     * @param type $active
     * @param type $roles
     * @return type
     */
    private function insertUserData($nick, $pass, $email, $active, $roles = '0') {

        $randomSalt = $this->generateRandomString();
        $fieldsValueArray = array(
            'nick' => $nick,
            'password' => $randomSalt . hash('sha512', $randomSalt . $pass),
            'email' => $email,
            'active' => $active,
            'roles' => $roles
        );
        $newUserId = $this->insert('user', $fieldsValueArray);

        if ($newUserId === FALSE) {
            return $newUserId;
        } else {
            if ($this->tableExists('notifications_settings')) {
                $rs = $this->select('notifications_settings', '*', "user_id='$newUserId'");

                if ($rs === FALSE || sizeof($rs) == 0) {
                    $fieldsValueArray = array();
                    $fieldsValueArray['user_id'] = $newUserId;
                    $this->insert('notifications_settings', $fieldsValueArray);
                }
            }
            // create notification settings

            return $newUserId;
        }
    }

    private function getAdminNavigation() {
        $rs = $this->select('site', 'id, module, role_need', "active=1 AND hide=0 AND (type='a')");
        if ($rs == FALSE) {
            return "";
        }
        foreach ($rs as $site) {
            if ($this->checkUserRights($site['role_need'])) {
                $rs2 = $this->select('site_data', '`name`, title', "site_id=" . $site['id'] . " AND language='" . $this->getLanguage() . "'");
                if ($rs2 == FALSE) {
                    // fallback if selected language is not fully translated
                    $rs2 = $this->select('site_data', '`name`, title', "site_id='{$site['id']}' AND language='" . Config::get('defaultLanguage') . "'");
                }
                $sites[] = array_merge($site, $rs2[0]);
            }
        }
        if (!isset($sites) || $sites === null) {
            return "";
        }
        $menu = Menu::getInstance();
        $menuContent = '';

        foreach ($sites as $item) {
            $menu->addItem(new MenuItem($item['name'], $item['title'], $item['module'], ($item['module'] == $this->getActiveModule() || $item['module'] == $this->getActiveModule() . '/' . $this->getActiveFunction()), Config::get('showAppIcons')));
        }
        $menuContent .= $menu->renderMenuItems('admin');
        return $menuContent;
    }

}
