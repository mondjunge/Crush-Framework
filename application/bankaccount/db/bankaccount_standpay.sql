CREATE TABLE IF NOT EXISTS `{dbprefix}bankaccount_standpay` (
    `id` bigint(11) NOT NULL AUTO_INCREMENT,
    `account_user_id` bigint(11) NOT NULL,
    `created_by_user_id` bigint(11) NOT NULL,
    `deposit` float NOT NULL,
    `purpose` text NOT NULL,
    `payment_pattern` text NOT NULL,
    `first_deposit` DATETIME NOT NULL,
    `next_deposit` DATETIME NULL,
    `last_deposit` DATETIME NULL,
    `date_created` DATETIME DEFAULT CURRENT_TIMESTAMP,    
  PRIMARY KEY (`id`)
);