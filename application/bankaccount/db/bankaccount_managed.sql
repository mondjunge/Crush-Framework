CREATE TABLE IF NOT EXISTS `{dbprefix}bankaccount_managed` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `account_name` text NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `managed_by_user_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
);