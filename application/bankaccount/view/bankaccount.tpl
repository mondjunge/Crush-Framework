<?php if($isAdmin):?><a href="<?php echo $this->action('bankaccount/admin'); ?>" class="pure-button"><?php echo $this->ts('back to overview'); ?></a><?php endif;?>
<h2><?php echo $this->ts('Bankaccount'); echo ' '.$userData['account_name'].'('.$userData['nick'].')';  ?></h2>
<?php if($userData['image'] != ''): ?>
<div class="accountPic" style="max-width:400px;max-height:400px; margin-left: auto; margin-right: auto;">    
    <?php echo $this->image(Config::get('relativeUrl').$userData['image'], 'responsiveImage', '500px', '500px') ?>
</div>
<?php endif; ?>
<h3 style="<?php if ( $userBalance < 0 ) { echo 'color:darkred;'; } else { echo 'color:darkgreen;'; } ?>"><?php echo $this->ts('Balance') .':'; ?>&nbsp;<?php echo number_format( $userBalance, 2,',','.') . $this->ts('€'); ?></h3>
<?php if($isAdmin):?>
<div class="overlay box boxShadow">
    <h3 class="overlayTitle"><?php echo $this->ts('Deposit/Withdraw'); ?></h3>
    <p><span class="help"><?php echo $this->ts('deposit_help'); ?></span></p><br/>
    <form class="pure-form pure-form-aligned" name="deposit" action="<?php echo $this->action('bankaccount/admin'); ?>" method="post">
        <fieldset>
            <div class="pure-control-group">
                <input type="hidden" name="userId" value="<?php echo $userData['id']; ?>" />
                <label for="purpose" ><?php echo $this->ts('Posting text'); ?></label>
                <textarea id="purpose" type="text" size="50" name="purpose" placeholder="<?php echo $this->ts('Posting text'); ?>" required="required"></textarea>
            </div>
            <div class="pure-control-group">
                <label for="amount" ><?php echo $this->ts('Amount'); ?></label>
                <input type="number" min="-1000000" max="1000000" step=".01" name="amount" required="required" placeholder="<?php echo $this->ts('Amount'); ?>" /><?php echo $this->ts('€');?>
                
            </div>
            <div class="pure-controls">
                <div class="pure-control-group">
                    <button type="submit" name="updatebalance" class="pure-button pure-button-primary"><?php echo $this->ts('Post'); ?></button>
                    <button type="cancel" name="cancelbalance" class="pure-button closeOverlay"><?php echo $this->ts('Cancel'); ?></button>
                </div>
            </div>
        </fieldset>
    </form>
</div>
<div class="overlay box boxShadow">
    <h3 class="overlayTitle"><?php echo $this->ts('Standing order'); ?></h3>
    <p><span class="help"><?php echo $this->ts('standingOrder_help'); ?></span></p><br/>
    <form class="pure-form pure-form-aligned" name="standingorder" action="<?php echo $this->action('bankaccount/admin'); ?>" method="post">
        <fieldset>
            <div class="pure-control-group">
                <input type="hidden" name="account_user_id" value="<?php echo $userData['id']; ?>" />
                <label for="purpose" ><?php echo $this->ts('Posting text'); ?></label>
                <textarea type="text" size="50" name="purpose" placeholder="<?php echo $this->ts('Posting text'); ?>" required="required"></textarea>
            </div>
            <div class="pure-control-group">
                <label for="amount" ><?php echo $this->ts('Amount'); ?></label>
                <input type="number" min="-1000000" max="1000000" step=".01" name="amount" required="required" placeholder="<?php echo $this->ts('Amount'); ?>" /><?php echo $this->ts('€');?>


            </div>
            <div class="pure-control-group">
                <label for="first_deposit"><?php echo $this->ts('First deposit'); ?></label>
                <input id="first_deposit" name="first_deposit" class="pure-input datepicker" type="date" value="<?php echo date("Y-m-d"); ?>" />
            </div>

            <div class="pure-control-group">
                <label for="last_deposit"><?php echo $this->ts('Last deposit'); ?></label>
                <input id="last_deposit" name="last_deposit" class="pure-input datepicker" type="date" value="<?php echo date("Y-m-d" , strtotime( "+364 days", strtotime( date("Y-m-d") ) )); ?>" />
            </div>

            <div class="pure-control-group">
                <label for="repeat"><?php echo $this->ts('Repetition'); ?></label>
                <select id="repeat" name="repeat" class="pure-input-1-5">
                    <option value="D" selected="selected"><?php echo $this->ts('daily'); ?></option>
                    <option value="M"><?php echo $this->ts('monthly'); ?></option>
                    <option value="Y"><?php echo $this->ts('yearly'); ?></option>
                </select>
            </div>

            <div class="pure-control-group">
                <label for="repeat_interval"><?php echo $this->ts('Repeat interval'); ?></label>
                <input id="repeat_interval" name="repeat_interval"  class="pure-input" type="text" size="2" value="7" /> <!-- disabled="disabled" -->
            </div>
            <div class="pure-controls">
                <div class="pure-control-group">
                    <button type="submit" name="createStandingorder" class="pure-button pure-button-primary"><?php echo $this->ts('Create'); ?></button>
                    <button type="cancel" name="cancelorder" class="pure-button closeOverlay"><?php echo $this->ts('Cancel'); ?></button>
                </div>
            </div>
            
        </fieldset>
    </form>
</div>
<?php endif; ?>
<h3 style=""><?php echo $this->ts('Deposits') ; ?></h3>
<form class="pure-form pure-form-aligned" name="depositsDate" action="" method="post">
    <div class="pure-control-group">
        <label for="since_date"><?php echo $this->ts('Since'); ?></label>
        <input id="since_date" name="since_date" class="pure-input datepicker" type="date" value="<?php echo $sinceDate; ?>" />
        <label for="to_date"><?php echo $this->ts('to'); ?></label>
        <input id="to_date" name="to_date" class="pure-input datepicker" type="date" value="<?php echo $toDate; ?>" />
        <button type="submit" name="showDeposits" class="pure-button" ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#magnifying-glass', 'svgIcon-small svgIcon-dark');?></button>
                       
    </div>
</form>
<div class="tableWrapper">
    <table class="pure-table pure-table-striped" summary="bankaccount overview" style="width:100%;">
        <thead>
            <tr>
                <th><?php echo $this->ts('Posting date') ?></th>
                <th><?php echo $this->ts('Posting text') ?></th>
                <th><?php echo $this->ts('Deposit in') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($accountData as $d) : ?>
                <tr>
                    <td><?php echo date($this->ts('Y-m-d H:i'),strtotime($d['date'])); echo $this->ts("h");?></td>
                    <td><?php echo $d['purpose']; ?></td>
                    
                    <?php if($d['deposit'] < 0 ) : ?>
                        <td class='negativeBalance' ><?php echo number_format($d['deposit'],2,',','.'); ?></td>
                    <?php else: ?>
                        <td class='positiveBalance' ><?php echo number_format($d['deposit'],2,',','.'); ?></td>
                    <?php endif; ?>
                </tr>  
            <?php endforeach; ?> 
        </tbody>
    </table>
</div>
<br/><!-- comment -->
<h3 style=""><?php echo $this->ts('Standing orders') ; ?></h3>
<div class="tableWrapper">
    <table class="pure-table pure-table-striped" summary="standing orders overview" style="width:100%;">
        <thead>
            <tr>
                <th><?php echo $this->ts('Creation date') ?></th>
                <th><?php echo $this->ts('Next deposit') ?></th>
                <th><?php echo $this->ts('Last deposit') ?></th>
                <th><?php echo $this->ts('Repeats') ?></th>
                <th><?php echo $this->ts('Posting text') ?></th>
                <th><?php echo $this->ts('Deposit in'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($standingOrders as $d) : ?>
                <tr>
                    <td><?php echo date($this->ts('Y-m-d H:i'),strtotime($d['date_created'])); echo $this->ts("h");?></td>
                    <td><?php if($d['next_deposit']!=null) { echo $this->ts(date('D',strtotime($d['next_deposit']))) . ", " . date($this->ts('Y-m-d'),strtotime($d['next_deposit'])); } else {echo "-";} //echo $this->ts("h");?></td>
                    <td><?php if($d['last_deposit']!=null) { echo $this->ts(date('D',strtotime($d['last_deposit']))) . ", " . date($this->ts('Y-m-d'),strtotime($d['last_deposit']));} else {echo "&infin;";} //echo $this->ts("h");?></td>
                    <td><?php if($d['payment_pattern'] == 'd:7') {echo $this->ts('weekly');}else{echo $d['payment_pattern'];} ?></td>
                    <td><?php echo $d['purpose']; ?></td>
                    
                    <?php if($d['deposit'] < 0 ) : ?>
                         <td class='negativeBalance' ><?php echo number_format($d['deposit'],2,',','.'); ?></td>
                    <?php else: ?>
                        <td class='positiveBalance' ><?php echo number_format($d['deposit'],2,',','.'); ?></td>
                    <?php endif; ?>
                    <td>
                        <div class="overlay box boxShadow">
                            <h3 class="overlayTitle"><?php echo $this->ts('Delete standing order'); ?></h3>
                            <p class="overlayButtonHtml" style="display:none"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon-small svgIcon-dark');?></p>
                            <p><?php echo $this->ts('Do you really want to delete this standing order?'); ?></p>
                            <form name="deleteStandingOrderForm" class="pure-form pure-form-aligned" action="<?php echo $this->action('bankaccount/admin'); ?>" method="post">
                                <input type="hidden" name="account_user_id" value="<?php echo $userData['id']; ?>" />
                                <input type="hidden" name="deleteStandingOrder" value="<?php echo $d['id']; ?>" />
                                <div class="pure-controls">
                                    <button type="submit" name="deleteStandingOrderButton" class="pure-button pure-button-error" ><?php echo $this->ts('Yes please, delete standing order!'); ?></button>
                                    <button type="cancel" name="cancelorder" class="pure-button closeOverlay"><?php echo $this->ts('Cancel'); ?></button>
                                </div>
                            </form>
                        </div>
                    </td>
                </tr>  
            <?php endforeach; ?> 
        </tbody>
    </table>
</div>

<?php if($isAdmin):?>
<br/><br/><br/>
<div class="overlay box boxShadow">
    <h3 class="overlayTitle"><?php echo $this->ts('Delete Account'); ?></h3>
    <p><?php echo $this->ts('Do you really seek to delete this exquisit account forever? (Thats a long time...)'); ?></p>
    <form class="pure-form pure-form-aligned" name="deleteaction" action="<?php echo $this->action('bankaccount/admin'); ?>" method="post">
        <input type="hidden" name="account_user_id" value="<?php echo $userData['id']; ?>" />
        <div class="pure-controls">
            
            <button type="submit" name="deleteaccount" class="pure-button pure-button-error"><?php echo $this->ts('Yes please, delete Account!'); ?></button>
            <button type="cancel" name="cancelorder" class="pure-button closeOverlay"><?php echo $this->ts('Cancel'); ?></button>
        </div>
    </form>
</div>
<?php endif; ?>