<h2><?php echo $this->ts('Manage Bankaccounts'); ?></h2>
<div class="pure-g pure-g-r">
    <?php foreach ($managedBankaccounts as $a) : ?>
         <div class="pure-u-1-3">
            <div class="accountContainer ">
                <div class="boxBorder boxShadow pure-u-1">
                    <a class="" href="<?php echo $this->action('bankaccount/admin').'/'.$a['user_id']; ?>" title="<?php echo $this->ts("Manage Bankaccount"); ?>">
                        <div class="pure-u-1" >
                            <h3 class="userNick"><?php echo $a['nick']; ?></h3>
                            <div class="accountPic">
                                <?php echo $this->image(Config::get('relativeUrl').$a['image'], 'responsiveImage ', '500px', '500px') ?>
                            </div>
                            <h4><?php echo number_format($a['balance'],2,',','.')."€"; ?> </h4>
                        </div>
                    </a>
                    <a class="pure-button pure-u-1" name="editUser" href="<?php echo $this->action("bankaccount/admin?editUser=".$a['user_id'])?>" ><?php echo $this->ts("Edit account"); ?></a>
                </div>
            </div>
        </div>
    <?php endforeach; ?> 
</div>
<br/><br/>
<a class="pure-button pure-button-primary" name="createBankaccount" href="?createBankaccount" ><?php echo $this->ts("Create new bankaccount"); ?></a>