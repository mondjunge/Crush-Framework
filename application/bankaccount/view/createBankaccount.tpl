<form id="createBankaccountForm" autocomplete="off" class="pure-form pure-form-aligned" action="<?php echo $this->action('bankaccount/admin?createBankaccount') ?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <br/>
        <legend><?php echo $this->ts('Create new account'); ?></legend>
        <input id="parentNick" type="hidden" name="parentNick" readonly="" value="<?php echo $transientNewAccount['parentNick']; ?>" autocomplete="off"/>
        <div class="pure-control-group ">
            <label for="image" ><?php echo $this->ts("User image") ?></label>
            <input id="image" class="" type="hidden" name="image" readonly="" value="<?php echo $transientNewAccount['image']; ?>" autocomplete="off"/>
            <a href="#" id="selectImageButton" class="fileSelect" data-type="image" data-mode="selectOnly" data-subdir="profil" data-callback="modules.bankaccount.selectNewUserImage">
                <?php echo $this->image(Config::get('relativeUrl').$transientNewAccount['image'], 'boxBorder boxShadow responsiveImage imagePreview pure-u-1-5', '500px', '500px') ?>
            </a>
        </div>
                
        <div class="pure-control-group">
            <label for="account_name" ><?php echo $this->ts("Accountname")." *" ?></label>
            <input id="account_name" name="account_name" type="text" size="26" value="<?php echo $transientNewAccount['account_name']; ?>" autocomplete="off" required="true"/>
            <span class="help"><?php echo $this->ts("Select a distinctive accountname"); ?></span>
        </div>
        
        <div class="pure-control-group">
            <label for="nick" ><?php echo $this->ts("Nickname")." *" ?></label>
            <input id="nick" name="nick" type="text" size="26" value="<?php echo $transientNewAccount['nick']; ?>" autocomplete="off" required="true"/>
            <span class="help"><?php echo $this->ts("Select a distinctive username for the new account"); ?></span>
        </div>
        
 <input id="password" name="password" type="hidden" value="<?php echo $transientNewAccount['password']; ?>" size="26" maxlength="64" autocomplete="off" required="true"/>
        <div class="pure-control-group">
            <label for="angebane" ></label>
            <span name="angebane" class="help"><em>* benötigte Angaben</em></span>
        </div>
        
        <div class="pure-controls">
            <button type="submit" class="pure-button pure-button-primary" name="insertBankaccount" ><?php echo $this->ts("Create Account") ?></button> 
            <a href="<?php echo $this->action('bankaccount/admin'); ?>" class="pure-button"><?php echo $this->ts('Cancel'); ?></a>
        </div>
    </fieldset>
</form>