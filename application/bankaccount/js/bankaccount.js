/**
 * bankaccount
 */

use_package('modules');
modules.bankaccount = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    this.init = function () {
        
        $("table").tablesorter({widthFixed: true, widgets: ['zebra']});
        
        $('.overlay').each(function(index,elem){
            var title = $(elem).find('.overlayTitle').text();
            var overlayButtonHtml = $(elem).find('.overlayButtonHtml').html();
            if(typeof overlayButtonHtml == 'undefined'){
                overlayButtonHtml = title;
            }
            $(elem).attr('id', "overlay_"+index);
            var button = $('<button/>', { id: "overlay_"+index+"_button", class:'pure-button pure-button-overlay', html: overlayButtonHtml, onclick: "modules.bankaccount.showOverlay('overlay_"+index+"');" });
            
            $(button).insertBefore(elem);
        });
        
        //handleRemoveImage();
        handleUsernameOnCreate();
    };
    
    var handleUsernameOnCreate = function() {
        $('#createBankaccountForm #account_name').on('keyup',function(){
            console.log("Account name changes1",$(this).val());
            $('#nick').val($(this).val()+"#"+$('#parentNick').val());
        });
    };
    
    this.showOverlay = function (id) {
//        if($("#"+id+"_bg").length==0){
//            var bg = $('<div/>', { id: id+"_bg", class:'overlay_background', onclick: "modules.bankaccount.closeOverlay('"+id+"');" });
//            $(bg).insertBefore("#"+id);
//        }
//        $("#"+id).css('display','block');
//        $('body').css('overflow','hidden');

        
        var title = $("#"+id).find('.overlayTitle').text();
        $("#"+id).find('.overlayTitle').hide();
        utils.overlay.show($("#"+id).html(), title, "", "");
    }
//    this.closeOverlay = function (id) {
//        $("#"+id).css('display','none');
//        $("#"+id+"_bg").css('display','none');
//        $('body').css('overflow','scroll');
//        //utils.overlay.show($(id).html(), $(id+"_button"), "", "");
//    }
    
//    var handleRemoveImage = function () {
//        $('.removeImage').off('click');
//        $('.removeImage').on("click",function () {
//            $('#image').val(relativeUrl+'theme/'+siteStyle+'/images/profil.png');
//            $('img.imagePreview').attr('src', relativeUrl+'theme/'+siteStyle+'/images/profil.png');
//            return false;
//        });
//
//    };
    this.selectNewUserImage = function () {
        //alert('Image selected');
        //alert(modules.files.fileSelectElement.val());
        
        $('img.imagePreview').attr('src', modules.files.fileSelectElement.data("path"));
        $('#image').val(modules.files.fileSelectElement.data("path"));
    };

//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


};