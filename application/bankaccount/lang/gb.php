<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Bankaccount' =>
	'Bankaccount'
	,
	'Posting date'=>
	'Posting date'
	,
	'Posting text'=>
	'Posting text'
	,
	'Deposit in'=>
	'Deposit in €'
	,
	'Withdraw in €'=>
	'Withdraw in €'
	,
	'Amount'=>
	'Amount'
	,
	'interval'=>
	'interval'
	,
	'fixed interval'=>
	'fixed interval'
	,
	'Add'=>
	'Add'
	,
	'Deposit/Withdraw'=>
	'Deposit/Withdraw'
	,
	'Account created.'=>
	'Account created.'
	,
	'Y-m-d H:i'=>
	'Y-m-d H:i'
	,
	'h'=>
	'h'
	,
	'remove device'=>
	'remove device'
	,
	'Post'=>
	'Post'
	,
	'Balance'=>
	'Balance'
	,
	'Manage Bankaccounts'=>
	'Manage Bankaccounts'
	,
	'Create new bankaccount'=>
	'Create new bankaccount'
	,
	'Save changes'=>
	'Save changes'
	,
	'Account'=>
	'Account'
	,
	'User image'=>
	'User image'
	,
	'Change user image'=>
	'Change user image'
	,
	'click here to select an image'=>
	'click here to select an image'
	,
	'remove image'=>
	'remove image'
	,
	'Nickname'=>
	'Nickname'
	,
	'Username changes will log you out automatically.'=>
	'Username changes will log you out automatically.'
	,
	'Email'=>
	'Email'
	,
	'You will get notifications on this address.'=>
	'You will get notifications on this address.'
	,
	'Password'=>
	'Password'
	,
	'New passphrase'=>
	'New passphrase'
	,
	'Repeat new passphrase'=>
	'Repeat new passphrase'
	,
	'Select a distinctive username for the new account'=>
	'Select a distinctive username for the new account'
	,
	'Passphrase'=>
	'Passphrase'
	,
	'Set a passphrase to access the newly created account.'=>
	'Set a passphrase to access the newly created account.'
	,
	'Repeat your passphrase.'=>
	'Repeat your passphrase.'
	,
	'Create new account'=>
	'Create new account'
	,
	'Create Account'=>
	'Create Account'
	,
	'E-Mail address contains unallowed characters!'=>
	'E-Mail address contains unallowed characters!'
	,
	'Account created successfully!'=>
	'Account created successfully!'
	,
	'Nick have to be at least 3 characters long!'=>
	'Nick have to be at least 3 characters long!'
	,
	'Password repeat does not match with the password!'=>
	'Password repeat does not match with the password!'
	,
	'Cannot create Account!'=>
	'Cannot create Account!'
	,
	'Username is already taken!'=>
	'Username is already taken!'
	,
	'Accountname'=>
	'Accountname'
	,
	'Select a distinctive accountname'=>
	'Select a distinctive accountname'
	,
	'€'=>
	'€'
	,
	'Standing order'=>
	'Standing order'
	,
	'Repetition'=>
	'Repetition'
	,
	'none'=>
	'none'
	,
	'daily'=>
	'daily'
	,
	'monthly'=>
	'monthly'
	,
	'yearly'=>
	'yearly'
	,
	'Repeat interval'=>
	'Repeat interval'
	,
	'Repetition end'=>
	'Repetition end'
	,
	'Create'=>
	'Create'
	,
	'deposit_help'=>
	'deposit_help'
	,
	'standingOrder_help'=>
	'standingOrder_help'
	,
	'First deposit'=>
	'First deposit'
	,
	'keep standing order until cancelation.'=>
	'keep standing order until cancelation.'
	,
	'Last deposit'=>
	'Last deposit'
	,
	'Cancel'=>
	'Cancel'
	,
	'back to overview'=>
	'back to overview'
	,
	'Creation date'=>
	'Creation date'
	,
	'Next deposit'=>
	'Next deposit'
	,
	'Y-m-d'=>
	'Y-m-d'
	,
	'Repeats'=>
	'Repeats'
	,
	'Standing orders'=>
	'Standing orders'
	,
	'weekly'=>
	'weekly'
	,
	'Deposits'=>
	'Deposits'
	,
	'Delete Account'=>
	'Delete Account'
	,
	'Delete Accout'=>
	'Delete Accout'
	,
	'Do you really seek to delete this exquisit account forever? (Thats a long time...)'=>
	'Do you really seek to delete this exquisit account forever? (Thats a long time...)'
	,
	'Yes please, delete Account!'=>
	'Yes please, delete Account!'
	,
	'Account deleted.'=>
	'Account deleted.'
	,
	'Do you really want to delete this standing order?'=>
	'Do you really want to delete this standing order?'
	,
	'standing order deleted.'=>
	'standing order deleted.'
	,
	'Since'=>
	'Since'
	,
	'to'=>
	'to'
	,
	'Delete standing order'=>
	'Delete standing order'
	,
	'Yes please, delete standing order!'=>
	'Yes please, delete standing order!'
	,
	'Edit user'=>
	'Edit user'
	,
	'Edit account'=>
	'Edit account'
	,
	'Update Account'=>
	'Update Account'
	,
	'editaccound_help'=>
	'editaccound_help'
	,
	'User updated successfully'=>
	'User updated successfully'
	,
	'Wrong password.'=>
	'Wrong password.'
	,
	'Password & User updated successfully'=>
	'Password & User updated successfully'
	,
	'all'=>
	'all'
	,
	'D'=>
	'days'
        ,
	'M'=>
	'month'
        ,
	'Y'=>
	'years'
	,
	'Sun'=>
	'Sun'
	,
	'Sat'=>
	'Sat'
	,
	'Fri'=>
	'Fri'
        ,
	'Thu'=>
	'Thu'
	,
	'Wed'=>
	'Wed'
	,
	'Tue'=>
	'Tue'
	,
	'Mon'=>
	'Mon'
	,
	'Password have to be at least 5 characters long and not longer than 64 characters! Use numbers and special chars for security.'=>
	'Password have to be at least 5 characters long and not longer than 64 characters! Use numbers and special chars for security.'
	,
	'Your child can login with this username to see the balance.'=>
	'Your child can login with this username to see the balance.'
	,
	'Set a passphrase to access the account.'=>
	'Set a passphrase to access the account.'
	,
	'Manage Bankaccount'=>
	'Manage Bankaccount'
);