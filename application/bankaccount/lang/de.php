<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Bankaccount' =>
	'Taschengeldkonto'
	,
	'Posting date'=>
	'Buchungsdatum'
	,
	'Posting text'=>
	'Betreff'
	,
	'Deposit in'=>
	'Betrag in €'
	,
	'Amount'=>
	'Betrag'
	,
	'Deposit/Withdraw'=>
	'Einzahlen/Abheben'
	,
	'Account created.'=>
	'Konto eröffnet.'
	,
	'Y-m-d H:i'=>
	'd.m.Y H:i'
	,
	'h'=>
	'Uhr'
	,
	'Post'=>
	'Buchen'
	,
	'Balance'=>
	'Kontostand'
	,
	'Manage Bankaccounts'=>
	'Taschengeldkonten verwalten'
	,
	'Create new bankaccount'=>
	'Erstelle neues Taschengeldkonto'
	,
	'Save changes'=>
	'Änderungen speichern'
	,
	'User image'=>
	'Nutzerbild'
	,
	'Change user image'=>
	'Ändere Nutzerbild'
	,
	'click here to select an image'=>
	'Klicke hier um ein Bild auszuwählen'
	,
	'remove image'=>
	'Bild entfernen'
	,
	'Nickname'=>
	'Nutzername'
	,
	'Email'=>
	'E-Mail Adresse'
	,
	'You will get notifications on this address.'=>
	'Benachrichtigungen zum Konto sowie E-Mails zur Passwortwiederherstellungen werden an diese E-Mail Adresse verschickt.'
	,
	'Repeat new passphrase'=>
	'Passwort Wiederholung'
	,
	'Select a distinctive username for the new account'=>
	'Wähle einen eindeutigen Nutzernamen für den Account. Mit diesem Nutzernamen + Passwort kann sich in das Konto eingeloggt und Kontostand sowie Kontobewegungen eingesehen werden. Der Nutzername kann später nicht mehr geändert werden.'
	,
	'Passphrase'=>
	'Passwort'
	,
	'Set a passphrase to access the newly created account.'=>
	'Vergebe ein Passwort mit dem auf den Account zugegriffen werden kann.'
	,
	'Repeat your passphrase.'=>
	'Wiederhole das Passwort um Tippfehler zu vermeiden.'
	,
	'Create new account'=>
	'Erstelle ein neues Taschengeldkonto'
	,
	'Create Account'=>
	'Erstelle Taschengeldkonto'
	,
	'Password'=>
	'Passwort'
	,
	'E-Mail address contains unallowed characters!'=>
	'Die E-Mail Adresse beinhaltet unerlaubte Zeichen!'
	,
	'Account created successfully!'=>
	'Taschengeldkonto wurde erfolgreich angelegt!'
	,
	'Nick have to be at least 3 characters long!'=>
	'Der Nutzername muss mindestens 3 Zeichen lang sein!'
	,
	'Password repeat does not match with the password!'=>
	'Die Passwortwiederholung stimmt nicht mit dem Passwort überein!'
	,
	'Cannot create Account!'=>
	'Nutzeraccount konnte nicht angelegt werden!'
	,
	'Username is already taken!'=>
	'Der Nutzername ist bereits vergeben!'
	,
	'Accountname'=>
	'Anzeigename'
	,
	'Select a distinctive accountname'=>
	'Der Anzeigename wird zur Anzeige der Accounts verwendet. Es können mehrere Accounts mit gleichem Anzeigenamen bestehen, aber nicht mehr als ein Account mit dem gleichen Nutzernamen.'
	,
	'€'=>
	'€'
	,
	'Standing order'=>
	'Dauerauftrag erstellen'
	,
	'Repetition'=>
	'Wiederholung'
	,
	'daily'=>
	'alle x Tage'
	,
	'monthly'=>
	'alle x Monate'
	,
	'yearly'=>
	'alle x Jahre'
	,
	'Repeat interval'=>
	'Interval (x)'
	,
	'Create'=>
	'Erstellen'
	,
	'deposit_help'=>
	'Zum Auszahlen einen negativen Betrag eingeben.'
	,
	'standingOrder_help'=>
	'Ein Dauerauftrag ist eine regelmässige Buchung auf das Konto. Bei monatlichen Buchungen sollten Anfang oder Mitte eines Monats als Anfangszeitpunkt eingestellt werden.'
	,
	'First deposit'=>
	'Erste Zahlung'
	,
	'Last deposit'=>
	'Letzte Zahlung'
	,
	'Cancel'=>
	'Abbrechen'
	,
	'back to overview'=>
	'Zurück zur Übersicht'
	,
	'Creation date'=>
	'Erstellungszeit'
	,
	'Next deposit'=>
	'Nächste Zahlung'
	,
	'Y-m-d'=>
	'd.m.Y'
	,
	'Repeats'=>
	'Wiederholt sich'
	,
	'Standing orders'=>
	'Daueraufträge'
	,
	'weekly'=>
	'wöchentlich'
	,
	'Deposits'=>
	'Buchungen'
	,
	'Delete Account'=>
	'Taschengeldkonto löschen'
	,
	'Do you really seek to delete this exquisit account forever? (Thats a long time...)'=>
	'Möchtest Du wirklich dieses Taschengeldkonto für immer löschen? (Das ist eine sehr lange Zeit...)'
	,
	'Yes please, delete Account!'=>
	'Ja, bitte lösche dieses Taschengeldkonto!'
	,
	'Account deleted.'=>
	'Taschengeldkonto gelöscht.'
	,
	'Do you really want to delete this standing order?'=>
	'Möchtests Du diesen Dauerauftrag wirklich für immer löschen?'
	,
	'standing order deleted.'=>
	'Dauerauftrag gelöscht.'
	,
	'Since'=>
	'Von'
	,
	'to'=>
	'bis'
	,
	'Delete standing order'=>
	'Dauerauftrag löschen'
	,
	'Yes please, delete standing order!'=>
	'Ja, bitte lösche den Dauerauftrag'
	,
	'Edit user'=>
	''
	,
	'Edit account'=>
	'Taschengeldkonto bearbeiten'
	,
	'Update Account'=>
	'Taschengeldkonto aktualisieren'
	,
	'User updated successfully'=>
	'Nutzer erfolgreich geändert'
	,
	'Password & User updated successfully'=>
	'Passwort und Nutzer erfolgreich geändert'
	,
	'all'=>
	'Alle'
	,
	'D'=>
	'Tage'
        ,
	'M'=>
	'Monate'
        ,
	'Y'=>
	'Jahre'
	,
	'Sun'=>
	'Sonntag'
	,
	'Sat'=>
	'Samstag'
	,
	'Fri'=>
	'Freitag'
	,
	'Wed'=>
	'Mittwoch'
	,
	'Tue'=>
	'Dienstag'
	,
	'Thu'=>
	'Donnerstag'
	,
	'Mon'=>
	'Montag'
	,
	'Password have to be at least 5 characters long and not longer than 64 characters! Use numbers and special chars for security.'=>
	'Passwörter müssen mindestens 5 Zeichen lang sein und nicht mehr als 64 Zeichen lang. Nutze Nummern und Spezialzeichen für extra sichere Passwörter.'
	,
	'Your child can login with this username to see the balance.'=>
	'Das Taschengeldkind kann sich mit dem Nutzernamen und einem frei zu vergebenen Passwort anmelden um sein Kontostand und Kontoaktivitäten zu überprüfen.'
	,
	'Set a passphrase to access the account.'=>
	'Setze hier ein Passwort für den Account. Passwörter müssen mindestens 5 Zeichen lang sein.'
	,
	'Manage Bankaccount'=>
	'Taschengeldkonto verwalten'
);