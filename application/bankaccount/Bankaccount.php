<?php

/**
 *  Copyright © 2021 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Bankaccount extends AppController implements ICronable {

    private $userTable;
    private $currentUserId;

    public function __construct() {
        $this->currentUserId = Session::get('uid');
        $this->userTable = "bankaccount_" . $this->currentUserId;
    }

    public function index() {
        //$this->runStandingOrders('10');
        $this->autoCheckRights();
        $this->createUserTable(); //if not exists
        //$this->runStandingOrders('5');
        if ($this->checkUserRights('55') && !$this->checkUserRights('1')) {
            // managed bankaccount login
            $this->showBankaccount($this->currentUserId);
        }
        if ($this->checkSiteRights('bankaccount/admin')) {
            $this->admin();
        }
    }

    public function admin() {
        //$this->runStandingOrders('10');
        /*
         * TODO:
         * - get_users 
         * - show users with bankaccounts
         * - create special users (55) with bankaccounts, 
         * creator is parent and admin of these
         * 
         * - create and link to edit bankaccounts
         * 
         */
        $this->autoCheckRights();

        $this->includeJs(Config::get('applicationDirectory') . '/files/js/.files.js');
       // $this->includeJs(Config::get('applicationDirectory') . '/user/js/user.js');
        $data = array();

        if (filter_has_var(INPUT_POST, 'deleteaccount')) {
            $userId = filter_input(INPUT_POST, 'account_user_id', FILTER_SANITIZE_NUMBER_INT);
            $this->deleteBankaccount($userId);
            $this->jumpTo('bankaccount/admin');
        }
        if (filter_has_var(INPUT_POST, 'updatebalance')) {
            $userId = filter_input(INPUT_POST, 'userId', FILTER_SANITIZE_NUMBER_INT);
            $this->updateUserBalance($userId);
            $this->jumpToLast();
        }

        if (filter_has_var(INPUT_POST, 'deleteStandingOrderButton')) {
            $userId = filter_input(INPUT_POST, 'account_user_id', FILTER_SANITIZE_NUMBER_INT);
            $id = filter_input(INPUT_POST, 'deleteStandingOrder', FILTER_SANITIZE_NUMBER_INT);
            $this->deleteStandingOrder($id);
            $this->jumpToLast('bankaccount/admin/' . $userId);
        }
        if (filter_has_var(INPUT_POST, 'createStandingorder')) {
            $userId = filter_input(INPUT_POST, 'account_user_id', FILTER_SANITIZE_NUMBER_INT);
            $this->createStandingOrder($userId);
            $this->jumpToLast();
        }

        if (filter_has_var(INPUT_POST, 'insertBankaccount')) {
            $this->createManagedBankaccount();

            // if an validation error occured, 
            $data['transientNewAccount'] = array(
                'nick' => filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW),
                'email' => filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL),
                'account_name' => filter_input(INPUT_POST, 'account_name', FILTER_UNSAFE_RAW),
                'image' => filter_input(INPUT_POST, 'image', FILTER_UNSAFE_RAW)
            );

            //return;
        } else {
            $data['transientNewAccount'] = array(
                'nick' => '',
                'email' => '',
                'account_name' => '',
                'image' => "theme/default/images/profil.png"
            );
        }
        if (filter_has_var(INPUT_POST, 'updateBankaccount')) {
            $this->updateBankaccount();
            $this->jumpTo('bankaccount');
        }

        if (filter_has_var(INPUT_GET, 'editUser')) {
            $userId = filter_input(INPUT_GET, 'editUser', FILTER_SANITIZE_NUMBER_INT);
            if ($this->isAccountManagerFor($userId)) {
                $managedUserIds = $this->selectPrepare('bankaccount_managed', "account_name", "user_id=:id", array('id' => $userId));
                $data['transientNewAccount'] = $this->getUserData($userId);
                $data['transientNewAccount']['account_name'] = $managedUserIds[0]['account_name'];
                $this->view('editBankaccount', $data);
                return;
            }
        }

        if (filter_has_var(INPUT_GET, 'createBankaccount')) {
            $data['transientNewAccount']['password'] = $this->generateRandomString(24);
            $data['transientNewAccount']['parentNick'] = Session::get('unick');
            $this->view('createBankaccount', $data);
            return;
        }

        if (filter_has_var(INPUT_GET, 'id')) {
            $userId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
            $this->showBankaccount($userId);
            return;
        }

        $data['managedBankaccounts'] = $this->getManagedAccounts();

        $this->view('bankaccountadmin', $data);
    }

    /**
     * Cronable
     * @return true or false
     */
    public function runMaintainance() {
        return $this->runStandingOrders('200');
    }

    /**
     * runs standing orders without any rights restriction,
     *  for getting called by cron jobs or similar.
     */
    public function runStandingOrders($limit = null) {
        $sendHeader = false;
        if ($limit == null) {
            $limit = '200';
            $sendHeader = true;
        }

        $rs = $this->select('bankaccount_standpay', "*", "next_deposit <= NOW() AND next_deposit IS NOT NULL", "next_deposit desc", "$limit");
        $i = 0;
        foreach ($rs as $so) {
            $i++;
            /**
             * TODO:
             * - add money to the given bankaccount
             */
            $this->updateUserBalance($so['account_user_id'], $so['deposit'], $so['purpose'], $i);

            /**
             * 
             * - evaluate repeat pattern
             * - add time to 'next_deposit' and update table
             * - if next_deposit would be after 'last_deposit', 
             *   ° send a notification to the adminaccount of that bankaccount 
             *   ° null next_deposit
             */
            $date = new DateTime($so['next_deposit']);

            $interval = new DateInterval($so['payment_pattern']);

            $date->add($interval);

            if ($so['last_deposit'] == null || $date->format("Y-m-d H:i:s") < $so['last_deposit']) {
                $fieldsValueArray = array(
                    'next_deposit' => $date->format("Y-m-d H:i:s")
                );
                $this->updatePrepare('bankaccount_standpay', $fieldsValueArray, 'id=:id LIMIT 1', array( 'id' => $so['id']) );
            } else {
                $fieldsValueArray = array(
                    'next_deposit' => 'NULL'
                );
                $this->updatePrepare('bankaccount_standpay', $fieldsValueArray, 'id=:id LIMIT 1', array( 'id' => $so['id']) );
            }
        }
        if ($sendHeader) {
            $this->standalone = true;
            header("Status: 200");
        } else {
            return true;
        }
    }

    private function deleteStandingOrder($id) {
        if ($this->deletePrepare('bankaccount_standpay', "id=:id AND created_by_user_id=:currentUserId", array('id' => $id, 'currentUserId' => $this->currentUserId))) {
            $this->setSystemMessage($this->ts("standing order deleted."));
        } else {
            // nothing
        }
    }

    /**
     *  deletes a bankaccount including 55 useraccount
     * @param type $userId 
     */
    private function deleteBankaccount($userId) {
        if ($this->isAccountManagerFor($userId)) {

            //delete bankaccount
            $this->deletePrepare('bankaccount_managed', "user_id=:userId", array(':userId'=> $userId) );
            // delete userTable
            $this->query("DROP TABLE `" . Config::get('dbPrefix') . "bankaccount_" . $userId . "`");
            //delete standing orders
            $this->deletePrepare('bankaccount_standpay', "account_user_id=:userId", array(':userId'=> $userId));
            //delete user
            $this->deletePrepare('user', "id=:userId", array(':userId'=> $userId));
            $this->setSystemMessage($this->ts("Account deleted."));
        } else {
            $this->setErrorMessage($this->td('You have not the right to delete this account.'));
        }
    }

    private function showBankaccount($userId) {

        $managedUserIds = $this->selectPrepare('bankaccount_managed', "user_id,managed_by_user_id,account_name", "user_id=:userId", array(':userId'=> $userId));
        if ($managedUserIds == null) {
            $accountName = '';
        } else {
            $accountName = $managedUserIds[0]['account_name'];
        }
        if (filter_has_var(INPUT_POST, 'showDeposits')) {
            $data['sinceDate'] = filter_input(INPUT_POST, 'since_date', FILTER_UNSAFE_RAW);
            $data['toDate'] = filter_input(INPUT_POST, 'to_date', FILTER_UNSAFE_RAW);
        } else {
            $data['sinceDate'] = date("Y-m-d", strtotime("-1 month", strtotime(date("Y-m-d"))));
            $data['toDate'] = date("Y-m-d");
        }

//        echo "userid:" .$userId;
//        print_r($managedUserIds);
        if ($userId == Session::get('uid') || $managedUserIds[0]['managed_by_user_id'] == $this->currentUserId) {
            $data['isAdmin'] = $this->isAccountManagerFor($userId);
            $data['standingOrders'] = $this->getStandingOrders($userId);
            $data['userData'] = $this->getUserData($userId);
            $data['userData']['account_name'] = $accountName;
            $data['userBalance'] = $this->getNewBalance($userId);
            $data['accountData'] = $this->getUserAccountData($userId);
            $this->view('bankaccount', $data);
        } else {
            return;
        }
    }

    private function isAccountManagerFor($userId) {
        // wenn man admin ist und das eigene Konto bearbeiten möchte,
        if ($this->checkSiteRights('bankaccount/admin') && $userId == Session::get('uid')) {
            return true;
        }
        $managedUserIds = $this->selectPrepare('bankaccount_managed', "user_id", "managed_by_user_id=:currentUserId" , array('currentUserId' => $this->currentUserId));

        // wenn man admin ist und ein eigens angelegtes, verwaltetes Konto bearbeiten möchte.
        if ($this->checkSiteRights('bankaccount/admin') && in_array($userId, array_column($managedUserIds, 'user_id'))) {
            return true;
        } else {
            return false;
        }
    }

    private function getManagedAccounts() {
        // username, userimage, balance
        $managedUserIds = $this->selectPrepare('bankaccount_managed', "user_id,account_name", "managed_by_user_id=:currentUserId" , array('currentUserId' => $this->currentUserId));
        $managedUserAccounts = array();
        foreach ($managedUserIds as $u) {
            $userId = $u['user_id'];
            $user = $this->selectPrepare('user', 'id,nick,last_online,image', 'id=:id', array('id' => $userId));
            $balance = $this->getNewBalance($userId);
            $managedUserAccounts[] = array(
                'user_id' => $userId,
                'nick' => $u['account_name'],
                'last_online' => $user[0]['last_online'],
                'image' => $user[0]['image'],
                'balance' => $balance,
            );
        }
        return $managedUserAccounts;
    }

    private function getStandingOrders($userId) {
        $rs = $this->selectPrepare('bankaccount_standpay', "*", "account_user_id=:userId ORDER BY date_created desc LIMIT 50" , array('userId' => $userId));
        $so = array();
        foreach($rs as $r){
            $timeUnit = substr($r['payment_pattern'], -1);
            $timeCount = substr($r['payment_pattern'], 1, strlen($r['payment_pattern'])-2);
            //echo $timeUnit." ".$timeCount;
           
            $r['payment_pattern'] =  $this->ts("all")." ".$timeCount." ".$this->ts($timeUnit);
            $so[] = $r;
        }
       // exit;
        return $so;
    }

    private function createStandingOrder($userId) {


        $deposit = filter_input(INPUT_POST, 'amount', FILTER_VALIDATE_FLOAT);
        
        $purpose = filter_input(INPUT_POST, 'purpose', FILTER_UNSAFE_RAW);

        $firstdeposit = filter_input(INPUT_POST, 'first_deposit', FILTER_UNSAFE_RAW);

        $lastdeposit = filter_input(INPUT_POST, 'last_deposit', FILTER_UNSAFE_RAW);
        if($lastdeposit!=''){
            $lastdeposit .= ' 23:59:59';
        }
        $repeat = filter_input(INPUT_POST, 'repeat', FILTER_UNSAFE_RAW);

        $interval = filter_input(INPUT_POST, 'repeat_interval', FILTER_UNSAFE_RAW);

        /**
         * CREATE TABLE IF NOT EXISTS `{dbprefix}bankaccount_standpay` (
          `id` bigint(11) NOT NULL AUTO_INCREMENT,
          `account_user_id` bigint(11) NOT NULL,
          `created_by_user_id` bigint(11) NOT NULL,
          `deposit` float NOT NULL,
          `purpose` text NOT NULL,
          `payment_pattern` text NOT NULL,
          `first_deposit` DATETIME NOT NULL,
          `next_deposit` DATETIME NOT NULL,
          `last_deposit` DATETIME NOT NULL,
          `date_created` DATETIME DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
          ); */
        $fieldsValueArray = array(
            'account_user_id' => $userId,
            'purpose' => $purpose,
            'deposit' => $deposit,
            'payment_pattern' => 'P' . $interval . strtoupper($repeat),
            'first_deposit' => $firstdeposit,
            'next_deposit' => $firstdeposit,
            'created_by_user_id' => $this->currentUserId
        );
        if($lastdeposit!=''){
            $lastdeposit .= ' 23:59:59';
            $fieldsValueArray['last_deposit'] = $lastdeposit;
        }
        $this->insert("bankaccount_standpay", $fieldsValueArray);
    }

    private function updateBankaccount() {
        $roles = '55'; //always set role 55 for bankaccounts

        if (filter_input(INPUT_POST, 'account_name') == '' || strlen(filter_input(INPUT_POST, 'account_name')) < 3) {
            $this->setErrorMessage($this->ts("Account Name have to be at least 3 characters long!"));
            return FALSE;
        }

        if (filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL) != filter_input(INPUT_POST, 'email')) {
            $this->setErrorMessage($this->ts("E-Mail address contains unallowed characters!"));
            return FALSE;
        }

        if (filter_input(INPUT_POST, 'password') != '') {
            if (strlen(filter_input(INPUT_POST, 'password')) < 5 || strlen(filter_input(INPUT_POST, 'password')) > 64) {
                $this->setErrorMessage($this->ts("Password have to be at least 5 characters long and not longer than 64 characters! Use numbers and special chars for security."));
                return FALSE;
            }

//            if (filter_input(INPUT_POST, 'password') != filter_input(INPUT_POST, 'passrepeat')) {
//                $this->setErrorMessage($this->ts("Password repeat does not match with the password!"));
//                return FALSE;
//            }

            if (filter_input(INPUT_POST, 'password') != filter_input(INPUT_POST, 'password', FILTER_UNSAFE_RAW)) {
                $this->setErrorMessage($this->ts("Unallowed characters occure in password! Please change the password!"));
                return FALSE;
            }
        }

        if (filter_input(INPUT_POST, 'image') != filter_input(INPUT_POST, 'image', FILTER_UNSAFE_RAW)) {
            $this->setErrorMessage($this->ts("Something unusual was entered in the image field, please use the filemanager to select a picture!"));
            return FALSE;
        }
        
        $accountname = filter_input(INPUT_POST, 'account_name', FILTER_UNSAFE_RAW);
        $fva = array(
            'account_name' => $accountname
        );
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $this->updatePrepare('bankaccount_managed', $fva, "user_id=:id", array('id' => $id));
        
        $this->updateUser();
        
        //$newId = $this->updateUserData(filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW), filter_input(INPUT_POST, 'password', FILTER_UNSAFE_RAW), filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL), '1', $roles, filter_input(INPUT_POST, 'image', FILTER_UNSAFE_RAW));
//        if (!$newId) {
//            $this->setErrorMessage($this->ts("Cannot create Account!"));
//        } else {
//            $this->setSystemMessage($this->ts("Account created successfully!"));
//        }
    }
    
    private function createManagedBankaccount() {
        if (!$this->checkSiteRights('bankaccount/admin')) {
            return;
        }
        // create bankaccount role if not exists
        if (sizeof($this->selectPrepare('user_role', 'id', 'id=55')) == 0) {
            $fieldsValueArray = array(
                'id' => '55',
                'name' => 'bankaccount',
                'description' => 'Special role for bankaccounts'
            );
            $this->insert('user_role', $fieldsValueArray);
        }

        //create user account
        $newUserAccountId = $this->createUserAccount();

        if ($newUserAccountId !== FALSE) {
            // create bankaccount table
            $this->createUserTable($newUserAccountId);

            // managed entry
            $fieldsValueArray = array(
                'user_id' => $newUserAccountId,
                'account_name' => filter_input(INPUT_POST, 'account_name', FILTER_UNSAFE_RAW),
                'managed_by_user_id' => $this->currentUserId
            );
            $this->insert('bankaccount_managed', $fieldsValueArray);
            $this->jumpTo("bankaccount/admin");
        }
    }

    private function getUserData($userId = null) {
        if ($userId == null) {
            $userId = $this->currentUserId;
        }
        $user = $this->select('user', 'id,nick,email,last_online,image', 'id=' . $userId);
        return $user[0];
    }

    private function getUserAccountData($userId = null) {

        $sinceDate = date("Y-m-d", strtotime("-1 month", strtotime(date("Y-m-d")))) . ' 00:00:00';
        $toDate = date("Y-m-d") . ' 23:59:59';

        if (filter_has_var(INPUT_POST, 'showDeposits')) {
            $sinceDate = filter_input(INPUT_POST, 'since_date', FILTER_UNSAFE_RAW) . ' 00:00:00';
            $toDate = filter_input(INPUT_POST, 'to_date', FILTER_UNSAFE_RAW) . ' 23:59:59';
        }
        // echo $sinceDate . " --- " .$toDate;// die;
        if ($sinceDate > $toDate) {
            //
        }

        if ($userId != null) {
            $userTable = "bankaccount_" . $userId;
        } else {
            $userTable = $this->userTable;
        }


        if ($this->tableExists($userTable)) {
            return $this->select($userTable, "*", "date > '$sinceDate' and date < '$toDate' ", "date desc", "200");
        } else {
            return array();
        }
    }

    /**
     * 
     * @param type $userId
     * @param type $deposit
     */
    private function updateUserBalance($userId, $deposit = null, $purpose = null, $i = 1) {
        if ($deposit == null) {
            $deposit = filter_input(INPUT_POST, 'amount', FILTER_VALIDATE_FLOAT);
        }
        if ($purpose == null) {
            $purpose = filter_input(INPUT_POST, 'purpose', FILTER_UNSAFE_RAW);
        }

        //echo $deposit; exit;
        $balance = $this->getNewBalance($userId, $deposit, "$i=$i"); //"$i=$i" is a workaround for request query caching. Otherwiese more than one standpays on one account will not add up correctly. 

        $userTable = "bankaccount_" . $userId;
        $fieldsValueArray = array(
            'purpose' => $purpose,
            'deposit' => $deposit,
            'balance' => $balance,
            'changed_by_user_id' => $this->currentUserId
        );
        $this->insert($userTable, $fieldsValueArray);
    }

    private function createUserTable($userId = null) {
        if ($userId != null) {
            $userTable = "bankaccount_" . $userId;
        } else {
            $userTable = $this->userTable;
        }
        // create user bankaccount table and first entry
        if (!$this->tableExists($userTable)) {
            $sql = "CREATE TABLE IF NOT EXISTS "
                    . "`" . $this->getConfig('dbPrefix') . $userTable . "` ("
                    . "`id` BIGINT NOT NULL AUTO_INCREMENT,"
                    . "`balance` FLOAT NOT NULL,"
                    . "`deposit` FLOAT NOT NULL,"
                    . "`purpose` text NOT NULL,"
                    . "`changed_by_user_id` int(11) NOT NULL DEFAULT '0',"
                    . "`date` DATETIME DEFAULT CURRENT_TIMESTAMP,"
                    . "PRIMARY KEY (`id`)"
                    . ");";
            $this->query($sql);

            $fieldsValueArray = array(
                "balance" => 0,
                "deposit" => 0,
                "purpose" => $this->ts('Account created.'),
            );
            $this->insert($userTable, $fieldsValueArray);
        }
    }

    /**
     * .
     * @param type $userId
     * @param type $deposit
     * @param type $where - workaround against query request cache.
     * @return int
     */
    private function getNewBalance($userId, $deposit = null, $where = '1=1') {
        $userTable = "bankaccount_" . $userId;
        $rs = $this->select($userTable, 'balance', $where, 'date desc', '1');
        if ($rs === FALSE) {
            return 0;
        } else if ($deposit == null) {
            return $rs[0]['balance'];
        } else {
            return $deposit + $rs[0]['balance'];
        }
    }

    private function createUserAccount() {

        $roles = '55'; //always set role 55 for bankaccounts

        if (filter_input(INPUT_POST, 'nick') == '' || strlen(filter_input(INPUT_POST, 'nick')) < 3) {
            $this->setErrorMessage($this->ts("Username have to be at least 3 characters long!"));
            return FALSE;
        }
        if (sizeof($this->selectPrepare('user', 'nick', "nick=:nick", array('nick' => filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW)))) > 0) {
            $this->setErrorMessage($this->ts("Username is already taken!"));
            return FALSE;
        }

//        if (filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL) != filter_input(INPUT_POST, 'email')) {
//            $this->setErrorMessage($this->ts("E-Mail address contains unallowed characters!"));
//            return FALSE;
//        }
//
//        if (filter_input(INPUT_POST, 'password') == '' || strlen(filter_input(INPUT_POST, 'password')) < 5 || strlen(filter_input(INPUT_POST, 'password')) > 64) {
//            $this->setErrorMessage($this->ts("Password have to be at least 5 characters long and not longer than 64 characters! Use numbers and special chars for security."));
//            return FALSE;
//        }
//
//        if (filter_input(INPUT_POST, 'password') != filter_input(INPUT_POST, 'passrepeat')) {
//            $this->setErrorMessage($this->ts("Password repeat does not match with the password!"));
//            return FALSE;
//        }
//
//        if (filter_input(INPUT_POST, 'password') != filter_input(INPUT_POST, 'password', FILTER_UNSAFE_RAW)) {
//            $this->setErrorMessage($this->ts("Unallowed characters occure in password! Please change the password!"));
//            return FALSE;
//        }

        if (filter_input(INPUT_POST, 'image') != filter_input(INPUT_POST, 'image', FILTER_SANITIZE_URL)) {
            $this->setErrorMessage($this->ts("Something unusual was entered in the image field, please use the filemanager to select a picture!"));
            return FALSE;
        }
        $newId = $this->insertUserData(filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW), filter_input(INPUT_POST, 'password', FILTER_UNSAFE_RAW), filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL), '1', $roles, filter_input(INPUT_POST, 'image', FILTER_SANITIZE_URL));
        if (!$newId) {
            $this->setErrorMessage($this->ts("Cannot create Account!"));
        } else {
            $this->setSystemMessage($this->ts("Account created successfully!"));
        }
        return $newId;
    }

    /**
     * REDUNDANT!
     * duplicated from User.php
     * START
     */
    private function insertUserData($nick, $pass, $email, $active, $roles, $image) {

        $randomSalt = $this->generateRandomString();
        $fieldsValueArray = array(
            'nick' => "$nick",
            'password' => "" . $randomSalt . hash('sha512', $randomSalt . $pass) . "",
            'email' => "$email",
            'active' => "$active",
            'image' => str_replace(Config::get('relativeUrl'), '', $image),
            'roles' => "$roles"
        );
        $newUserId = $this->insert('user', $fieldsValueArray);

        if ($newUserId === FALSE) {
            //
        } else {

            if (!$this->isModuleActive('notifications')) {
                return $newUserId;
            }
            // create notification settings
            $rs = $this->selectPrepare('notifications_settings', '*', "user_id=:newUserId", array('newUserId' => $newUserId));

            if ($rs === FALSE || sizeof($rs) == 0) {
                $fieldsValueArray = array();
                $fieldsValueArray['user_id'] = $newUserId;
                $this->insert('notifications_settings', $fieldsValueArray);
            }
        }
        return $newUserId;
    }
     /**
     * REDUNDANT!
     * duplicated from User.php
     * END
     */
    private function updateUser() {

        /**
         * TODO:
         * - fix possible to long password attack scenario. 
         */
        $currentUid = Session::get('uid');
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $nick = filter_input(INPUT_POST, 'nick', FILTER_UNSAFE_RAW);
        $image = filter_input(INPUT_POST, 'image', FILTER_SANITIZE_URL);
        $wallpaper = filter_input(INPUT_POST, 'wallpaper', FILTER_SANITIZE_URL);

        $description = filter_input(INPUT_POST, 'description', FILTER_UNSAFE_RAW);

        if ($currentUid !== $id) {
            if (!$this->checkSiteRights("bankaccount/admin")) {
                die; // lol
            }
        }

        if (Config::get('relativeUrl') != '/') {
            $userImagePath = str_replace(Config::get('relativeUrl'), '', $image); //$this->uploadUserImage($id);

            $wallpaperImagePath = str_replace(Config::get('relativeUrl'), '', $wallpaper); //$this->uploadUserImage($id);
        }
        if (Config::get('relativeUrl') == '/') {
            if ($image != '') {
                $userImagePath = substr($image, 1);
            } else {
                $userImagePath = '';
            }
            if ($wallpaper != '') {
                $wallpaperImagePath = substr($wallpaper, 1);
            } else {
                $wallpaper = '';
            }
        }
        if ($userImagePath !== false) {
            $fieldsValueArray['image'] = $userImagePath;
        }
        if ($id == $currentUid) {
            if ($userImagePath == "") {
                Session::set('uimage', "theme/" . Config::get("siteStyle") . "/images/profil.png", true);
            } else {
                Session::set('uimage', $userImagePath, true);
            }
        }
        if ($wallpaperImagePath !== false) {
            $fieldsValueArray['wallpaper'] = $wallpaperImagePath;
            if ($id == $currentUid) {
                Session::set('uwallpaper', $wallpaperImagePath, true);
            }
        }
        $repeat_wallpaper = '0';
        if (filter_has_var(INPUT_POST, 'repeat_wallpaper')) {
            $repeat_wallpaper = '1';
        }
        Session::set('urepeat_wallpaper', $repeat_wallpaper, true);

        $fieldsValueArray['repeat_wallpaper'] = $repeat_wallpaper;

        $fieldsValueArray['email'] = $email;
        //$fieldsValueArray['nick'] = $nick;
        $fieldsValueArray['description'] = $description;


        if (filter_input(INPUT_POST, 'password', FILTER_UNSAFE_RAW) != '') {

            $randomSalt = $this->generateRandomString();
            $newhash = $randomSalt . hash('sha512', $randomSalt . filter_input(INPUT_POST, 'password', FILTER_UNSAFE_RAW));

            $fieldsValueArray['password'] = $newhash;
            if ($this->updatePrepare('user', $fieldsValueArray, "id=:id", array('id' => $id))) {
                $this->setSystemMessage($this->ts('Password & User updated successfully'));
            } else {
                $this->setErrorMessage($this->ts('Something went wrong while updating the user'));
            }
        } else {

            if ($this->updatePrepare('user', $fieldsValueArray, "id=:id", array('id' => $id))) {
                $this->setSystemMessage($this->ts('User updated successfully'));
            } else {
                $this->setErrorMessage($this->ts('Something went wrong while updating the user'));
            }
        }
    }

   
}
