<?php

/**
 *  Copyright © tim 12.03.2014
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Notifications extends AppController {

    public function index() {
        //put your code here
        //$this->autoCheckRights();

        if (filter_has_var(INPUT_POST, 'mode')) {
            $postMode = filter_input(INPUT_POST, 'mode');
        } else {
            $this->autoCheckRights();
        }

        if (filter_has_var(INPUT_GET, 'viewNotificationSrc')) {
            $url = filter_input(INPUT_GET, 'viewNotificationSrc');
            $id = filter_input(INPUT_GET, 'id');
            $this->markAsRead($id);
            $this->jumpTo(str_replace(Config::get('reLangUrl'), '', $url));
            exit;
        }

        if (isset($postMode) && $postMode == 'getNotificationsHTML') {
            $this->standalone = true;
            $this->getNotificationsHTML();
            return;
        }

        if (isset($postMode) && $postMode == 'getUnreadNotificationsCount') {
            $this->standalone = true;
            if (!$this->isModuleActive('notifications') || !$this->checkSiteRights()) {
                echo "disabled";
                return;
            }
            echo $this->getUnreadNotificationsCount();
            return;
        }

        if (filter_has_var(INPUT_POST, 'markAllAsRead')) {
            $this->markAllAsRead();
            return;
            //$this->jumpTo('notifications?getNotificationsHTML=1&page=1');
        }

        if (filter_has_var(INPUT_GET, 'markAllAsRead')) {
            $this->markAllAsRead();
            $this->jumpTo('notifications?getNotificationsHTML=1&page=1');
        }

        if (filter_has_var(INPUT_POST, 'markAsRead')) {
            if (is_numeric(filter_input(INPUT_POST, 'markAsRead'))) {
                $id = filter_input(INPUT_POST, 'markAsRead');
                $this->markAsRead($id);
            }
        }

        if (filter_has_var(INPUT_GET, 'getNotificationsHTML')) {
            $page = 1;
            if (filter_has_var(INPUT_GET, 'page')) {
                $page = filter_input(INPUT_GET, 'page');
            }
            $this->getNotificationsHTML($page, 20, false);
            return;
        } else {
            $page = 1;
            if (filter_has_var(INPUT_GET, 'page')) {
                $page = filter_input(INPUT_GET, 'page');
            }
            $this->getNotificationsHTML($page, 20, false);
            return;
        }
    }

    public function admin() {
        //put your administrative code here
        $this->autoCheckRights();
        $data = array();

        if (filter_has_var(INPUT_POST, 'save')) {
            $this->saveSettings();
        }


        $userId = Session::get('uid');


        $rs = $this->select('notifications_settings', '*', "user_id='$userId'");

        if ($rs === FALSE || sizeof($rs) == 0) {
            $this->createSettings($userId);

            $rs = $this->select('notifications_settings', '*', "user_id='$userId' AND 1=1"); // AND 1=1 to override caching functionality
        }

        /**
         * DONE:
         * - get Categories from modules and alter table if necessary
         */
        $settings = $rs[0];
        $s = array();
        
        foreach($settings as $key => $value) {
            if ($key != 'id' && $key != 'user_id') {
                if ($value == 1) {
                    $s[$key] = 'checked="checked"';
                } else {
                    $s[$key] = '';
                }
            }
        }

        $data['s'] = $s;
        $data['userId'] = $userId;
        $data['moduleSettings'] = $this->getModuleSettings($data); // nice!!

        $this->view('notifications_settings', $data);
    }

    private function getModuleSettings($data) {
        $resultString = "";
        $directory = Config::get('applicationDirectory');
        $modules = scandir($directory);

        foreach ($modules as $module) {

            $modDir = $directory . '/' . $module;
            $modClass = ucfirst($module);
            if (is_dir($modDir) && is_file($modDir . '/' . $modClass . '.php') && substr($module, 0, 1) !== '.') {
                include_once $modDir . '/' . $modClass . '.php';
                $modFunctions = get_class_methods($modClass);

                if (in_array('getNotificationSettings', $modFunctions) && $this->isModuleActive($module)) {
                    //echo $module;
                    $m = new $modClass();
                    $this->setTemplatePath($modDir . '/view/');
                    $m->loadLang($modDir . "/", $this->getLanguage());
                    $resultString .= $m->getNotificationSettings($data);

                    if (in_array('getNotificationCategories', $modFunctions)) {
                        $this->createSettingsColumns($m->getNotificationCategories());
                    }
                }
            }
        }
        $this->loadLang($directory . '/notifications/', $this->getLanguage());
        $this->setTemplatePath($directory . '/notifications/view/');
        return $resultString;
    }

    /**
     * init all notification Categories known in current installation
     * DONE: Use where applicable.
     * * on install?
     */
    public function onInstall() {
        $directory = Config::get('applicationDirectory');
        $modules = scandir($directory);

        foreach ($modules as $module) {
            $modDir = $directory . '/' . $module;
            $modClass = ucfirst($module);
            if (is_dir($modDir) && is_file($modDir . '/' . $modClass . '.php') && substr($module, 0, 1) !== '.') {
                include_once $modDir . '/' . $modClass . '.php';
                $modFunctions = get_class_methods($modClass);
                if (in_array('getNotificationCategories', $modFunctions)) {
                    $m = new $modClass();
                    $this->createSettingsColumns($m->getNotificationCategories());
                }
            }
        }
    }

    private function createSettingsColumns($catArray) {
        /**
         * DONE:
         * - check if column is there
         * - create column if not
         */
        foreach ($catArray as $cat) {
            $result = $this->query("SHOW COLUMNS FROM `" . Config::get('dbPrefix') . "notifications_settings` LIKE '$cat'", true);
            if ($result == null || count($result) === 0) {
                // create categorie, alter table add column
                $sql = "ALTER TABLE " . Config::get('dbPrefix') . "notifications_settings ADD $cat tinyint(1) NOT NULL DEFAULT '1';";
                $this->query($sql);
            }
        }
    }

    public function widget() {
        $this->autoCheckRights();

        $data['notifications'] = $this->select('notifications', '*', '1=1', 'time DESC', '0,5');

        return $this->fetch('widget', $data);
    }

    private function markAllAsRead() {


        $userNotificationTable = 'notifications_' . Session::get('uid');

        $fieldsValueArray = array(
            'read' => '1'
        );

        $this->updatePrepare($userNotificationTable, $fieldsValueArray, '`read`=0');
    }

    private function markAsRead($id) {


        $userNotificationTable = 'notifications_' . Session::get('uid');
        if($this->tableExists($userNotificationTable)){
            $fieldsValueArray = array(
                'read' => '1'
            );
            $this->updatePrepare($userNotificationTable, $fieldsValueArray, "id=:id", array('id' => $id));
        }
        
    }

    private function getNotificationsHTML($page = 1, $limitCount = 5, $unreadOnly = true) {


        $userNotificationTable = 'notifications_' . Session::get('uid');

        if (!$this->tableExists($userNotificationTable)) {
            echo $this->ts('no notifications...');
            return;
        } else {
            //allow only int value
            $limitStart = (int)($page - 1) * (int)$limitCount;
            $limitStart = filter_var($limitStart, FILTER_VALIDATE_INT);
            $limitCount = filter_var($limitCount, FILTER_VALIDATE_INT);

            if ($unreadOnly) {
                $rs = $this->selectPrepare($userNotificationTable, '*', "`read`='0' ORDER BY time DESC LIMIT $limitStart,$limitCount" );
            } else {
                $rs = $this->selectPrepare($userNotificationTable, '*', "1=1 ORDER BY time DESC LIMIT $limitStart,$limitCount" );
            }

            $notificationsHtml = '';
            foreach ($rs as $n) {
                // make csvparams to array
                if(!empty($n['params'])){
                    $params = explode(';;;', $n['params']);
                    foreach ($params as $array) {
                        $keyValue = explode('=>', $array);
                        $key = $keyValue[0];
                        if (empty($keyValue[1])) {
                            $value = $this->ts('source of notification unknown');
                        } else {
                            $value = $keyValue[1];
                        }

                        $paramArray[$key] = $value;
                    }
                } else {
                    $paramArray = array();
                }
                if($n['category'] == 'newarticle'){
                     $n['message'] = $n['message'];
                }else{
                     $n['message'] = $this->ts($n['message'], $paramArray);
                }
                    
                $n['link'] = Config::get('reLangUrl') . $n['link'];
                $n['time'] = date($this->ts('Y-m-d H:i'), strtotime($n['time']));

                $n['unreadClass'] = '';
                if ($n['read'] == 0) {
                    $n['unreadClass'] = 'unread';
                }

                $rsUser = $this->select('user', 'image,nick', "id='" . $n['from_user_id'] . "'");
                if ($rsUser[0]['image'] == "") {
                    $rsUser[0]['image'] = Config::get('relativeUrl') . "theme/" . Config::get("siteStyle") . "/images/profil.png";
                } else {
                    $rsUser[0]['image'] = Config::get('relativeUrl') . $rsUser[0]['image'];
                }

                $n['fromUser'] = $rsUser[0];
                $notificationsHtml .= $this->fetch('notification', $n);
            }

            if ($page == 1 && $limitCount == 5) {
                $vars['notificationsLink'] = 'notifications?getNotificationsHTML=1&page=' . $page;
            } else {
                $vars['showMarkAllAsRead'] = true;
                if (count($rs) > $limitCount) {
                    $vars['notificationsLink'] = 'notifications?getNotificationsHTML=1&page=' . ($page + 1);
                } else {
                    // nothing
                }
            }


            $vars['notifications'] = $notificationsHtml;
            $this->view('notifications', $vars);
        }
    }

    private function getUnreadNotificationsCount() {
        /**
         * TODO :return JSON info about unread notifications 
         */
        $userNotificationTable = 'notifications_' . Session::get('uid');


        if (!$this->tableExists($userNotificationTable)) {
            return '0';
        } else {
            return $this->count($userNotificationTable, "`read`='0'");
//            $rs = $this->select($userNotificationTable, '*', 'read=0', 'time DESC', "0,20");
//            return json_encode($rs);
        }
    }

//    private function getLatestNotificationsJSON($unreadOnly=true){
//        if($unreadOnly){
//            $this->select($table, $fields, $where, $orderBy, '0,20');
//        }else{
//            
//        }
//    }

    /**
     * 
     * @param type $fromUserId
     * @param type $notifyCategory
     * @param type $subject
     * @param type $message
     */
//    public function notify($fromUserId, $notifyCategory, $subject, $message, $link) {
//        
//        $fromNick = Session::get('unick');
//
//        $subject = str_replace('$2', $fromNick, $subject);
//
//        $fieldsValueArray['user_id'] = $fromUserId;
//        $fieldsValueArray['text'] = "<a href='$link'>" . $subject . "</a>";
//
//        $this->insert('notifications', $fieldsValueArray);
//        $rs = $this->select('user as u, ' . Config::get('dbPrefix') . 'notifications_settings as ns'
//                , 'u.id, u.email, u.nick, ns.newentry'
//                , "u.id=ns.user_id AND ns.$notifyCategory=1 AND u.id!='$fromUserId'");
//
//        foreach ($rs as $r) {
//
//            $message = str_replace('$2', $fromNick, str_replace('$1', $r['nick'], $message));
//
//            mail($r['email'], $subject, $message);
//        }
//    }


    private function createSettings($userId) {
        $fieldsValueArray = array();
        $fieldsValueArray['user_id'] = $userId;
        $this->insert('notifications_settings', $fieldsValueArray);
        return;
    }

    private function saveSettings() {

        $userId = Session::get('uid');

        $fieldsValueArray = array();
        $sql = "SHOW COLUMNS FROM " . Config::get('dbPrefix') . "notifications_settings";
        $result = $this->query($sql, false); //"SHOW COLUMNS FROM `".Config::get('dbPrefix')."notifications_settings`",true);
        foreach (mysqli_fetch_all($result) as $cat) {
            $category = $cat[0];
            if ($category === 'id' || $category === 'user_id') {
                continue;
            }
            $postVar = filter_input(INPUT_POST, $category);
            if ($postVar == 'on') {
                $postVar = 1;
            } else {
                $postVar = 0;
            }
            $fieldsValueArray[$category] = $postVar;
        }

        if ($this->updatePrepare('notifications_settings', $fieldsValueArray, "user_id=:id", array('id' => $userId))) {
            $this->setSystemMessage($this->ts('Settings saved successfully.'));
        }
    }

}
