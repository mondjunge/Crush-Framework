<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'contact writes you a chat message, but you are offline.'=>
	'contact writes you a chat message, but you are offline.'
	,
	'save'=>
	'save'
	,
	'upon which events do you want to get notified by E-Mail?'=>
	'upon which events do you want to get notified by E-Mail?'
	,
	'your accounts e-mail address will be used to send you an E-Mail if following events occure...'=>
	'your accounts e-mail address will be used to send you an E-Mail if following events occure...'
	,
	'your accounts e-mail address will be used to send you an E-Mail, if one of the following events occure:'=>
	'your accounts e-mail address will be used to send you an E-Mail, if one of the following events occure:'
	,
	'contact creates a new event.'=>
	'contact creates a new event.'
	,
	'Settings saved successfully.'=>
	'Settings saved successfully.'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'What was happening?'=>
	'What was happening?'
	,
	'Y-m-d H:i'=>
	'Y-m-d H:i'
	,
	'New comment from $1'=>
	'New comment from $1'
	,
	'$1 created a new entry'=>
	'$1 created a new entry'
	,
	'more...'=>
	'old notifications...'
	,
	'$1 commented an entry you also commented'=>
	'$1 commented an entry you also commented'
	,
	'$1 commented your entry'=>
	'$1 commented your entry'
	,
	'no notifications...'=>
	'no notifications...'
	,
	'mark all notifications as read'=>
	'mark all notifications as read'
	,
	'$1 inserted a new event'=>
	'$1 inserted a new event'
	,
	'You will only recive an E-Mail if there is no unread notification for you or if your oldest notification is older than a month, to prevent mailbox spamming.'=>
	'You will only recive an E-Mail a day, as long as you have unread notifications.'
	,
	'Nothing found. Sorry.'=>
	'Nothing found. Sorry.'
	,
	'source of notification unknown'=>
	'source of notification unknown'
	,
	'key not set!'=>
	'key not set!'
	,
	'No current notifications.'=>
	'No current notifications.'
);