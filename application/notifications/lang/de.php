<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'save'=>
	'Einstellungen speichern'
	,
	'upon which events do you want to get notified by E-Mail?'=>
	'Bei welchen Ereignissen möchtest Du per E-Mail benachrichtigt werden?'
	,
	'your accounts e-mail address will be used to send you an E-Mail, if one of the following events occure:'=>
	'Es wird eine E-Mail an die Adresse Deines Kontos verschickt, wenn eines der folgenden Ereignisse eintritt:'
	,
	'Settings saved successfully.'=>
	'Einstellungen wurden erfolgreich gespeichert.'
	,
	'Insufficient rights!'=>
	'Ungenügende Rechte'
	,
	'What was happening?'=>
	'die letzten Ereignisse'
	,
	'Y-m-d H:i'=>
	'd.m.Y H:i'
	,
	'New comment from $1'=>
	'$1 hat ein Kommentar abgegeben'
	,
	'more...'=>
	'alte Benachrichtigungen...'
	,
	'$1 commented your entry'=>
	'$1 hat Deinen Beitrag kommentiert'
	,
	'$1 commented an entry you also commented'=>
	'$1 hat einen Beitrag kommentiert den Du auch kommentiert hast'
	,
	'$1 created a new entry'=>
	'$1 hat einen neuen Beitrag erstellt'
	,
	'no notifications...'=>
	'keine neuen Benachrichtigungen...'
	,
	'mark all notifications as read'=>
	'Alle als gelesen markieren'
	,
	'$1 inserted a new event'=>
	'$1 hat einen neuen Termin angelegt'
	,
	'You will only recive an E-Mail if there is no unread notification for you or if your oldest notification is older than a month, to prevent mailbox spamming.'=>
	'Du bekommst maximal eine Benachrichtigungsmail pro Tag, solange Du ungelesene Benachrichtigungen hast.'
	,
	'source of notification unknown'=>
	'Quelle der Benachrichtigung nicht bekannt.'
	,
	'key not set!'=>
	'Schlüssel nicht gesetzt!'
	,
	'No current notifications.'=>
	'Keine aktuellen Benachrichtigungen.'
);