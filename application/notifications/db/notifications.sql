CREATE TABLE IF NOT EXISTS `{dbprefix}notifications` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL, /* Who created notification , not user of entry_id! */
  `entry_user_id` bigint NOT NULL,
  `entry_id` bigint NOT NULL,
  `category` varchar(191) NOT NULL,
  `text` text NOT NULL,
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`)
);