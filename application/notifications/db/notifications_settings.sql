CREATE TABLE IF NOT EXISTS `{dbprefix}notifications_settings` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  PRIMARY KEY (`id`)
);