<!-- 

- Kontakt schreibt neuen Beitrag
- Jemand kommentiert eigenen Beitrag
- Jemand kommentiert fremden Beitrag den man zuvor auch kommentiert hat
- Man wird im Chat angeschrieben.
- neuer Event wird eingetragen

Optional
- neuer Kontakt

-->
<h1><?php echo $this->ts('upon which events do you want to get notified by E-Mail?');?></h1>
<div class="help"><?php echo $this->ts('your accounts e-mail address will be used to send you an E-Mail, if one of the following events occure:') ?></div>

<form class="pure-form pure-form-aligned" method="POST">
    
    <?php echo $moduleSettings ?>
    
    
    <br/>
    <div class="help"><?php echo $this->ts('You will only recive an E-Mail if there is no unread notification for you or if your oldest notification is older than a month, to prevent mailbox spamming.') ?></div>

    
    <div class="pure-controls">
        
        <button id="save" type="submit" name="save" class="pure-button pure-button-primary" ><?php echo $this->ts('save') ?></button> 
    </div>
    
    
    
</form>