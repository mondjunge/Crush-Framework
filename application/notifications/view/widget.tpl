<h2><?php echo $this->ts('What was happening?'); ?></h2>
<?php foreach ($notifications as $n) : ?>
    <p>
        <span class="time floatRight"><?php echo date($this->ts('Y-m-d H:i'), strtotime($n['time'])); ?></span>
        
        <span><?php echo $n['text'] ?></span>
        
    </p>
<div style="clear:both"></div>

<?php endforeach; ?>
