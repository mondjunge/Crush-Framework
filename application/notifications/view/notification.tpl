<div class="pure-g pure-g-r notification <?php echo $unreadClass; ?>">
    <a class="notificationLink" style="width:100%" data-notification-id="<?php echo $id; ?>" 
       href="<?php echo $this->action('notifications/?id='.$id.'&viewNotificationSrc')."=".urlencode($link) ?>">
        <div class="pure-u-1-5" >
            <?php echo $this->image($fromUser['image'], '', "", "", $fromUser['nick']); ?>
        </div>
        <div class="pure-u-4-5">
    <!--        <div class="nick"><?php echo $fromUser['nick']; ?></div>-->
            <div class="notificationMessage">
                <?php echo $message ?>
            </div>
            <span class="time"><?php echo $time; ?></span>
        </div>
    </a>
</div>