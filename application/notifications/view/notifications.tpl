<div class="notificationsBox">
    
    <?php if(empty($notifications)) : ?>
        <?php echo $this->ts('No current notifications.'); ?>
    <?php else : ?>
        <?php if (true || isset($showMarkAllAsRead) && $showMarkAllAsRead == true) : ?>
            <a id="markAllNotificationsAsRead" class="pure-button pure-button-primary" href="<?php echo $this->action('notifications?getNotificationsHTML=1&page=1&markAllAsRead'); ?>" ><?php echo $this->ts('mark all notifications as read'); ?></a>
        <?php endif; ?>
        <?php echo $notifications; ?>
        
    <?php endif; ?>
    <?php if (isset($notificationsLink)) : ?>
        <p>
        <a class="linkTo" href="<?php echo $this->action($notificationsLink); ?>"><?php echo $this->ts('more...'); ?></a>
        </p>
    <?php endif; ?>


</div>