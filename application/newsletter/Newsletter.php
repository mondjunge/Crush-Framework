<?php

/**
 *  Copyright © tim 23.10.2014
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Newsletter extends AppController {

    public function index() {
        //put your code here
        $this->autoCheckRights();
        
        if(filter_has_var(INPUT_POST, 'sendNewsletter')){
            $this->sendNewsletter();
        }
        
        $this->includeCkeditor('javascript/ck_config/ck_newsletter.js');
        
        $data = array();
        $users = $this->select('user', 'nick, email', "active=1 AND email != ''");
        $data['users'] = $users;
        $this->view('writeNL', $data);
    }

    private function sendNewsletter(){
        
        $postArray = filter_input_array(INPUT_POST); 
        $subject = filter_input(INPUT_POST, 'subject');
        $message = filter_input(INPUT_POST, 'text');
        
        foreach($postArray['to'] as $to){
            if(!$this->mail_utf8($to, $subject, $message)){
                $failed[] = $to;
            }
        }
        $this->setSystemMessage($this->ts('Newsletter was send.'));
        if(isset($failed) && !empty($failed)){
            $this->setErrorMessage($this->ts('Newsletter could not be send to:'));
            foreach($failed as $rzp){
                $this->setErrorMessage("<br>".$rzp."");
            }
        }
        
    }

}
