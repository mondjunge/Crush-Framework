<h2><?php echo $this->ts("Newsletter");?></h2>
<div class="help"><?php echo $this->ts("Write an E-Mail to all users");?></div>
<form class="pure-form pure-form-stacked" action="<?php $this->action('newsletter?send'); ?>" method="POST">
    <fieldset>

        <label for="to"><?php echo $this->ts('To'); ?></label>
        <select id="to" name="to[]" multiple="multiple">
            <?php foreach ($users as $u) : ?>
                <option selected="selected" value="<?php echo $u['email']; ?>"><?php echo $u['nick'] . " - " . $u['email']; ?></option>
            <?php endforeach; ?>
        </select>
        
        <input class="pure-u-1" type="text" name="subject" placeholder="<?php echo $this->ts('Subject'); ?>" />
        <textarea id="text" name="text" class="text">
            Liebe Nutzer,
            <br/>
            <br/>
            
            Viele Grüße<br/>
            Euer Admin</textarea>

        <button class="pure-button pure-button-primary" type="submit" id="sendNewsletter" name="sendNewsletter"><?php echo $this->ts("send Newsletter"); ?></button>
    </fieldset>
</form>