<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Subject' =>
	'Subject'
	,
	'To'=>
	'To'
	,
	'send Newsletter'=>
	'send Newsletter'
	,
	'Write an E-Mail to all users'=>
	'Write an E-Mail to all users'
	,
	'Newsletter'=>
	'Newsletter'
	,
	'Newsletter was send.'=>
	'Newsletter was send.'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'Newsletter could not be send to:'=>
	'Newsletter could not be send to:'
);