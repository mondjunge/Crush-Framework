<?php

/**
 *  Copyright © tim 26.03.2018
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Projects extends AppController {

    private $userTable;

    public function __construct() {
        $this->userTable = "projects_" . Session::get('uid');
    }

    public function index() {
        $this->autoCheckRights();
        //put your code here
        $this->createUserTable();
        if (filter_has_var(INPUT_POST, 'addProject')) {
            $this->view('addProject');
            return;
        }
        if (filter_has_var(INPUT_POST, 'addChild')) {
            $this->includeCkeditor("javascript/ck_config/ck_default.js");
            $data['parentId'] = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
            $this->view('addChild', $data);
            return;
        }
        if (filter_has_var(INPUT_POST, 'taskDone')) {
            $this->standalone = true;
            return $this->setTaskDone();
        }
        if (filter_has_var(INPUT_POST, 'updateNote')) {
            $this->standalone = true;
            return $this->updateNote();
        }
        
        if (filter_has_var(INPUT_POST, 'insertProject')) {
            $this->insertProject();
            $this->jumpTo('projects');
        }
        
        if (filter_has_var(INPUT_POST, 'updateProject')) {
            $this->updateProject();
            $this->jumpTo('projects');
        }
        
        if (filter_has_var(INPUT_POST, 'deleteProject')) {
            $this->deleteProject(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT));
            $this->jumpTo('projects');
        }
        if (filter_has_var(INPUT_POST, 'deleteTask')) {
            $this->standalone = true;
            $this->deleteProject(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT));
            return;
        }
        if (filter_has_var(INPUT_GET, 'editProject')) {
            $this->editProject();
            return;
        }

        $data = array();
        $data['projects'] = $this->getUserProjects();

        $this->view('projects', $data);
    }

    public function admin() {
        //put your administrative code here
    }

    private function createUserTable() {
        if (!$this->tableExists($this->userTable)) {
            $sql = "CREATE TABLE IF NOT EXISTS "
                    . "`" . $this->getConfig('dbPrefix') . $this->userTable . "` ("
                    . "`id` BIGINT NOT NULL AUTO_INCREMENT,"
                    . "`parent_id` BIGINT NULL,"
                    . "`name` varchar(191) NOT NULL,"
                    . "`description` text NULL,"
                    . "`start` datetime NOT NULL,"
                    . "`end` datetime NOT NULL,"
                    . "`done` tinyint(1) NOT NULL DEFAULT '0',"
                    . "`type` VARCHAR(1) NOT NULL DEFAULT 'p',"
                    . "`rank` BIGINT NULL DEFAULT '100',"
                    . "PRIMARY KEY (`id`)"
                    . ");";
            $this->query($sql);
        } else {
            /**
             * DONE: update table: add rank and update id int(11), parent_id to BIGINT
             */
            $result = $this->query("SHOW COLUMNS FROM `" . Config::get('dbPrefix') . $this->userTable . "` LIKE 'rank'", true);
            if ($result == null || count($result) === 0) {
                $sql = "ALTER TABLE " . Config::get('dbPrefix') . $this->userTable . " ADD `rank` BIGINT NULL DEFAULT '100';";
                $this->query($sql);
                $sql = "ALTER TABLE " . Config::get('dbPrefix') . $this->userTable . " CHANGE `id` `id` BIGINT NOT NULL AUTO_INCREMENT;";
                $this->query($sql);
                $sql = "ALTER TABLE " . Config::get('dbPrefix') . $this->userTable . " CHANGE `parent_id` `parent_id` BIGINT NULL;";
                $this->query($sql);
            }
        }
    }
    
    private function editProject(){
        $data = array();
        $id = filter_input(INPUT_GET, 'editProject', FILTER_SANITIZE_NUMBER_INT);
        //echo $id;
        $project = $this->select($this->userTable, '*', "id=$id");
        $data['project'] = $project[0];
        $data['project']['name'] = htmlspecialchars($data['project']['name']);
        $this->view('editProject',$data);
    }
    
    private function updateProject(){
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $name = filter_input(INPUT_POST, 'name', FILTER_UNSAFE_RAW);
        //$parentId = filter_input(INPUT_POST, 'parent_id', FILTER_SANITIZE_NUMBER_INT);
        $description = $this->parseDescription(filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRIPPED));
        $start = filter_input(INPUT_POST, 'start', FILTER_UNSAFE_RAW);
        $end = filter_input(INPUT_POST, 'end', FILTER_UNSAFE_RAW);
        //$done = filter_input(INPUT_POST, 'done', FILTER_SANITIZE_NUMBER_INT);
        $type = filter_input(INPUT_POST, 'type', FILTER_UNSAFE_RAW);

        $fieldsValueArray = array(
            'name' => $name,
            'description' => $description,
            'start' => $start,
            'end' => $end,
            //'done' => $done,
            'type' => $type
        );
        
        if (!$this->updatePrepare($this->userTable, $fieldsValueArray, "id=:id", array('id' => $id))) {
            $this->setErrorMessage($this->ts('An Error occured while updating the project!'));
        } else {
            $this->setSystemMessage($this->ts('Project changed.'));
        }
    }
    
    private function updateNote(){
        $id = $this->escapeString(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT));
        $noteText = filter_input(INPUT_POST, 'noteText', FILTER_UNSAFE_RAW);
        $fieldsValueArray = array(
            'description' => "$noteText"
        );
        if (!$this->updatePrepare($this->userTable, $fieldsValueArray, "id=:id", array('id' => $id))) {
            $this->setErrorMessage($this->ts('An Error occured while updating the Note!'));
        } else {
            $this->setSystemMessage($this->ts('Note changed.'));
        }
    }

    private function setTaskDone() {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $done = filter_input(INPUT_POST, 'taskDone', FILTER_SANITIZE_NUMBER_INT);
        $fieldsValueArray = array(
            'done' => "$done"
        );
        if (!$this->updatePrepare($this->userTable, $fieldsValueArray, "id=:id", array('id' => $id))) {
            $this->setErrorMessage($this->ts('An Error Occured while marking the Task!'));
        } else {
            if ($done == 1) {
                $this->setSystemMessage($this->ts('Task marked as done.'));
            } else {
                $this->setSystemMessage($this->ts('Task marked as undone.'));
            }
        }
    }

    private function deleteProject($projectId) {
        if ($this->delete($this->userTable, "id=".$this->escapeString($projectId))) {
            $this->setSystemMessage($this->ts('Deletion successful!'));
        } else {
            $this->setErrorMessage($tis->ts('Deletion not possible!'));
        }
    }

    private function insertProject() {
        $name = filter_input(INPUT_POST, 'name', FILTER_UNSAFE_RAW);
        $parentId = filter_input(INPUT_POST, 'parent_id', FILTER_SANITIZE_NUMBER_INT);
        $description = $this->parseDescription(filter_input(INPUT_POST, 'description', FILTER_UNSAFE_RAW));
        $start = filter_input(INPUT_POST, 'start', FILTER_UNSAFE_RAW);
        $end = filter_input(INPUT_POST, 'end', FILTER_UNSAFE_RAW);
        $done = filter_input(INPUT_POST, 'done', FILTER_SANITIZE_NUMBER_INT);
        $type = filter_input(INPUT_POST, 'type', FILTER_UNSAFE_RAW);

        $fieldsValueArray = array(
            'name' => $name,
            'parent_id' => $parentId,
            'description' => $description,
            'start' => $start,
            'end' => $end,
            'done' => $done,
            'type' => $type
        );
        $newId = $this->insert($this->userTable, $fieldsValueArray);
        if (!$newId) {
            $this->setErrorMessage($this->ts("An error occured while creating the new Element."));
        } else {
            if ($parentId == 0) {
                $this->setSystemMessage($this->ts("New Project was created successfully!"));
            } else {
                $this->setSystemMessage($this->ts("New Element was created successfully!"));
            }
        }
    }

    private function parseDescription($des) {
        $str = "";
        if(strpos($des, "*") !== false){
            //$str .= "<ul>";
            foreach (explode(PHP_EOL, $des) as $line){
                if(strpos($line,"*") === 0){
                    $str .= "<ul><li>".str_replace("*", "<input type='checkbox' />", $line)."</li></ul>";
                } else {
                    $str .= "<div>". $line ."</div>";
                }

            }
            //$str .= "</ul>";
        } else {
            $str = $des;
        }
        return $str;
//        $des = str_replace("-", "<input type='checkbox' checked='checked' />", $des);
//        $des2 = str_replace("[ ]", "<br/><input type='checkbox' />", $des);
//        $des3 = str_replace("[]", "<br/><input type='checkbox' />", $des2);
//        return str_replace("[x]", "<input type='checkbox' checked='checked'/>", $des3);
    }
    private function decodeDescription($des) {
        return parseDescription($des);
    }
    
    private function encodeDescription($des) {
        $des3 = str_replace( "<input type='checkbox' />", "[]", $des);
        return str_replace( "<input type='checkbox' checked='checked' />", "[x]", $des3);
    }

    private function getUserProjects() {
        $returnArray = array();
        $projects = $this->select($this->userTable, '*', "type='p' AND (parent_id IS NULL OR parent_id='')", "`rank` ASC, end ASC");
        foreach ($projects as $p) {
            $p['children'] = $this->getProjectChildren($p['id']);
            $returnArray[] = $p;
        }
        return $returnArray;
    }

    private function getProjectChildren($projectId) {
        return array_merge($this->getProjectTasks($projectId), $this->getProjectNotes($projectId));
    }

    private function getProjectTasks($projectId) {
        return $this->select($this->userTable, '*', "type='t' AND parent_id='$projectId'", "`rank` ASC, end ASC, id DESC");
    }

    private function getProjectNotes($projectId) {
        return $this->select($this->userTable, '*', "type='n' AND parent_id='$projectId'", "`rank` ASC, end ASC, id DESC");
    }

//    private function getProjectLists($projectId) {
//        $returnArray = array();
//        $lists = $this->select($this->userTable, '*', "type='l' AND parent_id='$projectId'", "end ASC, id ASC");
//        foreach ($lists as $l) {
//            $l['tasks'] = $this->getListTasks($l['id']);
//            $returnArray[] = $l;
//        }
//        return $returnArray;
//    }
//
//    private function getListTasks($listId) {
//        return $this->select($this->userTable, '*', "type='t' AND parent_id='$listId'", "end ASC");
//    }

}
