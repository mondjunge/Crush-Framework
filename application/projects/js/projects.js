/**
 * projects.js
 */

use_package('modules');
modules.projects = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);
    var self = this;
    this.init = function () {
        self.checkboxHandler();
        self.deleteTaskHandler();
        self.typeChangeHandler();
    };
    this.typeChangeHandler = function () {
        $('select[name="type"]').change(function () {
            if ($(this).val() === 'n') {
                $('#noteDescription').show();
            } else {
                $('#noteDescription').hide();
            }
        });
    };
    
    this.editTaskHandler = function () {
        
    };

    this.deleteTaskHandler = function () {
        $(".deleteTask").click(function () {
            var confirmBool = utils.check.deleteCheck($(this).data('deletetext'));
            if (confirmBool) {
                self.deleteTask($(this).parent().data('id'));
            }

            return false;
        });
    };
    this.deleteTask = function (id) {
        $.ajax({
            type: "POST",
            url: reLangUrl + "projects",
            async: true,
            timeout: 5000,
            data: {
                deleteTask: "true",
                id: id
            },
            success: function (html) {
                $('#element_' + id).remove();
                $('#content').append(html);
            },
            error: function () {

            }
        });
    };

    this.checkboxHandler = function () {
        $("input[type='checkbox']").change(function () {


            if ($(this).parent().hasClass('task')) {
                var checked = 1;
                if (!$(this).is(":checked")) {
                    checked = 0;
                }
                var id = $(this).parent().data('id');
                self.setTaskDone(id, checked);
            } else {
                // this is a checkbox in notes, update notes description
                if ($(this).is(":checked")) {
                    $(this).attr('checked', 'checked');
                    $(this).parent().addClass('elementDone');
                } else {
                    $(this).removeAttr('checked');
                    $(this).parent().removeClass('elementDone');
                }
                
                
                var noteText = $(this).parents(".noteText:first").html();
                var id = $(this).parents(".note:first").data('id');
                
                console.log("id:", id);
                console.log("noteText:", noteText);
                self.updateNoteDescription(id, noteText);

            }

        });
    };

    this.updateNoteDescription = function (id, noteText) {
        $.ajax({
            type: "POST",
            url: reLangUrl + "projects",
            async: true,
            timeout: 5000,
            data: {
                updateNote: 'true',
                id: id,
                noteText: noteText
            },
            success: function (html) {
                //$('#element_' + id).toggleClass('elementDone');
                $('#content').append(html);
            },
            error: function () {

            }
        });
    };

    this.setTaskDone = function (id, done) {
        // alert(done);
        $.ajax({
            type: "POST",
            url: reLangUrl + "projects",
            async: true,
            timeout: 5000,
            data: {
                taskDone: done,
                id: id
            },
            success: function (html) {
                $('#element_' + id).toggleClass('elementDone');
                $('#content').append(html);
            },
            error: function () {

            }
        });
    };
//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


};