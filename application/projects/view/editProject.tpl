<a class="pure-button" href="<?php echo $this->action("projects"); ?>" ><?php echo $this->ts("back"); ?></a>
<form class="pure-form pure-form-aligned" action="<?php $this->action('projects'); ?>" method="POST" >
    <fieldset>
        <input type="hidden" name="id" value="<?php echo $project['id'];?>" />
        <input type="hidden" name="type" value="<?php echo $project['type'];?>" />
        <input type="hidden" name="description" value="" />
        <input type="hidden" name="done" value="0" />
        <div class="pure-control-group">
            <label style="width: 13em;" for="name" ><?php echo $this->ts('Project Name') ?></label>
            <input id="name" name="name" value="<?php echo $project['name'];?>" type="text" maxlength="255"/>
            <div class="help help-margin"><?php echo $this->ts('projectName_help') ?></div>
        </div>
        <div class="pure-control-group">
            <label style="width: 13em;" for="start" ><?php echo $this->ts('Start Date') ?></label>
            <input id="start" name="start" value="<?php echo date("Y-m-d",date_create_from_format("Y-m-d H:i:s",$project['start'])->getTimestamp()) ?>" type="date" />
            <div class="help help-margin"><?php echo $this->ts('start_help') ?></div>
        </div>
         <div class="pure-control-group">
            <label style="width: 13em;" for="end" ><?php echo $this->ts('End Date') ?></label>
            <input id="end" name="end" value="<?php echo date("Y-m-d",date_create_from_format("Y-m-d H:i:s",$project['end'])->getTimestamp()) ?>" type="date" />
            <div class="help help-margin"><?php echo $this->ts('end_help') ?></div>
        </div>
        <div class="pure-control-group">
            <button name="updateProject" class="pure-button pure-button-primary"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#check', 'svgIcon svgIcon-light svgIcon-small'); echo " ".$this->ts('Update'); ?></button>
            <button name="deleteProject" onclick="return utils.check.deleteCheck('<?php echo $this->ts("Do you really want to delete this project forever?"); ?>')" class="pure-button pure-button-error floatRight"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-light svgIcon-small'); ?></button>
            <button class="pure-button" type="cancel" ><?php echo $this->ts('Cancel') ?></button>
        </div>
    </fieldset>
</form>