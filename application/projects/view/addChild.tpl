<a class="pure-button" href="<?php echo $this->action("projects"); ?>" ><?php echo $this->ts("back"); ?></a>
<form class="pure-form pure-form-aligned" action="<?php $this->action('projects'); ?>" method="POST" >
    <fieldset>
        <input type="hidden" name="parent_id" value="<?php echo $parentId; ?>" />
<!--        <input type="hidden" name="description" value="" />-->
        <input type="hidden" name="done" value="0" />
        <div class="pure-control-group">
            <label style="width: 13em;" for="type" ><?php echo $this->ts('Type') ?></label>
            <div class="pure-u-1-2">
                <select name="type" size='2' style="height:auto;">
                    <?php //foreach ($configNames as $confName): ?>

                    <option selected="selected" label="<?php echo $this->ts('Task') ?>" value="t" ><?php echo $this->ts('Task') ?></option>
                    <option label="<?php echo $this->ts('Note') ?>" value="n" ><?php echo $this->ts('Note') ?></option>
<!--                    <option label="<?php echo $this->ts('List') ?>" value="l" ><?php echo $this->ts('List') ?></option>-->
                    <?php //endforeach; ?>>
                </select>
            </div>
            <div class="help help-margin"><?php echo $this->ts('type_help') ?></div>

        </div>
        
        <div class="pure-control-group">
            <label style="width: 13em;" for="name" ><?php echo $this->ts('Elements Name') ?></label>
            <input id="name" name="name" value="" type="text" maxlength="255"/>
            <div class="help help-margin"><?php echo $this->ts('elementName_help') ?></div>
        </div>
        <div id="noteDescription" class="pure-control-group" >

            <label style="width: 13em;" for="description" ><?php echo $this->ts('Notes Text') ?></label>
            <div style="display: inline-block;">
                <textarea id="text" name="description" class="" value="" type="text" ></textarea>
            </div>
            <div class="help help-margin"><?php echo $this->ts('elementDescription_help') ?></div>
        </div>
        <div class="pure-control-group">
            <label style="width: 13em;" for="start" ><?php echo $this->ts('Start Date') ?></label>
            <input id="start" name="start" value="<?php echo date("Y-m-d") ?>" type="date" />
            <div class="help help-margin"><?php echo $this->ts('start_help') ?></div>
        </div>
        <div class="pure-control-group">
            <label style="width: 13em;" for="end" ><?php echo $this->ts('End Date') ?></label>
            <input id="end" name="end" value="<?php echo (1 + date("Y") . date("-m-d")) ?>" type="date" />
            <div class="help help-margin"><?php echo $this->ts('end_help') ?></div>
        </div>

        <button type="submit" name="insertProject" class="pure-button pure-button-primary"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#plus', 'svgIcon svgIcon-light svgIcon-small');
                    echo " " . $this->ts('Create'); ?></button>
        <button class="pure-button" type="cancel" ><?php echo $this->ts('Cancel') ?></button>
    </fieldset>
</form>
