<a class="pure-button" href="<?php echo $this->action("projects"); ?>" ><?php echo $this->ts("back"); ?></a>
<form class="pure-form pure-form-aligned" action="<?php $this->action('projects'); ?>" method="POST" >
    <fieldset>
        <input type="hidden" name="parent_id" value="" />
        <input type="hidden" name="type" value="p" />
        <input type="hidden" name="description" value="" />
        <input type="hidden" name="done" value="0" />
        <div class="pure-control-group">
            <label style="width: 13em;" for="name" ><?php echo $this->ts('Project Name') ?></label>
            <input id="name" name="name" value="" type="text" maxlength="255"/>
            <div class="help help-margin"><?php echo $this->ts('projectName_help') ?></div>
        </div>
        <div class="pure-control-group">
            <label style="width: 13em;" for="start" ><?php echo $this->ts('Start Date') ?></label>
            <input id="start" name="start" value="<?php echo date("Y-m-d") ?>" type="date" />
            <div class="help help-margin"><?php echo $this->ts('start_help') ?></div>
        </div>
         <div class="pure-control-group">
            <label style="width: 13em;" for="end" ><?php echo $this->ts('End Date') ?></label>
            <input id="end" name="end" value="<?php echo (1 + date("Y") . date("-m-d")) ?>" type="date" />
            <div class="help help-margin"><?php echo $this->ts('end_help') ?></div>
        </div>
        <div class="pure-control-group">
            <button name="insertProject" class="pure-button pure-button-primary"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#plus', 'svgIcon svgIcon-light svgIcon-small'); echo " ".$this->ts('Create'); ?></button>
            <button class="pure-button" type="cancel" ><?php echo $this->ts('Cancel') ?></button>
        </div>
    </fieldset>
</form>