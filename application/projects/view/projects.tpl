<form action="<?php echo $this->action('projects'); ?>" method="POST" >
    <button name="addProject" class="pure-button pure-button-primary"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#plus', 'svgIcon svgIcon-light'); ?></button>
</form>
<div class="pure-g pure-g-r">
    <?php foreach ($projects as $p) : ?>
        <form class="pure-form pure-u-1-3 projectForm" action="<?php echo $this->action('projects'); ?>" method="POST" >
            <div class="projectContainer">
                <input type="hidden" name="id" value="<?php echo $p['id'] ?>" />
                <input type="hidden" name="parent_id" value="" />
                <input type="hidden" name="type" value="p" />
                <input type="hidden" name="description" value="<?php echo $p['description'] ?>" />
                <input type="hidden" name="done" value="<?php echo $p['done'] ?>" />
                <div class="projectElement pure-u-1">
<!--                    <button name="deleteProject" onclick="return utils.check.deleteCheck('<?php echo $this->ts("Do you really want to delete this project forever?"); ?>')" class="pure-button pure-button-error floatRight"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-light svgIcon-small'); ?></button>-->
                    <h3><a href="<?php echo $this->action('projects')."?editProject=".$p['id']; ?>"><?php echo $p['name'] ?></a></h3>
                    <?php foreach ($p['children'] as $c) : ?>
                        <?php if ($c['type'] == 't'): ?>
                            <div id="element_<?php echo $c['id']; ?>" class="element task boxBorder <?php echo ($c['done'] ? 'elementDone' : ''); ?>" data-id="<?php echo $c['id']; ?>" >
                                <a class="deleteTask" href="" data-deletetext="<?php echo $this->ts("Do you really want to delete this element forever?"); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-dark svgIcon-small floatRight'); ?></a>
                                <input id="task_<?php echo $c['id']; ?>" name="task_<?php echo $c['id']; ?>" <?php echo ($c['done'] ? 'checked="checked"' : ''); ?> type="checkbox" />
                                <label for="task_<?php echo $c['id']; ?>" ><?php echo $c['name']; ?></label>
                            </div>
                        <?php endif; ?>
                        <?php if ($c['type'] == 'n'): ?>
                            <div id="element_<?php echo $c['id']; ?>" class="element note boxBorder <?php echo ($c['done'] ? 'elementDone' : ''); ?>" data-id="<?php echo $c['id']; ?>">
                                <a class="editTask" href="" ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#pencil', 'svgIcon svgIcon-dark svgIcon-small floatRight'); ?></a>
                                <a class="deleteTask" href="" data-deletetext="<?php echo $this->ts("Do you really want to delete this element forever?"); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-dark svgIcon-small floatRight'); ?></a>
                                <h4><?php echo htmlspecialchars($c['name']); ?></h5>
                                <div class="noteText"><?php echo htmlspecialchars($c['description']); ?></div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <button name="addChild" class="pure-button pure-button-primary floatRight addChildButton"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#plus', 'svgIcon svgIcon-light svgIcon-small'); ?></button>
                </div>
            </div>
        </form>
    <?php endforeach; ?>
</div>
