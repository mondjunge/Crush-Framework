<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Project Name' =>
	'Projektname'
	,
	'projectName_help'=>
	'Vergebe einen gut wiedererkennbaren Namen für dein Projekt.'
	,
	'Create'=>
	'Erstellen'
	,
	'Start Date'=>
	'Startdatum'
	,
	'start_help'=>
	'Gib ein Datum an zu dem begonnen wurde/werden soll.'
	,
	'End Date'=>
	'Enddatum'
	,
	'end_help'=>
	'Gib ein Fälligkeits-/Enddatum an.'
	,
	'An error occured while creating the new Entry.'=>
	'Ein Fehler ist beim Erzeugen des neuen Eintrags aufgetreten. Entschuldigung.'
	,
	'New Element was created successfully!'=>
	'Das neue Element wurde erfolgreich angelegt.'
	,
	'Do you really want to delete this project forever?'=>
	'Möchtest Du dieses Projekt, mit allen Aufgaben, für immer löschen? (was eine sehr lange Zeit ist...) '
	,
	'Deletion successful!'=>
	'Erfolgreich gelöscht!'
	,
	'Do you really want to delete this project forever? (and ever?)'=>
	'Möchtest Du dieses Projekt wirklich für immer löschen? (das ist seeehr lange..)'
	,
	'back'=>
	'Zurück'
	,
	'Cancel'=>
	'Abbrechen'
	,
	'Type'=>
	'Typ'
	,
	'Task'=>
	'Aufgabe'
	,
	'Note'=>
	'Notiz'
	,
	'List'=>
	'Liste'
	,
	'type_help'=>
	'Typ/Art des Eintrags. Aufgaben haben eine Checkbox und können als erledigt markiert werden. Notizen beinhalten einen Freitext.'
	,
	'Elements Name'=>
	'Name/Titel des Elements'
	,
	'elementName_help'=>
	'Gib einen Namen/Titel für das Element an. Für Aufgaben ist dies der angezeigte Text. Für Notizen ist dies die Überschrift der Notiz.'
	,
	'elementDescription_help'=>
	'Text für eine Notiz.'
	,
	'Notes Text'=>
	'Text der Notiz'
	,
	'Insufficient rights!'=>
	'Ungenügende Rechte!'
	,
	'Task marked as done.'=>
	'Aufgabe wurde als erledigt markiert!'
	,
	'Task marked as undone.'=>
	'Aufgabe wurde als zu erledigen markiert!'
	,
	'Do you really want to delete this element forever?'=>
	'Möchtest Du dieses Element wirklich für immer löschen? (das ist eine seeehr lange Zeit...)'
	,
	'New Project was created successfully!'=>
	'Das neue Projekt wurde erfolgreich angelegt!'
	,
	'An error occured while creating the new Element.'=>
	'Ein Fehler ist beim anlegen des Elementes aufgetreten!'
	,
	'An Error occured while updating the Note!'=>
	'Ein Fehler ist beim aktualisieren der Notiz aufgetreten!'
	,
	'Note changed.'=>
	'Notiz aktualisiert.'
	,
	'Update'=>
	'Aktulisieren'
	,
	'Project changed.'=>
	'Projekt geändert.'
	,
	'An Error occured while updating the project!'=>
	'Ein Fehler ist beim speichern aufgetreten!'
);