<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Project Name' =>
	'Project Name'
	,
	'_projectName'=>
	'_projectName'
	,
	'projectName_help'=>
	'projectName_help'
	,
	'Create'=>
	'Create'
	,
	'Start Date'=>
	'Start Date'
	,
	'start_help'=>
	'start_help'
	,
	'End Date'=>
	'End Date'
	,
	'end_help'=>
	'end_help'
	,
	'An error occured while creating the new Entry.'=>
	'An error occured while creating the new Entry.'
	,
	'New Element was created successfully!'=>
	'New Element was created successfully!'
	,
	'Do you really want to delete this project forever?'=>
	'Do you really want to delete this project forever?'
	,
	'Deletion successful!'=>
	'Deletion successful!'
	,
	'Do you really want to delete this project forever? (and ever?)'=>
	'Do you really want to delete this project forever? (and ever?)'
	,
	'back'=>
	'back'
	,
	'Cancel'=>
	'Cancel'
	,
	'Type'=>
	'Type'
	,
	'Task'=>
	'Task'
	,
	'Note'=>
	'Note'
	,
	'List'=>
	'List'
	,
	'type_help'=>
	'type_help'
	,
	'Elements Name'=>
	'Elements Name'
	,
	'elementName_help'=>
	'elementName_help'
	,
	'_helpText'=>
	'_helpText'
	,
	'elementDescription_help'=>
	'elementDescription_help'
	,
	'Notes Text'=>
	'Notes Text'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'Task marked as done.'=>
	'Task marked as done.'
	,
	'Task marked as undone.'=>
	'Task marked as undone.'
	,
	'Do you really want to delete this element forever?'=>
	'Do you really want to delete this element forever?'
	,
	'New Project was created successfully!'=>
	'New Project was created successfully!'
	,
	'An error occured while creating the new Element.'=>
	'An error occured while creating the new Element.'
	,
	'An Error occured while updating the Note!'=>
	'An Error occured while updating the Note!'
	,
	'Note changed.'=>
	'Note changed.'
	,
	'Update'=>
	'Update'
	,
	'Project changed.'=>
	'Project changed.'
	,
	'An Error occured while updating the project!'=>
	'An Error occured while updating the project!'
);