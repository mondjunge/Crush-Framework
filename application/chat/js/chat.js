/**
 * chat
 */
function logout() {
    //alert("echo1");
    //alert(""+$.support.onbeforeunload);

    var clientmsg = $("#userMsg").val();
    var room = $("#chatRoom").val();
    $.post("?m=logout", {
        text: clientmsg,
        room: room
    });
    if ($.browser.webkit) {
        return ("Chat wirklich verlassen?");
    }

//        
//    return input

// return "Chat wirklich verlassen?";
//alert("echo!");
}
//window.onbeforeunload = logout;



var firstRun = true;

use_package('modules');
modules.chat = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    this.init = function () {
        getData();

        $("#userMsg").focus();

        //If user submits the form
        $("#chatSend").on("click", function () {
            var clientmsg = $("#userMsg").val();
            var room = $("#chatRoom").val();
            if (clientmsg.length > 0) {
//                $.post("?m=post", {
//                    text: clientmsg,
//                    room: room
//                });
                $.ajax({
                type: "POST",
                url: reLangUrl + "chat/?m=post",
                async: true,
                timeout: 5000,
                data: {
                    text: clientmsg,
                    room: room,
                    m: 'post'
                },
                success: function (e) {
                    console.log("e", e);
                }
            });
            }
            //alert('hey!');
            $("#userMsg").val("").focus();
            return false;
        });
    }

//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


}

function getData() {
    var oldscrollHeight = $("#chatWindow").scrollTop();//attr("scrollHeight") - 20;
    console.log("oldscrollHeight" + oldscrollHeight);
    var room = $("#chatRoom").val();
    var chatUrl = "?m=load&room=" + room;

    if (!firstRun) {
        //alert("something");
        chatUrl = "?m=idle&room=" + room;
    }

    $.ajax({
        type: "POST",
        url: chatUrl,
        async: true,
        timeout: 30000,
        data: "get=true",
        success: function (html) {
            var htmlArr = html.split('<!--USERLIST-->');
            //alert(htmlArr[1]);
            $("#userList").html(htmlArr[1]);
            if (firstRun) {

                $("#chatWindow").html(htmlArr[0]);
                firstRun = false;
                var newscrollHeight = document.getElementById("chatWindow").scrollHeight;//$("#chatWindow").scrollHeight;//.attr("scrollHeight") - 20;
                    console.log("newscrollHeight" + newscrollHeight);
                    if (newscrollHeight > oldscrollHeight) {
                        $("#chatWindow").animate({
                            scrollTop: newscrollHeight
                        }, 'normal'); //Autoscroll to bottom of div
                    }
            } else {
                if (htmlArr[0] !== '') {
                    $("#chatWindow").append(htmlArr[0]);
                    //$("#chatWindow").html(html);
                    //Insert chat log into the #chatbox div
                    var newscrollHeight = document.getElementById("chatWindow").scrollHeight;//$("#chatWindow").scrollHeight;//.attr("scrollHeight") - 20;
                    console.log("newscrollHeight" + newscrollHeight);
                    if (newscrollHeight > oldscrollHeight) {
                        $("#chatWindow").animate({
                            scrollTop: newscrollHeight
                        }, 'normal'); //Autoscroll to bottom of div
                    }
                }
                
                // alert('idle');
            }



            setTimeout("getData()", 5000);

        }
    });
}