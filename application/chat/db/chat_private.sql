CREATE TABLE IF NOT EXISTS `{dbprefix}chat_private` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `user_id_second` bigint NOT NULL,
  PRIMARY KEY (`id`)
);