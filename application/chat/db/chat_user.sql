CREATE TABLE IF NOT EXISTS `{dbprefix}chat_user` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `user_nick` text NOT NULL,
    `user_id` bigint NOT NULL,
    `room` text NOT NULL,
    `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);