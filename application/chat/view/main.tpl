<form action="?m=post" method="post" class="pure-form form">

    <div id="chatContent" class="pure-g">
        <div id="userList" class="pure-u-1-4">Online Liste wird geladen...</div>
        <div id="chatWindow" class="pure-u-3-4">Chat wird geladen... Bitte warten...</div>

        <div id="chatInputDiv" class="pure-u-1-1">
            <input id="userMsg" class="pure-input pure-input-1" type="text" name="text" autocomplete="off"/>
            <button id="chatSend" class="pure-button" type="submit" >Senden</button>
        </div>
        <!--    <div id="roomList"></div>-->
        <input id="chatRoom" name="room" type="hidden" value="rooms-global"/>
        <div style="clear:both"></div>
    </div>
</form>
<!--<div style="clear:both"></div>-->