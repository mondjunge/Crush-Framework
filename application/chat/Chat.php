<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Chat extends AppController {

    private $uNick;
    private $uId;
    private $logPath = "/../../upload/logs/";
    private $room;
    private $logFilepath;
    private $private = false;

    public function __construct() {
        
        $this->uId = Session::get('uid');
        $this->uNick = Session::get('unick');
    }

    public function index() {
        $this->autoCheckRights();
        $mode = '';
        if (isset($_REQUEST['m'])) {
            $mode = $_REQUEST['m'];
        }
        $this->setRoomAndLogPath();

        switch ($mode) {
            case 'load':
                $this->standalone = true;
                echo $this->loadLog();
                break;
            case 'idle':
                $this->standalone = true;
                echo $this->observeLog();
                break;
            case 'post':
                $this->standalone = true;
                $this->writeLog();
                break;

            case 'logout':
                $this->standalone = true;
                $this->removeOnlineUser();
                break;

            default:
                $this->view('main', null);
                break;
        }
    }

    private function setRoomAndLogPath() {
        $room = '';
        if (isset($_REQUEST['room']))
            $room = $_REQUEST['room'];

        $array = explode('-', $room);
        $roomType = $array[0];
        //$targetId = $array[1];

        if ($roomType == "private") {
            $this->private = true;
        }
        // echo "blafasel";
        // check for . and / in roomname, as you could possibly read other files on the system.
        if ($roomType == "private" && ((!isset($array[1])) || strstr($targetId, '.') != FALSE || strstr($targetId, '/') != FALSE)) {
            $this->private = false;
            exit;
        }

        switch ($this->private) {
            case true:

                /**
                 * private chat 
                 */
                $uid = $this->uId;
                // get chat Id
                if (isset($array[1])) {
                    $secondUid = $array[1];

                    // check if some private channel exists
                    $data = $this->select('chat_private', '*', "(user_id='$uid' OR user_id_second='$uid') AND (user_id='$secondUid' OR user_id_second='$secondUid')");

                    if (sizeof($data) > 0) {
                        $chatId = $data[0]['id'];
                        //echo "-".$chatId;
                    } else {
                        $fieldsValueArray = array(
                            'user_id' => $uid,
                            'user_id_second' => $secondUid
                        );

                        $chatId = $this->insert('chat_private', $fieldsValueArray);


                        // $this->room = "private/$chatId";
                    }
                    //echo $this->room;
                    $this->room = "private/$chatId";
                }

                break;
            case false:
                //$this->room = "rooms/" . $room;
                $this->room = str_replace("-", "/", $room);

                break;
            default:
                $this->room = str_replace("-", "/", $room);

                break;
        }
//        if(!is_dir(__DIR__ . $this->logPath)){
//            mkdir(__DIR__ . $this->logPath);
//        }
//        if(!is_dir(__DIR__ . $this->logPath . $this->room)){
//            mkdir(__DIR__ . $this->logPath . $this->room);
//        }
        //echo $this->room;
        $this->logFilepath = __DIR__ . $this->logPath . $this->room . "/" . date("Y-m-d") . ".html";
       // echo $this->logFilepath;
    }

//    private function startPrivateChat() {
//        $room = $this->room;
//        $uid = $this->uId;
//        $secondUid = filter_input(INPUT_POST, 'target_uid');
//        // find out room and mkdir/logfile if needed
//        
//    }

    /**
     * Returns the Chatlog incl. Userlist.
     * @return String 
     */
    private function loadLog() {
        $room = $this->room;
        

        $this->createLogIfNotExists();

        $logContent = $this->readLogFile();

        Session::set('chatHistory_' . $room, $logContent, true);

        $this->updateUserTimestamp();

        Session::writeClose();

        return $logContent . "<div class='chatSysMsg'>System:</b> Hallo " . $this->uNick . ", wie geht es Dir heute?</span>" . $this->getOnlineUserList();
    }

    /**
     * actually reads the log file
     * @return String 
     */
    private function readLogFile() {
        //$room = $this->room;
        //  $path = __DIR__ . "/logs/" . $room . "/" . date("Y-m-d") . ".html";
        $path = $this->logFilepath;
        if(is_file($path)){
            $logContent = file_get_contents($path);
        } else {
            $logContent = "";
        }
        
        return $logContent;
    }

//    private function getOnlineRooms() {
//        $rooms = $this->select('chat_user', 'room', "user_nick='$this->unick'");
//
//        $html = "<!--ROOMLIST--><div id='chat_rooms'>";
//        foreach ($rooms as $r) {
//            // delete user if last_activity is longer than 5 minutes ago
//            $array = explode('/', $r['room']);
//            $roomType = $array[0];
//            $roomId = $array[1];
//
//            if ($roomType == 'private') {
//                $html .= "<span class='room'><a class='active_room' href='?private=true&target={$ou['id']}' >{$ou['user_nick']}</a></span><br/> ";
//            }
//            if ($roomType == "rooms") {
//                $html .= "<span class='room'><a class='active_room' href='?room={$r['room']}' >{$ou['user_nick']}</a></span><br/> ";
//            }
//
//            $sixMinutesAgo = date("Y-m-d H:i:s", mktime(date("H"), date("i") - 6));
//            if ($ou['last_activity'] < $sixMinutesAgo) {
//                $this->delete('chat_user', "id='" . $ou['id'] . "'");
//            } else {
//                $html .= "<span class='online_user'><a class='private' href='?private=true&target={$ou['id']}' >{$ou['user_nick']}</a></span><br/> ";
//            }
//        }
//        $html .="</div>";
//        return $html;
//    }

    private function addOnlineUser() {
        $room = $this->room;

        $_REQUEST['m'] = "load";
        $this->writeLog();
        $onlineUsers = $this->select('chat_user', '*', "user_id='$this->uId' AND room='$room'");
        if (sizeof($onlineUsers) == 0) {
            $fieldsValueArray = array(
                'user_nick' =>  $this->uNick ,
                'user_id' =>  $this->uId ,
                'room' =>  $room 
            );
            $this->insert('chat_user', $fieldsValueArray);
        }
    }

    private function removeOnlineUser() {
        $_REQUEST['m'] = "logout";
        $this->writeLog();
        $this->delete('chat_user', "user_id='$this->uId' AND room='$this->room'");
    }

    private function updateUserTimestamp() {
        $thisUser = $this->select('chat_user', 'last_activity', "user_id='$this->uId' AND room='" . $this->room . "'");

        if ($thisUser == FALSE || sizeof($thisUser) < 1) {
            $this->addOnlineUser();
        }
        $fiveMinutesAgo = date("Y-m-d H:i:s", mktime(date("H"), date("i") - 5)); // date("Y-m-d H:i:s");
        //print_r($thisUser)
        if ($thisUser[0]['last_activity'] < $fiveMinutesAgo) {
            $fieldsValueArray = array(
                'last_activity' => date("Y-m-d H:i:s")
            );
            //$this->query("");
            $this->updatePrepare('chat_user', $fieldsValueArray, "user_id=:uid AND room=:room", array('uid' => $this->uId, 'room'=> $this->room));
        }
    }

    private function observeLog() {
        /**
         *  get chatroom name
         * load Session Helper
         * create path to log
         */
        $room = $this->room;
        
        //$path = __DIR__ . "/logs/" . $room . "/" . date("Y-m-d") . ".html";
//        $onlineUsers = $this->select('chat_user', 'user_nick', '1=1');
//        if (in_array($this->uNick, $onlineUsers[0])) {
        $this->updateUserTimestamp();
//        }
//        else {
//            $this->addOnlineUser();
//        }

        /**
         * create Session chat History 
         */
        if (!Session::isValid('chatHistory_' . $room)) {
            Session::set('chatHistory_' . $room, $this->readLogFile());
        }
        $sessionHistory = Session::get('chatHistory_' . $room);
        $fileHistory = $this->readLogFile();


        $changes = str_replace($sessionHistory, "", $fileHistory);
        Session::set('chatHistory_' . $room, $fileHistory, true);
        Session::writeClose();
        return $changes . $this->getOnlineUserList();
    }

    private function getOnlineUserList() {
        // 
        $room = $this->room;
        $onlineUsers = $this->select('chat_user', '*', "room='$room'");

        $html = "<!--USERLIST--><div id='chat_users'>";
        foreach ($onlineUsers as $ou) {
            // delete user if last_activity is longer than 5 minutes ago
            $sixMinutesAgo = date("Y-m-d H:i:s", mktime(date("H"), date("i") - 6));
            if ($ou['last_activity'] < $sixMinutesAgo) {
                $this->delete('chat_user', "id='" . $ou['id'] . "'");
            } else {
                $u = $this->select('user', 'image', "id={$ou['user_id']}");
                if ($u['0']['image'] == '') {
                    $u['0']['image'] = Config::get('relativeUrl')."theme/".Config::get('siteStyle')."/images/"."profil.png";
                }else{
                    $u['0']['image'] = Config::get('relativeUrl').$u['0']['image'];
                }
                // $this->select('user','id',"nick='{$ou['user_nick']}'");
                if ($ou['user_id'] == $this->uId) {
                    $html .= "<span class='online_user_self'><img src='{$u['0']['image']}' height='16' width='16' style='padding-right:3px;' alt='img' title='{$ou['user_nick']}'/>{$ou['user_nick']}</span><br/> ";
                } else {
                    $html .= "<span class='online_user'><a id='private-{$ou['user_id']}' class='private' href='private/{$ou['user_id']}' ><img src='{$u['0']['image']}' height='16' width='16' style='padding-right:3px;' alt='img' title='{$ou['user_nick']}'/>{$ou['user_nick']}</a></span><br/> ";
                }
            }
        }
        $html .="</div>";
        return $html;
    }

    private function writeLog() {
        $text = "";
        if(filter_has_var(INPUT_POST, 'text')){
            $text = filter_input(INPUT_POST, 'text');
        }
        //$text = $_REQUEST['text'];
        //$room = $this->room;
        $mode = filter_input(INPUT_POST,  'm');
        $pfad = $this->logFilepath;
        $this->createLogIfNotExists();

        if ($mode == 'load') {
            file_put_contents($pfad, "<div class='chatSysMsg'><span class='chatDate'>(" . date("H:i:s") . ")</span> " . $this->uNick . " ist jetzt online!<br/></div>", FILE_APPEND);
            return;
        }

        if ($mode == 'logout') {
            file_put_contents($pfad, "<div class='chatSysMsg'><span class='chatDate'>(" . date("H:i:s") . ")</span> " . $this->uNick . " ist jetzt offline... " . stripslashes(nl2br(htmlspecialchars($text))) . "<br/></div>", FILE_APPEND);
            return;
        }

        if ($mode == 'post') {
            if ($this->startsWith($text, "/") == 1) {
                $commandString = $this->handleCommand($text);
                file_put_contents($pfad, "<div class='chatMsg'><span class='chatDate'>(" . date("H:i:s") . ")</span> <span class='chatCommand'>" . stripslashes(nl2br(htmlspecialchars($commandString))) . "<br/></div>", FILE_APPEND);

                return;
            } else {
                // is now directly in the string build ;) 
                //$text = $this->replaceUrls($text);

                file_put_contents($pfad, "<div class='chatMsg'><span class='chatDate'>(" . date("H:i:s") . ")</span> <span class='chatNick'>" . $this->uNick . "</span>: " . $this->replaceUrls(stripslashes(nl2br(htmlspecialchars($text)))) . "<br/></div>", FILE_APPEND);
                return;
            }
        }
    }

    private function replaceUrls($string) {

        /*         * * make sure there is an http:// on all URLs ** */
        //$string = preg_replace('/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-\+%]+)/i', "$1http://$2", $string);
        /*         * * make all URLs links ** */
        $string = preg_replace('/([\w]+:\/\/[\w\-?&;#,%~=\.\/\@\+]+[\w\/])/i', "<a target=\"_blank\" href=\"$1\">$1</a>", $string);
        /*         * * make all emails hot links ** */
        $string = preg_replace('/([\w\-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i', "<A HREF=\"mailto:$1\">$1</A>", $string);

        return $string;
    }

    /**
     * Not sure if this should be handled here... 
     * maybe javascript replacement is better and more flexible...
     * @param type $text
     * @return type 
     */
    private function replaceSmileys($text) {
//        $text = str_replace(":)", "<img src='".Config::get("relativeUrl")."theme/"
//                .Config::get("siteStyle")
//                ."/images/smileys/yellow/smile.gif' alt=':)'/>", $text);
//        $text = str_replace(":P", " <img src='".Config::get("relativeUrl")."theme/"
//                .Config::get("siteStyle")
//                ."/images/smileys/yellow/tongue.gif' alt=':P'/>", $text);

        return $text;
    }

    private function handleCommand($text) {

        if ($this->startsWith($text, "/me")) {
            return $this->uNick . "" . str_replace("/me", "", $text) . "";
        }
        if ($this->startsWith($text, "/slap")) {
            return $this->uNick . " slaps " . str_replace("/slap", "", $text) . " around a bit with a large trout.";
        }
    }

    private function startsWith($haystack, $needle) {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    private function createLogIfNotExists() {
        //$room = $this->room;
        $logDir = dirname($this->logFilepath);
        if (!is_dir($logDir)) {
            mkdir($logDir, 0777, true);
            //echo $logDir;
        }
    }

//    private function createUserFolderIfNotExists(){
//        if(!file_exist('logs/'.$this->uId)){
//            mkdir('logs/'.$this->uId);
//        }
//    }
}
