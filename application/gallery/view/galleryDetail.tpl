<div class="galNavi">
    <a class="pure-button pure-button-gallery" title="<?php echo $this->ts('gallery home');?>" href="<?php echo $this->action('gallery'); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#home', 'svgIcon svgIcon-dark galleryIcon'); //$this->ts('Overview');      ?></a>
</div>
<!--<span class="galleryName" ><?php echo $galleryName; ?></span>-->
<h2><?php echo trim(substr($galleryName, strpos($galleryName, '.')+1)); ?></h2>
<div class="pure-g pure-g-r ">
    <?php foreach ($pictures as $p) : ?>
        <div class="pure-u-1-4 " >
            <div class="galleryBox boxBorder boxShadow">

                <a href="<?php echo $this->action($galLink.urlencode($p['name'])."#picView"); ?>">
                    <div class="pic">
                        <?php if(substr($p['path'], -3, 3)=='mp4'): ?>
                            <span class="pictureThumb">
                                <video class="fancybox-video" controls>
                                    <source src="<?php echo $p['path'];?>" type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"'/>
                                </video>
                            </span>
                        <?php else: ?>
                            <?php echo $this->image($p['path'], 'pictureThumb', '768'); ?>
                        <?php endif; ?>
                        
                    </div>
                </a>
                <div class="xtraInfo">
    <!--                    <span><?php echo $p['time']; ?></span><br/>-->
                    <span><?php echo $p['commentCount']; echo " " . $this->ts('comments');?></span>
                </div>



            </div>
        </div>

    <?php endforeach; ?>
</div>