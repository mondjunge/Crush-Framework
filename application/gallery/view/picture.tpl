<a name="picView" ></a>
<div class="galNavi">
    <a class="pure-button pure-button-gallery" title="<?php echo $this->ts('gallery home');?>" href="<?php echo $this->action('gallery'); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#home', 'svgIcon svgIcon-dark galleryIcon'); //$this->ts('Overview');      ?></a>
   
<!--    <a class="pure-button pure-button-gallery" title="<?php echo $this->ts('previous gallery');?>" href="<?php echo $this->action('gallery'); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#chevron-left', 'svgIcon svgIcon-dark galleryIcon'); //$this->ts('Overview');      ?></a>
   -->
    <?php if(isset($archiveName)) :?>
   <a class="pure-button pure-button-gallery" title="<?php echo $this->ts('back to current gallery overview');?>" href="<?php echo $this->action('gallery/getArchive/' . urlencode($archiveName).'/'. urlencode($galleryName)); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#grid-three-up', 'svgIcon svgIcon-dark galleryIcon'); ?><span class="imgBtnText"><?php echo $galleryName; ?></span></a> 
    <?php else : ?>
        <a class="pure-button pure-button-gallery" title="<?php echo $this->ts('back to current gallery overview');?>" href="<?php echo $this->action('gallery/index/' . urlencode($galleryName)); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#grid-three-up', 'svgIcon svgIcon-dark galleryIcon'); ?><span class="imgBtnText"><?php echo $galleryName; ?></span></a> 
    <?php endif; ?>
    
<!--    <a class="pure-button pure-button-gallery" title="<?php echo $this->ts('next gallery');?>" href="<?php echo $this->action('gallery'); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#chevron-right', 'svgIcon svgIcon-dark galleryIcon'); //$this->ts('Overview');      ?></a>
        --><span class=""></span>
    

    <a id="fullscrButton" class="pure-button pure-button-gallery " title="<?php echo $this->ts('fullscreen');?>" href="#picView"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#fullscreen-enter', 'svgIcon svgIcon-dark galleryIcon'); ?></a>
    <a id="playbackButton2" class="pure-button pure-button-gallery " title="<?php echo $this->ts('start slideshow');?>" href="#picView"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#media-play', 'svgIcon svgIcon-dark galleryIcon'); ?></a>
    <a id="stopPlaybackButton2" class="pure-button pure-button-gallery  hidden" title="<?php echo $this->ts('stop slideshow');?>" href="#picView"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#media-pause', 'svgIcon svgIcon-dark galleryIcon'); ?></a>
    <a id="downloadButton" target="_blank" href="<?php echo $picture; ?>" class="pure-button pure-button-gallery " title="<?php echo $this->ts('download picture in full resolution');?>" ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#data-transfer-download', 'svgIcon svgIcon-dark galleryIcon'); ?></a>
</div>
<div style="clear: both"></div>
<!-- -->
<!--<a class="pure-button floatRight" style="margin-right: 5px;" href="#"><?php echo '>>'; ?></a>-->
    
<!--    <div style="clear: both"></div>-->
<div class="pure-g pure-g-r">
    <div id="imageContainer" class="pure-u-3-4 imageContainer">
        <?php echo $imageContainer; ?>
    </div>
<div class="pure-u-1-4">
        <div class="commentsContainer">
        <?php echo $comments; ?>
    </div>
</div>
    