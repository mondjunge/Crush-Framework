<!--<div class="imageNavi">
    <a id="fullscrButton" class="pure-button pure-button-gallery " title="<?php echo $this->ts('fullscreen'); ?>" href="#picView"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#fullscreen-enter', 'svgIcon svgIcon-dark galleryIcon'); ?></a>
    <a id="playbackButton2" class="pure-button pure-button-gallery " title="<?php echo $this->ts('start slideshow'); ?>" href="#picView"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#media-play', 'svgIcon svgIcon-dark galleryIcon'); ?></a>
    <a id="stopPlaybackButton2" class="pure-button pure-button-gallery  hidden" title="<?php echo $this->ts('stop slideshow'); ?>" href="#picView"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#media-stop', 'svgIcon svgIcon-dark galleryIcon'); ?></a>
    <a id="downloadButton" target="_blank" href="<?php echo $picture; ?>" class="pure-button pure-button-gallery " title="<?php echo $this->ts('download picture in full resolution'); ?>" ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#data-transfer-download', 'svgIcon svgIcon-dark galleryIcon'); ?></a>
</div>-->
<div style="clear: both"></div>
<div class="galleryButtonBar">
    <a id="downloadButton" target="_blank" href="<?php echo $picture; ?>" class="floatRight hidden"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#data-transfer-download', 'svgIcon svgIcon-light galleryIcon'); ?></a>
    <a id="fullscrCloseButton" title="<?php echo $this->ts('stop fullscreen'); ?>" class="floatRight hidden" href="#picView"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#fullscreen-exit', 'svgIcon svgIcon-light galleryIcon'); ?></a>
    <a id="playbackButton" class="floatRight hidden" href="#picView" title="<?php echo $this->ts('start slideshow'); ?>" ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#media-play', 'svgIcon svgIcon-light galleryIcon'); ?></a>
    <a id="stopPlaybackButton" class="floatRight hidden" href="#picView" title="<?php echo $this->ts('stop slideshow'); ?>" ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#media-pause', 'svgIcon svgIcon-light galleryIcon'); ?></a>
</div>
<div style="clear: both"></div>

<div class="loadingSpinner ">
    <div class="sk-spinner sk-spinner-wandering-cubes">
        <div class="sk-cube1"></div>
        <div class="sk-cube2"></div>
    </div>
</div>

<div class="imageHolder">

    <?php if (substr($picture, -3, 3) == 'mp4'): ?>
    
    <!-- DO NOT format these bastards, as they will create a space that corrupts the margins.-->
        <span class="verticalAlignHelper" ></span><video class="galleryVideo" controls>
            <source src="<?php echo $picture; ?>" type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"'/>
            <div style="height:100px;max-width:200px;">Dein Browser kann dieses Video nicht anzeigen. 
                <a href="<?php echo $picture; ?>" >Lade es hier herunter</a>
            </div>
        </video>
    <?php else: ?>
        <span class="verticalAlignHelper"></span><?php echo $this->image($picture, 'galPic', '1920', '1920'); ?>
    <?php endif; ?>
    <div id="progressSlide" class="hidden"></div>
</div>
<?php if (isset($prevPic)) : ?> 
    <a id="prevPicBtn" class="" href="<?php echo $prevPic['link']; ?>#picView" data-url="<?php echo $prevPic['url']; ?>">
        <div id="prevPic">
            <?php echo $this->icon('arrow-left.png', 'prevPic', $this->ts('previous picture')); ?>
        </div>
    </a>
<?php endif; ?>
<?php if (isset($nextPic)) : ?> 
    <a id="nextPicBtn" class="" href="<?php echo $nextPic['link']; ?>#picView" data-url="<?php echo $nextPic['url']; ?>">
        <div id="nextPic">
            <?php echo $this->icon('arrow-right.png', 'nextPic', $this->ts('next picture')); ?>
        </div>
    </a>
<?php endif; ?>
<div id="preload">
    <?php if (isset($nextPic)) : ?> 
        <?php echo $this->image($nextPic['url'], 'prePic', '1920', '1920'); ?>
    <?php endif; ?>
    <?php if (isset($prevPic)) : ?> 
        <?php echo $this->image($prevPic['url'], 'prePic', '1920', '1920'); ?>
    <?php endif; ?>

</div>
