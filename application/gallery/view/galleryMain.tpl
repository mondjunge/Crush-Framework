<?php if (count($galleries) === 0): ?>
    <span><?php echo $this->ts('There are no pictures at the moment.'); ?></span>
<?php endif; ?>
<?php if ($adminMode): ?>
    <div class="pure-u-1">
        <a class="pure-button pure-button-primary floatRight" href="<?php echo $this->action('files?userMode=1&subdir=gallery') ?>" ><?php echo $this->ts("Gallery file manager") ?></a>
    </div>
<?php endif; ?>
<div class="pure-g pure-g-r ">
    <?php foreach ($galleries as $g) : ?>
        <div class="pure-u-1-4 " >
            <?php if ($g['archive']) : ?>
                <div class="galleryBox boxBorder boxShadow" >
                    <a class="galleryName" href="<?php echo $this->action('gallery/getArchive/' . urlencode($g['name'])); ?>">
                        <span ><?php echo $g['name'] ?></span>
                        <div class="pic">
                            <?php echo $this->image($g['thumb'], 'galleryThumb', '768'); ?>
                        </div>
                    </a>
                    <div class="xtraInfo">
                        <span><?php echo $this->ts('archive'); ?></span>
                    </div>
                </div>

            <?php else : ?>
                <div class="galleryBox boxBorder boxShadow" >
                    <a class="galleryName" href="<?php echo $this->action('gallery/index/' . urlencode($g['name'])); ?>">
                        <span  ><?php echo trim(str_replace('.','',substr($g['name'], strpos($g['name'], '.')))); ?></span>
                        <div class="pic">
                            <?php echo $this->image($g['thumb'], 'galleryThumb', '768'); ?>
                        </div>
                    </a>
                    <div class="xtraInfo">
                        <span><?php
                            echo $g['pictureCount'];
                            echo " " . $this->ts('pictures');
                            ?></span>
                        <span class="floatRight"><?php
                            echo $g['commentCount'];
                            echo " " . $this->ts('comments');
                            ?></span>
                    </div>
                </div>

    <?php endif; ?>

        </div>

<?php endforeach; ?>
</div>