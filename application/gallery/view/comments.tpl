<h2><?php echo $this->ts("Comments"); ?></h2>
<div class="writeComment ">
    <!--                        <span class="nick"><?php echo $user_nick ?></span>-->

    <form name="com" class="pure-form" action="<?php echo $this->action("gallery") ?>" method="post" enctype="multipart/form-data" >
        <input type="hidden" name="gallery" value="<?php echo $gallery; ?>" />
        <input type="hidden" name="image" value="<?php echo $image; ?>" />
        <?php echo $this->image($user_image, 'nickImgInsertComment', "32px", "32px", $user_nick); ?>
        <div class="commentInputContainer">

            <textarea class="commentInput" name="comment" placeholder="<?php echo $this->ts("write a comment...") ?>" ></textarea>
            <a class="pure-button pure-button-small insertComment" onclick="return false;" >
                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#share', 'svgIcon svgIcon-dark commentSendIcon'); ?>
                <?php echo $this->ts("send") ?></a>
        </div>


    </form>
    <div style="clear:both"></div>
</div>
<div class="comments">
    
    <?php //print_r($comments); 
    foreach ($entries as $c): ?>

        <div class="commentEntry" style="">
            <div class="floatLeft commentImg">
                <a href="<?php echo $this->action('profilpage/index/' . $c['user_id']) ?>">
                    <?php echo $this->image($c['user_image'], 'comment_img', "32px", "32px", $c['user_nick']); ?>
                </a>
            </div>

            <div class="">
                <span class="nick"><a href="<?php echo $this->action('profilpage/index/' . $c['user_id']) ?>"><?php echo $c['user_nick']; ?></a></span> 
                <span class="time"><?php echo date($this->ts('d.m.Y H:i'), strtotime($c['created'])); ?><?php echo $this->ts("Uhr"); ?></span>
                <div style="clear:both;"></div>
                <?php if ($user_id == $c['user_id'] || $adminMode == true): ?>
                    <span class="time"><a class="deleteComment" onclick="return utils.check.deleteCheck('<?php echo $this->ts('Do you really want to delete this comment? It will be gone forever (which is a long time)!'); ?>');" href="?deleteComment=<?php echo $c['id'] ?>&amp;profileUserId=<?php echo $c['user_id'] ?>&amp;gallery=<?php echo urlencode($gallery); ?>&amp;image=<?php echo urlencode($image); ?>"  ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-dark commentDeleteIcon'); ?><?php //echo $this->ts("delete");   ?></a></span>
                <?php endif; ?>
                <div class="commentText">
                    <?php echo $c['text'] ?>
                </div>
            </div>
            <div style="clear:both"></div>
        </div>


    <?php endforeach; ?>
</div>