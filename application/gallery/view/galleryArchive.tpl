<div class="galNavi">
    <a class="pure-button pure-button-gallery" title="<?php echo $this->ts('gallery home');?>" href="<?php echo $this->action('gallery'); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#home', 'svgIcon svgIcon-dark galleryIcon'); //$this->ts('Overview');      ?></a>
</div>
<h2><?php echo $archiveName; ?></h2>
<div class="pure-g pure-g-r ">
    <?php foreach ($galleries as $g) : ?>
    <div class="pure-u-1-3 " >
            <div class="galleryBox boxBorder boxShadow" >
                <a class="galleryName" href="<?php echo $this->action('gallery/getArchive/' . urlencode($g['archiveName']).'/'. urlencode($g['name'])); ?>">
                    <span ><?php echo trim(substr($g['name'], strpos($g['name'], '.')+1)); ?></span>
                    <div class="pic">
                        <?php echo $this->image($g['thumb'], 'galleryThumb', '768'); ?>
                    </div>
                </a>
                <div class="xtraInfo">
                    <span class="pictureCount"><?php echo $g['pictureCount'];
                    echo " ".$this->ts('pictures'); ?></span>
                     <span class="commentCount"><?php echo $g['commentCount'];
                    echo " ".$this->ts('comments'); ?></span>
                </div>
            </div>
        
    </div>

<?php endforeach; ?>
</div>