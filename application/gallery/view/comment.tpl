<div class="commentEntry" style="display:none;">
    <div class="floatLeft commentImg">
        <a href="<?php echo $this->action('profilpage/index/' . $user_id) ?>">
            <?php echo $this->image($user_image, 'comment_img', "32px", "32px", $user_nick); ?>
        </a>
    </div>

    <div class="">
        <span class="commentNick nick"><a href="<?php echo $this->action('profilpage/index/' . $user_id) ?>"><?php echo $user_nick; ?></a></span> <span class="time"><?php echo date($this->ts('d.m.Y H:i'), strtotime($time)); ?><?php echo $this->ts("Uhr"); ?></span>
        <div style="clear:both;"></div>
        <span ><a class="deleteComment" onclick="return utils.check.deleteCheck('<?php echo $this->ts('Do you really want to delete this comment? It will be gone forever (which is a long time)!'); ?>');" 
                  href="?deleteComment=<?php echo $id ?>&amp;profileUserId=<?php echo $user_id ?>&amp;gallery=<?php echo urlencode($gallery); ?>&amp;image=<?php echo urlencode($image); ?>"  ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-dark commentDeleteIcon'); ?><?php //echo $this->ts("delete");  ?></a></span>
        
        <div class="commentText">
            <?php echo $text ?>
        </div>
    </div>
</div>  
<div style="clear:both;"></div>