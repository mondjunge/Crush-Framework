<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'prev' =>
	'prev'
	,
	'next'=>
	'next'
	,
	'Overview'=>
	'Overview'
	,
	'write a comment...'=>
	'write a comment...'
	,
	'send'=>
	'send'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'd.m.Y H:i'=>
	'd.m.Y H:i'
	,
	'Uhr'=>
	'Uhr'
	,
	'Do you really want to delete this comment? It will be gone forever (which is a long time)!'=>
	'Do you really want to delete this comment? It will be gone forever (which is a long time)!'
	,
	'Comments'=>
	'Comments'
	,
	'pictures'=>
	'pictures'
	,
	'comments'=>
	'comments'
	,
	'archive'=>
	'archive'
	,
	'gallery home'=>
	'gallery home'
	,
	'previous gallery'=>
	'previous gallery'
	,
	'back to current gallery overview'=>
	'back to current gallery overview'
	,
	'next gallery'=>
	'next gallery'
	,
	'fullscreen'=>
	'fullscreen'
	,
	'start slideshow'=>
	'start slideshow'
	,
	'stop slideshow'=>
	'stop slideshow'
	,
	'download picture in full resolution'=>
	'download picture in full resolution'
	,
	'stop fullscreen'=>
	'stop fullscreen'
	,
	'There are no pictures at the moment.'=>
	'There are no pictures at the moment.'
	,
	'previous picture'=>
	'previous picture'
	,
	'next picture'=>
	'next picture'
	,
	'Gallery file manager'=>
	'Gallery file manager'
);