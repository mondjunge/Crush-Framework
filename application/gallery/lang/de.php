<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'prev' =>
	'vorheriges'
	,
	'next'=>
	'nächstes'
	,
	'Overview'=>
	'Übersicht'
	,
	'write a comment...'=>
	'schreibe ein Kommentar...'
	,
	'send'=>
	'absenden'
	,
	'd.m.Y H:i'=>
	'd.m.Y H:i'
	,
	'Uhr'=>
	'Uhr'
	,
	'Do you really want to delete this comment? It will be gone forever (which is a long time)!'=>
	'Möchtest Du diesen Kommentar wirklich löschen? Er wird für immer weg sein (was eine lange Zeit ist)!'
	,
	'Comments'=>
	'Kommentare'
	,
	'pictures'=>
	'Bilder'
	,
	'comments'=>
	'Kommentare'
	,
	'archive'=>
	'Archiv'
	,
	'gallery home'=>
	'Galerie Übersicht'
	,
	'previous gallery'=>
	''
	,
	'back to current gallery overview'=>
	'Zurück zur Übersicht der aktuellen Galerie'
	,
	'next gallery'=>
	''
	,
	'fullscreen'=>
	'Vollbild Modus'
	,
	'start slideshow'=>
	'Starte Slideshow'
	,
	'stop slideshow'=>
	'Stoppe Slideshow'
	,
	'download picture in full resolution'=>
	'Lade Bild in voller Auflösung herunter'
	,
	'stop fullscreen'=>
	'Vollbild Modus beenden'
	,
	'There are no pictures at the moment.'=>
	'Es gibt momentan keine Bilder.'
	,
	'previous picture'=>
	'Vorheriges Bild'
	,
	'next picture'=>
	'Nächstes Bild'
	,
	'Gallery file manager'=>
	'Galerie Datei Manager'
);