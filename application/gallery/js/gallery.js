/**
 * gallery
 */

use_package('modules');
modules.gallery = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);
    var imageContainer = document.getElementById("imageContainer");
    var slideShowActive = false;
    var slideShowSpeed = 10000;
    var progressTimeout;
    var slideshowTimeout;
    var btnElms;
    this.init = function () {

        if (initButtons()) {
            handleButtons();
            handleFullscreenChange();
            handleComments();
            handleKeyboardEvents();
            // must be optimized for small screens
            //handleGalleryPictureClick();
        }

    };

    var handleGalleryPictureClick = function () {
        $(".pictureThumb > img").on("click", function () {
            //$(this).parent("a");
            console.log();
            var html = '<div class="pure-g-r"><div id="imageContainer" class="pure-u-3-4 imageContainer"></div><div class="pure-u-1-4"><div class="commentsContainer"></div></div>';

            utils.overlay.show(html, "", undefined, "pictureOverlay");
            initButtons();
            loadPic($(this).parents("a").get(0));
            return false;
        });
    };

    var initButtons = function () {
        var pic = $('.galPic img');
        if (typeof pic !== 'undefined') {
            btnElms = [];
            btnElms['fullscrCloseButton'] = $('#fullscrCloseButton');
            btnElms['playbackButton'] = $('#playbackButton');

            return true;
        } else {
            return false;
        }
    };

    var isFullScreen = function () {
        if (typeof document.webkitFullscreenElement !== 'undefined' && document.webkitFullscreenElement === null) {
            return false;
        }
        if (typeof document.mozFullscreenElement !== 'undefined' && document.mozFullscreenElement === null) {
            return false;
        }
        if (typeof document.msFullscreenElement !== 'undefined' && document.msFullscreenElement === null) {
            return false;
        }
        if (typeof document.mozFullScreen !== 'undefined') {
            return document.mozFullScreen;
        }
        return true;
    };

    var toggleFullScreen = function (imageContainer) {
//        console.log("" + document.mozFullScreen);
//        console.log("" + document.mozFullscreenElement);
//        console.log("" + document.webkitFullscreenElement);
//        console.log(document.webkitFullscreenElement);

        if (!isFullScreen()) {

            if (imageContainer.requestFullscreen()) {
                imageContainer.requestFullscreen()();
                handleFullScreenCss();
            } else {
                imageContainer.webkitRequestFullScreen(imageContainer.ALLOW_KEYBOARD_INPUT);
                handleFullScreenCss();
            }
        } else {
            if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
                handleFullScreenCss();
            } else {
                document.webkitCancelFullScreen();
                handleFullScreenCss();
            }
        }
        
    };

    var handleFullscreenChange = function () {
        /**
         * TODO:
         * Mozilla handles webkit now, doesn't they? Comment?
         */
        document.addEventListener("fullscreenchange", function () {
//            console.log(document.mozFullScreen);
            handleFullScreenCss();
        }, false);
        
        document.addEventListener("webkitfullscreenchange", function () {
            //console.log("webkit fullscreenchange");
            handleFullScreenCss();
        }, false);
    };

    var handleFullScreenCss = function () {
        initButtons();
        if (isFullScreen()) {
            //$('.galPic img').css('max-height', '100vh');
            btnElms['fullscrCloseButton'].removeClass('hidden');
            btnElms['playbackButton'].removeClass('hidden');
            $('#imageContainer').css('background-color', '#000');
            $('#imageContainer').css('height', '100vh');
            $('#imageContainer').css('width', '100vw');

            if (slideShowActive) {
                btnElms['playbackButton'].addClass('hidden');
                $('#stopPlaybackButton').removeClass('hidden');
                $('#progressSlide').removeClass('hidden');
            } else {
                btnElms['playbackButton'].removeClass('hidden');
                $('#stopPlaybackButton').addClass('hidden');
                $('#progressSlide').addClass('hidden');
            }

        } else {
            // $('.galPic img').css('max-height', '85vh');
            btnElms['fullscrCloseButton'].addClass('hidden');
            btnElms['playbackButton'].addClass('hidden');
            $('#stopPlaybackButton').addClass('hidden');
            $('#imageContainer').removeAttr('style');

            if (slideShowActive) {
                $('#playbackButton2').addClass('hidden');
                $('#stopPlaybackButton2').removeClass('hidden');
                $('#progressSlide').removeClass('hidden');

            } else {
                $('#playbackButton2').removeClass('hidden');
                $('#stopPlaybackButton2').addClass('hidden');
                $('#progressSlide').addClass('hidden');
            }
        }
    };
    var unbindButtons = function () {
        $('#prevPicBtn').off('click');
        $('#nextPicBtn').off('click');
        btnElms['fullscrCloseButton'].off('click');
        $('#fullscrButton').off('click');
        btnElms['playbackButton'].off("click");
        $('#playbackButton2').off("click");
        $('#stopPlaybackButton').off("click");
        $('#stopPlaybackButton2').off("click");
        $('#imageContainer').off('swipeleft');
        $('#imageContainer').off('swiperight');
        
    };
    var handleButtons = function () {
        unbindButtons();
        $('#prevPicBtn').on("click", function () {
            loadPic(this);
            return false;
        });
        $('#nextPicBtn').on("click", function () {
            loadPic(this);
            return false;
        });
        $('#fullscrButton').on("click", function () {
            toggleFullScreen(imageContainer);
            return false;
        });
        btnElms['fullscrCloseButton'].on("click", function () {
            toggleFullScreen(imageContainer);
            return false;
        });
        btnElms['playbackButton'].on("click", function () {
            slideShowActive = true;
            btnElms['playbackButton'].addClass('hidden');
            $('#stopPlaybackButton').removeClass('hidden');
            toggleSlideshow();
            return false;
        });

        $('#playbackButton2').on("click", function () {
            slideShowActive = true;
            $('#playbackButton2').addClass('hidden');
            $('#stopPlaybackButton2').removeClass('hidden');
            toggleSlideshow();
            return false;
        });
        $('#stopPlaybackButton').on("click", function () {
            slideShowActive = false;
            btnElms['playbackButton'].removeClass('hidden');
            $('#stopPlaybackButton').addClass('hidden');
            toggleSlideshow();
            return false;
        });
        $('#stopPlaybackButton2').on("click", function () {
            slideShowActive = false;
            $('#playbackButton2').removeClass('hidden');
            $('#stopPlaybackButton2').addClass('hidden');

            toggleSlideshow();
            return false;
        });
        $('#imageContainer').on('swipeleft', function () {
            loadPic($('#nextPicBtn').get(0));
            return false;
            //$('#nextPicBtn').trigger("click");
        });
        $('#imageContainer').on('swiperight', function () {
            loadPic($('#prevPicBtn').get(0));
            return false;
            //$('#prevPicBtn').trigger("click");
        });
        
    };
    
    var handleKeyboardEvents = function (){
        // arrow keys
        document.addEventListener("keyup", function (e) {
            console.log("keycode: " + e.keyCode);
            if (e.keyCode === 39) {
                //right
                if ($('#nextPicBtn').length > 0) {
                    loadPic($('#nextPicBtn').get(0));
                    return false;
                }
            }
            if (e.keyCode === 37) {
                //left
                if ($('#prevPicBtn').length > 0) {
                    loadPic($('#prevPicBtn').get(0));
                    return false;
                }
            }
        }, false);
    };

    var toggleSlideshow = function () {
        if ($('#nextPicBtn').length > 0 && slideShowActive) {
            clearTimeout(slideshowTimeout);
            $('#progressSlide').css('width', '100%');
            $('#progressSlide').removeClass('hidden');
            showSlideShowProgress(slideShowSpeed / 1000);
            slideshowTimeout = setTimeout(function () {
                if (slideShowActive) {
                    $('#nextPicBtn').trigger("click");

                } else {
                    $('#progressSlide').addClass('hidden');
                    handleFullScreenCss();
                }
            }, slideShowSpeed);
        } else {
            clearTimeout(slideshowTimeout);
            $('#progressSlide').addClass('hidden');
            handleFullScreenCss();
        }

    };
    var showSlideShowProgress = function (timer) {
        if (timer <= 0 || !slideShowActive) {
            timer = slideShowSpeed / 1000;
            return;
        }
        clearTimeout(progressTimeout);
        progressTimeout = setTimeout(function () {
            if (!slideShowActive) {
                $('#progressSlide').addClass('hidden');
                $('#progressSlide').css('width', '100%');
                return;
            }
            timer = timer - 1;
            var percent = parseInt((timer / (slideShowSpeed / 1000) * 100), 10) + "%";
            //$('#progress').html(timer);
            $('#progressSlide').css('width', percent);
            showSlideShowProgress(timer);
        }, 1000);

    };

    var loadPic = function (button) {
        //initButtons();
        var link = button.href.replace('#picView', '') + "?AJAX&picOnly";
        var picUrl = $(button).data('url');
        $(button).data('clicked','true');
        console.log(link);

        $("#downloadButton").attr('href', picUrl);

        $('#imageContainer').find('img').fadeOut('slow');
        //$('.loadingSpinner').removeClass('hidden');
        $('.loadingSpinner').fadeIn('slow');
        //return;
        $.ajax({
            type: "GET",
            url: link,
            async: true,
            timeout: 5000,
            success: function (html) {
                var array = html.split(':::');
//                console.log(html);
//                console.log(array[1]);


                $('#imageContainer').html(array[0]);

                $('.loadingSpinner').hide();
                $('#imageContainer').find('.galPic > img').hide();
                $('#imageContainer').find('.galPic > img').fadeIn('slow');
                $('.commentsContainer').html(array[1]);


                handleFullScreenCss();
                handleButtons();
                handleComments();
                if (slideShowActive) {
                    toggleSlideshow();
                }
            }
        });



    };

    var handleComments = function () {
        $("textarea[name='comment']").focus(function () {
            var textarea = $(this);
            if (textarea.hasClass('autosize')) {
                //do nothing
            } else {
                textarea.autosize();
                textarea.addClass('autosize');
            }
            sendCommentClickHandler(textarea.parent().find(".insertComment"));
        });
        $("textarea[name='comment']").off('keypress');
        $("textarea[name='comment']").on('keypress', function (e) {
            if (e.keyCode === 13) {
                // 13 is enter/return key
                if (e.shiftKey) {
                    // Enter and Shift pressed... do anything here...
                    //alert(""+e.keyCode);
                    $(this).parent().find(".insertComment").trigger("click");
                    return false;
                }

            }

        });

    };
    var sendCommentClickHandler = function (element) {

        $(element).off('click');
        $(element).on("click", function () {
            var element = $(this);
            var parentFormElement = element.parent().parent();
            var textarea = element.siblings("textarea[name='comment']").first();
            var gallery = parentFormElement.children("input[name='gallery']").first().val();
            var image = parentFormElement.children("input[name='image']").first().val();
            var text = textarea.val();

            console.log("image:" + image + "; gallery:" + gallery + "; text:" + text);
            text = text.trim();
            if (text === "") {
                return false;
            }

            $.ajax({
                type: "POST",
                url: reLangUrl + "gallery",
                async: true,
                timeout: 5000,
                data: {
                    insertComment: 'send',
                    text: text,
                    gallery: gallery,
                    image: image
                },
                success: function (html) {
                    var commentsElement = parentFormElement.parent().parent().find('.comments');
                    commentsElement.prepend(html);
                    commentsElement.find(":first").show("slow", function () {
                    });

                    textarea.val('');
                    textarea.trigger('autosize');
                    //handleDeleteComment();
                },
                error: function () {

                }
            });
            return false;
        });
    };


//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


};