<?php

/**
 *  Copyright © tim 25.05.2015
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Gallery extends AppController {

    private $archiveName = null;
    private $galleryName = null;
    private $picture = null;

    public function index() {
        $this->autocheckrights();

        if (filter_has_var(INPUT_GET, 'id')) {
            $this->includeJs('extras/autosize/jquery.autosize-min.js');
            $galleryName = urldecode(filter_input(INPUT_GET, 'id'));
            $this->galleryName = $galleryName;
            $basePath = 'upload/images/gallery/' . $galleryName;
        }
        if (filter_has_var(INPUT_GET, 'id_2')) {
            $picture = urldecode(filter_input(INPUT_GET, 'id_2'));
            $this->picture = $picture;
        }
        
        if (filter_has_var(INPUT_POST, 'insertComment') && filter_input(INPUT_POST, 'text') != "") {
            $this->standalone = true;
            $this->doInsertComment();
            return;
        }
        
        if (filter_has_var(INPUT_POST, 'getCommentsHtml')) {
            $this->standalone = true;
            echo $this->getCommentsHtml($gallery, $image);
            return;
        }
        
        if (filter_has_var(INPUT_GET, 'deleteComment')) {
            $this->standalone = true;
            $this->deleteComment(filter_input(INPUT_GET, 'profileUserId'), filter_input(INPUT_GET, 'deleteComment'));
            //echo 'ok';
            $this->jumpTo("gallery/index/" . urlencode(filter_input(INPUT_GET, 'gallery')) . "/" . urlencode(filter_input(INPUT_GET, 'image')) . "#picView");
            return;
        }
        $data['adminMode'] = $this->checkSiteRights('gallery/admin');
        // image requested with AJAX
        if (filter_has_var(INPUT_GET, 'picOnly') && filter_has_var(INPUT_GET, 'id') && filter_has_var(INPUT_GET, 'id_2')) {
            $this->standalone = true;
            
            $data['galleryName'] = $galleryName;
            $data['picture'] = Config::get('relativeUrl') . $basePath . '/' . $picture;
            $pics = $this->getPrevAndNextPic($basePath, $picture);
            if (isset($pics['prev'])) {
                $data['prevPic'] = $pics['prev'];
            }
            if (isset($pics['next'])) {
                $data['nextPic'] = $pics['next'];
            }

            echo $this->fetch('imageContainer', $data) . ":::" . $this->getCommentsHtml($galleryName, $picture);
//            echo ":::";
//            echo $this->getCommentsHtml($galleryName, $picture);
            return;
        }
        
        // Image selected
        if (filter_has_var(INPUT_GET, 'id') && filter_has_var(INPUT_GET, 'id_2')) {
            $this->includeJs('extras/autosize/jquery.autosize-min.js');
            $data['galleryName'] = $galleryName;
            $data['picture'] = Config::get('relativeUrl') . $basePath . '/' . $picture;
            $pics = $this->getPrevAndNextPic($basePath, $picture);
            if (isset($pics['prev'])) {
                $data['prevPic'] = $pics['prev'];
            }
            if (isset($pics['next'])) {
                $data['nextPic'] = $pics['next'];
            }

            $data['imageContainer'] = $this->fetch('imageContainer', $data);

            $data['comments'] = $this->getCommentsHtml($galleryName, $picture);
            $this->view('picture', $data);
            return;
        }
        //echo "hello".$picture;exit;
        
        // Gallery selected
        if (filter_has_var(INPUT_GET, 'id')) {
            $data['galLink'] = 'gallery/index/' . urlencode($galleryName) . '/';
            $data['galleryName'] = $galleryName;
            $data['pictures'] = $this->fetchPictures($galleryName);
            $this->view('galleryDetail', $data);
            return;
        }

        // show Gallery Overview
        $data['galleries'] = $this->fetchGalleries();
        $this->view('galleryMain', $data);
    }

    public function getArchive() {

        if (!$this->checkSiteRights('gallery')) {
            echo "died";
            die;
        }

        // show Gallery Overview
        if (filter_has_var(INPUT_GET, 'id')) {
            $archiveName = urldecode(filter_input(INPUT_GET, 'id'));
            $this->archiveName = $archiveName;
            $data['archiveName'] = $archiveName;
            $basePath = 'upload/images/gallery/' . $archiveName;
        } else {
            die;
        }
        if (filter_has_var(INPUT_GET, 'id_2')) {
            $galleryName = urldecode(filter_input(INPUT_GET, 'id_2'));
            $this->galleryName = $galleryName;
            $data['galleryName'] = $galleryName;
            $basePath = 'upload/images/gallery/' . $archiveName . '/' . $galleryName;
        }
        if (filter_has_var(INPUT_GET, 'id_3')) {
            $picture = urldecode(filter_input(INPUT_GET, 'id_3'));
            $this->picture = $picture;
        }




        //TODO: rewrite for archives support. 
        //
//        if (filter_has_var(INPUT_POST, 'insertComment') && filter_input(INPUT_POST, 'text') != "") {
//            $this->standalone = true;
//            $this->doInsertComment();
//            return;
//        }
//
//        if (filter_has_var(INPUT_POST, 'getCommentsHtml')) {
//            $this->standalone = true;
//            echo $this->getCommentsHtml($gallery, $image);
//            return;
//        }
//
//        if (filter_has_var(INPUT_GET, 'deleteComment')) {
//            $this->standalone = true;
//            $this->deleteComment(filter_input(INPUT_GET, 'profileUserId'), filter_input(INPUT_GET, 'deleteComment'));
//            //echo 'ok';
//            $this->jumpTo("gallery/index/" . urlencode(filter_input(INPUT_GET, 'gallery')) . "/" . urlencode(filter_input(INPUT_GET, 'image')) . "#picView");
//            return;
//        }
        // image requested with AJAX
        if (filter_has_var(INPUT_GET, 'picOnly') && filter_has_var(INPUT_GET, 'id_3')) {
            $this->standalone = true;
            $data['picture'] = Config::get('relativeUrl') . $basePath . '/' . $picture;
            $pics = $this->getPrevAndNextPic($basePath, $picture);
            if (isset($pics['prev'])) {
                $data['prevPic'] = $pics['prev'];
            }
            if (isset($pics['next'])) {
                $data['nextPic'] = $pics['next'];
            }

            echo $this->fetch('imageContainer', $data) . ":::" . $this->getCommentsHtml($galleryName, $picture);
//            echo ":::";
//            echo $this->getCommentsHtml($galleryName, $picture);
            return;
        }
        // Image selected
        if (filter_has_var(INPUT_GET, 'id') && filter_has_var(INPUT_GET, 'id_3')) {
            $this->includeJs('extras/autosize/jquery.autosize-min.js');

            $data['comments'] = $this->getCommentsHtml($galleryName, $picture);
            $data['picture'] = Config::get('relativeUrl') . $basePath . '/' . $picture;
            $pics = $this->getPrevAndNextPic($basePath, $picture);
            if (isset($pics['prev'])) {
                $data['prevPic'] = $pics['prev'];
            }
            if (isset($pics['next'])) {
                $data['nextPic'] = $pics['next'];
            }

            $data['adminMode'] = $this->checkSiteRights('gallery/admin');

            $data['imageContainer'] = $this->fetch('imageContainer', $data);

            
            $this->view('picture', $data);
            return;
        }

        // Gallery selected
        if (filter_has_var(INPUT_GET, 'id_2')) {
            $data['galLink'] = 'gallery/getArchive/' . urlencode($archiveName) . '/' . urlencode($galleryName) . '/';
            $data['pictures'] = $this->fetchPictures($galleryName, $archiveName);
            $this->view('galleryDetail', $data);
            return;
        }

        $data['galleries'] = $this->fetchArchive($archiveName);
        $this->view('galleryArchive', $data);
    }

    public function admin() {
        //put your administrative code here
    }

    private function doInsertComment() {
        
        //$user_id = Session::get('uid');
        $text = addslashes(
              //  $this->replaceSmilies(
                        $this->replaceUrls(
                                stripslashes(
                                        nl2br(
                                                htmlspecialchars(
                                                        filter_input(INPUT_POST, 'text', FILTER_UNSAFE_RAW)
                                                )
                                        )
                                )
                        )
               // )
        );
        $gallery = filter_input(INPUT_POST, 'gallery');
        $image = filter_input(INPUT_POST, 'image');
        // $text = $this->replaceUrls($text);

        $data['id'] = $this->insertComment($gallery, $image, $text);

        $stripedText = stripslashes($text);

        $rs = $this->select('user', 'id,image,nick', "id='" . Session::get('uid') . "'");
        $data['user_id'] = $rs[0]['id'];

        if ($rs[0]['image'] == '') {
            $data['user_image'] = "profil.png";
        } else {
            $data['user_image'] = Config::get('relativeUrl') . $rs[0]['image'];
        }
        $data['gallery'] = $gallery;
        $data['image'] = $image;
        $data['user_nick'] = $rs[0]['nick'];
        $data['time'] = date('Y-m-d H:i:s'); // 2013-02-06 22:55:41
        $data['text'] = $stripedText;

        $this->view('comment', $data);

        /**
         * new Notification
         * 
         * send notification to user who created an entry on which someone comments now
         */
//        $category = "comownentry";
//
//        $usersRsToNotify = $this->select('user'
//                , 'id, email, nick'
//                , "active=1 AND id='$userId' AND id!='{Session::get('uid')}'");
//
//        $linkToSource = "profilpage/index/" . $userId . "#entry" . $entryId . "u" . $userId;
//        //#entry<?php echo $e['id']."u".$e['user_id'];
//
//        $messageParams[1] = Session::get('unick');
//        $message = "$1 commented your entry";
//        $tryToSendEmail = true;
//
//        $this->setNotification($usersRsToNotify, $linkToSource, $category, $message, $messageParams, $tryToSendEmail);
//
//        /**
//         * next
//         * send notification to users who comment on an entry that got commented now
//         */
//        $category2 = "comoncom";
//        $userCommentsTable = "profile_" . $userId . "_comments";
//        $rs2 = $this->select($userCommentsTable, 'user_id'
//                , "entry_id='$entryId' AND user_id!='{Session::get('uid')}' AND user_id!='$userId'");
//        $us = array();
//        foreach ($rs2 as $r) {
//            $us[] = $r['user_id'];
//        }
//        $usCsv = implode(',', $us);
//        $usersRsToNotify2 = $this->select('user'
//                , 'id, email, nick'
//                , "active=1 AND id IN ($usCsv)");
//
//        $linkToSource = "profilpage/index/" . $userId . "#entry" . $entryId . "u" . $userId;
//
//        $messageParams[1] = Session::get('unick');
//        $message2 = "$1 commented an entry you also commented";
//        $tryToSendEmail2 = true;
//
//        $this->setNotification($usersRsToNotify2, $linkToSource, $category2, $message2, $messageParams, $tryToSendEmail2);
    }

    private function insertComment($gallery, $image, $text) {
        
        $commentsTable = "gallery_comments";
        $fieldsValueArray = array(
            'gallery' =>  $gallery ,
            'user_id' => Session::get('uid') ,
            'text' => $text ,
            'image' => $image
        );
        return $this->insert($commentsTable, $fieldsValueArray);
    }

    private function getCommentsHtml($gallery, $image) {
        
        $data['adminMode'] = $this->checkSiteRights('gallery/admin');
        $data['gallery'] = $gallery;
        $data['image'] = $image;
        $data['user_image'] = Config::get('relativeUrl') . Session::get('uimage');
        $data['user_nick'] = Session::get('unick');
        $data['user_id'] = Session::get('uid');
        $data['entries'] = $this->selectComments($gallery, $image);
        return $this->fetch('comments', $data);
    }

    private function selectComments($gallery, $image) {
        $coms = $this->select('gallery_comments', '*', "gallery='$gallery' AND image='$image'", 'created desc');
        $return = array();
        foreach ($coms as $c) {
            $u = $this->select('user', 'nick,image', "id='{$c['user_id']}'");
            if ($u[0]['image'] == '') {
                $c['user_image'] = "profil.png";
            } else {
                $c['user_image'] = Config::get('relativeUrl') . $u[0]['image'];
            }

            $c['user_nick'] = $u[0]['nick'];
            $return[] = $c;
        }
        return $return;
    }

    private function countComments($gallery, $image = null) {
        if ($image == null || $image == '') {
            $rs = $this->query("SELECT COUNT(id) FROM " . Config::get('dbPrefix') . "gallery_comments " .
                    "WHERE gallery='$gallery'",true);
        } else {
            $rs = $this->query("SELECT COUNT(id) FROM " . Config::get('dbPrefix') . "gallery_comments " .
                    "WHERE gallery='$gallery' AND image='$image'",true);
        }
        $array = $rs; //mysqli_fetch_array($rs);
        //print_r($array);
        return $array[0]['COUNT(id)'];
    }

    private function getPrevAndNextPic($basePath, $p) {
        //$basePath = 'upload/images/gallery/' . $g;

        $fileArray = $this->fetchFiles($basePath);
        $prevFile = '';
        $nextFile = '';
        $break = false;
        $pic = array();
        $firstFile = $fileArray[0];
        foreach ($fileArray as $f) {
            if ($break) {
                $nextFile = $f;
                break;
            }
            if ($f == $p) {

                $break = true;
            } else {
                $prevFile = $f;
            }
        }
        if ($nextFile != '') {
            if ($this->getActiveFunction() == 'getArchive') {
                $pic['next']['link'] = Config::get('relativeUrl') . $this->getLanguage() . "/"
                        . $this->getActiveModule() . "/" . $this->getActiveFunction()
                        . "/" . urlencode($this->archiveName) . "/" . urlencode($this->galleryName) . "/" . urlencode($nextFile);
            } else {
                $pic['next']['link'] = Config::get('relativeUrl') . $this->getLanguage() . "/"
                        . $this->getActiveModule() . "/" . $this->getActiveFunction()
                        . "/" . urlencode($this->galleryName) . "/" . urlencode($nextFile);
            }

            $pic['next']['url'] = Config::get('relativeUrl') .
                    $basePath . "/" . $nextFile;
        } else{
             if ($this->getActiveFunction() == 'getArchive') {
                $pic['next']['link'] = Config::get('relativeUrl') . $this->getLanguage() . "/"
                        . $this->getActiveModule() . "/" . $this->getActiveFunction()
                        . "/" . urlencode($this->archiveName) . "/" . urlencode($this->galleryName) . "/" . urlencode($firstFile);
            } else {
                $pic['next']['link'] = Config::get('relativeUrl') . $this->getLanguage() . "/"
                        . $this->getActiveModule() . "/" . $this->getActiveFunction()
                        . "/" . urlencode($this->galleryName) . "/" . urlencode($firstFile);
            }
            $pic['next']['url'] = Config::get('relativeUrl') .
                    $basePath . "/" .$firstFile;
        }

        if ($prevFile != '') {
            if ($this->getActiveFunction() == 'getArchive') {
                $pic['prev']['link'] = Config::get('relativeUrl') . $this->getLanguage() . "/"
                        . $this->getActiveModule() . "/" . $this->getActiveFunction()
                        . "/" . urlencode($this->archiveName) . "/" . urlencode($this->galleryName) . "/" . urlencode($prevFile);
            } else {
                $pic['prev']['link'] = Config::get('relativeUrl') . $this->getLanguage() . "/"
                        . $this->getActiveModule() . "/" . $this->getActiveFunction()
                        . "/" . urlencode($this->galleryName) . "/" . urlencode($prevFile);
            }

            $pic['prev']['url'] = Config::get('relativeUrl') .
                    $basePath . "/" . $prevFile;
        }

        return $pic;
    }

    private function fetchPictures($gallery, $archiveName = '') {
        if ($archiveName == '') {
            $basePath = 'upload/images/gallery/' . $gallery;
        } else {
            $basePath = 'upload/images/gallery/' . $archiveName . '/' . $gallery;
        }

        $fileArray = $this->fetchFiles($basePath);

        $returnArray = array();
        foreach ($fileArray as $p) {
            //echo dirname(__FILE__)."/../../".$basePath.'/'.$g;
//            $exif = exif_read_data(dirname(__FILE__)."/../../".$basePath.'/'.$g, 'IFD0');
//            
//            $time = 'n/a';
//            if($exif!== false && isset($exif['DateTime'])){
//                $time = $exif['DateTime'];
//            }

            $returnArray[] = array(
                'name' => $p,
                'path' => Config::get('relativeUrl') . $basePath . '/' . $p,
//                'time' =>  $time,
                'commentCount' => $this->countComments($gallery, $p)
            );
        }

        return $returnArray;
    }

    private function fetchFiles($basePath) {
        $d = dir($basePath);
        $fileArray = array();
        while (false !== ($entry = $d->read())) {
            //echo "../../".$basePath."/".$entry."<br/>";
            //echo is_file("../../".$basePath."/".$entry);
            /**
             * TODO: FIX is_file
             */
            if ($entry != "." && $entry != ".." && strtolower($entry) != ".thumb.jpg" && strpos($entry, ".") > 4) {
                $fileArray[] = $entry;
            }
        }

        array_multisort($fileArray, SORT_ASC);

        return $fileArray;
    }

    private function countPictures($gallery) {
        $basePath = 'upload/images/gallery/' . $gallery;
        return count($this->fetchFiles($basePath));
    }

    private function fetchGalleries() {
        $basePath = 'upload/images/gallery';

        $galleryArray = array();
        
        
        if (!is_dir($basePath)) {
            mkdir($basePath, 0755);
            return $galleryArray;
        }
        
        $d = dir($basePath);
        while (false !== ($entry = $d->read())) {
            if(is_dir($basePath."/".$entry) && strpos($entry, ".") !== 0){
                $galleryArray[] = $entry;
            }
        }
        if (count($galleryArray) === 0) {
            return $galleryArray;
        }
        array_multisort($galleryArray, SORT_DESC);


        return $this->sortGalleriesAndArchives($basePath, $galleryArray);
    }

    private function sortGalleriesAndArchives($basePath, $fileArray) {
        $returnArray = array();
        $archiveArray = array();
        foreach ($fileArray as $g) {
            $picCount = $this->countPictures($g);
            if ($picCount == 0) {
                $archiveArray[] = array(
                    'name' => $g,
                    'archive' => true,
                    'thumb' => 'archive.jpg'
                );
            } else {
                $returnArray[] = array(
                    'name' => $g,
                    'commentCount' => $this->countComments($g),
                    'pictureCount' => $picCount,
                    'archive' => false,
                    'thumb' => $this->getThumbLink($basePath . '/' . $g)
                );
            }
        }
        foreach ($archiveArray as $a) {
            array_push($returnArray, $a);
        }
        return $returnArray;
    }

    private function fetchArchive($archive) {
        $basePath = 'upload/images/gallery/' . $archive;


        $d = dir($basePath);
        $fileArray = array();
        while (false !== ($entry = $d->read())) {
            if ($entry != "." && $entry != ".." && strtolower($entry) != ".thumb.jpg") {
                $fileArray[] = $entry;
            }
        }

        array_multisort($fileArray, SORT_DESC);

        $returnArray = array();
        foreach ($fileArray as $g) {
            $returnArray[] = array(
                'name' => $g,
                'commentCount' => $this->countComments($g),
                'pictureCount' => $this->countPictures($archive . '/' . $g),
                'archive' => false,
                'archiveName' => $archive,
                'thumb' => $this->getThumbLink($basePath . '/' . $g)
            );
        }

        return $returnArray;
    }

    private function getThumbLink($path) {
        if (is_file($path . '/.thumb.jpg')) {
            return Config::get('relativeUrl') . $path . '/.thumb.jpg';
        }
        if (is_file($path . '/.thumb.JPG')) {
            return Config::get('relativeUrl') . $path . '/.thumb.JPG';
        }

        $d = dir($path);
        while (false !== ($entry = $d->read())) {
            if ($entry != "." && $entry != "..") {
                return Config::get('relativeUrl') . $path . '/' . $entry;
            }
        }

        return Config::get('relativeUrl') . 'theme/' . Config::get('siteStyle') . '/images/logo90x60.png';
    }

//    private function replaceSmilies($string) {
//
//        $icons = $this->getSmilies();
//
//        foreach ($icons as $icon => $image) {
//            $icon = preg_quote($icon);
//            $string = preg_replace("!$icon!", $image, $string);
//        }
//
//
//        return $string;
//    }
//
//    /**
//     * Returns [smilie][replacement] Array
//     * @return array 
//     */
//    private function getSmilies() {
//        $themeUrl = Config::get('relativeUrl') . 'theme/' . Config::get('siteStyle');
//        $icons = array(
//            ':)' => '<img src="' . $themeUrl . '/images/smileys/green/smile.gif" alt=":)" title=":)" class="smilie" />',
//            ':(' => '<img src="' . $themeUrl . '/images/smileys/green/sad.gif" alt=":(" title=":("  class="smilie" />',
//            ':\'(' => '<img src="' . $themeUrl . '/images/smileys/green/heul.gif" alt=":\'(" title=":\'("  class="smilie" />',
//            ';)' => '<img src="' . $themeUrl . '/images/smileys/green/twink.gif" alt=";)" title=";)"  class="smilie" />',
//            ':*' => '<img src="' . $themeUrl . '/images/smileys/green/kiss.png" alt=":*" title=":*"  class="smilie" />',
//            ':D' => '<img src="' . $themeUrl . '/images/smileys/green/grin.gif" alt=":D" title=":D"  class="smilie" />',
//            ':P' => '<img src="' . $themeUrl . '/images/smileys/green/tongue.gif" alt=":P" title=":P"  class="smilie" />',
//            ':O' => '<img src="' . $themeUrl . '/images/smileys/green/wtf.gif" alt=":O" title=":O"  class="smilie" />',
//            '8|' => '<img src="' . $themeUrl . '/images/smileys/green/omg.gif" alt="8|" title="8|"  class="smilie" />',
//            '&lt;||' => '<img src="' . $themeUrl . '/images/smileys/green/joint.gif" alt="&lt;||" title="&lt;||"  class="smilie" />',
//            'B)' => '<img src="' . $themeUrl . '/images/smileys/green/cool.gif" alt="B)" title="B)"  class="smilie" />',
//            ':horny:' => '<img src="' . $themeUrl . '/images/smileys/green/love.gif" alt=":horny:" title=":horny:"  class="smilie" />',
//            ':what:' => '<img src="' . $themeUrl . '/images/smileys/green/huuh.gif" alt=":what:" title=":what:"  class="smilie" />',
//            ':coffee:' => '<img src="' . $themeUrl . '/images/smileys/green/coffee.gif" alt=":coffee:" title=":coffee:"  class="smilie" />',
//            ':beer:' => '<img src="' . $themeUrl . '/images/smileys/green/bier.gif" alt=":beer:" title=":beer:"  class="smilie" />',
//            ':wein:' => '<img src="' . $themeUrl . '/images/smileys/green/rotwein.gif" alt=":wein:" title=":wein:"  class="smilie" />',
//            ':smoke:' => '<img src="' . $themeUrl . '/images/smileys/green/smoke.gif" alt=":smoke:" title=":smoke:"  class="smilie" />',
//            ':bye:' => '<img src="' . $themeUrl . '/images/smileys/green/byebye.gif" alt=":bye:" title=":bye:"  class="smilie" />',
//            ':hello:' => '<img src="' . $themeUrl . '/images/smileys/green/hello.gif" alt=":hello:" title=":hello:"  class="smilie" />',
//            ':doh:' => '<img src="' . $themeUrl . '/images/smileys/green/doh.gif" alt=":doh:" title=":doh:"  class="smilie" />',
//            ':zzz:' => '<img src="' . $themeUrl . '/images/smileys/green/sleep.gif" alt=":zzz:" title=":zzz:"  class="smilie" />',
//            ':holy:' => '<img src="' . $themeUrl . '/images/smileys/green/holy.gif" alt=":holy:" title=":holy:"  class="smilie" />',
//            ':evil:' => '<img src="' . $themeUrl . '/images/smileys/green/twisted.gif" alt=":evil:" title=":evil:"  class="smilie" />',
//            ':puke:' => '<img src="' . $themeUrl . '/images/smileys/green/puke.gif" alt=":puke:" title=":puke:"  class="smilie" />',
//            ':devil:' => '<img src="' . $themeUrl . '/images/smileys/green/devil.gif" alt=":devil:" title=":devil:"  class="smilie" />',
//            ':angel:' => '<img src="' . $themeUrl . '/images/smileys/green/engel.gif" alt=":angel:" title=":angel:"  class="smilie" />',
//            ':agree:' => '<img src="' . $themeUrl . '/images/smileys/green/agree.gif" alt=":agree:" title=":agree:"  class="smilie" />',
//            ':disagree:' => '<img src="' . $themeUrl . '/images/smileys/green/disagree.gif" alt=":disagree:" title=":disagree:"  class="smilie" />',
//            ':nonazi:' => '<img src="' . $themeUrl . '/images/smileys/green/nofascho.gif" alt=":nonazi:" title=":nonazi:"  class="smilie" />',
//            ':rofl:' => '<img src="' . $themeUrl . '/images/smileys/green/rofl_1.gif" alt=":rofl:" title=":rofl:" class="smilie" />',
//            ':lol:' => '<img src="' . $themeUrl . '/images/smileys/green/lol.gif" alt=":lol:" title=":lol:"  class="smilie" />',
//            ':up:' => '<img src="' . $themeUrl . '/images/smileys/green/thumb.gif" alt=":up:" title=":up:"  class="smilie" />',
//            ':down:' => '<img src="' . $themeUrl . '/images/smileys/green/thumbdown.gif" alt=":down:" title=":down:"  class="smilie" />',
//            ':fu:' => '<img src="' . $themeUrl . '/images/smileys/green/fu.gif" alt=":fu:" title=":fu:" class="smilie" />',
//            ':rock:' => '<img src="' . $themeUrl . '/images/smileys/green/rock.gif" alt=":rock:" title=":rock:"  class="smilie" />',
//            ':hack:' => '<img src="' . $themeUrl . '/images/smileys/green/hack.gif" alt=":hack:" title=":hack:"  class="smilie" />',
//            ':blush:' => '<img src="' . $themeUrl . '/images/smileys/green/blush.gif" alt=":blush:" title=":blush:"  class="smilie" />',
//            ':hug:' => '<img src="' . $themeUrl . '/images/smileys/green/hug.gif" alt=":hug:" title=":hug:"  class="smilie" />',
//            ':cuddle:' => '<img src="' . $themeUrl . '/images/smileys/green/cuddle.gif" alt=":cuddle:" title=":cuddle:"  class="smilie" />',
//            ':sex:' => '<img src="' . $themeUrl . '/images/smileys/green/sex.gif" alt=":sex:" title=":sex:"  class="smilie" />',
//            ':hide:' => '<img src="' . $themeUrl . '/images/smileys/green/hide.gif" alt=":hide:" title=":hide:"  class="smilie" />',
//            '&lt;3' => '<img src="' . $themeUrl . '/images/smileys/zz_red_heart.png" alt="&lt;3" title="&lt;3" width="20" height="20" class="smilie" />'
//        );
//
//        return $icons;
//    }

    private function replaceUrls($string) {

        /*         * * make sure there is an http:// on all URLs ** */
        $string = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-\+%]+)/i", "$1http://$2", $string);


        $pattern = '/([\w]+:\/\/[\w?!&;#,%~=\.\/\@\+-]+[\w\/])/i';
        $string = preg_replace_callback($pattern, array($this, 'replaceSwitch'), $string);

        return $string;
    }

    private function replaceSwitch($matches) {
        if (strpos($matches[1], 'youtube.') !== false) {
            return $this->replaceYoutube($matches[1]);
        } else if (strpos($matches[1], 'youtu.') !== false) {
            return $this->replaceYoutu($matches[1]);
        } else if (strpos($matches[1], 'vimeo.') !== false) {
            return $this->replaceVimeo($matches[1]);
        } else if (strpos($matches[1], 'soundcloud.') !== false) {
            return $this->replaceSoundcloud($matches[1]);
        } else if (strpos($matches[1], 'dailymotion.') !== false) {
            return $this->replaceDailyMotion($matches[1]);
        } else if (strtolower(substr($matches[0], -4, 4)) == '.jpg' || strtolower(substr($matches[0], -4, 4)) == '.jpeg' || strtolower(substr($matches[0], -4, 4)) == '.png' || strtolower(substr($matches[0], -4, 4)) == '.gif') {
            return $this->replaceImage($matches[0]);
        } else {
            $title = $this->getTitleCurl($matches[1]);
            $favicon = $this->getFavicon($matches[1]);
            return $favicon . "&nbsp;<b>" . $title . "</b><br/>" . $this->replaceUrl($matches[1]);
        }
    }

    private function replaceImage($string) {
        $data['path'] = $string;
        $data['width'] = '';
        $return = $this->fetch('imageHtml', $data);

        return $return;
    }

    private function replaceVimeo($string) {
        $pattern = '/https?:\/\/vimeo\.com\/m?\/?([a-zA-Z0-9_\-]+)(\S*)/i';
        //$replace = '<object width="100%" ><param name="movie" value="https://www.youtube.com/v/$2&amp;hl=en_US&amp;fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/$2&amp;hl=en_US&amp;fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="100%" ></embed></object>';
        //$replaceNew = '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/$2" frameborder="0" allowfullscreen></iframe>';
        //<iframe src="//player.vimeo.com/video/93365760?byline=0&amp;color=9ebf33" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        $replaceNew = '<iframe class="vimeo" src="//player.vimeo.com/video/$1" allowfullscreen></iframe>';

        $favicon = $this->getFavicon($string);
        $title = $favicon . "<b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($string);
        return "" . $title . "<br/>" . preg_replace($pattern, $replaceNew, $string) . $link . "";
    }

    private function replaceYoutube($string) {
        $pattern = '/https?:\/\/www\.youtube\.com\/watch\?(.*?)v=([a-zA-Z0-9_\-]+)(\S*)/i';
        //$replace = '<object width="100%" ><param name="movie" value="https://www.youtube.com/v/$2&amp;hl=en_US&amp;fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/$2&amp;hl=en_US&amp;fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="100%" ></embed></object>';

        $replaceNewUrl = 'https://www.youtube.com/watch/$2';
        $newUrl = preg_replace($pattern, $replaceNewUrl, $string);


        $favicon = $this->getFavicon($string);
        $title = $favicon . "<b>" . $this->getTitleCurl($string) . "</b>";

        $favicon = $this->getFavicon($string);
        $title = $favicon . "<b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($newUrl);

        $replaceNew = '<iframe class="youTube" src="https://www.youtube.com/embed/$2" allowfullscreen></iframe>';

        return "" . $title . "<br/>" . preg_replace($pattern, $replaceNew, $string) . $link . "";
    }

    private function replaceYoutu($string) {
        $pattern = '/https?:\/\/youtu\.be\/([a-zA-Z0-9_\-]+)(\S*)/i';
        //$replace = '<object width="100%" ><param name="movie" value="https://www.youtube.com/v/$1&amp;hl=en_US&amp;fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/$1&amp;hl=en_US&amp;fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="100%" ></embed></object>';

        $replaceNew = '<iframe class="youTube" src="https://www.youtube.com/embed/$1" allowfullscreen></iframe>';

        $favicon = $this->getFavicon($string);
        $title = $favicon . "<b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($string);
        return "" . $title . "<br/>" . preg_replace($pattern, $replaceNew, $string) . $link . "";
    }

    private function replaceSoundcloud($string) {
        //$pattern = '/https?:\/\/w?\.?soundcloud\.com\/(.*?)\/([a-zA-Z0-9_\-]+)(\S*)/i';
        #https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/138362966&color=ff5500&auto_play=false&hide_related=false&show_artwork=true

        $replace = 'https://w.soundcloud.com/player/?url=' . urlencode($string) . "&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=true";
        $iFrame = '<iframe class="soundCloud" src="' . $replace . '"></iframe>';
        $link = "<br/>" . $this->replaceUrl($string);

        return $iFrame . $link;
        //return $link.preg_replace($pattern, $replace, $string);
    }

    private function replaceDailyMotion($string) {
        #<iframe frameborder="0" width="480" height="270" src="http://www.dailymotion.com/embed/video/x1eg6fc" allowfullscreen></iframe>
        $pattern = '/https?:\/\/www\.dailymotion.com\/video\/([a-zA-Z0-9_\-]+)(\S*)/i';

        $replace = '<iframe class="dailyMotion" src="https://www.dailymotion.com/embed/video/$1" allowfullscreen></iframe>';

        $favicon = $this->getFavicon($string);
        $title = $favicon . "<b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($string);
        return "" . $title . "<br/>" . preg_replace($pattern, $replace, $string) . $link . "";
    }

    private function replaceUrl($string) {
        /*         * * make all URLs links ** */
        $string = preg_replace('/([\w]+:\/\/[\w-?!&;#,%~=\.\/\@\+]+[\w\/])/i', "<a target=\"_blank\" href=\"$1\">$1</a>", $string);
        /*         * * make all emails hot links ** */
        $string = preg_replace("/([\w-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i", "<A HREF=\"mailto:$1\">$1</A>", $string);

        return $string;
    }

    /**
     * 
     * @param string $url
     * @return string
     */
    private function getUrlContent($url) {
        // get html via url
        $savePath = dirname(__FILE__) . "/../../cache/html/" . md5($url) . ".html";
        if (!is_dir(dirname(__FILE__) . "/../../cache/html/")) {
            mkdir(dirname(__FILE__) . "/../../cache/html/", 0777);
        }
        if (is_file($savePath)) {
            //echo "ist schon da!";
            return file_get_contents($savePath);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 400);
//        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 0);
        $html = curl_exec($ch);
        curl_close($ch);
//        fclose($fp);
//        chmod($savePath, 0644);
        file_put_contents($savePath, $html);

        return $html;
    }

    public function getTitleCurl($url) {
        $this->checkSiteRights('profilpage/dash');

        $html = $this->getUrlContent($url);

        // get title
        $dom = new DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        $titleTagElement = $dom->documentElement->getElementsByTagName('title');
        $titleRaw = $titleTagElement->item('0')->textContent;
        $title = trim(empty($titleRaw) ? 'Untitled' : $titleRaw);

        // convert title to utf-8 character encoding
        if ($title != 'Untitled') {
            preg_match('/<charset\=(.+)\"/i', $html, $match);
            if (!empty($match[0])) {
                $charset = str_replace('"', '', $match[0]);
                $charset = str_replace("'", '', $charset);
                $charset = strtolower(trim($charset));
                if ($charset != 'utf-8') {
                    $title = iconv($charset, 'utf-8', $title);
                }
            }
        }

        return $title;
    }

    private function isUrl($string) {
        $pattern = '/([\w]+:\/\/[\w-?!&;#,%~=\.\/\@\+]+[\w\/])/i';

        if (preg_match($pattern, $string) == 1) {
            return true;
        }
    }

    public function getFavicon($url) {
        $this->checkSiteRights('profilpage/dash');

        $urlParsed = parse_url($url);


        // check for standard favicon
        $userPath = $urlParsed['host'];
        $path = "http://" . $userPath . "/favicon.ico";
        $header = get_headers($path);
        if (preg_match("|200|", $header[0])) {
            return $this->imageHtml($path, 'ext_fav', '16px'); //$this->imageHtml();//'<img src="'..'" />';
        }


        # make the URL simpler
        $elems = parse_url($url);
        $url = $elems['scheme'] . '://' . $elems['host'];

        # load site
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
        curl_close($ch);

        # look for the shortcut icon inside the loaded page
        $matches = array();
        $regex_pattern = "/rel=\"shortcut icon\" (?:href=[\'\"]([^\'\"]+)[\'\"])?/";
        preg_match_all($regex_pattern, $output, $matches);

        if (isset($matches[1][0])) {
            $favicon = $matches[1][0];

            # check if absolute url or relative path
            $favicon_elems = parse_url($favicon);

            # if relative
            if (!isset($favicon_elems['host']) && substr($favicon, 0, 2) != "//") {
                $favicon = $url . '/' . $favicon;
            }
            return $this->imageHtml($favicon, 'ext_fav', '16px'); //'<img src="'..'" />';
            //return "<img src='".$favicon."' />";
        }

        return false;
    }

    /**
     * delete own comment (or any if superuser)
     * @param type $profileUserId
     * @param type $commentId 
     */
    private function deleteComment($profileUserId, $commentId) {
        
        $userCommentsTable = "gallery_comments";
        // check if comment is own comment or user is superadmin or comment table is own comment table
        $checkRs = $this->select($userCommentsTable, '*', "id='" . $commentId . "' AND user_id='" . Session::get('uid') . "'");
        if ($checkRs !== FALSE || $this->checkSiteRights('profilpage/admin') || $profileUserId == Session::get('uid')) {
            $this->delete($userCommentsTable, "id='" . $commentId . "'");
        }
    }

}
