<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
$aTranslationStrings = array(
    'Hello World' =>
    'Hello World'
    ,
    'This is a text' =>
    'This is a text'
    ,
    'Hello sea' =>
    'Hello sea'
    ,
    'This is the sea' =>
    'This is the sea'
    ,
    'key not set!' =>
    'key not set!'
    ,
    'everything fine' =>
    'everything fine'
    ,
    'somethings wrong ;) no.. just joking' =>
    'somethings wrong ;) no.. just testing'
    ,
    'go to the sea' =>
    'go to the sea'
    ,
    'go back to the index' =>
    'go back to the index'
);