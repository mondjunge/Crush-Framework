<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
$aTranslationStrings = array(
    'Hello World' =>
    'Hallo Welt'
    ,
    'This is a text' =>
    'Das hier ist ein Text'
    ,
    'Hello sea' =>
    'Hallo Meer'
    ,
    'This is the sea' =>
    'Das hier ist das Meer'
    ,
    'key not set!' =>
    'Schlüssel nicht gesetzt!'
    ,
    'everything fine' =>
    'alles gut'
    ,
    'somethings wrong ;) no.. just joking' =>
    'irgendwas ist falsch... Error test ;)'
    ,
    'go to the sea' =>
    'geh ans Meer'
    ,
    'go back to the index' =>
    'zurück zur index Funktion'
);