<?php echo$title ?>
<br/>
<?php echo$text ?>
<div><?php echo$link ?></div>

<h3>Checkbox Test</h3>
<form class="pure-form pure-form-aligned" action="/Crush-Framework/de/login" method="post">
    <fieldset>

        <div class="pure-control-group">
            <label for="mn">E-Mail</label>
            <input id="mn" name="nick" type="text" placeholder="email@example.org">
        </div>
        <div class="pure-control-group">
            <label for="aerg">Passwort</label>
            <input id="aerg" name="pass" type="password" maxlength="64" placeholder="********"><br>
        </div>

        <div class="pure-control-group pure-checkbox">
            <label for="raeA">Label vor input in pure-control-group</label>
            <input id="raeA" name="resident" type="checkbox">
        </div>
        <div class="pure-control-group">
            <input id="raeD" name="resident" type="checkbox">
            <label for="raeD" class="pure-checkbox" tabindex="0">
                Label nach input in pure-control-group
            </label>
        </div>
        <!--        <div class="pure-control-group">
                    <input id="raeB" name="resident" type="checkbox">
                    <label for="raeB">Label nach input</label>
                </div>
                
                <div class="pure-controls">
                    <label for="raeC" class="pure-checkbox">Label umschließt input
                        <input id="raeC" name="resident" type="checkbox">
                    </label>
                </div>-->
        <!--         <div class="pure-control-group">
                    <label for="raeD" class="pure-checkbox">
                        <input id="raeD" name="resident" type="checkbox">
                        Label umschließt input in pure-control-group
                    </label>
                </div>-->

        <!--        <div class="pure-controls">
                    <label for="raeD" class="pure-checkbox">
                        <input id="raeD" name="resident" type="checkbox">
                        Label umschließt input
                    </label>
                </div>-->
        <div class="pure-control-group">
            <label class="pure-checker" tabindex="0" title="Some wild checkbox">
                <div class="checker">
                    <input class="checkbox" name="wild" type="checkbox" />
                    <div class="check-bg"></div>
                    <div class="checkmark">
                        <svg viewBox="0 0 100 100">
                        <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                </div>
                <span class="label">Some wild checkbox</span>
            </label>
        </div>

        <div class="pure-controls">
            <label class="pure-checker" tabindex="0" title="Another wild checkbox">
                <div class="checker">
                    <input class="checkbox" name="wildTwo" type="checkbox" />
                    <div class="check-bg"></div>
                    <div class="checkmark">
                        <svg viewBox="0 0 100 100">
                        <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                </div>
                <span class="label">Another wild checkbox</span>
            </label>
<!--            <label for="rae" class="pure-checkbox" tabindex="0">
                <input id="rae" name="resident" type="checkbox">
                Label umschließt input in pure-controls
            </label>-->
            <input class="pure-button pure-button-primary" type="submit" name="sendLoginRequest" value="Anmelden"> 
        </div>  

        <div class="pure-controls">  
            <a href="/Crush-Framework/de/login/retreivePassword">Passwort vergessen?</a>
        </div>
        <div class="pure-controls"> 
            <a id="" class="" href="/Crush-Framework/de/login?register">Noch keine Anmeldedaten? Erstelle einen neuen Nutzeraccount!</a>
        </div>
    </fieldset>
</form>