<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'This module is for development only and should be removed in productive environment. It is a security leak.' =>
	'This module is for development only and should be removed in productive environment. It is a security leak.'
	,
	'Unsufficiant rights.'=>
	'Unsufficiant rights.'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
);