<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'This module is for development only and should be removed in productive environment. It is a security leak.' =>
	'Dieses Modul is für Entwicklungszwecke und sollte in Produktivumgebungen deaktiviert werden. Es ist ein potentielles Sicherheitsloch.'
	,
	'Unsufficiant rights.'=>
	''
	,
	'Insufficient rights!'=>
	''
);