<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Phpinfo extends AppController {

    public function index() {
        $this->autoCheckRights();
        //$this->setErrorMessage($this->ts("This module is for development only and should be removed in productive environment. It is a security leak."));

        ob_start();                              // Capturing
        phpinfo();                               // phpinfo ()
        $info = trim(ob_get_clean());           // output

        $start = 6 + strpos($info, '<body>');
        $stop = strpos($info, '</body>');
        $body = substr($info, $start, $stop - $start);
        echo "<div class='phpinfo'>" . $body . "</div>";
    }

}
