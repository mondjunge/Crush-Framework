<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 * INSERT INTO `{dbprefix}site_data` (`id`, `site_id`, `name`, `title`, `content`, `author`, `language`) VALUES 
(1, 1, 'Impressum', 'Impressum', '<div>\r\n<div>\r\n<h2>Impressum</h2>\r\n\r\n<p><strong>Verantwortlich für den Inhalt:</strong></p>\r\n\r\n<p>Max Muster - max@muster.de</p>\r\n\r\n<div id=\"adresse\">\r\n<p class=\"mapLocation\">Dresdener Straße 18<br />\r\n32423 Minden</p>\r\n</div>\r\n<strong>Copyright Hinweis:</strong>\r\n\r\n<p style=\"text-align:justify\">Sämtliche Texte, Bilder, Grafiken, Animationen, Videos, Sounds und sonstige Inhalte dieser Website sowie deren Anordung sind durch das Urheberrecht und andere Schutzgesetze geschützt. Es darf keine Vervielfältigung, Veränderung oder Verwendung der genannten Inhalte in anderen elektronischen oder gedruckten Publikationen ohne vorherige Zustimmung durch den Urheber erfolgen.</p>\r\n\r\n<p style=\"text-align:justify\">Alle Markenzeichen sind, soweit nicht anders angegeben, markenrechtlich geschützt.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Datenschutzerklärung:</strong></p>\r\n\r\n<p style=\"text-align:justify\">Sofern innerhalb des Internetangebotes die Möglichkeit zur Eingabe persönlicher oder geschäftlicher Daten (E-Mail Adressen, Namen, Anschriften) besteht, so erfolgt die Preisgabe dieser Daten seitens des Nutzers auf ausdrücklich freiwilliger Basis. Pseudonyme dürfen genutzt werden.</p>\r\n\r\n<p>Diese Seite verwendet selbst gehostete Statistiksoftware (<a href=\"https://matomo.org/\">Matomo.org</a> ehm. Piwik) zur Erfassung des Besucherverhaltens. Dabei werden Datum, Uhrzeit, Verweildauer, verwendeter Browser, Betriebsystem, Bildschirmauflösung, Geolocation und IP-Adresse des aufrufenden Systems aufgezeichnet. IP Adressen werden nur gekürzt gespeichert.</p>\r\n\r\n<p>Alle gesammelten oder freiwillig übersendeten Informationen werden ausschließlich zur Optimierung der Webinhalte und Funktionen genutzt und nicht an Dritte übermittelt.</p>\r\n\r\n<p>Die Webseite verwendet Cookies zum Spamschutz, zur dauerhaften Sprachauswahl und zum dauerhaften einloggen.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Links / Verweise</strong>:</p>\r\n\r\n<p>Die Inhalte verlinkter Domains liegen nicht in meinem Einflußbereich und ich kann für den Inhalt dieser Domains keine Verantwortung übernehmen. Sollten sich Inhalte verlinkter Domains ändern und dem Verlinkungsgrund nicht mehr entsprechen, bitte ich um kurzen Hinweis per E-Mail oder Kommentar im Blog. Danke.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Haftung:</strong></p>\r\n\r\n<p>Von mir erstellte Artikel im Blog sind als Hilfen zu verstehen. Jeglicher Schaden der durch Verwendung dieser Hilfen entstehen könnte, ist vom Nutzer selbst zu tragen.</p>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Credits:</strong></p>\r\n\r\n<p>Diese Seite wurde unter Einsatz freier Software mit dem <a href=\"https://gitlab.com/mondjunge/Crush-Framework\">Crush-Framework</a> erstellt.</p>\r\n</div>\r\n', '', 'de');

 */
class Site extends AppController implements ISearchable {

    /**
     * Security Audit 
     * Datum: 25.06.2016
     * Gefährdung: mittel
     * Komplexität: mittel
     * Status: OK
     */
    public function index() {
        $this->autoCheckRights();
        if (filter_has_var(INPUT_GET, 'getAddressJSON')) {
            $this->standalone = true;
            $address = filter_input(INPUT_GET, 'getAddressJSON');
            // + address + "&format=json&polygon=1&addressdetails=1";
            echo $this->curlUrlContent("https://nominatim.openstreetmap.org/search?q=" . $address . "&format=json&polygon=1&addressdetails=1");
            return;
        }

        if (filter_has_var(INPUT_GET, 'show')) {
            $this->showStatic();
        } else {
            $rs = $this->selectPrepare('site', 'module', "active=1 AND type='s' AND hide=0 ORDER BY rank asc LIMIT 0,1");
            //echo $rs[0]['module']; exit;
            //$this->jumpTo($rs[0]['module']);

            if (empty($rs[0]['module'])) {
                $this->jumpTo('login');
            } else {
                $this->jumpTo($rs[0]['module']);
            }
        }
    }

    public function admin() {

        $this->autoCheckRights();
        if (filter_has_var(INPUT_GET, 'areUpdatesAvailable')) {
            $this->standalone = true;
            /**
             * TODO: check for module updates
             */
            $data = $this->getSitesData();

            if ($this->areUpdatesAvailable($data['sites']) || $this->isCoreUpdateAvailable()) {
                echo "true";
                return;
            } else {
                echo "false";
                return;
            }
        }

        if (filter_has_var(INPUT_POST, 'saveConfiguration')) {
            $this->createConfiguration();
            Link::jumpToLast();
            //$this->viewEditModule();
        }
        if (filter_has_var(INPUT_POST, 'activate')) {
            $this->activateModule();
        }
        if (filter_has_var(INPUT_POST, 'deactivate')) {
            $this->deactivateModule();
        }
        if (filter_has_var(INPUT_POST, 'deleteModuleFromDb')) {
            $this->deleteModuleFromDb();
        }
        if (filter_has_var(INPUT_POST, 'deleteSiteFromDb')) {
            $this->deleteSiteFromDb();
        }

        if (filter_has_var(INPUT_GET, 'rank')) {
            $this->standalone = true;
            $this->updateRank();
            // echo filter_input(INPUT_POST, 's');updateStatic
            //echo "Changes Saved";
        } elseif (filter_has_var(INPUT_GET, 'edit')) {
            $this->viewEditModule();
        } elseif (filter_has_var(INPUT_GET, 'editStatic')) {
            $this->viewEditStatic();
        } elseif (filter_has_var(INPUT_POST, 'updateStatic')) {
            $this->updateStatic();
            $this->main();
        } elseif (filter_has_var(INPUT_POST, 'updateModule')) {
            $this->updateModule();
            $this->jumpToLast();
        } elseif (filter_has_var(INPUT_GET, 'manageUpdates')) {
            if (filter_has_var(INPUT_GET, 'success')) {
                /**
                 * DONE: 
                 * files have been updated succesfully, run database update scripts.
                 */
                $installer = new Installer();
                $installer->updateModulesDb( array(1 => Input::sanitize(filter_input(INPUT_GET, 'manageUpdates'))) );
                $this->setSystemMessage($this->ts("Updated $1 successfully!", array(1 => Input::sanitize(filter_input(INPUT_GET, 'manageUpdates'), 'site'))));
            }
            if (filter_has_var(INPUT_GET, 'failure')) {
                $this->setErrorMessage($this->ts(url_decode(filter_input(INPUT_GET, 'failure'))));
            }
            $this->manageUpdates();
        } elseif (filter_has_var(INPUT_GET, 'manageDownloads')) {
            $this->setSystemMessage($this->ts("Not implemented, yet."));
            $this->main();
        } elseif (filter_has_var(INPUT_POST, 'createStatic')) {
            $this->insertStatic();
            $this->main();
        } elseif (filter_has_var(INPUT_GET, 'addStatic')) {
            $this->viewAddStatic();
        } elseif (filter_has_var(INPUT_POST, 'initialize')) {
            $this->main();
        } elseif (filter_has_var(INPUT_POST, 'update')) {
            /**
             * header to updater with module info
             */
            if (filter_has_var(INPUT_POST, 'module')) {
                $securityHash = hash('sha512', $this->generateRandomString());
                Session::set('updateSecurityHash', $securityHash, true, false);
                Session::writeClose();
                header("Location: ../../updater/update.php?module=" . Input::sanitize(filter_input(INPUT_POST, 'module'), 'site') . "&sh=" . $securityHash);
            }
            if (filter_has_var(INPUT_POST, 'core')) {
                /**
                 * TODO:
                 * - (optional) enable maintainance mode
                 * - download latest core
                 * - move old files to backup
                 * - extract latest core
                 * - (optional) run update scripts
                 */
                if ($this->downloadCoreNewVersion()) {
                    if ($this->unzipCoreNewVersion()) {
                        if ($this->copyUpdaterNewVersion()) {
                            $securityHash = hash('sha512', $this->generateRandomString());
                            Session::set('updateSecurityHash', $securityHash, true, false);
                            Session::writeClose();
                            header("Location: ../../updater/update.php?core&sh=" . $securityHash);
                        } else {
                            $this->setErrorMessage($this->ts("Could not update Updater! Check your servers permissions."));
                        }
                    } else {
                        $this->setErrorMessage($this->ts("Extracting the update package failed! Check your servers permissions."));
                    }
                } else {
                    $this->setErrorMessage($this->ts("Unable to download the latest release. Try again later."));
                }
                // header("Location: ../../updater/update.php?core");
            }
        } else {
            $this->main();
        }
    }

    private function copyUpdaterNewVersion() {
        $savePath = dirname(__FILE__) . "/../../cache/updates/core/updater/";
        $updaterPath = dirname(__FILE__) . "/../../updater/";
        //( delete old updater
        if (is_dir($updaterPath)) {
            $this->delTree($updaterPath);
        }

//        if (!is_dir($updaterPath)) {
//            mkdir($updaterPath, 0777, true);
//        }
        try {
            rename($savePath, $updaterPath);
            //copy($savePath."Updater.php", $updaterPath."Updater.php");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    private function delTree($dir) {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    private function downloadCoreNewVersion() {
        $savePath = dirname(__FILE__) . "/../../cache/updates/core/";
        if (!is_dir($savePath)) {
            mkdir($savePath, 0777, true);
        }
        $fileCachePath = $savePath . "latest.zip";

        if (is_file($fileCachePath)) {
            unlink($fileCachePath);
        }

        $url = Config::get("updateEndpoint");
        $url .= "?downloadLatestSystem";

        $ch = curl_init($url);
        $fp = fopen($fileCachePath, 'x');

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);

        fclose($fp);
        if (is_readable($fileCachePath)) {
            return true;
        } else {
            return false;
        }
    }

    private function unzipCoreNewVersion() {
        $savePath = dirname(__FILE__) . "/../../cache/updates/core/";
        $fileCachePath = $savePath . "latest.zip";
        if (is_file($fileCachePath)) {
            $zip = new ZipArchive;
            $res = $zip->open($fileCachePath);
            if ($res === TRUE) {
                $zip->extractTo($savePath);
                $zip->close();
                unlink($fileCachePath);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function createConfiguration() {
        $ConfigWriter = new ConfigWriter();
        $ConfigWriter->setArrayName('$moduleConfig');
        if ($ConfigWriter->createConfiguration(true) == FALSE) {
            // fail
            $this->setErrorMessage($this->ts("An Error occured while saving configuration. You might have to manually edit it. Sorry."));
        } else {
            $this->setSystemMessage($this->ts("Configuration saved successfully."));
        }
    }

    private function manageUpdates() {
        $this->setTitle($this->ts("Available module updates"));
        $data = array();
        $data['sites'] = array();
        $sites = $this->getSitesFromApplicationDir();
        $data['coreVersion'] = $this->getLocalCoreVersion();
        $data['remoteCoreVersion'] = $this->getRemoteCoreVersion();
        $data['coreUpdateAvailable'] = $this->isCoreUpdateAvailable();

        $updates = $this->areUpdatesAvailable($sites);
        //print_r($updates);
        foreach ($sites as $site) {
            foreach ($updates as $u) {
                if ($site['module'] == $u['module']) {
                    $site['update_version'] = $u['version'];
                    $data['sites'][] = $site;
                }
            }
        }

        $this->view("updates", $data);
    }

    private function isCoreUpdateAvailable() {
        $coreVer = explode('.', $this->getLocalCoreVersion());
        $remoteVer = explode('.', $this->getRemoteCoreVersion());
        for ($i = 0; $i < count($remoteVer); $i++) {
            if ($remoteVer[$i] > $coreVer[$i]) {
                return true;
            } else if ($remoteVer[$i] == $coreVer[$i]){
                continue;
            } else {
                return false;
            }
        }
        return false;
    }

    private function getLocalCoreVersion() {
        try {
            if (is_file("core/info.xml")) {
                $xml = simplexml_load_file("core/info.xml");
            }
        } catch (Exception $ex) {
            echo "$ex";
        }
        if ($xml == null) {
            return null;
        }
        return $xml['version'];
    }

    private function getRemoteCoreVersion() {
        $url = Config::get("updateEndpoint");
        $url .= "?getSystemVersion";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    private function main() {

        /**
         * DONE:
         * codeflow:
         * - fetch sites from db into array
         * - add modules from dir which are not installed
         * - 
         */

        $data = $this->getSitesData();

        $data['updatesAvailable'] = false; //($this->areUpdatesAvailable($data['sites']) || $this->isCoreUpdateAvailable());

        $data['type'] = 'user';
        $this->view('site', $data);
    }

    private function getSitesData() {
        $sitesArray = $this->getSitesFromApplicationDir();
        $data = array();
        foreach ($sitesArray as $site) {
            //echo $site['module']."<br>";
            $moduleFunctionArray = explode("/", $site['module']);
            if (isset($moduleFunctionArray[1]) && $moduleFunctionArray[1] === 'index') {
                $siteModule = $moduleFunctionArray[0];
            } else {
                $siteModule = $site['module'];
            }

            if (isset($moduleFunctionArray[1]) && $moduleFunctionArray[1] !== 'index') {
                // 
                $rs2 = $this->select('site_data', '`name`, title', "site_id='{$site['id']}' AND language='" . $this->getLanguage() . "'");
                if ($rs2 == FALSE) {
                    // fallback if selected language is not fully translated
                    $rs2 = $this->select('site_data', '`name`, title', "site_id='{$site['id']}' AND language='" . Config::get('defaultLanguage') . "'");
                }
                if($rs2 == FALSE) {
                    $data['sites'][$moduleFunctionArray[0]]['children'][] = $site;
                } else {
                    $data['sites'][$moduleFunctionArray[0]]['children'][] = array_merge($site, $rs2[0]);
                }
                //$data['sites'][$moduleFunctionArray[0]]['children'][] = array_merge($site, $rs2[0]);
            } else {

                $rs2 = $this->select('site_data', '`name`, title', "site_id='{$site['id']}' AND language='" . $this->getLanguage() . "'");
                if ($rs2 == FALSE) {
                    // fallback if selected language is not fully translated
                    $rs2 = $this->select('site_data', '`name`, title', "site_id='{$site['id']}' AND language='" . Config::get('defaultLanguage') . "'");
                }
                if ($rs2 === false) {
                    if (isset($data['sites'][$siteModule])) {
                        $data['sites'][$siteModule] = array_merge($site, $rs2[0], $data['sites'][$siteModule]);
                    } else {
                        $data['sites'][$siteModule] = array_merge($site, $rs2[0]);
                    }
                } else {
                    if (isset($data['sites'][$siteModule])) {
                        $data['sites'][$siteModule] = array_merge($site, $data['sites'][$siteModule]);
                    } else {
                        $data['sites'][$siteModule] = $site;
                    }
                }
            }
            if ($site['type'] === 's') {
                $data['sites'][] = $site;
            }
        }
        return $data;
    }

    private function areUpdatesAvailable($sites) {
        $csv = "";
        $modules = array();
        foreach ($sites as $site) {
            $modA = explode("/", $site["module"]);
            if (!in_array($modA[0], $modules)) {
                array_push($modules, $modA[0]);
                $csv = $this->appendCsv($csv, $modA[0] . ":" . $site['version']);
            }
        }

        //print_r( $csv . "<br/>" ); 
        $updateAnswer = $this->curlUpdateSite($csv);
        //print_r( "Update available for: " . $updateAnswer);exit;
        if ($updateAnswer == "") {
            return array();
        } else {
            $updateAnswerA = explode(";", $updateAnswer);

            $returnArray = array();
            foreach ($updateAnswerA as $ua) {

                $uaa = explode(":", $ua);
                array_push($returnArray, array('module' => $uaa[0], 'version' => $uaa[1]));
            }
            return $returnArray;
        }

        //return true;
    }

    private static function appendCsv($csv, $string) {
        if ($csv === "") {
            $csv = $string;
        } else {
            $csv .= ";" . $string;
        }
        return $csv;
    }

    private function curlUpdateSite($csv) {
        $url = Config::get("updateEndpoint");
        $url .= "?areModUpdatesAvailable=$csv";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    private function getSitesFromApplicationDir() {
        $d = scandir(Config::get('applicationDirectory'));
        $modulesArray = [];

        //$rs = $this->select("site", '*', "type!='a'", "active desc,rank asc");
        $rs = $this->select("site", '*', "1=1", "active desc,`rank` asc");

        foreach ($rs as $module) {

            $rs2 = $this->select('site_data', '`name`, title', "site_id='{$module['id']}' AND language='" . $this->getLanguage() . "'");
            if ($rs2 == FALSE) {
                // fallback if selected language is not fully translated
                $rs2 = $this->select('site_data', '`name`, title', "site_id='{$module['id']}' AND language='" . Config::get('defaultLanguage') . "'");
                $rs2[0]['name'] = '$L_' . $rs2[0]['name'];
                //print_r($module);
            }

            //if (strpos($module['module'], '/') === FALSE) {
            if ($module['type'] != 's') {
                $module['version'] = $this->getModuleInfo($module['module'], 'version');
                $module['description'] = $this->getModuleInfo($module['module'], 'description');
                $module['core-min'] = $this->getModuleInfo($module['module'], 'core-min');
            } else {
                $module['version'] = "";
                $module['description'] = "";
                $module['core-min'] = "";
            }

            //}

            array_push($modulesArray, array_merge($module, $rs2[0]));
        }

        return $this->getAdditionalModulesInfo($d, $modulesArray);
    }

    private function getAdditionalModulesInfo($d, $modulesArray) {
        foreach ($d as $module) {
            $modulePath = Config::get('applicationDirectory') . "/" . $module;
            //echo $modulePath;
            if (!is_dir($modulePath) || !is_file($modulePath . "/" . ucfirst($module . ".php")) || !is_file($modulePath . "/info.xml")) {
                /**
                 * no module or no install information, cannot be installed in db.
                 */
                continue;
            }
            //$xml = simplexml_load_file($modulePath . "/info.xml") or die("Error: Cannot create object");
            $version = $this->getModuleInfo($module, 'version');
            $coreMin = $this->getModuleInfo($module, 'core-min');
            $description = $this->getModuleInfo($module, 'description');

            $rs = $this->select("site", '*', "module='" . $module . "'");
            //print_r($rs);
            if ($rs === false || count($rs) === 0) {
                // module does not exist in db, make a dummy array
                array_push($modulesArray, array(
                    'id' => '',
                    'module' => $module,
                    'name' => $module,
                    'active' => '0',
                    'type' => 'd',
                    'role_need' => '',
                    'version' => $version,
                    'core-min' => $coreMin,
                    'description' => $description
                ));
            } else {
                // do nothing
            }
        }
        return $modulesArray;
    }

    /**
     *  Can only fetch descripton and version
     *  can fetch anything that goes as descrption sibling and has <de>, <gb> localized children.
     * @param String $module - modulename 
     * @param String $key - key
     * @return type
     */
    private function getModuleInfo($module, $key, $method="") {
        $xml = null;
        $keyValueArray = array();
        $moduleArray = explode('/', $module);
        $modulePath = Config::get('applicationDirectory') . "/";
        if (isset($moduleArray[1])) {
            $modulePath .= $moduleArray[0];
        } else {
            $modulePath .= $module;
            $moduleArray[1] = $method;
        }
        try {
            if (is_file($modulePath . "/info.xml")) {
                $xml = simplexml_load_file($modulePath . "/info.xml");
            }
        } catch (Exception $ex) {
            echo "$ex";
        }
        if ($xml == null) {
            return null;
        }
        if ($key == "version") {
            return $xml['version'];
        }
        if ($key == "core-min") {
            return $xml['core-min'];
        }

        $lang = $this->getLanguage();
        foreach ($xml->children() as $child) {

            if ($child->getName() == "method" && $child['name'] == $moduleArray[1]) {
                // echo "  ".$module." -> $key";
                foreach ($child->children() as $grands) {
                    if ($grands->getName() == $key) {
                        $keyValueArray[$key] = $grands->{$lang}[0];
                    }
                }

                break;
            }

            if ($child->getName() == "description" && $key == "description" && $moduleArray[1] == "") {
                //echo "  ".$module." -> $key";
                $keyValueArray[$key] = $child->{$lang}[0];
                break;
            }
        }

        if (isset($keyValueArray[$key])) {
            return $keyValueArray[$key];
        }
        return null;
    }

    private function activateModule() {
        // just update all modules
        $module = filter_input(INPUT_POST, 'module');
        $rs = $this->select('site', '*', "module LIKE '$module'");
        if ($rs !== FALSE) {
            foreach ($rs as $c) {
                $f = array(
                    'active' => '1'
                );
                $this->updatePrepare('site', $f, "id=:id", array('id' => $c['id']));
            }
        }
        $module = filter_input(INPUT_POST, 'module');
        $this->installModule($module);

        $rs2 = $this->selectPrepare('site', 'id', "module LIKE :module", array('module' => $module."%"));
        $id = $rs2[0]['id'];
        $this->setSystemMessage($this->ts("Module $1 activated", array(1 => $module)));
        $this->jumpTo('site/admin?edit=' . $id);
    }

    private function installModule($module) {
        $Installer = new Installer();
        $Installer->installModules(array(0 => $module));
    }

    private function deactivateModule() {
        if (filter_has_var(INPUT_POST, 'id')) {
            // just update all modules
            //$id = filter_input(INPUT_POST, 'module');
            $module = filter_input(INPUT_POST, 'module');
            $rs = $this->selectPrepare('site', '*', "module LIKE :module", array('module' => $module."%"));
            foreach ($rs as $c) {
                $f = array(
                    'active' => '0'
                );
                $this->updatePrepare('site', $f, "id=:id", array('id' => $c['id']));
            }
            $this->setSystemMessage($this->ts("Module $1 deactivated", array(1 => $module)));
        }
    }

    /**
     * deletes module from db, 
     * does not delete module tables from db.
     */
    private function deleteModuleFromDb() {
        if (filter_has_var(INPUT_POST, 'id')) {
            // just update all modules
            $module = filter_input(INPUT_POST, 'module');
            if (substr($module, 0, 4) === 'site') {
                // do not delete site!
                return;
            }

            $rs = $this->select('site', '*', "module LIKE '$module%'");
            foreach ($rs as $c) {
                $this->delete('site_data', "site_id='" . $c['id'] . "'");
                $this->delete('site', "id='" . $c['id'] . "'");
            }
            $this->setSystemMessage($this->ts('$1 deleted from database.', array(1 => $module)));
        }
    }

    private function deleteSiteFromDb() {
        if (filter_has_var(INPUT_POST, 'id')) {
            $id = filter_input(INPUT_POST, 'id');
            $rs = $this->select('site', '*', "id = '$id'");
            $this->delete('site_data', "site_id='" . $id . "'");
            $this->delete('site', "id='" . $id . "'");
        }
    }

//    private function manageAdmin() {
//        $rs = $this->select("site", '*', "type='a'", "active desc, rank asc");
//
//        foreach ($rs as $site) {
//            $site['version'] = $this->getModuleInfo($site['module'], 'version');
//            $site['description'] = $this->getModuleInfo($site['module'], 'description');
//            $rs2 = $this->select('site_data', 'name, title', "site_id='{$site['id']}' AND language='" . $this->getLanguage() . "'");
//            if ($rs2 == FALSE) {
//                // fallback if selected language is not fully translated
//                $rs2 = $this->select('site_data', 'name, title', "site_id='{$site['id']}' AND language='" . Config::get('defaultLanguage') . "'");
//                $rs2[0]['name'] = '$L_' . $rs2[0]['name'];
//            }
//            $data['sites'][] = array_merge($site, $rs2[0]);
//        }
//
//        $data['type'] = 'admin';
//        $this->view('site', $data);
//    }

    private function showStatic() {
        //edit link
        if ($this->checkSiteRights('site/admin')) {
            $data['editLink'] = true;
        } else {
            $data['editLink'] = false;
        }
        if (filter_has_var(INPUT_GET, 'show')) {

            $name = urldecode(filter_input(INPUT_GET, 'show'));
            //$name = Input::sanitize($name, 'site');

            $rs = $this->select('site', '*', "unique_name='$name'");
            if ($rs == FALSE) {
                echo "404";
                return;
            }
            if ($this->checkUserRights($rs[0]['role_need'])) {
                $rs2 = $this->select('site_data', '*', "site_id='{$rs[0]['id']}' AND language='" . $this->getLanguage() . "'");

                $data['site'] = array_merge($rs2[0], $rs[0]);
                $data['site']['content'] = $this->obfuscateEmail($data['site']['content']);
                $this->setName($data['site']['unique_name']);
                $this->setTitle($data['site']['title']);

                $this->view('static', $data);
            } else {
                //nothing... pff..
            }
        } else {

            $rs = $this->select('site', '*', "active=1 AND type='s'", 'rank ASC', '0,1');
            //print_r($rs);
            if ($this->checkUserRights($rs[0]['role_need'])) {
                $rs2 = $this->select('site_data', '*', "site_id='{$rs[0]['id']}' AND language='" . $this->getLanguage() . "'");

                $data['site'] = array_merge($rs2[0], $rs[0]);
                $data['site']['content'] = $this->obfuscateEmail($data['site']['content']);
                $this->setName($data['site']['unique_name']);
                $this->setTitle($data['site']['title']);
                $this->view('static', $data);
            }
        }
        /**
         * Cache Headers
         */
//        $date = new DateTime($data['site']['created']);
//        // seconds, minutes, hours, days
//        $expires = 60 * 60 * 24 * 14;
//        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
//        header("Last-Modified: " . $date->format("D, d M Y H:i:s") . " GMT");
//
//        header("Cache-Control: private, must-revalidate");
//        header("Pragma: private");
    }

    private function updateStatic() {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT); //filter_input(INPUT_POST, 'id');
        $name = filter_input(INPUT_POST, 'name');
        $unique_name = filter_input(INPUT_POST, 'unique_name');
        if ($unique_name == '') {
            // if you saved without a name and update without setting a new unique name
            $unique_name = $name;
        }
        $editLang = filter_input(INPUT_POST, 'editLang');
        $module = "site/" . $this->encodeTitleForModRewrite($unique_name); //filter_input(INPUT_POST, 'module');
        $rank = filter_input(INPUT_POST, 'rank');
        $content = filter_input(INPUT_POST, 'text');
        $title = filter_input(INPUT_POST, 'title');
        $active = '0';
        if (filter_has_var(INPUT_POST, 'active')) {
            $active = '1';
        }
        $hide = '0';
        if (filter_has_var(INPUT_POST, 'hide')) {
            $hide = '1';
        }

        $roleNeed = join(',', filter_input(INPUT_POST, 'role_need', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));

        $fieldsValueArray = array(
            'unique_name' => $unique_name,
            'module' => $module,
            'active' => $active,
            'hide' => $hide,
            'rank' => $rank,
            'role_need' => $roleNeed
        );
        $this->updatePrepare('site', $fieldsValueArray, "id=:id", array('id' => $id));


        if (FALSE == $this->selectPrepare('site_data', "id", "site_id=:id AND language=:editLang", array('id' => $id, 'editLang' => $editLang))) {
            // does not exist yet.
            // insert data
            $fieldsValueArray2 = array(
                'title' => $title,
                'name' => $name,
                'content' => $content,
                'author' => ''
            );
            $fieldsValueArray2['site_id'] = $id;
            $fieldsValueArray2['language'] = $editLang;
            if ($this->insert('site_data', $fieldsValueArray2)) {
                $this->setSystemMessage($this->ts('Updated successfully'));
            } else {
                $this->setErrorMessage($this->ts('Could not updated site'));
            }
        } else {
            // update data
            $fieldsValueArray2 = array(
                'title' => $title,
                'name' => $name,
                'content' => $content,
                'author' => ''
            );
            if ($this->updatePrepare('site_data', $fieldsValueArray2, "site_id=:id AND language=:editLang", array('id' => $id, 'editLang' => $editLang))) {
                $this->setSystemMessage($this->ts('Updated successfully'));
            } else {
                $this->setErrorMessage($this->ts('Could not updated site'));
            }
        }
        Link::jumpToLast();
        //Link::gotoModule('site/' . urlencode($unique_name));
    }

    private function updateModule() {
        $id = filter_input(INPUT_POST, 'id');
        $name = filter_input(INPUT_POST, 'name');
        $editLang = filter_input(INPUT_POST, 'editLang');
        $title = filter_input(INPUT_POST, 'title');
        $module = filter_input(INPUT_POST, 'module');
        $rank = filter_input(INPUT_POST, 'rank');
        $active = '0';
        if (filter_has_var(INPUT_POST, 'active')) {
            $active = '1';
        }
        $hide = '0';
        if (filter_has_var(INPUT_POST, 'hide')) {
            $hide = '1';
        }
        $type = 'd';
        if (filter_has_var(INPUT_POST, 'admin')) {
            $type = 'a';
        }

        $roleNeed = join(',', filter_input(INPUT_POST, 'role_need', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));

        $fieldsValueArray = array(
            'type' => $type,
            'module' => $module,
            'active' => $active,
            'hide' => $hide,
            'role_need' => $roleNeed,
            'rank' => $rank
        );
        $this->updatePrepare('site', $fieldsValueArray, "id=:id", array('id' => $id));

        $fieldsValueArrayData = array(
            'title' => $title,
            'name' => $name
        );
        if ($this->updatePrepare('site_data', $fieldsValueArrayData, "site_id=:id AND language=:editLang", array('id' => $id, 'editLang' => $editLang))) {
            $this->setSystemMessage($this->ts('Updated successfully'));
        } else {
            $this->setErrorMessage($this->ts('Could not updated module'));
        }
    }

    private function updateRank() {

        $mods = explode(',', filter_input(INPUT_POST, 's'));
        $n = 0;
        foreach ($mods as $mod) {
            ++$n;
            //echo $mod." ".$n;

            $fieldsValueArray = array(
                'rank' => "$n"
            );
            $this->updatePrepare("site", $fieldsValueArray, "module=:mod", array('mod' => $mod));
        }
        echo $this->ts("Changes Saved");
    }

    private function viewEditStatic() {

        $this->includeCkeditor('javascript/ck_config/ck_default.js');
        //$this->includeJs('javascript/ck_config/ck_default.js', 'single');

        $id = filter_input(INPUT_GET, 'editStatic');
        if (filter_has_var(INPUT_GET, 'editLang')) {
            $data['editLang'] = filter_input(INPUT_GET, 'editLang');
        } else {
            $data['editLang'] = Config::get('defaultLanguage');
        }

        $rs = $this->selectPrepare('site', '*', "id=:id", array('id' => $id));
        $data['enabledLanguages'] = Config::get('enabledLanguages');
        $data['roles'] = $this->select('user_role');
        $data['rnArray'] = explode(',', $rs[0]['role_need']);
        $rs2 = $this->selectPrepare('site_data', 'name,title,content,author,language', "site_id=:id AND language=:editLang", array('id' => $id, 'editLang' => $data['editLang']));
        if (!$rs2) {
            $data['site'] = $rs[0];
        } else {
            $data['site'] = array_merge($rs[0], $rs2[0]);
        }

        if ($data['site']['active'] == '1') {
            $data['site']['active'] = "checked='checked'";
        } else {
            $data['site']['active'] = "";
        }
        if ($data['site']['hide'] == '1') {
            $data['site']['hide'] = "checked='checked'";
        } else {
            $data['site']['hide'] = "";
        }
//        if ($data['site']['type'] == 'a') {
//            $data['site']['admin'] = "checked='checked'";
//        } else {
//            $data['site']['admin'] = "";
//        }

        $this->view('editStatic', $data);
    }

    private function viewEditModule() {
        $id = filter_input(INPUT_GET, 'edit');
        if (filter_has_var(INPUT_GET, 'editLang')) {
            $data['editLang'] = filter_input(INPUT_GET, 'editLang');
        } else {
            $data['editLang'] = Config::get('defaultLanguage');
        }
        $rs = $this->selectPrepare('site', '*', "id=:id", array('id' => $id));

        $data['enabledLanguages'] = Config::get('enabledLanguages');
        $data['roles'] = $this->select('user_role');

        $rs2 = $this->selectPrepare('site_data', '`name`,title,content,author,language', "site_id=:id AND language=:editLang", array('id' => $id, 'editLang' => $data['editLang']));
        if (!$rs2) {
            $site = $rs[0];
        } else {
            $site = array_merge($rs[0], $rs2[0]);
        }
        $site['rnArray'] = explode(',', $rs[0]['role_need']);
        if ($site['active'] == '1') {
            $site['active'] = "checked='checked'";
        } else {
            $site['active'] = "";
        }
        if ($site['hide'] == '1') {
            $site['hide'] = "checked='checked'";
        } else {
            $site['hide'] = "";
        }
        if ($site['type'] == 'a') {
            $site['admin'] = "checked='checked'";
        } else {
            $site['admin'] = "";
        }
        $data['moduleConfiguration'] = "";
        $data['module'] = $site['module'];
        $data['config'] = $this->loadModuleConfiguration($site['module']);

        if (sizeof($data['config']) > 0) {
            $data['configLegend'] = $this->ts("Configuration"); // translate before switching to module lang file.
            $data['saveButtonValue'] = $this->ts('save configuration');
            // load lang files from module, to have translation data in modules.
            $this->loadLang(Config::get("applicationDirectory") . "/" . $site['module'] . "/", $this->getLanguage());
            $data['moduleConfiguration'] = $this->fetch('config', $data);
            // switch back to site lang files
            $this->loadLang(Config::get("applicationDirectory") . "/site/", $this->getLanguage());
        }

        $children = $this->getModuleChildren($rs[0]['module'], $data['editLang']);
        $sites[0] = $site; //$children;//array_push(,$site);
        $sites = array_merge($sites, $children);
        foreach ($sites as $s) {
            //echo $s['module'];
            $s['description'] = $this->getModuleInfo($s['module'], 'description', 'index');
            $data['sites'][] = $s;
        }
        //$data['sites'] = $sites;

        $this->view('editModule', $data);
    }

    private function getModuleChildren($module, $lang) {
        $childrenArray = [];
        $rs = $this->selectPrepare('site', '*', "module LIKE :module", array('module' => $module."/%"));
        foreach ($rs as $c) {
            $rs2 = $this->selectPrepare('site_data', 'name,title,content,author,language', "site_id=:id AND language=:editLang", array('id' => $c['id'], 'editLang' => $lang));
            if (!$rs2) {
                //$c = $c;
            } else {
                $c = array_merge($c, $rs2[0]);
            }
            $c['rnArray'] = explode(',', $c['role_need']);
            if ($c['active'] == '1') {
                $c['active'] = "checked='checked'";
            } else {
                $c['active'] = "";
            }
            if ($c['hide'] == '1') {
                $c['hide'] = "checked='checked'";
            } else {
                $c['hide'] = "";
            }
            if ($c['type'] == 'a') {
                $c['admin'] = "checked='checked'";
            } else {
                $c['admin'] = "";
            }
            $childrenArray[] = $c;
        }
        return $childrenArray;
    }

    private function viewAddStatic() {

        $this->includeCkeditor('javascript/ck_config/ck_default.js');

        $data['roles'] = $this->getRoles('50');
        $this->view('addStatic', $data);
    }

    private function insertStatic() {
        $title = filter_input(INPUT_POST, 'title');
        $content = filter_input(INPUT_POST, 'text');
        $name = filter_input(INPUT_POST, 'name');
        $roleNeed = join(',', filter_input(INPUT_POST, 'role_need', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));
        if (filter_has_var(INPUT_POST, 'active'))
            $active = '1';
        else
            $active = '0';
        if (filter_has_var(INPUT_POST, 'hide'))
            $hide = '1';
        else
            $hide = '0';
        //$language = filter_input(INPUT_POST, 'lang');
        //$parent = filter_input(INPUT_POST, 'parent');

        $fieldsValueArray = array(
            'unique_name' => $name,
            'type' => 's',
            'module' => "site/" . $this->encodeTitleForModRewrite($name) ,
            'role_need' => $roleNeed,
            'active' => $active,
            'hide' => $hide,
            'rank' => "0",
            'parent' => ''
        );
        $newId = $this->insert('site', $fieldsValueArray);
        if (!$newId) {
            $this->setErrorMessage("$name " . $this->ts('could not be added'));
        } else {
            $langs = Config::get('enabledLanguages');
            foreach ($langs as $lang) {
                $fieldsValueArray = array(
                    'title' => $title,
                    'name' => $name,
                    'content' => $content,
                    'site_id' => $newId,
                    'language' => $lang
                );
                $c = $this->insert('site_data', $fieldsValueArray);
            }
            if (!$c) {
                $this->setErrorMessage("$name " . $this->ts('could not be added'));
            } else {
                $this->setSystemMessage("$name " . $this->ts('added successfully to sites'));
            }
        }
    }

    private function setStaticCacheHeader() {

        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT\n");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    }

    public function searchModule($searchString) {
        /**
         * DONE: SECURITY
         * * check rights for search...
         */
        if (!$this->checkSiteRights('site')) {
            return array();
        }
        $result = array();
        // first grade
        // $rs1 = $this->select('blog', '*', "active=1 AND (text LIKE '%$searchString%' OR title LIKE '%$searchString%')");
        //second grade
        $ss = explode(' ', $searchString);

        $qs = " (sd.name LIKE ' %$searchString% ' OR sd.title LIKE ' %$searchString% ' OR sd.content LIKE ' %$searchString% ')";

        $n = true;
        foreach ($ss as $s) {
            if ($n)
                $qs .= " OR ((sd.name LIKE '%$s%' OR sd.title LIKE '%$s%' OR sd.content LIKE '%$s%')";
            else
                $qs .= " AND (sd.name LIKE '%$s%' OR sd.title LIKE '%$s%' OR sd.content LIKE '%$s%')";
            $n = false;
        }
        $qs .= ')';
        $rs = $this->select('site as s, ' . Config::get('dbPrefix') . 'site_data as sd', '*', "s.id=sd.site_id AND s.active=1 AND s.hide=0 AND sd.language='" . $this->getLanguage() . "' AND ($qs)");

        //$rs = array_merge($rs1, $rs2);
        //$rs = array_unique($rs);

        foreach ($rs as $r) {
            if ($this->checkUserRights($r['role_need'])) {
                $shorttext = $this->obfuscateEmail(substr(strip_tags($r['content']), 0, 500));
                foreach ($ss as $s) {
                    $shorttext = str_replace($s, "<b>$s</b>", $shorttext);
                }
                $r['title'] = htmlspecialchars_decode($r['title']);
                $r['text'] = $shorttext;
                if ($r['type'] === 's') {
                    $r['link'] = 'site/' . $this->encodeTitleForModRewrite($r['unique_name']);
                } else {
                    $r['link'] = $r['module'];
                }

                $result[] = $r;
            }
        }
        return $result;
    }
    
    /**
     * 
     * @global array $aConfig
     * @param array $modules
     * @return array
     */
    private function getUpdateSqlFiles($modules) {
        $aConfig = Config::getConfig();
        $sqlFiles = array();
        $directory = $aConfig['applicationDirectory'];
//$modules = scandir($directory);

        foreach ($modules as $module) {
            if (is_dir($directory . '/' . $module . '/db/update/')) {
                $sqlFiles = array_merge($sqlFiles, $this->getUpdateSqlFilesFromDir($directory . '/' . $module . '/db/'));
            }
        }
        return $sqlFiles;
    }

    private function getUpdateSqlFilesFromDir($dirPath) {
        $sqlFiles = array();
        $sql = scandir($dirPath);
        foreach ($sql as $file) {
            if (substr($file, 0, 1) != '.' && !$this->tableExists(basename($file, '.sql'))) {
                $sqlFiles[] = $dirPath . $file;
            }
        }
        return $sqlFiles;
    }

}
