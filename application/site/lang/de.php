<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Manage sites' =>
	'Seitenverwaltung'
	,
	'Title'=>
	'Titel'
	,
	'Module'=>
	'Modul'
	,
	'Role need'=>
	'Berechtigte Rolle(n)'
	,
	'Active'=>
	'Aktiv'
	,
	'Email'=>
	'E-Mail'
	,
	'Add user'=>
	'Nutzer hinzufügen'
	,
	'Rank'=>
	'Ebene'
	,
	'Language'=>
	'Sprache'
	,
	'Add site'=>
	'Seite hinzufügen'
	,
	'Content'=>
	'Inhalt'
	,
	'Add static site'=>
	'Statische Seite hinzufügen'
	,
	'Add module'=>
	'Modul hinzufügen'
	,
	'Add a static site'=>
	'Füge eine statische Seite hinzu'
	,
	'Add a module'=>
	'Füge ein Modul hinzu'
	,
	'Initialize modules'=>
	'Installierte Module hinzufügen'
	,
	'Manage admin modules'=>
	'Verwalte Admin Module'
	,
	'Manage user modules'=>
	'Verwalte Nutzer Module'
	,
	'Unsufficiant rights to access this page'=>
	'Nicht genügend Rechte für diese Seite'
	,
	'Edit'=>
	'Bearbeiten'
	,
	'Save Changes'=>
	'Speichere Änderungen'
	,
	'Hidden'=>
	'Versteckt'
	,
	'Name'=>
	'Name'
	,
	'Is admin module'=>
	'In Account-Navigationsmenü anzeigen'
	,
	'Everybody'=>
	'Jeder Besucher'
	,
	'Updated successfully'=>
	'Erfolgreich aktualisiert'
	,
	'added successfully to sites'=>
	'erfolgreich zu Seiten hinzugefügt'
	,
	'could not be added'=>
	'konnte nicht hinzugefügt werden'
	,
	'Unsufficiant rights.'=>
	'Unzureichende Rechte.'
	,
	'Save the order'=>
	''
	,
	'Changes Saved'=>
	'Änderung gespeichert'
	,
	'Unique ID'=>
	'Schlüssel'
	,
	'Link'=>
	'Verweis'
	,
	'Cancel'=>
	'Abbrechen'
	,
	'already there'=>
	'schon vorhanden.'
	,
	'Insufficient rights!'=>
	'Ungenügende Rechte!'
	,
	'edit'=>
	'Seite bearbeiten'
	,
	'Role needed'=>
	'Benötigte Rolle'
	,
	'activate'=>
	'Aktivieren'
	,
	'deactivate'=>
	'Deaktivieren'
	,
	'back'=>
	'zurück'
	,
	'module \'$1\' does not have a class file.'=>
	'Modul \'$1\' hat keine Klasse. ;)'
	,
	'version'=>
	'Version'
	,
	'Delete module from DB'=>
	'Lösche Modul aus der Datenbank'
	,
	'The title is shown in the web-browsers titlebar.'=>
	'Der Titel wird in der Titelleiste des Webbrowsers angezeigt.'
	,
	'The name defines the label for this specific page in the navigation.'=>
	'Der Name bestimmt das Label der Navigation für diese Seite.'
	,
	'Active Pages can be accessed by defined roles.'=>
	'Nur aktive Seiten können von den definierten Rollen aufgerufen werden.'
	,
	'Setting the page to hidden, hides the page from the navigation, but is still accessible.'=>
	'Eine versteckte Seite wird in der Navigation ausgeblendet, ist aber über die URL erreichbar.'
	,
	'No specific role required'=>
	'Keine besondere Rolle erforderlich'
	,
	'Specify roles that can access this page.'=>
	'Schränke das Zugriffsrecht auf diese Seite auf bestimmte Rollen ein. Halte [Strg] gedrückt um mehrere Rollen aus zu wählen.'
	,
	'Unique ID for this page. Must conform with the part of link after \'site/\''=>
	'Schlüssel dieser Seite. Mit \'site/Schlüssel\' kann diese Seite über die URL aufgerufen werden.'
	,
	'Rank specifies the position in the navigation menu.'=>
	'Bestimmt die Position im Navigationsmenü.'
	,
	'Relative URL for this page without localization. Must conform with Unique ID.\''=>
	'Relative URL zu dieser Seite.'
	,
	'Could not updated site'=>
	'Fehler: Konnte die Seite nicht aktualisieren.'
	,
	'Edit Module'=>
	'Konfiguriere Modul'
	,
	'Edit page'=>
	'Bearbeite Seite'
	,
	'Delete site'=>
	'Lösche Seite'
	,
	'Do you really want to delete this site forever?'=>
	'Möchtest Du diese Seite wirklich für immer löschen?'
	,
	'Do you really want to delete this module forever?'=>
	'Möchtest Du dieses Modul wirklich aus der DB löschen?'
	,
	'Configuration'=>
	'Konfiguration'
	,
	'friendsEnabled_helpText'=>
	'Wird diese Option aktiviert, kann man nur Beiträge von Nutzern sehen, die einem selbst folgen.'
	,
	'save configuration'=>
	'Speichere Konfiguration'
	,
	'An Error occured while saving configuration. You might have to manually edit it. Sorry.'=>
	'Ein Fehler ist beim speichern der Konfiguration aufgetreten. Überprüfe die Schreibrechte.'
	,
	'Configuration saved successfully.'=>
	'Konfiguration erfolgreich gespeichert.'
	,
	'$1 deleted from database.'=>
	'$1 wurde aus der Datenbank gelöscht.'
	,
	'Everyone can see and use this.'=>
	'Jeder Besucher kann diese Seite sehen und nutzen.'
	,
	'Only Superadmins can see and use this.'=>
	'Nur Superadmins können diese Seite sehen und nutzen.'
	,
	'No rights set at all. Be careful.'=>
	'Es wurden keine Rechte gesetzt.'
	,
	'Users might see and use this.'=>
	'Es gibt Nutzer die diese Seite sehen und nutzen können.'
	,
	'Legend'=>
	'Legende'
	,
	'Module $1 activated'=>
	'Modul $1 erfolgreich aktiviert!'
	,
	'Module $1 deactivated'=>
	'Modul $1 erfolgreich deaktiviert!'
	,
	'Updates'=>
	'Aktualisierungen'
	,
	'Downloads'=>
	'Downloads'
	,
	'update'=>
	'Aktualisieren'
	,
	'Updated $1 successfully!'=>
	'$1 wurde erfolgreich aktualisiert!'
	,
	'No updates available at the moment.'=>
	'Im Moment sind keine Aktualisierungen verfügbar.'
	,
	'Could not backup old module! Check your servers permissions.'=>
	'Konnte kein Backup der alten Version anlegen. Aktualisierung abgebrochen.'
	,
	'Available Updates'=>
	'Verfügbare Aktualisierungen'
	,
	'Available module updates'=>
	'Verfügbare Modul Aktualisierungen'
	,
	'Something went wrong with the download! Please try again later.'=>
	'Der Download ist fehlgeschlagen. Bitte versuche es später noch einmal.'
	,
	'Not implemented, yet.'=>
	'Funktion noch nicht verfügbar.'
	,
	'update core to version $1'=>
	'Auf Version $1 aktualisieren'
	,
	'Before updating the core, you should install all available updates for modules.'=>
	'Eine Aktualisierung des Systems überschreibt ggf. Änderungen an .htaccess, .user.ini und robots.txt.'
	,
	'sendEmail'=>
	'E-Mail senden'
	,
	'send@email.com'=>
	'email@senden.de'
	,
	'successfully initialized'=>
	'erfolgreich initialisiert.'
);