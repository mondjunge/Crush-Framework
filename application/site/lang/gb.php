<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Manage sites' =>
	'Manage sites'
	,
	'Title'=>
	'Title'
	,
	'Module'=>
	'Module'
	,
	'Role need'=>
	'Role need'
	,
	'Active'=>
	'Active'
	,
	'Email'=>
	'Email'
	,
	'Add user'=>
	'Add user'
	,
	'Rank'=>
	'Rank'
	,
	'Language'=>
	'Language'
	,
	'Add site'=>
	'Add site'
	,
	'Content'=>
	'Content'
	,
	'Add static site'=>
	'Add static site'
	,
	'Add module'=>
	'Add module'
	,
	'Add a static site'=>
	'Add a static site'
	,
	'Add a module'=>
	'Add a module'
	,
	'Initialize modules'=>
	'Initialize modules'
	,
	'Save the order'=>
	'Save the order'
	,
	'Manage admin modules'=>
	'Manage admin modules'
	,
	'Manage user modules'=>
	'Manage user modules'
	,
	'Unsufficiant rights to access this page'=>
	'Unsufficiant rights to access this page'
	,
	'Edit'=>
	'Edit'
	,
	'Save Changes'=>
	'Save Changes'
	,
	'Hidden'=>
	'Hidden'
	,
	'Name'=>
	'Name'
	,
	'Is admin module'=>
	'Show in account-navigation'
	,
	'Everybody'=>
	'Everybody'
	,
	'Updated successfully'=>
	'Updated successfully'
	,
	'added successfully to sites'=>
	'added successfully to sites'
	,
	'could not be added'=>
	'could not be added'
	,
	'Unsufficiant rights.'=>
	'Unsufficiant rights.'
	,
	'Changes Saved'=>
	'Changes Saved'
	,
	'Unique ID'=>
	'Unique ID'
	,
	'Link'=>
	'Link'
	,
	'Cancel'=>
	'Cancel'
	,
	'already there'=>
	'already there'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'edit'=>
	'edit'
	,
	'active'=>
	'active'
	,
	'roles'=>
	'roles'
	,
	'Role needed'=>
	'Role needed'
	,
	'activate'=>
	'activate'
	,
	'deactivate'=>
	'deactivate'
	,
	'back'=>
	'back'
	,
	'module \'$1\' does not have a class file.'=>
	'module \'$1\' does not have a class file.'
	,
	''=>
	''
	,
	'n/a'=>
	'n/a'
	,
	'version'=>
	'version'
	,
	'Delete module from DB'=>
	'Delete module from DB'
	,
	'The title is shown in the web-browsers titlebar.'=>
	'The title is shown in the web-browsers titlebar.'
	,
	'The name defines the label for this specific page in the navigation.'=>
	'The name defines the label for this specific page in the navigation.'
	,
	'Active Pages can be accessed by defined roles.'=>
	'Active Pages can be accessed by defined roles.'
	,
	'Setting the page to hidden, hides the page from the navigation, but is still accessible.'=>
	'Setting the page to hidden, hides the page from the navigation, but is still accessible.'
	,
	'No specific role required'=>
	'No specific role required'
	,
	'Specify roles that can access this page.'=>
	'Specify roles that can access this page. Hold down [Ctrl] to select more than one role.'
	,
	'Unique ID for this page. Must conform with the part of link after \'site/\''=>
	'Unique ID for this page. Load this site with \'site/Unique ID\''
	,
	'Rank specifies the position in the navigation menu.'=>
	'Rank specifies the position in the navigation menu.'
	,
	'Relative URL for this page without localization. Must conform with Unique ID.\''=>
	'Relative URL for this page without localization. Must conform with Unique ID.\''
	,
	'Could not updated site'=>
	'Could not updated site'
	,
	'Edit Module'=>
	'Edit Module'
	,
	'Edit page'=>
	'Edit page'
	,
	'Delete site'=>
	'Delete site'
	,
	'Do you really want to delete this site forever?'=>
	'Do you really want to delete this site forever?'
	,
	'Do you really want to delete this module forever?'=>
	'Do you really want to delete this module forever?'
	,
	'Configuration'=>
	'Configuration'
	,
	'Configuration Name'=>
	'Configuration Name'
	,
	'configname_helpText'=>
	'configname_helpText'
	,
	'friendsEnabled_helpText'=>
	'friendsEnabled_helpText'
	,
	'save configuration'=>
	'save configuration'
	,
	'An Error occured while saving configuration. You might have to manually edit it. Sorry.'=>
	''
	,
	'New Configuration saved successfully.'=>
	''
	,
	'Configuration saved successfully.'=>
	'Configuration saved successfully.'
	,
	'$1 deleted from database.'=>
	'$1 deleted from database.'
	,
	'apiToken_helpText'=>
	'apiToken_helpText'
	,
	'Everyone can see and use this.'=>
	'Everyone can see and use this.'
	,
	'Only Superadmins can see and use this.'=>
	'Only Superadmins can see and use this.'
	,
	'No rights set at all. Be careful.'=>
	'No rights set at all. Be careful.'
	,
	'Users might see and use this.'=>
	'Users might see and use this.'
	,
	'Legend'=>
	'Legend'
	,
	'Module activated'=>
	'Module activated'
	,
	'Module deactivated'=>
	'Module deactivated'
	,
	'Module $1 activated'=>
	'Module $1 activated'
	,
	'Module $1 deactivated'=>
	'Module $1 deactivated'
	,
	'Updates'=>
	'Updates'
	,
	'Downloads'=>
	'Downloads'
	,
	'update'=>
	'update'
	,
	'Updated $1 successfully!'=>
	'Updated $1 successfully!'
	,
	'No updates available at the moment.'=>
	'No updates available at the moment.'
	,
	'Could not backup old module! Check your servers permissions.'=>
	'Could not backup old module! Check your servers permissions.'
	,
	'Available module updates'=>
	'Available module updates'
	,
	'Available Updates'=>
	'Available Updates'
	,
	'Something went wrong with the download! Please try again later.'=>
	'Something went wrong with the download! Please try again later.'
	,
	'Not implemented, yet.'=>
	'Not implemented, yet.'
	,
	'update core to version $1'=>
	'update core to version $1'
	,
	'Before updating the core, you should install all available updates for modules.'=>
	'Before updating the core, you should install all available updates for modules.'
	,
	'sendEmail'=>
	'Send e-mail'
	,
	'send@email.com'=>
	'send@email.com'
	,
	'successfully initialized'=>
	'successfully initialized'
);