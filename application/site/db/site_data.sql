CREATE TABLE IF NOT EXISTS `{dbprefix}site_data` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `site_id` bigint NOT NULL,
  `name` text NOT NULL DEFAULT '',
  `title` text NOT NULL DEFAULT '',
  `content` longtext NULL,
  `author` text NOT NULL DEFAULT '',
  `language` char(2) NOT NULL DEFAULT 'gb',
  PRIMARY KEY (`id`)
);
