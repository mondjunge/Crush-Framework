CREATE TABLE IF NOT EXISTS `{dbprefix}site` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `unique_name` VARCHAR(191) NOT NULL,
  `type` char(1) NOT NULL DEFAULT 'd', 
  `module` VARCHAR(191) NOT NULL,
  `role_need` VARCHAR(191) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  `parent` bigint NULL DEFAULT NULL,
  `version` varchar(32) NULL DEFAULT NULL,
  `created` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`), UNIQUE(`unique_name`)
);
