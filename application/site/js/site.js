/**
 * site
 */

use_package('modules');
modules.site = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    

    this.init = function () {
        utils.string.showEmailHandler(); // show obfuscated E-Mails
        handleImageRotator();
        $(".mapLocation").mapLocation(); 
        //check for updates via ajax
        if (typeof $('#availableUpdates').length !== undefined) {
            $.get(reLangUrl + '/site/admin?areUpdatesAvailable',
                    function (data) {
                        //var content = $( data ).find( '#content' );
                        console.log("return: " + data);
                        if (data == 'true') {
                            $("#availableUpdates").addClass("pure-button-primary");
                        } else {
                            $("#availableUpdates").attr("disabled", "disabled");
                        }
                    }
            );
        }

        $(".sortable").sortable({
            stop: function (event, ui) {
                // alert("click "+$( ".ui-state-default" ).length);
                var uiEs = $("div.siteModule");
                var type = $("input.type").attr("name");
                /* stop form from submitting normally */
                // event.preventDefault(); 
                /* get some values from elements on the page: */
                var csv = '';

                for (var i = 0; uiEs.length > i; i++) {
                    // alert(uiEs.get(i).getAttribute("name"));
                    if (i > 0) {
                        csv += ',' + uiEs.get(i).getAttribute("name");
                    } else {
                        csv += uiEs.get(i).getAttribute("name");
                    }
                }
                //alert(reLangUrl+'/site?rank=change');
                /* Send the data using post and put the results in a div */
                $.post(reLangUrl + '/site/admin?rank=change', {
                    s: csv,
                    type: type
                },
                        function (data) {
                            //var content = $( data ).find( '#content' );
                            $("#result").html(data);
                        }
                );
            }
        });
    };

    var handleImageRotator = function () {

        $('.imageRotator').each(function () {
            var rotatorTimeout;
            var rotator = $(this);
            //configure timeout
            var controlsEnabled = rotator.data('controls');
            var timeout = rotator.data('timeout');
            var fixHeight = rotator.data('height');
            if (typeof controlsEnabled === 'undefined' || controlsEnabled !== 'false') {
                controlsEnabled = false;
            }
            if (typeof timeout === 'undefined') {
                timeout = 5000;
            }
            if (typeof fixHeight !== 'undefined') {
                rotator.css({
                    'height': fixHeight,
                    'overflow': 'hidden'
                });
            }
            rotator.removeClass('pure-g-r');
            var controlHtml = '<div class="rotateControl">';
            var containers = rotator.find('.rotateContainer');
            containers.each(function (i) {

                $(this).removeClass('pure-u-1-' + containers.size);
                controlHtml += '&nbsp;<a class="" href="#">&diams;</a>&nbsp;';
                if (i > 0) {
                    //fix height of included items for smoother transition
                    $(this).find('div,span,img,p,blockquote').each(function () {
                        var imgbreite = $(this).width();
                        var imghoehe = $(this).height();
                        $(this).css({
                            'height': imghoehe,
                            'width': imgbreite
                        });

                    });
                    $(this).addClass('hiddenRotateContainer');
                    $(this).css({
                        'width': '0px'
                    });
                }

                $(this).css({
                    'float': 'right'
                });
            });
            controlHtml += '</div>';

            rotator.css({
                'display': 'block'
            });
            var rotHeight = rotator.height();
            var rotWidth = rotator.width();

            if (controlsEnabled) {
                rotator.append(controlHtml);

                var control = rotator.find('.rotateControl');
                control.css({
                    'top': rotHeight - 30,
                    'left': rotWidth / 2 - control.width() / 2
                });
                control.fadeIn();
            }

            rotatorTimeout = setTimeout(function () {
                rotateImage(containers, timeout, control);
            }, timeout);
            var rotateImage = function (images, timeout, control) {
                var size = images.size;
                console.log("images size: ", size);

                console.log("START rotate image ");

                images.each(function (i) {

                    if (!$(this).hasClass('hiddenRotateContainer')) {
                        //fix height of included items
                        $(this).find('div,span,img,p,blockquote').each(function () {
                            var imgbreite = $(this).width();
                            var imghoehe = $(this).height();
                            $(this).css({
                                'height': imghoehe,
                                'width': imgbreite
                            });

                        });
                        console.log("rotate to image: " + (i + 1));
                        $(this).css({
                            'float': 'left'
                        });

                        $(this).animate({
                            width: 0
                        });

                        $(this).addClass('hiddenRotateContainer');

                        var rotateIMG = function (element) {
                            $(element).removeClass('hiddenRotateContainer');
                            $(element).css({
                                'float': 'right'
                            });
                            $(element).animate({
                                width: '100%'
                            });
                        };

                        if (typeof images.get(i + 1) !== 'undefined') {
                            rotateIMG(images.get(i + 1));
                            return false;
                        }
                        if (typeof images.get(i + 1) === 'undefined') {
                            rotateIMG(images.get(0));
                            console.log("rotate to beginning: ");
                            return false;
                        }
                    }

                });
                rotatorTimeout = setTimeout(function () {
                    rotateImage(images, timeout, control);
                }, timeout);
                //console.log("END rotate image ");
            };
            console.log("containers size: ", containers);
        });
    };



//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


};