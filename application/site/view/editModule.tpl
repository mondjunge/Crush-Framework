<h1><?php echo $this->ts('Edit') . " " . $sites[0]['unique_name'] ?></h1>
<a class="pure-button" href="<?php echo $this->action("site/admin"); ?>" ><?php echo $this->ts("back"); ?></a>
<br/>
<?php foreach ($enabledLanguages as $lang): ?>
    <?php if ($lang == $editLang): ?>
        <span class="selected_lang"><?php echo $lang ?> version</span>
    <?php else: ?>
        <a href="<?php echo $this->action('site/admin?edit=' . $sites[0]['id'] . '&editLang=' . $lang) ?>" ><?php echo $lang ?> version</a>
    <?php endif; ?>

<?php endforeach; ?>
        
<?php echo $moduleConfiguration; $n=0; ?>
        
<?php foreach ($sites as $site): if($site['type'] != 's'): $n++;?>
    <form class="pure-form pure-form-stacked" action="<?php echo $this->action('site/admin') ?>" method="post">
        <input id="id_<?php echo $n;?>" name="id" type="hidden" value="<?php echo $site['id'] ?>"/>
        <input id="editLang_<?php echo $n;?>" name="editLang" type="hidden" value="<?php echo $editLang ?>"/>
        <fieldset>
            <legend><?php echo $site['module'] ?></legend>
            <div class="pure-g pure-g-r">
                <div class="pure-u-1 help">
                    <?php echo $site['description']; ?>
                </div>
                <div class="pure-u-1-3">
                    <div class="pure-control-group">
                        <label for="title_<?php echo $n;?>" ><?php echo $this->ts("Title") ?></label>
                        <input id="title_<?php echo $n;?>" name="title" type="text" value="<?php echo $site['title'] ?>"/>
                    </div>
                    <div class="pure-control-group">
                        <label for="name_<?php echo $n;?>" ><?php echo $this->ts("Name") ?></label>
                        <input id="name_<?php echo $n;?>" name="name" type="text" value="<?php echo $site['name'] ?>"/>
                    </div>
                    <div class="pure-control-group">
                        <label for="module_<?php echo $n;?>" ><?php echo $this->ts("Module") ?></label>
                        <input id="module_<?php echo $n;?>" name="module"  type="text" value="<?php echo $site['module'] ?>"/>
                    </div>
                                <div class="pure-control-group">
                                    <label for="rank_<?php echo $n;?>" ><?php echo $this->ts("Rank") ?></label>
                                    <input id="rank_<?php echo $n;?>" name="rank" type="text" size="6" value="<?php echo $site['rank'] ?>"/>
                                </div>
                    <!-- -->
                    <!--            <div class="pure-control-group">
                                    <label for="lang" ><?php echo $this->ts("Language") ?></label>
                                    <input id="lang" name="lang" type="text" size="6" maxlength="2"/>
                                </div>-->


                </div>
                <div class="pure-u-1-3" style="margin-top: 1.2em ;margin-bottom: 1.2em;">
                    <div class="pure-control-group">
                        <label for="active_<?php echo $n;?>" class="pure-checkbox">
                            <div class="checker">
                                <input id="active_<?php echo $n;?>" class="checkbox" name="active" <?php echo $site['active'] ?> type="checkbox">
                            <div class="check-bg"></div>
                                <div class="checkmark">
                                    <svg viewBox="0 0 100 100">
                                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                </div>
                            </div>
                            <span class="label"><?php echo $this->ts("Active") ?></span>
                        </label>
                    </div>
                    <br/>
                    <div class="pure-control-group">
                        <label for="hide_<?php echo $n;?>" class="pure-checkbox">
                            <div class="checker">
                                <input id="hide_<?php echo $n;?>" class="checkbox" name="hide" <?php echo $site['hide'] ?> type="checkbox">
                            <div class="check-bg"></div>
                                <div class="checkmark">
                                    <svg viewBox="0 0 100 100">
                                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                </div>
                            </div>
                            <span class="label"><?php echo $this->ts("Hidden") ?></span>
                        </label>
                    </div>
                    <br/>
                    <div class="pure-control-group">
                        <label for="admin_<?php echo $n;?>" class="pure-checkbox">
                            <div class="checker">
                                <input id="admin_<?php echo $n;?>" class="checkbox" name="admin" <?php echo $site['admin'] ?> type="checkbox">
                            <div class="check-bg"></div>
                                <div class="checkmark">
                                    <svg viewBox="0 0 100 100">
                                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                    </svg>
                                </div>
                            </div>
                            <span class="label"> <?php echo $this->ts("Is admin module") ?></span>
                        </label>
                    </div>



                </div>
                <div class="pure-u-1-3">
                    <div class="pure-control-group">
                        <label for="role_need_<?php echo $n;?>" ><?php echo $this->ts("Role need") ?></label>
                        <select id="role_need_<?php echo $n;?>" name='role_need[]' multiple='multiple' size='5' style='width:150px'>
                            <?php
                            $check = '';
                            if (in_array('0', $site['rnArray'])) {
                                $check = "selected='selected'";
                            }
                            ?>
                            <option value="0" <?php echo $check ?> ><?php echo $this->ts("Everybody") ?></option>
                            <?php
                            foreach ($roles as $role) : $check = '';
                                if (in_array($role['id'], $site['rnArray'])) {
                                    $check = "selected='selected'";
                                }
                                ?>

                                <option value="<?php echo $role['id'] ?>" <?php echo $check ?>><?php echo $role['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="pure-u-1">
                    <div class="pure-controls">
                        <button type="submit" class="pure-button pure-button-primary" name="updateModule" ><?php echo $this->ts("Save Changes") ?></button> 
                        <?php if(substr($site['module'], 0, 4) !== 'site' && substr($site['module'], 0, 4) !== 'user') :?>
                        <button type="submit" onclick="return utils.check.deleteCheck('<?php echo $this->ts('Do you really want to delete this module forever?') ?>');" class="pure-button pure-button-error" name="deleteModuleFromDb" ><?php echo $this->ts("Delete module from DB") ?></button> 
                        <?php endif;?>
                    </div>
                </div>

            </div>
        </fieldset>
    </form>
<?php endif; endforeach; ?>