<h1><?php echo $this->ts('Add a static site') ?></h1>
<form class="pure-form pure-form-stacked" action="<?php echo $this->action('site/admin') ?>" method="post">
    <fieldset>
        <div class="pure-g pure-g-r" >
            <div class="pure-u-1-2" >
                <div class="pure-control-group">
                    <label for="title"><?php echo $this->ts("Title") ?></label>
                    <input id="title" name="title" type="text" value="" size="30" />
                    <span class="help"><?php echo $this->ts("The title is shown in the web-browsers titlebar."); ?></span>
                </div>
                <div class="pure-control-group">
                    <label for="name"><?php echo $this->ts("Name") ?></label>
                    <input id="name" name="name" type="text" value="" size="30"/>
                    <span class="help"><?php echo $this->ts("The name defines the label for this specific page in the navigation."); ?></span>
                </div>
                <div class="pure-control-group">
                    <label class="pure-checker" tabindex="0" title="<?php echo $this->ts("Active") ?>">
                        <div class="checker">
                            <input class="checkbox" name="active" type="checkbox" />
                            <div class="check-bg"></div>
                            <div class="checkmark">
                                <svg viewBox="0 0 100 100">
                                <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <span class="label"><?php echo $this->ts("Active") ?></span>
                    </label>
                    <span class="help"><?php echo $this->ts("Active Pages can be accessed by defined roles."); ?></span>
                </div>
<!--                <div class="pure-control-group">
                    <label for="active" >
                        <input id="active" name="active" type="checkbox" />
                        <?php echo $this->ts("Active") ?>
                    </label>
                    <span class="help"><?php echo $this->ts("Active Pages can be accessed by defined roles."); ?></span>
                </div>-->
                <div class="pure-control-group">
                    <label class="pure-checker" tabindex="0" title="<?php echo $this->ts("Hidden") ?>">
                        <div class="checker">
                            <input class="checkbox" name="hide" type="checkbox" />
                            <div class="check-bg"></div>
                            <div class="checkmark">
                                <svg viewBox="0 0 100 100">
                                <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <span class="label"><?php echo $this->ts("Hidden") ?></span>
                    </label>
                    <span class="help"><?php echo $this->ts("Setting the page to hidden, hides the page from the navigation, but is still accessible."); ?></span>
                </div>
<!--                <div class="pure-control-group">
                    <label for="hide" >
                        <input id="hide" name="hide" type="checkbox"  />
                        <?php echo $this->ts("Hidden") ?></label>
                    <span class="help"><?php echo $this->ts("Setting the page to hidden, hides the page from the navigation, but is still accessible."); ?></span>
                </div>-->
            </div>
            <div class="pure-u-1-2" >
                <div class="pure-control-group">
                    <label for="role_need"><?php echo $this->ts("Role need") ?></label>
                    <select id="role_need" name='role_need[]' multiple='multiple' size='5' style="max-width:90%;">
                        <option value="0" selected="selected" ><?php echo $this->ts("No specific role required") ?></option>
                        <?php foreach ($roles as $role) : ?>
                            <option value="<?php echo $role['id'] ?>"><?php echo $role['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="help"><?php echo $this->ts("Specify roles that can access this page."); ?></span>
                </div>
            </div>
            <div class="pure-u-1" >
                <div class="pure-control-group">
                    <label for="text" ><?php echo $this->ts("Content") ?></label>
                    <textarea id="ckeditor" name="text" class="text" rows="10" cols="30"></textarea>
                </div>
            </div>
            <div class="pure-u-1" >
                <div class="pure-controls">
                    <button type="submit" class="pure-button pure-button-primary" name="createStatic" ><?php echo $this->ts("Add site") ?></button> 
                    <a class="pure-button" href="<?php echo$this->action('site/admin'); ?>"><?php echo $this->ts('Cancel'); ?></a>
                </div>
            </div>
        </div>
    </fieldset>
</form>