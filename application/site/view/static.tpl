<?php if ($editLink): ?>
    <div><a href="<?php echo $this->action('site/admin?editStatic=') . $site['id'] ?>" ><?php echo $this->ts('edit') ?></a></div>
<?php endif; ?>
<div>
    <?php echo $site['content'] ?>
</div>