<br/><br/>
<form class="pure-form pure-form-aligned" action="<?php //$this->action('site/admin') ?>" method="POST">
    <fieldset>
        <legend><?php echo $configLegend ?></legend>
        <input type="hidden" name="module" value="<?php echo $module ?>" />
        <?php foreach ($config as $key => $value) :
            $type = gettype($value);
            ?>
            <?php if ($type == 'boolean'): ?>
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <input class="pure-input-1-2" id="<?php echo $key ?>" name="<?php echo $key ?>" <?php echo ($value ? 'checked="checked"' : ''); ?> type="checkbox" />
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php elseif ($key == 'enabledLanguages'): ?>    
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <select name="enabledLanguages[]" multiple='multiple' size='2'>
                        <option label="deutsch" value="de" <?php echo (in_array('de', $value) ? 'selected="selected"' : ''); ?> >deutsch</option>
                        <option label="english" value="gb" <?php echo (in_array('gb', $value) ? 'selected="selected"' : ''); ?>>english</option>
                    </select>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php elseif ($type == 'array'): ?>   
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <select name="enabledLanguages[]" multiple='multiple' size='2'>
                <?php foreach ($value as $val) : ?>

                                    <option label="deutsch" value="<?php echo $val ?>"  selected="selected" ><?php echo $val ?></option>

                <?php endforeach; ?>
                    </select>
                    <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php else: ?>
                <div class="pure-control-group">
                    <label style="width: 13em;" for="<?php echo $key ?>" ><?php echo $key ?></label>
                    <input class="pure-input-1-2" id="<?php echo $key ?>" name="<?php echo $key ?>" value="<?php echo $value ?>" type="text" />
                   <div class="help help-margin"><?php echo $this->ts($key . '_helpText') ?></div>
                </div>
            <?php endif; ?>
<?php endforeach; ?>
        <div class="pure-controls">
            <button class="pure-button pure-button-primary" type="submit" name="saveConfiguration" ><?php echo $saveButtonValue ?></button>
        </div>
    </fieldset>
</form>
