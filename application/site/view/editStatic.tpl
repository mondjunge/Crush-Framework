<h1><?php echo $this->ts('Edit') . " " . $site['unique_name'] ?></h1>
<a class="pure-button" href="<?php echo $this->action("site/admin"); ?>" ><?php echo $this->ts("back"); ?></a>
<br/>
<?php foreach ($enabledLanguages as $lang): ?>
    <?php if ($lang == $editLang): ?>
        <span class="pure-button pure-button-active"><?php echo $lang ?> version</span>
    <?php else: ?>
        <a class="pure-button" href="<?php echo $this->action('site/admin?editStatic=' . $site['id'] . '&editLang=' . $lang) ?>" ><?php echo $lang ?> version</a>
    <?php endif; ?>

<?php endforeach; ?>
<form class="pure-form pure-form-stacked" action="<?php echo $this->action('site/admin') ?>" method="post">
    <input type="hidden" name="id" value="<?php echo $site['id'] ?>" />
    <input id="editLang" name="editLang" type="hidden" value="<?php echo $editLang; ?>"/>
    <fieldset>
        <div class="pure-g pure-g-r">
            <div class="pure-u-1-2" >
                <div class="pure-control-group">
                    <label for="title" ><?php echo $this->ts("Title") ?></label>
                    <input id="title" name="title" type="text" value="<?php echo $site['title'] ?>" size="30"/>
                    <span class="help"><?php echo $this->ts("The title is shown in the web-browsers titlebar."); ?></span>
                </div>
                <div class="pure-control-group">
                    <label for="name" ><?php echo $this->ts("Name") ?></label>
                    <input id="name" name="name" type="text" value="<?php echo $site['name'] ?>" size="30"/>
                    <span class="help"><?php echo $this->ts("The name defines the label for this specific page in the navigation."); ?></span>
                </div>

            </div>
            <div class="pure-u-1-2" >
                <div class="pure-control-group">
                    <label for="unique_name" ><?php echo $this->ts("Unique ID") ?></label>
                    <input id="unique_name" name="unique_name" type="text" value="<?php echo $site['unique_name'] ?>" size="30" />
                    <span class="help"><?php echo $this->ts("Unique ID for this page. Must conform with the part of link after 'site/'"); ?></span>
                </div>
                <div class="pure-control-group">
                    <label for="module" ><?php echo $this->ts("Link") ?></label>
                    <input id="module" name="module" type="text" value="<?php echo $site['module'] ?>" size="30" disabled=""/>
                    <span class="help"><?php echo $this->ts("Relative URL for this page without localization. Must conform with Unique ID.'"); ?></span>
                </div>


            </div>
            <div class="pure-u-1-2" >

                <div class="pure-control-group">
                    <label class="pure-checker" tabindex="0" title="<?php echo $this->ts("Active") ?>">
                        <div class="checker">
                            <input class="checkbox" name="active" type="checkbox" <?php echo $site['active'] ?> />
                            <div class="check-bg"></div>
                            <div class="checkmark">
                                <svg viewBox="0 0 100 100">
                                <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <span class="label"><?php echo $this->ts("Active") ?></span>
                    </label>
                    <span class="help"><?php echo $this->ts("Active Pages can be accessed by defined roles."); ?></span>
                </div>
                <div class="pure-control-group">
                    <label class="pure-checker" tabindex="0" title="<?php echo $this->ts("Hidden") ?>">
                        <div class="checker">
                            <input class="checkbox" name="hide" type="checkbox" <?php echo $site['hide'] ?> />
                            <div class="check-bg"></div>
                            <div class="checkmark">
                                <svg viewBox="0 0 100 100">
                                <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </div>
                        </div>
                        <span class="label"><?php echo $this->ts("Hidden") ?></span>
                    </label>
                    <span class="help"><?php echo $this->ts("Setting the page to hidden, hides the page from the navigation, but is still accessible."); ?></span>
                </div>
                <div class="pure-control-group">
                    <label for="rank" ><?php echo $this->ts("Rank") ?></label>
                    <input id="rank" name="rank" type="text" value="<?php echo $site['rank'] ?>" />
                    <span class="help"><?php echo $this->ts("Rank specifies the position in the navigation menu."); ?></span>
                </div>

            </div>
            <div class="pure-u-1-2" >
                <div class="pure-control-group">
                    <label for="role_need" ><?php echo $this->ts("Role need") ?></label>
                    <select id="role_need" name='role_need[]' multiple='multiple' size='5' style='width:150px'>
                        <?php
                        $check = '';
                        if (in_array('0', $rnArray)) {
                            $check = "selected='selected'";
                        }
                        ?>
                        <option value="0" <?php echo $check ?> ><?php echo $this->ts("Everybody") ?></option>
                        <?php
                        foreach ($roles as $role) : $check = '';
                            if (in_array($role['id'], $rnArray)) {
                                $check = "selected='selected'";
                            }
                            ?>

                            <option value="<?php echo $role['id'] ?>" <?php echo $check ?>><?php echo $role['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="help"><?php echo $this->ts("Specify roles that can access this page."); ?></span>
                </div>
            </div>
            <div class="pure-u-1">
                <div class="pure-controls">
                    <button type="submit" class="pure-button pure-button-primary" name="updateStatic" ><?php echo $this->ts("Save Changes") ?></button> 
                </div>
            </div>
            <div class="pure-u-1">
                <div class="pure-control-group">
                    <label for="text" ><?php echo $this->ts("Content") ?></label>
                    <textarea id="text" name="text" class="text" rows="10" cols="30"><?php echo $site['content'] ?></textarea>
                </div>
            </div>
            <div class="pure-u-1">
                <div class="pure-controls">
                    <button type="submit" class="pure-button pure-button-primary" name="updateStatic" ><?php echo $this->ts("Save Changes") ?></button> 
                    <button type="submit" onclick="return utils.check.deleteCheck('<?php echo $this->ts('Do you really want to delete this site forever?') ?>');" class="pure-button pure-button-error" name="deleteSiteFromDb" ><?php echo $this->ts("Delete site") ?></button> 
                </div>
            </div>
        </div>
    </fieldset>
</form>