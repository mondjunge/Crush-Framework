<h1><?php echo $this->ts('Manage sites') ?></h1>


<form class="pure-form pure-form-aligned" action="<?php echo $this->action('site/admin') ?>" method="post">
    <a class="pure-button pure-button-primary" href="<?php echo $this->action('site/admin?addStatic'); ?>"><?php echo $this->ts("Add static site") ?></a>
    <br/><br/>
    <?php if ($updatesAvailable) : ?>
        <a class="pure-button pure-button-primary " href="<?php echo $this->action('site/admin?manageUpdates'); ?>"><?php echo "** ".$this->ts("Updates")." **" ?></a>
    <?php else: ?>
        <a id="availableUpdates" class="pure-button" href="<?php echo $this->action('site/admin?manageUpdates'); ?>"><?php echo $this->ts("Updates") ?></a>
    <?php endif; ?>
    <a class="pure-button" href="<?php echo $this->action('site/admin?manageDownloads'); ?>"><?php echo $this->ts("Downloads") ?></a>
</form>


<div id="result"></div>
<div id="sortable">
    <?php foreach ($sites as $site) : ?>

        <?php
        $siteClass = 'siteActive';
        if ($site['active'] == 0) {
            $siteClass = "siteInactive";
        }
        ?>
        <div name="<?php echo $site['module']; ?>" class="siteModule pure-table <?php echo $siteClass; ?>">
            <?php if ($type == 'admin'): ?>
                <form class="pure-form pure-form-aligned" action="<?php echo $this->action('site/admin?manageAdmin'); ?>" method="post">
                <?php else: ?>
                    <form class="pure-form pure-form-aligned" action="<?php echo $this->action('site/admin'); ?>" method="post">
                    <?php endif; ?>
                    <?php if ($site['id'] === '' || $site['active'] == 0): ?>
                        <?php echo $site['name']; ?>
                    <?php else : ?>
                        <?php if ($site['type'] == 's') : ?>
                            <a class="moduleLink" href="<?php echo $this->action('site/admin?editStatic=') . $site['id'] ?>" ><?php echo $site['name']; ?></a>
                            <span class="moduleName"><?php echo "(" . $site['module'] . ")"; ?></span>
                        <?php else : ?>
                            <a class="moduleLink" href="<?php echo $this->action('site/admin?edit=') . $site['id'] ?>" ><?php echo $site['name']; ?></a>
                            <span class="moduleName"><?php echo "(" . $site['module'] . ")"; ?></span>
                        <?php endif; ?>
                        <input type="hidden" name="id" value="<?php echo $site['id']; ?>" />
                    <?php endif; ?>

                    <div class="floatRight">
                        <label for="role" ><?php //echo $this->ts('Role needed').":";  ?></label>
                        <?php //echo $site['role_need'] ?>
                        <?php if (strpos($site['role_need'], '0') !== FALSE) : //everyone ?>
                            <?php //echo $this->image("profil.png", 'user_image_mini', "24px", "24px", "everyone"); ?>
                            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#eye', 'svgIcon svgIcon-dark profilpageIcon rightEveryoneIcon'); ?>
                        <?php elseif ($site['role_need'] === '1') : //admin ?>
                            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#lock-locked', 'svgIcon svgIcon-dark profilpageIcon rightSuperadminIcon'); ?>
                        <?php elseif ($site['role_need'] === ''): // nothing defined ?>
                            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#minus', 'svgIcon svgIcon-dark profilpageIcon rightNothingIcon'); ?>
                        <?php else: // some user ?>
                            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#people', 'svgIcon svgIcon-dark profilpageIcon rightUserIcon'); ?>
                        <?php endif; ?>
                    </div>

                    <input type="hidden" name="module" value="<?php echo $site['module']; ?>" />
                    <div class="clearBoth"></div>
                    <span class="version">
                        <?php if ($site['type'] !== 's') : echo $this->ts('version'); ?>: <?php echo $site['version'];
                        else : echo "&nbsp;";
                        endif; ?>
                    </span>
                    <div class="clearBoth"></div>
                    <span class="description">
                        <?php
                        if (isset($site['description'])): echo $site['description'];
                        endif;
                        ?>
                    </span>



                    <div class="floatRight">

                        

                            <?php if ($site['type'] == 's') : ?>
                                <a class="pure-button pure-button-site pure-button-site-edit " href="<?php echo $this->action('site/admin?editStatic=') . $site['id'] ?>" ><?php echo $this->ts('Edit page'); ?></a>
                            <?php else : ?>
                                <?php if ($site['active'] == 1): ?>
                                    <a class="pure-button pure-button-site pure-button-site-edit " href="<?php echo $this->action('site/admin?edit=') . $site['id'] ?>" ><?php echo $this->ts('Edit Module'); ?></a>
                                <?php endif; ?>
                            <?php endif; ?>
                                
                        <?php if ($site['active'] == 0): ?>
                                    <button class="pure-button pure-button-primary pure-button-site pure-button-site-activate " type="submit" name="activate" ><?php echo $this->ts('activate'); ?></button>
                        <?php else: ?>
                                    <button class="pure-button pure-button-error pure-button-site pure-button-site-deactivate " type="submit" name="deactivate"><?php echo $this->ts('deactivate'); ?></button>
                        <?php endif; ?>
                            
                    </div>
                    <div class="clearBoth"></div>
                </form>
        </div>

    <?php endforeach; ?>
</div>
<div class="legend" >
    <h3><?php echo $this->ts('Legend'); ?></h3>
<?php echo $this->icon('open-iconic/sprite/sprite.min.svg#lock-locked', 'svgIcon svgIcon-dark profilpageIcon rightSuperadminIcon'); ?> &#61; <?php echo $this->ts('Only Superadmins can see and use this.'); ?>
    <br/><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#people', 'svgIcon svgIcon-dark profilpageIcon rightUserIcon'); ?> &#61; <?php echo $this->ts('Users might see and use this.'); ?>
    <br/><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#eye', 'svgIcon svgIcon-dark profilpageIcon rightEveryoneIcon'); ?> &#61; <?php echo $this->ts('Everyone can see and use this.'); ?>
    <br/><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#minus', 'svgIcon svgIcon-dark profilpageIcon rightNothingIcon'); ?> &#61; <?php echo $this->ts('No rights set at all. Be careful.'); ?>

</div>