<form class="pure-form pure-form-aligned" action="<?php echo $this->action('site/admin') ?>" method="post">
    <a class="pure-button" href="<?php echo $this->action('site/admin'); ?>"><?php echo $this->ts("back") ?></a>
</form>
<br/>
<b>System Version:</b> <?php echo $coreVersion; ?>
<br/>
<?php if ($coreUpdateAvailable): ?>
    <?php if (!empty($sites)) : ?>
        <?php echo $this->ts("Before updating the core, you should install all available updates for modules."); ?> 
    <?php endif;?>
<form class="pure-form pure-form-aligned" action="<?php echo $this->action('site/admin') ?>" method="post">
    <input type="hidden" name="core"/>
    <button class="pure-button pure-button-site pure-button-site-activate" type="submit" name="update" ><?php echo $this->ts('update core to version $1',array(1=>$remoteCoreVersion)); ?></button>
</form>
<?php endif;?>
<br/>
<?php if (empty($sites)) : ?>
<?php echo $this->ts("No updates available at the moment."); ?>
<?php else:?>
<h2><?php echo $this->ts("Available Updates"); ?></h2>
<?php endif;?>
<?php foreach ($sites as $site) : ?>

        <?php
        $siteClass = 'siteActive';
        if ($site['active'] == 0) {
            $siteClass = "siteInactive";
        }
        ?>
        <div name="<?php echo $site['module']; ?>" class="siteModule pure-table <?php echo $siteClass; ?>">
                    <form class="pure-form pure-form-aligned" action="<?php echo $this->action('site/admin'); ?>" method="post">
                    <a class="moduleLink" href="<?php echo $this->action('site/admin?edit=') . $site['id'] ?>" ><?php echo $site['name']; ?></a>
                    <span class="moduleName"><?php echo "(" . $site['module'] . ")"; ?></span>
                    <input type="hidden" name="id" value="<?php echo $site['id']; ?>" />
                    <input type="hidden" name="module" value="<?php echo $site['module']; ?>" />

                    <div class="floatRight">
                        <label for="role" ><?php //echo $this->ts('Role needed').":"; ?></label>
                        <?php //echo $site['role_need'] ?>
                        <?php if (strpos($site['role_need'], '0') !== FALSE) : //everyone ?>
                            <?php //echo $this->image("profil.png", 'user_image_mini', "24px", "24px", "everyone"); ?>
                            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#eye', 'svgIcon svgIcon-dark profilpageIcon rightEveryoneIcon'); ?>
                        <?php elseif ($site['role_need'] === '1') : //admin ?>
                            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#lock-locked', 'svgIcon svgIcon-dark profilpageIcon rightSuperadminIcon'); ?>
                        <?php elseif($site['role_need'] === ''): // nothing defined ?>
                            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#minus', 'svgIcon svgIcon-dark profilpageIcon rightNothingIcon'); ?>
                        <?php else: // some user ?>
                            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#people', 'svgIcon svgIcon-dark profilpageIcon rightUserIcon'); ?>
                        <?php endif; ?>
                    </div>

                    
                    <div class="clearBoth"></div>
                    <span class="version">
                        <?php if($site['type']!=='s') : echo $this->ts('version'); ?>: <?php echo $site['version']." => ". $site['update_version']; else : echo "&nbsp;"; endif; ?>
                    </span>
                    <div class="clearBoth"></div>
                    <span class="description">
                        <?php if (isset($site['description'])): echo $site['description'];
                        endif;
                        ?>
                    </span>



                    <div class="floatRight">
                        
                        <button class="pure-button pure-button-site pure-button-site-activate floatRight" type="submit" name="update" ><?php echo $this->ts('update'); ?></button>
                        
                    </div>
                    <div class="clearBoth"></div>
                </form>
        </div>

<?php endforeach; ?>