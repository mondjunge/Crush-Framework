<h1><?php echo $this->ts('Add a module') ?></h1>
<form action="<?php echo $this->action('site/admin') ?>" method="post">
    <div class="form">
        <div class="site">
            <p>
                <label for="title" class="divlabel"><?php echo $this->ts("Title") ?></label>
                <input id="title" name="title" type="text" size="26"/>
            </p>
            <p>
                <label for="module" class="divlabel"><?php echo $this->ts("Module") ?></label>
                <input id="module" name="module" type="text" size="26" />
            </p>
            <p>
                <label for="rank" class="divlabel"><?php echo $this->ts("Rank") ?></label>
                <input id="rank" name="rank" type="text" size="6" />
            </p>
<!--            <p>
                <label for="lang" class="divlabel"><?php echo $this->ts("Language") ?></label>
                <input id="lang" name="lang" type="text" size="6" maxlength="2"/>
            </p>-->


        </div>
        <div class="site_security">
            <p>
                <label for="role_need" class="divlabel"><?php echo $this->ts("Role need") ?></label>
                <input id="role_need" name="role_need" type="text" size="26"/>
            </p>
            <p>
                <label for="active" class="divlabel"><?php echo $this->ts("Active") ?></label>
                <input id="active" name="active" type="checkbox" size="26"/>
            </p>


        </div>
        <div style="clear:both"></div>
        <div class="site">
            <p>
                <button type="submit" name="createModule" class="pure-button pure-button-primary" ><?php echo $this->ts("Add module") ?></button> 
            </p>
        </div>
        <div style="clear:both"></div>
    </div>
</form>