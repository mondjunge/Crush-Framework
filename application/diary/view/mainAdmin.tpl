<h1><?php echo $this->ts('Blog Administration') ?></h1>

<!--<a href="<?php echo $this->action('blog/admin?new') ?>"><?php echo $this->ts('Create a new article') ?></a>-->
<form action="<?php echo $this->action('blog/admin') ?>" method="get">
    <div class="form">
        <button  type="submit" name="new" ><?php echo $this->ts('Create a new article') ?></button>
    </div>
</form>


<div class="blog">

    <div class="page">
        
        <a href="<?php echo $this->action('blog/admin?page=') . ($page + 1) ?>" ><?php echo $this->ts('Older Posts'); ?></a>
        
        <?php if ($page != 0): ?>
            <a href="<?php echo $this->action('blog/admin?page=') . ($page - 1) ?>" ><?php echo $this->ts('Newer Posts'); ?></a>
        <?php endif; ?>
        
    </div>
    
    <?php foreach ($articles as $article) : ?>
        <div class="blog_article">
            <h2><?php echo $article['title']; ?></h2>
            <div><a href="<?php echo $this->action('blog/admin?edit=') . $article['id'] ?>" ><?php echo $this->ts('edit') ?></a></div>
            <div class="blog_text">
                <?php echo $article['text'] ?>
            </div>
            <div class="blog_tags">
                Tags: <?php echo $article['tags']; ?>
            </div>
            <a href="<?php echo $this->action('blog?title=') . urlencode($article['title']) ?>">
                <?php echo $this->ts('read more...'); ?></a>
        </div>
    <?php endforeach; ?>

    <div class="page">
        
        <a href="<?php echo $this->action('blog/admin?page=') . ($page + 1) ?>" ><?php echo $this->ts('Older Posts'); ?></a>
        
        <?php if ($page != 0): ?>
            <a href="<?php echo $this->action('blog/admin?page=') . ($page - 1) ?>" ><?php echo $this->ts('Newer Posts'); ?></a>
        <?php endif; ?>
        
    </div>

</div>