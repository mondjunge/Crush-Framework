<h1><?php echo $this->ts('Create a new article') ?></h1>
<form class="pure-form pure-form-stacked" action="<?php echo $this->action('diary/admin') ?>" method="post" enctype="multipart/form-data">
    <fieldset>

        <div class="pure-g pure-g-r">

            <div class="pure-u-1-2">
                <div class="pure-control-group">
                    <label for="title" ><?php echo $this->ts('Title') ?></label>
                    <input id="title" name="title" type="text" value="<?php echo $titledate; ?>"/>
                    <span class="help"><?php echo $this->ts('Title for the article'); ?></span>
                </div>

                

            </div>
            <div class="pure-u-1-2">
                <div class="pure-control-group">
                    <label for="tags" ><?php echo $this->ts('Tags') ?></label>
                    <input id="tags" name="tags" type="text" value=""/>
                    <span id="tagsEmoticons" class="pure-button">😊</span>
                </div>
                <p>
                <div class="tags">
                    <span ><?php echo $this->ts("Tags in use") ?></span>
                    <?php foreach ($tags as $tag) : ?>
                        <a class="pure-button pure-button-xsmall" href="#" onclick="addTag('<?php echo $tag ?>');return false;"><?php echo $tag ?></a>
                    <?php endforeach; ?>
                </div>
                </p>
                <p>
    <!--                <label for="language" ><?php echo $this->ts('Language') ?></label>-->
                    <input id="language" name="language" type="hidden" size="3" value="de"/>
                </p>
                
            </div>
        <!-- <div class="pure-control-group">
                        
                             <label for="active" ><?php echo $this->ts('Active') ?></label>
                             <input id="active" name="active" type="hidden" value="0" />
             
            
         </div>-->
        <div class="pure-u-1-1">
            <textarea id="ckeditor" class="text" name="text" cols="50" rows="10" style="width:100%;"></textarea>
        </div>
        <div class="pure-u-1-2">
                <label for="author" ><?php echo $this->ts('Author') ?></label>
                <input id="author" name="author" type="text" value="<?php echo $defaultAuthor ?>"/>

                <label for="date" ><?php echo $this->ts('Date') ?></label>
                <input id="date" class="date" name="date" type="date" size="10" value="<?php echo $date ?>" readonly="readonly"/>
        </div>
        <div class="pure-u-1-1">
            <button name="insert" class="pure-button pure-button-primary" type="submit" ><?php echo $this->ts('Create article'); ?></button>
        </div>
    </fieldset>
</form>
<div style="clear:both"></div>
