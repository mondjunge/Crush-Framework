<div class="tags">
    <a class="pure-button pure-button-primary pure-button-small button-blog" href="<?php echo $this->action('diary'); ?>" ><?php echo $this->ts('back'); ?></a>
</div>
<h1 id="blogHeader"><?php echo $article['title']; ?></h1>
<div class="tags">
    <?php echo $this->ts('Tags') ?>:
    <?php foreach ($article['tags'] as $tag) : ?>
        <a class="blog_tags" href="<?php echo $this->action('diary' . $adminContext . '?tag=' . urlencode($tag)) ?>" ><?php echo $tag ?></a>
    <?php endforeach; ?>                
</div> 
<?php if ($editLink): ?>
    <div><a href="<?php echo $this->action('diary/admin?edit=') . $article['id'] ?>" ><?php echo $this->ts('edit') ?></a></div>
<?php endif; ?>

<div class="blog">
    <div id="blogArticle">
        <?php echo $article['text']; ?>
    </div>
    <div id="authorAndDate">
        <div class="author"><?php echo $article['author']; ?></div> 
        <div class="date"><?php echo $article['date']; ?>
        <?php if ($article['date'] != $article['editDate']): ?>
            <div class="editDateMainList" > <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#pencil', 'svgIcon dateIcon', $this->ts('time edited') ); ?> <?php echo $article['editDate']; ?></div>
        <?php endif; ?>
        </div>
    </div>
</div>
<div style="clear:both"><br/></div>
<div class="tags">
    <?php if (isset($lastNext['0'])) : ?>
        <a class="pure-button pure-button-primary pure-button-small button-blog" href="<?php echo $this->action('diary/'). $lastNext['0']['dateurl'] .'/' . $lastNext['0']['linkTitle']; ?>">
            <?php echo "&lt;&nbsp;" . $lastNext['0']['title']; ?>
        </a>
        &nbsp;
    <?php endif; ?>
    <?php if (isset($lastNext['1'])) : ?>
        <a class="pure-button pure-button-primary pure-button-small button-blog" href="<?php echo $this->action('diary/'). $lastNext['1']['dateurl'] .'/' . $lastNext['1']['linkTitle']; ?>">
            <?php echo $lastNext['1']['title'] . "&nbsp;&gt;"; ?>
        </a>
    <?php endif; ?>
</div>  

<div style="clear:both"></div>
