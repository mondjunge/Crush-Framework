<h1><?php echo $this->ts('Edit Article') ?></h1>


<form class="pure-form pure-form-stacked" action="<?php echo $this->action('diary/admin') ?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <input type="hidden" name="id" value="<?php echo $article['id'] ?>" />
        <div class="pure-g pure-g-r">
            <div class="pure-u-1-2">
                <div class="pure-control-group">
                    <label for="title" ><?php echo $this->ts('Title') ?></label>
                    <input id="title" name="title" type="text" value="<?php echo $article['title'] ?>"/>
                    <span class="help"><?php echo $this->ts('Title for the article'); ?></span>
                </div>
            </div>    
            <div class="pure-u-1-2">
                <div class="pure-control-group">
                    <label for="tags" ><?php echo $this->ts('Tags') ?></label>
                    <input id="tags" name="tags" type="text" value="<?php echo $article['tags'] ?>"/>
                    <span class="help"><?php echo $this->ts('Seperate tags with comma.'); ?></span>
                </div>
                <p>
                <div class="tags">
                    <span ><?php echo $this->ts("Tags in use") ?></span>
                    <?php foreach ($tags as $tag) : ?>
                        <a class="pure-button pure-button-xsmall" href="#" onclick="addTag('<?php echo $tag ?>');return false;"><?php echo $tag ?></a>
                    <?php endforeach; ?>
                </div>
                </p>
                <p>
<!--                    <label for="language" ><?php echo $this->ts('Language') ?></label>-->
                    <input id="language" name="language" type="hidden" size="3" value="<?php echo $article['language'] ?>"/>
                </p>

            </div>
                <!--                    <div class="pure-control-group">
                                            <label for="active" ><?php echo $this->ts('Active') ?></label>
                                            <input id="active" name="active" type="checkbox"  />
                                    </div>-->
                <input id="active" name="active" type="hidden" value="<?php echo $article['active'] ?>" />
        </div>
        <div style="clear:both"></div>
        <button name="update" type="submit" class="pure-button pure-button-primary" ><?php echo $this->ts('Update article') ?></button>
        <div style="clear:both"></div>

        <div>
            <textarea id="blog_article_content" class="text" name="text" cols="50" rows="10" ><?php echo $article['text'] ?></textarea>
        </div>

        <div>
            <p class="float">
                <label for="author"><?php echo $this->ts('Author') ?></label>
                <input id="author" name="author" type="text" value="<?php echo $article['author'] ?>"/>

                <label for="date"><?php echo $this->ts('Date') ?></label>
                <input id="date" class="date" name="date" type="date" size="10" readonly="readonly" value="<?php echo $article['date'] ?>" />
            </p>
        </div>
        <div style="clear:both"></div>
        <button name="update" type="submit" class="pure-button pure-button-primary" ><?php echo $this->ts('Update article') ?></button>
        <button name="delete" type="submit" class="pure-button pure-button-error" onclick="return deleteCheck();" ><?php echo $this->ts('Delete article') ?></button>
    </fieldset>
</form>
<div style="clear:both"></div>
