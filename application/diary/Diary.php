<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Diary extends AppController {

    private $blogModel;

    /** Comment Notices are send to this e-mail adress. 
     * Comments from this email don't need notifications. * */
    private $commentNoticeEmail = '';
    private $defaultAuthor = '';

    public function __construct() {
        include_once 'model/DiaryModel.php';
        $this->blogModel = new DiaryModel();

        if (Session::isValid('uroles')) {
            $roles = explode(',', Session::get('uroles'));
            $this->blogModel->setUserRoles($roles);
        } else {
            $this->blogModel->setUserRoles(array('0'));
        }

        if ($this->defaultAuthor == '') {
            $this->defaultAuthor = Session::get('unick');
        }

        if ($this->commentNoticeEmail == '') {
            $this->commentNoticeEmail = Config::get('noticeEmail');
        }
    }

    public function index() {

        $this->autoCheckRights();

       if (filter_has_var(INPUT_GET, 'title')) {
            // show article
            //$this->includeJs('javascript/diary/deleteCheck.js');
            $this->viewDetail();
        } elseif (filter_has_var(INPUT_GET, 'list')) {
            $this->viewList();
        } elseif (filter_has_var(INPUT_GET, 'group')) {
            //$this->includeJs('javascript/diary/mainGroup.js');
            $this->viewGroup();
        } else {
            $this->viewList();
        }
        // $this->main();
    }

    public function admin() {

        $this->autoCheckRights();
        //if(filter_has_var(INPUT_POST,''))

        if (filter_has_var(INPUT_GET, 'comDel')) {
            $this->deleteComment();
        } elseif (filter_has_var(INPUT_POST, 'delete')) {
            $this->deleteArticle();
        } elseif (filter_has_var(INPUT_POST, 'update')) {
            $this->updateArticle();
        } elseif (filter_has_var(INPUT_POST, 'updateandpublish')) {
            $this->updateArticle('1', true);
        } elseif (filter_has_var(INPUT_POST, 'unpublish')) {
            $this->updateArticle('0');
        } elseif (filter_has_var(INPUT_POST, 'insert')) {
            $this->insertArticle();
        } elseif (filter_has_var(INPUT_POST, 'insertandpublish')) {
            $this->insertArticle('1');
        } elseif (filter_has_var(INPUT_GET, 'new')) {
            $this->newArticle();
        } elseif (filter_has_var(INPUT_GET, 'edit')) {
            $this->editArticle();
        } else {
            //this->adminMain();
            $this->viewList();
        }
    }


    private function editArticle() {

        $id = Input::sanitize(filter_input(INPUT_GET, 'edit'), 'int');

        $this->includeJs('extras/vanilla-javascript-emoji-picker-2.0.1/vanillaEmojiPicker.js');
        $this->includeCkeditor('javascript/ck_config/ck_default.js');

        $data['article'] = $this->blogModel->getArticleById($id);
        $data['tags'] = $this->blogModel->getTags(true);

        $date = new DateTime($data['article']['date']);
        $data['article']['date'] = $date->format('Y-m-d');

        if ($data['article']['allow_comments'] == '1') {
            $data['article']['allow_comments'] = "checked='checked'";
        } else {
            $data['article']['allow_comments'] = "";
        }
        $data['article']['title'] = $data['article']['title'];

        $data['roles'] = $this->getRoles('50');
        $data['rnArray'] = explode(',', $data['article']['role_need']);


        $this->view('edit', $data);
    }

    private function deleteArticle() {

        $id = Input::sanitize(filter_input(INPUT_POST, 'id'), 'int');

        if ($this->delete('diary', "id=$id")) {
            $this->setSystemMessage($this->ts('Article deleted.'));
        } else {
            $this->setErrorMessage($this->ts('Article could not be deleted!'));
        }

        Link::jumpTo('diary');
    }

    private function updateArticle($active = '1', $notify = false) {

        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

        $title = strip_tags(filter_input(INPUT_POST, 'title', FILTER_SANITIZE_ADD_SLASHES));

        //$returnTitle = stripslashes($title);
        //$title = htmlspecialchars($title);

        $text = filter_input(INPUT_POST, 'text');
        $author = filter_input(INPUT_POST, 'author');
        $date = filter_input(INPUT_POST, 'date');
        //$roleNeed = join(',', $_POST['role_need']);
        $language = filter_input(INPUT_POST, 'language');
        $tags = filter_input(INPUT_POST, 'tags');

//        if (filter_has_var(INPUT_POST,'active'))
//            $active = '1';
//        else
//            $active = '0';

        $fieldsValueArray = array(
            'title' => "$title",
            'text' => "$text",
            'author' => "$author",
            'language' => "$language",
            'tags' => "$tags"
        );

        if ($this->updatePrepare('diary', $fieldsValueArray, "id=:id", array('id' => $id))) {
            $this->setSystemMessage($this->ts('Updated article successfully.'));

        } else {
            $this->setErrorMessage($this->ts('Article could not be updated!'));
        }

        if ($active === '1') {

            Link::jumpTo('diary/0/' . $this->encodeTitleForModRewrite($title));
        } else {
            Link::jumpTo('diary/admin?edit=' . $id);
        }
    }


    private function insertArticle($active = '0') {
        $title = filter_input(INPUT_POST, 'title');
        $title = strip_tags($title);

        //$returnTitle = stripslashes($title);
        //$title = htmlspecialchars($title);

        $text = filter_input(INPUT_POST, 'text');
        $author = filter_input(INPUT_POST, 'author');
       // $date = filter_input(INPUT_POST, 'date');
        //$roleNeed = join(',', $_POST['role_need']);
        $language = filter_input(INPUT_POST, 'language');
        $tags = filter_input(INPUT_POST, 'tags');

       // $dateTime = new DateTime($date);
//        if ($dateTime->getTimestamp() > time()) {
//            $articleInFuture = true;
//        } else {
//            $articleInFuture = false;
//        }
//
//        if (filter_has_var(INPUT_POST,'active'))
//            $active = '1';
//        else
//            $active = '0';

        $fieldsValueArray = array(
            'title' => $title,
            'text' => $text,
            'author' => $author,
            //'role_need' => $roleNeed,
            //'date' => $date,
            'language' => $language,
            'tags' => $tags,
            'active' => 1,
            'creator_user_id' => Session::get('uid'),
        );
        $newId = $this->insert('diary', $fieldsValueArray);

        if ($newId === false) {
            $this->setErrorMessage($this->ts('Could not insert article. Sry, this is an never expected problem. Check your database.'));
            Link::jumpTo('diary/admin?new');
        } else {

                $this->setSystemMessage($this->ts('Article created successfully.'));
                Link::jumpTo('diary/0/' . $this->encodeTitleForModRewrite($title));
        }
    }

    private function getNewArticleNotificationMessage() {

        $m = "$1 hat einen neuen Artikel veröffentlicht.";
//        $m .= "<br/>";
//        $m .= "<br/>" . Session::get('unick') . " hat einen neuen Artikel auf " . Config::get('rootUrl') . Config::get('relativeUrl') . " veröffentlicht.";
//        $m .= "<br/>";
//        $m .= "<br/>Hier kommst Du direkt zum neuen Artikel: ";
//        $m .= "<br/><a href='{link}' >{link}</a>";

        return $m;
    }

    private function getNewCommentNotificationMessage() {

        $m = "Hallo {user},";
        $m .= "<br/>";
        $m .= "<br/>" . Session::get('unick') . " hat einen Kommentar auf " . Config::get('rootUrl') . Config::get('relativeUrl') . " abgegeben.";
        $m .= "<br/>";
        $m .= "<br/>Hier kommst Du zum kommentierten Artikel: ";
        $m .= "<br/><a href='{link}' >{link}</a>";

        return $m;
    }

    /**
     * 
     * @param type $link
     * @param type $rawMessage
     */
    private function sendNotificationToUsers($link, $subject, $rawMessage, $usersRsToNotify) {

        $linkedMessage = str_replace('{link}', $link, $rawMessage);
        foreach ($usersRsToNotify as $u) {
            $finalMessage = str_replace('{user}', $u['nick'], $linkedMessage);
            $this->mail_utf8($u['email'], $subject, $finalMessage);
        }
    }

    private function newArticle() {
        $this->includeJs('extras/vanilla-javascript-emoji-picker-2.0.1/vanillaEmojiPicker.js');
        $this->includeCkeditor('javascript/ck_config/ck_default.js');

        $data['tags'] = $this->blogModel->getTags(true);

        $data['date'] = date('Y-m-d');
        $date = new DateTime(date('Y-m-d H:i:s'));
        $data['titledate'] = $this->ts($date->format("l")).", ".$date->format($this->ts('d|m|Y H:i'))." ". $this->ts('h');
        $data['roles'] = $this->getRoles('50');
        $data['defaultAuthor'] = $this->defaultAuthor;
        $this->view('insert', $data);
    }

    private function getArticlesForWidgetAsHtml() {
        $this->standalone = true;

        $articles = $this->blogModel->getAllArticles('0,5');
        $html = '';
        foreach ($articles as $art) {
            $text = $art['text'];
            // newline whitespaces entfernen
//             $text = str_replace("\r", "",$art['text']);
//             $text = str_replace("\n", "",$text);
            // Absatzenden mit newline ersetzen, tags strippen
            $text = str_replace("<\p>", "<\p>&nbnr;", $text);
            $text = str_replace("<br/>", "<br/>&nbnr;", $text);
            $text = str_replace("<br />", "<br />&nbnr;", $text);

            $text = substr(strip_tags($text), 0, 150);

            $art['text'] = str_replace("&nbnr;", "<br/>", $text) . " [...]";

            $html .= "<div><p><a class='feed_title' href='" . Link::action('diary/0/') . str_replace('%2F', '/', urlencode($art['title'])) . "' target='_blank' >{$art['title']}</a></p>";

            $date = new DateTime($art['date']);
            $art['date'] = $date->format($this->ts('d|m|Y'));
            $html .= "<p class='date'>" . $art['date'] . "</p>";
            $html .= "<span class='feed_content'><a href='" . Link::action('diary/0/') . str_replace('%2F', '/', urlencode($art['title'])) . "' target='_blank' >{$art['text']}</a></span></div><br/>";
        }
        echo $html;
    }

    private function viewDetail() {

        //edit link
        if ($this->checkSiteRights('diary/admin')) {
            $data['editLink'] = true;
        } else {
            $data['editLink'] = false;
        }
        
        //$this->includeCkeditor('javascript/ck_config/ck_basic.js');

        $title = urldecode(filter_input(INPUT_GET, 'title'));
        //echo $title;

        if (preg_match("/^([0-9])+$/", $title)) {
            $data['article'] = $this->blogModel->getArticleById($title);
        } else {
            $data['article'] = $this->blogModel->getArticleByTitle($title);
        }
        if ($data['article']['active'] == 0 && $this->checkSiteRights('diary/admin') !== true) {
            $data['article'] = false;
            $this->setErrorMessage($this->ts("Something went wrong while getting the article. Sorry."));
            $this->view('detail', $data);
            return;
        }

        if ($data['article'] == false) {
            $this->setErrorMessage($this->ts("Something went wrong while getting the article. Sorry."));
            $this->view('detail', $data);
            return;
        }
        $this->setTitle(stripslashes($data['article']['title']));
        //$data['lastNext'] = array();
        $data['lastNext'] = $this->blogModel->getLastNext($data['article']['id']);
        if (isset($data['lastNext']['0'])) {
            $data['lastNext']['0']['linkTitle'] = $this->encodeTitleForModRewrite($data['lastNext'][0]['title']);
            $date = new DateTime($data['lastNext']['0']['date']);
            $data['lastNext']['0']['dateurl'] = $date->format($this->ts('Y-m-d'));
        }
        if (isset($data['lastNext']['1'])) {
            $data['lastNext']['1']['linkTitle'] = $this->encodeTitleForModRewrite($data['lastNext'][1]['title']);
            $date = new DateTime($data['lastNext']['1']['date']);
            $data['lastNext']['1']['dateurl'] = $date->format($this->ts('Y-m-d'));
        }

        $date = new DateTime($data['article']['date']);
        $data['article']['date'] = $date->format($this->ts('d|m|Y H:i'))." ".$this->ts('h');
        $editDate = new DateTime($data['article']['edit_date']);
        $data['article']['editDate'] = $editDate->format($this->ts('d|m|Y H:i'))." ".$this->ts('h');
        
        $articleTags = explode(",", $data['article']['tags']);
        $data['article']['tags'] = $articleTags;

       // $data['likeUrl'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
       // $data['likeDesc'] = substr(strip_tags($data['article']['text']), 0, 255) . " [...]";


        $this->view('detail', $data);
    }

    private function viewList() {
        $data['adminContext'] = "";
        if (filter_has_var(INPUT_GET, 'page') && is_numeric(filter_input(INPUT_GET, 'page'))) {

            $page = Input::sanitize(filter_input(INPUT_GET, 'page'), 'int');
        } else {
            $page = 0;
        }

        if ($this->checkSiteRights('diary/admin')) {
            $data['editLink'] = true;
        } else {
            $data['editLink'] = false;
        }
        $start = 5 * $page;
        $end = 5;
        $data['page'] = $page;

        $tag = "Alle";
        if (filter_has_var(INPUT_GET, 'tag') && filter_input(INPUT_GET, 'tag') != $tag) {
            $tag = filter_input(INPUT_GET, 'tag');
            $articles = $this->blogModel->getArticlesByTag($tag, "$start,$end", $data['editLink']);
        } else {
            $articles = $this->blogModel->getAllArticles("$start,$end", $data['editLink']);
        }
        $data['selected_tag'] = $tag;
        if (count($articles) == 0 && filter_has_var(INPUT_GET, 'page')) {

            $this->setErrorMessage($this->ts('No older articles in stock, sorry!'));
            Link::jumpTo('diary?page=' . ($page - 1) . '&tag=' . $tag);
        }
        if (count($articles) == 0 && !filter_has_var(INPUT_GET, 'page')) {
            $data['articles'] = [];
            $this->setErrorMessage($this->ts('No articles in stock, sorry!'));
        }

        if (extension_loaded('tidy')) {
            $tidy = new tidy();
        } else {
            $tidy = false;
        }
        if (is_array($articles)) {
            foreach ($articles as $article) {
                $date = new DateTime($article['date']);
                $editDate = new DateTime($article['edit_date']);
//                if($date->getTimestamp() > time()){
//                    continue;
//                }
                // if there is tidy extension installed, use it to shorten large articles in overview.
                if ($tidy != false && strlen($article['text']) > 2500) {
                    $article['text'] = $tidy->repairString(substr($article['text'], 0, 2500), array("doctype" => "omit", "char-encoding" => "utf8"), 'utf8') . " [...]";
                }
                $article['date'] = $date->format($this->ts('d|m|Y H:i'))." ".$this->ts('h');
                $article['editDate'] = $editDate->format($this->ts('d|m|Y H:i'))." ".$this->ts('h');
                $article['dateurl'] = $date->format($this->ts('Y-m-d'));

                $article['title'] = htmlspecialchars_decode($article['title']);
                $article['linkTitle'] = $this->encodeTitleForModRewrite($article['title']);
                $articleTags = explode(",", $article['tags']);
                $article['tags'] = $articleTags;
                $data['articles'][] = $article;
            }
        }

//        if (Config::get('AJAXsupport')) {
//            $data['ajax'] = "onclick=\"getPage(this.href); return false;\" ";
//        } else
//            $data['ajax'] = "";
        $data['isRssActive'] = $this->isModuleActive('rss');
        $data['tags'][] = "Alle";
        $data['tags'] = array_merge($data['tags'], $this->blogModel->getTags($data['editLink']));
        $this->view('mainList', $data);
    }

    private function viewGroup() {

        $tags = $this->blogModel->getTags();
        $data['tags'] = $tags;
        $aTag = array();
        foreach ($tags as $tag) {
            $aTag["$tag"] = $this->blogModel->getArticlesByTag($tag);
        }

        $data['articles'] = $aTag;
        $this->view('mainGroup', $data);
    }


    public function getNotificationCategories() {
        return array('newarticle');
    }

    public function getNotificationSettings($data) {
        return $this->fetch('notifications_settings', $data);
    }

    /*     * *
     * OMG, mess ahead, duplicated code from Profilpage...
     * 
     * TODO: make a helper or something to replace strings in text
     * - replace twitter with embedded code
     */

    private function replaceUrls($string) {

        /*         * * make sure there is an https:// on all URLs ** */
        $string = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-\+%]+)/i", "$1https://$2", $string);


        $pattern = '/([\w]+:\/\/[\w\-?!&;:#,%~=\.\/\@\+]+[\w\/])/i';
        $string = preg_replace_callback($pattern, array($this, 'replaceSwitch'), $string);

        return $string;
    }

    private function replaceSwitch($matches) {
        if (strpos($matches[1], 'youtube.') !== false) {
            return $this->replaceYoutube($matches[1]);
        } else if (strpos($matches[1], 'youtu.') !== false) {
            return $this->replaceYoutu($matches[1]);
        } else if (strpos($matches[1], 'vimeo.') !== false) {
            return $this->replaceVimeo($matches[1]);
        } else if (strpos($matches[1], 'soundcloud.') !== false) {
            return $this->replaceSoundcloud($matches[1]);
        } else if (strpos($matches[1], 'dailymotion.') !== false) {
            return $this->replaceDailyMotion($matches[1]);
        } else if (strpos($matches[1], 'facebook.') !== false) {
            return $this->replaceFacebookVideo($matches[1]);
        } else if (strtolower(substr($matches[0], -4, 4)) == '.jpg' || strtolower(substr($matches[0], -4, 4)) == '.jpeg' || strtolower(substr($matches[0], -4, 4)) == '.png' || strtolower(substr($matches[0], -4, 4)) == '.gif') {
            return $this->replaceImage($matches[0]);
        } else {
            $title = $this->getTitleCurl($matches[1]);
            $favicon = $this->getFavicon($matches[1]);
            return "<br/>" . $favicon . "&nbsp;<b>" . $title . "</b><br/>" . $this->replaceUrl($matches[1]);
        }
    }

    private function replaceImage($string) {
        $data['path'] = $string;
        $data['width'] = '';
        $return = $this->fetch('imageHtml', $data);

        return $return;
    }

    private function replaceFacebookVideo($string) {
// example
// https://www.facebook.com/MrCianTwomey/videos/771163216349283/
        $pattern = '/https?:\/\/w?w?w?\.?facebook\.com\/(.*?)\/videos\/([a-zA-Z0-9_\-]+)(\S*)/i';
        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($string);
        $replaceNew = "<div id='fb-root'>&nbsp;</div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your embedded video player code -->
  <div class='fb-video' data-href='https://www.facebook.com/$1/videos/$2/' data-width='500' data-show-text='false'>
    <div class='fb-xfbml-parse-ignore'>
      <blockquote cite='https://www.facebook.com/$1/videos/$2/'>
        Posted by <a href='https://www.facebook.com/$1/'>$1</a>
      </blockquote>
    </div>
  </div>";
        return "" . $title . "<br/>" . preg_replace($pattern, $replaceNew, $string) . $link;
    }

    private function replaceVimeo($string) {
        $pattern = '/https?:\/\/vimeo\.com\/m?\/?([a-zA-Z0-9_\-]+)(\S*)/i';
//$replace = '<object width="100%" ><param name="movie" value="https://www.youtube.com/v/$2&amp;hl=en_US&amp;fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/$2&amp;hl=en_US&amp;fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="100%" ></embed></object>';
//$replaceNew = '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/$2" frameborder="0" allowfullscreen></iframe>';
//<iframe src="//player.vimeo.com/video/93365760?byline=0&amp;color=9ebf33" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        $replaceNew = '<iframe class="vimeo" src="//player.vimeo.com/video/$1" allowfullscreen></iframe>';

        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($string);
        return "" . $title . "<br/>" . preg_replace($pattern, $replaceNew, $string) . $link . "";
    }

    private function replaceYoutube($string) {
        $pattern = '/https?:\/\/w?w?w?\.?youtube\.com\/watch(.*?)v?=?([a-zA-Z0-9_\-]+)(\S*)/i';
        return $this->makeYoutubeIframe($string, $pattern); //"" . $title . "<br/>" . $replaceNew . '<iframe class="privacyIframe privacyIframeHidden" src="" allowfullscreen="" frameborder="0" border="0" cellspacing="0"></iframe>' .$link;
    }

    private function replaceYoutu($string) {
        $pattern = '/https?:\/\/youtu\.(be)\/([a-zA-Z0-9_\-]+)(\S*)/i';
        return $this->makeYoutubeIframe($string, $pattern);
    }

    private function makeYoutubeIframe($string, $pattern) {

        $replaceNewUrl = 'https://www.youtube-nocookie.com/watch/$2';
        $newUrl = preg_replace($pattern, $replaceNewUrl, $string);


        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($newUrl);

        $dataurl = preg_replace($pattern, "https://www.youtube-nocookie.com/embed/$2", $string);
        $replaceNew = "<a class='privacyImageLink' href='$dataurl' data-class='youTube' data-url='$dataurl' onclick='return false;'>";
        $replaceNew .= $this->imageHtml('youtube.png', 'privacyImage'); //'<iframe class="youTube" src="https://www.youtube.com/embed/$2" allowfullscreen></iframe>';
        $replaceNew .= "</a>";

        return "" . $title . "<br/>" . $replaceNew . '<iframe class="privacyIframe privacyIframeHidden" src="" allowfullscreen="" frameborder="0" border="0" cellspacing="0"></iframe>' . $link;
    }

    private function replaceSoundcloud($string) {
//$pattern = '/https?:\/\/w?\.?soundcloud\.com\/(.*?)\/([a-zA-Z0-9_\-]+)(\S*)/i';
#https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/138362966&color=ff5500&auto_play=false&hide_related=false&show_artwork=true
        $newUrl = 'https://w.soundcloud.com/player/?url=' . urlencode($string) . "&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=true";

        $link = "<br/>" . $this->replaceUrl($string);
        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";

        $dataurl = $newUrl;
        $replaceNew = "<a class='privacyImageLink' href='$dataurl' data-class='soundcloud' data-url='$dataurl' onclick='return false;'>";
        $replaceNew .= $this->imageHtml('soundcloud.jpg', 'privacyImage'); //'<iframe class="youTube" src="https://www.youtube.com/embed/$2" allowfullscreen></iframe>';
        $replaceNew .= "</a>";

        $iFrame = '<iframe class="privacyIframe privacyIframeHidden" src="" allowfullscreen="" frameborder="0" border="0" cellspacing="0"></iframe>';

        return "" . $title . "<br/>" . $replaceNew . $iFrame . $link;
    }

    private function replaceDailyMotion($string) {
#<iframe frameborder="0" width="480" height="270" src="http://www.dailymotion.com/embed/video/x1eg6fc" allowfullscreen></iframe>
        $pattern = '/https?:\/\/www\.dailymotion.com\/video\/([a-zA-Z0-9_\-]+)(\S*)/i';

        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($string);

        $dataurl = preg_replace($pattern, "https://www.dailymotion.com/embed/video/$1", $string);
        $replaceNew = "<a class='privacyImageLink' href='$dataurl' data-class='dailyMotion' data-url='$dataurl' onclick='return false;'>";
        $replaceNew .= $this->imageHtml('youtube.png', 'privacyImage'); //'<iframe class="youTube" src="https://www.youtube.com/embed/$2" allowfullscreen></iframe>';
        $replaceNew .= "</a>";

        return "" . $title . "<br/>" . $replaceNew . '<iframe class="privacyIframe privacyIframeHidden" src="" allowfullscreen="" frameborder="0" border="0" cellspacing="0"></iframe>' . $link;
    }

    private function replaceUrl($string) {
        /*         * * make all URLs links ** */
        $string = preg_replace('/([\w]+:\/\/[\w\-?!&:;#,%~=\.\/\@\+]+[\w\/])/i', "<a target=\"_blank\" href=\"$1\">$1</a>", $string);
        /*         * * make all emails hot links ** */
        $string = preg_replace("/([\w\-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i", "<A HREF=\"mailto:$1\">$1</A>", $string);

        return $string;
    }

    /**
     * 
     * @param string $url
     * @return string
     */
    private function getUrlContent($url) {
// get html via url
        $savePath = dirname(__FILE__) . "/../../cache/html/" . md5($url) . ".html";
        if (!is_dir(dirname(__FILE__) . "/../../cache/html/")) {
            mkdir(dirname(__FILE__) . "/../../cache/html/", 0777);
        }
        if (is_file($savePath)) {
//echo "ist schon da!";
            return file_get_contents($savePath);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
//        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 0);
        $html = utf8_decode(curl_exec($ch));
        curl_close($ch);
//        fclose($fp);
//        chmod($savePath, 0644);
        file_put_contents($savePath, $html);

        return $html;
    }

    public function getTitleCurl($url) {
        $this->checkSiteRights('diary/admin');
//return $url;

        $html = $this->getUrlContent($url);

// get title
        $dom = new DomDocument();
        libxml_use_internal_errors(true);
        if ($html != "") {
            $dom->loadHTML($html);
            if ($dom->documentElement != null) {
                $titleTagElement = $dom->documentElement->getElementsByTagName('title');
                $titleRaw = $titleTagElement->item('0')->textContent;
                $title = trim(empty($titleRaw) ? 'Untitled' : $titleRaw);
            }
        } else {
            $title = $url;
        }



// convert title to utf-8 character encoding
        if ($title != 'Untitled') {
            $match = array();
            preg_match('/<charset\=(.+)\"/i', $html, $match);
            if (!empty($match[0])) {
                $charset = str_replace('"', '', $match[0]);
                $charset = str_replace("'", '', $charset);
                $charset = strtolower(trim($charset));
                if ($charset != 'utf-8') {
                    $title = iconv($charset, 'utf-8', $title);
                }
            }
        }

        return $title;
    }

    private function isUrl($string) {
        $pattern = '/([\w]+:\/\/[\w-?!&;#,%~=\.\/\@\+]+[\w\/])/i';

        if (preg_match($pattern, $string) == 1) {
            return true;
        }
    }

    private function get_file_extension($file_name) {
        return substr(strrchr($file_name, '.'), 1);
    }

    public function getFavicon($url) {
        $this->checkSiteRights('profilpage/dash');

        $imgUrl = "cache/thumbs/src/" . md5($url); //Config::get('relativeUrl').
//echo $imgUrl;
        $imgHtml = "<img src='" . Config::get('relativeUrl') . $imgUrl . "' class='ext_fav' style='max-width:16px;'/>";
        if (is_file($imgUrl)) {
            return $imgHtml;
        }


        $savePath = dirname(__FILE__) . "/../../cache/thumbs/src/" . md5($url);
        $urlParsed = parse_url($url);

// check for standard favicon
        $userPath = $urlParsed['host'];
        $path = "http://" . $userPath . "/favicon.ico";
        $header = get_headers($path);
        if (preg_match("|200|", $header[0])) {
            $this->cacheFavicon($path, $savePath);
            return $imgHtml;
//return $this->imageHtml($path, 'ext_fav', '16px'); //$this->imageHtml();//'<img src="'..'" />';
        }


# make the URL simpler
        $elems = parse_url($url);
        $url = $elems['scheme'] . '://' . $elems['host'];

# load site
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = curl_exec($ch);
        curl_close($ch);

# look for the shortcut icon inside the loaded page
        $matches = array();
        $regex_pattern = "/rel=\"shortcut icon\" (?:href=[\'\"]([^\'\"]+)[\'\"])?/";
        preg_match_all($regex_pattern, $output, $matches);

        if (isset($matches[1][0])) {
            $favicon = $matches[1][0];

# check if absolute url or relative path
            $favicon_elems = parse_url($favicon);

# if relative
            if (!isset($favicon_elems['host']) && substr($favicon, 0, 2) != "//") {
                $favicon = $url . '/' . $favicon;
            }

            $this->cacheFavicon($favicon, $savePath);

            return $imgHtml;
//return $this->imageHtml($favicon, 'ext_fav', '16px'); //'<img src="'..'" />';
//return "<img src='".$favicon."' />";
        }

        return false;
    }

    private function cacheFavicon($url, $savePath) {
        if (is_file($savePath)) {
            return;
        } else {
            if (substr($url, 0, 2) == '//') {
                $url = "http:" . $url;
            }
            if (strpos($url, '?') !== FALSE) {
                $arr = explode('?', $url);
                $url = $arr['0'];
            }
            $imgUrl = $url;

            $ch = curl_init($imgUrl);
            $fp = fopen($savePath, 'x');

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec($ch);
            curl_close($ch);


            fclose($fp);
            return;
        }
    }

}
