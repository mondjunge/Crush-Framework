CREATE TABLE IF NOT EXISTS `{dbprefix}diary` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(191) NOT NULL,
  `text` MEDIUMTEXT NOT NULL,
  `author` varchar(191) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role_need` varchar(191) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `allow_comments` tinyint(1) NOT NULL DEFAULT '1',
  `language` char(2) NOT NULL DEFAULT 'gb',
  `creator_user_id` bigint NULL,
  `parent` bigint NULL,
  `tags` text NOT NULL ,
  PRIMARY KEY (`id`)
);