<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Insufficient rights!' =>
	'Insufficient rights!'
	,
	'send'=>
	'send'
	,
	'profile picture'=>
	'profile picture'
	,
	'Video'=>
	'Video'
	,
	'Image'=>
	'Image'
	,
	'upload image'=>
	'upload image'
	,
	'Image uploaded successful'=>
	'Image uploaded successful'
	,
	'Error uploading Image.'=>
	'Error uploading Image.'
	,
	'Preview'=>
	'Preview'
	,
	'cancel'=>
	'cancel'
	,
	'Y-m-d H:i'=>
	'Y-m-d H:i'
	,
	'newest entries'=>
	'newest entries'
	,
	'upload video'=>
	'upload video'
	,
	'video uploaded successful'=>
	'video uploaded successful'
	,
	'upload '=>
	'upload '
	,
	'File uploaded successful'=>
	'File uploaded successful'
	,
	'delete'=>
	'delete'
	,
	'Entry deleted'=>
	'Entry deleted'
	,
	'Do you really want to delete this entry? It will be gone forever (which is a long time)!'=>
	'Do you really want to delete this entry? It will be gone forever (which is a long time)!'
	,
	'Link'=>
	'Link'
	,
	'einfügen'=>
	'einfügen'
	,
	'insert link'=>
	'insert link'
	,
	'internet addresses will be made links, e-mail adresses will be hotlinked. On following adresses, content will get embeded:'=>
	'internet addresses will be made links, e-mail adresses will be hotlinked. On following adresses, content will get embeded:'
	,
	'more to come...'=>
	'more to come...'
	,
	'write a comment...'=>
	'write a comment...'
	,
	'd.m.Y H:i'=>
	'd.m.Y H:i'
	,
	'Uhr'=>
	'Uhr'
	,
	'write entries, create content for your contacts'=>
	'write entries, create content for your contacts'
	,
	'write entries, create content'=>
	'write entries, create content'
	,
	'write an entry, create content'=>
	'write an entry, create content'
	,
	'New Entry'=>
	'New Entry'
	,
	'New Comment'=>
	'New Comment'
	,
	'Hello $1,'=>
	'Hello $1,'
	,
	'there is a new entry from $2.'=>
	'there is a new entry from $2.'
	,
	'Take a look now under following link:'=>
	'Take a look now under following link:'
	,
	'there is a new comment from $2.'=>
	'there is a new comment from $2.'
	,
	'New Comment from $2'=>
	'New Comment from $2'
	,
	'New Entry from $2'=>
	'New Entry from $2'
	,
	'Invalid file format. Only ogv, mp4 and mpeg is allowed.'=>
	'Invalid file format. Only ogv, mp4 and mpeg is allowed.'
	,
	'Invalid file format. Only ogv and mp4 is allowed.'=>
	'Invalid file format. Only ogv and mp4 is allowed.'
	,
	'Invalid file format. Only jpg, png and gif is allowed.'=>
	'Invalid file format. Only jpg, png and gif is allowed.'
	,
	'Only ogv and mp4 files allowed!'=>
	'Only ogv and mp4 files allowed!'
	,
	'Only jpeg, png and gif files allowed!'=>
	'Only jpeg, png and gif files allowed!'
	,
	'No url'=>
	'No url'
	,
	'no url found.'=>
	'no url found.'
	,
	'comment'=>
	'comment'
	,
	'$1 wrote a $2.'=>
	'$1 wrote a $2.'
	,
	'entry'=>
	'entry'
	,
	'$1 created a new $2.'=>
	'$1 created a new $2.'
	,
	'this is great!'=>
	'this is great!'
	,
	'nobody cares!'=>
	'nobody cares!'
	,
	'comments'=>
	'comments'
	,
	'People who think this is great:'=>
	'People who think this is great:'
	,
	'People who think nobody cares:'=>
	'People who think nobody cares:'
	,
	'and'=>
	'and'
	,
	'privat chatten'=>
	'privat chatten'
	,
	'private chat'=>
	'private chat'
	,
	'choose a file by clicking...'=>
	'choose a file by clicking...'
	,
	'Change own data'=>
	'Change own data'
	,
	'change own data'=>
	'change own data'
	,
	'Own entries:'=>
	'Own entries:'
	,
	'Own comments:'=>
	'Own comments:'
	,
	'Upvotes recieved :'=>
	'Upvotes recieved :'
	,
	'Downvotes recieved:'=>
	'Downvotes recieved:'
	,
	'Upvotes recieved total / average:'=>
	'Upvotes recieved total / average:'
	,
	'Downvotes recieved total / average:'=>
	'Downvotes recieved total / average:'
	,
	'Upvotes total / average:'=>
	'Upvotes total / average:'
	,
	'Downvotes total / average:'=>
	'Downvotes total / average:'
	,
	'Comments recieved:'=>
	'Comments recieved:'
	,
	'Entries:'=>
	'Entries:'
	,
	'write something...'=>
	'write something...'
	,
	'load more entries...'=>
	'load more entries...'
	,
	'older entries do not exist...'=>
	'older entries do not exist...'
	,
	'unfollow user'=>
	'unfollow user'
	,
	'follow user'=>
	'follow user'
	,
	'follow'=>
	'follow'
	,
	'unfollow'=>
	'unfollow'
	,
	'Smilie'=>
	'Smilie'
	,
	'Followers:'=>
	'Followers:'
	,
	'Entries- / comments recieved:'=>
	'Entries- / comments recieved:'
	,
	'Entries / comments recieved:'=>
	'Entries / comments recieved:'
	,
	'Followers / Principals:'=>
	'Followers / Principals:'
	,
	'$1 commented your entry'=>
	'$1 commented your entry'
	,
	'$1 commented an entry you also commented'=>
	'$1 commented an entry you also commented'
	,
	'$1 created a new entry'=>
	'$1 created a new entry'
	,
	'click following link to go directly to the source of this notification:'=>
	'click following link to go directly to the source of this notification:'
	,
	'edit'=>
	'edit'
	,
	'update entry'=>
	'update entry'
	,
	'entry updated successfully'=>
	'entry updated successfully'
	,
	'Do you really want to delete this comment? It will be gone forever (which is a long time)!'=>
	'Do you really want to delete this comment? It will be gone forever (which is a long time)!'
	,
	'You only get one notification per month, unless you mark your notifications as read.'=>
	'You only get one notification per month, unless you mark your notifications as read.'
	,
	'To view all your notifications and mark them as read, go to this page:'=>
	'To view all your notifications and mark them as read, go to this page:'
	,
	'To change your notification settings, go to this page:'=>
	'To change your notification settings, go to this page:'
	,
	'Edit entry'=>
	'Edit entry'
	,
	'people to follow'=>
	'people to follow'
	,
	'year'=>
	'year'
	,
	'years'=>
	'years'
	,
	'month'=>
	'month'
	,
	'months'=>
	'months'
	,
	'day'=>
	'day'
	,
	'days'=>
	'days'
	,
	'hour'=>
	'hour'
	,
	'hours'=>
	'hours'
	,
	'minute'=>
	'minute'
	,
	'minutes'=>
	'minutes'
	,
	'second'=>
	'second'
	,
	'seconds'=>
	'seconds'
	,
	'Last time online:'=>
	'Last time online:'
	,
	' ago'=>
	' ago'
	,
	'less than an hour ago'=>
	'less than an hour ago'
	,
	'not 10 minutes ago'=>
	'not 10 minutes ago'
	,
	'less than 10 minutes ago'=>
	'less than 10 minutes ago'
	,
	'Feeds'=>
	'Feeds'
	,
	'feeds'=>
	'feeds'
	,
	'now'=>
	'now'
	,
	'not 5 minutes ago'=>
	'not 5 minutes ago'
	,
	'in the last 30 minutes'=>
	'in the last 30 minutes'
	,
	'yesterday'=>
	'yesterday'
	,
	'today'=>
	'today'
	,
	'just now'=>
	'just now'
	,
	'profilpage'=>
	'profilpage'
	,
	'contact posts a new entry.' =>
	'contact posts a new entry.'
	,
	'contact comments your entry.'=>
	'contact comments your entry.'
	,
	'contact comments an entry you commented, too.'=>
	'contact comments an entry you commented, too.'
	,
	'added successfully to sites'=>
	'added successfully to sites'
	,
	'friendsEnabled_helpText'=>
	'friendsEnabled_helpText'
	,
	'Entry successfully created!'=>
	'Entry successfully created!'
	,
	'select picture'=>
	'select picture'
	,
	'select video'=>
	'select video'
	,
	'enter link'=>
	'enter link'
);