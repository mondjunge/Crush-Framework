<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Insufficient rights!' =>
	'Ungenügende Rechte!'
	,
	'send'=>
	'absenden'
	,
	'profile picture'=>
	'Profilbild'
	,
	'Video'=>
	'Video'
	,
	'Image'=>
	'Bild'
	,
	'upload'=>
	'hochladen'
	,
	'upload image'=>
	'Bild hochladen'
	,
	'Image uploaded successful'=>
	'Bild wurde erfolgreich hochgeladen'
	,
	'Error uploading Image.'=>
	'Ein Fehler ist beim Bild-Hochladen aufgetreten. :('
	,
	'Preview'=>
	'Vorschau'
	,
	'cancel'=>
	'abbrechen'
	,
	'Y-m-d H:i'=>
	'd.m.Y H:i'
	,
	'newest entries'=>
	'aktuelle Beiträge'
	,
	'upload video'=>
	'Video hochladen'
	,
	'video uploaded successful'=>
	'Video erfolgreich hochgeladen'
	,
	'upload '=>
	'hochladen'
	,
	'File uploaded successful'=>
	'Datei wurde erfolgreich hochgeladen'
	,
	'delete'=>
	'✘'
	,
	'Entry deleted'=>
	'Eintrag wurde für immer gelöscht...'
	,
	'Do you really want to delete this entry? It will be gone forever (which is a long time)!'=>
	'Möchtest Du diesen Eintrag wirklich löschen? Er wird für immer gelöscht (was eine lange Zeit ist)!'
	,
	'Link'=>
	'Link'
	,
	'insert link'=>
	'Link einfügen'
	,
	'insert'=>
	'einfügen'
	,
	'internet addresses will be made links, e-mail adresses will be hotlinked. On following adresses, content will get embeded:'=>
	'Internet Adressen werden hier zu anklickbaren Links gemacht. Von folgenden Domains wird der Inhalt, wenn möglich, eingebettet.'
	,
	'more to come...'=>
	'weitere Domains kommen mit der Zeit dazu...'
	,
	'write a comment...'=>
	'kommentiere den Beitrag...'
	,
	'd.m.Y H:i'=>
	'd.m.Y H:i'
	,
	'Uhr'=>
	'Uhr'
	,
	'write an entry, create content'=>
	'Teile was dir gefällt oder was dich interessiert mit deinen Freunden...'
	,
	'New Entry'=>
	'Neuer Beitrag'
	,
	'New Comment'=>
	'Neuer Kommentar'
	,
	'Hello $1,'=>
	'Hallo $1,'
	,
	'there is a new entry from $2.'=>
	'$2 hat einen neuen Beitrag verfasst.'
	,
	'Take a look now under following link:'=>
	'Schau ihn Dir doch gleich an und antworte unter folgendem Link :'
	,
	'there is a new comment from $2.'=>
	'$2 hat einen neuen Kommentar abgegeben.'
	,
	'New Comment from $2'=>
	'$2 hat einen neuen Kommentar abgegeben'
	,
	'New Entry from $2'=>
	'$2 hat einen neuen Beitrag verfasst'
	,
	'Invalid file format. Only ogv and mp4 is allowed.'=>
	'Falsches Dateiformat. Nur ogv und mp4 Dateien sind erlaubt.'
	,
	'Invalid file format. Only jpg, png and gif is allowed.'=>
	'Falsches Dateiformat. Nur jpg, png und gif Dateien sind erlaubt.'
	,
	'Only ogv and mp4 files allowed!'=>
	'Nur ogv und mp4 Dateien sind erlaubt!'
	,
	'Only jpeg, png and gif files allowed!'=>
	'Nur jpeg, png and gif Dateien sind erlaubt!'
	,
	'no url found.'=>
	'Keine gültige URL gefunden.'
	,
	'comment'=>
	'Kommentar'
	,
	'entry'=>
	'Beitrag'
	,
	'this is great!'=>
	'sehr geil!'
	,
	'nobody cares!'=>
	'interessiert doch keinen!'
	,
	'comments'=>
	'Kommentare'
	,
	'People who think this is great:'=>
	'finden auch:'
	,
	'People who think nobody cares:'=>
	'finden auch:'
	,
	'and'=>
	'und'
	,
	'private chat'=>
	'privat chatten'
	,
	'choose a file by clicking...'=>
	'wähle eine Datei durch anklicken...'
	,
	'change own data'=>
	'ändere eigene Daten'
	,
	'Upvotes total / average:'=>
	'insgesamt / Durchschnitt'
	,
	'Downvotes total / average:'=>
	'insgesamt / Durchschnitt'
	,
	'Comments recieved:'=>
	'Kommentare erhalten'
	,
	'Entries:'=>
	'Beiträge'
	,
	'write something...'=>
	'Teile Links, Bilder, Videos oder schreibe was Dich beschäftigt...'
	,
	'load more entries...'=>
	'zeige mir ältere Beiträge...'
	,
	'older entries do not exist...'=>
	'es gibt keine älteren Beiträge...'
	,
	'follow'=>
	'folge'
	,
	'unfollow'=>
	'nicht mehr folgen'
	,
	'Smilie'=>
	'Smilie'
	,
	'Followers:'=>
	'Anhänger:'
	,
	'Entries / comments recieved:'=>
	'Beiträge erstellt / Kommentare erhalten'
	,
	'Followers / Principals:'=>
	'Anhänger / beobachtet selber:'
	,
	'$1 created a new entry'=>
	'$1 hat einen neuen Beitrag erstellt.'
	,
	'click following link to go directly to the source of this notification:'=>
	'Klicke folgenden Link um zur Quelle der Benachrichtigung zu gelangen:'
	,
	'$1 commented your entry'=>
	'$1 hat Deinen Beitrag kommentiert.'
	,
	'$1 commented an entry you also commented'=>
	'$1 hat einen Beitrag kommentiert, den Du auch kommentiert hast.'
	,
	'edit'=>
	'bearbeiten'
	,
	'update entry'=>
	'Beitrag aktualisieren'
	,
	'entry updated successfully'=>
	'Beitrag wurde erfolgreich aktualisiert'
	,
	'Do you really want to delete this comment? It will be gone forever (which is a long time)!'=>
	'Möchtest Du diesen Kommentar wirklich löschen? Er wird für immer weg sein (was eine lange Zeit ist)!'
	,
	'You only get one notification per month, unless you mark your notifications as read.'=>
	'Du bekommst nur eine Benachrichtigung pro Monat, solange Du alte Benachrichtigungen nicht als gelesen markierst'
	,
	'To view all your notifications and mark them as read, go to this page:'=>
	'Um alle Benachrichtigungen zu sehen und als gelesen zu markieren, klicke folgenden Link:'
	,
	'To change your notification settings, go to this page:'=>
	'Um Deine Benachrichtigungseinstellungen zu ändern gehe auf folgende Seite:'
	,
	'Edit entry'=>
	'Eintrag bearbeiten'
	,
	'people to follow'=>
	'Folge anderen Nutzern um deren Beiträge auf Deiner Profilseite zu sehen!'
	,
	'year'=>
	'Jahr'
	,
	'years'=>
	'Jahren'
	,
	'month'=>
	'Monat'
	,
	'months'=>
	'Monaten'
	,
	'day'=>
	'Tag'
	,
	'days'=>
	'Tagen'
	,
	'hour'=>
	'Stunde'
	,
	'hours'=>
	'Stunden'
	,
	'minute'=>
	'Minute'
	,
	'minutes'=>
	'Minuten'
	,
	'second'=>
	'Sekunde'
	,
	'seconds'=>
	'Sekunden'
	,
	'Last time online:'=>
	'Letztes mal online vor:'
	,
	' ago'=>
	' &nbsp;'
	,
	'less than an hour ago'=>
	'Weniger als einer Stunde'
	,
	'not 10 minutes ago'=>
	'Weniger als 10 Minuten'
	,
	'Feeds'=>
	'Feeds'
	,
	'feeds'=>
	'Feeds'
	,
	'now'=>
	'Jetzt'
	,
	'not 5 minutes ago'=>
	'Weniger als 5 Minuten'
	,
	'yesterday'=>
	'Gestern'
	,
	'today'=>
	'Heute'
	,
	'just now'=>
	'Jetzt'
	,
	'profilpage'=>
	'Profilseite'
	,
	'contact posts a new entry.' =>
	'Kontakt schreibt einen neuen Beitrag.'
	,
	'contact comments your entry.'=>
	'Kontakt kommentiert einen Deiner Beiträge.'
	,
	'contact comments an entry you commented, too.'=>
	'Kontakt kommentiert einen Beitrag, den Du auch kommentiert hast.'
	,
	'friendsEnabled_helpText'=>
	'Ist dies Option aktiviert, muss anderen Accounts explizit gefolgt werden um deren Beiträge auf der Übersichtsseite zu sehen.'
	,
	'Entry successfully created!'=>
	'Danke für Deinen Beitrag!'
	,
	'select picture'=>
	'Wähle ein Bild aus'
	,
	'select video'=>
	'Wähle ein Video aus'
	,
	'enter link'=>
	'Gib eine URL ein'
);