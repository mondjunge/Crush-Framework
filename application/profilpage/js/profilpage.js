/* global modules, base, utils, CKEDITOR, relativeUrl, reLangUrl */

/**
 * profilpage
 */

use_package('modules');
modules.profilpage = new function () {
    base.main.ModuleRegistry.registerModule(this);
    base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    var self = this;

    var previewText = "";
    var previewMediacontent = "";

    this.init = function () {
        modules.profilpage.selectHandler();
        modules.profilpage.textInputHandler();
        modules.profilpage.commentHandler();
        modules.profilpage.voteHandler();
        modules.profilpage.smilieHandler();
        modules.profilpage.loadNextPageHandler();
        modules.profilpage.handleFollow();
        modules.profilpage.editEntryHandler();

        modules.profilpage.privacyDisabler();
        modules.profilpage.handleImageOverlay();


    };

    this.ajaxRefresh = function () {
        modules.profilpage.commentHandler();
        modules.profilpage.voteHandler();
        modules.profilpage.editEntryHandler();
        modules.profilpage.privacyDisabler();
    };

    this.handleImageOverlay = function () {
        $(".mediacontent > a[target='_blank']").on('click', function () {
            var imgSrc = $(this).attr('href');
            $(this).find('img').attr('src',imgSrc);
//            console.log('clicked image'+$(this).attr('href')+" url:"+imgSrc);
//            utils.overlay.show('<div onclick="utils.overlay.hide()" style="cursor:pointer; text-align:center;" ><img src="' + imgSrc + '" style="max-width:100%; max-height: 90vh; "/></div>');
            return false;
        });
    };

    this.privacyDisabler = function () {

        $('.privacyImageLink').on("click", function () {
            var url = $(this).attr('href');
            var cssClass = $(this).data('class');
            //privacyContentHolder
            //$(this).parent('.privacyContentHolder').html('<iframe class="youTube" src="'+url+'" allowfullscreen></iframe>');
            var iframe = $(this).siblings('.privacyIframe');
            $(this).addClass("hidden");
            iframe.attr('src', url);
            iframe.addClass(cssClass);
            iframe.removeClass('privacyIframeHidden');


            return false;//privacyIframe
        });

    };

    this.handleFollow = function () {
        $('.follow').on("click", function () {
            var url = $(this).attr('href');
            var button = $(this);
            $.ajax({
                type: "GET",
                url: url + "&ajax",
                async: true,
                success: function (html) {

                    //window.location.href = window.location.href;
                    //console.log(button.attr('id'));
                    if (button.attr('id') === 'follow') {
                        //alert('follow');
                        $('#follow').hide();
                        $('#unfollow').removeClass('hidden');
                        $('#unfollow').show();

                    } else {
                        //alert('unfollow');
                        $('#unfollow').hide();
                        $('#follow').removeClass('hidden');
                        $('#follow').show();
                    }
                }
            });
//            url = window.location.href;
//            $.ajax({
//                type: "GET",
//                url: url+"&ajax",
//                async: true,
//                success: function(html){
//                    $("#content").html(html);
//                }
//            });
            return false;
        });
    };

    this.editEntryHandler = function () {
        $('a.editEntry').on("click", function () {
            var url = $(this).attr('href');
            var element = this;
            $.ajax({
                type: "GET",
                url: url + "&ajax",
                async: true,
                success: function (html) {
                    //alert(html);
                    utils.overlay.show(html, element);
                    setTimeout(function () {
                        CKEDITOR.replace('text', {
                            customConfig: relativeUrl + 'javascript/ck_config/tims_config.js',
                            uiColor: '#eeeeee',
                            toolbar: 'Default',
                            templates_files: [relativeUrl + 'javascript/ck_config/tims_templates.js']
                        });
                    }, '3000');
                }
            });
            return false;
        });
    };

    this.loadNextPageHandler = function () {
        $('.loadNextPage').on("click", function () {
            var url = $(this).attr('href');
            var buttonElement = $(this);
            //alert(url);

            $.ajax({
                type: "GET",
                url: url + "&ajax",
                async: true,
                success: function (html) {
                    //alert(html);
                    buttonElement.replaceWith(html);
                    modules.profilpage.loadNextPageHandler();
                }
            });
        });
    };

    this.smilieHandler = function () {
        $('#activateSmilies').on("click", function () {
            //$('#smilieSelect').toggle('slow');
            var url = $(this).attr('href');
            //alert(url);
            if (!$(".smilieContainer").length) {
                // load smilies if they have not been loaded before
                utils.waiting.startWait();
                $.ajax({
                    type: "GET",
                    url: url + "&ajax",
                    async: true,
                    success: function (html) {
                        $('#smilieSelect').html(html).toggle('slow');
                        modules.profilpage.setSmilieClickHandler();
                        utils.waiting.stopWait();
                    }
                });
            } else {
                $('#smilieSelect').toggle('slow');
            }

            return false;
        });

    };
    this.setSmilieClickHandler = function () {
        $(".smilieContainer").find("img").off("click");
        $(".smilieContainer").find("img").on("click", function () {
            $("#newEntry").focus();
            $("#newEntry").val($("#newEntry").val() + $(this).prop("title"));

            $("#smilieSelect").hide();
            $("#newEntry").focus();
            $("#newEntry").keyup();
        });
    };

    this.voteHandler = function () {
        $('.vote').off('click');
        $('.vote').on("click", function () {
            var url = $(this).attr('href');
            var buttonElement = $(this);
            //alert(url);

            $.ajax({
                type: "GET",
                url: url + "&ajax",
                async: true,
                success: function (string) {
                    var countArr = string.split(",");
                    //alert(string);
                    buttonElement.parent().find('.vote > .countUp').html(countArr[0]);
                    buttonElement.parent().find('.vote > .countDown').html(countArr[1]);
                }
            });
            return false;
        });
    };
    this.commentHandler = function () {
        $("textarea[name='comment']").focus(function () {
            var textarea = $(this);
            if (textarea.hasClass('autosize')) {
                //do nothing
            } else {
                textarea.autosize();
                textarea.addClass('autosize');
            }
            modules.profilpage.sendCommentClickHandler(textarea.parent().find(".insertComment"));
        });
        $("textarea[name='comment']").off('keypress');
        $("textarea[name='comment']").on('keypress', function (e) {
            if (e.keyCode === 13) {
                // 13 is enter/return key
                if (e.shiftKey) {
                    // Enter and Shift pressed... do anything here...
                    //alert(""+e.keyCode);
                    $(this).parent().find(".insertComment").trigger("click");
                    return false;
                }

            }

        });

    };
    this.sendCommentClickHandler = function (element) {

        $(element).off('click');
        $(element).on("click", function () {
            var element = this;
            var parentFormElement = $(this).parent().parent();
            var textarea = $(this).siblings("textarea[name='comment']").first();
            var entryId = parentFormElement.children('.entry_id').first().val();
            var userId = parentFormElement.children('.user_id').first().val();
            var text = textarea.val();

            console.log("entryId:" + entryId + "; userId:" + userId + "; text:" + text);
            text = text.trim();
            if (text === "") {
                return false;
            }

            $.ajax({
                type: "POST",
                url: reLangUrl + "profilpage/dash",
                async: true,
                timeout: 5000,
                data: {
                    insertComment: 'send',
                    text: text,
                    entry_id: entryId,
                    user_id: userId
                },
                success: function (html) {
                    var commentsElement = parentFormElement.parent().parent().find('.comments');
                    commentsElement.append(html);
                    commentsElement.find(":last").prev().show("slow", function () {
                    });

                    textarea.val('');
                    textarea.trigger('autosize');

                    //handleDeleteComment();
                },
                error: function () {

                }
            });
            return false;
        });
    };
    this.selectHandler = function () {
        $(".linkInsert").on("click", function () {
            console.log('click on linkInsert');
            var element = this;
            $.ajax({
                type: "GET",
                url: reLangUrl + "profilpage/dash?linkInsert&ajax",
                async: true,
                success: function (html) {
                    utils.overlay.show(html, element);
                    modules.profilpage.linkHandler();
                    //modules.profilpage.selectClickHandler();
                    //modules.profilpage.uploadClickHandler();
                }
            });
            return false;
        });

    };

    this.linkHandler = function () {
        $('#link').keyup(function () {
            modules.profilpage.linkInputAction();

        });
        $('#link').change(function () {
            modules.profilpage.linkInputAction();

        });

        $('#linkInsert').on("click", function () {
            var html = $('#linkContent').html();
            $("#mediacontent").append(html);

            $("input[name='mediacontent']").val($("input[name='mediacontent']").val() + html);

            if ($('#preview').hasClass('hidden')) {
                $('#preview').removeClass('hidden');
            }
            utils.overlay.hide();
            return false;
        });

    };

    this.linkInputAction = function () {
        var link = $('#link').val();
        console.log("Link changed! Value is: " + link);
        var linkRefined = '';
        var data = new FormData();
        data.append('link', link);
        $.ajax({
            type: "POST",
            url: reLangUrl + "profilpage/dash?getLinkHtml&ajax",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            async: true,
            success: function (html) {
                //utils.overlay.show(html,'auto');
                linkRefined = html;
                $('#linkContent').html(linkRefined);


                if ($('#linkPreview').hasClass('hidden')) {
                    $('#linkPreview').removeClass('hidden');
                }
            }
        });

        //var linkRefined = '<a href="'+link+'">'+link+'</a>';


    };

    this.textInputHandler = function () {
        var previewTimeout;
        $('#newEntry').focus(function () {
            if ($(this).hasClass('autosize')) {
                //do nothing
            } else {
                $(this).autosize();
                $(this).addClass('autosize');
            }
        });
        $('#newEntry').keyup(function (e) {
            if (e.keyCode === 9) {
                // 9 is tab key
                return;
            }

            var text = $(this).val();
            text = utils.string.escapeHtml(text);
            text = text.replace(/\n/g, "<br/>");
            
            var mediacontent = $("input[name='mediacontent']").val();

            if(text === '' && mediacontent === '' ){
                if(!$('#preview').hasClass('hidden')){
                    $('#preview').addClass('hidden');
                }
                return;
            }
            clearTimeout(previewTimeout);

            previewTimeout = setTimeout(function () {

                self.getPreview(text, mediacontent);
            }, 500);
        });
    };

    this.getPreview = function (text, mediacontent) {
        if (self.previewText === text && self.previewMediacontent === mediacontent) {
            // nothing changed, return;
            return;
        } else {
            self.previewText = text;
            self.previewMediacontent = mediacontent;
        }

        $.ajax({
            url: reLangUrl + "profilpage/dash?getPreview=true&ajax",
            data: {
                getPreview: true,
                textcontent: text,
                mediacontent: mediacontent
            },
            cache: false,
            type: 'POST',
            success: function (html) {

                if (html === '' && !$('#preview').hasClass('hidden')) {
                    $('#preview').addClass('hidden');
                    return;
                }

                if ($('#preview').hasClass('hidden')) {
                    $('#preview').removeClass('hidden');
                }
                $('#textcontent').html(html);

                $("input[name='textcontent']").val(text);
                //$("input[name='mediacontent']").val(mediacontent);

            }

        });
    };

    this.uploadClickHandler = function () {

        $("#uploadFile").on("click", function () {
            console.log("upload clicked!");

            modules.profilpage.uploadFile();
        });

    };

    this.uploadFile = function () {

        var subdir = $('#subdir').val();
        var fileType = $('#fileType').val();


        console.log("upload clicked..");
        console.log("fileType: " + fileType);
        console.log("subdir: " + subdir);

        var data = new FormData();

        jQuery.each($('#file')[0].files, function (i, file) {
            data.append('file-' + i, file);
        });
        data.append('subdir', subdir);
        data.append('fileType', fileType);
        $.ajax({
            url: reLangUrl + "profilpage/dash?uploadFile",
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (html) {
                console.log('file Upload success!');
                $('#fullOverlayContent').html(html);
                modules.profilpage.changeDirHandler();
                modules.profilpage.selectClickHandler();
                modules.profilpage.uploadClickHandler();
            }
            ,
            progress: function (evt) {
                if (evt.lengthComputable) {
                    var percent = parseInt((evt.loaded / evt.total * 100), 10) + "%";
                    $('#uploadProgress > span').html(percent);
                    $('#uploadProgress').css('width', percent);
                    //console.log("Progress: " + parseInt( (evt.loaded / evt.total * 100), 10) + "%");
                } else {
                    console.log("Progress: Length not computable.");
                }
            },
            progressUpload: function (evt) {
                if (evt.lengthComputable) {
                    if ($('#uploadProgress').hasClass('hidden')) {
                        $('#uploadProgress').removeClass('hidden');
                    }
                    var percent = parseInt((evt.loaded / evt.total * 100), 10) + "%";
                    $('#uploadProgress > span').html(percent);
                    $('#uploadProgress').css('width', percent);
                    //console.log("Upload " + parseInt( (evt.loaded / evt.total * 100), 10) + "%");
                } else {
                    console.log("Upload: Length not computable.");
                }
            }
        });
    };

    this.changeDirHandler = function () {

        $(".dir").on("click", function () {
            console.log('click on change dir');
            var dir = $(this).text();

            var type = $('#fileType').val();
            modules.profilpage.changeDir(dir, type);
            return false;
        });
    };

    this.changeDir = function (dir, type) {
        $.ajax({
            type: "GET",
            url: reLangUrl + "profilpage/dash?changeDir=" + dir + "&fileType=" + type + "&ajax",
            async: true,
            success: function (html) {

                $('#fullOverlayContent').html(html);
                modules.profilpage.changeDirHandler();
                modules.profilpage.selectClickHandler();
                modules.profilpage.uploadClickHandler();

            }

        });
    };

    this.selectClickHandler = function () {
        $('.selectableFile').on("click", function () {
            var path = $(this).data("path");
            var fileType = $("#fileType").val();
            console.log(fileType);
            $.ajax({
                type: "GET",
                url: reLangUrl + "profilpage/dash?getFileHtml=" + path + "&fileType=" + fileType + "&ajax",
                async: true,
                success: function (html) {

                    //imghtml = html;
                    $("#mediacontent").html(html);

                    $("input[name='mediacontent']").val(html);


                    if ($('#preview').hasClass('hidden')) {
                        $('#preview').removeClass('hidden');
                    }

                    utils.overlay.hide();
                    console.log(path);
                }
            });
        });
    };

    var handleFeeds = function () {

    };

    var fetchFeed = function (feedTile, id) {
        $.ajax({
            url: reLangUrl + "feeds/fetchFeed",
            type: 'POST',
            async: true,
            timeout: 20000,
            data: {
                fetchFeed: id
            },
            success: function (html) {
                feedTile.find(".feedBox").html(html);
            },
            error: function (e) {
                console.error(e);
                feedTile.find(".feedBox").html("Ouch... please <a id='reload" + id + "' class='reload' href='#'>reload</a>.").ready(function () {
                    handleReload(feedTile, id);
                });
            }

        });
    };
//    
//    this.ajaxStart = function(){
//        
//    }


};