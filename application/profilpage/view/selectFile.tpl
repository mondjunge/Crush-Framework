<div id="selectFileContent">

    <?php if ($fileType == "image"): ?>
        <div class="help">
            <?php echo $this->ts('Only jpeg, png and gif files allowed!'); ?>
        </div>
    <?php endif; ?>
    <?php if ($fileType == "video"): ?>
        <div class="help">
            <?php echo $this->ts('Only ogv and mp4 files allowed!'); ?>
        </div>
    <?php endif; ?>
    <div>

        <form action="<?php echo $this->action("profilpage/dash") ?>" method="post" enctype="multipart/form-data">

            <input id="subdir" name="subdir" type="hidden" value="<?php echo $subdir ?>" />
            <input id="fileType" name="fileType" type="hidden" value="<?php echo $fileType ?>" />
            <label for="file" ><?php echo $this->ts("upload " . $fileType) ?></label>

            <input class="pure-button" id="file" name="file" type="file"/>
            <button class="pure-button pure-button-primary" id="uploadFile" onclick="return false;" name="uploadFile" type="submit" ><?php echo $this->ts("upload") ?></button>
        </form>
        <div id="uploadProgress" class="hidden"><span class="percent">0%</span></div>
    </div>


    <div>
        <div class="dirTile">
            <a class="pure-button dir" data-file-type="<?php echo $fileType ?>">/</a>
            <?php echo $subdir ?>
            <?php foreach ($dirs as $d) : ?>
                <a class="pure-button dir" data-type="image"><?php echo $d ?></a>
            <?php endforeach; ?>
        </div>

    </div>

    <div class="">
        <div class="">
            <?php if (sizeof($files) > 0) : ?>
                <?php echo $this->ts('choose a file by clicking...'); ?>
            <?php else: ?>
                <?php echo $this->ts('upload a file...'); ?>
            <?php endif; ?>
        </div>
        <?php foreach ($files as $i) : ?>
            <div class="imageTile">
                <div class="imagePadding">
                    <div class="selectableFile" data-path="<?php echo $i['path'] ?>" data-file-type="<?php echo $fileType ?>">

                        <?php if ($fileType == "image"): ?>
                            <?php echo $this->image($i['path'], 'image', "150", ""); ?>
                        <?php endif; ?>
                        <?php if ($fileType == "video"): ?>
                            <video src="<?php echo $i['path'] ?>" width="100%" height="100%" >
                                <div> Schade – hier käme ein Video, wenn Ihr Browser HTML 5 
                                    Unterstützung für OGG hätte, wie z.B. der aktuelle Firefox</div>
                            </video>
                        <?php endif; ?>

                    </div>
                    <span class="imgdetail"><?php echo basename($i['path']); //substr(basename($i['path']), 0, 18);      ?></span>
                    <br/>
                    <span class="imgdetail"><?php echo $i['last_modified']; ?></span>
                </div>
            </div>

        <?php endforeach; ?>
    </div>
</div>