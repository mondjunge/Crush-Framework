<div>
    <form action="<?php echo $this->action("profilpage/dash") ?>" method="post" enctype="multipart/form-data">

        <input id="subdir" name="subdir" type="hidden" value="<?php echo $subdir ?>" />
        <input id="fileType" name="fileType" type="hidden" value="<?php echo $fileType ?>" />
        <label for="video" ><?php echo $this->ts("upload video") ?></label>

        <input class="pure-button" id="video" name="video" type="file"/>
        <button class="pure-button pure-button-primary" id="uploadVideo" onclick="return false;" name="uploadVideo" type="submit" ><?php echo $this->ts("upload") ?></button>
    </form>
    <div id="uploadProgress" class="hidden"><span class="percent">0%</span></div>
</div>


<div>

    <div class="dirTile">
        <a class="pure-button dir" data-type="video">/</a>
        <?php echo $subdir ?>
        <?php foreach ($dirs as $d) : ?>
            <a class="pure-button dir" data-type="video" ><?php echo $d ?></a>
        <?php endforeach; ?>
    </div>

</div>
<div class="">

    <?php foreach ($videos as $i) : ?>
        <div class="imageTile">
            <div class="imagePadding">
                <div class="selectableVideo" data-path="<?php echo $i['path'] ?>">
                    <?php echo "" //$this->image("video.png", 'video', "150", ""); ?>
                    <video src="<?php echo $i['path'] ?>" width="100%" height="100%" >

                        <div> Schade – hier käme ein Video, wenn Ihr Browser HTML 5 
                            Unterstützung für OGG hätte, wie z.B. der aktuelle Firefox</div>

                    </video>

                </div>
                <span class="imgdetail"><?php echo basename($i['path']); //substr(basename($i['path']), 0, 18);    ?></span>
                <br/>
                <span class="imgdetail"><?php echo $i['last_modified']; ?></span>
            </div>
        </div>

    <?php endforeach; ?>
</div>