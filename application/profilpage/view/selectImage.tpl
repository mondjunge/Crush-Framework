<div>
    <form action="<?php echo $this->action("profilpage/dash") ?>" method="post" enctype="multipart/form-data">

        <input id="subdir" name="subdir" type="hidden" value="<?php echo $subdir ?>" />
        <input id="fileType" name="fileType" type="hidden" value="<?php echo $fileType ?>" />
        <label for="image" ><?php echo $this->ts("upload image") ?></label>

        <input class="pure-button" id="image" name="image" type="file"/>
        <button class="pure-button pure-button-primary" id="uploadImage" onclick="return false;" name="uploadImage" type="submit" ><?php echo $this->ts("upload") ?></button>
    </form>
    <div id="uploadProgress" class="hidden"><span class="percent">0%</span></div>
</div>


<div>
    <div class="dirTile">
        <a class="pure-button dir" data-type="image">/</a>
        <?php echo $subdir ?>
        <?php foreach ($dirs as $d) : ?>
            <a class="pure-button dir" data-type="image"><?php echo $d ?></a>
        <?php endforeach; ?>
    </div>

</div>
<div class="">

    <?php foreach ($images as $i) : ?>
        <div class="imageTile">
            <div class="imagePadding">
                <div class="selectableImage" data-path="<?php echo $i['path'] ?>">
                    <?php echo $this->image($i['path'], 'image', "150", ""); ?>

                </div>
                <span class="imgdetail"><?php echo basename($i['path']); //substr(basename($i['path']), 0, 18);   ?></span>
                <br/>
                <span class="imgdetail"><?php echo $i['last_modified']; ?></span>
            </div>
        </div>

    <?php endforeach; ?>
</div>