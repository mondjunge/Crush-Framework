
<?php foreach ($entries as $e): ?>
    <div class="pure-u-1">
        <div class="dashEntry">
            <div class="floatLeft">
                <a tabindex="-1" href="#entry<?php echo $e['id']."u".$e['user_id']; ?>" id="entry<?php echo $e['id']."u".$e['user_id']; ?>"></a>
                <a href="<?php echo $this->action('profilpage/index/' . $e['user_id']) ?>">
                    <?php echo $this->image($e['image'], 'comment_img', "32px", "32px", $e['nick']); ?>
                </a>
            </div>
            <div class="floatLeft">
                <span class="nick"><a href="<?php echo $this->action('profilpage/index/' . $e['user_id']) ?>"><?php echo $e['nick'] ?></a></span> 

                <?php if ($user_id == $e['user_id'] || $adminMode == true): ?>
                    <a class="floatRight" onclick="return utils.check.deleteCheck('<?php echo $this->ts('Do you really want to delete this entry? It will be gone forever (which is a long time)!'); ?>');" href="<?php echo $this->action('profilpage/dash?deleteEntry=' . $e['id'] . '&amp;userId=' . $e['user_id']) ?>" ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-dark profilpageIcon entryDeleteIcon'); ?></a>
                    <?php if ($adminMode == true): ?>
                    <a class="editEntry floatRight" data-title="<?php echo $this->ts('Edit entry');?>" href="<?php echo $this->action('profilpage/dash?editEntry=' . $e['id'] . '&amp;userId=' . $e['user_id']) ?>" ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#pencil', 'svgIcon svgIcon-dark profilpageIcon entryEditIcon'); ?></a>
                    <?php endif; ?>
                <?php endif; ?>
                    
                <span class="time floatRight"><?php echo $e['time'] ?><?php echo $this->ts("Uhr"); ?></span>
            </div>
            <div style="clear:both"></div>
            <div class="entryText"><?php echo $e['text'] ?></div>

            <div class="voter">
                <!-- ⇑⇓ ⇧⇩⍍⍔▲▼△▽ ⤊⤋⬆⬇ -->
                <a href="<?php echo $this->action('profilpage/dash'); ?>?voteUp=<?php echo $e['id'] ?>&amp;userId=<?php echo $e['user_id']; ?>" 
                   title="<?php
            echo $this->ts('this is great!');
            if ($e['votes_up_users'] != '') {
                echo "\n\n" . $this->ts('People who think this is great:');
                echo "\n" . $e['votes_up_users'];
            }
                ?>" 
                   class="pure-button pure-button-xsmall pure-button-primary  vote">
                   <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#thumb-up', 'svgIcon svgIcon-light profilpageIcon thumbUp'); ?>&nbsp;<span class="countUp"><?php echo $e['votes_up_count']; ?></span></a>
                <a href="<?php echo $this->action('profilpage/dash'); ?>?voteDown=<?php echo $e['id']; ?>&amp;userId=<?php echo $e['user_id']; ?>" 
                   title="<?php
                echo $this->ts('nobody cares!');
                if ($e['votes_down_users'] != '') {
                    echo "\n\n" . $this->ts('People who think nobody cares:');
                    echo "\n" . $e['votes_down_users'];
                }
                    ?>" 
                   class="pure-button pure-button-xsmall pure-button-error  vote">
                    <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#thumb-down', 'svgIcon svgIcon-light profilpageIcon thumbDown'); ?>&nbsp;<span class="countDown"><?php echo $e['votes_down_count']; ?></span></a>
            </div>
            <div style="clear:both"></div>
            <?php //echo $this->ts('comments');   ?>
            <div class="comments">

                <?php foreach ($e['comments'] as $c): ?>

                    <div class="commentEntry" style="">
                        <div class="floatLeft commentImg">
                            <a href="<?php echo $this->action('profilpage/index/' . $c['user_id']) ?>">
                                <?php echo $this->image($c['image'], 'comment_img', "32px", "32px", $c['nick']); ?>
                            </a>
                        </div>

                        <div class="">
                            <span class="nick"><a href="<?php echo $this->action('profilpage/index/' . $c['user_id']) ?>"><?php echo $c['nick']; ?></a></span> 
                            <span class="time floatRight"><?php echo $c['time'] ?><?php echo $this->ts("Uhr"); ?></span>

                            <?php if ($user_id == $e['user_id'] || $user_id == $c['user_id'] || $adminMode == true): ?>
                                <br/>
                                <span class="time floatRight"><a class="deleteComment" onclick="return utils.check.deleteCheck('<?php echo $this->ts('Do you really want to delete this comment? It will be gone forever (which is a long time)!'); ?>');" href="?deleteComment=<?php echo $c['id'] ?>&amp;profileUserId=<?php echo $e['user_id'] ?>"  ><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#trash', 'svgIcon svgIcon-light profilpageIcon commentDeleteIcon'); ?><?php //echo $this->ts("delete"); ?></a></span>
                            <?php endif; ?>
                            <div class="commentText">
                                <?php echo $c['text'] ?>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                    </div>


                <?php endforeach; ?>
            </div>
            <div class="writeComment ">
    <!--                        <span class="nick"><?php echo $user_nick ?></span>-->

                <form name="com" class="pure-form" action="<?php echo $this->action("profilpage/dash") ?>" method="post" enctype="multipart/form-data" >
                        <input class="entry_id" type="hidden" name="entry_id" value="<?php echo $e['id'] ?>" />
                        <input class="user_id" type="hidden" name="user_id" value="<?php echo $e['user_id'] ?>" />
                        <?php echo $this->image($user_image, 'nickImgInsertComment', "32px", "32px", $user_nick); ?>
                        <div class="commentInputContainer">
                            
                            <textarea class="commentInput pure-u-3-4" name="comment" placeholder="<?php echo $this->ts("write a comment...") ?>" ></textarea>
                            <a class="pure-button insertComment floatRight pure-u-1-4" href="#" onclick="return false;" >
                                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#share', 'svgIcon svgIcon-dark profilpageIcon commentSendIcon'); ?>
                                <span class="insertCommentText"><?php echo $this->ts("send") ?></span>
                            </a>
                        </div>

                        
                </form>
                <div style="clear:both"></div>
                <!--                    <div style="clear:both"></div>-->
            </div>
        </div>
    </div>

<?php endforeach; ?>
<?php if (isset($page) && isset($user)): ?>
    <div class="pure-u-1">
        <a class="pure-button pure-u-1 loadNextPage" onclick="return false;" href="<?php echo $this->action('profilpage/index/' . $user['id'] . "?page=" . ++$page); ?>" ><?php echo $this->ts('load more entries...'); ?></a>
    </div>
<?php endif; ?>
<?php if (isset($page) && !isset($user)): ?>
    <div class="pure-u-1">
        <a class="pure-button pure-u-1 loadNextPage" onclick="return false;" href="<?php echo $this->action("profilpage/dash?page=" . ++$page); ?>" ><?php echo $this->ts('load more entries...'); ?></a>
    </div>
<?php endif; ?>