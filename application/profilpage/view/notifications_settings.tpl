<!-- 

- Kontakt schreibt neuen Beitrag
- Jemand kommentiert eigenen Beitrag
- Jemand kommentiert fremden Beitrag den man zuvor auch kommentiert hat
- Man wird im Chat angeschrieben.
- neuer Event wird eingetragen

Optional
- neuer Kontakt

-->
<fieldset>
    <legend><?php echo $this->ts("profilpage") ?></legend>
    <div class="pure-controls">
         <label for="newentry" class="pure-checkbox">
            <div class="checker">
                <input id="newentry" class="checkbox" name="newentry" <?php echo $s['newentry'] ?> type="checkbox"> 
                <div class="check-bg"></div>
                <div class="checkmark">
                    <svg viewBox="0 0 100 100">
                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
            </div>
            <span class="label"><?php echo $this->ts('contact posts a new entry.');?></span>
        </label>
    </div>
    
    <div class="pure-controls">
         <label for="comownentry" class="pure-checkbox">
            <div class="checker">
                <input id="comownentry" class="checkbox" name="comownentry" <?php echo $s['comownentry'] ?> type="checkbox"> 
                <div class="check-bg"></div>
                <div class="checkmark">
                    <svg viewBox="0 0 100 100">
                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
            </div>
            <span class="label"><?php echo $this->ts('contact comments your entry.');?></span>
        </label>
    </div>
    
    <div class="pure-controls">
         <label for="comoncom" class="pure-checkbox">
            <div class="checker">
                <input id="comoncom" class="checkbox" name="comoncom" <?php echo $s['comoncom'] ?> type="checkbox">
                <div class="check-bg"></div>
                <div class="checkmark">
                    <svg viewBox="0 0 100 100">
                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
            </div>
            <span class="label"><?php echo $this->ts('contact comments an entry you commented, too.');?></span>
        </label>
    </div>
</fieldset>