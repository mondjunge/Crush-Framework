<h2><?php echo $this->ts("people to follow") ?></h2>
<div class="pure-g" >
    
    <?php foreach ($users as $u) : ?>
        <div class="pure-u-1-4">
            <div class="dashtile dashtile_nick">
            <a class="user_nickLink" href="<?php echo $this->action('profilpage/index/' . $u['id']); ?>"><?php echo $u['nick']; ?></a>
            <a class="" href="<?php echo $this->action('profilpage/index/' . $u['id']); ?>">
                <?php if ($u['image'] == '') : ?>
                    <?php echo $this->image('profil.png','follower','250','250',$u['nick']); ?>
                <?php else : ?>
                    <?php echo $this->image($relativeUrl . $u['image'],'follower','250','250',$u['nick']); ?>
                <?php endif; ?>
            </a>
            </div>
        </div>
    <?php endforeach; ?>
</div>