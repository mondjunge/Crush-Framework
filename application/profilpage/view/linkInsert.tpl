<form class="pure-form pure-form-aligned">
    <label for="link" ><?php echo $this->ts("insert link") ?></label>
    <input id="link" type="text" name="link" placeholder="z.B. http://www.youtube.com/?v=abcd1234"/>

    <div id="linkPreview" class="hidden">
        <div ><b><?php echo $this->ts('Preview') ?>:</b></div>
        <div id="linkContent"></div>
        
        <button id="linkInsert" type="submit" class="pure-button pure-button-primary" name="linkInsert" ><?php echo $this->ts('insert'); ?></button>
    </div>
</form>
<div class="help"><?php echo $this->ts('internet addresses will be made links, e-mail adresses will be hotlinked. On following adresses, content will get embeded:')?>
    <ul>
        <li>Dailymotion.com</li>
        <li>Soundcloud.com</li>
        <li>Youtube.com</li>
        <li>Vimeo.com</li>
        <li><?php echo $this->ts('more to come...');?></li>
    </ul>
</div>