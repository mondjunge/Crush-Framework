<input type="hidden" id="subdir" value="<?php echo $subdir; ?>" ></span>

<div class="pure-g pure-g-r dash">

    <div class="pure-u-3-5">

        <div class="pure-g pure-g-r">

            <div class="pure-u-1-3">
                <div class="dashtile dashtile_nick">
                    <?php echo $this->image($user_image, 'user_image_mini', "32px", "32px", $user_nick); ?>
                    <span class="user_nick">
                        <a class="user_nickLink" href="<?php echo $this->action('profilpage/index/'.$user_id); ?>"><?php echo $user_nick; ?></a>
                    </span>
                    <?php echo $this->image($user_image, 'user_image', "300px", "", $user_nick); ?>
                </div>
            </div>

            <div class="pure-u-2-3">
                <div class="dashtile">
                    <h2><?php echo $this->ts("write an entry, create content") ?></h2>
                    <form class="pure-form pure-form-stacked" method="POST" >
                        <textarea id="newEntry"></textarea>
                        
                        <?php echo$smilieHtml?>
                        <img id="activateSmilies" src="<?php echo $themeUrl ?>/images/smileys/green/smile.gif" alt=":)" title=":)" class="pure-button pure-button-secondary pure-button-small smilie activeSelect" />
                        <a id="imageSelect" class="pure-button pure-button-secondary pure-button-small fileSelect" data-selecthandler="modules.profilpage.selectClickHandler" data-type="image"><?php echo $this->ts('Image'); ?></a>
                        <a id="videoSelect" class="pure-button pure-button-secondary pure-button-small fileSelect" data-selecthandler="modules.profilpage.selectClickHandler" data-type="video"><?php echo $this->ts('Video'); ?></a>
                        <a id="linkInsert" class="pure-button pure-button-secondary pure-button-small linkInsert" data-type="link"><?php echo $this->ts('Link'); ?></a>

                        <input name="textcontent" type="hidden" value="" />
                        <input name="mediacontent" type="hidden" value="" />

                        <div id="preview" class="hidden">
                            <div ><b><?php echo $this->ts('Preview') ?>:</b></div>
                            <!--                            <div id="previewContent"></div>-->
                            <p>
                                <span id="textcontent"></span>
                            </p>
                            <div id="mediacontent"></div>

                            <button type="submit" name="sendEntry" class="pure-button pure-button-primary pure-button-small" ><?php echo $this->ts("send") ?></button>
                            <a href="" class="pure-button pure-button-error pure-button-small" ><?php echo $this->ts("cancel") ?></a>
                        </div>



                    </form>
                </div>
            </div>

        </div>
        <div class="pure-g pure-g-r">
            <div class="pure-u-1-3">
                <div class="dashtile calendar">
                    <?php echo $calendar ?>
                </div>
            </div>
            <div class="pure-u-2-3 ">
                <div class="dashtile">
                    <?php echo $notifications; //phpinfo() ?>
                    <!--                    Neustes Bild / Zufallsbild-->
                </div>
            </div>
        </div>
    </div>

    <div class="pure-u-2-5 ">
        <div class="dashtile newestEntries">
            <h2><?php echo $this->ts("newest entries") ?></h2>
            <?php echo $latestEntries ?>
        </div>
    </div> 

</div>