<div class="pure-g pure-g-r">

    <div class="pure-u-1-5">
        <div class="dashtile dashtile_nick">
            <?php echo $this->image($user['image'], 'user_image_mini', "32px", "32px", $user['nick']); ?>
            <span class="user_nick"><?php echo $user['nick'] ?></span>
            <?php echo $this->image($user['image'], 'user_image', "300px", "", $user['nick']); ?>
        </div>
    </div>
    <div class="pure-u-4-5">
        <div class="pure-g pure-g-r">

            <div class="pure-u-4-5">
                <div class="pure-u-1 chatButtonParent">
                    <?php if ($user_id == $user['id']) : ?>
                        <a class="pure-button pure-button-primary pure-button-small" 
                           href="<?php echo $this->action('user/adminOwnData'); ?>">
                            <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#credit-card', 'svgIcon svgIcon-light profilpageIcon'); ?>
                            <span clas="imgBtnText"><?php echo $this->ts('change own data'); ?></span></a>
                    <?php endif; ?>
                    <?php if ($user_id != $user['id']) : ?>
                        <?php if ($user['isFriend']) : $hideFollow = 'hidden'; $hideUnfollow = '';?>
                                
                             <?php else : $hideFollow = ''; $hideUnfollow = 'hidden';?>
                           
                        <?php endif; ?>
                    <a id="follow" class="pure-button pure-button-primary pure-button-small follow <?php echo $hideFollow;?>" 
                               href="<?php echo $this->action('user?addFriend=' . $user['id']); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#star', 'svgIcon svgIcon-light profilpageIcon'); ?>
                        <span clas="imgBtnText"><?php echo $this->ts('follow')." ".$user['nick']; ?></span></a>
                       
                     <a id="unfollow" class="pure-button pure-button-error pure-button-small follow <?php echo $hideUnfollow;?>" 
                               href="<?php echo $this->action('user?removeFriend=' . $user['id']); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#star', 'svgIcon svgIcon-light profilpageIcon'); ?>
                         <span clas="imgBtnText"><?php echo $this->ts('unfollow');//." ".$user['nick']; ?></span></a>

                    
                        <a class="pure-button pure-button-primary pure-button-small chatButton" 
                           href="<?php echo $this->action('messages?showDetail=' . $user['id']); ?>"><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#chat', 'svgIcon svgIcon-light profilpageIcon'); ?>
                            <span clas="imgBtnText"><?php echo $this->ts('private chat'); ?></span></a>
                    <?php endif; ?>
                </div>

                <div class="pure-u-1">
                    <div class="dashtile description">
                        <?php echo $user['description'] ?>
                    </div>
                </div>
            </div>
            <div class="pure-u-1-5">
                <div class="userStats">
                    <?php //if($adminMode):?>
                    <h5><?php echo $this->ts('Last time online:'); ?></h5>
                    <span class="statNr"><?php echo $stats['lastOnline'] ?></span>
                    <?php //endif;?>
                    <h5><?php echo $this->ts('Entries / comments recieved:'); ?></h5>
                    <span class="statNr"><?php echo $stats['entriesTotal'] ?> / <?php echo $stats['commentsRecieved'] ?></span>

                    <h5><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#thumb-up', 'svgIcon svgIcon-dark profilpageIcon'); ?>&nbsp;<?php echo $this->ts('Upvotes total / average:'); ?></h5>
                    <span class="statNr"><?php echo $stats['votesUp'] ?> / <?php echo $stats['votesUpAverage'] ?></span>
                    <h5><?php echo $this->icon('open-iconic/sprite/sprite.min.svg#thumb-down', 'svgIcon svgIcon-dark profilpageIcon'); ?>&nbsp;<?php echo $this->ts('Downvotes total / average:'); ?></h5>
                    <span class="statNr"><?php echo $stats['votesDown'] ?> / <?php echo $stats['votesDownAverage'] ?></span>
                    <h5><?php echo $this->ts('Followers / Principals:'); ?></h5>
                    <span class="statNr"><?php echo $stats['followersCount'] ?> / <?php echo $stats['principleCount'] ?></span>
                    
                    
                </div>
            </div>
        </div>
    </div>

    <?php echo $userEntries; ?>
</div>