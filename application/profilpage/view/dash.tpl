<div class="pure-g pure-g-r dash">
    <div class=" dashPaneLeft">
        <div class="pure-u-1-4">
            <div class="dashtile dashtile_nick">
                <?php echo $this->image($user_image, 'user_image_mini', "32px", "32px", $user_nick); ?>
                    <a class="user_nickLink" href="<?php echo $this->action('profilpage/index/' . $user_id); ?>"><?php echo $user_nick; ?></a>
                <?php echo $this->image($user_image, 'user_image', "300px", "", $user_nick); ?>
            </div>
        </div>
        <div id="createEntryNormal" class="pure-u-3-4">
            <div class="dashtile ">
                <h2><?php echo $this->ts("write an entry, create content") ?></h2>
                <form class="pure-form pure-form-stacked" method="POST" action="<?php echo $this->action('profilpage/dash') ?>" >
                    <fieldset>
                        <div class="dashEntry">
                            <textarea id="newEntry" class="" placeholder="<?php echo $this->ts("write something...") ?>" ></textarea>
                            
                            <div id="smilieSelect">
                                <?php //echo $smilieHtml ?>
                            </div>
                            
                            <a id="activateSmilies" class="pure-button pure-button-secondary pure-button-small activeSelect" href="<?php echo $this->action("profilpage/dash?getSmilies"); ?>" >
                                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#smilie', 'svgIcon svgIcon-dark profilpageIcon'); ?>
                                <?php echo $this->ts('Smilie'); ?></a>
                            <a id="imageSelect" class="pure-button pure-button-secondary pure-button-small fileSelect" href="#" data-title="<?php echo $this->ts('select picture');?>" data-selecthandler="modules.profilpage.selectClickHandler" data-type="image">
                                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#image', 'svgIcon svgIcon-dark profilpageIcon'); ?>
                                <?php echo $this->ts('Image'); ?></a>
                            <a id="videoSelect" class="pure-button pure-button-secondary pure-button-small fileSelect" href="#" data-title="<?php echo $this->ts('select video');?>" data-selecthandler="modules.profilpage.selectClickHandler" data-type="video">
                                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#video', 'svgIcon svgIcon-dark profilpageIcon'); ?>
                                <?php echo $this->ts('Video'); ?></a>
                            <a id="linkInsert" class="pure-button pure-button-secondary pure-button-small linkInsert" href="#" data-title="<?php echo $this->ts('enter link');?>" data-type="link">
                                <?php echo $this->icon('open-iconic/sprite/sprite.min.svg#link-intact', 'svgIcon svgIcon-dark profilpageIcon'); ?>
                                <?php echo $this->ts('Link'); ?></a>

                            <input name="textcontent" type="hidden" value="" />
                            <input name="mediacontent" type="hidden" value="" />

                            <div id="preview" class="dashEntry hidden">
                                <div ><b><?php echo $this->ts('Preview') ?>:</b></div>
                                <!--                            <div id="previewContent"></div>-->
                                <div class="entryText">
                                    <span id="textcontent"></span>
                                </div>
                                <div id="mediacontent"></div>

                                <button type="submit" name="sendEntry" class="pure-button pure-button-primary pure-button-small" ><?php echo $this->ts("send") ?></button>
                                <a href="" class="pure-button pure-button-error pure-button-small" ><?php echo $this->ts("cancel") ?></a>
                            </div>


                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        
    </div>
    <div class="pure-u-1 dashPaneRight">
        <!-- newEntry and current-->
        <div class="pure-u-1">
            <div class="dashtile calendarWidgetNormal">
                <?php echo $calendar ?>
            </div>
        </div>
        
        <div class="pure-u-1 ">
            <div class="dashtile newestEntries">

                <?php echo $latestEntries ?>
            </div>
        </div> 


    </div>

</div>
