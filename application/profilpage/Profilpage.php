<?php

/**
 *  Copyright © tim 01.03.2014
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Profilpage extends AppController implements INotify, ISearchable {

    private $friendsEnabled;

    public function getNotificationSettings($data) {

        return $this->fetch('notifications_settings', $data);
    }

    public function getNotificationCategories() {
        return array('newentry', 'comownentry', 'comoncom');
    }

    public function __construct() {
        ini_set('memory_limit', '1024M');
        ini_set('post_max_size', '1024M');
        ini_set('upload_max_filesize', '1024M');

        $this->modConf = $this->loadModuleConfiguration('profilpage');
        $this->friendsEnabled = $this->modConf['friendsEnabled'];
    }

    public function index() {
//put your code here
        $this->autoCheckRights();

        $this->includeCkeditor('');

        if (filter_has_var(INPUT_GET, 'deleteComment')) {
            $this->standalone = true;
            $this->deleteComment(filter_input(INPUT_GET, 'profileUserId'), filter_input(INPUT_GET, 'deleteComment'));
            $this->jumpToLast();
            return;
        }


        
        $sessionUserId = Session::get('uid');
        if (filter_has_var(INPUT_GET, 'user')) {
            $userId = filter_input(INPUT_GET, 'user');
        } else if (filter_has_var(INPUT_GET, 'id')) {
            $userId = filter_input(INPUT_GET, 'id');
        } else {
            $userId = $sessionUserId;
        }

        $this->includeJs('extras/autosize/jquery.autosize-min.js');

        $rs = $this->select('user', 'id,nick,email,description,image,last_online', "id='$userId'");

        if ($rs[0]['image'] == '') {
            $rs[0]['image'] = "profil.png";
        } else {
            $rs[0]['image'] = Config::get('relativeUrl') . $rs[0]['image'];
        }

        $data['user'] = $rs[0];
        $data['user']['isFriend'] = $this->isFriend($data['user']['id']);
        $data['user_image'] = Config::get('relativeUrl') . Session::get('uimage');
        $data['user_nick'] = Session::get('unick');
        $data['user_id'] = Session::get('uid');
        $data['adminMode'] = $this->checkSiteRights("profilpage/admin");
        $data['page'] = '1';

        $data['stats'] = $this->getStatsArray($userId);
        $data['stats']['lastOnline'] = $this->timeElapsedString(strtotime($rs[0]['last_online']));

        if (filter_has_var(INPUT_GET, 'page') && filter_input(INPUT_GET, 'page') > 1) {
            $this->standalone = true;
            $data['page'] = filter_input(INPUT_GET, 'page');
            $limitCount = 10;
            $limitTop = filter_input(INPUT_GET, 'page') * $limitCount;
            $limitMin = $limitTop - $limitCount;
            $limit = "$limitMin,$limitCount";
            $data['entries'] = $this->getUserEntries($userId, $limit);
            if (count($data['entries']) == 0) {
                echo $this->ts('older entries do not exist...');
                return;
            }
            echo $this->fetch('entries', $data);
            return;
        } else {
            $data['entries'] = $this->getUserEntries($userId);
        }

        $data['userEntries'] = $this->fetch('entries', $data);




        $this->view('user', $data);
    }

    private function timeElapsedString($ptime) {
        $etime = time() - $ptime;

        if ($etime < 1) {
            return $this->ts('just now');
        }

        $a = array(365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $a_plural = array('year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );

        if ($etime < 60) {
            return $this->ts('now');
        }

        if ($etime < 300) {
            return $this->ts('not 5 minutes ago');
        }

        if ($etime < 3600) {
            return $this->ts('less than an hour ago');
        }

        if ($etime < 3600 * 13) {
            return $this->ts('today');
        }

        if ($etime < 3600 * 36) {
            return $this->ts('yesterday');
        }

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $this->ts($a_plural[$str]) : $this->ts($str)) . $this->ts(' ago');
            }
        }
    }

    public function admin() {
//put your administrative code here

        /**
         * ONLY NEEDED TO CHECK FOR ADMIN RIGHTS IN PROFILPAGE
         */
    }

    /**
     * shows or fetches html of users the current user could follow
     * @param type $fetchOnly
     * @return type
     */
    public function usersToFollow($fetchOnly = false) {
        $this->autoCheckRights();
        
        $uid = Session::get('uid');

        $rs = $this->select('user', '*', 'id!=' . $uid);

        $data['users'] = $rs;
        $data['relativeUrl'] = Config::get('relativeUrl');
        if ($fetchOnly) {
            return $this->fetch('usersToFollow', $data);
        } else {
            $this->view('usersToFollow', $data);
        }
    }

    /**
     * gives an overview of existing content
     */
    public function dash() {
        $this->autoCheckRights();

        $this->includeCkeditor();

        if (filter_has_var(INPUT_GET, 'getSmilies')) {
            $this->standalone = true;
            echo $this->getSmiliesHtml();
            return;
        }

        if (filter_has_var(INPUT_GET, 'editEntry') && filter_has_var(INPUT_GET, 'userId')) {
            $this->standalone = true;
            echo $this->getEntry();
            return;
        }
        if (filter_has_var(INPUT_POST, 'updateEntry')) {
            $this->updateEntry();
            return;
        }

        if (filter_has_var(INPUT_GET, 'voteUp')) {
//$this->standalone = true;
            $newCount = $this->voteUp();
            if (!filter_has_var(INPUT_GET, 'ajax')) {
                $this->jumpToLast();
            } else {
                $this->standalone = true;
                echo $newCount;
                return;
            }
//return;
        }
        if (filter_has_var(INPUT_GET, 'voteDown')) {
//$this->standalone = true;
            $newCount = $this->voteDown();
            if (!filter_has_var(INPUT_GET, 'ajax')) {
                $this->jumpToLast();
            } else {
                $this->standalone = true;
                echo $newCount;
                return;
            }
//return;
        }

        if (filter_has_var(INPUT_POST, 'insertComment') && filter_input(INPUT_POST, 'text') != "") {
            $this->standalone = true;
            $this->doInsertComment();
            return;
        }

        if (filter_has_var(INPUT_GET, 'deleteComment')) {
            $this->standalone = true;
            $this->deleteComment(filter_input(INPUT_GET, 'profileUserId'), filter_input(INPUT_GET, 'deleteComment'));
            $this->jumpToLast();
            return;
        }

        if (filter_has_var(INPUT_GET, 'linkInsert')) {
            $this->standalone = true;
            $data = array();
            echo $this->fetch('linkInsert', $data);
            return;
        }

        if (filter_has_var(INPUT_GET, 'getLinkHtml')) {
            $this->standalone = true;
            $link = filter_input(INPUT_POST, 'link');
            $this->getLinkHtml($link);
            return;
        }

        if (filter_has_var(INPUT_GET, 'deleteEntry')) {
            $this->standalone = true;
            $this->deleteEntry(filter_input(INPUT_GET, 'userId'), filter_input(INPUT_GET, 'deleteEntry'));
            $this->jumpTo('profilpage/dash');
            return;
        }

        if (filter_has_var(INPUT_GET, 'changeDir')) {
            $this->standalone = true;
            $this->selectFile(filter_input(INPUT_GET, 'fileType'), filter_input(INPUT_GET, 'changeDir'));
            return;
        }
        if (filter_has_var(INPUT_GET, 'selectFile')) {
            $this->standalone = true;
            $this->selectFile(filter_input(INPUT_GET, 'fileType'), "");
            return;
        }
        if (filter_has_var(INPUT_GET, 'getFileHtml')) {
            $this->standalone = true;
            $fileType = filter_input(INPUT_GET, 'fileType');

            $path = filter_input(INPUT_GET, 'getFileHtml');
            $data['path'] = $path;

            echo "<div class='mediacontent'>" . $this->fetch($fileType . 'Html', $data) . "</div>";
            return;
        }
        if (filter_has_var(INPUT_GET, 'uploadFile')) {
            $this->standalone = true;

            $subdir = filter_input(INPUT_POST, 'subdir');
            $fileType = filter_input(INPUT_POST, 'fileType');
            $this->uploadFile($fileType, $subdir);
            $this->selectFile($fileType, $subdir);
            return;
        }
        if (filter_has_var(INPUT_POST, 'sendEntry')) {
            $this->sendEntry();
            $this->jumpTo('profilpage/dash');
        }
        if (filter_has_var(INPUT_POST, 'getPreview')) {
            $this->standalone = true;
            $this->sendEntry(true);
            return;
        }

        $this->createUserTableIfNotExists();

        $this->includeJs('extras/autosize/jquery.autosize-min.js');

        $this->includeJs(Config::get('applicationDirectory').'/files/js/.files.js');

        $data['calendar'] = $this->loadWidget('calendar/widget');

        $data['user_image'] = Config::get('relativeUrl') . Session::get('uimage');
        $data['user_nick'] = Session::get('unick');
        $data['user_id'] = Session::get('uid');
        $data['adminMode'] = $this->checkSiteRights("profilpage/admin");

        $data['page'] = '1';

        if (filter_has_var(INPUT_GET, 'page') && filter_input(INPUT_GET, 'page') > 1) {
            $this->standalone = true;
            $data['page'] = filter_input(INPUT_GET, 'page');

            $data['entries'] = $this->getLatestEntries($data['page']);
            if (count($data['entries']) == 0) {
                echo $this->ts('older entries do not exist...');
                return;
            }
            echo $this->fetch('entries', $data);
            return;
        } else {
            $data['entries'] = $this->getLatestEntries();
        }


        if (sizeof($data['entries']) == 0) {
            $data['latestEntries'] = $this->usersToFollow(true);
        } else {
            $data['latestEntries'] = '<div class="pure-u-1"><h2>'
                    . $this->ts("newest entries") . '</h2></div>'
                    . $this->fetch('entries', $data)
                    . $this->usersToFollow(true);
        }

        $data['smilieHtml'] = $this->getSmiliesHtml();
        $data['themeUrl'] = Config::get('relativeUrl') . "theme/" . Config::get('siteStyle');

        $this->view('dash', $data);
    }

    /**
     * checks if given UserId is in the friends list of user table
     * @param type $userId
     * @return boolean
     */
    private function isFriend($userId) {
        
        $friendsRs = $this->select('user', 'friends', "id=" . Session::get('uid'));

        if ($friendsRs[0]['friends'] == "") {
            $bReturn = false;
        } else {
            $fArr = explode(",", $friendsRs[0]['friends']);
            if (in_array($userId, $fArr)) {
                $bReturn = true;
            } else {
                $bReturn = false;
            }
        }
        return $bReturn;
    }

    private function getEntry() {
        $entryId = filter_input(INPUT_GET, 'editEntry');
        $userId = filter_input(INPUT_GET, 'userId');
        $userTable = "profile_" . $userId;
        $rs = $this->select($userTable, '*', 'id=' . $entryId);
        $vars['e'] = $rs[0];
//$vars['e']['userId'] = $userId;
        return $this->fetch('editEntry', $vars);
    }

    private function updateEntry() {
        $entryId = filter_input(INPUT_POST, 'id');
        $userId = filter_input(INPUT_POST, 'userId');
        $text = filter_input(INPUT_POST, 'text');
        $userTable = "profile_" . filter_input(INPUT_POST, 'userId');

        $fieldsValueArray = array(
            'text' => $text
        );
        $this->updatePrepare($userTable, $fieldsValueArray, "id=:id", array('id' => $entryId));

        $this->setSystemMessage($this->ts('entry updated successfully'));

        $this->jumpTo('profilpage/index/' . $userId . '#entry' . $entryId . 'u' . $userId);
        exit;
    }

    private function getStatsArray($userId) {
        $userCommentsTable = "profile_" . $userId . "_comments";
        $userTable = "profile_" . $userId;

        $this->createUserTableIfNotExists($userId);
//        if(!$this->tableExists($userCommentsTable)){
//            return;
//        }

        $stats = array();
        $stats['entriesTotal'] = $this->count($userTable);

        $stats['commentsRecieved'] = $this->count($userCommentsTable);

        $rsVotes = $this->select($userTable, 'votes_up,votes_down', "votes_up != '' OR votes_down != ''");
        $stats['votesUp'] = 0;
        $stats['votesDown'] = 0;
        foreach ($rsVotes as $v) {
            if ($v['votes_up'] != '') {
                $stats['votesUp'] = $stats['votesUp'] + count(explode(',', $v['votes_up']));
            }

            if ($v['votes_down'] != '') {
                $stats['votesDown'] = $stats['votesDown'] + count(explode(',', $v['votes_down']));
            }
        }
        $voteUpAverage = 0;
        $voteDownAverage = 0;
        if ($stats['entriesTotal'] != 0) {
            $voteUpAverage = $stats['votesUp'] / $stats['entriesTotal'];
            $voteDownAverage = $stats['votesDown'] / $stats['entriesTotal'];
        }
        if (empty($this->getFollowers($userId))) {
            $stats['followersCount'] = 0;
        } else {
            $stats['followersCount'] = count($this->getFollowers($userId));
        }
        if (empty($this->getPrincipalsIds($userId))) {
            $stats['principleCount'] = 0;
        } else {
//print_r($this->getPrincipalsIds($userId));
            $stats['principleCount'] = count($this->getPrincipalsIds($userId));
        }



        $stats['votesUpAverage'] = round($voteUpAverage, 2);
        $stats['votesDownAverage'] = round($voteDownAverage, 2);

        return $stats;
    }

    private function getSmiliesHtml() {
        $s = array();
        $s['icons'] = $this->getSmilies();
        return $this->fetch('smilies', $s);
    }

    private function getLinkHtml($link) {

//        if (!$this->isUrl($link)) {
//            echo $this->ts('no url found.');
//            return;
//        }
//        $title = $this->getTitleCurl($link);
//        $favicon = $this->getFavicon($link);

        $anchor = $this->replaceUrls($link);
//        if ($title != "Untitled") {
//            $title = "<b>" . $title . "</b><br/>";
//            $return = $favicon . "&nbsp;" . $title . $anchor;
//        } else {
//            $return = $favicon . "&nbsp;" . $anchor;
//        }

        echo $anchor;
    }

    private function replaceUrls($string) {

        /*         * * make sure there is an https:// on all URLs ** */
        $string = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-\+%]+)/i", "$1https://$2", $string);


        $pattern = '/([\w]+:\/\/[\w\-?!&;:#,%~=\.\/\@\+]+[\w\/])/i';
        $string = preg_replace_callback($pattern, array($this, 'replaceSwitch'), $string);

        return $string;
    }

    private function replaceSwitch($matches) {
        if (strpos($matches[1], 'youtube.') !== false) {
            return $this->replaceYoutube($matches[1]);
        } else if (strpos($matches[1], 'youtu.') !== false) {
            return $this->replaceYoutu($matches[1]);
        } else if (strpos($matches[1], 'vimeo.') !== false) {
            return $this->replaceVimeo($matches[1]);
        } else if (strpos($matches[1], 'soundcloud.') !== false) {
            return $this->replaceSoundcloud($matches[1]);
        } else if (strpos($matches[1], 'dailymotion.') !== false) {
            return $this->replaceDailyMotion($matches[1]);
        } else if (strpos($matches[1], 'facebook.') !== false) {
            return $this->replaceFacebookVideo($matches[1]);
        } else if (strtolower(substr($matches[0], -4, 4)) == '.jpg' || strtolower(substr($matches[0], -4, 4)) == '.jpeg' || strtolower(substr($matches[0], -4, 4)) == '.png' || strtolower(substr($matches[0], -4, 4)) == '.gif') {
            return $this->replaceImage($matches[0]);
        } else {
            $title = $this->getTitleCurl($matches[1]);
            $favicon = $this->getFavicon($matches[1]);
            return "<br/>" . $favicon . "&nbsp;<b>" . $title . "</b><br/>" . $this->replaceUrl($matches[1]);
        }
    }

    private function replaceImage($string) {
        $data['path'] = $string;
        $data['width'] = '';
        $return = $this->fetch('imageHtml', $data);

        return $return;
    }

    private function replaceFacebookVideo($string) {
// example
// https://www.facebook.com/MrCianTwomey/videos/771163216349283/
        $pattern = '/https?:\/\/w?w?w?\.?facebook\.com\/(.*?)\/videos\/([a-zA-Z0-9_\-]+)(\S*)/i';
        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($string);
        $replaceNew = "<div id='fb-root'>&nbsp;</div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your embedded video player code -->
  <div class='fb-video' data-href='https://www.facebook.com/$1/videos/$2/' data-width='500' data-show-text='false'>
    <div class='fb-xfbml-parse-ignore'>
      <blockquote cite='https://www.facebook.com/$1/videos/$2/'>
        Posted by <a href='https://www.facebook.com/$1/'>$1</a>
      </blockquote>
    </div>
  </div>";
        return "" . $title . "<br/>" . preg_replace($pattern, $replaceNew, $string) . $link;
    }

    private function replaceVimeo($string) {
        $pattern = '/https?:\/\/vimeo\.com\/m?\/?([a-zA-Z0-9_\-]+)(\S*)/i';
//$replace = '<object width="100%" ><param name="movie" value="https://www.youtube.com/v/$2&amp;hl=en_US&amp;fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/$2&amp;hl=en_US&amp;fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="100%" ></embed></object>';
//$replaceNew = '<iframe width="100%" height="auto" src="https://www.youtube.com/embed/$2" frameborder="0" allowfullscreen></iframe>';
//<iframe src="//player.vimeo.com/video/93365760?byline=0&amp;color=9ebf33" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        $replaceNew = '<iframe class="vimeo" src="//player.vimeo.com/video/$1" allowfullscreen></iframe>';

        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($string);
        return "" . $title . "<br/>" . preg_replace($pattern, $replaceNew, $string) . $link . "";
    }

    private function replaceYoutube($string) {
        $pattern = '/https?:\/\/w?w?w?\.?youtube\.com\/watch(.*?)v?=?([a-zA-Z0-9_\-]+)(\S*)/i';
        return $this->makeYoutubeIframe($string, $pattern); //"" . $title . "<br/>" . $replaceNew . '<iframe class="privacyIframe privacyIframeHidden" src="" allowfullscreen="" frameborder="0" border="0" cellspacing="0"></iframe>' .$link;
    }

    private function replaceYoutu($string) {
        $pattern = '/https?:\/\/youtu\.(be)\/([a-zA-Z0-9_\-]+)(\S*)/i';
        return $this->makeYoutubeIframe($string, $pattern);
    }

    private function makeYoutubeIframe($string, $pattern) {

        $replaceNewUrl = 'https://www.youtube-nocookie.com/watch/$2';
        $newUrl = preg_replace($pattern, $replaceNewUrl, $string);


        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($newUrl);

        $dataurl = preg_replace($pattern, "https://www.youtube-nocookie.com/embed/$2", $string);
        $replaceNew = "<a class='privacyImageLink' href='$dataurl' data-class='youTube' data-url='$dataurl' onclick='return false;'>";
        $replaceNew .= $this->imageHtml('youtube.png', 'privacyImage'); //'<iframe class="youTube" src="https://www.youtube.com/embed/$2" allowfullscreen></iframe>';
        $replaceNew .= "</a>";

        return "" . $title . "<br/>" . $replaceNew . '<iframe class="privacyIframe privacyIframeHidden" src="" allowfullscreen="" frameborder="0" border="0" cellspacing="0"></iframe>' . $link;
    }

    private function replaceSoundcloud($string) {
//$pattern = '/https?:\/\/w?\.?soundcloud\.com\/(.*?)\/([a-zA-Z0-9_\-]+)(\S*)/i';
#https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/138362966&color=ff5500&auto_play=false&hide_related=false&show_artwork=true
        $newUrl = 'https://w.soundcloud.com/player/?url=' . urlencode($string) . "&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=true";

        $link = "<br/>" . $this->replaceUrl($string);
        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";

        $dataurl = $newUrl;
        $replaceNew = "<a class='privacyImageLink' href='$dataurl' data-class='soundcloud' data-url='$dataurl' onclick='return false;'>";
        $replaceNew .= $this->imageHtml('soundcloud.jpg', 'privacyImage'); //'<iframe class="youTube" src="https://www.youtube.com/embed/$2" allowfullscreen></iframe>';
        $replaceNew .= "</a>";

        $iFrame = '<iframe class="privacyIframe privacyIframeHidden" src="" allowfullscreen="" frameborder="0" border="0" cellspacing="0"></iframe>';

        return "" . $title . "<br/>" . $replaceNew . $iFrame . $link;
    }

    private function replaceDailyMotion($string) {
#<iframe frameborder="0" width="480" height="270" src="http://www.dailymotion.com/embed/video/x1eg6fc" allowfullscreen></iframe>
        $pattern = '/https?:\/\/www\.dailymotion.com\/video\/([a-zA-Z0-9_\-]+)(\S*)/i';

        $favicon = $this->getFavicon($string);
        $title = $favicon . " <b>" . $this->getTitleCurl($string) . "</b>";
        $link = "<br/>" . $this->replaceUrl($string);

        $dataurl = preg_replace($pattern, "https://www.dailymotion.com/embed/video/$1", $string);
        $replaceNew = "<a class='privacyImageLink' href='$dataurl' data-class='dailyMotion' data-url='$dataurl' onclick='return false;'>";
        $replaceNew .= $this->imageHtml('youtube.png', 'privacyImage'); //'<iframe class="youTube" src="https://www.youtube.com/embed/$2" allowfullscreen></iframe>';
        $replaceNew .= "</a>";

        return "" . $title . "<br/>" . $replaceNew . '<iframe class="privacyIframe privacyIframeHidden" src="" allowfullscreen="" frameborder="0" border="0" cellspacing="0"></iframe>' . $link;
    }

    private function replaceUrl($string) {
        /*         * * make all URLs links ** */
        $string = preg_replace('/([\w]+:\/\/[\w\-?!&:;#,%~=\.\/\@\+]+[\w\/])/i', "<a target=\"_blank\" href=\"$1\">$1</a>", $string);
        /*         * * make all emails hot links ** */
        $string = preg_replace("/([\w\-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i", "<A HREF=\"mailto:$1\">$1</A>", $string);

        return $string;
    }

    /**
     * 
     * @param string $url
     * @return string
     */
    private function getUrlContent($url) {
// get html via url
        $savePath = dirname(__FILE__) . "/../../cache/html/" . md5($url) . ".html";
        if (!is_dir(dirname(__FILE__) . "/../../cache/html/")) {
            mkdir(dirname(__FILE__) . "/../../cache/html/", 0777);
        }
        if (is_file($savePath)) {
//echo "ist schon da!";
            return file_get_contents($savePath);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
//        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 0);
        $html = utf8_decode(curl_exec($ch));
        curl_close($ch);
//        fclose($fp);
//        chmod($savePath, 0644);
        file_put_contents($savePath, $html);

        return $html;
    }

    public function getTitleCurl($url) {
        if ($url == null) {
            die;
        }
        $this->checkSiteRights('profilpage/dash');
//return $url;

        $html = $this->getUrlContent($url);

// get title
        $dom = new DomDocument();
        libxml_use_internal_errors(true);
        if ($html != "") {
            $dom->loadHTML($html);
            if ($dom->documentElement != null) {
                $titleTagElement = $dom->documentElement->getElementsByTagName('title');
                $titleRaw = $titleTagElement->item('0')->textContent;
                $title = trim(empty($titleRaw) ? 'Untitled' : $titleRaw);
            }
        } else {
            $title = $url;
        }



// convert title to utf-8 character encoding
        if ($title != 'Untitled') {
            $match = array();
            preg_match('/<charset\=(.+)\"/i', $html, $match);
            if (!empty($match[0])) {
                $charset = str_replace('"', '', $match[0]);
                $charset = str_replace("'", '', $charset);
                $charset = strtolower(trim($charset));
                if ($charset != 'utf-8') {
                    $title = iconv($charset, 'utf-8', $title);
                }
            }
        }

        return $title;
    }

    private function isUrl($string) {
        $pattern = '/([\w]+:\/\/[\w\-?!&:;#,%~=\.\/\@\+]+[\w\/])/i';
                    // '[\w]+:\/\/[\w\-?!&:;#,%~=\.\/\@\+]+[\w\/]'

        if (preg_match($pattern, $string) == 1) {
            return true;
        }
    }

    private function get_file_extension($file_name) {
        return substr(strrchr($file_name, '.'), 1);
    }

    public function getFavicon($url) {
        if ($url == null) {
            die;
        }
        $this->checkSiteRights('profilpage/dash');

        $imgUrl = "cache/thumbs/src/" . md5($url); //Config::get('relativeUrl').
//echo $imgUrl;
        $imgHtml = "<img src='" . Config::get('relativeUrl') . $imgUrl . "' class='ext_fav' style='max-width:16px;'/>";
        if (is_file($imgUrl)) {
            return $imgHtml;
        }


        $savePath = dirname(__FILE__) . "/../../cache/thumbs/src/" . md5($url);
        $urlParsed = parse_url($url);

// check for standard favicon
        $userPath = $urlParsed['host'];
        $path = "http://" . $userPath . "/favicon.ico";
        $header = get_headers($path);
        if (preg_match("|200|", $header[0])) {
            $this->cacheFavicon($path, $savePath);
            return $imgHtml;
//return $this->imageHtml($path, 'ext_fav', '16px'); //$this->imageHtml();//'<img src="'..'" />';
        }


# make the URL simpler
        $elems = parse_url($url);
        $url = $elems['scheme'] . '://' . $elems['host'];

# load site
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = curl_exec($ch);
        curl_close($ch);

# look for the shortcut icon inside the loaded page
        $matches = array();
        $regex_pattern = "/rel=\"shortcut icon\" (?:href=[\'\"]([^\'\"]+)[\'\"])?/";
        preg_match_all($regex_pattern, $output, $matches);

        if (isset($matches[1][0])) {
            $favicon = $matches[1][0];

# check if absolute url or relative path
            $favicon_elems = parse_url($favicon);

# if relative
            if (!isset($favicon_elems['host']) && substr($favicon, 0, 2) != "//") {
                $favicon = $url . '/' . $favicon;
            }

            $this->cacheFavicon($favicon, $savePath);

            return $imgHtml;
//return $this->imageHtml($favicon, 'ext_fav', '16px'); //'<img src="'..'" />';
//return "<img src='".$favicon."' />";
        }

        return false;
    }

    private function cacheFavicon($url, $savePath) {
        if (is_file($savePath)) {
            return;
        } else {
            if (substr($url, 0, 2) == '//') {
                $url = "http:" . $url;
            }
            if (strpos($url, '?') !== FALSE) {
                $arr = explode('?', $url);
                $url = $arr['0'];
            }
            $imgUrl = $url;

            $ch = curl_init($imgUrl);
            $fp = fopen($savePath, 'x');

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec($ch);
            curl_close($ch);


            fclose($fp);
            return;
        }
    }

    /**
     * delete own comment (or any if superuser)
     * @param type $profileUserId
     * @param type $commentId 
     */
    private function deleteComment($profileUserId, $commentId) {
        
        $userCommentsTable = "profile_" . $profileUserId . "_comments";
// check if comment is own comment or user is superadmin or comment table is own comment table
        $checkRs = $this->select($userCommentsTable, '*', "id='" . $commentId . "' AND user_id='" . Session::get('uid') . "'");
        if ($checkRs !== FALSE || $this->checkSiteRights('profilpage/admin') || $profileUserId == Session::get('uid')) {
            $this->delete($userCommentsTable, "id='" . $commentId . "'");
        }
    }

    private function deleteEntry($userId, $id) {
        $userTable = "profile_" . $userId;
        $userCommentsTable = "profile_" . $userId . "_comments";
        
        if ($userId == Session::get('uid') || $this->checkSiteRights('profilpage/admin')) {
            $this->delete($userTable, "id='$id'");
            $this->delete($userCommentsTable, "entry_id='$id'");
            $this->setSystemMessage($this->ts("Entry deleted"));
        } else {
            $this->setErrorMessage($this->ts("You are not allowed to delete this entry!"));
        }
        return;
    }

    private function getUserEntries($userId, $limit = '0,10') {
        $userTable = "profile_" . $userId;
        if (!$this->tableExists($userTable)) {
            $this->createUserTableIfNotExists($userId);
        }

        $return = array();
        $rs = $this->select($userTable, "id,user_id,text,votes_up,votes_down,time", "active=1", 'time DESC', "$limit");
        $uI = $this->select("user", 'nick,image', "id='" . $userId . "'");
        $comments = $this->indexCommentsByEntryId($this->getComments($userId));

        foreach ($rs as $r) {
            $r['time'] = date($this->ts('Y-m-d H:i'), strtotime($r['time']));

            if ($r['votes_up'] != '') {
                $upArr = explode(',', $r['votes_up']);
                $r['votes_up_count'] = count($upArr);
                $r['votes_up_users'] = $this->getUserNicksAsCsv($r['votes_up']);
            } else {
                $r['votes_up_count'] = "0";
                $r['votes_up_users'] = "";
            }

            if ($r['votes_down'] != '') {
                $downArr = explode(',', $r['votes_down']);
                $r['votes_down_count'] = count($downArr);
                $r['votes_down_users'] = $this->getUserNicksAsCsv($r['votes_down']);
            } else {
                $r['votes_down_count'] = "0";
                $r['votes_down_users'] = "";
            }


            if (!isset($comments[$r['id']]) || $comments[$r['id']] == null) {
                $r['comments'] = array();
            } else {
                $r['comments'] = $comments[$r['id']];
            }
//$r['comments'] = $this->getComments($userId, $r['id']);
            if ($uI[0]['image'] == '') {
                $r['image'] = "profil.png";
            } else {
                $r['image'] = Config::get('relativeUrl') . $uI[0]['image'];
            }
            $r['nick'] = $uI[0]['nick'];
            $return[] = $r;
        }
        return $return;
    }

    private function getPrincipalsIds($userId) {
        $userIds = array();
        $rs = $this->select('user', 'friends', 'id=' . $userId);
        if ($rs !== FALSE && $rs[0]['friends'] != '') {
            $userIds = explode(',', $rs[0]['friends']);
        }

        return $userIds;
    }

    private function getLatestEntries($page = 1) {

        

        $userProfilTables = array();
// get all user ids


        if (Config::get('friendsEnabled')) {
            $userIds = $this->getPrincipalsIds(Session::get('uid'));
            $userIds[] = Session::get('uid');
        } else {
            $rs = $this->select('user', 'id');
            foreach ($rs as $u) {
                $userIds[] = $u['id'];
            }
        }

        foreach ($userIds as $uId) {
// if ($u['id'] != Session::get('uid')){
//$userIds[] = $u['id'];
            if ($this->tableExists("profile_" . $uId)) {
// $userTable = Config::get('dbPrefix')."profile_" . $u['id'];
                $userTable = "profile_" . $uId;

                $userProfilTables[] = $userTable;
            }
//}
        }
        $sizeOfUserProfilTables = sizeof($userProfilTables);
        if ($sizeOfUserProfilTables == 0) {
            return array();
        }

//$rs = array();
        $limitCount = round(50 / $sizeOfUserProfilTables);
        if ($limitCount < 1) {
            $limitCount = 1;
        }
        $limitMin = ($page - 1) * $limitCount;
//echo $limitCount;

        $return = array();
        foreach ($userProfilTables as $t) {
            $rs = $this->select($t, "id,user_id,text,votes_up,votes_down,time", "active=1", 'time DESC', "$limitMin,$limitCount");
            if (sizeof($rs) > 0) {
                $uI = $this->select("user", 'nick,image', "id='" . $rs[0]['user_id'] . "'");
                $comments = $this->indexCommentsByEntryId($this->getComments($rs[0]['user_id']));

                foreach ($rs as $r) {
//var_dump($comments);

                    if ($r['votes_up'] != '') {
                        $upArr = explode(',', $r['votes_up']);
                        $r['votes_up_count'] = count($upArr);
                        $r['votes_up_users'] = $this->getUserNicksAsCsv($r['votes_up']);
                    } else {
                        $r['votes_up_count'] = "0";
                        $r['votes_up_users'] = "";
                    }

                    if ($r['votes_down'] != '') {
                        $downArr = explode(',', $r['votes_down']);
                        $r['votes_down_count'] = count($downArr);
                        $r['votes_down_users'] = $this->getUserNicksAsCsv($r['votes_down']);
                    } else {
                        $r['votes_down_count'] = "0";
                        $r['votes_down_users'] = "";
                    }


                    if (!isset($comments[$r['id']]) || $comments[$r['id']] == null) {
                        $r['comments'] = array();
                    } else {
                        $r['comments'] = $comments[$r['id']];
                    }
//$r['comments'] = $comments[$r['id']];
//                $r['comments'] = $this->getComments($r['user_id'], $r['id']);
//var_dump($r['comments']);
                    if ($uI[0]['image'] == '') {
                        $r['image'] = "profil.png";
                    } else {
                        $r['image'] = Config::get('relativeUrl') . $uI[0]['image'];
                    }
                    $r['nick'] = $uI[0]['nick'];
                    $return[] = $r;
                }
            }
        }
        $returnSorted = $this->sortFiles($return, 'time');


        $returnFormatted = array();
        foreach ($returnSorted as $r) {
            $r['time'] = date($this->ts('Y-m-d H:i'), strtotime($r['time']));
            $returnFormatted[] = $r;
        }

        return $returnFormatted;
    }

    private function getUserNicksAsCsv($userIdsCsv) {
        if ($userIdsCsv == '') {
            return '';
        }
        $userRs = $this->select('user', 'nick', "id IN($userIdsCsv)");
        $userNicksCsv = '';
        $n = 0;
        foreach ($userRs as $user) {
            ++$n;
            if ($userNicksCsv == '') {
                $userNicksCsv = " " . $user['nick'];
            } else {
                if (count($userRs) == $n) {
                    $userNicksCsv .= " " . $this->ts('and') . "\n " . $user['nick'];
                } else {
                    $userNicksCsv .= ",\n " . $user['nick'];
                }
            }
        }
        return $userNicksCsv;
    }

    private function getComments($userId, $entryId = '') {
        $aConfig = Config::getConfig();
        if ($userId == '') {
            return array();
        }
        $userCommentsTable = "profile_" . $userId . "_comments";
        if (!$this->tableExists($userCommentsTable)) {
            $this->createCommentsTableIfNotExists($userId);
        }
        $entryWhere = '';
        if ($entryId != '') {
            $entryWhere = ' AND entry_id = ' . $entryId;
        }
        $rs = $this->select($userCommentsTable . " as c , " . $aConfig['dbPrefix'] . "user as u", 'c.id,c.user_id,c.entry_id,c.text,c.votes_up,c.votes_down,c.active,c.time,u.nick,u.image', 'c.active=1 AND u.id=c.user_id' . $entryWhere, "c.time asc");
        $return = array();
        if ($rs !== FALSE) {
            foreach ($rs as $r) {
                $r['time'] = date($this->ts('Y-m-d H:i'), strtotime($r['time']));
                if ($r['image'] == '') {
                    $r['image'] = "profil.png";
                } else {
                    $r['image'] = Config::get('relativeUrl') . $r['image'];
                }
                $return[] = $r;
            }
        }
//var_dump($return);
        return $return;
    }

    private function indexCommentsByEntryId($comments) {
//var_dump($comments);
        $return = array();
        foreach ($comments as $c) {
            $return[$c['entry_id']][] = $c;
        }
//var_dump($return);
        return $return;
    }

//    private function getComments($userId, $entryId) {
//        $userCommentsTable = "profile_" . $userId . "_comments";
//        //$userTable = Config::get('dbPrefix') . "profile_" . $userId;
//        $return = array();
//        
//        $rs = $this->select($userCommentsTable,'*',"entry_id='$entryId' and active='1'",'time desc');
//        if($rs!==FALSE){
//            foreach($rs as $r){
//                $r['time'] = date($this->ts('Y-m-d H:i'), strtotime($r['time']));
//            }
//            $return = $rs;
//        }
////        $sql = "SELECT ut.id,ut.user_id,ut.text,ut.time,ut.votes_up,ut.votes_down, "
////                + "uct. "
////                + "FROM $userTable as ut, $userCommentsTable as uct "
////                + "WHERE ut.id=uct.entry_id AND ut.active=1 "
////                + "ORDER BY ut.time LIMIT 0,10";
//
//
//        return $return;
//    }


    private function getFollowers($userId, $idsAsCsv = false) {

        if ($idsAsCsv) {
            $rsRaw = $this->select('user', 'id', "friends LIKE '%,$userId' OR friends LIKE '$userId,%' OR friends LIKE '%,$userId,%' OR friends='$userId'");

            $arr = array();

            foreach ($rsRaw as $r) {
                $arr[] = $r['id'];
            }
            $rs = implode(',', $arr);
        } else {
            $rs = $this->select('user', 'id,nick,image', "friends LIKE '%,$userId' OR friends LIKE '$userId,%' OR friends LIKE '%,$userId,%' OR friends='$userId'");
        }
        return $rs;
    }

    /**
     * Database: Error inserting data. (You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'mediacontent'> 
     * @param type $preview
     * @return type
     */
    private function sendEntry($preview = false) {
//echo "WHAAT?"; exit;
        
        $userId = Session::get('uid');
//$userFollowers = $this->getFollowers($userId);
        $userTable = "profile_" . $userId;
        $this->createUserTableIfNotExists($userId);

        if ($preview) {
            echo $this->replaceSmilies($this->replaceUrls(filter_input(INPUT_POST, 'textcontent')));
            return;
        }

        $text = "<span class='textcontent'>" . $this->replaceSmilies($this->replaceUrls(filter_input(INPUT_POST, 'textcontent'))) . "</span>";
        $media = filter_input(INPUT_POST, 'mediacontent'); //"<div class=\'mediacontent\'>" . filter_input(INPUT_POST, 'mediacontent') . "</div>";

        $fieldsValueArray = array(
            'text' => $text . " " . $media ,
            'active' => "1",
            'user_id' => $userId
        );

        $newEntryId = $this->insert($userTable, $fieldsValueArray);
        if($newEntryId!==FALSE){
            $this->setSystemMessage($this->ts("Entry successfully created!"));
        }

// DONE: generalize Notification if possible
//        $umlautUrl = str_replace("xn--", "ü", Config::get('rootUrl'));
//        $umlautUrl = str_replace("-jva", "", $umlautUrl);

        $followersOnly = '';
        if (Config::get('friendsEnabled')) {
            $followersA = $this->getFollowers($userId, true);
            $followersOnly = "AND id IN (" . $followersA . ")";
        }

        $category = "newentry";
        $usersRsToNotify = $this->select('user'
                , 'id, email, nick'
                , "active=1 AND id!='$userId' $followersOnly");

//$link = Config::get('reLangUrl') . "profilpage/index/" . $userId . "#entry" . $newEntryId;
//$link = "<a href='$link'>".$link."</a>";
        $linkToSource = "profilpage/index/" . $userId . "#entry" . $newEntryId . "u" . $userId;

        $messageParams[1] = Session::get('unick');
        $message = "$1 created a new entry";

        $n = new Notification();
        $n->setCategory($category);
        $n->setMessage($message);
        $n->setMessageParams($messageParams);
        $n->setSubject("Neuer Beitrag");
        $n->setLink($linkToSource);
                
        $this->setNotification($n, $usersRsToNotify);


//        $subject = $this->ts('New Entry from $2');
//
//        $message = $this->ts("Hello $1,");
//        $message .= "<br/><br/>";
//        $message .= $this->ts("there is a new entry from $2.");
//        $message .= "<br/><br/>";
//        $message .= $this->ts("Take a look now under following link:");
//        $message .= "<br/><br/>";
//        $message .= "<a href='$umlautUrl$link'>" . $umlautUrl . $link . "</a>";
//
//        $this->notifyUsers($rs, $category, $subject, $message, $link);
//        if ($rs !== FALSE && sizeof($rs) > 0 && $newEntryId !== FALSE) {
//            $nick = Session::get('unick');
//            foreach ($rs as $r) {
//                $subject = $this->ts('New Entry from $2');
//
//                $message = $this->ts("Hello $1,");
//                $message .= "\n\r";
//                $message .= $this->ts("there is a new entry from $2.");
//                $message .= "\n\r";
//                $message .= $this->ts("Take a look now under following link:");
//                $message .= "\n\r";
//                $message .= $link;
//                $message = str_replace('$1', $r['nick'], $message);
//                $message = str_replace('$2', $nick, $message);
//
//                mail($r['email'], $subject, $message);
//            }
//        }
    }

    private function selectFile($fileType, $subdir = "") {
        $filesA = array();
        $dirsA = array();
        if (sizeof($subdir) > 0 && $subdir != "" && $subdir != "/" && substr($subdir, -1, 1) != "/") {

            $subdir = $subdir . "/";
//$dirsA[] = "/";
        }
        if ($subdir == "/") {
            $subdir = "";
        }

        
        $userId = Session::get('uid');
        $userImageDir = "upload/users/" . $userId . "/" . $fileType . "s/" . $subdir;
        $userImageDirPath = __DIR__ . "/../../" . $userImageDir;

        if (!is_dir($userImageDirPath)) {
            mkdir($userImageDirPath, 0777, true);
            chmod($userImageDirPath, 0777);
        }
        $dir = scandir($userImageDirPath);


        foreach ($dir as $f) {
            if (is_file($userImageDirPath . $f)) {
                $fi = array();
                $fi['path'] = Config::get('relativeUrl') . $userImageDir . $f;
                $fi['last_modified'] = date("Y-m-d H:i:s", filemtime($userImageDirPath . $f));
                $filesA[] = $fi; //Config::get('relativeUrl') . $userImageDir . $f;
            }
            if (is_dir($userImageDirPath . $f) && substr($f, 0, 1) != ".") {
                $dirsA[] = $subdir . $f;
            }
        }

        $data['files'] = $this->sortFiles($filesA, 'last_modified');
        $data['dirs'] = $dirsA;
        $data['subdir'] = $subdir;
        $data['fileType'] = $fileType;
        /**
         * TODO
         */
// date("F d Y H:i:s.", filectime($filename));

        echo $this->fetch('selectFile', $data);
    }

    private function uploadFile($fileType, $subdir = "") {


        /**
         * DONE:
         * - restrict upload on jpg, gif and png
         */
        if (isset($_FILES['file-0']) && is_file($_FILES['file-0']['tmp_name'])) {

            if ($fileType == 'image') {
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                if (false === $ext = array_search(
                        $finfo->file($_FILES['file-0']['tmp_name']), array(
                    'jpg' => 'image/jpeg',
                    'png' => 'image/png',
                    'gif' => 'image/gif',
                        ), true
                        )) {
//throw new RuntimeException('Invalid file format.');
                    echo "<div class='error'>" . $this->ts("Invalid file format. Only jpg, png and gif is allowed.") . "</div>";
                    unset($_FILES['file-0']['tmp_name']);
                    return;
                }
            }
            if ($fileType == 'video') {
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                if (false === $ext = array_search(
                        $finfo->file($_FILES['file-0']['tmp_name']), array(
                    'ogv' => 'video/ogg',
                    'mp4' => 'video/mp4',
                        //'mpeg' => 'video/mpeg',
                        ), true
                        )) {
// throw new RuntimeException('Invalid file format.');
                    echo "<div class='error'>" . $this->ts("Invalid file format. Only ogv and mp4 is allowed.") . "</div>";
                    unset($_FILES['file-0']['tmp_name']);
                    return;
                }
            }

            
            $id = Session::get('uid');
            if (substr($subdir, -1, 1) != "/") {
                $subdir .= "/";
            }


            $target_dir = "upload/users/" . $id . "/" . $fileType . "s/" . $subdir; //Session::get('uid');

            $target_path = $target_dir;
            if (!is_dir($target_path)) {

                mkdir($target_path, 0777, true);
                chmod($target_path, 0777);
            }

            $filename = basename($_FILES['file-0']['name']);

            $target_file_path = dirname(__FILE__) . '/../../' . $target_path . $filename;


            if (move_uploaded_file($_FILES['file-0']['tmp_name'], $target_file_path)) {
                chmod($target_file_path, 0777);
                echo "<div class='system'>" . $this->ts("File uploaded successful") . "</div>";
            } else {
                echo "<div class='error'>" . $this->ts("Error uploading Image.") . "</div>";
            }
        }
        return;
    }

    private function sortFiles($files, $key, $sort = "desc") {


        $sorter = array();
        $ret = array();
        reset($files);
        foreach ($files as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $files[$ii];
        }
//$ret = ;
        if ($sort == "desc") {
            $ret = array_reverse($ret);
        }
        return $ret;
    }

    private function createNotificationSettings($userId) {
        if(!$this->tableExists('notifications_settings')){
            // activate notifications
            include 'core/Installer.php';
            $Installer = new Installer();
            $Installer->installModules(array('notifications'));
        }
        
        $rs = $this->select('notifications_settings', '*', "user_id='$userId'");
        if ($rs === FALSE || sizeof($rs) == 0) {
            $fieldsValueArray = array();
            $fieldsValueArray['user_id'] = $userId;
            $this->insert('notifications_settings', $fieldsValueArray);
        }
    }

    private function createUserTableIfNotExists($userId = "") {

        if ($userId == "") {
            
            $userId = Session::get('uid');
        }
        $userTable = "profile_" . $userId;

        if ($this->tableExists($userTable)) {
            return null;
        }

        $this->createNotificationSettings($userId);

        $query = "CREATE TABLE IF NOT EXISTS `" . Config::get('dbPrefix') . $userTable . "` (
                    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    `user_id` BIGINT NOT NULL ,
                    `text` TEXT NOT NULL ,
                    `votes_up` TEXT NOT NULL ,
                    `votes_down` TEXT NOT NULL ,
                    `active` TINYINT(1) NOT NULL ,
                    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                    `visibility` TEXT NOT NULL
                    );";
        if (!$this->query($query)) {

            return false;
        } else {
            $this->createCommentsTableIfNotExists($userId);
            return true;
        }
    }

    private function createCommentsTableIfNotExists($userId) {

        $userCommentsTable = "profile_" . $userId . "_comments";

        if ($this->tableExists($userCommentsTable)) {
            return null;
        }

        $query = "CREATE TABLE IF NOT EXISTS `" . Config::get('dbPrefix') . $userCommentsTable . "` (
                    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    `user_id` BIGINT NOT NULL ,
                    `entry_id` BIGINT NOT NULL ,
                    `text` TEXT NOT NULL ,
                    `votes_up` TEXT NOT NULL ,
                    `votes_down` TEXT NOT NULL ,
                    `active` TINYINT(1) NOT NULL ,
                    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                    `visibility` TEXT NOT NULL
                    );";
        if (!$this->query($query)) {
            return false;
        } else {
            return true;
        }
    }

    private function loadWidget($module) {
        if (!$this->checkSiteRights($module)) {
            return "";
        }
        $moduleArray = explode('/', $module);
        if (isset($moduleArray[1])) {
            $function = $moduleArray[1];
        } else {
            $function = "widget";
        }

        $moduleName = $moduleArray[0];
        $moduleClass = ucfirst($moduleArray[0]);

        include_once Config::get('applicationDirectory') . '/' . $moduleName . '/' . $moduleClass . '.php';
        $modulePath = Config::get('applicationDirectory') . '/' . $moduleName . '/';
        $module = new $moduleClass();
        $module->setActiveModule($moduleName);
        $module->setActiveFunction($function);
        $module->setLanguage($this->getLanguage());
        $module->setTemplatePath($modulePath . 'view/');
        $module->loadLang($modulePath, $this->getLanguage());

        $widget = $module->widget();

        $modulePath = Config::get('applicationDirectory') . '/profilpage/';

        $this->setTemplatePath($modulePath . 'view/');
        $this->setActiveModule('profilpage');
        $this->setActiveFunction('dash');
        $this->loadLang($modulePath, $this->getLanguage());

        return $widget;
    }

    /**
     * DONE: rework 
     */
    private function doInsertComment() {
        
        $text = filter_input(INPUT_POST, 'text');
        $userId = filter_input(INPUT_POST, 'user_id');
        $entryId = filter_input(INPUT_POST, 'entry_id');
// $text = $this->replaceUrls($text);
        $text = addslashes($this->replaceSmilies($this->replaceUrls(stripslashes(nl2br(htmlspecialchars($text))))));
        $data['id'] = $this->insertComment($userId, $entryId, $text);
        $text = stripslashes($text);

        $rs = $this->select('user', 'id,image,nick', "id='" . Session::get('uid') . "'");
        $data['user_id'] = $rs[0]['id'];
        $data['profileUserId'] = $userId;

        if ($rs[0]['image'] == '') {
            $data['user_image'] = "profil.png";
        } else {
            $data['user_image'] = Config::get('relativeUrl') . $rs[0]['image'];
        }


        $data['user_nick'] = $rs[0]['nick'];
        $data['time'] = date('Y-m-d H:i:s'); // 2013-02-06 22:55:41
        $data['text'] = $text;



        $this->view('comment', $data);

// Notification on comownentry
//        $umlautUrl = str_replace("xn--", "ü", Config::get('rootUrl'));
//        $umlautUrl = str_replace("-jva", "", $umlautUrl);

        /**
         * new Notification
         * 
         * send notification to user who created an entry on which someone comments now
         */
        $category = "comownentry";

        $usersRsToNotify = $this->select('user'
                , 'id, email, nick'
                , "active=1 AND id='$userId' AND id!='".Session::get('uid')."'");

        $linkToSource = "profilpage/index/" . $userId . "#entry" . $entryId . "u" . $userId;
//#entry<?php echo $e['id']."u".$e['user_id'];

        $messageParams[1] = Session::get('unick');
        $message = "$1 commented your entry";

        //$this->setNotification($usersRsToNotify, $linkToSource, $category, $message, $messageParams, $tryToSendEmail);
        $n = new Notification();
        $n->setCategory($category);
        $n->setMessage($message);
        $n->setMessageParams($messageParams);
        $n->setSubject("Neuer Kommentar");
        $n->setLink($linkToSource);
                
        $this->setNotification($n, $usersRsToNotify);
        /**
         * next
         * send notification to users who comment on an entry that got commented now
         */
        $category2 = "comoncom";
        $userCommentsTable = "profile_" . $userId . "_comments";
        $rs2 = $this->select($userCommentsTable, 'user_id'
                , "entry_id='$entryId' AND user_id!='".Session::get('uid')."' AND user_id!='$userId'");
        $us = array();
        foreach ($rs2 as $r) {
            $us[] = $r['user_id'];
        }
        if (!empty($us)) {
            $usCsv = implode(',', $us);
            $usersRsToNotify2 = $this->select('user'
                    , 'id, email, nick'
                    , "active=1 AND id IN ($usCsv)");

            $linkToSource = "profilpage/index/" . $userId . "#entry" . $entryId . "u" . $userId;

            $messageParams[1] = Session::get('unick');
            $message2 = "$1 commented an entry you also commented";

            $n = new Notification();
            $n->setCategory($category2);
            $n->setMessage($message2);
            $n->setMessageParams($messageParams);
            $n->setSubject("Neuer Kommentar");
            $n->setLink($linkToSource);

            $this->setNotification($n, $usersRsToNotify2);
        }
    }

    /**
     * DONE: rework
     */
    private function insertComment($userId, $entryId, $text) {
        
        $userCommentsTable = "profile_" . $userId . "_comments";
        if (!$this->tableExists($userCommentsTable)) {
            $this->createCommentsTableIfNotExists($userId);
        }
        $fieldsValueArray = array(
            'entry_id' =>  $entryId ,
            'user_id' =>  Session::get('uid') ,
            'text' =>  $text ,
            'active' => '1'
        );
        return $this->insert($userCommentsTable, $fieldsValueArray);
    }

    private function voteUp() {
        $userId = filter_input(INPUT_GET, 'userId');
        $entryId = filter_input(INPUT_GET, 'voteUp');
        
        $sessionUserId = Session::get('uid');
        $userTable = "profile_" . $userId;
        $rs = $this->select($userTable, "votes_up,votes_down", "id=$entryId");
        $votesUp = $this->addToCsvString($sessionUserId, $rs[0]['votes_up']);
        $votesDown = $this->removeFromCsvString($sessionUserId, $rs[0]['votes_down']);

        $fieldsValueArray = array(
            'votes_down' => $votesDown,
            'votes_up' => $votesUp
        );

        $this->updatePrepare($userTable, $fieldsValueArray, "id=:id", array('id' => $entryId));

        if ($votesUp == '' || $votesUp == ',') {
            $votesUpCount = 0;
        } else {
            $upArr = explode(',', $votesUp);
            $votesUpCount = count($upArr);
        }
        if ($votesDown == '' || $votesDown == ',') {
            $votesDownCount = 0;
        } else {
            $downArr = explode(',', $votesDown);
            $votesDownCount = count($downArr);
        }

        return $votesUpCount . "," . $votesDownCount; //array('votesUpCount' => $votesUpCount, 'votesDownCount' => $votesDownCount); //$votesUpCount . ':' . $votesDownCount;
    }

    private function voteDown() {
        $userId = filter_input(INPUT_GET, 'userId');
        $entryId = filter_input(INPUT_GET, 'voteDown');

        
        $sessionUserId = Session::get('uid');
        $userTable = "profile_" . $userId;
        $rs = $this->select($userTable, "votes_down,votes_up", "id=$entryId");
        $votesDown = $this->addToCsvString($sessionUserId, $rs[0]['votes_down']);
        $votesUp = $this->removeFromCsvString($sessionUserId, $rs[0]['votes_up']);
        $fieldsValueArray = array(
            'votes_down' => $votesDown,
            'votes_up' => $votesUp
        );

        $this->updatePrepare($userTable, $fieldsValueArray, "id=:id", array('id' => $entryId));

        if ($votesUp == '' || $votesUp == ',') {
            $votesUpCount = 0;
        } else {
            $upArr = explode(',', $votesUp);
            $votesUpCount = count($upArr);
        }
        if ($votesDown == '' || $votesDown == ',') {
            $votesDownCount = 0;
        } else {
            $downArr = explode(',', $votesDown);
            $votesDownCount = count($downArr);
        }

        return $votesUpCount . "," . $votesDownCount; //array('votesUpCount' => $votesUpCount, 'votesDownCount' => $votesDownCount); //$votesUpCount . ':' . $votesDownCount;
    }

    /**
     * adds a userId o other String to a csv or empty String
     * user for voting and adding friends
     * @param type $userId
     * @param type $csvString
     * @return type 
     */
    private function addToCsvString($userId, $csvString) {
        if ($csvString == "") {
            $fString = $userId;
        } else {
            $fArr = explode(",", $csvString);
            if (in_array($userId, $fArr)) {
// userId is already in string
                return $csvString;
            }
            array_push($fArr, $userId);

            $fUnique = array_unique($fArr);

            $fString = implode(",", $fUnique);
        }
        return $fString;
    }

    /**
     * removes a user id or other string from a csv String
     * @param type $userId
     * @param type $csvString
     * @return type 
     */
    private function removeFromCsvString($userId, $csvString) {
        if ($csvString == "") {
            $fString = "";
        } else {
            $fArr = explode(",", $csvString);

            foreach ($fArr as $i => $v) {
                if ($v == $userId) {
                    array_splice($fArr, $i, 1);
//return $i;
                }
            }

            $fString = implode(",", $fArr);
        }

        return $fString;
    }

    /**
     * an migration/update script from older 9tw to current
     */
//    public function migrate9tw() {
//        $this->autoCheckRights();
//        $this->migrate9twProfiles();
//        $this->migrate9twUserTable();
//    }

//    private function migrate9twProfiles() {
//
//        $rs = $this->select('user', 'id');
//
//        foreach ($rs as $u) {
//            $table = "profile_{$u['id']}";
//            if ($this->tableExists($table)) {
//                $table = "chat_" . $table;
//
//                $sql = "ALTER TABLE `$table` ENGINE = InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
//                $this->query($sql);
//
//                $sql = "ALTER TABLE `$table` CHANGE `category_id` `user_id` INT( 11 ) NOT NULL ;";
//                $this->query($sql);
//
//                $sql = "UPDATE `$table` SET `user_id` = '{$u['id']}' WHERE 1=1;";
//                $this->query($sql);
//            }
//        }
//    }

//    private function migrate9twUserTable() {
//        $sql = "ALTER TABLE `chat_user` CHANGE `password` `password` VARCHAR( 192 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;";
//        $this->query($sql);
//
//        $sql = "ALTER TABLE `chat_user` ADD `image` VARCHAR( 255 ) NOT NULL , ADD `description` TEXT NOT NULL , ADD `friends` TEXT NOT NULL , ADD `last_online` DATETIME NOT NULL , ADD `wallpaper` VARCHAR( 255 ) NOT NULL, ADD `repeat_wallpaper` tinyint( 1 ) NOT NULL DEFAULT '1';";
//        $this->query($sql);
//
//        $rs = $this->select('profile', '*');
//        foreach ($rs as $u) {
//            $array = explode('.de/', $u['image_path']);
//            $image_relative = $array[1];
//            $fieldsValueArray = array(
//                'image' => "$image_relative",
//                'friends' => $u['friends']
//            );
//            $this->update('user', $fieldsValueArray, "id='{$u['user_id']}'");
//        }
//    }

    public function searchModule($searchString): array {
        if (!$this->checkSiteRights('profilpage')) {
            return array();
        }
        $result = array();

        $ss = explode(' ', $searchString);

        $qs = " (sd.name LIKE ' %$searchString% ' OR sd.title LIKE ' %$searchString% ' OR sd.content LIKE ' %$searchString% ')";

        $n = true;
        foreach ($ss as $s) {
            if ($n) {
                $qs .= " OR ((sd.name LIKE '%$s%' OR sd.title LIKE '%$s%' OR sd.content LIKE '%$s%')";
            } else {
                $qs .= " AND (sd.name LIKE '%$s%' OR sd.title LIKE '%$s%' OR sd.content LIKE '%$s%')";
            }
            $n = false;
        }
        return $result;
    }

}
