<?php

/**
 *  Copyright © tim 17.10.2018
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Userstyle extends AppController {

    public function index() {
        $this->autoCheckRights();

        if (filter_has_var(INPUT_POST, 'updateCss')) {
            $this->updateCss();
            $this->jumpToLast();
        }

        if (filter_has_var(INPUT_POST, 'updateSassVars')) {
            $this->updateSassVars();
            $this->jumpToLast();
        }

        if (filter_has_var(INPUT_POST, 'resetSassVarChanges')) {
            $this->resetSassVarChanges();
            $this->jumpToLast();
        }

        $this->main();
    }

    private function main() {
        $this->includeCodemirror('css');
        $this->includeJs("javascript/component/collabsible.js");
        $data = array();
        $data['userStyleContents'] = $this->getUserStyleContents();


        $sassVariablesFileContent = $this->getSassVariablesFile();


        $data['sassVariables'] = $this->getSassVariablesArray($sassVariablesFileContent);



        $this->view('userstyle', $data);
    }

    private function resetSassVarChanges() {
        $sassVarsFile = __DIR__ . "/../../theme/" . Config::get('siteStyle') . "/css/sassVars/a_" . Config::$useConfig . ".scss";
        if (is_file($sassVarsFile)) {
            unlink($sassVarsFile);
        }
    }

    private function updateSassVars() {
        $sassVarsFile = __DIR__ . "/../../theme/" . Config::get('siteStyle') . "/css/sassVars/a_" . Config::$useConfig . ".scss";
        $string = "";
        foreach ($_POST as $key => $value) {
            if (substr($key, 0, 5) == "svar_") {
                $string .= str_replace('svar_', '', $key) . ":" . trim($value) . ";";
            }
        }
        if (FALSE !== file_put_contents($sassVarsFile, $string)) {
            $this->setSystemMessage($this->ts("changes applied!"));
        } else {
            $this->setSystemMessage($this->ts("could not create:") . $sassVarsFile);
        }
    }

    private function getSassVariablesArray($sassVariablesFileContent) {
        //remove and explode
        $vars = explode(';', $sassVariablesFileContent);
        array_pop($vars);

        $n = 0;
        foreach ($vars as $var) {
            $vars[$n] = explode(':', $var);
            ++$n;
        }

        //print_r($vars);
        return $vars;
    }

    private function getUserStyleContents() {
        $userStyleFilePath = __DIR__ . "/../../theme/" . Config::get('siteStyle') . "/css/user_style";
        //echo $userStyleFilePath;

        if (is_file($userStyleFilePath . ".scss")) {
            return file_get_contents($userStyleFilePath . ".scss");
        }
        if (is_file($userStyleFilePath . ".css")) {
            return file_get_contents($userStyleFilePath . ".css");
        }

        return "* { }";
    }

    private function getSassVariablesFile() {
        $fileContent ="";
        $sassVarsFile = __DIR__ . "/../../theme/" . Config::get('siteStyle') . "/css/sassVars/a.scss";
        if (is_file($sassVarsFile)) {
            $sassArray = array();
            $handle = fopen($sassVarsFile, "r");
            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    // process the line read.
                    if (strpos($line, "$") === 0) {
                        $a = explode(':', $line);
                        $sassArray[$a[0]] = trim(str_replace(";", "", $a[1]));
                    }
                }

                fclose($handle);
            } else {
                // error opening the file.
            }

            //$fileContent = file_get_contents($sassVarsFile);
            $sassVarsFile = __DIR__ . "/../../theme/" . Config::get('siteStyle') . "/css/sassVars/a_" . Config::$useConfig . ".scss";
            if (is_file($sassVarsFile)) {
                $handle = fopen($sassVarsFile, "r");
                if ($handle) {
                    while (($line = fgets($handle)) !== false) {
                        // process the line read.
                        if (strpos($line, "$") === 0) {
                            $a = explode(':', $line);
                            $sassArray[$a[0]] = trim(str_replace(";", "", $a[1]));
                        }
                    }

                    fclose($handle);
                } else {
                    // error opening the file.
                }


                //$fileContent .= file_get_contents($sassVarsFile);
            }
            foreach ($sassArray as $key => $value) {
                $fileContent .= "$key:$value;\n\r";
                //echo $fileContent;
            }
            return $fileContent;
        } else {
            $sassVarsFile = __DIR__ . "/../../theme/" . Config::get('siteStyle') . "/css/a.scss";
            if (is_file($sassVarsFile)) {
                $fileContent = file_get_contents($sassVarsFile);
            }
        }

        $regex = array(
            "`^([\t\s]+)`ism" => '',
            "`^\/\*(.+?)\*\/`ism" => "",
            "`([\n\A;]+)\/\*(.+?)\*\/`ism" => "$1",
            "`([\n\A;\s]+)//(.+?)[\n\r]`ism" => "$1\n",
            "`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism" => "\n"
        );
        $fileContent = preg_replace(array_keys($regex), $regex, $fileContent);
        return $fileContent;
    }

    private function updateCss() {
        $cssText = filter_input(INPUT_POST, 'cssarea');
        $userStyleFilePath = __DIR__ . "/../../theme/" . Config::get('siteStyle') . "/css/user_style";
        //echo $userStyleFilePath;

        if (is_file($userStyleFilePath . ".scss")) {
            file_put_contents($userStyleFilePath . ".scss", $cssText);
        } else if (is_file($userStyleFilePath . ".css")) {
            file_put_contents($userStyleFilePath . ".css", $cssText);
        }
    }

}
