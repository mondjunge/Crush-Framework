<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Edit userstyles' =>
	'Bearbeite CSS Styles'
	,
	'Save customisation'=>
	'Anpassungen am Theme speichern'
	,
	'Reset customisation'=>
	'Änderungen am Theme löschen'
	,
	'changes applied!'=>
	'Änderungen am Theme gespeichert!'
	,
	'could not create:'=>
	'Datei konnte nicht angelegt werden. Sind Schreibrechte vorhanden? '
);