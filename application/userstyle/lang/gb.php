<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Edit userstyles' =>
	'Edit userstyles'
	,
	'Save customisation'=>
	'Save customisation'
	,
	'changes applied!'=>
	'Changes applied!'
	,
	'Reset customisation'=>
	'Reset customisation'
	,
	'could not create:'=>
	'Could not create file:'
);