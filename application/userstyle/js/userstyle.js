/**
 * userstyle
 */

use_package('modules');
modules.userstyle = new function () {
    base.main.ModuleRegistry.registerModule(this);
    //base.main.ModuleRegistry.registerAjaxRefreshModule(this);
    //base.main.ModuleRegistry.registerAjaxStartModule(this);

    this.init = function () {
        //initCodemirror();
        handleColorInputs();
        handleVarChangePreview();
    };

    var initCodemirror = function () {
        CodeMirror.fromTextArea(document.getElementById('cssTextarea'),
                {
                    lineNumbers: true,
                    autoCloseBrackets: true

                });
    };

    var handleColorInputs = function () {

        $('input[type="color"]').change(function () {
            $(this).prev("input").val($(this).val());
        });
        $('input[type="color"]').prev("input").change(function () {
            $(this).next("input").val($(this).val());
        });

    };

    var handleVarChangePreview = function () {
        $('input').change(function () {
            var data = $('form').serialize();
            $.ajax({
                type: "POST",
                url: relativeUrl + "css/userstyle/123_userstyle.css",
                async: true,
                timeout: 5000,
                data: data,
                success: function (css) {

                    if ($("style").length === 0) {
                        $("head").append('<style>' + css + '</style>');
                        console.log("css 1");
                    } else {
                        $("style").text(css);
                        console.log("css 2");
                    }
                    console.log("css", css);
                }
            });
        });
    };

//    this.ajaxRefresh = function(){
//        
//    }
//    
//    this.ajaxStart = function(){
//        
//    }


};