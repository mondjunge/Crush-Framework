<h1><?php echo $this->ts("Edit userstyles");?></h1>
<form class="pure-form pure-form-aligned" method="post">
    
    <div class="sassVariables collabsible">
        <div class="pure-control-group sassVar">
            <label for="siteStyle">$siteStyle</label>
            <input class="pure-input pure-input-1-4" type="text" name="siteStyle" value="<?php echo Config::get('siteStyle'); ?>" disabled/>
            <label for="relativeUrl">$relativeUrl</label>
            <input class="pure-input pure-input-1-4" type="text" name="relativeUrl" value="<?php echo Config::get('relativeUrl'); ?>" disabled/>
        </div>
        <div class="pure-control-group sassVar">
            
        </div>
        <?php foreach($sassVariables as $sassVar): ?>   
            <div class="pure-control-group sassVar">
                <label class="pure-label-extrawidth" for="svar_<?php echo $sassVar[0]; ?>"><?php echo $sassVar[0]; ?></label>
                <?php if(strpos($sassVar[1], '#')!== FALSE): ?>
                    <input class="pure-input pure-input-1-4" type="text" name="svar_<?php echo $sassVar[0]; ?>" value="<?php echo $sassVar[1]; ?>" />
                    <input class="pure-input pure-input-1-4" type="color" name="<?php echo $sassVar[0]; ?>" value="<?php echo $sassVar[1]; ?>" />
                <?php else: ?>
                    <input class="pure-input pure-input-1-2" type="text" name="svar_<?php echo $sassVar[0]; ?>" value="<?php echo $sassVar[1]; ?>" />
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <button name="updateSassVars" type="submit" class="pure-button pure-button-primary"><?php echo $this->ts('Save customisation') ;?></button>
    
    <button name="resetSassVarChanges" type="submit" class="pure-button pure-button-error"><?php echo $this->ts('Reset customisation') ;?></button>
    
    
    
<!--    <textarea id="cssTextarea" name="cssarea" class=""><?php echo $userStyleContents;?></textarea>
    <button name="updateCss" type="submit" class="pure-button pure-button-primary"><?php echo $this->ts('Save customisation') ;?></button>-->
    
    
</form>
