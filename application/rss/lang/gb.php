<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'more...' =>
	'more...'
	,
	'd|m|Y H:i:s'=>
	'd|m|Y H:i:s'
	,
	'Keine aktuellen Dates verfügbar...'=>
	'Keine aktuellen Dates verfügbar...'
	,
	'Insufficient rights!'=>
	'Insufficient rights!'
	,
	'No content available'=>
	'No content available'
	,
	'rssTitle_helpText'=>
	'Title of the RSS feed'
	,
	'rssDescription_helpText'=>
	'Short Description of the content'
	,
	'rssLanguage_helpText'=>
	'Language of the content (ISO 3166)'
	,
	'rssAuthor_helpText'=>
	'Copyright owner of the content'
);