<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'more...' =>
	'weiterlesen...'
	,
	'd|m|Y H:i:s'=>
	'd.m.Y H:i:s'
	,
	'Keine aktuellen Dates verfügbar...'=>
	'Keine aktuellen Termine verfügbar...'
	,
	'Insufficient rights!'=>
	'Ungenügende Rechte!'
	,
	'No content available'=>
	'Kein Inhalt verfügbar'
	,
	'rssTitle_helpText'=>
	'Titel/Name des RSS Feeds'
	,
	'rssDescription_helpText'=>
	'Kurze Beschreibung des Inhalts des RSS Feeds'
	,
	'rssLanguage_helpText'=>
	'Sprache der Feed Inhalte (ISO 3166)'
	,
	'rssAuthor_helpText'=>
	'Copyright Inhaber der Inhalte'
);