<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Rss extends AppController {

    public $standalone = true;
    
    public function __construct(){
        $this->modConf = $this->loadModuleConfiguration('rss');
    }

    public function index() {
        $this->autoCheckRights();
        $this->makeRss();
    }

    private function makeRSS() {
        $xml = new DOMDocument("1.0", "utf-8");
        
        $root = $xml->createElement("rss");
        $root->setAttribute("version", "2.0");
        $xml->appendChild($root);
        
        //<?xml-stylesheet type="text/css" href="http://you.com/rss.css"
//        $css = $xml->createElement("xml-stylesheet");
//        $css->setAttribute("type","text/css");
//        $href = "http://yui.yahooapis.com/pure/0.3.0/pure-min.css";//Config::get('rootUrl').Config::get('relativeUrl')."theme/".Config::get('siteStyle')
//        $css->setAttribute("href",$href);
//        $xml->appendChild($css);
        
        $channel = $xml->createElement("channel");
        $root->appendChild($channel);

        $title = $xml->createElement("title", $this->modConf['rssTitle']);
        $channel->appendChild($title);

        $link = $xml->createElement("link", "http://".$_SERVER['HTTP_HOST'].Config::get('reLangUrl')."blog");
        $channel->appendChild($link);

        $description = $xml->createElement("description", $this->modConf['rssDescription']);
        $channel->appendChild($description);

        $language = $xml->createElement("language", $this->modConf['rssLanguage']);
        $channel->appendChild($language);

        $pubDate = $xml->createElement("pubDate", "" . date("D, d M Y G:i:s") . " MEST");
        $channel->appendChild($pubDate);

        $copyright = $xml->createElement("copyright", $this->modConf['rssAuthor']);
        $channel->appendChild($copyright);

        $image = $xml->createElement("image");
        $channel->appendChild($image);


        $url = $xml->createElement("url", Config::get('relativeUrl') . "theme/" . Config::get('siteStyle') . "/images/logoRSS.png");
        $image->appendChild($url);

        $rs = $this->select('blog', '*', "active=1", "date desc", '0,25');

        foreach ($rs as $article) {
            if ($this->checkUserRights($article['role_need'])) {
                $return[] = $article;
            }
        }

        for ($n = 0; $n < count($return); ++$n) {
            $entrys[] = $return[$n];
        }

        foreach ($entrys as $entry) {
            $item = $xml->createElement("item");
            $channel->appendChild($item);
            
            
            $title = $xml->createElement("title", htmlspecialchars($entry['title']) . " - " . htmlspecialchars($entry['tags']));
            $item->appendChild($title);

            $link = $xml->createElement("link", Config::get('rootUrl') .Config::get('relativeUrl') . "de/blog/0/" . $this->encodeTitleForModRewrite($entry['title']));
            $item->appendChild($link);

            $entryText = str_replace('width: 100%','max-width: 100%', $entry['text']);
            $description = $xml->createElement("description", htmlspecialchars($entryText));
           
            $item->appendChild($description);

            $date = new DateTime($entry['date']);
            $entry['date'] = $date->format('Y-m-d H:i:s');

            $pubDate = $xml->createElement("pubDate", $entry['date']);
            $item->appendChild($pubDate);
        }
        
        $xmlDOC = $xml->saveXML();

        Header("Content-Type: text/xml\text/xml\n\n");
        echo $xmlDOC;
        exit;
    }

    /**
     * get some feed as html and give it back to some js function
     * @global type $title
     * @global type $link
     * @global type $content
     * @global type $category
     * @param type $url
     * @return type 
     */
    public function getFeedAsHtml() {
        //$url = "http://lovetechno.blogsport.de/feed/";
        $url = filter_input(INPUT_POST, 'url');
        //$url = "http://localhost/Crush-Framework/de/blog";
        //return file_get_contents($url); exit;
        global $title, $link, $content, $category;
        $this->fetchFeed($url);
        $html = "";
        for ($n = count($content) - 1; $n >= 0; --$n) {
            //for ($n = 0; $n < count($content); ++$n) {

            if ($category[$n] == "dates") {
                //$matches = array();
                preg_match("/([0-9]{2})\.([0-9]{2})\.([0-9]{2})/", $title[$n], $matches);
                //$html .= $matches[0];
                
                //mktime(0, 0, 0, $matches[2], $matches[1], $matches[3]);
                if (mktime(0, 0, 0, $matches[2], $matches[1]+1, $matches[3]) >= time()) {
                    $html .= "<div><span class='feed_title'><a href='{$link[$n]}' target='_blank' >{$title[$n]}</a></span>";
                    $html .= "<span class='feed_content'><a href='{$link[$n]}' target='_blank' >{$content[$n]}</a></span></div>";
                }
            }
        }
        if($html == ""){
            echo $this->ts("No content available");
        }
        echo $html;
        exit;
    }

    private function fetchFeed($url) {
        global $inTag, $item, $title, $link, $content, $category, $insideData;
        // $feedContent = file_get_contents($url);

        $connection = curl_init();
        curl_setopt($connection, CURLOPT_URL, $url);
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($connection, CURLOPT_HEADER, 0);
        $response = curl_exec($connection);


        $parser = xml_parser_create();
        xml_set_object($parser, $this);
        if (!xml_set_element_handler($parser, "foundAnOpeningTag", "foundAClosingTag")) {
            $this->setErrorMessage("could not set element handler");
        }
        xml_set_character_data_handler($parser, "foundSomeText");

        xml_parse($parser, $response);
        xml_parser_free($parser);

        //curl_close($connection);
    }

    function foundAnOpeningTag($parser, $tag, $attributes) {
        global $inTag, $item, $insideData;
        $inTag = $tag;
        switch ($inTag) {
            case 'ITEM':
                $item = true;
                break;
            default:
                break;
        }
        $insideData = false;
    }

    function foundAClosingTag($parser, $tag) {
        global $inTag, $item, $insideData;
        switch ($tag) {
            case 'ITEM':
                $item = false;
                break;
            default:
                break;
        }
        $inTag = '';
        $insideData = false;
    }

    // Function to handle characters within tags.
    function foundSomeText($parser, $characters) {

        global $inTag, $item, $title, $link, $content, $category, $insideData;
        //$characters = str_replace("–", "-", $characters);
        //echo $inTag;
        if ($item) {
            switch ($inTag) {
                case 'TITLE':
                    $characters = str_replace("–", "-", $characters);
                    if ($insideData) {
                        $title[count($title) - 1] .=$characters;
                    } else {
                        $title[] = $characters;
                    }
                    break;
                case 'LINK':
                    if ($insideData) {
                        $link[count($link) - 1] .=$characters;
                    } else {
                        $link[] = $characters;
                    }
                    //$link[] = $characters;
                    break;
                case 'CONTENT:ENCODED':
                    /**
                     * TODO: extract date and only save items with a date equal or gt today
                     */
                    $characters = str_replace("]]>", "", str_replace("<![CDATA[", "", $characters));
                    if ($insideData) {
                        $content[count($content) - 1] .=$characters;
                    } else {
                        $content[] = $characters;
                    }
                    // $content[] = $characters;
                    break;
                case 'CATEGORY':
                    $category[] = $characters;
//                    if($characters=="dates"){
//                        $finalArray[] = array('title'=> $title, 'link' => $link, 'content' => $content);
//                    }
                    break;
                default:
                    break;
            }
            $insideData = true;
        }
    }

}
