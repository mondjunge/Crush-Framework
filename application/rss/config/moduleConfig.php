<?php

$moduleConfig = array(
    'rssTitle' => 'RSS-Feed',
    'rssDescription' => 'Mein privater Blog',
    'rssLanguage' => 'de',
    'rssAuthor' => Config::get('fromName')
);

