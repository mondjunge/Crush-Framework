CREATE TABLE IF NOT EXISTS `{dbprefix}sidebar` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(191) NOT NULL,
  `content` MEDIUMTEXT NOT NULL,
  PRIMARY KEY (`id`), UNIQUE (`name`)
);

INSERT INTO `{dbprefix}sidebar` (`id` ,`name` ,`content`)
VALUES (1 , 'leftSidebar', '<div>leftSidebar</div>');

INSERT INTO `{dbprefix}sidebar` (`id` ,`name` ,`content`)
VALUES (2 , 'rightSidebar', '<div>rigthSidebar</div>'); 

INSERT INTO `{dbprefix}sidebar` (`id` ,`name` ,`content`)
VALUES (3 , 'footer', '<div>footer</div>'); 

