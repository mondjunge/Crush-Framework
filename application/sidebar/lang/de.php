<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'If you can\'t see sidebars or footer, your theme does not support them.' =>
	'Wird Seitenleiste oder Footer nicht angezeigt, so werden diese vom Theme nicht unterstützt.'
	,
	'sidebar left'=>
	'Seitenleiste links'
	,
	'sidebar right'=>
	'Seitenleiste rechts'
	,
	'footer'=>
	'Footer'
	,
	'Update'=>
	'Update'
);