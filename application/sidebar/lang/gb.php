<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'If you can\'t see sidebars or footer, your theme does not support them.' =>
	'If you can\'t see sidebars or footer, your theme does not support them.'
	,
	'sidebar left'=>
	'sidebar left'
	,
	'sidebar right'=>
	'sidebar right'
	,
	'footer'=>
	'footer'
	,
	'Update'=>
	'Update'
);