<form class="pure-form pure-form-stacked" action="<?php echo $this->action('sidebar') ?>" method="post" enctype="multipart/form-data">

    <div>
        <?php echo $this->ts('If you can\'t see sidebars or footer, your theme does not support them.') ?>
    </div>
    <label for="leftSidebar_content" ><?php echo $this->ts('sidebar left') ?></label>
    <div>
        <textarea id="leftSidebar_content" class="text" name="leftSidebar_content" cols="50" rows="10" ><?php echo $sidebars['left']['content'] ?></textarea>
    </div>
    <label for="rightSidebar_content" ><?php echo $this->ts('sidebar right') ?></label>
    <div>
        <textarea id="rightSidebar_content" class="text" name="rightSidebar_content" cols="50" rows="10" ><?php echo $sidebars['right']['content'] ?></textarea>
    </div>
    <label for="footer_content" ><?php echo $this->ts('footer') ?></label>
    <div>
        <textarea id="footer_content" class="text" name="footer_content" cols="50" rows="10" ><?php echo $sidebars['footer']['content'] ?></textarea>
    </div>

    <button id="updateButton" name="update" type="submit" class="pure-button pure-button-primary" ><?php echo $this->ts('Update') ?></button>
</form>