<?php

/**
 *  Copyright © wahrendorff 03.06.2020
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Sidebar extends AppController {

    public function index() {
        $this->autoCheckRights();
        
        if(filter_has_var(INPUT_POST, 'update')){
            $this->updateSidebars();
            
            $this->jumpTo('sidebar');
        }
        
        
        $data = array();
        $data['sidebars'] = array();
        $rs = $this->select('sidebar');
        foreach($rs as $r){
            if($r['name']=='leftSidebar'){
                $data['sidebars']['left'] = $r;
            }
            if($r['name']=='rightSidebar'){
                $data['sidebars']['right'] = $r;
            }
            if($r['name']=='footer'){
                $data['sidebars']['footer'] = $r;
            }
        }
        
        $this->includeCkeditor('javascript/ck_config/ck_default.js');
        $this->view('sidebar',$data);
    }

    private function updateSidebars() {
        $l = filter_input(INPUT_POST, 'leftSidebar_content');
        $r = filter_input(INPUT_POST, 'rightSidebar_content');
        $f = filter_input(INPUT_POST, 'footer_content');
        $fieldInputArray = array(
            'content' => $l
        );
        $this->updatePrepare('sidebar',$fieldInputArray,"name='leftSidebar'");
        $fieldInputArray = array(
            'content' => $r
        );
        $this->updatePrepare('sidebar',$fieldInputArray,"name='rightSidebar'");
        $fieldInputArray = array(
            'content' => $f
        );
        $this->updatePrepare('sidebar',$fieldInputArray,"name='footer'");
    }
}
