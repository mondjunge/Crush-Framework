<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Search results' =>
	'Search results'
	,
	'Please specify your search more concrete.'=>
	'Please specify your search more concrete.'
	,
	'Nothing found. Sorry.'=>
	'Nothing found. Sorry.'
	,
	'Search'=>
	'Search'
	,
	'Search string'=>
	'Search string'
	,
	'search'=>
	'search'
	,
	'Search results for'=>
	'Search results for'
	,
	'New search'=>
	'New search'
);