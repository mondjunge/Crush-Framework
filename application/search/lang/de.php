<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Search results' =>
	'Suchergebnisse'
	,
	'Please specify your search more concrete.'=>
	'Bitte spezifiziere Deine Suche'
	,
	'Nothing found. Sorry.'=>
	'Es wurde nichts passendes gefunden.'
	,
	'Search'=>
	'Suche'
	,
	'Search string'=>
	'Suchbegriff'
	,
	'search'=>
	'suchen'
	,
	'Search results for'=>
	'Suchergebnisse für'
	,
	'New search'=>
	'Neue Suche'
);
