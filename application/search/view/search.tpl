<form class="pure-form pure-form-aligned" action="<?php echo $this->action('search') ?>" method="get">
    <fieldset>
        <legend><?php echo $this->ts("Search") ?></legend>
        <div class="pure-control-group">
            <label for="s" ><?php echo $this->ts("Search string") ?></label>
            <input id="searchString" name="s" type="text" value="<?php if (isset($searchString)) echo $searchString; ?>"/>
            <button type="submit" class="pure-button pure-button-small pure-button-primary" ><?php echo $this->ts("search") ?></button> 
        </div>
    </fieldset>
</form>
<?php if (isset($searchString)): ?>
    <h2><?php echo $this->ts('Search results for') . ' "' . $searchString . '"' ?></h2>
<?php endif;?>
<?php if (count($results)>0) :?>
    
    <?php foreach ($results as $r) : ?>
        <p>
            <b><a href="<?php echo $this->action($r['link']) ?>"><?php echo $r['title'] ?></a></b>
            <br/>
            <?php echo $r['text'] ?>
        </p>
        <br/>

    <?php endforeach; ?>
<?php endif;?>