<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Search extends AppController {

    public function index() {
        $this->autoCheckRights();

        if (filter_has_var(INPUT_GET, 's')) {

            $searchString = strip_tags(filter_input(INPUT_GET, 's',FILTER_SANITIZE_ADD_SLASHES)); // strip_tags(filter_input(INPUT_POST, 'searchString');
            if (strlen($searchString) < 3) {
                $this->setErrorMessage($this->ts('Please specify your search more concrete.'));
                $data['results'] = array();
            } else {
                $data['results'] = $this->searchModules($searchString);
                if (count($data['results']) == 0) {
                    $this->setErrorMessage($this->ts('Nothing found. Sorry.'));
                }
                $data['searchString'] = stripslashes($searchString);
            }
            
        } else {
            $data['results'] = array();
        }
        $this->view('search', $data);
    }

    /**
     * ToDo
     * @param type $searchString 
     * @return array
     */
    private function searchModules($searchString) {

        $directory = Config::get('applicationDirectory');
        $modules = scandir($directory);
        $result = array();
        foreach ($modules as $module) {
            //echo $searchString;
            $modDir = $directory . '/' . $module;
            $modClass = ucfirst($module);
            if (is_dir($modDir) && is_file($modDir . '/' . $modClass . '.php') && substr($module, 0, 1) !== '.') {
                include_once $modDir . '/' . $modClass . '.php';
                $modFunctions = get_class_methods($modClass);

                if (in_array('searchModule', $modFunctions) && $this->isModuleActive($module)) {
                    //echo $module;
                    $m = new $modClass();
                    $this->setTemplatePath($modDir . '/view/');
                    $m->loadLang($modDir . "/", $this->getLanguage());
                    $result = array_merge($result, $m->searchModule($searchString));
                }
            }
        }
        $this->loadLang($directory . '/search/', $this->getLanguage());
        $this->setTemplatePath($directory . '/search/view/');
        return $result;
    }

//    private function searchBlog($searchString) {
//        if (strlen($searchString) < 3) {
//            $this->setErrorMessage($this->ts('Please specify your search more concrete.'));
//            return array();
//        } else {
//            // $searchString = $searchString;
//        }
//        $result = array();
//        // first grade
//        // $rs1 = $this->select('blog', '*', "active=1 AND (text LIKE '%$searchString%' OR title LIKE '%$searchString%')");
//        //second grade
//        $ss = explode(' ', $searchString);
//
//        $qs = " (text LIKE ' %$searchString% ' OR title LIKE ' %$searchString% ')";
//
//        $n = true;
//        foreach ($ss as $s) {
//            if ($n)
//                $qs .= " OR ((text LIKE '%$s%' OR title LIKE '%$s%')";
//            else
//                $qs .= " AND (text LIKE '%$s%' OR title LIKE '%$s%')";
//            $n = false;
//        }
//        $qs .= ')';
//        $rs = $this->select('blog', '*', "active=1 AND ($qs)");
//
//        //$rs = array_merge($rs1, $rs2);
//        //$rs = array_unique($rs);
//
//        foreach ($rs as $r) {
//            if ($this->checkUserRights($r['role_need'])) {
//                $shorttext = substr(strip_tags($r['text']), 0, 500);
//                foreach ($ss as $s) {
//                    $shorttext = str_replace($s, "<b>$s</b>", $shorttext);
//                }
//                $r['title'] = htmlspecialchars_decode($r['title']);
//                $r['text'] = $shorttext;
//                $result[] = $r;
//            }
//        }
//        return $result;
//    }
}
