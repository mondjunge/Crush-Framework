<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Tasks' =>
	'Aufgaben'
	,
	'Title'=>
	'Name'
	,
	'Start'=>
	'Start'
	,
	'interval'=>
	'Wiederholung in Tagen'
	,
	'fixed interval'=>
	'Fixiertes Interval'
	,
	'Add'=>
	'Hinzufügen'
	,
	'due today'=>
	'Heute fällig!'
	,
	'overdue!'=>
	'Längst fällig!!!'
	,
	'due in the next days'=>
	'Demnächst fällig...'
	,
	'Y|m|d'=>
	'd.m.Y'
	,
	'check'=>
	'erledigt'
	,
	'delete'=>
	'entfernen'
	,
	'Do you really want to delete this task?'=>
	'Möchtest Du wirklich diese Aufgabe löschen?'
);