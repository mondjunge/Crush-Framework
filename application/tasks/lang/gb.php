<?php

/**
 *  Language file
 */
$aTranslationStrings = array(
	'Tasks' =>
	'Tasks'
	,
	'Title'=>
	'Title'
	,
	'Start'=>
	'Start'
	,
	'interval'=>
	'interval'
	,
	'fixed interval'=>
	'fixed interval'
	,
	'Add'=>
	'Add'
	,
	'due today'=>
	'due today'
	,
	'overdue!'=>
	'overdue!'
	,
	'due in the next days'=>
	'due in the next days'
	,
	'Y|m|d'=>
	'Y|m|d'
	,
	'check'=>
	'check'
	,
	'delete'=>
	'delete'
	,
	'Do you really want to delete this task?'=>
	'Do you really want to delete this task?'
);