<?php

/**
 *  Copyright © tim 19.10.2014
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Tasks extends AppController {

    public function index() {
        //put your code here
        $this->autoCheckRights();
        $data = array();

        $userTasksTable = 'tasks_' . Session::get('uid');

        if (!$this->tableExists($userTasksTable)) {
            //CREATE USER SPECIFIC NOTIFICATION TABLE
            $query = "CREATE TABLE IF NOT EXISTS `" . $this->getConfig('dbPrefix') . $userTasksTable . "` (
                    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    `title` TEXT NOT NULL ,
                    `next_due_date` datetime NOT NULL ,
                    `due_interval_days` INT(11) NOT NULL ,
                    `fixed_interval` TINYINT(1) NOT NULL DEFAULT 0
                    );";
            $this->query($query);
        }
        
        $rsToday = $this->select($userTasksTable, '*', "next_due_date = '" . date("Y-m-d") . " 00:00:00'");

        $rsOverdue = $this->select($userTasksTable, '*', "next_due_date < '" . date("Y-m-d") . " 00:00:00'", "next_due_date ASC");

        $rsDueFuture = $this->select($userTasksTable, '*', "next_due_date > '" . date("Y-m-d") . " 00:00:00'", "next_due_date ASC");

        $data['rsOverdue']= array();
        foreach ($rsOverdue as $ft) {
            $start = new DateTime($ft['next_due_date']);
            $ft['next_due_date'] = $start->format($this->ts('Y|m|d'));
            $data['rsOverdue'][] = $ft;
        }

        $data['rsToday'] = $rsToday;
        
        $data['rsDueFuture'] = array();
        foreach ($rsDueFuture as $ft) {
            $start = new DateTime($ft['next_due_date']);
            $ft['next_due_date'] = $start->format($this->ts('Y|m|d'));
            $data['rsDueFuture'][] = $ft;
        }

        $this->view('tasks', $data);
    }

    public function admin() {
        //put your administrative code here
        $this->autoCheckRights();
        $userTasksTable = 'tasks_' . Session::get('uid');
        
        if (filter_has_var(INPUT_GET,'deleteTask')) {
            
            $id = filter_input(INPUT_GET, 'deleteTask', FILTER_SANITIZE_NUMBER_INT);
            
            $this->delete($userTasksTable, "id=$id");
        }
        if (filter_has_var(INPUT_GET,'checkTask')) {
            $id = filter_input(INPUT_GET, 'checkTask');
            $taskRs = $this->select($userTasksTable, '*', "id='$id' ");
            $task = $taskRs[0];
            
            $int = $task['due_interval_days'];
            $intObject = new DateInterval("P" . $int . "D");

            if($task['fixed_interval'] == 1){
                $newDueDateObject = new DateTime($task['next_due_date']);
            }else{
                $newDueDateObject = new DateTime(date('Y-m-d'));
            }
            

            $newDueDateObject->add($intObject);

            $newDueDate = $newDueDateObject->format('Y-m-d') . " 00:00:00"; //date('Y-m-d', mktime($hour, $minute, $second, $month, $day, $year));
        
            $fieldsValueArray = array(
                'next_due_date' => "$newDueDate"
            );
            $this->updatePrepare($userTasksTable, $fieldsValueArray, "id=:id", array('id' => $id));
        }


        if (filter_has_var(INPUT_GET,'addTask')) {
            $title = filter_input(INPUT_POST, 'title', FILTER_UNSAFE_RAW);
            $start = filter_input(INPUT_POST, 'start', FILTER_UNSAFE_RAW);
            if($start==""){
                $start = date("Y-m-d") . " 00:00:00";
            }
            $interval = filter_input(INPUT_POST, 'interval');
            if($interval==""){
                $interval=0;
            }
            if (filter_has_var(INPUT_POST,'fixed_interval')) {
                $fixed_interval = 1;
            } else {
                $fixed_interval = 0;
            }
            $fieldsValueArray = array(
                'title' => $title,
                'next_due_date' => $start,
                'due_interval_days' => $interval,
                'fixed_interval' => $fixed_interval
            );
            $this->insert($userTasksTable, $fieldsValueArray);
        }
        
        $this->jumpTo('tasks');
    }

}
