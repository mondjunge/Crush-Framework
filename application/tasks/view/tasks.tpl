<h2><?php echo $this->ts('Tasks'); ?></h2>

<h3><?php if (count($rsOverdue) > 0) echo $this->ts('overdue!'); ?></h3>
<?php foreach ($rsOverdue as $task) : ?>
    <div class="task pure-u-1">
        <strong><?php echo $task['title']; ?> <?php echo $task['next_due_date']; ?> </strong>
        <a href="<?php echo $this->action('tasks/admin?checkTask='.$task['id']);?>" class="pure-button pure-button-primary floatRight"><?php echo $this->ts('check'); ?></a>
        <a href="<?php echo $this->action('tasks/admin?deleteTask='.$task['id']);?>" class="pure-button pure-button-error floatRight" onclick="return utils.check.deleteCheck('<?php echo $this->ts("Do you really want to delete this task?")?>');"><?php echo $this->ts('delete'); ?></a>
    
    </div>
<?php endforeach; ?>

<h4><?php if (count($rsToday) > 0) echo $this->ts('due today') . " " . date($this->ts("Y|m|d")); ?></h4>
<?php foreach ($rsToday as $task) : ?>
    <div class="task">
        <?php echo $task['title']; ?>
        <a href="<?php echo $this->action('tasks/admin?checkTask='.$task['id']);?>" class="pure-button pure-button-primary floatRight"><?php echo $this->ts('check'); ?></a>
        <a href="<?php echo $this->action('tasks/admin?deleteTask='.$task['id']);?>" class="pure-button pure-button-error floatRight" onclick="return utils.check.deleteCheck('<?php echo $this->ts("Do you really want to delete this task?")?>');"><?php echo $this->ts('delete'); ?></a>
    
    </div>
<?php endforeach; ?>

<h4><?php if (count($rsDueFuture) > 0) echo $this->ts('due in the next days'); ?></h4>
<?php foreach ($rsDueFuture as $task) : ?>
    <div class="task">
        <?php echo $task['title']; ?> <?php echo $task['next_due_date']; ?>
        <a href="<?php echo $this->action('tasks/admin?checkTask='.$task['id']);?>" class="pure-button pure-button-primary floatRight"><?php echo $this->ts('check'); ?></a>
        <a href="<?php echo $this->action('tasks/admin?deleteTask='.$task['id']);?>" class="pure-button pure-button-error floatRight" onclick="return utils.check.deleteCheck('<?php echo $this->ts("Do you really want to delete this task?")?>');"><?php echo $this->ts('delete'); ?></a>
    
    </div>
<?php endforeach; ?>



<form class="pure-form pure-form-aligned" name="" action="<?php echo $this->action('tasks/admin?addTask'); ?>" method="post">
    <fieldset>

        <input type="text" size="50" maxlength="255" name="title" placeholder="<?php echo $this->ts('Title'); ?>"/>
        <input type="date" name="start" placeholder="<?php echo $this->ts('Start'); ?>"/>
        <input type="number" name="interval" placeholder="<?php echo $this->ts('interval'); ?>"/>
        <label for="fixed_interval" class="pure-checker" tabindex="0" title="<?php echo $this->ts('fixed interval'); ?>">
            <div class="checker">
                <input id="fixed_interval" class="checkbox" name="fixed_interval" type="checkbox" />
                <div class="check-bg"></div>
                <div class="checkmark">
                    <svg viewBox="0 0 100 100">
                    <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" stroke-width="15" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </div>
            </div>
            <span class="label"><?php echo $this->ts('fixed interval'); ?></span>
        </label>
        <button type="submit" class="pure-button pure-button-primary"><?php echo $this->ts('Add'); ?></button>
    </fieldset>
</form>