#!/bin/bash

Write () { 
	
	DATE=`date +%Y-%m-%d_%H:%M:%S`
	echo "$DATE $1"
}


# create dist/update folders
mkdir -m 0755 ./update
mkdir -m 0755 ./update/system

# copy app folders
mv ./build/application update
cp ./update/application/.htaccess ./update/.htaccess

cd update/application

# find and delete modules without info.xml
APPDIRS="find . -maxdepth 1 -type d"

for d in `$APPDIRS`
do
	if [ "x$d" != "x." ] 
	then 
    		#Write "inspecting $d"
		if grep -q "<module" "$d/info.xml" 2>/dev/null
		then
			Write "adding $d to update packages"
		    	zip -9 -rq $d/latest.zip $d
			
			MODDIR="find $d -maxdepth 1"
			for m in `$MODDIR`
			do
				if [ "x$m" != "x$d" ] && [ "x$m" != "x$d/latest.zip" ] && [ "x$m" != "x$d/info.xml" ]
				then
					rm -r $m
				fi			
			done
		else
			#Write "No info.xml found... removing $d from updates..."
			rm -r $d
		fi
	fi
done

cd ../../


Write "move core system update files to packaging..."

mv build/* update/system
mv build/.[!.]* update/system

# copy core info to root folder
cp -r update/system/core/info.xml update/system/info.xml

Write "packing system update..."
cd update/system
zip -9 -rq latest.zip * .htaccess .user.ini
#Write "packing system update finished"

Write "removing obsolete files..."
SYSDIR="find . -maxdepth 1"
for s in `$SYSDIR`
do
	if [ "x$s" != "x." ] && [ "x$s" != "x./latest.zip" ] && [ "x$s" != "x./info.xml" ]
	then
		rm -r $s
	fi			
done

cd ../../


#Write "finished creating update packages"
