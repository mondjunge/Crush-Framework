<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
$aTranslationStrings = array(
    '404 Page not found' =>
    '404 Seite wurde nicht gefunden'
    ,
    'Debug Information:' =>
    'Debug Ausgabe:'
    ,
    'Configuration:' =>
    'Konfiguration:'
    ,
    'Database: Error while connecting to database.' =>
    'Datenbank: Fehler beim Verbinden zur Datenbank.'
    ,
    'Database: Error while connecting to database. Check your Configuration!' =>
    'Datenbank: Fehler beim Verbinden zur Datenbank. Überprüfe die Konfiguration!'
    ,
    'powered by Crush Framework©' =>
    'powered by Crush Framework©'
    ,
    'Debug Information:' =>
    'Debug Information:'
    ,
    'key not set!' =>
    'Kein Wert vorhanden!'
    ,
    'Install' =>
    'Installation'
    ,
    'Phpinfo' =>
    'Phpinfo'
    ,
    'Webgrind' =>
    'Webgrind'
    ,
    'Test' =>
    'Test'
    ,
    'Login' =>
    'anmelden'
    ,
    'Username' =>
    'Benutzer'
    ,
    'Passphrase' =>
    'Passwort'
    ,
    'stay logged in' =>
    'angemeldet bleiben'
    ,
    'login' =>
    'anmelden'
    ,
    'logged in as:' =>
    'angemeldet als:'
    ,
    'logout' =>
    'Abmelden'
    ,
    'Restricted area menu' =>
    'Seitenadministration'
    ,
    'OMG!!! Your Internet Explorer is so old, it smells rotten in here! <a href="http://www.microsoft.com/ie/">Update now</a> or ask your Administrator to do so, ASAP.' =>
    'Dein Internet Explorer ist so alt, es riecht hier nach Kadaver! Du solltest <a href="http://www.microsoft.com/ie/">JETZT updaten</a> oder Deinem Administrator sagen es schnellstmöglich zu tun.'
    ,
    'Your Internet Explorer is to old to render this site properly. I recommend <a href="http://www.microsoft.com/ie/">an update</a> urgently.' =>
    'Dein Internet Explorer ist zu alt um diese Seite richtig an zu zeigen. Ich empfehle dringend <a href="http://www.microsoft.com/ie/">ein Update</a>.'
    ,
    'There is a newer version of Internet Explorer. <a href="http://www.microsoft.com/ie/">An update</a> would not hurt.' =>
    'Es gibt eine neue Version des Internet Explorers. <a href="http://www.microsoft.com/ie/">Ein Update</a> würde nicht schaden.'
    ,
    'Alternatively you can install <a href="http://www.mozilla-europe.org/">Firefox</a> or <a href="http://www.google.com/chrome/">Chrome</a>. Both browsers update automatically and even run on older machines!' =>
    'Alternativ kannst Du <a href="http://www.mozilla-europe.org/">Firefox</a> oder <a href="http://www.google.com/chrome/">Chrome</a> installieren. Beide Browser halten sich automatisch aktuell und laufen auch auf älteren PC\'s.'
    ,
    'Search' =>
    'Suche'
    ,
    'search' =>
    'suchen'
    ,
    'Search string' =>
    'Suchbegriff'
    ,
    'Connect with us on facebook.' =>
    'Folge uns auf facebook.'
    ,
    'Connect with us on MySpace.' =>
    'Folge uns auf My|___|'
    ,
    'Language not supported. Sorry.' =>
    'Die Sprache wird nicht unterstützt.'
    ,
    'd|m|Y' =>
    'd.m.Y'
    ,
    'RSS Feed' =>
    'RSS Feed'
    ,
    'open' =>
    'Öffnen'
    ,
    'open/close' =>
    'öffnen/schließen'
    ,
    'about' =>
    'über'
    ,
    'Settings' =>
    'Einstellungen'
    ,
    'loading...' =>
    'lade Daten...'
    ,
    'Account' =>
    'Mein Konto'
    ,
    'logged in as ' =>
    'eingeloggt als '
    ,
    'account' =>
    'Konto'
    ,
    'Menu' =>
    'Menü'
    ,
    'Scroll to Top' =>
    'Zum Seitenanfang'
    ,
    '(no title)' =>
    '(kein Titel)'
    ,
    'Module not in DB! Everybody can use it!' =>
    ''
    ,
    'search site' =>
    'durchsuche Seite'
    ,
    'Maintainance Mode. Unless you are an Superadmin, you can not use any functions until maintainance ends. <br/>Please come back later.' =>
    'Wartungsmodus. Das System wird gerade gewartet. Bitte komme zu einem späteren Zeitpunkt wieder.'
    ,
    'language_de' =>
    'Deutsche Sprache'
    ,
    'language_gb' =>
    'English Language'
    ,
    'To login'=>
    'Zur Anmeldung'
    ,
    'Close overlay'=>
    'Overlay schließen'
    ,
    'Insufficient rights!'=>
    'Erforderliche Rechte fehlen.'
    ,
    'accesskeys: n for navigation, c for content'=>
    'Accesskeys: Mit n zur Navigation, mit c zum Inhalt'
    ,
    'open/close navigation'=>
    'Öffne-/schließe Navigationsmenü'
    ,
    'notifications'=>
    'Benachrichtigungen'
    ,
    'open/close user-navigation'=>
    'Öffne-/schließe Account-Navigationsmenü'
    ,
    'cookieWarningText'=>
    '<p>Diese Webseite verwendet Cookies zum Spamschutz, zur dauerhaften Sprachauswahl, zum dauerhaften einloggen und für Statistiksoftware. </p><p>Mit Nutzung dieser Webseite erklären Sie sich damit einverstanden. <a href="/de/site/Impressum">Datenschutzerklärung</a></p>'
    ,
    'cookieWarningButtonText'=>
    'Verstanden!'
    ,
    'Hello $1,'=>
    'Hallo $1,'
    ,
    'click following link to go directly to the source of this notification:'=>
    'Klicke folgenden Link um direkt zur Quelle dieser Benachrichtigung zu gelangen:'
    ,
    'To change your notification settings, go to this page:'=>
    'Um Deine Benachrichtigungseinstellungen zu ändern besuche folgenden Link:'
);