<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
$aTranslationStrings = array(
    '404 Page not found' =>
    '404 Page not found'
    ,
    'Debug Output:' =>
    'Debug Output:'
    ,
    'Configuration:' =>
    'Configuration:'
    ,
    'Database: Error while connecting to database.' =>
    'Database: Error while connecting to database.'
    ,
    'Database: Error while connecting to database. Check your Configuration!' =>
    'Database: Error while connecting to database. Check your Configuration!'
    ,
    'powered by Crush Framework©' =>
    'powered by Crush Framework©'
    ,
    'Debug Information:' =>
    'Debug Information:'
    ,
    'key not set!' =>
    'key not set!'
    ,
    'Install' =>
    'Install'
    ,
    'Phpinfo' =>
    'Phpinfo'
    ,
    'Webgrind' =>
    'Webgrind'
    ,
    'Test' =>
    'Test'
    ,
    'Login' =>
    'login'
    ,
    'Username' =>
    'Username'
    ,
    'Passphrase' =>
    'Passphrase'
    ,
    'stay logged in' =>
    'stay logged in'
    ,
    'login' =>
    'login'
    ,
    'Debug Information:' =>
    'Debug Information:'
    ,
    'logged in as:' =>
    'logged in as:'
    ,
    'Debug Information:' =>
    'Debug Information:'
    ,
    'logout' =>
    'logout'
    ,
    'Debug Information:' =>
    'Debug Information:'
    ,
    'Restricted area menu' =>
    'Restricted area menu'
    ,
    'OMG!!! Your Internet Explorer is so old, it smells rotten in here! Update now or ask your Administrator to do so, ASAP.' =>
    'OMG!!! Your Internet Explorer is so old, it smells rotten in here! Update now or ask your Administrator to do so, ASAP.'
    ,
    'Your Internet Explorer is quite old, I recommend an update.' =>
    'Your Internet Explorer is quite old, I recommend an update.'
    ,
    'There is a newer version of Internet Explorer. An update would not hurt.' =>
    'There is a newer version of Internet Explorer. An update would not hurt.'
    ,
    'Your Internet Explorer is to old to render this site properly. I recommend an update.' =>
    'Your Internet Explorer is to old to render this site properly. I recommend an update.'
    ,
    'OMG!!! Your Internet Explorer is so old, it smells rotten in here! <a href="http://www.microsoft.com/ie/">Update now</a> or ask your Administrator to do so, ASAP.' =>
    'OMG!!! Your Internet Explorer is so old, it smells rotten in here! <a href="http://www.microsoft.com/ie/">Update now</a> or ask your Administrator to do so, ASAP.'
    ,
    'Your Internet Explorer is to old to render this site properly. I recommend <a href="http://www.microsoft.com/ie/">an update</a>.' =>
    'Your Internet Explorer is to old to render this site properly. I recommend <a href="http://www.microsoft.com/ie/">an update</a>.'
    ,
    'There is a newer version of Internet Explorer. <a href="http://www.microsoft.com/ie/">An update</a> would not hurt.' =>
    'There is a newer version of Internet Explorer. <a href="http://www.microsoft.com/ie/">An update</a> would not hurt.'
    ,
    'Alternatively you can install <a href="http://www.mozilla-europe.org/">Firefox</a> or <a href="http://www.google.com/chrome/">Chrome</a>. Both browsers update automatically and even run on older machines!' =>
    'Alternatively you can install <a href="http://www.mozilla-europe.org/">Firefox</a> or <a href="http://www.google.com/chrome/">Chrome</a>. Both browsers update automatically and even run on older machines!'
    ,
    'Your Internet Explorer is to old to render this site properly. I recommend <a href="http://www.microsoft.com/ie/">an update</a> urgently.' =>
    'Your Internet Explorer is to old to render this site properly. I recommend <a href="http://www.microsoft.com/ie/">an update</a> urgently.'
    ,
    'Search' =>
    'Search'
    ,
    'search' =>
    'search'
    ,
    'Search string' =>
    'Search string'
    ,
    'Connect with us on facebook.' =>
    'Connect with us on facebook.'
    ,
    'Connect with us on MySpace.' =>
    'Connect with us on MySpace.'
    ,
    'Language not supported. Sorry.' =>
    'Language not supported. Sorry.'
    ,
    'd|m|Y' =>
    'd|m|Y'
    ,
    'Tag group view' =>
    'Tag group view'
    ,
    'RSS Feed' =>
    'RSS Feed'
    ,
    'open' =>
    'open'
    ,
    'open/close' =>
    'open/close'
    ,
    'about' =>
    'about'
    ,
    'Settings' =>
    'Settings'
    ,
    'loading...' =>
    'loading...'
    ,
    'Account' =>
    'Account'
    ,
    'logged in as ' =>
    'logged in as '
    ,
    'account' =>
    'account'
    ,
    'Menu' =>
    'Menu'
    ,
    'Scroll to Top' =>
    'Scroll to Top'
    ,
    '(no title)' =>
    '(no title)'
    ,
    'Module not in DB! Everybody can use it!' =>
    'Module not in DB! Everybody can use it!'
    ,
    'search site' =>
    'search site'
    ,
    'Maintainance Mode. Unless you are an Superadmin, you can not use any functions until maintainance ends. <br/>Please come back later.' =>
    'Maintainance Mode. This site is getting updated/maintained. Please come back later.'
    ,
    'language_de' =>
    'Deutsche Sprache'
    ,
    'language_gb' =>
    'English Language'
    ,
    'To login'=>
    'To login'
    ,
    'Close overlay'=>
    'Close overlay'
    ,
    'Insufficient rights!'=>
    'Insufficient rights!'
    ,
    'notifications'=>
    'notifications'
    ,
    'admin/user navigation'=>
    'admin/user navigation'
    ,
    'open/close user-navigation'=>
    'open/close user-navigation'
    ,
    'open/close navigation'=>
    'open/close navigation'
    ,
    'cookieWarningText'=>
    '<p>This side uses cookies to remember login, language settings and user analytics.</p><p>By using this service you agree to our cookie use. <a href="/en/site/Impress">Privacy policy</a></p>'
    ,
    'cookieWarningButtonText'=>
    'Acknowledged!'
    ,
    'accesskeys: n for navigation, c for content'=>
    'accesskeys: n for navigation, c for content'
    ,
    'Hello $1,'=>
    'Hello $1,'
    ,
    'click following link to go directly to the source of this notification:'=>
    'click following link to go directly to the source of this notification:'
    ,
    'To change your notification settings, go to this page:'=>
    'To change your notification settings, go to this page:'
);