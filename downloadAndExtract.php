<?php
/**
 *  Copyright © tim.wahrendorff 19.01.2021
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 */
if (is_file("index.php")) {
    // let the script die, when there is an index.php file present.
    die("Already downloaded and extracted...");
}

$crush = new CrushFramework();
try {
    $crush->download();
    $crush->unzip();
    $crush->deleteDownloadedZip();
    header('Location: index.php');
} catch (Exception $ex) {
    die($ex->getMessage());
}

class CrushFramework {

    private $downloadUrl = "https://update.9tw.de/";
    private $zipName = "CrushFramework.zip";
    private $savePath;

    function CrushFramework() {
        $this->savePath = dirname(__FILE__) . "/";
    }

    function download() {
        if (!is_dir($this->savePath)) {
            mkdir($this->savePath, 0777, true);
        }
        $fileCachePath = $this->savePath . $this->zipName;

        if (is_file($fileCachePath)) {
            unlink($fileCachePath);
        }

        $ch = curl_init($this->downloadUrl . $this->zipName);
        if (!is_writable($this->savePath)) {
            throw new Exception("Cannot download Crush-Framework!<br>Reason: Folder ($this->savePath) is not writeable!<br><br>Please make sure this folder is writeable for the web daemon!");
        }
        $fp = fopen($fileCachePath, 'x');

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);

        fclose($fp);
        if (is_readable($fileCachePath)) {
            return true;
        } else {
            return false;
        }
    }

    function unzip() {
        $zip = new ZipArchive;
        $res = $zip->open($this->zipName);
        if ($res === TRUE) {
            $zip->extractTo('.');
            $zip->close();
        } else {
            throw new Exception("error extracting " . $this->zipName);
        }
    }

    function deleteDownloadedZip() {
        if (is_file($this->savePath . $this->zipName)) {
            unlink($this->savePath . $this->zipName);
        }
    }

}
