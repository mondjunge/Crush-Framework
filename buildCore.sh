#!/bin/bash
Write () { 
	
	DATE=`date +%Y-%m-%d_%H:%M:%S`
	echo "$DATE $1"
}

Write "creating config folder and copying dummy config..."
mkdir -m 0777 ./build/config
cp config/config.php build/config/config.php
cp config/.htaccess build/config/.htaccess

Write "copying core folders..."
cp -r core build/core
cp -r javascript build/javascript
cp -r lang build/lang
rm -rf ./build/core/unit

Write "copying extras..."
#mkdir -m 0755 ./build/extras
cp -r extras build/extras
rm -rf ./build/extras/piwik
rm -rf ./build/extras/simpletest-1.1.7

Write "removing demo/example files from extras..."
rm -rf ./build/extras/phpThumb-*/cache
rm -rf ./build/extras/phpThumb-*/demo
rm -rf ./build/extras/phpThumb-*/docs
rm -rf ./build/extras/phpThumb-*/fonts
rm -rf ./build/extras/phpThumb-*/images

rm -rf ./build/extras/fullcalendar-*/demos
rm -rf ./build/extras/scssphp-*/example
rm -rf ./build/extras/codemirror-*/test
rm -rf ./build/extras/codemirror-*/doc

Write "copying updater..."
#mkdir -m 0755 ./build/updater
cp -r updater build/updater

Write "creating and populate theme folder..."
mkdir -m 0755 ./build/theme
cp theme/Theme.php build/theme/Theme.php
cp -r theme/default build/theme/default

Write "removing custom sass configuration..."
find ./build/theme/default/css/sassVars -regex '.*a_.*\.scss' -exec rm {} \+

Write "copying core files..."
cp INSTALL build/INSTALL
cp LICENSE build/LICENSE
cp README build/README
cp robots.txt build/robots.txt
cp .htaccess build/.htaccess
cp .user.ini build/.user.ini
cp index.php build/index.php
cp manifest-json.php build/manifest-json.php
cp serviceworker.js build/serviceworker.js
cp serviceworker.php build/serviceworker.php


