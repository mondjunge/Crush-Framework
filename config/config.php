<?php

/**
 *  Configuration File
 * 
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: Configuration Array
 */

$serverDocumentRoot = ($_SERVER['DOCUMENT_ROOT'] == '') ? $_SERVER['DOCUMENT_ROOT'] : $_SERVER['DOCUMENT_ROOT']; // get document root
$dirName = str_replace("\\","/",dirname(__FILE__)); // fixes path seperator problems under windows
$relativeUrlCalculation = substr(str_replace("/config","",$dirName."/"), strpos(str_replace("/config","",$dirName."/"), basename($serverDocumentRoot))+strlen(basename($serverDocumentRoot))); // magic

//echo '$serverDocumentRoot is: ' . $serverDocumentRoot;
//echo "<br/>";
//echo '$relativeUrlCalculation is: '. $relativeUrlCalculation;
//die;
$aConfig = array(
    /**
     * The name of the theme to use
     */
    'siteStyle' => 'default',
    
    /**
     * Should the navigation render Icons for the navigation links from appIcons theme images folder?
     */
    'showAppIcons' => true,
    
    /**
     * The default language of the application/website
     */
    'defaultLanguage' => 'de',
    /**
     * used languages, language changer is automatically generating links 
     * if more than one language is enabled
     */
    'enabledLanguages' => array('de','gb'), //,'gb'
    /**
     * Database configuration
     */
    'dbType' => 'Mysql', /* database connection type used. mysql and MysqliConnector possible atm. */
    'dbServer' => 'localhost', /* database servers hostname. If you need to change the port, use something like 'localhost:3306' */
    'dbLogin' => 'root', /* username for database access */
    'dbPassword' => '', /* password for the database user */
    'dbName' => 'crushdb', /* name of the database beeing used  */
    'dbPrefix' => 'crush_', /* a prefix for the tables. usefull if more than one programm is using the database.
     * It also is used as prefix for Cookie and Session Variables */
    'dbCollation' => 'utf8mb4_general_ci', /* set Collation utf8mb4_0900_ai_ci is for mysql 8+ use utf8mb4_general_ci for mysql < 8 */
    
     /**
     * Default module to load if requested module can not be found or no module is given
     */
    'defaultController' => 'site',
    'defaultControllerAfterLogin' => 'login',
    'applicationDirectory' => 'application', #the application directory (where the modules are located
    
    /**
     * Which editor should be used for HTML input?
     * summernote or ckeditor (EOL, deprecated)
     */
    'defaultEditor' => 'summernote',
     /**
     * relative Url (folderpath from Document root to index.php file)
     */
    'relativeUrl' => $relativeUrlCalculation, #name of the directory where the index.php file is located if it is not in the root directory. No trailing slashes.
    /* uncoment and change relative path manually if automatic recognition does not work */
    //'relativeUrl' => "/",
    /**
     * boolean - should a user be able to register him-/herself ?
     */
    'registrationOpen' => false,
    /**
     * TODO: Make debug mode a COOKIE flag for superadmins to set in a new to create global System Settings module view.
     */
    'debugMode' => false, #set debug mode to true for debugging while developing. Set to false when in productive use.
    /**
     *  Shall make the site behave like nothing is in there for visitors and normal users, just show a maintainace mode discalaimer and login. 
     *  Only Superadmins can login and do stuff.
     */
    'maintainanceMode' => false,
    /**
     * Update url
     */
    'updateEndpoint' => "https://update.9tw.de/",
    /**
     * experimental AJAX support for fetching sites/modules
     */
    //'AJAXsupport' => false,

    /**
     * E-Mail settings 
     *  - USED for mail_utf8 in AppController.
     */
    'fromName' => 'Crush Info',
    'fromEmail' => 'mail@xxx.de',
    'noticeEmail' => 'admin@xxx.de', /* Admin E-mail for Systemnotifications */
    
    'emailMethod' => 'mail', // mail, sendmail, smtp
    'emailSmtpServer' => '',
    'emailSmtpServerPort' => '',
    'emailSmtpAuth' => false,
    'emailSmtpUser' => '',
    'emailSmtpPassword' => '',
    
    
    /**
     * Manifest.json settings for PWA
     */
    'pwaName' => $_SERVER['HTTP_HOST'],
    'pwaShortName' => $_SERVER['HTTP_HOST'],
    
    'publicPushApplicationKey' => '',
    'privatePushApplicationKey' => ''
        
);
