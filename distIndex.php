<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: update server REST style
 */
if (filter_has_var(INPUT_GET, 'getModuleVersion')) {
    echo Dist::getModuleVersion(filter_input(INPUT_GET, 'getModuleVersion', FILTER_UNSAFE_RAW));
    exit;
}

if (filter_has_var(INPUT_GET, 'getSystemVersion')) {
    Dist::getSystemVersion();
}

if (filter_has_var(INPUT_GET, 'downloadLatestModule')) {
    Dist::downloadLatestModule(filter_input(INPUT_GET, 'downloadLatestModule', FILTER_UNSAFE_RAW));
}

if (filter_has_var(INPUT_GET, 'downloadLatestSystem')) {
    Dist::downloadLatestSystem();
}

if (filter_has_var(INPUT_GET, 'areModUpdatesAvailable')) {
    echo Dist::areModUpdatesAvailable(filter_input(INPUT_GET, 'areModUpdatesAvailable', FILTER_UNSAFE_RAW));
    die();
}

if (filter_has_var(INPUT_GET, 'fetch')) {
    if (filter_input(INPUT_GET, 'fetch') === "downloadAndExtract.php") {
        header("Content-Type: application/octet-stream");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length:" . filesize("downloadAndExtract.php"));
        header("Content-Disposition: attachment; filename=downloadAndExtract.php");
        readfile("downloadAndExtract.php");
        die();
    }
}

echo "<a href='./CrushFramework.zip'>Download latest Crush-Framework</a>";
echo "<br/>";
echo "<a href='?fetch=downloadAndExtract.php'>Download install script</a>";
echo "<br/>";
echo "<a href='https://gitlab.com/mondjunge/Crush-Framework/issues'>Bugtracker</a>";
echo "<br/>";
echo "<a href='https://gitlab.com/mondjunge/Crush-Framework/wikis/home'>Wiki</a>";

class Dist {

    public static function areModUpdatesAvailable($csv) {
        //print_r($csv ."<br/>");
        $updatesAvailableForCsv = "";
        $modules = explode(';', $csv);
        foreach ($modules as $site) {
        //print_r($site);
            $sA = explode(":", $site);
            $m = $sA[0];
            $v = $sA[1];

            if (self::hasNewerVersion($m, $v)) {
                $updatesAvailableForCsv = self::appendCsv($updatesAvailableForCsv, $m);
            }
        }
        return $updatesAvailableForCsv;
    }

    private static function appendCsv($csv, $string) {
        if ($csv === "") {
            $csv = $string . ":" . self::getModuleVersion($string);
        } else {
            $csv .= ";" . $string . ":" . self::getModuleVersion($string);
        }
        return $csv;
    }

    private static function hasNewerVersion($m, $v) {
        $selfVersion = self::getModuleVersion($m);
        $selfVerArray = explode(".", $selfVersion);
        $curVerArray = explode(".", $v);

        for ($i = 0; $i < count($curVerArray); $i++) {
            if ($selfVerArray[$i] > $curVerArray[$i]) {
                return true;
            } else if ($selfVerArray[$i] == $curVerArray[$i]){
                continue;
            } else {
                return false;
            }
        }
        return false;
    }

    public static function getModuleVersion($m) {
        $modulePath = "update/application/$m";
        try {
            if (is_file($modulePath . "/info.xml")) {
                $xml = simplexml_load_file($modulePath . "/info.xml");
            } else {
                //echo $modulePath . "/info.xml does not exist";
                //die;
                //echo "n/a";
                return;
            }
        } catch (Exception $ex) {
            //echo "$ex";
            return;
        }
        if ($xml == null) {
            //echo "n/a";
            return;
        }
        if (str_word_count($xml['version'], 0, ".") < 1) {
            //echo "0." . $xml['version'];
            return;
        }
        return $xml['version'];
    }

    public static function getSystemVersion() {
        $path = "update/system";
        try {
            if (is_file($path . "/info.xml")) {
                $xml = simplexml_load_file($path . "/info.xml");
            } else {
                die;
            }
        } catch (Exception $ex) {
            echo "$ex";
            return;
        }
        if ($xml == null) {
            echo "n/a";
            return;
        }
        if (str_word_count($xml['version'], 0, ".") < 1) {
            
        }
        echo $xml['version'];
        exit;
    }

    public static function downloadLatestModule($m) {
        $filename = "update/application/$m/latest.zip";
        if (!is_file($filename)) {
            echo "n/a";
            exit;
        }
        //header('Location: '.$filename . '');
        // Send Headers
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename="' . $m . '.zip"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');

        // Send Headers: Prevent Caching of File
        header('Cache-Control: private');
        header('Pragma: private');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        echo file_get_contents($filename);
        exit;
    }

    public static function downloadLatestSystem() {
        $filename = "update/system/latest.zip";

        if (!is_file($filename)) {
            echo "n/a";
            exit;
        }
        //header('Location: '.$filename . '');
        // Send Headers
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename="CrushFramework.zip"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');

        // Send Headers: Prevent Caching of File
        header('Cache-Control: private');
        header('Pragma: private');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        echo file_get_contents($filename);
        exit;
    }

}
