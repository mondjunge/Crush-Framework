<?php
if (strlen(session_id()) === 0) {
	session_start();
}
$dirlist = getFileList("./", true);
if(!isset($_SESSION['chat_usid']) || $_SESSION['chat_usid']!==session_id()){
	die('nicht angemeldet');
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>File Tree</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta charset="utf-8" />
</head>
<body>
<table border="1"> <thead> <tr><th>Size</th><th>Name</th><th>Full Path</th><th>Type</th><th>Last Modified</th></tr> </thead> <tbody> 
	<?PHP // output file list as table rows 
	foreach($dirlist as $file) { 
		if($file['size'] < 1024000) continue; 
		echo "<tr>\n";
		if($file['size'] > 102400000){
			echo "<td style='color:#cc0000;'>".formatSizeUnits($file['size'])."</td>\n"; 
		}else{
			echo "<td>".formatSizeUnits($file['size'])."</td>\n"; 
		}
		
		echo "<td><a href=\"{$file['name']}\">",basename($file['name']),"</a></td>\n"; 
		echo "<td>{$file['name']}</td>\n"; 
		echo "<td>{$file['type']}</td>\n"; 
		echo "<td>",date('r', $file['lastmod']),"</td>\n"; 
		echo "</tr>\n"; 
	} 
	?> 
	</tbody> </table>
</body>
</html>

<?php

function getFileList($dir, $recurse=false, $depth=false) { 
	$retval = array(); 
	// add trailing slash if missing 
	if(substr($dir, -1) != "/") $dir .= "/"; 
	// open pointer to directory and read list of files 
	$d = @dir($dir) or die("getFileList: Failed opening directory $dir for reading"); 
	while(false !== ($entry = $d->read())) { 
	// skip hidden files 
		if($entry[0] == "." || $entry[0]=="cache") continue; 
		if(is_dir("$dir$entry")) { 
			$retval[] = array( "name" => "$dir$entry/", "type" => filetype("$dir$entry"), "size" => 0, "lastmod" => filemtime("$dir$entry") ); 
			if($recurse && is_readable("$dir$entry/")) { 
				if($depth === false) { 
					$retval = array_merge($retval, getFileList("$dir$entry/", true)); 
				} 
				elseif($depth > 0) { 
					$retval = array_merge($retval, getFileList("$dir$entry/", true, $depth-1)); 
				} 
			} 
		} 
		elseif(is_readable("$dir$entry")) { 
			$retval[] = array( "name" => "$dir$entry", "type" => mime_content_type("$dir$entry"), "size" => filesize("$dir$entry"), "lastmod" => filemtime("$dir$entry") ); 
		} 
	} 
	$d->close(); 
	return $retval; 
} 

function formatSizeUnits($bytes){
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}