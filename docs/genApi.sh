#!/bin/bash
PHP_PATH=/opt/lampp/bin/php
PROJECT_PATH=$(dirname "$0")

# generate API Docs
$PHP_PATH $PROJECT_PATH/apigen.phar generate --source $PROJECT_PATH/../core --destination $PROJECT_PATH/Apigen --title Crush-Framework --charset UTF-8 --access-levels public --access-levels protected --php --tree

# correct errors in html files afterwards (id's beginning with $ not html conform, remove, clean up anchors).
find $PROJECT_PATH/Apigen -type f -print | xargs sed -i 's/id="\$/id="/g'
find $PROJECT_PATH/Apigen -type f -print | xargs sed -i 's/#\$/#/g'
