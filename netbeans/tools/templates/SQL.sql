CREATE TABLE IF NOT EXISTS `{dbprefix}${name}` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
);