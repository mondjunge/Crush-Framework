<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	 
 * @package Controller
 */

/**
 * Access to all core functionalities
 * 
 * @package Controller
 */
class Controller implements IDatabase, IMessage {

    private $_template;
    private $_database;
    private $_registry;
    // private $_message;
    private $content;
    private $js = array();
    private $css = array();
    private $jsHead = array();
    private $jsSingle = array();
    // private $language;
    public $standalone = false;

    /* #########################################################################
     * Main Controller Functions
     */

    public function __contruct() {
        
    }

    /**
     * Get main config value
     * @param String $key name of the specific key
     * @return String value of the requested key 
     * @depricated use Config::get instead
     */
    public final function getConfig($key) {
        return Config::get($key);
    }

    /**
     * loads a module config file and gives back an array of key value bindings, 
     * based on the keys from the default file, filled with values from the special file.
     * 
     * @param string $modulename (optional) if you want to read a config file from other modules
     * @return array
     */
    public function loadModuleConfiguration($modulename = null) {
        $modConf = array();
        if ($modulename == null) {
            $modulename = $this->getActiveModule();
        }
        $configPath = Config::get('applicationDirectory') . "/" . $modulename . "/config/moduleConfig.php";
        $specialConfigPath = Config::get('applicationDirectory') . "/" . $modulename . "/config/moduleConfig_" . Config::getUseConfig() . ".php";


        if (is_readable($configPath)) {
            include $configPath;
            $modConf = $moduleConfig;
        }

        $moduleConfig = array();
        if (is_readable($specialConfigPath)) {
            include $specialConfigPath;
        }
        foreach ($modConf as $key => $value) {
            Config::addConfigKey($key, $value);
            if (isset($moduleConfig[$key])) {
                Config::addConfigKey($key, $moduleConfig[$key]);
                $modConf[$key] = $moduleConfig[$key];
            }
        }

        return $modConf;
    }

    /**
     * includes js needed for ckeditor
     * @param array of Strings or String $config path to config or additional files, like adaptors
     */
    public final function includeCkeditor($config = NULL) {
        $editor = Config::get('defaultEditor','ckeditor');
        if($editor == 'ckeditor'){
            $this->includeJs('extras/ckeditor-4.22.1/ckeditor.js', 'single');
            if ($config == '' || $config == NULL) {
                return;
            }
            if (is_array($config)) {
                foreach ($config as $c) {
                    $this->includeJs($c, 'single');
                }
            } else {
                $this->includeJs($config, 'single');
            }
        }
        if($editor == 'summernote'){
            $this->includeSummernote();
        }
        
    }
    
    /**
     * includes js needed for summernote
     * @param array of Strings or String $config path to config or additional files, like adaptors
     */
    public final function includeSummernote($config = NULL) {
        $this->includeCodemirror('htmlmixed');
        $this->includeJs('extras/summernote-0.9.1/summernote-lite.min.js', 'body');
        $this->includeJs('javascript/component/summernote-editor.js', 'body');
        if ($config == '' || $config == NULL) {
            return;
        }
        if (is_array($config)) {
            foreach ($config as $c) {
                $this->includeJs($c, 'body');
            }
        } else {
            $this->includeJs($config, 'body');
        }
    }

    /**
     * includes js needed for codemirror
     * @param type $mode
     */
    public final function includeCodemirror($mode = 'css') {
        $this->includeJs('extras/codemirror-5.65.17/lib/codemirror.js');
        $this->includeJs('extras/codemirror-5.65.17/mode/' . $mode . '/' . $mode . ".js");
        $this->includeJs('extras/codemirror-5.65.17/mode/css/css.js');
        $this->includeJs('extras/codemirror-5.65.17/mode/xml/xml.js');
        $this->includeJs('extras/codemirror-5.65.17/addon/edit/closebrackets.js');
        $this->includeJs('extras/codemirror-5.65.17/addon/hint/show-hint.js');
        $this->includeJs('extras/codemirror-5.65.17/addon/hint/css-hint.js');
    }

    /**
     * Load javascript files
     * @param type $path 
     * @param $target - body or head or single
     */
    public final function includeJs($path, $target = "body") {
        switch ($target) {
            case 'body':
                $this->js[] = $path;
                $this->setRegVar('jsIncludesA', $this->js, true);
                break;

            case "head":
                $this->jsHead[] = Config::get("relativeUrl") . $path;
                $this->setRegVar('jsHeadIncludesA', $this->jsHead, true);
                break;
            case "single":
                $this->jsSingle[] = Config::get("relativeUrl") . $path;
                $this->setRegVar('jsSingleIncludesA', $this->jsSingle, true);
                break;
            default :
                // nothing
                break;
        }
    }

    /**
     * Sets the active module name, mainly for the Menu class.
     * @param String $name name of the currently active module
     */
    public final function setActiveModule($name) {
        $this->setRegVar('activeModule', $name, true);
    }

    /**
     * returns the name of the active module
     * @return String name of the currently active module
     */
    public final function getActiveModule() {
        return $this->getRegVar('activeModule');
    }

    /**
     * Sets the active module function name, mainly for the Menu class.
     * @param String $name name for the active module function
     */
    public final function setActiveFunction($name) {
        $this->setRegVar('activeFunction', $name, true);
    }

    /**
     * returns the name of the active function
     * @return String name of the active module function
     */
    public final function getActiveFunction() {
        return $this->getRegVar('activeFunction');
    }

    /**
     * Sets language
     * @param String $lang language code in ISO 3166-1 alpha-2
     */
    public final function setLanguage($lang) {
        $this->setRegVar('language', $lang, true);
    }

    /**
     * Gets language
     * @return String language code in ISO 3166-1 alpha-2
     */
    public final function getLanguage() {
        return $this->getRegVar('language');
    }

    /**
     * @deprecated
     * Loads a helper Class
     * @param String $name name of the helper Class
     * @return Object helper object
     */
    public final function loadHelper($name) {
        /**
         *  the last Object Helper used in code
         */
        require_once 'core/helper/Menu.php';
        return Menu::getInstance();

//        $name = ucfirst($name);
//        require_once 'core/helper/' . $name . '.php';
//        //return call_user_func(array($name, "getInstance"));
//
//        switch ($name) {
//            case 'Link':
//                return Link::getInstance();
//
//                break;
//            case 'Session':
//                return Session::getInstance();
//
//                break;
//            case 'Menu':
//                return Menu::getInstance();
//
//                break;
//            case 'Input':
//                return Input::getInstance();
//
//                break;
//            case 'Cookie':
//                return Cookie::getInstance();
//
//                break;
//            case 'File':
//                return File::getInstance();
//
//                break;
//
//            default:
//                return null;
//                break;
//        }
    }

    /**
     * Returns all the html that is stored from the view function and
     * cleans the content variable
     * @return String html of the processed module
     */
    public final function getContent() {
        $content = $this->content;
        $this->content = '';
        return $content;
    }

    /* #########################################################################
     * Registry functions
     */

    private function getRegistry() {
        return Registry::getInstance();
    }

    /**
     * Sets a Registry value or object
     * @param String $key index of the object or value
     * @param Mixed $value what to store
     * @param Bool $overwrite
     */
    public final function setRegVar($key, $value, $overwrite = false) {
        try {
            $this->getRegistry()->set($key, $value, $overwrite);
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
        }
    }

    /**
     * Get a RegVar
     * @param <type> $key
     * @return <type>
     */
    public final function getRegVar($key) {
        $value = $this->getRegistry()->get($key);
        if ($value == null) {
            $this->errorMessages[] = $key . ': ' . $this->ts('key not set!');
        } else {
            return $value;
        }
    }

    /**
     * check if RegVar is valid
     * @param type $key
     * @return boolean 
     */
    public final function isRegValid($key) {
        return $this->getRegistry()->isValid($key);
    }

    /* #########################################################################
     * Template Functions
     */

    private function loadTemplate() {

        try {
            $this->_template = CachedTemplate::getInstance();
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
        }
    }

    private function getTemplate() {
        if (!$this->_template instanceof CachedTemplate) {
            $this->loadTemplate();
        }
        return $this->_template;
    }

    /**
     * Loads language files of given path
     * @param String $modulePath
     * @param String $lang
     */
    public final function loadLang($modulePath, $lang) {
        $this->getTemplate()->loadLang($modulePath, $lang);
    }

    /**
     * Set path to the templates with trailing slash
     * @param String $path
     */
    public final function setTemplatePath($path) {
        $this->getTemplate()->set_path($path);
    }

    /**
     * fetch files from siteStyle theme directory, but from default, if not available in selected theme
     * @param type $file
     * @param type $data
     * @return type
     */
    public final function fetchThemeFile($file, $data = null) {
        $path = 'theme/' . Config::get('siteStyle') . '/';
        $this->setTemplatePath($path);
        if (!is_file($path . $file . '.tpl')) {
            $path = 'theme/default/';
            $this->setTemplatePath($path);
            if (!is_file($path . $file . '.tpl')) {
                $this->setErrorMessage("template " . $path . $file . " does not exist.");
            }
        }
        return $this->getTemplate()->fetch($file, $data);
    }

    /**
     * Set a single template Variable
     * @param String $name
     * @param String $value
     */
    public final function setVar($name, $value) {
        $this->getTemplate()->set($name, $value);
    }

    /**
     * Sets an array of template data
     * @param String $data
     * @param String $clear set true to wipe out all previous set data, default false
     */
    public final function setData($data, $clear = false) {
        $this->getTemplate()->set_vars($data, $clear);
    }

    /**
     * Translates and sets a template variable
     * @param String $name
     * @param String $value
     * @param Bool $return whether to return the translated String or save it for fetching, default false
     * return void or translated string
     */
    public final function translateVar($name, $value, $return = false) {
        if (!$return) {
            $this->getTemplate()->translate($name, $value, $return);
        } else {
            return $this->getTemplate()->translate($name, $value, $return);
        }
    }

    /**
     * Translates and sets an array of template Data
     * @param Array[] $vars Array of Strings to be translated
     * @param Bool $clear true if you want to wipe out all previous data, default false
     */
    public final function translateData($vars, $clear = false) {
        $this->getTemplate()->translate_vars($vars, $clear);
    }

    /**
     * Open and parses the template file to $this->content to be processed later.
     * @param String $file  string the template file basename (no path, no extension)
     * @param Array[] $data Array full of Variables and Arrays for the view
     * @param Bool $clear true to erase Variables Array
     */
    public final function view($file, $data = null, $clear = false) {
        $this->content .= $this->getTemplate()->fetch($file, $data, $clear);
    }

    /**
     * Bad class, somehow. (Injects HTML in Controller)
     * Anyhow, gives <img Html for an image back (used in messages functionality)
     * @param type $name
     * @param type $class
     * @param type $width
     * @param type $height
     * @param type $title
     * @return type 
     */
    public final function imageHtml($name, $class = '', $width = '', $height = '', $title = '') {
        return $this->getTemplate()->image($name, $class, $width, $height, $title);
    }

    /**
     * Open and parses the template file and returns it to the caller.
     * @param String string the template file basename (no path, no extension)
     * @param array $data data array with variables to fill the template
     * @return String
     */
    public final function fetch($file, $data = null, $clear = false) {
        return $this->getTemplate()->fetch($file, $data, $clear);
    }

    /**
     * Function takes a String, search for a translation in the appropriate Language file and gives back
     * the translated string. If no translation is available, the Key-String will be appended to the
     * current language file if it is not present and a String that indicates to translate it will be given back.
     * @param string $string
     * @param array $param - '$1' gets replaced by $param[1], '$user' by $param['user'], etc.
     * @return string
     */
    public final function ts($string, $params = null) {
        return $this->getTemplate()->ts($string, $params);
    }

    /**
     * checks if the core Systemtable is installed, a configuration File is set and a superadmin exists.
     * @return boolean true if system is installed and functional else false 
     */
    public final function isInstalled() {
        return ((is_file('config/useConfig.php') || is_file('config/hostConfig.php')) && $this->tableExists("site") && $this->tableExists("user") && $this->select("user", "nick", "roles='1' OR roles LIKE '1,%' OR roles LIKE '%,1' OR roles LIKE '%,1,%'") );
    }

    /**
     * Checks if the current request has an session id that is logged in or if there is a cookie allowing to log back in.
     * @return boolean
     */
    public function isLoggedIn() {
        if (!$this->dbExists() || !$this->tableExists('user')) {
            return false;
        }
        /**
         * first check if this function was already called and there is a regvar
         */
        if ($this->isRegValid('isLoggedIn')) {
            return $this->getRegVar('isLoggedIn');
        }

        /**
         * second choice, if session var is not valid, check resident cookies and log in, if possible
         */
        if (!Session::isValid('uid')) {
            if ($this->checkResident()) {
                $this->setRegVar('isLoggedIn', 1, true);
                return true;
            } else {
                $this->setRegVar('isLoggedIn', 0, true);
                return false;
            }
        }

        /**
         * third choice, if session uid is valid, check current session
         */
        $rs = $this->select('user', 'lastsid', "id='" . Session::get('uid') . "'");
        if (sizeof($rs) == 1 && Session::get('usid') === session_id() && $rs[0]['lastsid'] === session_id()) {
            $this->setRegVar('isLoggedIn', 1, true);
            return true;
        } else {
            if ($this->checkResident()) {
                $this->setRegVar('isLoggedIn', 1, true);
                return true;
            } else {
                $this->setRegVar('isLoggedIn', 0, true);
                return false;
            }
        }
    }

    /**
     * check if there is a cookie, able to log the user in
     * @return boolean
     */
    public function checkResident() {

        if (Cookie::isValid('uid') && Cookie::isValid('uhash')) {
            $id = Cookie::get('uid');
            //$nick = Cookie::get('unick');
            $uhash = Cookie::get('uhash');

            if($this->isModuleActive('devices')){
                // check if the device is known.
                // logout if device is not known or deleted
                // fingerprint is based on os, browser and devicetype
                $userAgent = addslashes($_SERVER['HTTP_USER_AGENT']);
                $ua = new UserAgent();
                $rua = $ua->getReadableUserAgent($userAgent);
                $rs = $this->select('user ', '*', "id='$id'");

                $devRs = $this->select('devices', '*', 'user_id=' . $id . " AND fingerprint='" . $rua->fingerprint . "'");

                // Logout (also deleting the resident cookie) 
                // if the device fingerprint does not match any of the know devices
                if ($devRs == FALSE) {
                    $this->logout();
                    return FALSE;
                }
            }
            
            // this is the salted and sha512 hashed passwort, sha512 hashed again.
            if (hash('sha512', $rs[0]['password']) == $uhash) {
                $this->doStuffOnSuccessfulLogin($rs[0]);

                //$this->setProfileInfos($rs[0]['id']);
                return TRUE;
            } else {
                $this->logout();
                return FALSE;
            }
        }
    }

    public function doStuffOnSuccessfulLogin($user) {
        /**
         * set session Variables
         */
        if (is_file($user['wallpaper'])) {
            Session::set('uwallpaper', $user['wallpaper']);
        } else if (is_file("upload/users/" . Session::get('uid') . "/wallpaper.jpg")) {
            Session::set('uwallpaper', "upload/users/" . Session::get('uid') . "/wallpaper.jpg");
        }
        Session::set('urepeat_wallpaper', $user['repeat_wallpaper'], true);

        Session::set('usid', session_id());
        Session::set('uid', $user['id']);
        Session::set('unick', $user['nick']);
        Session::set('uroles', $user['roles']);
        Session::set('uhash', hash('sha512', $user['password']));
        if ($user['image'] == "") {
            Session::set('uimage', "theme/" . Config::get("siteStyle") . "/images/profil.png");
        } else {
            Session::set('uimage', $user['image']);
        }

        if (filter_has_var(INPUT_POST, 'resident')) {
            Cookie::set('unick', $user['nick']);
            Cookie::set('uid', $user['id']);
            Cookie::set('usid', session_id());
            Cookie::set('uhash', hash('sha512', $user['password']));
        }

        $fieldsValueArray = array(
            'lastsid' => session_id(),
            'last_online' => date("Y-m-d H:i:s")
        );
        $this->updatePrepare('user', $fieldsValueArray, "id=:id", array('id' => $user['id']));
        if($this->isModuleActive('devices')){
            $this->updateDevices($user['id']);
        }
        
        $this->setOldUnreadNotificationsAsUnread($user);
    }
    
    /*
     * This is part of a magic trick to notify users only once a period,
     * but restoring all unread notifications since last online time upon login
     */
    private function setOldUnreadNotificationsAsUnread($user){
        $userNotificationTable = 'notifications_' . $user['id'];
        if($this->tableExists($userNotificationTable)){
            $array = array(
                    'read' => 0
                );
            $this->updatePrepare($userNotificationTable, $array, "`time` > :time", array('time' => $user['last_online']));
        }
    }

    /**
     * set fingerprint in db to manage device logins and push subscribtions
     * or just update last login date, if device exists in db.
     * @param type $userId
     */
    private function updateDevices($userId) {

        $userAgent = addslashes($_SERVER['HTTP_USER_AGENT']);
        $ua = new UserAgent();
        $rua = $ua->getReadableUserAgent($userAgent);
        Session::set("currentDeviceFingerprint", $rua->fingerprint);
        
        $rs = $this->select('devices', '*', 'user_id=' . $userId . " AND fingerprint='" . $rua->fingerprint . "'");

        if (sizeof($rs) > 0) {
            // this exact device exists
            // update last Login
            $fieldsValueArray = array(
                'last_login' => date('Y-m-d H:i:s')
            );
            $this->update('devices', $fieldsValueArray, "user_id='{$userId}' AND fingerprint='" . $rua->fingerprint . "'");
        } else {
            // this exact device does not
            $fieldsValueArray = array(
                'user_id' => $userId,
                'useragent' => $userAgent,
                'fingerprint' => $rua->fingerprint
            );
            $this->insert('devices', $fieldsValueArray);
        }
    }

    /**
     * Checks if a module is registered in the sites table and active
     * @param type $module
     * @return boolean
     */
    public function isModuleActive($module) {
        $rs = $this->selectPrepare("site", '*', "module=:module and active='1'", array('module' => $module));
        
        if ($rs != FALSE && count($rs) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function loginNow() {
        // TODO: check input
        if (!filter_has_var(INPUT_POST, 'nick')) {
            return false;
        }
        $nick = filter_input(INPUT_POST, 'nick');
        $pass = filter_input(INPUT_POST, 'pass');
        /**
         * TODO: handle in UI, too.
         */
        if (strlen($pass) > 64) {
            //$this->setErrorMessage("Password to long.");
            return FALSE;
        }
        if ($nick == '') {
            return FALSE;
        }

        $rs = $this->selectPrepare("user", "*", "(nick=:nick OR email=:nick) AND active=1 ORDER BY id asc LIMIT 1", array('nick' => $nick));
        
        if ($rs == FALSE) {
            //$this->setErrorMessage("User or password wrong.");
            return FALSE;
        } else {
            if (count($rs) != 1) {
                //$this->setErrorMessage("User or password wrong.");
                return FALSE;
            }

            if (strlen($rs[0]['password']) == 64) {
                // LEGACY CODE for old logins
                $passSHA = hash('sha256', $pass);
               
            } else {
                $pwdSalt = substr($rs[0]['password'], 0, 64);
                $passSHA = $pwdSalt . hash('sha512', $pwdSalt . $pass);
            }
//             print_r($rs);
//             print_r($passSHA);
            if ($passSHA === $rs[0]['password']) {

                $this->doStuffOnSuccessfulLogin($rs[0]);

                //$lastlogin = date("d.m.Y - G:i");
                //$this->setProfileInfos($rs[0]['id']);
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function updateOnlineTimestamp() {

        $userData = $this->selectPrepare('user', 'last_online', "id=:userId", array('userId'=> Session::get('uid')));

        if ($userData[0]['last_online'] < date("Y-m-d H:i:s", mktime(date("H"), date("i") - 2))) {
            //only update if the timestamp is 2 Minutes old...
            $fieldsValueArray = array(
                'last_online' => date("Y-m-d H:i:s")
                    //'first_ts' => date("Y-d-m H:i:s", $timestamp)
            );
            $this->updatePrepare('user', $fieldsValueArray, "id=:userId", array('userId'=> Session::get('uid')));
        } else {
            //do nothing
        }
    }

    public function logout() {

        if (Cookie::isValid('unick')) {
            Cookie::del('unick');
        }
        if (Cookie::isValid('usid')) {
            Cookie::del('usid');
        }
        if (Cookie::isValid('uid')) {
            Cookie::del('uid');
        }
        if (Cookie::isValid('uroles')) {
            Cookie::del('uroles');
        }
        if (Cookie::isValid('uhash')) {
            Cookie::del('uhash');
        }
        if (Cookie::isValid('language')) {
            Cookie::del('language');
        }

        Session::destroy();

        //Link::jumpTo('login');
    }

    /**
     * May return true if empty String is given as param and the logged in user have no role
     * Returns true, if the logged in User has a role that is given in the csv string
     * Needs the user module
     * @param String $roleNeed csv string with all roles to check agains
     * @return boolean 
     */
    public function checkUserRights($roleNeed, $userId = '') {
        //echo $roleNeed; //exit;
        $rnA = explode(",", $roleNeed);

        // if there is a 0 in $roleNeed, the site is for everyone
        if (in_array("0", $rnA)) {
            return true;
        }
        if ($userId == '') {
            $userRolesA = $this->getUserRoles();
        } else {
            $userRolesA = $this->getUserRoles($userId);
        }

        if (!$userRolesA) {
            return false;
        }
        if (in_array("1", $userRolesA)) {
            return true;
        }
        foreach ($userRolesA as $userRole) {
            //print_r($rnA); echo $userRole; exit;
            if (in_array($userRole, $rnA)) {
                return true;
            }
        }
        return false;
    }

    /**
     * checkes if a given site (modulename[/functionname]) is allowed to be 
     * accessed by the current user. If no module is given
     * Needs the site module.
     * @param String $module
     * @return Boolean 
     */
    public function checkSiteRights($module = '') {
        if ($module == '') {
            if ($this->getActiveFunction() == "index") {
                $site = $this->getActiveModule();
            } else {
                $site = $this->getActiveModule() . "/" . $this->getActiveFunction();
            }

            $rs = $this->select("site", "role_need", "module='{$site}'");
            if ($rs == false) {
                $site = $this->getActiveModule() . "/" . $this->getActiveFunction();
                $rs = $this->select("site", "role_need", "module='$site'");
            }
        } else {
            $rs = $this->select("site", "role_need", "module='$module'");
        }

        if ($rs !== FALSE) {
            if (!isset($rs[0])) {
                return false;
            } else {
                return $this->checkUserRights($rs[0]['role_need']);
            }
        } else {
            return FALSE;
        }
    }

    public function isSiteActive() {
        if ($this->getActiveFunction() == "index") {
            $site = $this->getActiveModule();
        } else {
            $site = $this->getActiveModule() . "/" . $this->getActiveFunction();
        }

        $rs = $this->select("site", "active", "module='{$site}'");
        if ($rs == false) {
            $site = $this->getActiveModule() . "/" . $this->getActiveFunction();
            $rs = $this->select("site", "active", "module='$site'");
        }
        if ($rs !== FALSE) {
            if ($rs[0]['active'] == 1) {
                return true;
            }
            return FALSE;
        }
    }

    /**
     * automatically checks the curretn module/function rights and sends the 
     * user to the login page if  righcheckSiteRightsts are not sufficient
     */
    public function autoCheckRights() {
        //$this->isLoggedIn();
        if (!$this->isSiteActive()) {
            Link::jumpTo('login');
        }
        if (!$this->checkSiteRights()) {

            $this->loadLang('', $this->getLanguage());
            $this->setErrorMessage($this->ts("Insufficient rights!"));
            //$this->loadLang(Config::get('applicationDirectory')."/".$this->getActiveModule()."/", $this->getLanguage());

            Link::jumpTo('login?redirect=' . urlencode(str_replace(Config::get('reLangUrl'), '', $_SERVER['REQUEST_URI'])));
        }
    }

    /**
     * Generic function to check if a Database row is allowed to be 
     * accessed by the current user.
     * @param type $id
     * @param type $table
     * @return type 
     */
    public function checkDbAllowance($id, $table = "site") {
        $rs = $this->select($table, 'role_need', "id=$id");
        return $this->checkUserRights($rs[0]['role_need']);
    }

    /**
     * Gets the roles for the given or currently logged in user
     * @param type $userId
     * @return type 
     */
    private function getUserRoles($userId = '') {

        if ($userId == '') {
            if (!Session::isValid('uid')) {
                return false;
            }

            $id = Session::get('uid');
            if ($id == '' || !$id) {
                return false;
            }
        } else {
            $id = $userId;
        }
        $rs = $this->select("user", "roles", "id='$id'", "id asc", '1');
        if (!$rs) {
            return false;
        }
        $userRolesA = explode(",", $rs[0]['roles']);
        return $userRolesA;
    }

    /**
     * Gets all available Roles in the role table
     * @param int cuts description of the roles down to given number of chars
     * @return Array all roles in an array 
     */
    public function getRoles($cutDescTo = '') {
        //echo "getUserRoles";
        $roles = $this->select('user_role', '*', '1=1', 'id asc');

        if ($cutDescTo != '') {
            foreach ($roles as $role) {
                if (strlen($role['description']) > 3 + $cutDescTo) {
                    $role['description'] = substr($role['description'], 0, $cutDescTo) . "...";
                }
                $return[] = $role;
            }
            return $return;
        } else {
            return $roles;
        }
    }

    private function getMessage(): Message {
        try {
            return Message::getInstance();
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
        }
    }

    private function getDatabase(): Database {
        try {
            return Database::getInstance();
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
        }
    }

    /**
     * Sets an error Message to be shown to the user
     * @param String $string the error message
     */
    public function setErrorMessage($string) {
        $this->getMessage()->setErrorMessage($string);
    }

    /**
     * Sets an system alert (success) Message to be shown to the user
     * @param String $string the success message
     */
    public function setSystemMessage($string) {
        $this->getMessage()->setSystemMessage($string);
    }

    /**
     * Set an debug message to output when developing
     * @param String $string
     */
    public function setDebugMessage($string) {
        $aConfig = Config::getConfig();
        if (isset($aConfig['debugMode']) && $aConfig['debugMode'] == true) {
            $this->getMessage()->setDebugMessage($this->getActiveModule()."/".$this->getActiveFunction().": ".$string);
        }
    }

    /**
     * gets ErrorMessages Array
     * @return Array of Strings
     */
    public function getErrorMessages() {
        return $this->getMessage()->getErrorMessages();
    }

    /**
     * gets SystemMessages Array
     * @return Array of Strings
     */
    public function getSystemMessages() {
        return $this->getMessage()->getSystemMessages();
    }

    /**
     * gets debugMessages Array
     * @return Array of Strings
     */
    public function getDebugMessages() {
        return $this->getMessage()->getDebugMessages();
    }

    /**
     * select data from the database
     * @param String $table
     * @param String $fields
     * @param String $where
     * @param String $orderBy
     * @param String $limit
     * @return Array[r][k] result records as array or errormessage
     */
    public function select($table, $fields = '*', $where = '', $orderBy = '', $limit = '') {
        try {
            return $this->getDatabase()->select($table, $fields, $where, $orderBy, $limit);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
            return false;
        }
    }
    
    /**
     * pdo prepare version of select
     * @param type $table
     * @param type $fields
     * @param type $wherePart
     * @param type $params
     * @return type
     */
    public final function selectPrepare($table, $fields = '*', $wherePart = '', $params = array()) {
        try {
            return $this->getDatabase()->selectPrepare($table, $fields, $wherePart, $params);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
            return false;
        }
         
    }

    public function count($table, $where = '') {
        try {
            return $this->getDatabase()->count($table, $where);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }

    public function dbExists(): bool {
        return $this->getDatabase()->dbExists();
    }

    /**
     * delete data from the database
     * @param String $table
     * @param String $where
     * @return Bool true or false
     */
    public function delete($table, $where): bool {
        try {
            return $this->getDatabase()->delete($table, $where);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }
    
    public function deletePrepare($table, $wherePart, $params = array()): bool {
        try {
            return $this->getDatabase()->deletePrepare($table, $wherePart, $params );
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }
    /**
     * 
     * @param type $string
     * @return String
     * @deprecated since version number
     */
    public function escapeString($string): String {
        try {
            return $this->getDatabase()->escapeString($string);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }

    /**
     * insert database records
     * @param String $table
     * @param Array $fieldsValueArray
     * @return int id of the last inserted record or false on error
     */
    public function insert($table, $fieldsValueArray) {
        try {
            return $this->getDatabase()->insert($table, $fieldsValueArray);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }

    /**
     * send a regular sql query to the database, not recommended to use.
     * @param String $query
     * @param bool $array - returns the resultset as array if set to true
     * @return Mixed resultset or false
     */
    public function query($query, $array = false) {
        try {
            return $this->getDatabase()->query($query, $array);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }
    
    public function prepare($sql, $options = []) {
        try {
            return $this->getDatabase()->prepare($sql, $options);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }

    public function tableExists($table): bool {
        try {
            return $this->getDatabase()->tableExists($table);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }
    /**
     * Updates the given table with the values given in the fieldsValueArray.
     * @param String $table name of the table to update
     * @param Array $fieldsValueArray keys have to be the fieldnames, values are the new values
     * @param String $where a where condition is mandatory. Mostly something like "id=$id" or "1=1"
     * @param String $limit if can give a limit like "1" for only updating the first dataset or "10,25" for datasets 10 to 25
     * @return Bool true on success false on error
     */
    public function update($table, $fieldsValueArray, $where, $limit = ''): bool {
        try {
            return $this->getDatabase()->update($table, $fieldsValueArray, $where, $limit);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }
    
    /**
     * Updates the given table with the values given in the fieldsValueArray.
     * @param String $table name of the table to update
     * @param Array $fieldsValueArray keys have to be the fieldnames, values are the new values
     * @param String $where a where condition is mandatory. Mostly something like "id=$id" or "1=1"
     * @param String $limit if can give a limit like "1" for only updating the first dataset or "10,25" for datasets 10 to 25
     * @return Bool true on success false on error
     */
    public function updatePrepare($table, $fieldsValueArray, $wherePart, $params = array()): bool {
        try {
            return $this->getDatabase()->updatePrepare($table, $fieldsValueArray, $wherePart, $params);
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }

}
