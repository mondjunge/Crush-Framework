<?php

/**
 *  Copyright © tim 01.02.2018
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: 
 * 
 */
/**
 * Compiles scss to css and caches the css as file until the source scss files get changed
 * @package Util
 */
class ScssCompiler {
    
    private static $version = '1.11.0';

    private static $cachePath = __DIR__ . "/../cache/";

    /**
     * Takes an array of paths to scss files, combines them, caches them, gives back the cache file path.
     * 
     * @param array paths of scss files 
     * @return string combined and compiled css cache file path
     */
    public static function compile($input) {
        // $vars = array(); // scss variables to pass
        $scssComplete = "";
        // $scss->setVariables($vars);

        if (empty($input)) {
            return "";
        }

        // handle array of css filepath
        $fileNameHash = hash("md5", implode(",", $input));
        $timeModified = 0;
        foreach ($input as $f) {
            $fileModified = filemtime($f);
            if ($fileModified > $timeModified) {
                $timeModified = $fileModified;
            }
            $scssComplete .= file_get_contents($f);
        }

        try {
            return self::getCssCacheFile($fileNameHash, $timeModified);
        } catch (Exception $ex) {
            // do nothing, just go on...
        }

        self::includeLibs();
        $scss = new \ScssPhp\ScssPhp\Compiler();
        $scss->setVariables(array(
            'relativeUrl' => Config::get('relativeUrl'),
            'siteStyle' => Config::get('siteStyle')
        ));
        try {
            $contents = $scss->compile($scssComplete);
            return self::setCssCacheFile($fileNameHash, $contents);
        } catch (Exception $ex) {
            echo $ex->getMessage()." : Files -> ". implode(",", $input);
        }
    }

    private static function setCssCacheFile($hash, $contents) {
        $cachePath = self::$cachePath . "scss_" . $hash . ".css";
        file_put_contents($cachePath, $contents);
        return $cachePath;
    }

    private static function getCssCacheFile($hash, $filesModified) {
        $cachePath = self::$cachePath . "scss_" . $hash . ".css";
        if (is_readable($cachePath)) {
            $cacheModified = filemtime($cachePath);

            if ($filesModified > $cacheModified) {
                // delete files if they are older than the cache Lifetime.
                unlink($cachePath);
                throw new Exception();
            }

            return $cachePath;
        } else {
            throw new Exception();
        }
    }
    
    private static function includeLibs(){
        include_once 'extras/scssphp-'.self::$version.'/scss.inc.php';
    }

}
