<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 * @package Model
 */

/**
 * Parent for Model classes
 * 
 * @package Model
 */
class Database implements IMessage, IDatabase {

    private static $_instance;
    private $_database;
    private $_cache = array();

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {
        // have to be empty
    }

    /**
     * loads the database INstance
     * @return void
     */
    private function loadDb() {
        $aConfig = Config::getConfig();
        $dbType = ucfirst($aConfig['dbType']);

        try {
            if ($this->_database instanceof $dbType) {
// return $this->_database;
            } else {
                $this->setDebugMessage("Database get Instance");
                $this->_database = call_user_func(array($dbType, "getInstance"));
            }
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
            $this->_database = null;
        }
    }

    /**
     * for the Database function implementation.
     * @return Object Databaseinstance
     */
    private function getDb() {

        $this->loadDb();
        return $this->_database;
    }

    /**
     * function to determine if the configured database exists 
     * @return boolean true if the db exists, false if not
     */
    public function dbExists(): bool {
        try {
            if($this->getDb()==null){
                return false;
            }
            return $this->getDb()->dbExists();
        } catch (Exception $e) {
            //$this->setErrorMessage($e->getMessage());
            return false;
        }
    }

    /**
     * tries to select a table, returns true if possible , false if not.
     * @param type $table
     * @return boolean - true or false
     */
    public function tableExists($table): bool {
        try {
            if($this->getDb()==null){
                return false;
            }
            return $this->getDb()->tableExists($table);
        } catch (Exception $e) {
// $this->setErrorMessage($e->getMessage());
            return false;
        }
    }

    /**
     * Counts the rows in a table
     * @param type $table
     * @param type $where
     * @return int 
     */
    public final function count($table, $where = '') {
        try {
            return $this->getDb()->count($table, $where);
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
            return false;
        }
    }

    /**
     * select data from the database
     * @param String $table
     * @param String $fields
     * @param String $where
     * @param String $orderBy
     * @param String $limit
     * @return Array[r][k] result records as array or errormessage
     */
    public final function select($table, $fields = '*', $where = '', $orderBy = '', $limit = '') {
        /**
         * Request Caching
         */
        $index = $table . ":" . $fields . ":" . $where . ":" . $orderBy . ":" . $limit;
        if (!isset($this->_cache[$index])) {
            $this->_cache[$index] = $this->getDb()->select($table, $fields, $where, $orderBy, $limit);
        }
        return $this->_cache[$index];
    }
    
    /**
     * pdo prepare version of select
     * @param type $table
     * @param type $fields
     * @param type $wherePart
     * @param type $params
     * @return type
     */
    public final function selectPrepare($table, $fields = '*', $wherePart = '', $params = array()) {
         return $this->getDb()->selectPrepare($table, $fields, $wherePart, $params);
    }

    /**
     * delete data from the database
     * @param String $table
     * @param String $where
     * @return Bool true or false
     */
    public final function delete($table, $where): bool {
        try {
            return $this->getDb()->delete($table, $where);
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
            return false;
        }
    }
    
    /**
     * delete data from the database
     * @param String $table
     * @param String $where
     * @param array params for sql prepare
     * @return Bool true or false
     */
    public function deletePrepare($table, $wherePart, $params = array()): bool {
        try {
            return $this->getDb()->deletePrepare($table, $wherePart, $params );
        } catch (Exception $ex) {
            $this->setErrorMessage($ex);
        }
    }

    /**
     * insert database records
     * @param String $table
     * @param Array $fieldsValueArray
     * @return int id of the last inserted record or false on error
     */
    public final function insert($table, $fieldsValueArray) {
        try {
            return $this->getDb()->insert($table, $fieldsValueArray);
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
            return false;
        }
    }

    /**
     * send a regular sql query to the database, not recommended to use.
     * @param String $query
     * @param bool $array - returns the resultset as array if set to true
     * @return Mixed resultset or false
     */
    public final function query($query, $array = false) {
        try {
            return $this->getDb()->query($query, $array);
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
            return false;
        }
    }
    
    public final function prepare($sql, $options = []) {
        try {
            return $this->getDb()->prepare($sql, $options);
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
            return false;
        }
    }

    /**
     * Updates the given table with the values given in the fieldsValueArray.
     * @param String $table name of the table to update
     * @param Array $fieldsValueArray keys have to be the fieldnames, values are the new values
     * @param String $where a where condition is mandatory. Mostly something like "id=$id" or "1=1"
     * @param String $limit if can give a limit like "1" for only updating the first dataset or "10,25" for datasets 10 to 25
     * @return Bool true on success false on error
     */
    public final function update($table, $fieldsValueArray, $where, $limit = ''): bool {
        try {
            return $this->getDb()->update($table, $fieldsValueArray, $where, $limit);
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
            return false;
        }
    }
    
    public function updatePrepare($table, $fieldsValueArray, $wherePart, $whereParams = []): bool {
        try {
            return $this->getDb()->updatePrepare($table, $fieldsValueArray, $wherePart, $whereParams);
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
            return false;
        }
    }

    public final function escapeString($string): string {
        return $this->getDb()->escapeString($string);
    }

    /* #########################################################################
     * Message Functions
     */

//    private function loadMessage() {
//        try {
//            $this->_message = Message::getInstance();
//        } catch (Exception $e) {
////$this->setErrorMessage($e->getMessage());
//        }
//    }

    private function getMessage() {
        try {
            return Message::getInstance();
        } catch (Exception $e) {
            $this->setErrorMessage($e->getMessage());
        }
    }

    /**
     * Sets an error Message to be shown to the user
     * @param String $string the error message
     */
    public function setErrorMessage($string) {
        $this->getMessage()->setErrorMessage($string);
    }

    /**
     * Sets an system alert (success) Message to be shown to the user
     * @param String $string the success message
     */
    public function setSystemMessage($string) {
        $this->getMessage()->setSystemMessage($string);
    }

    /**
     * Set an debug message to output when developing
     * @param String $string
     */
    public function setDebugMessage($string) {
        $aConfig = Config::getConfig();
        if (isset($aConfig['debugMode']) && $aConfig['debugMode'] == true)
            $this->getMessage()->setDebugMessage($string);
    }

    /**
     * gets ErrorMessages Array
     * @return Array of Strings
     */
    public function getErrorMessages() {
        return $this->getMessage()->getErrorMessages();
    }

    /**
     * gets SystemMessages Array
     * @return Array of Strings
     */
    public function getSystemMessages() {
        return $this->getMessage()->getSystemMessages();
    }

    /**
     * gets debugMessages Array
     * @return Array of Strings
     */
    public function getDebugMessages() {
        return $this->getMessage()->getDebugMessages();
    }

}
