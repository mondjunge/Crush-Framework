<?php

/**
 *  Copyright © tim 25.06.2016
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */

/**
 * Installs Modules
 * @package Util
 */
class Installer extends AppController {

    /**
     * Install modules inkl. Dependencies
     * @param array with folder names of module
     */
    public function installModules($modules) {

        $this->installModulesDb($modules);

        if (!$this->insertModulesInSite($modules)) {
            foreach ($modules as $mod) {
                $this->setErrorMessage("<b>$mod</b> " . $this->ts('could not be added'));
            }
        } else {
            $this->additionalModuleInit($modules);
            foreach ($modules as $mod) {
                //$this->setSystemMessage("<b>$mod</b> " . $this->ts('successfully initialized'));
            }
        }
    }

    public function installModulesDb($modules) {
        $sqlFiles = $this->getSqlFiles($modules);
        foreach ($sqlFiles as $sqlFile) {
            if (substr($sqlFile, -4) != '.sql') {
                continue;
            }
            $sql = str_replace('{dbprefix}', Config::get('dbPrefix'), file_get_contents($sqlFile));
            $array = explode(";", $sql);
            foreach ($array as $query) {
                if (trim($query) === '') {
                    
                } else if (!$this->query($query . ";")) {
                    $this->setErrorMessage($query . " : Query cannot be executed!");
                }
            }
        }
    }

    public function installTable($table) {
        $tableA = explode("_", $table);
        $module = $tableA[0];

        $sqlFiles = $this->getSqlFiles($modules);
        foreach ($sqlFiles as $sqlFile) {
            if (substr($sqlFile, -4) != $table . '.sql') {
                continue;
            }
            $sql = str_replace('{dbprefix}', Config::get('dbPrefix'), file_get_contents($sqlFile));
            $array = explode(";", $sql);
            foreach ($array as $query) {
                if (trim($query) === '') {
                    
                } else if (!$this->query($query . ";")) {
                    $this->setErrorMessage($query . " : Query cannot be executed!");
                }
            }
        }
    }

    /**
     *  Returns an array build from modules info.xml
     * @param string $module
     * @return array
     */
    public function getInstallInfo($module) {
        $modDir = Config::get('applicationDirectory') . '/' . $module;
        if (!is_file($modDir . "/info.xml")) {
            return array(); //$return;
        }

        $xml = simplexml_load_file($modDir . "/info.xml") or die("Error: Cannot create object");
        $version = $xml['version'];

        $methods = array();

        foreach ($xml->children() as $method) {
            if ($method->getName() == "dependencies") {
                // install Dependencies
                $dependenciesArray = explode(",", $method);
                foreach ($dependenciesArray as $dep) {
                    $trimDep = trim($dep);
                    if (!$this->isModuleActive($trimDep)) {
                        $this->installModules(array($trimDep));
                    }
                }
            }
            if ($method->getName() == "method") {
                $index = $method['name'];
                $methods["$index"] = array(
                    'module' => $module,
                    'name' => array(
                        'de' => $method->name[0]->de[0],
                        'gb' => $method->name[0]->gb[0]
                    ),
                    'title' => array(
                        'de' => $method->title[0]->de[0],
                        'gb' => $method->title[0]->gb[0]
                    ),
                    'hide' => $method->hide,
                    'active' => $method->active,
                    'version' => $version,
                    'type' => $method->type,
                    'rank' => $method->rank,
                    'role_need' => $method->role_need
                );
            }
        }
        //print_r($methods); exit;
        return $methods;
    }

    private function insertModulesInSite($modules) {

        foreach ($modules as $mod) {
            if ($mod == "") {
                continue;
            }
            $modInfo = $this->getInstallInfo($mod);

            $modDir = Config::get('applicationDirectory') . '/' . $mod;
            $modClass = ucfirst($mod);
            if (!is_file($modDir . '/' . $modClass . '.php')) {
                $this->setErrorMessage($this->ts("module '$1' does not have a class file.", array(1 => $mod)));
                continue;
            }
            include_once $modDir . '/' . $modClass . '.php';
            $controllerFunctionsArray = get_class_methods('AppController');
            $controllerFunctionsArray[] = $modClass;

            $moduleFunctions = array_diff(get_class_methods($modClass), $controllerFunctionsArray);
            // $n = $this->count('site');
            foreach ($moduleFunctions as $mf) {
                if (!isset($modInfo[$mf])) {
                    continue;
                }

                if ($mf == 'index') {
                    $modName = $mod;
                } else {
                    $modName = "$mod/$mf";
                }

                $newId = $this->insertModuleFunction($modName, $modInfo[$mf]);

                /**
                 * set language specific database entries
                 */
                $fail = $this->setLSDE($modName, $modInfo[$mf], $newId);
            }
        }

        // TODO: does not make mcuh sense
        return true;
    }

    /**
     * set language specific database entries
     * @param type $mod
     * @param type $modInfo
     * @param type $newId
     */
    private function setLSDE($mod, $modInfo, $newId) {
        $langs = array('de','gb');//Config::get('enabledLanguages');
        foreach ($langs as $lang) {
            $rs = $this->select('site_data', 'name', "site_id='$newId' AND language='$lang'");
            if (count($rs) > 0) {
                /**
                 *  already there
                 */
                continue;
            }
            $fieldsValueArray = array(
                'title' =>  $modInfo['title'][$lang] ,
                'name' =>  $modInfo['name'][$lang] ,
                'site_id' => $newId,
                'language' => $lang,
                'author' => ''
            );
            $this->insert('site_data', $fieldsValueArray);
//            if (!$this->insert('site_data', $fieldsValueArray)) {
//                $this->setErrorMessage("<b>$mod</b> " . $this->ts('could not be added') . "(" . $lang . ")");
//            } else {
//                $this->setSystemMessage("<b>$mod</b> " . $this->ts('added successfully to sites') . "(" . $lang . ")");
//            }
        }
    }

    private function insertModuleFunction($module, $methodInfo) {
        // check for module
        $rs = $this->select('site', 'id', "unique_name='$module'");
        if (count($rs) > 0) {
            // Module already there! Happens normally, when it got deactivated and activated again
            $newId = $rs[0]['id'];
            //$this->setErrorMessage("<b>$module</b> " . $this->ts('already there') . "(ID: '$newId')");
            return $newId;
        } else {
            // insert module to db
            $fieldsValueArray = array(
                'unique_name' =>  $module ,
                'type' =>  $methodInfo['type'],
                'module' => $module,
                'role_need' => $methodInfo['role_need'] ,
                'active' => $methodInfo['active'] ,
                'hide' => $methodInfo['hide'] ,
                'rank' =>  $methodInfo['rank'] ,
                'version' =>  $methodInfo['version']
            );
            $newId = $this->insert('site', $fieldsValueArray);
        }
        return $newId;
    }

    /**
     * Runs onInstall method for each module given if available
     * @param array $modules
     */
    private function additionalModuleInit($modules) {
        $directory = Config::get('applicationDirectory');

        foreach ($modules as $module) {
            $modDir = $directory . '/' . $module;
            $modClass = ucfirst($module);
            if (is_dir($modDir) && is_file($modDir . '/' . $modClass . '.php') && substr($module, 0, 1) !== '.') {
                include_once $modDir . '/' . $modClass . '.php';
                $modFunctions = get_class_methods($modClass);
                if (in_array('onInstall', $modFunctions)) {
                    $m = new $modClass();
                    $m->onInstall();
                }
            }
        }
    }

    /**
     * 
     * @param array $modules
     */
    public function updateModulesDb($modules) {
        $sqlFiles = $this->getSqlFiles($modules, true);
        foreach ($sqlFiles as $sqlFile) {
            if (substr($sqlFile, -4) != '.sql') {
                continue;
            }
            $sql = str_replace('{dbprefix}', Config::get('dbPrefix'), file_get_contents($sqlFile));
            $updateSql = str_replace('{dbname}', Config::get('dbName'), file_get_contents($sql));
            $array = explode(";", $updateSql);
            foreach ($array as $query) {
                if (trim($query) === '') {
                    
                } else if (!$this->query($query . ";")) {
                    $this->setErrorMessage($query . " : Query cannot be executed!");
                }
            }
        }
    }

    /**
     * 
     * @param array $modules
     * @param boolean $updateFiles
     * @return array
     */
    private function getSqlFiles($modules, $updateFiles = false) {
        $directory = Config::get('applicationDirectory');


        $sqlFiles = array();

        foreach ($modules as $module) {
            if ($updateFiles) {
                $sqlDir = $directory . '/' . $module . '/db/update/';
            } else {
                $sqlDir = $directory . '/' . $module . '/db/';
            }
            
            if (is_dir($sqlDir)) {
                $sqlFiles = array_merge($sqlFiles, $this->getSqlFilesFromDir($sqlDir));
            }
        }
        return $sqlFiles;
    }

    private function getSqlFilesFromDir($dirPath) {
        $sqlFiles = array();
        $sql = scandir($dirPath);
        foreach ($sql as $file) {
            if (substr($file, 0, 1) != '.' && !$this->tableExists(basename($file, '.sql'))) {
                $sqlFiles[] = $dirPath . $file;
            }
        }
        return $sqlFiles;
    }

}
