<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 * @package Controller
 */

/**
 * Custom Application related Controller functions
 *
 * @package Controller
 */
class AppController extends Controller {

    public function setTitle($title) {
        $this->setRegVar('site_title', $title);
    }

    public function getTitle() {
        return $this->getRegVar('site_title');
    }

    public function setName($name) {
        $this->setRegVar('unique_name', $name);
    }

    public function getName() {
        return $this->getRegVar('unique_name');
    }

    public function jumpToLogin() {
        Link::gotoModule('login');
    }

    public function jumpTo($module) {
        Link::gotoModule($module);
    }

    public function jumpToLast() {
        Link::jumpToLast();
    }
    
    public function curlUrlContent($url) {
        // get html via url
        $savePath = dirname(__FILE__) . "/../cache/html/" . md5($url);
        if (!is_dir(dirname(__FILE__) . "/../cache/html/")) {
            mkdir(dirname(__FILE__) . "/../cache/html/", 0755);
        }
        if (is_file($savePath)) {
            //echo "ist schon da!";
            return file_get_contents($savePath);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0"); // some sites block curl user-agents
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
//        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 0);
        $content = curl_exec($ch);
        curl_close($ch);
//        fclose($fp);
//        chmod($savePath, 0644);
        file_put_contents($savePath, $content);

        return $content;
    }

    /**
     * return true of $roleCsv contains a value that is also in $roleNeedCsv or $roleNeedCsv contains 0(zero)
     * @param type $rolesCsv
     * @param type $roleNeedCsv
     * @return boolean
     */
    public function checkRoleNeed($rolesCsv,$roleNeedCsv){
        $rnA = explode(",", $roleNeedCsv);

        // if there is a 0 in $roleNeed, the site is for everyone
        if (in_array("0", $rnA)) {
            return true;
        }
        
        $ra = explode(",", $rolesCsv);
        
        if (is_array($ra)){
            foreach ($ra as $userRole) {
                //print_r($rnA); echo $userRole; exit;
                if (in_array($userRole, $rnA)) {
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * Obfuscates all email adresses to Utf-8 Hex Entities. No matter if in attribute or Textnode.
     * if in an attribute with prefix 'mailto:', the prefix will also get obfuscated.
     * 
     * makes scrapping of e-mail adresses from websites a lot harder for spammers.
     * 
     * Example: For instance, <a href="mailto:someone@example.com">someone@example.com</a> will become
     * <a href="&#x6d;&#x61;&#x69;&#x6c;&#x74;&#x6f;&#x3a;&#x73;&#x6f;&#x6d;&#x65;&#x6f;&#x6e;&#x65;&#x40;&#x65;&#x78;&#x61;&#x6d;&#x70;&#x6c;&#x65;&#x2e;&#x63;&#x6f;&#x6d;">&#x73;&#x6f;&#x6d;&#x65;&#x6f;&#x6e;&#x65;&#x40;&#x65;&#x78;&#x61;&#x6d;&#x70;&#x6c;&#x65;&#x2e;&#x63;&#x6f;&#x6d;</a>
     * in the source code.
     * @param type $content
     * @return type
     */
    public function obfuscateEmail($content) {
        $email_pattern = "/(([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,}))/i";
        $html = preg_replace_callback($email_pattern, array($this, 'encodeEmail'), $content);

        return $html;
    }

    public function encodeEmail($matches) {
        $emailRot = str_rot13($matches[1]);
        return "<a class='emailLink' href='mailto:admin@localhost.localdomain' >" . $this->ts('sendEmail') . "</a><span class='hidden' data-hash='" . base64_encode(strrev($emailRot)) . "' />";
    }

    public function getUsersRsToNotify($sqlAndAddition = null) {
        $usersRsToNotifyWhere = "active=1 AND id!='" . Session::get('uid') . "'";

        if ($sqlAndAddition != null) {
            $usersRsToNotifyWhere .= " AND $sqlAndAddition";
        }

        return $this->select('user'
                        , 'id, email, nick'
                        , $usersRsToNotifyWhere);
    }

    /**
     * setNotification 
     * @param type $usersRsToNotify
     * @param type $linkToSource
     * @param type $category
     * @param type $message
     * @param type $messageParams
     * @param type $tryToSendEmail
     */
    public function setNotification(Notification $notification, $usersRsToNotify = null, $tryToSendEmail = true, $tryToPushNotify = true) {

        if(!$this->isModuleActive("notifications") || !$this->tableExists("notifications_settings")){
            return;
        }
        if ($usersRsToNotify === null) {
            // get all users if none are specified
            $usersRsToNotify = $this->getUsersRsToNotify();
        }

        if ($usersRsToNotify === FALSE) {
            return;
        }

        $mParamsCsv = '';
        if ($notification->getMessageParams() !== null) {
            /**
             * array to String
             */
            foreach ($notification->getMessageParams() as $key => $value) {
                if ($mParamsCsv == '') {
                    $mParamsCsv .= $key . '=>' . $value;
                } else {
                    $mParamsCsv .= ';;;' . $key . '=>' . $value;
                }
            }
        }

        foreach ($usersRsToNotify as $userToNotify) {
            $userNotificationTable = 'notifications_' . $userToNotify['id'];

            if (!$this->tableExists($userNotificationTable)) {
                //CREATE USER SPECIFIC NOTIFICATION TABLE
                $query = "CREATE TABLE IF NOT EXISTS `" . Config::get('dbPrefix') . $userNotificationTable . "` (
                    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    `from_user_id` BIGINT NOT NULL ,
                    `message` TEXT NOT NULL ,
                    `params` TEXT NOT NULL ,
                    `link` TEXT NOT NULL ,
                    `category` TEXT NOT NULL ,
                    `read` TINYINT(1) NOT NULL DEFAULT 0,
                    `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
                    );";
                $this->query($query);
            }
            
            

            //$this->writeToDebugLog(date('n') - 1 ." : userIdToNotify = ".$userToNotify['id']." : unreadCountLastMonth = $unreadCountLastMonth : oneMonthEarlierTimestamp = $oneMonthEarlierTimestamp");
// INSERT new Notification
            $fieldsValueArray = array(
                'from_user_id' =>  Session::get('uid') ,
                'params' =>  $mParamsCsv ,
                'message' =>  $notification->getMessage() ,
                'link' =>  $notification->getLink(),
                'category' =>  $notification->getCategory() 
            );
            $notificationId = $this->insert($userNotificationTable, $fieldsValueArray);
            
            if($tryToSendEmail){
                $this->notifyUserViaEmail($userToNotify, $notificationId, $notification);
            }
            
            if($tryToPushNotify){
                $this->notifyUserViaPush($userToNotify, $notificationId,$notification);
            }
            
        }
    }
    
    private function notifyUserViaPush($userToNotify, $notificationId, Notification $notification) {
        //TODO
    }

    private function notifyUserViaEmail($userToNotify, $notificationId, Notification $notification) {
        $userNotificationTable = 'notifications_' . $userToNotify['id'];
        // EMAil Notifications
        //DONE check if working corectly, i suppose it is not...
        //
        // It should work this way:
        // send notificatins only when:
        // - user has no notifications at all 
        // OR
        // - all users notifications are read 
        // OR
        // - user has unread notifications, the oldest unread notification is older than a week. (a day?!)
        // Mark all old messages as read in the process to not spam users after day 7. 
        //
        //
        //
        // CHECK for unread notifications
        $unread = $this->select($userNotificationTable,"id,time","`read`='0'");
        
//        foreach($unread as $notif){
//            $this->setDebugMessage("notifTime: " . $notif['time']);
//        }

        // TODO: make this superadmin configurable in module configuration.
        $earlierTime = mktime(date('H'), date('i'), date('s'), date('n'), date('j')) - 86400 * 1; //86400 = 1 day in seconds
        
        $oneWeekEarlierTimestamp = date('Y-m-d H:i:s', $earlierTime);
//        $this->setDebugMessage("earlierTimestamp: " . $oneWeekEarlierTimestamp);
//        $this->setDebugMessage("unreadCount: " . count($unread));
        $unreadCountOlderThanOneWeek = 0;
        if (count($unread) > 0) {
            $unreadCountOlderThanOneWeek = $this->select($userNotificationTable,"id,time", "`read`='0' AND `time` < '$oneWeekEarlierTimestamp'");
            $this->setDebugMessage("unreadCountTooOld: " . count($unreadCountOlderThanOneWeek));
            foreach($unreadCountOlderThanOneWeek as $notif){
                $this->setDebugMessage("notifTime: " . $notif['time']);
            }
            if ($unreadCountOlderThanOneWeek > 0) {
                // mark toooOld notifications as read. 
                // DONE:     mark old notifications as unread on login, based on last login           
                
                $array = array(
                    '`read`' => 1
                );
                $this->update($userNotificationTable, $array, "`read`='0'");
                $unreadCount = 0;
            }
        }

        /**
         * DONE:
         * Only send mail if oldest unread notification is older
         * or there is no unread notification
         */
        $message = $notification->getMessage();
        //$messageParams = $notification->getMessageParams();
        $linkToSource = $notification->getLink();
        $category = $notification->getCategory();
        //if ($tryToSendEmail && ($unreadCount == 0 || $unreadCountOlderThanOneMonth > 0)) {
        // TODO:
        if (Config::get('debugMode') || $unreadCount == 0 ) {
            // check if user wants an E-Mail in this case...
            $rs = $this->select('notifications_settings', $category, "user_id='".$userToNotify['id']."'");
            $manualOverrideSituation = false; // Legacy workaround
            if($rs === FALSE || sizeof($rs) == 0){
                //if no settings column exists yet, create it.
                
                $fieldsValueArray = array();
                $fieldsValueArray['user_id'] = Session::get('uid');
                $this->insert('notifications_settings', $fieldsValueArray);
                $manualOverrideSituation = true;
            }
            if (($rs !== FALSE && $rs[0][$category] == '1') || $manualOverrideSituation) {
                
                //TODO: translation of message from module, translation of notifications from system
                
                //$emailMessage = $this->ts($message, $messageParams);
                
                $emailMessage = $message;
                $subject = $notification->getSubject();
                $this->loadLang('', $this->getLanguage());
                $emailMessage = $this->ts("Hello $1,", array(1 => $userToNotify['nick'])) . "<br/>\r\n" . $emailMessage;
                $emailMessage .= "<br/>\r\n";
                $emailMessage .= "<br/>\r\n";
                $emailMessage .= $this->ts("click following link to go directly to the source of this notification:");
                $emailMessage .= "<br/>\r\n";
                $fullLink = Config::get('rootUrl') . Config::get('reLangUrl') . 'notifications/?id=' . $notificationId . '&viewNotificationSrc' . "=" . urlencode($linkToSource);
                $targetLink = Config::get('rootUrl') . Config::get('reLangUrl') . $linkToSource;
                $emailMessage .= "<a href='$fullLink'>$targetLink</a>";
                $emailMessage .= "";
                //$emailMessage .= "<br/>";
                //$emailMessage .= "<br/>";
                //$emailMessage .= "<br/>";
                //$emailMessage .= $this->ts("You only get one notification per month, unless you mark your notifications as read.");
//                $emailMessage .= "<br/>";
//                $emailMessage .= "<br/>";
//                $emailMessage .= $this->ts("To view all your notifications and mark them as read, go to this page:");
//                $emailMessage .= "<br/>";
//                $notificationLink = Config::get('rootUrl') . Config::get('reLangUrl') . "notifications?getNotificationsHTML=1&page=1";
//                $emailMessage .= "<a href='$notificationLink'>$notificationLink</a>";
                $emailMessage .= "<br/>\r\n";
                $emailMessage .= "<br/>\r\n";
                $emailMessage .= "<br/>\r\n";
                $emailMessage .= $this->ts("To change your notification settings, go to this page:");
                $emailMessage .= "<br/>\r\n";
                $notificationSettingsLink = Config::get('rootUrl') . Config::get('reLangUrl') . "notifications/admin";
                $emailMessage .= "<a href='$notificationSettingsLink'>$notificationSettingsLink</a>";
                $this->loadLang(Config::get('applicationDirectory')."/".$this->getActiveModule()."/", $this->getLanguage());

                $this->mail_utf8($userToNotify['email'], $subject, $emailMessage);
            }
        }
    }

    /**
     * Get a Pseudorandomstring
     * @param number $length
     * @return string
     */
    public function generateRandomString($length = 64) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    /**
     * send html utf8 emails
     * @param type $to
     * @param type $subject
     * @param type $message
     * @param type $from_user
     * @param type $from_email
     * @return bool
     */
    public function mail_utf8($to, $subject = '(No subject)', $message = '', $from_user = '', $from_email = '') {
        if ($from_user == '') {
            $from_user = Config::get('fromName');
        }
        if ($from_email == '') {
            $from_email = Config::get('fromEmail');
        }

        //return $this->mail_utf8_multipart($to, $subject, $message, $from_user, $from_email);
        
        return $this->mailPhpMailer($to, $subject, $message, $from_user, $from_email);

        //return $this->mail_utf8_plaintext($to, $subject, $message, $from_user, $from_email);

        //return mail($to, $subject, $htmlMessage, $header);
    }
    
    private function mailPhpMailer($to, $subject = '(No subject)', $message = '', $from_user = '', $from_email = ''){
        $a = Mailer::sendMail($to, $subject, $message, $from_user, $from_email);
        if(true!== $a){
            $this->setErrorMessage($a);
            return false; 
        } else{
            return true; 
        }
    }
    
    private function mail_utf8_multipart($to, $subject = '(No subject)', $message = '', $from_user = '', $from_email = '') {
        if ($from_user == '') {
            $from_user = Config::get('fromName');
        }
        if ($from_email == '') {
            $from_email = Config::get('fromEmail');
        }

        //$message = nl2br($message);
//        $from_user = "=?UTF-8?B?" . base64_encode($from_user) . "?=";
//        $subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";

        $rn = "\r\n";
        $boundary = md5(rand());
        $boundary_content = md5(rand());

        $htmlMessage = '<html>
                <head>
                   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                   <title>' . $subject . '</title>
                </head>
                <body>';

        $htmlMessage .= $message;

        $htmlMessage .= '</body></html>';

        // Message Body
        $msg = $rn . '--' . $boundary . $rn;
        $msg .= "Content-Type: multipart/alternative;" . $rn;
        $msg .= " boundary=\"$boundary_content\"" . $rn;

        //Body Mode text
        $msg .= $rn . "--" . $boundary_content . $rn;
        $msg .= 'Content-Type: text/plain; charset=UTF-8' . $rn;

        $msg .= $rn . strip_tags(str_replace("<br/>", "\n\r", wordwrap($message, 70, "\r\n"))) . $rn;

        //Body Mode Html       
        $msg .= $rn . "--" . $boundary_content . $rn;
        $msg .= 'Content-Type: text/html; charset=UTF-8' . $rn;
        $msg .= 'Content-Transfer-Encoding: quoted-printable' . $rn;

        //equal sign are email special characters. =3D is the = sign
        $msg .= $rn . str_replace("=", "=3D", $htmlMessage) . $rn;
        $msg .= $rn . '--' . $boundary_content . '--' . $rn;
        $msg .= $rn . '--' . $boundary . '--' . $rn;

        /* Baut Header der Mail zusammen */
        $header = array();
        $header[] = "MIME-Version: 1.0";
        $header[] = "Content-type: multipart/related; boundary=" . $boundary;
        $header[] = "Date: " . date("r");
        $header[] = "Message-ID: <" . md5(uniqid(microtime())) . "@" . $_SERVER["SERVER_NAME"] . ">";
        $header[] = "From: " . $from_user . " <" . $from_email . ">";
        $header[] = "Reply-To: <" . $from_email . ">";
        $header[] = "Return-Path: <" . $from_email . ">";
        $header[] = "X-Mailer: PHP/" . phpversion();
        $header[] = "X-Sender-IP: " . $_SERVER["REMOTE_ADDR"]; //$_SERVER["REMOTE_ADDR"];

        /* Verschicken der Mail */
        return mail($to, $subject, $msg, implode("\r\n", $header), "-f " . $from_email);
    }
    
    public function writeToDebugLog($string) {
        $aConfig = Config::getConfig();
        if ($aConfig['debugMode']) {
            if (is_file("debug.log") && filesize("debug.log") > 2048) {
                file_put_contents("debug.log", $string . "\n\r");
            } else {
                file_put_contents("debug.log", $string . "\n\r", FILE_APPEND);
            }
        }
    }

    public function replaceSmilies($string) {

        $icons = $this->getSmilies(true);

        foreach ($icons as $icon => $image) {
            $icon = preg_quote($icon);
            $string = preg_replace("!$icon!", $image, $string);
        }


        return $string;
    }

    /**
     * Returns [smilie][replacement] Array
     * @return array 
     */
    public function getSmilies($replace = false) {
        $themeUrl = Config::get('relativeUrl') . 'theme/' . Config::get('siteStyle');
        $icons = array(
            ':)' => '<img src="' . $themeUrl . '/images/smileys/green/smile.gif" alt=":)" title=":)" class="smilie" />',
            ':(' => '<img src="' . $themeUrl . '/images/smileys/green/sad.gif" alt=":(" title=":("  class="smilie" />',
            ':\'(' => '<img src="' . $themeUrl . '/images/smileys/green/heul.gif" alt=":\'(" title=":\'("  class="smilie" />',
            ';)' => '<img src="' . $themeUrl . '/images/smileys/green/twink.gif" alt=";)" title=";)"  class="smilie" />',
            ':*' => '<img src="' . $themeUrl . '/images/smileys/green/kiss.png" alt=":*" title=":*"  class="smilie" />',
            ':D' => '<img src="' . $themeUrl . '/images/smileys/green/grin.gif" alt=":D" title=":D"  class="smilie" />',
            ':P' => '<img src="' . $themeUrl . '/images/smileys/green/tongue.gif" alt=":P" title=":P"  class="smilie" />',
            ':O' => '<img src="' . $themeUrl . '/images/smileys/green/wtf.gif" alt=":O" title=":O"  class="smilie" />',
            '8|' => '<img src="' . $themeUrl . '/images/smileys/green/omg.gif" alt="8|" title="8|"  class="smilie" />',
            '&lt;||' => '<img src="' . $themeUrl . '/images/smileys/green/joint.gif" alt="&lt;||" title="&lt;||"  class="smilie" />',
            'B)' => '<img src="' . $themeUrl . '/images/smileys/green/cool.gif" alt="B)" title="B)"  class="smilie" />',
//            ':horny:' => '<img src="' . $themeUrl . '/images/smileys/green/love.gif" alt=":horny:" title=":horny:"  class="smilie" />',
//            ':what:' => '<img src="' . $themeUrl . '/images/smileys/green/huuh.gif" alt=":what:" title=":what:"  class="smilie" />',
//            ':coffee:' => '<img src="' . $themeUrl . '/images/smileys/green/coffee.gif" alt=":coffee:" title=":coffee:"  class="smilie" />',
//            ':beer:' => '<img src="' . $themeUrl . '/images/smileys/green/bier.gif" alt=":beer:" title=":beer:"  class="smilie" />',
//            ':wein:' => '<img src="' . $themeUrl . '/images/smileys/green/rotwein.gif" alt=":wein:" title=":wein:"  class="smilie" />',
//            ':smoke:' => '<img src="' . $themeUrl . '/images/smileys/green/smoke.gif" alt=":smoke:" title=":smoke:"  class="smilie" />',
//            ':bye:' => '<img src="' . $themeUrl . '/images/smileys/green/byebye.gif" alt=":bye:" title=":bye:"  class="smilie" />',
//            ':hello:' => '<img src="' . $themeUrl . '/images/smileys/green/hello.gif" alt=":hello:" title=":hello:"  class="smilie" />',
//            ':doh:' => '<img src="' . $themeUrl . '/images/smileys/green/doh.gif" alt=":doh:" title=":doh:"  class="smilie" />',
//            ':zzz:' => '<img src="' . $themeUrl . '/images/smileys/green/sleep.gif" alt=":zzz:" title=":zzz:"  class="smilie" />',
//            ':holy:' => '<img src="' . $themeUrl . '/images/smileys/green/holy.gif" alt=":holy:" title=":holy:"  class="smilie" />',
//            ':evil:' => '<img src="' . $themeUrl . '/images/smileys/green/twisted.gif" alt=":evil:" title=":evil:"  class="smilie" />',
//            ':puke:' => '<img src="' . $themeUrl . '/images/smileys/green/puke.gif" alt=":puke:" title=":puke:"  class="smilie" />',
//            ':devil:' => '<img src="' . $themeUrl . '/images/smileys/green/devil.gif" alt=":devil:" title=":devil:"  class="smilie" />',
//            ':angel:' => '<img src="' . $themeUrl . '/images/smileys/green/engel.gif" alt=":angel:" title=":angel:"  class="smilie" />',
//            ':agree:' => '<img src="' . $themeUrl . '/images/smileys/green/agree.gif" alt=":agree:" title=":agree:"  class="smilie" />',
//            ':disagree:' => '<img src="' . $themeUrl . '/images/smileys/green/disagree.gif" alt=":disagree:" title=":disagree:"  class="smilie" />',
//            ':nonazi:' => '<img src="' . $themeUrl . '/images/smileys/green/nofascho.gif" alt=":nonazi:" title=":nonazi:"  class="smilie" />',
//            ':rofl:' => '<img src="' . $themeUrl . '/images/smileys/green/rofl_1.gif" alt=":rofl:" title=":rofl:" class="smilie" />',
//            ':lol:' => '<img src="' . $themeUrl . '/images/smileys/green/lol.gif" alt=":lol:" title=":lol:"  class="smilie" />',
            ':up:' => '<img src="' . $themeUrl . '/images/smileys/green/thumb.gif" alt=":up:" title=":up:"  class="smilie" />',
            ':down:' => '<img src="' . $themeUrl . '/images/smileys/green/thumbdown.gif" alt=":down:" title=":down:"  class="smilie" />',
//            ':fu:' => '<img src="' . $themeUrl . '/images/smileys/green/fu.gif" alt=":fu:" title=":fu:" class="smilie" />',
//            ':rock:' => '<img src="' . $themeUrl . '/images/smileys/green/rock.gif" alt=":rock:" title=":rock:"  class="smilie" />',
//            ':hack:' => '<img src="' . $themeUrl . '/images/smileys/green/hack.gif" alt=":hack:" title=":hack:"  class="smilie" />',
//            ':blush:' => '<img src="' . $themeUrl . '/images/smileys/green/blush.gif" alt=":blush:" title=":blush:"  class="smilie" />',
//            ':hug:' => '<img src="' . $themeUrl . '/images/smileys/green/hug.gif" alt=":hug:" title=":hug:"  class="smilie" />',
//            ':cuddle:' => '<img src="' . $themeUrl . '/images/smileys/green/cuddle.gif" alt=":cuddle:" title=":cuddle:"  class="smilie" />',
//            ':sex:' => '<img src="' . $themeUrl . '/images/smileys/green/sex.gif" alt=":sex:" title=":sex:"  class="smilie" />',
//            ':hide:' => '<img src="' . $themeUrl . '/images/smileys/green/hide.gif" alt=":hide:" title=":hide:"  class="smilie" />',
            '&lt;3' => '<img src="' . $themeUrl . '/images/smileys/zz_red_heart.png" alt="&lt;3" title="&lt;3" width="20" height="20" class="smilie" />'
        );
        if ($replace) {
            return array_merge($icons, $this->getMoreSmilies());
        } else {
            return array_merge($this->getMoreSmilies(), array('&lt;3' => '<img src="' . $themeUrl . '/images/smileys/zz_red_heart.png" alt="&lt;3" title="&lt;3" width="20" height="20" class="smilie" />'));
        }
    }

    private function getMoreSmilies() {
        $path = __DIR__ . "/../theme/" . Config::get('siteStyle') . "/images/smileys/green/";
        $themeUrl = Config::get('relativeUrl') . 'theme/' . Config::get('siteStyle') . "/images/smileys/green/";
        $ss = $this->fetchFiles($path);
        //print_r($ss);

        $r = array();
        foreach ($ss as $s) {
            $sn = ":" . str_replace(substr($s, -4, 4), '', $s) . ":";
            $r[$sn] = "<img src='" . $themeUrl . $s . "' alt='$sn' title='$sn' class='smilie' />";
        }

        return $r;
    }

    private function fetchFiles($basePath) {
        if (!file_exists($basePath)) {
            return array();
        }
        $d = dir($basePath);
        while (false !== ($entry = $d->read())) {
            if ($entry != "." && $entry != ".." && strtolower(substr($entry, -4, 4)) == '.gif') {
                $fileArray[] = $entry;
            }
        }

        array_multisort($fileArray, SORT_DESC);

        return $fileArray;
    }

    public function encodeTitleForModRewrite($title) {
        // + muss dreifach codiert werden. -> %2B -> %252B -> %25252B
        // & muss doppelt codiert werden. -> %26 -> %2526
        // / muss doppelt codiert werden. -> %2F -> %252F
        return str_replace('%2F', '%252F', str_replace('%2B', '%25252B', str_replace('%26', '%2526', urlencode($title))));
    }

    /**
     * OLD function for ue3sick
     */
//    public function notifyUsers($rs, $category, $subject, $message, $link, $saveinDB = true) {
//
//        if ($this->tableExists('notifications') && $this->tableExists('notifications_settings')) {
//
//            $fromUserId = Session::get('uid');
//            $fromNick = Session::get('unick');
//
//            $subject = str_replace('$2', $fromNick, $subject);
//
//            $fieldsValueArray['user_id'] = $fromUserId;
//            //$fieldsValueArray['text'] = "'<a href=\"$link\">" . $subject . "</a>'";
//
//
//            switch ($category) {
//                case 'newentry':
//                    # TIM hat einen neuen Beitrag gepostet.
//                    #$aConfig['reLangUrl'] . "/" . $module
//                    $nickLink = "<a href='" . Config::get('reLangUrl') . "/profilpage/index/$fromUserId' >" . $fromNick . "</a> ";
//                    $actionLink = "<a href='$link' >" . $this->ts('entry') . "</a> ";
//
//                    $dbText = $this->ts('$1 created a new $2.');
//                    break;
//                case 'comownentry':
//                    $nickLink = "<a href='" . Config::get('reLangUrl') . "/profilpage/index/$fromUserId' >" . $fromNick . "</a> ";
//                    $actionLink = "<a href='$link' >" . $this->ts('comment') . "</a> ";
//
//                    $dbText = $this->ts('$1 wrote a $2.');
//
//                    break;
//                case 'comoncom':
//                    $nickLink = "<a href='" . Config::get('reLangUrl') . "/profilpage/index/$fromUserId' >" . $fromNick . "</a> ";
//                    $actionLink = "<a href='$link' >" . $this->ts('comment') . "</a> ";
//
//                    $dbText = $this->ts('$1 wrote a $2.');
//                    break;
//                case 'event':
//                    $nickLink = "<a href='" . Config::get('reLangUrl') . "/profilpage/index/$fromUserId' >" . $fromNick . "</a> ";
//                    $actionLink = "<a href='$link' >" . $this->ts('event') . "</a> ";
//
//                    $dbText = $this->ts('$1 created an $2.');
//                    break;
//                default :
//                    //do nothing
//                    break;
//            }
//
//            $fieldsValueArray['text'] = "'" . str_replace('$1', $nickLink, str_replace('$2', $actionLink, $dbText)) . "'";
//
//
//            if ($saveinDB) {
//                $this->insert('notifications', $fieldsValueArray);
//            }
//
////            $rs = $this->select('user as u, ' . Config::get('dbPrefix') . 'notifications_settings as ns'
////                    , 'u.id, u.email, u.nick, ns.newentry'
////                    , "u.id=ns.user_id AND ns.$notifyCategory=1 AND u.id!='$fromUserId'");
//
//            if ($rs !== FALSE && sizeof($rs) > 0) {
//                foreach ($rs as $r) {
//
//                    $message = str_replace('$2', $fromNick, str_replace('$1', $r['nick'], $message));
//
//                    $this->mail_utf8($r['email'], $subject, $message);
//                }
//            }
//        }
//    }
}
