<?php

/**
 *  Copyright © tim 15.02.2018
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
/**
 * Static functions to create, read, write config files and objects.
 * @package Util
 */
class Config {

    private static $config;
    public static $useConfig;

    public static function createConfig() {
        include_once 'config/config.php';

        if (self::getUseConfig() != '' && is_readable('config/config_' . self::getUseConfig() . '.php')) {
            $bConfig = $aConfig;
            include_once 'config/config_' . self::getUseConfig() . '.php';
            foreach($bConfig as $key => $value){
                if(isset($aConfig[$key])){
                    $bConfig[$key] = $aConfig[$key];
                }
            }
            // haha lol. switch again so only variables in the main config are present.
            $aConfig = $bConfig;
        }
        $protocol = (stripos($_SERVER['SERVER_PROTOCOL'], 'https') !== false || $_SERVER['SERVER_PORT'] == '443') ? 'https://' : 'http://';
        $port = ":" . $_SERVER['SERVER_PORT'];

        if ($_SERVER['SERVER_PORT'] == '80' || $_SERVER['SERVER_PORT'] == '443') {
            $port = "";
        }
        $aConfig['rootUrl'] = $protocol . $_SERVER['SERVER_NAME'] . $port;
        //$aConfig['rootUrl'] = ($_SERVER['SERVER_PORT'] == '80' ? 'http' : 'https') . "://" . $_SERVER['SERVER_NAME'];
        Config::saveConfig($aConfig);
        return $aConfig;
    }
    
    public static function getDefaultConfig(){
        include 'config/config.php';
        return $aConfig;
    }

    public static function saveConfig($array) {
        self::setUseConfig();
        self::$config = $array;
    }

    public static function getConfig() {
        return self::$config;
    }

    public static function get($key, $default=null) {
        $config = self::getConfig();
        if($config[$key] == null){
            return $default;
        }
        return $config[$key];
    }

    public static function addConfigKey($key, $value) {
        $c0nfig = self::getConfig();
        $c0nfig[$key] = $value;
        self::saveConfig($c0nfig);
    }

    public static function getUseConfig() {
        if (self::$useConfig == null) {
            self::setUseConfig();
        } 
        return self::$useConfig;
    }
    
    private static function setUseConfig() {
        if (is_file('config/useConfig.php')) {
            include('config/useConfig.php');
            self::$useConfig = $useConfig;
        } else {
            self::$useConfig = '';
        }
    }

}
