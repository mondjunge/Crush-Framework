<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 * @package Unit
 */
require_once 'core/interfaces/IMessage.php';
//require_once 'core/Controller.php';
require_once 'core/Language.php';
require_once 'core/CachedTemplate.php';
require_once 'core/helper/Menu.php';

/**
 * Unit Test for Menu helper functions
 *
 * @package Unit
 */
class testMenu extends UnitTestCase {

    function testAddRemove() {

        $oMenu = Menu::getInstance();
        $oMenu->add('install', 'admin/install');

        $this->assertIsA($oMenu->getStructure(), 'Array');

        $oMenu->add('start', 'site/start');
        $this->assertIdentical(count($oMenu->getStructure()), 2);

        $oMenu->clear();
        $this->assertIsA($oMenu->getStructure(), 'Array');
        $this->assertIdentical(count($oMenu->getStructure()), 0);
    }

}
