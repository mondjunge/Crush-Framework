<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 * @package Unit
 */

require_once 'core/Registry.php';
/**
 * Unit Test for Registry Class
 *
 * @package Unit
 */
class testRegistry extends UnitTestCase {

    function testIsSingleton() {
        $reg = Registry::getInstance();
        $this->assertIsA($reg, 'Registry');
        $this->assertSame($reg, Registry::getInstance());
    }

    function testSetGetValid() {
        $reg = Registry::getInstance();

        $reg->set('testkey', 'testvalue');

        $this->assertTrue($reg->isValid('testkey'));

      
    }

}