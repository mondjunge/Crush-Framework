<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 *  
 */
/**
 * Unit test for db connections
 * @package Unit
 */
class testDatabase extends UnitTestCase {

    function testConnection() {

        try {
            $aConfig = Config::getConfig();
            $dbType = ucfirst($aConfig['dbType']);

            $oDB = call_user_func(array($dbType, "getInstance"));
            $this->assertIsA($oDB, $dbType, 'Database Connection is no instance of Mysql');
        } catch (Exception $e) {
            $this->assertTrue(false, 'Exception thrown while connecting to database');
        }
    }

    function testQuery() {
        $aConfig = Config::getConfig();
        $oController = Database::getInstance();

        $rs = $oController->query("CREATE TABLE IF NOT EXISTS `" . $aConfig['dbPrefix'] . "test` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
                  PRIMARY KEY (`id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8");
        $this->assertTrue($rs);
    }

    function testInsert() {
        $aConfig = Config::getConfig();
        $oController = Database::getInstance();

        $fieldsValueArray = array(
            "key" => "testkey",
            "value" => "fortestvalue"
        );
        $csvMessages = "";
        if (!$oController->insert('test', $fieldsValueArray)) {
            foreach ($oController->getDebugMessages() as $message) {
                $csvMessages .= " :: $message";
            }
            $ass = false;
        } else {
            $ass = true;
        }
        $this->assertTrue($ass, "Cannot insert test data into test table" . $csvMessages);
    }

    function testUpdate() {
        $oController = Database::getInstance();

        $fieldsValueArray = array(
            "key" => "testkey",
            "value" => "updateTestvalue"
        );
        $csvMessages = "";
        if (!$oController->insert('test', $fieldsValueArray)) {
            foreach ($oController->getDebugMessages() as $message) {
                $csvMessages .= " :: $message";
            }
            $ass = false;
        } else {
            $ass = true;
        }
        $this->assertTrue($ass, "Cannot update test data in test table" . $csvMessages);
    }

    function testSelect() {
        $oController = Database::getInstance();
        //select values
        $aResult = $oController->select('test', '`value`', "`key`='testkey'");

        //print_r($oDB->getDebugMessages());
        // check if there is a result Array
        $this->assertIsA($aResult, 'Array');
    }

    function testDelete() {
        $aConfig = Config::getConfig();
        $oController = Database::getInstance();


        if (!$oController->delete('test', "`key`='testkey'")) {
            $ass = false;
        } else {
            $ass = true;
        }
        $this->assertTrue($ass, "Cannot delete test data from system table");

        $oController->query("DROP TABLE `" . $aConfig['dbPrefix'] . "test`");
    }

}
