<?php

/**
 *  Copyright © tim 08.10.2018
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: A data representation of a MenuItem
 */
class MenuItem {

    private $name;
    private $title;
    private $link;
    private $selected;
    private $icon;

    public function __construct($name, $title, $link, $selected, $icon) {
        $this->name = (String) $name;
        $this->title = (String) $title;
        $this->link = (String) $link;
        $this->selected = (bool) $selected;
        $this->icon = (bool) $icon;
    }

    /**
     * shown text of the link
     * @return \String
     */
    public function getName(): String {
        return $this->name;
    }

    /**
     * text for the title attribute of the item
     * @return \String
     */
    public function getTitle(): String {
        return $this->title;
    }
    
    /**
     * url of the nav item
     * @return \String
     */
    public function getLink(): String {
        return $this->link;
    }
    
    /**
     * if the item represents the current navigation state, should be highlighted 
     * @return \Boolean
     */
    public function getSelected(): bool {
        return $this->selected;
    }
    
    /**
     * if an icon should be rendered for the menuitem
     * @return \Boolean
     */
    public function getIcon(): bool {
        return $this->icon;
    }

}
