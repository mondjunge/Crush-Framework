<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: 
 * @package View
 */

/**
 * Language Class to translate GUI text into different languages
 * translates Strings and adds untranslated Strings to the available language files.
 * 
 * @package View
 */
class Language {

    private $translations;
    private $langFile;
    private $lang;
    private $modulePath;

    /**
     * loads the language file for the current module,
     * loads a special file with suffix _$siteStyle if is_file
     * @param string $modulePath
     * @return unknown_type
     */
    public function loadLang($modulePath, $lang) {
        $this->lang = $lang;
        $this->modulePath = $modulePath;
        $this->langFile = $this->modulePath . 'lang/' . $this->lang . '.php';
        
        $this->translations = $this->readLangFile($this->langFile);
        
        if(is_file($this->modulePath . 'lang/' . $this->lang . '_'.Config::$useConfig.'.php')){
            $customTranslations = $this->readLangFile($this->modulePath . 'lang/' . $this->lang . '_'.Config::$useConfig.'.php');
            foreach($customTranslations as $key => $value) {
                $this->translations[$key] = $value;
            }
        }
        
    }

    /**
     * Function takes a String, search for a translation in the appropriate Language file and gives back
     * the translated string. If no translation is available, the Key-String will be appended to the
     * current language file if it is not present and a String that indicates to translate it will be given back.
     * @param string $string
     * @param array $param - '$1' gets replaced by $param[1], '$user' by $param['user'], etc.
     * @return string
     */
    public function ts($string, $param = null) {
        if (isset($this->translations[$string]) && trim($this->translations[$string]) != '') {
            $translation = $this->translations[$string];
        } elseif (!isset($this->translations[$string]) || trim($this->translations[$string]) == '') {
            $translation = '$L_' . $string;
        }

        if (!isset($this->translations[$string])) {
            $translation = '$L_' . $string;
            $this->addTranslationToLangFile($string);
        }
        /**
         * replace $params in translation
         */
        if ($param !== null && $param !== '') {
            foreach ($param as $key => $value) {
                $translation = str_replace('$' . $key, $value, $translation);
            }
        }
        
        // translationMode ?!
        if(Config::get('maintainanceMode') 
                // blog rules
                && substr($string, 0, 5) != "d|m|Y"
                && substr($string, 0, 5) != "Y-m-d"){
            return "<span class='translate_string'>" . $translation ."</span>";
            //return $translation;
        } else{
            return $translation;
        }
                
    }

    private function addTranslationToLangFile($string) {
        /**
         * if translation string is not set, put it in the file.
         * search for other files and place a String there also
         */
        $aAllLangFiles = $this->searchLangFiles();
        foreach ($aAllLangFiles as $langFile) {
            /**
             * load lang File again, to avoid repetitions caused by loops
             */
            $translationA = $this->readLangFile($langFile);
            if (!isset($translationA[$string])) {
                $this->addTranslationString($langFile, $string);
            }
        }
        /**
         * add dev lang file when it is not there  
         */
        $devLangFile = $this->modulePath . 'lang/gb.php';
        $translationA = $this->readLangFile($devLangFile);
        if (!isset($translationA[$string])) {
            $this->addTranslationString($devLangFile, $string);
        }
    }

    /**
     * search for language files in the module
     * @return string
     */
    private function searchLangFiles() {
        $allFiles = scandir($this->modulePath . 'lang/');
        foreach($allFiles as $key => $value) {
            if (substr($value, -4, 4) == '.php' && strpos($value, '_') === FALSE){
                $allLangFiles[] = $this->modulePath . 'lang/' . $value;
            }
        }
        return $allLangFiles;
    }

    /**
     * adds a String to the language file
     * if the language file is empty, add copyright information and array..
     * @param <type> $string
     * @return void
     */
    private function addTranslationString($langFile, $rawString) {

        $string = str_replace("'", "\'", $rawString);
        $filecontent = trim(file_get_contents($langFile));

        if (is_file($langFile) && ");" == substr($filecontent, -2, 2)) {

            $filecontent = substr($filecontent, 0, -2);

            if ('gb' == basename($langFile, ".php")) {

                file_put_contents($langFile, $filecontent . "\t,\n\t'" . $string . "'=>\n\t'" . $string . "'\n);");
            } else {
                file_put_contents($langFile, $filecontent . "\t,\n\t'" . $string . "'=>\n\t''\n);");
            }
        } elseif (is_file($langFile) && $filecontent == '') {

            if ('gb' == basename($langFile, ".php")) {
                $filecontent = $this->getTranslationFileStartString($string, true);
            } else {
                $filecontent = $this->getTranslationFileStartString($string, false);
            }

            file_put_contents($langFile, $filecontent);
        }
    }
    
    private function getTranslationFileStartString($translationString, $isDevLang){
        if($isDevLang){
            return "<?php

/**
 *  Language file
 */
\$aTranslationStrings = array(
\t'" . $translationString . "' =>
\t'" . $translationString . "'
);";
        }else{
            return "<?php

/**
 *  Language file
 */
\$aTranslationStrings = array(
\t'" . $translationString . "' =>
\t''
);";
        }
        
    }

    /**
     * reads the lang File and gives back an array of translations
     * @return array
     */
    private function readLangFile($langFile) {
        if (is_file($langFile)) {
            include($langFile);
        } else {
            $this->createLangFile($langFile);
        }

        if (isset($aTranslationStrings)) {
            return $aTranslationStrings;
        } else {
            return [];
        }
    }

    private function createLangFile($langFile) {
        if ($this->lang === '') {
            return;
        }
        $f = fopen($langFile, "w");
        fclose($f);
        chmod($langFile, 0666);
    }

}
