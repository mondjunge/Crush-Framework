<?php

/**
 *  Copyright © tim 31.01.2018
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
use MatthiasMullie\Minify;
/**
 * Static functions to minify, cache and return css and js files.
 * @package Util
 */
class Minifier {
    
    // update url: https://github.com/matthiasmullie/minify/releases
    private static $version = '1.3.63';
    // update url: https://github.com/matthiasmullie/path-converter/releases
    private static $pathConverterVersion = '1.1.3';
    

    private static $maxImportSize = 500; // in kB, 5 is default from CSS.php
    private static $cachePath = "";

    /**
     * takes a set of CSS files paths and returns a string of minified files content
     * this class caches all minified files until the sources get changed.
     * 
     * @param array or string $input is an array of filepaths to css resources to be combined, or a string to be minified.
     * return string of the combined css files 
     */
    public static function minifyCss($inputParam) {
        self::$cachePath = str_replace("core","cache/",__DIR__ );
        
        $input = array_unique($inputParam);
        if (empty($input)) {
            return "";
        }
        
        // handle plain css...
        if (gettype($input) === "string") {
            $fileNameHash = hash("md5", $input);
        } else {
            // handle array of css filepath
            $fileNameHash = hash("md5", implode(",", $input));
            $timeModified = 0;
            foreach ($input as $f) {
                $fileModified = filemtime($f);
                if ($fileModified > $timeModified) {
                    $timeModified = $fileModified;
                }
            }
        }

        try {
            return self::getCssCacheFile($fileNameHash, $timeModified);
        } catch (Exception $ex) {
            // do nothing, just go on...
        }
        spl_autoload_register("self::includeLibs");
        $minifier = new Minify\CSS();
        if (gettype($input) === "string") {
            $minifier->add($input);
        } else {
            $minifier->setMaxImportSize(self::$maxImportSize);
            foreach ($input as $f) {
                $minifier->add($f);
            }
        }

        $contents = $minifier->minify();
        if(!self::setCssCacheFile($fileNameHash, $contents)){
           echo "cannot put cache files..."; 
        }
        return $contents;
    }

    private static function setCssCacheFile($hash, $contents) {
        $cachePath = self::$cachePath . "min_" . $hash . ".css";
        if(is_writable(self::$cachePath)){
            return file_put_contents($cachePath, $contents);
        } else {
            echo "Cache directory not writeable!";
        }
        
    }

    private static function getCssCacheFile($hash, $filesModified) {
        $cachePath = self::$cachePath . "min_" . $hash . ".css";
        if (is_readable($cachePath)) {
            $cacheModified = filemtime($cachePath);

            if ($filesModified > $cacheModified) {
                // delete files if they are older than the last modified file
                unlink($cachePath);
                throw new Exception();
            }
           
            return file_get_contents($cachePath);
        } else {
            throw new Exception();
        }
    }

    /**
     * takes a set of JS File path and gives back minified JS as String.
     * 
     * @param type $filePathArray
     * @return type
     */
    public static function minifyJs($filePathArray) {
        self::$cachePath = str_replace("core","cache/",__DIR__ );
        $filePathArray = array_unique($filePathArray);
        $fileNameHash = hash("md5", implode(",", $filePathArray));
        $timeModified = 0;
        foreach ($filePathArray as $f) {
            $fileModified = filemtime($f);
            if ($fileModified > $timeModified) {
                $timeModified = $fileModified;
            }
        }

        try {
            return self::getJsCacheFile($fileNameHash, $timeModified);
        } catch (Exception $ex) {
            // do nothing, just go on...
        }
        spl_autoload_register("self::includeLibs");
        $minifier = new Minify\JS();
        foreach ($filePathArray as $f) {
            $minifier->add($f);
        }

        $contents = $minifier->minify();
        self::setJsCacheFile($fileNameHash, $contents);
        return $contents;
    }

    private static function setJsCacheFile($hash, $contents) {
        $cachePath = self::$cachePath . "min_" . $hash . ".js";
        return file_put_contents($cachePath, $contents);
    }

    private static function getJsCacheFile($hash, $filesModified) {
        $cachePath = self::$cachePath . "min_" . $hash . ".js";
        if (is_readable($cachePath)) {
            $cacheModified = filemtime($cachePath);
            if ($filesModified > $cacheModified) {
                // delete files if they are older than the last modified file.
                unlink($cachePath);
                throw new Exception();
            }

            return file_get_contents($cachePath);
        } else {
            throw new Exception();
        }
    }

    public static function includeLibs($className) {
        require_once 'extras/minify-'.self::$version.'/src/Minify.php';
        require_once 'extras/minify-'.self::$version.'/src/CSS.php';
        require_once 'extras/minify-'.self::$version.'/src/JS.php';
        require_once 'extras/minify-'.self::$version.'/src/Exception.php';
        require_once 'extras/minify-'.self::$version.'/src/Exceptions/BasicException.php';
        require_once 'extras/minify-'.self::$version.'/src/Exceptions/FileImportException.php';
        require_once 'extras/minify-'.self::$version.'/src/Exceptions/IOException.php';
        require_once 'extras/path-converter-'.self::$pathConverterVersion.'/src/ConverterInterface.php';
        require_once 'extras/path-converter-'.self::$pathConverterVersion.'/src/Converter.php';
    }

}
