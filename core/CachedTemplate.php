<?php

/**
 * Copyright (c) 2003 Brian E. Lozier (brian[at]massassi.net)
 *
 * Template Class modified 2010 by Tim Wahrendorff (tim[at]timwahrendorff.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * @package View
 */

/**
 * Template Class to process php templates
 *
 * @package View
 */
class Template extends Language {

    private $vars = array(); /// Holds all the template variables
    private $path; /// Path to the templates
    private static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {
        // have to be empty
    }

    private function __construct() {
        
    }

    /**
     * Set the path to the template files.
     *
     * @param string $path path to template files
     * @return void
     */
    public function set_path($path) {
        $this->path = $path;
    }

    /**
     * 
     * @param type $url
     * @param type $class
     * @param type $alt
     * @return type
     */
    public function icon($url, $class = '', $alt = 'icon') {
        $icon = $this->loadIcon($url, $class, $alt);
        if($icon === false){
            $icon = $this->loadIcon($url, $class, $alt, 'default');
        }
        return $icon;
    }

    private function loadIcon($url, $class = '', $alt = 'icon', $theme = null) {
        
        if($theme == null){
            $theme = Config::get('siteStyle');
        }
        // new FUNction iconSvg with file search like icon or check for fileending in one func
        $splitUrl = explode('#', $url);
        $urlTarget = '';
        $fileExt = $this->get_file_extension($splitUrl[0]);
        if (!empty($splitUrl[1])) {
            $url = $splitUrl[0];
            $urlTarget = '#' . $splitUrl[1];
        }

        // try to load icon from modules dir in theme
        $themeModuleUrl = "theme/" . $theme . "/images/modules/" . filter_input(INPUT_GET, 'site') . "/icons/" . $url;
        if (is_file($themeModuleUrl)) {
            if ($fileExt === 'svg') {
                return "<svg viewBox='0 0 8 8' class='icon $class'>
                <use xlink:href='" . Config::get('relativeUrl') . $themeModuleUrl . $urlTarget . "'></use>
              </svg>";
            } else {
                return "<img class='icon $class' src='" . Config::get('relativeUrl') . $themeModuleUrl . "' alt='$alt' />";
            }
        }
        // try to load icon from theme
        $baseUrl = "theme/" . $theme . "/images/icons/" . $url;
        if (is_file($baseUrl)) {
            if ($fileExt === 'svg') {
                return "<svg viewBox='0 0 8 8' class='icon $class'>
                <use xlink:href='" . Config::get('relativeUrl') . $baseUrl . $urlTarget . "'></use>
              </svg>";
            } else {
                return "<img class='icon $class' src='" . Config::get('relativeUrl') . $baseUrl . "' alt='$alt' />";
            }
        } else {
            $moduleUrl = Config::get('applicationDirectory') . "/" . filter_input(INPUT_GET, 'site') . "/images/icons/" . $url;
            if (is_file($moduleUrl) && $this->copyIconFromModuleDir($moduleUrl, $urlTarget, $themeModuleUrl, $fileExt, $class, $alt)) {
                if ($fileExt === 'svg') {
                    return "<svg viewBox='0 0 8 8' class='icon $class'>
                        <use xlink:href='" . Config::get('relativeUrl') . $themeModuleUrl . $urlTarget . "'></use>
                      </svg>";
                } else {
                    return "<img class='icon $class' src='" . Config::get('relativeUrl') . $themeModuleUrl . "' alt='$alt' />";
                }
            } else {
                // fallback to default theme
                return false;
            }
        }
    }

    private function copyIconFromModuleDir($moduleUrl, $urlTarget, $themeModuleUrl, $fileExt, $class, $alt) {
        /**
         * can't load images from application dir, cause it is protected for localhost only
         * 
         * copy from module to theme dir.
         */
        
        mkdir(substr(__DIR__, 0, -4) . "theme/" . Config::get('siteStyle') . "/images/modules/" . filter_input(INPUT_GET, 'site') . "/icons/", 0777, true);
        if (!copy(substr(__DIR__, 0, -4) . $moduleUrl, substr(__DIR__, 0, -4) . $themeModuleUrl)) {
            echo "error copying module icon to theme";
            return false;
        } else {
            return true;
        }
    }

    /**
     * 
     * @param type $url - relative url from doc root or web link starting with http
     * @param type $class to put on the span around the image
     * @param string $width
     * @param string $height
     * @param type $title
     * @return type
     */
    public function image($url, $class = '', $width = '', $height = '', $title = '') {
        /**
         *  - create thumbnail if image is bigger than widht & height
         *  - link to thumbnail
         */
        if (substr($url, 0, 4) == 'http' || substr($url, 0, 2) == '//') {
            //external image
            // download to cache
            if (substr($url, 0, 2) == '//') {
                $url = "http:" . $url;
            }
            if (strpos($url, '?') !== FALSE) {
                $arr = explode('?', $url);
                $url = $arr['0'];
            }
            $imgUrl = $url;
            $savePath = dirname(__FILE__) . "/../cache/thumbs/src/" . md5($imgUrl) . "." . $this->get_file_extension(basename($imgUrl));
            $this->saveImageToCache($url, $savePath);
        } else if (substr($url, 0, 1) != '/') {
            //internal image
            //relative url, do magic to find an image
            $defaultUrl = "theme/default/images/" . $url;
            $themeUrl = "theme/" . Config::get('siteStyle') . "/images/" . $url;
            $themeModuleUrl = "theme/" . Config::get('siteStyle') . "/images/modules/" . filter_input(INPUT_GET, 'site') . "/" . $url;
            if (is_file($themeModuleUrl)) {
                $savePath = $themeModuleUrl;
            } else if (is_file($themeUrl)) {
                $savePath = $themeUrl;
            } else if(is_file(Config::get("applicationDirectory") . "/" . filter_input(INPUT_GET, 'site') . "/images/" . $url)){
                $savePath = Config::get("applicationDirectory") . "/" . filter_input(INPUT_GET, 'site') . "/images/" . $url;
            } else {
                $savePath = $defaultUrl;
            }
        } else { // if (substr($url, 0, 1) == '/')
            // Old behaviour
            // TODO: Legacy code, remove when possible or replace with code for absolute internal images               
            /**
             * TODO: nasty shit
             */
            $saveBase = str_replace(Config::get('relativeUrl') . "core", '', str_replace('\\', '/', dirname(__FILE__)));
            $savePath = $saveBase . '/' . $url;
        }
        $fileExt = $this->get_file_extension($url);
        if ($fileExt === 'svg') {
            return "<div class='svgImage $class' >".file_get_contents($savePath)."</div>";
        } else {
            return $this->getImgHtml($savePath, $class, $width, $height, $title, $url);
        }
        
    }

    private function getImgHtml($savePath, $classProp, $width, $height, $title, $url) {
        /**
         * quadrate ohne verzerrung, bitte 
         * TODO: scheint buggy zu sein
         */
        list($imgWidth, $imgHeight, $imgType, $imgAttr) = getimagesize($savePath);
        // ext enkodn
        $ext = strtolower(pathinfo($savePath, PATHINFO_EXTENSION));
        
         if ($ext == 'jpg') {
            // exif data is only available in jpg files.
            $exif = exif_read_data($savePath);
            
            // orientierung fix für jpg
            if (isset($exif['Orientation'])) {
                switch($exif['Orientation']) {
                    case 3: // 180°
                        break;
                    case 6: // 90° cw
                        // w, h vertauschen
                        list($imgWidth, $imgHeight) = array($imgHeight, $imgWidth);
                        break;
                    case 8: // 90° ccw
                        // w, h vertauschen
                        list($imgWidth, $imgHeight) = array($imgHeight, $imgWidth);
                        break;
                }
            }
        }
        
        if ($imgWidth > $imgHeight) {
            // landscape
            $classProp .= " landscape";
//            if ($width == $height) {
//                $height = '';
//            }
        }
        if ($imgWidth < $imgHeight) {
            // portrait
            $classProp .= " portrait";
//            if ($width == $height) {
//                $width = '';
//            }
        }
        $fileExt = $this->get_file_extension($url);
        if ($fileExt !== 'svg') {
            // create optimized cached versions of bitmap images
            $imgUrl = Config::get('relativeUrl') . "cache/thumbs/" . $this->createThumbnail($savePath, $width, $height);
        
        } else {
            $imgUrl = Config::get('relativeUrl') . $savePath;
        }
        
        // output html
        $class = " class='$classProp' ";
        
        $alt = '';
        if ($title != '') {
            $alt = " alt='$title' ";
            $title = " title='$title' ";
        }

        return "<img src='" . $imgUrl . "' " . $title . $alt . $class . "data-original-url='$url' />"; // . $imgContainerDivClose;
    }

    private function saveImageToCache($imgUrl, $savePath) {
        if (is_file($savePath)) {
            return;
        } else {
            $ch = curl_init($imgUrl);
            $fp = fopen($savePath, 'x');

            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_exec($ch);
            curl_close($ch);

            fclose($fp);
            return;
        }
    }

    private function createThumbnail($src, $width, $height) {
        $t = Thumb::Instance();
        return $t->createThumbnail($src, $width, $height);
    }

    private function get_file_extension($file_name) {
        return substr(strrchr($file_name, '.'), 1);
    }

    /**
     * Path from Documents root with lang attribute
     * normally good for form attribute action values 
     * or creating href for inside links.
     * @return String Path from DocumentRoot to module including language part
     */
    public function action($module = '') {
        return Config::get('reLangUrl') . $module;
    }

    /**
     * Set a template variable.
     *
     * @param string $name name of the variable to set
     * @param mixed $value the value of the variable
     * @return void
     */
    public function set($name, $value) {
        $this->vars[$name] = $value;
    }

    /**
     * Set a bunch of variables at once using an associative array.
     *
     * @param array $vars array of vars to set
     * @param bool $clear whether to completely overwrite the existing vars
     * @return void
     */
    public function set_vars($vars, $clear = false) {
        if ($clear) {
            $this->vars = $vars;
        } else {
            if (is_array($vars))
                $this->vars = array_merge($this->vars, $vars);
        }
    }

    /**
     * Translates and sets a template variable
     *
     * @param String $name
     * @param String $value
     * @param boolean $return whether to return the translated String or save it for fetching
     * return void
     */
    public function translate($name, $value, $return = false) {
        if ($return) {
            return $this->ts($value);
        } else {
            $this->vars[$name] = $this->ts($value);
        }
    }

    /**
     * translates a bunch of variables and sets them, using an associative array
     * 
     * @param array $vars
     * @param boolean $clear whether to completely overwrite the existing vars
     */
    public function translate_vars($vars, $clear = false) {
        if ($clear)
            $this->vars = array();

        foreach($vars as $key => $value) {
            $this->translate($key, $value);
        }
    }

    /**
     * Open, parse, and return the template file.
     *
     * @param String $file the template file name
     * @return String
     */
    public function fetch($file, $vars = null, $clear = false) {
        if ($vars != null) {
            $this->set_vars($vars, $clear);
        }

        if ($this->vars != null) {
            extract($this->vars);      // Extract the vars to local namespace
        }
        ob_start();                    // Start output buffering
        include($this->path . $file . '.tpl');  // Include the file
        $contents = ob_get_contents(); // Get the contents of the buffer
        ob_end_clean();                // End buffering and discard
        return $contents;              // Return the contents
    }

}

/**
 * An extension to Template that provides caching of
 * template contents. 
 * You need to actively use the function fetch_cache to archive a caching of desired templates.
 * @package View
 */
class CachedTemplate extends Template {

    var $cache_id;
    var $expire;
    var $cached;

    /**
     * Constructor.
     *
     * @param string $path path to template files
     * @param string $cache_id unique cache identifier
     * @param int $expire number of seconds the cache will live
     * @return void
     */
    public function __construct($path, $cache_id = null, $expire = 900) {
        $this->set_path($path);
        $this->cache_id = $cache_id ? 'cache/' . md5($cache_id) : $cache_id;
        $this->expire = $expire;
    }

    /**
     * Test to see whether the currently loaded cache_id has a valid
     * corrosponding cache file.
     * @return bool
     */
    private function is_cached() {
        if ($this->cached) {
            return true;
        }

        // Passed a cache_id?
        if (!$this->cache_id) {
            return false;
        }

        // Cache file exists?
        if (!file_exists($this->cache_id)) {
            return false;
        }

        // Can get the time of the file?
        if (!($mtime = filemtime($this->cache_id))) {
            return false;
        }

        // Cache expired?
        if (($mtime + $this->expire) < time()) {
            @unlink($this->cache_id);
            return false;
        } else {
            /**
             * Cache the results of this is_cached() call.  Why?  So
             * we don't have to double the overhead for each template.
             * If we didn't cache, it would be hitting the file system
             * twice as much (file_exists() & filemtime() [twice each]).
             */
            $this->cached = true;
            return true;
        }
    }

    /**
     * This function returns a cached copy of a template (if it exists),
     * otherwise, it parses it as normal and caches the content.
     *
     * @param $file string the template file
     *
     * @return string
     */
    public function fetch_cache($file) {
        if ($this->is_cached()) {
            $fp = @fopen($this->cache_id, 'r');
            $contents = fread($fp, filesize($this->cache_id));
            fclose($fp);
            return $contents;
        } else {
            $contents = $this->fetch($file);

            // Write the cache
            if ($fp = @fopen($this->cache_id, 'w')) {
                fwrite($fp, $contents);
                fclose($fp);
            } else {
                die('Unable to write cache.');
            }

            return $contents;
        }
    }

}
