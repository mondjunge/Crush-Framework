<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * @package Helper
 */

/**
 * Link Helper for creating links and jump to other sites via header("Location:...
 *
 * @package Helper
 */
class Link {

    /**
     * helps to create a link to another function in the same module
     * @param String $description Text to be shown as the link
     * @param String $targetFunction
     */
//    public static function linkFunction($description, $targetFunction) {
//        $aConfig = Config::getConfig();
//        $link = '<a href="' . $aConfig['relativeUrl'] .
//                '' . $this->getLanguage() .
//                '/' . $this->getActiveModule() .
//                '/' . $targetFunction . '" >' . $description . '</a>';
//        return $link;
//    }

    /**
     * helps to create a link to another module
     * @param String $description Text to be shown as the link, html possible.
     * @param String $targetModule 'modulename[/desiredfunction]' as target. [] is optional, if not given index function is called.
     */
    public static function linkModule($description, $targetModule) {
        $aConfig = Config::getConfig();
        $link = '<a href="' . $aConfig['reLangUrl'] 
                . $targetModule . '" >' . $description . '</a>';
        return $link;
    }
    /**
     * Path from Documents root with lang attribute
     * normally good for form attribute action values 
     * or creating href for inside links.
     * @return String Path from DocumentRoot to module including language part
     */
    public static function action($module='') {
        $aConfig = Config::getConfig();
        return $aConfig['reLangUrl'] . $module;
    }
    
    /**
     * goto another module via header
     * @global array $aConfig
     * @param string $targetModule
     */
    public static function gotoModule($targetModule) {
        $aConfig = Config::getConfig();
        self::retainMessages();
        header('Location: '.$aConfig['reLangUrl']  . $targetModule . '');
        exit;
    }

    /**
     * jump to another module via header, synonym for gotoModule
     * @global array $aConfig
     * @param string $targetModule
     */
    public static function jumpTo($targetModule) {
        //$aConfig = Config::getConfig();
//        if($aConfig['AJAXsupport'] && filter_has_var(INPUT_GET,'AJAX')){
//            if(preg_match("/[\?]{1,}/", $targetModule)==0){
//                //echo "? gefunden HALLO!";
//                $targetModule = $targetModule."?AJAX";
//            }else{
//                //echo "ELSE HALLO!";
//                $targetModule = $targetModule."&AJAX";
//            }
//        }
        self::gotoModule($targetModule);
    }

    /**
     * jump to the last site you where on. Uses HTTP_REFERER to find out what site this was 
     * @global array $aConfig
     */
    public static function jumpToLast() {
        //$aConfig = Config::getConfig();
        self::retainMessages();
//        if($aConfig['AJAXsupport'] && substr($_SERVER[ 'HTTP_REFERER'], -4, 4) == "AJAX"){
//           $_SERVER['HTTP_REFERER'] = str_replace("?AJAX", "", $_SERVER[ 'HTTP_REFERER']);
//            $_SERVER['HTTP_REFERER'] = str_replace("&AJAX", "", $_SERVER[ 'HTTP_REFERER']);
//        }
        if (substr($_SERVER[ 'HTTP_REFERER'], 0, 5) == "https") {
            header("Location: " . str_replace("https://" . $_SERVER[ 'HTTP_HOST'], "", $_SERVER[ 'HTTP_REFERER']) . "");
            exit;
        } else {
            header("Location: " . str_replace("http://" . $_SERVER[ 'HTTP_HOST'], "", $_SERVER[ 'HTTP_REFERER']) . "");
            exit;
        }
    }

    /**
     * save any generated messages in Session before header is send
     */
    private static function retainMessages() {
        
        $m = Message::getInstance();
        //print_r ($m->getSystemMessages()); exit;
        Session::set('errorMessages', $m->getErrorMessages(),true,false);
        Session::set('systemMessages', $m->getSystemMessages(),true,false);
        Session::set('debugMessages', $m->getDebugMessages(),true,false);

        Session::writeClose();
    }

}