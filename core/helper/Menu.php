<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 * @package Helper
 */

/**
 * Helper for creating Navigation Menus
 *
 * @package Helper
 */
class Menu extends Template {

    private $content = array();
    private $menuItems = array();
    static private $_instance;

    public static function getInstance() {

        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {
// have to be empty
    }

    private function __construct($array = null) {
//        $aConfig = Config::getConfig();
//        if ($aConfig['debugMode'])
//            $this->setDebugMessage('<b>Menu</b>');
    }

    /**
     * add a menuItem to the navigation
     * @param MenuItem $menuItem
     */
    public function addItem(MenuItem $menuItem): void {
        $this->menuItems[] = $menuItem;
    }

    /**
     * 
     * @param type $mode - default or admin
     * @return String
     */
    public function renderMenuItems($mode = "default"): String {

        $path = 'theme/' . Config::get('siteStyle') . '/view/menu/' . $mode . '/';
        if (!file_exists($path)) {
            $path = 'theme/default/view/menu/' . $mode . '/';
        }
        $this->set_path($path);
        $menu = '';

        foreach ($this->menuItems as $item) {
            $data['item'] = $item;
            $data['icon'] = $this->getIcon($item);
            if ((boolean) $item->getSelected()) {
                $menu .= $this->fetch('selected', $data, true);
            } else {
                $menu .= $this->fetch('element', $data, true);
            }
        }
        $this->clear();
        return $menu;
    }

    /**
     * get AppIcon for a MenuItem, if enabled
     * @param MenuItem $item
     * @return String
     */
    private function getIcon(MenuItem $item): String {
        if (Config::get('showAppIcons')) {
            $moduleArray = explode("/", $item->getLink());
            $iconFile = "theme/" . Config::get('siteStyle') . "/images/appIcons/" . $moduleArray[0] . ".svg";
            if (!file_exists($iconFile)) {
                $iconFile = 'theme/default/images/appIcons/' . $moduleArray[0] . '.svg';
            }
            $selectedStyle = "";
            if ($item->getSelected()) {
                $selectedStyle = "appIconSelected";
            }
            if (is_file($iconFile)) {
                //$iconFile = $aConfig['relativeUrl'].$iconFile;
                $iconFile = "<span class='appIcon $selectedStyle'>" . file_get_contents($iconFile) . "</span>";
            } else {
                $iconFile = "";
            }
        } else {
            $iconFile = "";
        }
        return $iconFile;
    }

    /**
     * @Deprecated use addItem instead
     * Adds a link to the menu object
     * @param String $name
     * @param String $link
     * @param boolean $selected
     */
    public function add($name, $link, $selected = false) {
        $aConfig = Config::getConfig();
        $module = str_replace("/index", "", $link);

        if ($aConfig['showAppIcons']) {
            $moduleArray = explode("/", $module);
            $iconFile = "theme/" . $aConfig['siteStyle'] . "/images/appIcons/" . $moduleArray[0] . ".svg";
            $selectedStyle = "";
            if ($selected) {
                $selectedStyle = "appIconSelected";
            }
            if (is_file($iconFile)) {
                //$iconFile = $aConfig['relativeUrl'].$iconFile;
                $iconFile = "<span class='appIcon $selectedStyle'>" . file_get_contents($iconFile) . "</span>";
            } else {
                $iconFile = "";
            }
        } else {
            $iconFile = "";
        }

        $newLink = array('name' => $name, 'link' => $module, 'selected' => $selected, 'icon' => $iconFile);
        $this->content[] = $newLink;
    }

    /**
     * @Depricated
     * gets the link array out of the object (for testing)
     * @return array
     */
    public function getStructure() {
        return $this->content;
    }
    
    public function getMenuItems() {
        return $this->menuItems;
    }

    /**
     * clears the content and menuItem array
     */
    public function clear() {
        $this->content = array();
        $this->menuItems = array();
    }

    /**
     * @Depricated use addItems and renderMenuItems instead
     * render the menu
     */
    public function render($menuDirectory) {
        $aConfig = Config::getConfig();
        if (isset($aConfig['AJAXsupport']) && $aConfig['AJAXsupport']) {
            $ajax = 'ajax/';
        } else {
            $ajax = '';
        }
        $path = 'theme/' . $aConfig['siteStyle'] . '/menu/' . $menuDirectory . '/' . $ajax;
        $this->set_path($path);
        $menu = '';

        foreach ($this->content as $site) {
            // $site['link'] = '/'.$aConfig['relativeUrl'].'/'.$site['link'];
//            if ($aConfig['debugMode']) {
//                $this->setDebugMessage($site['name'] . ' - ' . $site['link']);
//            }

            if ($site['selected']) {
                $menu .= $this->fetch('selected', $site, true);
            } else {
                $menu .= $this->fetch('element', $site, true);
            }
        }
        $this->clear();
        return $menu;
    }

}
