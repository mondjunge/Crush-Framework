<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Notification
 *
 * @author tim wahrendorff
 */
class Notification {
    //put your code here
    
    /**
     * subject to be send with email/push notification
     * @var string 
     */
    private $subject = "";
    /**
     * message to be shown in notification, email and push
     * should be translateable 
     * @var string 
     */
    private $message = "";
    /**
     * params for message
     * @var string 
     */
    private $messageParams;
    /**
     * notification category
     * @var string 
     */
    private $category;
    /**
     *
     * @var string 
     */
    private $link;
    
    public function getSubject() {
        if($this->subject == ""){
            return $this->message;
        }
        return $this->subject;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getMessageParams() {
        return $this->messageParams;
    }

    public function getCategory() {
        return $this->category;
    }

    public function getLink() {
        return $this->link;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setMessageParams($messageParams) {
        $this->messageParams = $messageParams;
    }

    public function setCategory($category) {
        $this->category = $category;
    }

    public function setLink($link) {
        $this->link = $link;
    }

}
