<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: 
 * @package Helper
 */

/**
 * Cookie Helper for Cookie Management
 *
 * @package Helper
 */
class Cookie {

    private static function prefix(){
        $aConfig = Config::getConfig();
        return $aConfig['dbPrefix'];
    }
    /**
     *  Set a Cookie Value
     * @param mixed $key
     * @param mixed $value
     * @param boolean $overwrite
     * @param boolean $prefix
     */
    public static function set($key, $value, $overwrite = false, $prefix = true) {
        if (!self::isValid($key) || $overwrite) {
            if ($prefix) {

                // PHP 7.3 new signature with sameSite directive
                if(version_compare(PHP_VERSION, "7.3", '>=')){
                    setcookie(self::prefix() . $key,   $value, ['samesite' => 'Lax', 'expires' => time() + 315360000, 'path' => "/"]);
                } else{
                    setcookie(self::prefix() . $key, $value, time() + 315360000, "/");
                }
            } else {
                // PHP 7.3 new signature with sameSite directive
                if(version_compare(PHP_VERSION, "7.3", '>=')){
                    setcookie($key,   $value, ['samesite' => 'Lax', 'expires' => time() + 315360000, 'path' => "/"]);
                } else{
                    setcookie($key, $value, time() + 315360000, "/");
                }
            }
        }
    }

    /**
     *  Get a Cookie Value
     * @param mixed $key
     * @param boolean $prefix
     * @return mixed
     */
    public static function get($key, $prefix = true) {
        if ($prefix) {
            return filter_input(INPUT_COOKIE, self::prefix() . $key);
        } else {
            return filter_input(INPUT_COOKIE, $key);
        }
    }

    /**
     *  Delete a Cookie
     * @param mixed $key
     * @param boolean $prefix
     */
    public static function del($key, $prefix = true) {

        if ($prefix) {
            if (self::isValid($key)) {
                setcookie(self::prefix() . $key, self::get($key), time() - 3600, "/");
            }
        } else {
            if (self::isValid($key,false)) {
                setcookie($key, self::get($key,false), time() - 3600, "/");
            }
        }
    }

    /**
     *  Check if a Cookie is set
     * @param mixed $key
     * @param boolean $prefix
     * @return boolean
     */
    public static function isValid($key, $prefix = true) {
        if ($prefix) {
            return filter_has_var(INPUT_COOKIE, self::prefix() . $key);
        } else {
            return filter_has_var(INPUT_COOKIE, $key);
        }
    }

}
