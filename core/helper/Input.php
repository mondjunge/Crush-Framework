<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 * @package Helper
 */

/**
 * Input Helper class for escaping and sanitizing user input
 *
 * @package Helper
 */
class Input {

    /**
     *  check Strings for plausibility
     * @deprecated check input with checkInput
     * @param string $string
     * @param string $type - email or number
     * @return boolean
     */
    public static function check($string, $type) {
        switch ($type) {
            case "email":
                $sstring = preg_replace('/[^a-zA-Z0-9+_.@-]/i', '', $string);
                if ($sstring === $string) {
                    return true;
                } else {
                    return false;
                }
                break;

            case "number" :
                $sstring = preg_replace('/[^a-zA-Z+_.@-]/i', '', $string);
                if ($sstring === $string) {
                    return true;
                } else {
                    return false;
                }
                break;
//            case "string":
//                /**
//                 * min, max length of string
//                 */
//                $min = 1;
//                $max = 255;
//                if (isset($string)) {
//                    $string = preg_replace("/[^_.a-zA-Z0-9\&\?\/\é\ó\ú\+üöäß\_\-@]/", "", $string);
//                    $len = strlen($string);
//                    if ((($min != '') && ($len < $min)) || (($max != '') && ($len > $max)))
//                        return FALSE;
//                    return $string;
//                }
//                break;
            default:
                return true;
        }
    }

    /**
     * checks if the functon is a public function of the module and not a core function
     * 
     * @param String $string - name of the module function to be called
     * @return Bool - true if the functions is NOT a function of the main Controllers or false otherwise
     */
    public static function isValidFunctionCall($module,$function) {
        $controllerFunctionsArray = get_class_methods('AppController');
        $moduleFunctionsArray = get_class_methods(ucfirst($module));
        
        if (in_array($function, array_diff($moduleFunctionsArray,$controllerFunctionsArray))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Escapes the input string or string in input array if apache have not already
     * @param String $string
     * @return String escaped string
     * @deprecated
     */
    public static function escape($string) {
        if (is_array($string)) {
            $array = array();
            foreach ($string as $str) {
                $array[] = addslashes($str);
            }
            return $array;
        }
        
        return addslashes($string);
    }

    /**
     * @depricated use getSanitizedInput instead
     * sanitize user input to prevent malicious characters been send to the system
     * @param $string
     * @param $type takes: string, int, user, email, lang, site, default(0-9a-zA-Z only)
     * @return String
     * @deprecated
     */
    public static function sanitize($string, $type = "string") {
        switch ($type) {
            case "site":
                /**
                 * min, max length of string
                 */
                $min = 1;
                $max = 150;
                if (isset($string)) {
                    $string = preg_replace("/[^a-zA-Z0-9\_\-\.\~]/", "", $string);
                    $len = strlen($string);
                    if ((($min != '') && ($len < $min)) || (($max != '') && ($len > $max))) {
                        return FALSE;
                    }
                    return $string;
                }
                break;

            case "string":
                /**
                 * min, max length of string
                 */
                $min = 1;
                $max = 255;
                if (isset($string)) {
                    $string = preg_replace("/[^_.a-zA-Z0-9\&\?\/\é\ó\ú\+üöäß\_\-@]/", "", $string);
                    $len = strlen($string);
                    if ((($min != '') && ($len < $min)) || (($max != '') && ($len > $max))) {
                        return FALSE;
                    }
                    return $string;
                }
                break;
            case "int":
                /**
                 * min, max length of string
                 */
                $min = 1;
                $max = 11;
                if (isset($string)) {
                    $string = preg_replace("/[^0-9]/", "", $string);
                    $len = strlen($string);
                    if ((($min != '') && ($len < $min)) || (($max != '') && ($len > $max))) {
                        return substr($string, 0, 11);
                    }
                    return $string;
                }
                break;
            case "lang":
                /**
                 * min, max length of string
                 */
                $min = 1;
                $max = 5;
                if (isset($string)) {
                    $string = preg_replace("/[^_.a-zA-Z]/", "", $string);
                    $len = strlen($string);
                    if ((($min != '') && ($len < $min)) || (($max != '') && ($len > $max))) {
                        return FALSE;
                    }
                    return $string;
                }
                break;
            case "user":
                /**
                 * min, max length of string
                 */
                $min = 3;
                $max = 50;
                if (isset($string)) {
                    $string = preg_replace("/[^_.a-zA-Z0-9 \&\?\/\é\ó\ú\_\-\.üöäß\<\>+]/", "", $string);
                    $len = strlen($string);
                    if ((($min != '') && ($len < $min)) || (($max != '') && ($len > $max))) {
                        return FALSE;
                    }
                    return $string;
                }
                break;
            case "email":
                $emailarray = explode(';', $string);
                for ($i = 0; $i <= count($emailarray) - 1; $i++) {
                    $emailarray[$i] = preg_replace('/[^a-zA-Z0-9;+_.@-]/i', '', $emailarray[$i]);
                }
                return implode(';', $emailarray);
            default:
                /**
                 * min, max length of string
                 */
                $min = 1;
                $max = 150;
                if (isset($string)) {
                    $string = preg_replace("/[^_.a-zA-Z0-9 \/\é\_\-\.üöäß\<\>\+]/", "", $string);
                    $len = strlen($string);
                    if ((($min != '') && ($len < $min)) || (($max != '') && ($len > $max))) {
                        return FALSE;
                    }
                    return $string;
                }
                break;
        }
    }

}
