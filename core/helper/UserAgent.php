<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserAgent
 *
 * @author wahrendorff
 */
class ReadableUserAgent{
    public $deviceType;
    public $browser;
    public $os;
    public $useragent;
    public $fingerprint;
}

class UserAgent {
    
    public function getReadableUserAgent($uas) {
        $useragent = "";
        if (empty($uas)) {
            // this is a precaution measure agains null pointers
            // set a default user-agent
            $useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36";
        } else {
            $useragent = $uas;
        }

        $rua = new ReadableUserAgent();
        $rua->deviceType = $this->getDeviceTypeString(strtolower($useragent));
        $rua->browser = $this->getBrowserString($useragent);
        $rua->os = $this->getOsString($useragent);
        $rua->useragent = $useragent;
        $rua->fingerprint = hash('md5',Session::get('uid').$rua->deviceType.$rua->browser.$rua->os);
        
        return $rua;
    }

    /**
     * gets the form factor as readable String: Desktop or Mobile (Smartphone, Tablet, etc.)
     * @return Desktop or Mobile
     */
    private function getDeviceTypeString($ualc) {

        $form="";
        if (strpos($ualc, "ipad")!==FALSE
            || (strpos($ualc, "android")!==FALSE && !strpos($ualc, "mobile")!==FALSE)
            || strpos($ualc, "surface")!==FALSE) {

            $form = "useragent.tablet";

        } else if (strpos($ualc, "mobile")!==FALSE
                   || strpos($ualc, "iphone")!==FALSE
                   || strpos($ualc, "ipod")!==FALSE
                   || strpos($ualc, "android")!==FALSE
                   || strpos($ualc, "backberry")!==FALSE
                   || strpos($ualc, "playbook")!==FALSE
                   || strpos($ualc, "opera mobi")!==FALSE
                   || strpos($ualc, "windows phone")!==FALSE) {

            $form = "useragent.mobile";

        } else { // fallback
            $form = "useragent.desktop";
        }

        return $form;
    }

    /**
     * @return the os as readable String
     */
    private function getOsString($useragent) {

        $osName="";

        $start = strpos($useragent,'(');
        if($start == -1) {
            // crippled user agent
            return "useragent.os.unknown";
        }
        $end = strpos($useragent,')');
        $osString = substr($useragent,$start, $end);
        if (strpos($osString, "Windows Phone")!==FALSE) {
            $osName = "Windows Phone OS";
        } else if (strpos($osString, "Windows")!==FALSE) {
            $osName = "Windows";
        } else if (strpos($osString, "iPhone")!==FALSE) {
            $osName = "iPhone OS";
        } else if (strpos($osString, "iPad")!==FALSE) {
            $osName = "iPad OS";
        } else if (strpos($osString, "iPod")!==FALSE) {
            $osName = "iPod OS";
        } else if (strpos($osString, "Macintosh")!==FALSE) {
            $osName = "Mac OS";
        } else if (strpos($osString, "Android")!==FALSE) {
            $osName = "Android";
        } else if (strpos($osString, "BlackBerry")!==FALSE) {
            $osName = "BlackBerry";
        } else if (strpos($osString, "Linux")!==FALSE) {
            $osName = "Linux";
        } else { // fallback
            $osName = "useragent.os.unknown";
        }
        return $osName;
    }

    /**
     * @return the browser
     */
    private  function getBrowserString($useragent) {
        $browserName = "useragent.browser.unknown";
        $start = strpos($useragent,'(');
        if($start == -1) {
            // crippled user agent
            return "useragent.browser.unknown";
        }
        $ualc = strtolower($useragent);
        $browserString = substr($ualc,$start, strlen($useragent));

        if (strpos($browserString, "firefox")!==FALSE) {
            $browserName = "Firefox";
        } else if (strpos($browserString, "edge")!==FALSE) {
            $browserName = "Edge";
        } else if (strpos($browserString, "opr")!==FALSE) {
            $browserName = "Opera";
        } else if (strpos($browserString, "crios")!==FALSE) {
            // Chrome on iOS
            $browserName = "Chrome";
        } else if (strpos($browserString, "safari")!==FALSE
                   && (strpos($ualc, "iphone")!==FALSE
                       || strpos($ualc, "ipad")!==FALSE
                       || strpos($ualc, "ipod")!==FALSE
                       || strpos($ualc, "chromeframe")!==FALSE
                       || strpos($ualc, "macintosh")!==FALSE
                          && strpos($ualc, "chrome")===FALSE)) {
            $browserName = "Safari";
        } else if (strpos($browserString, "chrome")!==FALSE) {
            // must come after safari check or all safaris are detected as chrome
            $browserName = "Chrome";
        } 
        return $browserName;
    }
    
    
}

