<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: Session Management
 * @package Helper
 */

/**
 * Session Helper for Session Management and settin and getting Session
 * variables
 * 
 * needs to be initialized before using static functions. 
 * Is done in index.php, so no hassle to use in modules.
 * Usage in modules:  
 * Session::set($key,$value);
 *
 * @package Helper
 */
class Session {

    private static $_instance;
    private static $prefix;

    public static function init() {

        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
    }

    private function __clone() {
        // have to be empty
    }

    private function __construct() {
        $aConfig = Config::getConfig();
        self::$prefix = $aConfig['dbPrefix'];
        if (strlen(session_id()) === 0) {
            session_start();
        }
    }

    /**
     * Set a session variable
     * @param String $key
     * @param String $value
     */
    public static function set($key, $value, $overwrite = false, $usePrefix = true) {
        if ($usePrefix) {
            if (!self::isValid($key) || $overwrite) {
                $_SESSION[self::$prefix . $key] = $value;
            }
        } else {
            if (!self::isValid($key, false) || $overwrite) {
                $_SESSION[$key] = $value;
            }
        }
    }

    /**
     * Get a session variable
     * @param mixed $key
     * @return mixed Get a session Variable
     */
    public static function get($key, $usePrefix = true) {
        if ($usePrefix) {
            if (self::isValid($key)) {
                return $_SESSION[self::$prefix . $key];
            } else {
                return null;
            }
        } else {
            if (self::isValid($key, false)) {
                return $_SESSION[$key];
            } else {
                return null;
            }
        }
    }

    /**
     * Delete Session variable
     * @param mixed $key
     */
    public static function del($key, $usePrefix = true) {
        if ($usePrefix) {
            unset($_SESSION[self::$prefix . $key]);
        } else {
            unset($_SESSION[$key]);
        }
    }

    public static function isValid($key, $usePrefix = true) {
        //echo self::$prefix;
        if ($usePrefix) {
            if (isset($_SESSION[self::$prefix . $key])) {
                return true;
            } else {
                return false;
            }
        } else {
            if (isset($_SESSION[$key])) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function destroy() {
        $_SESSION = array();
        return;
    }

    public static function writeClose() {
        session_write_close();
    }

}
