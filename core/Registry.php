<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: stores global Objects and variables for use through the whole application logic
 */

/**
 * Class for storing global Objects and variables for use through the whole
 * application logic
 *
 * @package Util
 */
class Registry {

    private $store = array();
    private static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {
        // have to be empty
    }

    /**
     * Set Objects and variables for later use in other classes/objects
     * @param String $key
     * @param Mixed $value
     * @param Bool $overwrite
     */
    public function set($key, $value, $overwrite=false) {
        if (!$this->isValid($key) || $overwrite) {
            $this->store[$key] = $value;
        } else {
            throw new Exception("Registry: key is already set");
        }
    }

    /**
     * Returns a stored Object
     * @param String $key
     * @return Object or null
     */
    public function get($key) {
        if ($this->isValid($key)) {
            return $this->store[$key];
        } else {
            return null;
        }
    }

    /**
     *
     * @param String $key
     * @return Bool true if the key exists, false if not
     */
    public function isValid($key) {
        return array_key_exists($key, $this->store);
    }

}