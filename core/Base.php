<?php

/**
 *  Copyright © tim 12.08.2017
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */

/**
 * Static functions for basic application logic called from index.php
 * @package Controller
 */
class Base {

    /**
     * autoloads core classes, Modules and Theme class.
     * @param type $className
     */
    public static function autoload($className) {
        if (is_file("core/$className.php")) {
            require_once "core/$className.php";
        } else if (is_file("core/helper/$className.php")) {
            require_once "core/helper/$className.php";
        } else if (is_file("core/theming/$className.php")) {
            require_once "core/theming/$className.php";
        } else if (is_file("core/interfaces/$className.php")) {
            require_once "core/interfaces/$className.php";
        } else if (is_file("core/database/$className.php")) {
            require_once "core/database/$className.php";
        } else if ($className == "Theme" && is_file('theme/' . Config::get('siteStyle') . '/Theme.php')) {
            require_once 'theme/' . Config::get('siteStyle') . '/Theme.php';
        } else if ($className == "Theme") {
            require_once 'theme/Theme.php';
        } else if (is_file(Config::get('applicationDirectory') . "/" . strtolower($className) . "/$className.php")) {
            require_once Config::get('applicationDirectory') . "/" . strtolower($className) . "/$className.php";
        }
    }

    /**
     * run maintinanace jobs from a cronjob (Free Service: cron-job.org)
     * exits with header 200 or 500
     */
    public static function doMaintainance() {
        $status = '200';
        $directory = Config::get('applicationDirectory');
        $modules = scandir($directory);
        $result = array();
        foreach ($modules as $module) {
            $modDir = $directory . '/' . $module;
            $modClass = ucfirst($module);
            if (is_dir($modDir) && is_file($modDir . '/' . $modClass . '.php') && substr($module, 0, 1) !== '.') {
                include_once $modDir . '/' . $modClass . '.php';
                $modFunctions = get_class_methods($modClass);
                if (in_array('runMaintainance', $modFunctions)) {
                    $m = new $modClass();
                    $result[] = $m->runMaintainance();
                }
            }
        }
        foreach ($result as $r) {
            if ($r === FALSE) {
                $status = '500';
            }
        }
        if ($status == '200') {
            header("Status: $status");
            header('Content-Type: application/json');
            echo json_encode("success");
        } else {
            header("Status: $status");
            header('Content-Type: application/json');
            echo json_encode("error");
        }

        Session::writeClose();
        exit;
    }

    /**
     * 
     */
    public static function minifyCss() {
        if (filter_has_var(INPUT_GET, 'css')) {
            $css = trim(filter_input(INPUT_GET, 'css'));
            if (!empty($css)) {
                $o = new Output();
                $o->setActiveModule($css);
                header("Content-type: text/css");
                self::setSecurityHeaders();
                self::setCacheHeader(true);
                echo $o->getCssIncludes();
                exit;
            }
        }
    }

    /**
     * 
     */
    public static function minifyJs() {
        if (filter_has_var(INPUT_GET, 'js')) {
            $js = trim(filter_input(INPUT_GET,'js'));
            if (!empty($js)) {
                header("Content-type: text/javascript");
                self::setSecurityHeaders();
                self::setCacheHeader(true);
                $o = new Output();
                echo $o->getJsIncludes($js);
                exit;
            }
        }
    }

    /**
     * 
     * @global array $aConfig
     * @return string
     */
    public static function handleLanguage($aConfig) {
        if (filter_has_var(INPUT_GET, 'lang') && filter_input(INPUT_GET, 'lang') != '') {
            $language = filter_input(INPUT_GET, 'lang');
            if (!in_array($language, $aConfig['enabledLanguages'])) {
                //default settings for intruders
                $language = $aConfig['defaultLanguage'];
                Cookie::set('language', $language, true);
            }
            Cookie::set('language', $language, true);
        } else if (Cookie::isValid('language')) {
            $language = Cookie::get('language');
        } else {
            $language = $aConfig['defaultLanguage'];
            Cookie::set('language', $language, true);
        }

        Config::addConfigKey('reLangUrl', $aConfig['relativeUrl'] . $language . '/');

        return $language;
    }

    /**
     * 
     * @return \AppController
     */
    public static function initController() {
        $oController = new Controller();

        /**
         * check for resident cookie and log in if needed
         */
        if (!$oController->isLoggedIn()) {
            $oController->checkResident();
        }
        return $oController;
    }

    /**
     * TODO: refactor
     * @param Controller $oController
     * @param array $aConfig
     * @param string $language
     * @return \moduleClass
     */
    public static function handleModuleSelection($oController, $aConfig, $language) {
        if (filter_has_var(INPUT_GET, 'site') && filter_input(INPUT_GET, 'site') != '') {

            $moduleName = Input::sanitize(filter_input(INPUT_GET, 'site'), 'site');
            $moduleClass = ucfirst($moduleName);

            if (filter_has_var(INPUT_GET, 'function') && filter_input(INPUT_GET, 'function') != '' && Input::isValidFunctionCall($moduleClass, filter_input(INPUT_GET, 'function'))) {
                $function = Input::sanitize(filter_input(INPUT_GET, 'function'), 'site');
            } else {
                $function = 'index';
            }

            if (!is_file('application/' . $moduleName . '/' . $moduleClass . '.php')) {
                if ($oController->isLoggedIn()) {
                    Link::jumpTo($aConfig['defaultControllerAfterLogin']);
                } else {
                    Link::jumpTo($aConfig['defaultController']);
                }
                $moduleName = $moduleArray[0];
                $moduleClass = ucfirst($moduleArray[0]);
                if (isset($moduleArray[1])) {
                    $function = $moduleArray[1];
                } else {
                    $function = 'index';
                }
            }
        } else {
            if ($oController->isLoggedIn()) {
                $moduleArray = explode('/', $aConfig['defaultControllerAfterLogin']);
            } else {
                $moduleArray = explode('/', $aConfig['defaultController']);
            }
            $moduleName = $moduleArray[0];
            $moduleClass = ucfirst($moduleArray[0]);
            if (isset($moduleArray[1])) {
                $function = $moduleArray[1];
            } else {
                $function = 'index';
            }
        }

        // fallback to login (it is always there and accessible
        if ($moduleName == "" || $moduleClass == "") {
            $moduleName = "login";
            $moduleClass = "Login";
            $function = "index";
        }

        /**
         * check if System is installed and jump to installer if not
         */
        if ($moduleName !== 'install' && !$oController->isInstalled()) {
            if ($oController->isLoggedIn()) {
                $oController->logout();
            }
            Link::jumpTo('install');
        }

        /**
         * load and process module
         */
        $modulePath = $aConfig['applicationDirectory'] . '/' . $moduleName . '/';

        $oController->setActiveModule($moduleName);
        $oController->setActiveFunction($function);
        $oController->setLanguage($language);
        $oController->setTemplatePath($modulePath . 'view/');
        $oController->loadLang($modulePath, $language);
        $oController->loadModuleConfiguration($moduleName);

        return new $moduleClass();
    }

    /**
     * 
     * @param type $module
     * @return string
     */
    public static function getModuleContent($module) {
        ob_start();
        call_user_func(array($module, $module->getActiveFunction()));
        $moduleEchos = ob_get_contents();
        ob_end_clean();

        if ($moduleEchos != '') {
            $content = $moduleEchos;
            $content .= $module->getContent();
        } else {
            $content = $module->getContent();
        }
        return $content;
    }

    /**
     * 
     * @param type $data
     * @param type $module
     * @return array
     */
    public static function getMessages($data, $module) {
        $data['errorMessages'] = $module->getErrorMessages();
        $data['systemMessages'] = $module->getSystemMessages();
        $data['debugMessages'] = $module->getDebugMessages();

        if (Session::isValid('errorMessages', false)) {
            $data['errorMessages'] = array_merge($data['errorMessages'], Session::get('errorMessages', false));
            Session::del('errorMessages', false);
        }
        if (Session::isValid('systemMessages', false)) {
            $data['systemMessages'] = array_merge($data['systemMessages'], Session::get('systemMessages', false));
            Session::del('systemMessages', false);
        }
        if (Session::isValid('debugMessages', false)) {
            $data['debugMessages'] = array_merge($data['debugMessages'], Session::get('debugMessages', false));
            Session::del('debugMessages', false);
        }

        return $data;
    }

    /**
     * also sets a Cache Header.
     */
    public static function setSecurityHeaders() {
        /* TODO make security headers configurable in settings. */
        header("Access-Control-Allow-Origin: *");
        header("X-Xss-Protection: 1; mode=block");
        header("X-Frame-Options: SAMEORIGIN");
        header("X-Content-Type-Options: nosniff");
        header("Strict-Transport-Security: \"max-age=31536000; includeSubDomains\"");
        header("Referrer-Policy: strict-origin-when-cross-origin");
    }

    public static function setCacheHeader($mustRevalidate = false, $maxAge = "31536000") {
        /* TODO make cache control configurable in settings. */
        if ($mustRevalidate) {
            header("Cache-Control: max-age=$maxAge, must-revalidate");
        } else {
            header("Cache-Control: max-age=$maxAge");
        }
    }

}
