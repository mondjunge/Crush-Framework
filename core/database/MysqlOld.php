<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: Class utilizing improved mysql functions
 * @package Model
 */

/**
 * Database Class for improved mysql connection
 *
 * @package Model
 */
class MysqlOld implements IDatabase {

    private $dbType;
    private $dbServer;
    private $dbLogin;
    private $dbPassword;
    private $dbName;
    private $dbPrefix;
    private $dbConnection;
    private $debugMode;
    private $_message;
    private static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {
        // have to be empty
    }

    /**
     * establish Databaseconnection
     */
    private function __construct() {
        $aConfig = Config::getConfig();
        $this->dbType = $aConfig['dbType'];
        $this->dbServer = $aConfig['dbServer'];
        $this->dbLogin = $aConfig['dbLogin'];
        $this->dbPassword = $aConfig['dbPassword'];
        $this->dbName = $aConfig['dbName'];
        $this->dbPrefix = $aConfig['dbPrefix'];
        $this->debugMode = $aConfig['debugMode'];

        try {
            $this->connectDbServer();
        } catch (Exception $ex) {
            $this->getMessage()->setErrorMessage($ex->getMessage());
            return;
        }
        if ($this->dbExists()) {
            try {
                $this->selectDb();
            } catch (Exception $ex) {
                $this->getMessage()->setErrorMessage($ex->getMessage());
            }
        }

    }

    private function connectDbServer() {
        if (!$this->dbConnection = mysqli_connect($this->dbServer, $this->dbLogin, $this->dbPassword)) {
            throw new Exception("Database: Error while connecting to db server. " . mysqli_connect_errno() . ": " . mysqli_connect_error());
        } 
    }

    private function selectDb() {
        if (!mysqli_select_db($this->dbConnection, $this->dbName)) {
            throw new Exception("Database: Error while selecting database.");
        } else {
            $this->query("SET NAMES 'utf8mb4'");
        }
    }

    /**
     * function to determine if the configured database exists 
     * @return boolean 
     */
    public function dbExists(): bool {

        try {
            $this->selectDb();
            return true;
        } catch (Exception $ex) {
            //$this->getMessage()->setErrorMessage($ex);
            return false;
        }

    }

    /**
     * execute a query on the database
     * @param string $query
     * @param boolean $array - return rows as array on select query
     * @return bool true or false when inserting, deleting updating or resultset when selecting
     */
    public function query($query, $array = false) {
        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($query);
        }
        $result = mysqli_query($this->dbConnection, $query);

        if ($array) {
            $result = mysqli_fetch_array($result, MYSQLI_ASSOC);
        }

        return $result;

    }

    /**
     *  counts rows of a table
     * @param string $table
     * @param string $where
     * @return int
     */
    public function count($table, $where = '') {
        // SELECT COUNT(*) FROM sometable WHERE number=7997;
        if ($where != '') {
            $where = 'WHERE ' . $where;
        }

        $sql = "SELECT id FROM `$this->dbPrefix$table` $where";

        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }
        $rs = mysqli_query($this->dbConnection, $sql);
        if ($rs === FALSE) {
            return 0;
        }
        return mysqli_num_rows($rs);
    }

    /**
     *  checks if a table exists
     * @param type $table
     * @return boolean
     */
    public function tableExists($table): bool {
        $sql = "SHOW TABLES LIKE '".$this->dbPrefix . $table."'";
        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }

        $rs = mysqli_query($this->dbConnection, $sql);
        //$this->getMessage()->setDebugMessage(print_r(mysqli_num_rows($rs)));
        if ($rs === FALSE || mysqli_num_rows($rs) === 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * returning an array of selected rows.
     * @param string $table
     * @param string $fields
     * @param string $where
     * @param string $orderBy
     * @param string $limit
     * @return array $results
     */
    public function select($table, $fields = '*', $where = '', $orderBy = '', $limit = '') {

        /**
         * build the query
         */
        if ($orderBy != '') {
            $orderBy = 'ORDER BY ' . $orderBy;
        }

        if ($where != '') {
            $where = 'WHERE ' . $where;
        }

        if ($limit != '') {
            $limit = 'LIMIT ' . $limit;
        }

        $sql = "SELECT ". $fields. " FROM ". $this->dbPrefix.$table." ".$where." ".$orderBy." ".$limit;
        $sql = trim($sql).";";
        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }

        /**
         * build the array
         */
        $rs = mysqli_query($this->dbConnection, $sql);
        $resultArray = array();

        if ($rs === FALSE) {
            /**
             * throw exception if no resultset came back
             */
            //$this->getMessage()->setErrorMessage("Database: Error while selecting data.");
            $errormsg = "Database: Error selecting data.";
            //if ($this->debugMode)
            $errormsg .= " (" . mysqli_error($this->dbConnection) . ")";
            throw new Exception($errormsg);
        } else {
            /**
             * return result as array
             */
            foreach (mysqli_fetch_assoc($rs) as $row) {
                print_r($row);
                $resultArray[] = $row;
            }
            return $resultArray;
        }
    }

    /**
     *  insert a new row to the db and returns the new id
     * @param string $table
     * @param array $fieldsValueArray
     * @return int
     * @throws Exception
     */
    public function insert($table, $fieldsValueArray) {
        //  $sql = "INSERT INTO $this->dbPrefix$table ($fields) VALUES ($values); ";
        //($fields) VALUES ($values); ";
        $keys = '';
        $values = '';
        foreach ($fieldsValueArray as $key => $value) {
            if ($keys == '') {
                $keys = "`" . $key . "`";
            } else {
                $keys .= ",`" . $key . "`";
            }

            /**
             * TODO:
             * - escape value
             * - add quotes if: not null, not date?
             */
            if ($values == '') {
                $values = $this->addInsertQuotesAndEscapeValue($value);
            } else {
                $values .= "," . $this->addInsertQuotesAndEscapeValue($value);
            }
        }
        $sql = "INSERT INTO $this->dbPrefix$table (" . $keys . ") VALUES (" . $values . ");";
        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }

        if (mysqli_query($this->dbConnection, $sql)) {
            return mysqli_insert_id($this->dbConnection);
        } else {
            $errormsg = "Database: Error inserting data.";
            if ($this->debugMode) {
                $errormsg .= " (" . mysqli_error($this->dbConnection) . ")";
            }
            throw new Exception($errormsg);
        }
    }
    
    private function addInsertQuotesAndEscapeValue($value, $quoteStyle="'"){
        /*
         * Do not add Quotes if value is
         * not a string
         * null
         */
        // remove trailing quotes if any
        $value = trim($value,$quoteStyle);
        
        //escape and quote value if string and not null
        if(!is_string($value)
            || strtolower($value) == "null"){
            return $this->escapeString($value);
        } else {
            return $quoteStyle.$this->escapeString($value).$quoteStyle;
        }
    }

    /**
     * Updates datasets
     * @param string $table
     * @param string $fieldsValueArray
     * @param string $where
     * @param string $limit
     * @return boolean true
     * @throws Exception
     */
    public function update($table, $fieldsValueArray, $where, $limit = ''): bool {
        if ($limit != '') {
            $limit = 'LIMIT ' . $limit;
        }

        $expression = '';
        // while (list($key, $value) = each($fieldsValueArray)) {
        foreach ($fieldsValueArray as $key => $value) {
            if (strtolower($value) != "null") {
                $value = "'" . $this->escapeString($value) . "'";
            }

            if ($expression == '') {
                $expression .= $key . "=" . $value . " ";
            } else {
                $expression .= " ," . $key . "=" . $value . " ";
            }
        }
        $sql = "UPDATE $this->dbPrefix$table ";
        $sql .= " SET ";
        $sql .= $expression;
        $sql .= " WHERE $where $limit";

        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }

        if ($this->query($sql)) {
            return true;
        } else {
            $errormsg = "Database: Error updating data.";
            if ($this->debugMode) {
                $errormsg .= " (" . mysqli_error($this->dbConnection) . ")";
            }
            throw new Exception($errormsg);
            // $this->getMessage()->setErrorMessage("Database: Error while updating data.");
        }
    }
    
    /**
     * deletes defined rows
     * @param string $table
     * @param string $where
     * @return boolean true
     * @throws Exception
     */
    public function delete($table, $where): bool {
        $sql = "DELETE FROM $this->dbPrefix$table WHERE $where ";

        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }

        if (mysqli_query($this->dbConnection, $sql)) {
            return true;
        } else {
            $errormsg = "Database: Error deleting data.";
            if ($this->debugMode) {
                $errormsg .= " (" . mysqli_error($this->dbConnection) . ")";
            }
            throw new Exception($errormsg);
        }
    }

    /**
     * escapes Strings to be used without harm in sql statements
     * @param string $string
     * @return string
     */
    public function escapeString($string): string {
        return mysqli_escape_string($this->dbConnection, $string);
    }

    private function getMessage() {
        try {
            return Message::getInstance();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
