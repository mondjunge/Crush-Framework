<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: Class utilizing pdo mysql functions
 * @package Model
 */

/**
 * Database Class for pdo mysql connection
 *
 * @package Model
 */
class Mysql implements IDatabase {

    private $dbType;
    private $dbServer;
    private $dbLogin;
    private $dbPassword;
    private $dbName;
    private $dbPrefix;
    private $dbConnection;
    private $pdo;
    private $debugMode;
    private $_message;
    private static $_instance;

    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {
        // have to be empty
    }

    /**
     * establish Databaseconnection
     */
    private function __construct() {
        $aConfig = Config::getConfig();
        $this->dbType = $aConfig['dbType'];
        $this->dbServer = $aConfig['dbServer'];
        $this->dbLogin = $aConfig['dbLogin'];
        $this->dbPassword = $aConfig['dbPassword'];
        $this->dbName = $aConfig['dbName'];
        $this->dbPrefix = $aConfig['dbPrefix'];
        $this->debugMode = $aConfig['debugMode'];

         try {
            $this->pdo = new PDO('mysql:host='.$this->dbServer.';dbname=INFORMATION_SCHEMA', $this->dbLogin, $this->dbPassword);
        } catch (Exception $ex) {
            $this->getMessage()->setErrorMessage($ex->getMessage());
            return;
        }
        if ($this->dbExists()) {
            try {
                $this->pdo = new PDO('mysql:host='.$this->dbServer.';dbname='.$this->dbName.';charset=utf8mb4', $this->dbLogin, $this->dbPassword);
                $this->setCollation();
            } catch (Exception $ex) {
                $this->getMessage()->setErrorMessage($ex->getMessage());
            }
        } else {
            //$this->getMessage()->setErrorMessage("Datenbank existiert nicht");
        }

    }
    
    private function setCollation(): void{
        //utf8mb4_0900_ai_ci
        $this->pdo->exec("SET NAMES 'utf8mb4' COLLATE '".Config::get('dbCollation', 'utf8mb4_general_ci')."'");
    }
    
    public function dbExists(): bool {
        $statement = $this->pdo->prepare("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = ?");
        $statement->execute(array($this->dbName)); 
        return ($statement->rowCount() > 0);
    }

    /**
     * execute a query on the database
     * @param string $query
     * @param boolean $array - return rows as array on select query
     * @return bool true or false when inserting, deleting updating or resultset when selecting
     */
    public function query($query, $array = false) {
        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($query);
        }
        
        $result = $this->pdo->query($query);
        if($array){
            return $result->fetchAll(\PDO::FETCH_BOTH);
        } else {
            return $result;
        }
        
    }

    /**
     *  counts rows of a table
     * @param string $table
     * @param string $where
     * @return int
     */
    public function count($table, $where = '') {
        $sql = "SELECT count(*) FROM `$this->dbPrefix$table` ";
        if($where!=''){
           $sql =  "SELECT count(*) FROM `$this->dbPrefix$table` WHERE $where";
        }
        
        $statement = $this->pdo->query($sql);
        return $statement->fetchColumn(); 
    }

    /**
     *  checks if a table exists
     * @param type $table
     * @return boolean
     */
    public function tableExists($table): bool {
        $sql = "SHOW TABLES LIKE ?";
        

        $statement = $this->pdo->prepare($sql);
        $statement->execute(array($this->dbPrefix.$table)); 
        $rowCount = $statement->rowCount();
        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage(str_replace("?", $this->dbPrefix.$table, $sql)." (".$rowCount.")");
        }
        
        return ( $rowCount > 0);
    }

    public function selectPrepare($table, $fields = '*', $wherePart = '', $params = array()) {
        if ($wherePart != '') {
            $where = 'WHERE ' . $wherePart;
        }
        
        $sql = "SELECT ". $fields. " FROM ". $this->dbPrefix.$table." ".$where;
        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }
        $prep = $this->prepare($sql);
            
        $prep->execute($params);
        if(true){ //TODO
            return $prep->fetchAll(\PDO::FETCH_BOTH);
            //return $prep->fetchAll(\PDO::FETCH_BOTH);
        } else {
            return $result;
        }
    }
    /**
     * returning an array of selected rows.
     * @param string $table
     * @param string $fields
     * @param string $where
     * @param string $orderBy
     * @param string $limit
     * @return array $results
     * @deprected lagacy use selectPrepare
     */
    public function select($table, $fields = '*', $where = '', $orderBy = '', $limit = '') {

        /**
         * build the query
         */
        if ($orderBy != '') {
            $orderBy = 'ORDER BY ' . $orderBy;
        }

        if ($where != '') {
            $where = 'WHERE ' . $where;
        }

        if ($limit != '') {
            $limit = 'LIMIT ' . $limit;
        }
//        foreach ($fields as $f){
//            
//        }

        $sql = "SELECT ". $fields. " FROM ". $this->dbPrefix.$table." ".$where." ".$orderBy." ".$limit;
        $sql = trim($sql).";";
        
//        if ($this->debugMode) {
//            $this->getMessage()->setDebugMessage($sql);
//        }
        
//        if($table == 'site_data'){
//            $rs = $this->query($sql, true);
//            print_r($rs);
//            print_r($sql); 
//            //exit;
//        }
        
        return $this->query($sql, true);
        
//        $rs = $this->pdo->query($sql);
//        $resultArray = array();
//       
//        if ($rs === FALSE) {
//            /**
//             * throw exception if no resultset came back
//             */
//            //$this->getMessage()->setErrorMessage("Database: Error while selecting data.");
//            $errormsg = "Database: Error selecting data.";
//            //if ($this->debugMode)
//            $errormsg .= " (" . mysqli_error($this->dbConnection) . ")";
//            throw new Exception($errormsg);
//        } else {
//            /**
//             * return result as array
//             */
//             print_r($rs); //exit;
////            foreach ($rs->fetchAll(\PDO::FETCH_ASSOC) as $row) {
////                //print_r($row);
////                $resultArray[] = $row;
////            }
//            return $rs->fetchAll(\PDO::FETCH_BOTH);//$resultArray;
//        }
    }

    /**
     *  insert a new row to the db and returns the new id
     * @param string $table
     * @param array $fieldsValueArray
     * @return int
     * @throws Exception
     */
    public function insert($table, $fieldsValueArray) {
        $keys = '';
        $values = '';
        $params = [];
        foreach ($fieldsValueArray as $key => $value) {
            if ($keys == '') {
                $keys = "`" . $key . "`";
            } else {
                $keys .= ",`" . $key . "`";
            }

            if ($values == '') {
                $values = ":".$key;
            } else {
                $values .= ",:" . $key;
            }
            $params[$key] = $value;
        }
        $sql = "INSERT INTO $this->dbPrefix$table (" . $keys . ") VALUES (" . $values . ");";
        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }
        //if(!$this->pdo->query($sql)){
        $prep = $this->prepare($sql);
            
        if (!$prep->execute($params)) {
            $errormsg = "Database: Error inserting data.";
            if ($this->debugMode) {
                $errormsg .= $this->pdo->errorInfo()." (". $sql . ")";
            }
            throw new Exception($errormsg);
        }
        return $this->pdo->lastInsertId();
    }
    /**
     * 
     * @param type $value
     * @param type $quoteStyle
     * @return type
     * @deprecated
     */
    private function addInsertQuotesAndEscapeValue($value, $quoteStyle="'"){
       // return $value;
        /*
         * Do not add Quotes if value is
         * not a string
         * null
         */
        // remove trailing quotes if any
        $value = trim($value,$quoteStyle);
        
        //escape and quote value if string and not null
//        if(!is_string($value)
//            || strtolower($value) == "null"){
//            return $this->escapeString($value);
//        } else {
//            return $quoteStyle.$this->escapeString($value).$quoteStyle;
//        }
        
        return $this->escapeString($value);
    }

    public function updatePrepare($table, $fieldsValueArray, $wherePart, $whereParams = []): bool {
        $expression = '';
        $params = [];
        foreach ($fieldsValueArray as $key => $value) {

            if ($expression != '') {
                $expression .= ", ";
            } 
            $expression .= "`$key`" . "=:" . $key . "";
            $params[$key] = $value;
        }
        $sql = "UPDATE $this->dbPrefix$table ";
        $sql .= " SET ";
        $sql .= $expression;
        $sql .= " WHERE $wherePart ;";

        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }
        $prep = $this->prepare($sql);
        if ($prep->execute(array_merge($params,$whereParams))) {
            return true;
        } else {
            $errormsg = "Database: Error updating data.";
            throw new Exception($errormsg);
            // $this->getMessage()->setErrorMessage("Database: Error while updating data.");
        }
    }
    /**
     * Updates datasets
     * @param string $table
     * @param string $fieldsValueArray
     * @param string $where
     * @param string $limit
     * @return boolean true
     * @throws Exception
     */
    public function update($table, $fieldsValueArray, $where, $limit = ''): bool {
        if ($limit != '') {
            $limit = 'LIMIT ' . $limit;
        }

        $expression = '';
        $params = [];
//        foreach ($fieldsValueArray as $key => $value) {
//            if (strtolower($value) != "null") {
//                $value =  $this->escapeString($value);
//            }
//
//            if ($expression == '') {
//                $expression .= "`$key`" . "=" . $value . "";
//            } else {
//                $expression .= ", " . "`$key`" . "=" . $value . "";
//            }
//        }
        foreach ($fieldsValueArray as $key => $value) {

            if ($expression != '') {
                $expression .= ", ";
            } 
            $expression .= "`$key`" . "=:" . $key . "";
            $params[$key] = $value;
        }
        /**
        * TODO:
        * - prepare parameters
        * - escaping via filter_input or htmlentities/-specialchars etc. in module, where appropriate 
        */
        $sql = "UPDATE $this->dbPrefix$table ";
        $sql .= " SET ";
        $sql .= $expression;
        $sql .= " WHERE $where $limit;";

        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }
        $prep = $this->prepare($sql);
            
        if ($prep->execute($params)) {
            return true;
        } else {
            $errormsg = "Database: Error updating data.";
            throw new Exception($errormsg);
            // $this->getMessage()->setErrorMessage("Database: Error while updating data.");
        }
    }
    
    /**
     * deletes defined rows
     * @param string $table
     * @param string $where
     * @return boolean true
     * @throws Exception
     */
    public function delete($table, $where): bool {
        $sql = "DELETE FROM $this->dbPrefix$table WHERE $where ";

        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }
        if($this->pdo->query($sql)){
            return true;
        } else {
            $errormsg = "Database: Error deleting data.";
            throw new Exception($errormsg);
        }
    }
    
    public function deletePrepare($table, $wherePart, $params = array()): bool {
        $sql = "DELETE FROM $this->dbPrefix$table WHERE $wherePart ";

        if ($this->debugMode) {
            $this->getMessage()->setDebugMessage($sql);
        }
        $prep = $this->prepare($sql);
            
        if ($prep->execute($params)) {
            return true;
        } else {
            $errormsg = "Database: Error deleting data.";
            throw new Exception($errormsg);
        }
    }

    /**
     * escapes Strings to be used without harm in sql statements
     * @param string $string
     * @return string
     */
    public function escapeString($string): string {
        return $this->pdo->quote($string);
    }
    
    /**
     * prepares a pdo query
     * @param type $sql
     * @param type $options
     * @return type
     */
    public function prepare($sql, $options=array()){
        return $this->pdo->prepare($sql, $options);
    }

    private function getMessage() {
        try {
            return Message::getInstance();
        } catch (Exception $e) {
            throw $e;
        }
    }

}
