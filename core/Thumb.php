<?php

/**
 *  Copyright © wahrendorff 22.06.2020
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
class Thumb {

    private static $version = "1.7.20";

    public static function Instance() {
        static $inst = null;
        if ($inst === null) {
            //self::includeLibs();
            $inst = new Thumb();
        }
        return $inst;
    }

    public function createThumbnail($src, $width, $height, $cacheBaseDir = '') {

        if (!is_file($src)) {
            print_r($src);
            // no large error texts on missing images.
            return "";
        }
        /**
         * create output filename/path
         */
        if ($cacheBaseDir == '') {
            $cacheBaseDir = dirname(__FILE__) . '/../cache/thumbs/';
        }
        $imgExt = $this->get_file_extension($src);
        $newfilename = md5($src . "-" . $width . "x" . $height) . "." . $imgExt;
        $output_filename = $cacheBaseDir . $newfilename;

        /* return path without generating, if file exists and src is not newer than cache */
        if (is_file($output_filename) && filemtime($src) < filemtime($output_filename)) {
            return $newfilename;
        }

        if (strtolower($imgExt) != 'gif' && strtolower($imgExt) != 'jpg' && strtolower($imgExt) != 'jpeg' && strtolower($imgExt) != 'png') {
            return 'src/' . basename($src);
        }
        $this->resizeWithGd($src, $output_filename, $width, $height);

        return $newfilename;
        //old
        //return;
//        $phpThumb = new phpThumb();
//        if (Config::get('debugMode')) {
//            $phpThumb->setParameter('config_disable_debug', false);
//        } else {
//            $phpThumb->setParameter('config_disable_debug', true);
//        }
//        $thumbnail_width = $width;
//        $thumbnail_height = $height;
//
//        /* set data source -- do this first, any settings must be made AFTER this call */
//        $phpThumb->setSourceData(file_get_contents($src));



        /* PLEASE NOTE:
          You must set any relevant config settings here. The phpThumb
          object mode does NOT pull any settings from phpThumb.config.php */

//        $phpThumb->setParameter('config_document_root', dirname(__FILE__) . '/../');
//        $phpThumb->setParameter('config_cache_directory', dirname(__FILE__) . '/../cache/thumbs/');
//        $phpThumb->setParameter('config_temp_directory', dirname(__FILE__) . '/../cache/thumbs/src/');
//
//        $phpThumb->setParameter('config_prefer_imagemagick', true);
//        $phpThumb->setParameter('config_imagemagick_use_thumbnail', false);
//
//        /* set watermark for animated gifs (they must be clicked for the animation to play. */
//        if ($imgExt == 'gif') {
//            $phpThumb->setParameter('fltr', 'wmt|animated gif, click to play|3|TL|000000||80|5|0|ffffff|80');
//        }
//
//        //        $phpThumb->setCacheDirectory(Config::get('relativeUrl')."cache");
//        /* set parameters (see "URL Parameters" in phpthumb.readme.txt) */
//        if ($thumbnail_width != '') {
//            $phpThumb->setParameter('w', $thumbnail_width);
//        }
//        if ($thumbnail_height != '') {
//            $phpThumb->setParameter('h', $thumbnail_height);
//        }
//        $phpThumb->setParameter('config_output_format', $imgExt);
//
//        //keep EXIF orientation "auto-rotate"
//        $phpThumb->setParameter('ar', 'x');
//        //$phpThumb->setParameter('fltr', 'gam|1.2');
//        //$phpThumb->setParameter('fltr', 'wmi|../watermark.jpg|C|75|20|20');
//        /* generate & output thumbnail */
//        if ($phpThumb->GenerateThumbnail()) { // this line is VERY important, do not remove it!
//            if ($phpThumb->RenderToFile($output_filename)) {
//                // do something on success
//            } else if (Config::get('debugMode')) {
//                // do something with debug/error messages
//                echo 'Failed:(' . $src . ')<pre>' . implode("\n\n", $phpThumb->debugmessages) . '</pre>';
//            }
//            $phpThumb->purgeTempFiles();
//        } else if (Config::get('debugMode')) {
//            // do something with debug/error messages
//            echo 'Failed:(' . $src . ')<pre>' . $phpThumb->fatalerror . "\n\n" . implode("\n\n", $phpThumb->debugmessages) . '</pre>';
//        }
//        return $newfilename;
    }

    public function resizeImage($sourcePath, $destinationPath, $width, $height, $ext):void {
        resizeWithGd($sourcePath, $destinationPath, $width, $height);
//        if (class_exists('Imagick')) {
//            // Imagick verwenden, wenn verfügbar
//            $imagick = new Imagick($sourcePath);
//            $imagick->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1);
//            $imagick->writeImage($destinationPath);
//            $imagick->clear();
//            $imagick->destroy();
//        } elseif (function_exists('imagecreatefromjpeg')) {
//            // Fallback auf GD
//            resizeWithGd($sourcePath, $destinationPath, $width, $height);
//        } else {
//            throw new Exception('Weder Imagick noch GD sind verfügbar.');
//        }
    }

    private function resizeWithGd($src, $dest, $maxW=5000, $maxH=5000) :void{
        if ($maxW == '' || $maxW == 0 || $maxW == null){
            $maxW=5000;
        }
        if ($maxH == '' || $maxH== 0 || $maxH== null){
            $maxH=5000;
        }
        // ext enkodn
        $ext = strtolower(pathinfo($src, PATHINFO_EXTENSION));
        
         if ($ext == 'jpg') {
            // exif data is only available in jpg files.
            $exif = exif_read_data($src);
        }
        
        // bild einlesen
        switch ($ext) {
            case 'png':
                $img = imagecreatefrompng($src); // kd
                break;
            case 'gif':
                $img = imagecreatefromgif($src); // kd
                break;
            default:
                $img = @imagecreatefromjpeg($src); // ...
                if (!$img) {
                  error_log("Konnte kein gültiges Image aus $src erzeugen");
                  return;
}
                break;
        }
        
        // orientierung fix für jpg
        if (isset($exif['Orientation'])) {
            switch($exif['Orientation']) {
                case 3: // 180°
                    $img = imagerotate($img, 180, 0);
                    break;
                case 6: // 90° cw
                    $img = imagerotate($img, -90, 0);
                    // w, h vertauschen
                    list($maxW, $maxH) = array($maxH, $maxW);
                    break;
                case 8: // 90° ccw
                    $img = imagerotate($img, 90, 0);
                    // w, h vertauschen
                    list($maxW, $maxH) = array($maxH, $maxW);
                    break;
            }
        }
        
        $origW = imagesx($img);
        $origH = imagesy($img);

        // wenn kleiner, einfach kopiern
        if ($origW <= $maxW && $origH <= $maxH) {
            copy($src, $dest);
            imagedestroy($img);
            return;
        }
        
        // seitenverhältniss fix
        $ratio = min((int)$maxW / $origW, (int)$maxH / $origH);
        $newW = (int) ($origW * $ratio);
        $newH = (int) ($origH * $ratio);

//        error_log("origW: $origW, origH: $origH, maxW: $maxW, maxH: $maxH");
//        error_log("newW: $newW, newH: $newH, ratio: $ratio");
//        error_log("desination;:".$dest. "  Extensiosn:".$ext);
        // neu erzeugn
        $dstImg = imagecreatetruecolor($newW, $newH);
        // bei png/gif alpha fix
        if ($ext === 'png' || $ext === 'gif') {
            imagecolortransparent($dstImg, imagecolorallocatealpha($dstImg, 0, 0, 0, 127));
            imagealphablending($dstImg, false);
            imagesavealpha($dstImg, true);
        }

        // resizen
        imagecopyresampled($dstImg, $img, 0, 0, 0, 0, $newW, $newH, $origW, $origH);

        // sichern
        switch ($ext) {
            case 'png':
                imagepng($dstImg, $dest);
                break;
            case 'gif':
                imagegif($dstImg, $dest);
                break;
            default:
                $result = @imagejpeg($dstImg, $dest, 90);
                if (!$result) {
                  error_log("Konnte $dest nicht schreiben");
                }
                break;
        }

        imagedestroy($img);
        imagedestroy($dstImg);
    }

//    public function resizeImage($srcPath, $targetPath, $maxWidth, $maxHeight, $ext) {
//
//        // create phpThumb object (yeah, I know what I am doing, do I not?)
//        $phpThumb = new phpThumb();
//
//        $imgExt = $ext;
//
//        if ($ext == 'jpg') {
//            // exif data is only available in jpg files.
//            $exif = exif_read_data($srcPath);
//        }
//
//        if (isset($exif['COMPUTED']) && !empty($exif['COMPUTED']['Height']) && !empty($exif['COMPUTED']['Width'])) {
//            // exif computed
//            $imgWidth = $exif['COMPUTED']['Width'];
//            $imgHeight = $exif['COMPUTED']['Height'];
//        } else {
//            list($imgWidth, $imgHeight, $imgType, $imgAttr) = getimagesize($srcPath);
//        }
//
//        // switch maxWidth and maxHeight when Orientation is (-)90° ... 
//        if (isset($exif['Orientation']) && ($exif['Orientation'] == 6 || $exif['Orientation'] == 8)) {
//            $mw = $maxWidth;
//            $maxWidth = $maxHeight;
//            $maxHeight = $mw;
//        }
//
//        // calculate width and height from max values. $maxHeight or $maxWidth can be empty, but not both.
//        if (($maxWidth != '' || $maxHeight != '') && ($imgWidth > $maxWidth || $imgHeight > $maxHeight)) {
//
//            if ($imgWidth >= $imgHeight) {
//                $ratio = $maxWidth / $imgWidth;
//                $thumbnail_height = $imgHeight * $ratio;
//                $thumbnail_width = $maxWidth;
//            } else {
//                $ratio = $maxHeight / $imgHeight;
//                $thumbnail_width = $imgWidth * $ratio;
//                $thumbnail_height = $maxHeight;
//            }
//        } else {
//            // just move the file, if it is not bigger than maxWidth & maxHeight
//            if (move_uploaded_file($srcPath, $targetPath)) {
//                chmod($targetPath, 0777);
//                return true;
//            }
//        }
//
//        $phpThumb->setSourceData(file_get_contents($srcPath));
//
//        $output_filename = $targetPath;
//
//        // PLEASE NOTE:
//        // You must set any relevant config settings here. The phpThumb
//        // object mode does NOT pull any settings from phpThumb.config.php
//        if ($thumbnail_width != '') {
//            $phpThumb->setParameter('w', $thumbnail_width);
//        }
//
//        if ($thumbnail_height != '') {
//            $phpThumb->setParameter('h', $thumbnail_height);
//        }
//
//        $phpThumb->setParameter('config_output_format', $imgExt);
//
//        //keep EXIF orientation "auto-rotate"
//        $phpThumb->setParameter('ar', 'x');
//
//        //quality setting for high quality. 75 is default.
//        $phpThumb->setParameter('q', '90');
//
//        // generate & output thumbnail
//        if ($phpThumb->GenerateThumbnail()) { // this line is VERY important, do not remove it!
//            if ($phpThumb->RenderToFile($output_filename)) {
//                // do something on success
//                return true;
//            } else {
//                // do something with debug/error messages
//                echo 'Failed:<pre>' . implode("\n\n", $phpThumb->debugmessages) . '</pre>';
//                //$this->setErrorMessage('Failed:<pre>' . implode("\n\n", $phpThumb->debugmessages) . '</pre>');
//                return false;
//            }
//            $phpThumb->purgeTempFiles();
//        } else {
//            // do something with debug/error messages
//            echo 'Failed:<pre>' . $phpThumb->fatalerror . "\n\n" . implode("\n\n", $phpThumb->debugmessages) . '</pre>';
//            //$this->setErrorMessage('Failed:<pre>' . $phpThumb->fatalerror . "\n\n" . implode("\n\n", $phpThumb->debugmessages) . '</pre>');
//            return false;
//        }
//    }

    private function get_file_extension($file_name) {
        return substr(strrchr($file_name, '.'), 1);
    }

//    public static function includeLibs() {
//        /* create phpThumb object */
//        require_once 'extras/phpThumb-' . self::$version . '/phpthumb.class.php';
//    }

}
