<?php

/**
 *  Copyright © tim 02.06.2017
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 * 
 */
require_once 'core/Message.php';

/**
 *  Reads and writes System and Module Configuration files.
 *  @package Util
 */
class ConfigWriter {

    private $arrayName = '$aConfig';

    /**
     *  variable name of the config array, eg '$configArray'
     * @param type $arrayName
     */
    public function setArrayName($arrayName) {
        $this->arrayName = $arrayName;
    }

    /**
     * 
     * @param type $name
     * @return type
     */
    public function writeSpecialConfig($name) {
        $filePath = "config/useConfig.php";
        if (!is_file($filePath)) {
            $this->createConfigFile($filePath);
        }
        $content = "<?php \n\$useConfig = '";
        $content .= $name;
        $content .= "';\n";
        return file_put_contents($filePath, $content);
    }

    /**
     * 
     * @param type $path
     * @return type
     */
    public function readConfig($path) {
        $bConfig = $this->readDefaultConfig();
        require($path);
        return array_merge($bConfig, $aConfig);
    }
    
    private function readDefaultConfig(){
        $bConfig = Config::getConfig();
        // get rid of dynamically generates config values
        unset($bConfig['rootUrl']);
        unset($bConfig['reLangUrl']);
        return $bConfig;
    }

    /**
     * 
     * @param type $path
     * @return type
     */
    public function readModuleConfig($path) {
        include($path);
        return $moduleConfig;
    }

    /**
     * Writes a config file
     * @param type $aConfig
     * @param type $path
     * @return type
     */
    public function writeConfig($aConfig, $path) {
        $length = count($aConfig);
        $n = 0;

        $string = $this->getConfigStartString();
        foreach($aConfig as $key => $value)  {
            $type = gettype($value);
            $valueString = '';

            if ($type == 'boolean') {
                $valueString = $value ? 'true' : 'false';
            } elseif ($type == 'integer' || $type == 'double') {
                $valueString = $value;
            } elseif ($type == 'array') {
                //urgh
                $valueString = "array(";
                $n = 1;
                foreach ($value as $v) {
                    if (count($value) > $n) {
                        $valueString .= "'$v',";
                    } else {
                        $valueString .= "'$v'";
                    }
                    $n++;
                }
                $valueString .= ")";
            } else {
                $valueString = "'".str_replace("'", "\'", $value)."'";
            }

            $n++;
            if ($n == $length) {
                //last line
                $string .= "'$key' => $valueString\n";
            } else {
                //normal line
                $string .= "'$key' => $valueString,\n";
            }
        }

        $string .= $this->getConfigEndString();
        if (!is_file($path)) {
            $this->createConfigFile($path);
        }
        //echo $string; die;
        return file_put_contents($path, $string);
    }

    private function getConfigStartString() {
        $string = "<?php\n";
        $string .= "$this->arrayName = array(\n";
        return $string;
    }

    private function getConfigEndString() {
        return ");\n";
    }

    private function createConfigFile($path) {
        //echo $path; die;
        $f = fopen($path, "w");
        fclose($f);
        chmod($path, 0666);
    }

    /**
     *  Writes a configuration file, based on config/config.php or a module config, from POST variables.
     * @param boolean $overwrite overwrite config file. default true.
     */
    public function createConfiguration($overwrite = true, $module = "") {

        if (filter_has_var(INPUT_POST, 'module')) {
            $module = filter_input(INPUT_POST, 'module');
            //echo "module: ".$module;
        }
        $newConfig = array();
        if ($module != '') {
            $aConfig = Config::getConfig();
            $modulePath = $aConfig['applicationDirectory'] . "/" . $module . "/";
            $config = $this->readModuleConfig($modulePath . 'config/moduleConfig.php');
        } else {
            $modulePath = "";
            $config = $this->readConfig('config/config.php');
        }

        foreach($config as $key => $value) {
            $type = gettype($value);

            if ($type == 'array') {
                $newConfig[$key] = filter_input(INPUT_POST, $key, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            } elseif ($type == 'boolean') {
                if (filter_has_var(INPUT_POST, $key)) {
                    $newConfig[$key] = true;
                } else {
                    $newConfig[$key] = false;
                }
            } elseif ($type == 'integer') {
                $newConfig[$key] = (int) filter_input(INPUT_POST, $key);
            } else {
                $newConfig[$key] = filter_input(INPUT_POST, $key);
            }
        }
        if ($module == "") {
            if (filter_has_var(INPUT_POST, 'configselect') && filter_input(INPUT_POST, 'configname') == '') {
                $configName = filter_input(INPUT_POST, 'configselect');
            } else {
                $configName = filter_input(INPUT_POST, 'configname');
            }

            $newConfigFilePath = 'config/config_' . $configName . '.php';
        } else {
            include 'config/useConfig.php';
            $configName = $useConfig;
            $newConfigFilePath = $modulePath . 'config/moduleConfig_' . $configName . '.php';
            //echo $newConfigFilePath;
        }


        if (is_file($newConfigFilePath) && !$overwrite) {
            $m = Message::getInstance();
            $m->setErrorMessage($this->ts('You already have a configuration with that name ($1). No files were written.', array(1 => $newConfigFilePath)));
            return;
        }

        $failure = $this->writeConfig($newConfig, $newConfigFilePath);
        if (filter_has_var(INPUT_POST, 'saveOnlyConfiguration')) {
            // do not set saved config as active. just return.
            return $failure;
        }
        if (!isset($useConfig)) {
                $failure2 = $this->writeSpecialConfig($configName);
                return ($failure !== FALSE && $failure2 !== FALSE);
            } else {
                return $failure;
            }
    }

}
