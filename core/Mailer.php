<?php

/**
 *  Copyright © tim 31.01.2018
 *  example[at]email.org
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 */
use PHPMailer\PHPMailer\PHPMailer;
/**
 * Static functions to minify, cache and return css and js files.
 * @package Util
 */
class Mailer {
    
    public const PHPMAILER_VERSION = '6.1.6';
    
    public const METHOD_SMTP = 'smtp';
    public const METHOD_MAIL = 'mail';
    public const METHOD_SENDMAIL = 'sendmail';
    private static $method = self::METHOD_MAIL;

    /**
     * takes a set of CSS files paths and returns a string of minified files content
     * this class caches all minified files until the sources get changed.
     * 
     * @param array or string $input is an array of filepaths to css resources to be combined, or a string to be minified.
     * return string of the combined css files 
     */
    public static function setMethod($method = METHOD_MAIL){
        self::$method = $method;
    }
    public static function sendMail($to, $subject = '(No subject)', $message = '', $from_user = '', $from_email = ''){
        //Import the PHPMailer class into the global namespace
        spl_autoload_register("self::includeLibs");
        //require '../vendor/autoload.php';

        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        
        if(self::$method == self::METHOD_SMTP){
            $mail->isSMTP();
            //Enable SMTP debugging
            // SMTP::DEBUG_OFF = off (for production use)
            // SMTP::DEBUG_CLIENT = client messages
            // SMTP::DEBUG_SERVER = client and server messages
            $mail->SMTPDebug = SMTP::DEBUG_OFF;
            //Set the hostname of the mail server
            $mail->Host = Config::get('emailSmtpServer');//'mail.example.com';
            //Set the SMTP port number - likely to be 25, 465 or 587
            $mail->Port = Config::get('emailSmtpServerPort');//
            //Whether to use SMTP authentication
            $mail->SMTPAuth = Config::get('emailSmtpAuth',true);//true;
            //Username to use for SMTP authentication
            $mail->Username = Config::get('emailSmtpUser');
            //Password to use for SMTP authentication
            $mail->Password = Config::get('emailSmtpPassword');
        }
        // Set PHPMailer to use the sendmail transport
        //$mail->isSendmail();
        //Set who the message is to be sent from
        $mail->setFrom($from_email, $from_user);
        //Set an alternative reply-to address
        //$mail->addReplyTo('replyto@example.com', 'First Last');
        //Set who the message is to be sent to
        $mail->addAddress($to);
        //Set the subject line
        $mail->Subject = $subject;
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML($message);
        //Replace the plain text body with one created manually
        //$mail->AltBody = 'This is a plain-text message body';
        //Attach an image file
        //$mail->addAttachment('images/phpmailer_mini.png');
        //print_r($mail); exit;
        //send the message, check for errors
        if (!$mail->send()) {
            return 'Mailer Error: '. $mail->ErrorInfo;
        } else {
            return true;
        }
    }

    public static function includeLibs($className) {
        require_once 'extras/PHPMailer-'.self::PHPMAILER_VERSION.'/src/PHPMailer.php';
        require_once 'extras/PHPMailer-'.self::PHPMAILER_VERSION.'/src/Exception.php';
        if(self::$method === 'smtp'){
            require_once 'extras/PHPMailer-'.self::PHPMAILER_VERSION.'/src/SMTP.php';
        }
    }

}
