<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose:
 * @package View
 */

/**
 * Class to set and get Messages from the application/system to show them to
 * the user
 *
 * @package View
 */
class Message implements IMessage {

    private $errorMessages = array();
    private $systemMessages = array();
    private $debugMessages = array();
    private static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {
        // have to be empty
    }

    public function setErrorMessage($string) {
        $this->errorMessages[] = $string;
    }

    public function setSystemMessage($string) {
        $this->systemMessages[] = $string;
    }

    public function setDebugMessage($string) {
        $aConfig = Config::getConfig();
        if (isset($aConfig['debugMode']) && $aConfig['debugMode'] == true) {
            $this->debugMessages[] = $string;
        }
    }

    public function getErrorMessages() {
        return $this->errorMessages;
    }

    public function getSystemMessages() {
        return $this->systemMessages;
    }

    public function getDebugMessages() {
        return $this->debugMessages;
    }

}
