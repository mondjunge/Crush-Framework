<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: Main output and navigation rendering
 */

/**
 * Class for rendering Navigation and getting additional information
 *
 * @package View
 */
class Output {

    private $_controller;

    public function __construct($controller = null) {
        if ($controller == null) {
            //throw new Exception("Controller not set in Output!");
        } else {
            $this->_controller = $controller;
        }
    }

    public function getController() {
        return $this->_controller;
    }

    private function getTemplate() {
        return Template::getInstance();
    }

    /**
     * Gets language
     * @return String language code in ISO 3166-1 alpha-2
     */
    public final function getLanguage() {
        return $this->getRegVar('language');
    }

    /**
     * Loads language files of given path
     * @param String $modulePath
     * @param String $lang
     */
    public final function loadLang($modulePath, $lang) {
        $this->getTemplate()->loadLang($modulePath, $lang);
    }

    /**
     * Set path to the templates with trailing slash
     * @param String $path
     */
    public final function setTemplatePath($path) {
        $this->getTemplate()->set_path($path);
    }

    /**
     * Set a single template Variable
     * @param String $name
     * @param String $value
     */
    public final function setVar($name, $value) {
        $this->getTemplate()->set($name, $value);
    }

    /**
     * Sets an array of template data
     * @param String $data
     * @param String $clear set true to wipe out all previous set data, default false
     */
    public final function setData($data, $clear = false) {
        $this->getTemplate()->set_vars($data, $clear);
    }

    /**
     * Translates and sets a template variable
     * @param String $name
     * @param String $value
     * @param Bool $return whether to return the translated String or save it for fetching, default false
     * return void or translated string
     */
    public final function translateVar($name, $value, $return = false) {
        if (!$return) {
            $this->getTemplate()->translate($name, $value, $return);
        } else {
            return $this->getTemplate()->translate($name, $value, $return);
        }
    }

    /**
     * Translates and sets an array of template Data
     * @param Array[] $vars Array of Strings to be translated
     * @param Bool $clear true if you want to wipe out all previous data, default false
     */
    public final function translateData($vars, $clear = false) {
        $this->getTemplate()->translate_vars($vars, $clear);
    }

    public function getRegVar($var) {
        $reg = Registry::getInstance();
        return $reg->get($var);
    }

    public final function setRegVar($key, $value, $overwrite = false) {
        $reg = Registry::getInstance();
        return $reg->set($key, $value, $overwrite);
    }

    public function getName() {
        return $this->getRegVar('unique_name');
    }

    public function getTitle() {
        return $this->getRegVar('site_title');
    }

    /**
     * Sets the active module name, mainly for the Menu class.
     * @param String $name name of the currently active module
     */
    public final function setActiveModule($name) {
        $this->setRegVar('activeModule', $name, true);
    }

    /**
     * returns the name of the active module
     * @return String name of the currently active module
     */
    public final function getActiveModule() {
        return $this->getRegVar('activeModule');
    }

    /**
     * Sets the active module function name, mainly for the Menu class.
     * @param String $name name for the active module function
     */
    public final function setActiveFunction($name) {
        $this->setRegVar('activeFunction', $name, true);
    }

    /**
     * returns the name of the active function
     * @return String name of the active module function
     */
    public final function getActiveFunction() {
        return $this->getRegVar('activeFunction');
    }

    public final function getIncludedJs($target = "body") {
        // echo "get "; print_r($this->js); exit;
        switch ($target) {
            case 'body':
                return $this->getRegVar('jsIncludesA');
            case 'head':
                return $this->getRegVar('jsHeadIncludesA');
            case 'single':
                return $this->getRegVar('jsSingleIncludesA');
            default:
                // nothing
                break;
        }
    }

    public final function getIncludedCSS() {
        return $this->getRegVar('cssIncludes');
    }

    public function getCssMinLink() {
        /**
         * get latest modification timestamp from files about to be minified 
         */
        $cssPaths = array_merge(
                $this->getExtraCss(), $this->getExtraScssCssCachePath(), $this->getModuleCss(), $this->getModuleScssCssCachePath(), $this->getMainCss(), $this->getMainScssCssCachePath(), $this->getCkeditorCss(), $this->getCkeditorScssCssCachePath()
        );
        $cssPathsUnique = array_unique($cssPaths);
        $timeModified = 0;
        foreach ($cssPathsUnique as $f) {
            $fileModified = filemtime($f);
            if ($fileModified > $timeModified) {
                $timeModified = $fileModified;
            }
        }
        /* return css link with timestamp of latest modified file */
        return Config::get('relativeUrl') . "css/" . $this->getActiveModule() . "/" . $timeModified . "_" . $this->getActiveModule() . ".css";
    }

    public function getAdditionalHeadIncludes() {

        return $this->getRegVar('headIncludes');
    }

    public function getJavascriptIncludes($target = "body") {
        global $aConfig;
        $output = '';
        $jsPaths = array();

        if ($target == "single") {
            /**
             * module manually included head Tag scripts
             */
            $jsA = $this->getIncludedJs("single");
            if (is_array($jsA)) {
                foreach ($jsA as $path) {
                    $output .= "<script src=\"$path\"></script>";
                }
            }
            return $output;
        }
        if ($target == "head") {
            /**
             * module manually included head Tag scripts
             */
            $jsA = $this->getIncludedJs("head");
            if (is_array($jsA)) {
                foreach ($jsA as $path) {
                    $output .= "<script src=\"$path\"></script>";
                }
            }
            return $output;
        }
        if ($target == "body") {

            /**
             * get global Javascript
             */
            $jsDirPath = 'javascript';
            if (is_dir($jsDirPath)) {
                $jss = scandir($jsDirPath);

                foreach ($jss as $js) {
                    if (substr($js, 0, 1) != '.' && substr($js, -3, 3) == '.js') {
                        $jsPaths[] = $jsDirPath . '/' . $js;
                    }
                }
            }

            /**
             * get theme specific Javascript
             */
            $themePath = 'theme/' . Config::get('siteStyle');
            $jsDir = 'js';
            $jsDirPath = $themePath . '/' . $jsDir;
            if (is_dir($jsDirPath)) {
                $jss = scandir($jsDirPath);
                foreach ($jss as $js) {
                    if (substr($js, 0, 1) != '.' && substr($js, -3, 3) == '.js') {
                        $jsPaths[] = $jsDirPath . '/' . $js;
                    }
                }
            }

            /**
             * module manually included scripts
             */
            $jsA = $this->getIncludedJs();
            if (is_array($jsA)) {
                foreach ($jsA as $js) {
                    $jsPaths[] = $js;
                }
            }

            /**
             * get module specific Javascript
             */
            $modulePath = Config::get('applicationDirectory') . '/' . $this->getActiveModule();
            // already declared 
            //$jsDir = 'js';
            $jsDirPath = $modulePath . '/' . $jsDir;
            if (is_dir($jsDirPath)) {
                $jss = scandir($jsDirPath);
                foreach ($jss as $js) {
                    if (substr($js, 0, 1) != '.' && substr($js, -3, 3) == '.js') {
                        $jsPaths[] = $jsDirPath . '/' . $js;
                    }
                }
            }
            if (Config::get('debugMode')) {
                $scriptIncludes = "";
                foreach ($jsPaths as $js) {
                    $scriptIncludes .= '<script src="' . Config::get('relativeUrl') . "js/" . $js . '" ></script>';
                }
                return $scriptIncludes;
            } else {
                $jsCsv = implode(",", array_unique($jsPaths));
                $jsUrl = Config::get('relativeUrl') . "js/" . $jsCsv;
                return "<script src=\"$jsUrl\"></script>";
            }
        }
    }

    public function getJsIncludes($csvJs) {
        $jsArray = explode(",", $csvJs);
        return Minifier::minifyJs($jsArray);
    }

    /**
     * returns minified css from all css and scss files needed for the active module.
     * creates new cache when any included files have changed
     * 
     * @return string
     */
    public function getCssIncludes() {

        $cssHeader = '@charset "utf-8";' . "\n\r";
        $cssPaths = array_merge(
                $this->getExtraCss(), $this->getExtraScssCssCachePath(), $this->getModuleCss(), $this->getModuleScssCssCachePath(), $this->getMainCss(), $this->getMainScssCssCachePath(), 
               // $this->getCkeditorCss(), $this->getCkeditorScssCssCachePath(),
                $this->getSummernoteCss(), $this->getSummernoteScssCssCachePath()
        );
        // print_r($cssPaths);
        $cssPathsUnique = array_unique($cssPaths);
        // print_r($cssPathsUnique);
        //$cssPaths[] = $this->getScssCssCachePath();

        return $cssHeader . Minifier::minifyCss($cssPathsUnique);
    }

    private function getMainScssCssCachePath() {
        $scssPaths = array_merge(
                $this->getScssVariablePath(),
                $this->getMainCss(".scss")
        );
        return array(ScssCompiler::compile($scssPaths));
    }

    private function getExtraScssCssCachePath() {
        $scssPaths = array_merge(
                $this->getScssVariablePath(),
                $this->getExtraCss(".scss")
        );
        return array(ScssCompiler::compile($scssPaths));
    }

    private function getModuleScssCssCachePath() {
        $scssPaths = array_merge(
                $this->getScssVariablePath(),
                $this->getModuleCss(".scss")
        );
        return array(ScssCompiler::compile($scssPaths));
    }

    private function getScssVariablePath() {
        $sassVarsTempFile = __DIR__ . "/../cache/temp_svars.scss";
        $string = "";
        foreach ($_POST as $key => $value) {
            if (substr($key, 0, 5) == "svar_") {
                $string .= str_replace('svar_', '', $key) . ":" . trim($value) . ";";
            }
        }

        if ($string != "") {
            if (FALSE !== file_put_contents($sassVarsTempFile, $string)) {
                return array(0 => 'cache/temp_svars.scss');
            }
        }

        if (is_file('theme/' . Config::get('siteStyle') . '/css/sassVars/a.scss')) {
            $array = array(0 => 'theme/' . Config::get('siteStyle') . '/css/sassVars/a.scss');

            if (is_file('theme/' . Config::get('siteStyle') . '/css/sassVars/a_' . Config::$useConfig . '.scss')) {
                $array[1] = 'theme/' . Config::get('siteStyle') . '/css/sassVars/a_' . Config::$useConfig . '.scss';
            }
            return $array;
        } else {
            // LEGACY
            return array(0 => 'theme/' . Config::get('siteStyle') . '/css/a.scss');
        }
    }

    private function getCkeditorScssCssCachePath() {
        $scssPaths = array_merge(
                $this->getScssVariablePath(),
                $this->getCkeditorCss(".scss")
        );
        //print_r($scssPaths);
        return array(ScssCompiler::compile($scssPaths));
    }

    private function getCkeditorCss($extension = ".css") {
        if ($this->getActiveModule() == 'ckeditor' && $extension == ".css" && is_file('theme/' . Config::get('siteStyle') . '/editorarea.css')) {
            // gets additional css for ckeditor
            // LEGACY CODE
            return array(0 => 'theme/' . Config::get('siteStyle') . '/editorarea.css');
        } else if ($this->getActiveModule() == 'ckeditor' && $extension == ".scss") {
            // modern code
            $cssDir = 'theme/' . Config::get('siteStyle') . '/css/ckeditor/';
            return $this->getCssFilePaths($cssDir, $extension);
        } else {
            return array();
        }
    }
    
    private function getSummernoteScssCssCachePath() {
        $scssPaths = array_merge(
                $this->getScssVariablePath(),
                $this->getSummernoteCss(".scss")
        );
        //print_r($scssPaths);
        return array(ScssCompiler::compile($scssPaths));
    }
    
    private function getSummernoteCss($extension = ".css") {
        $cssDir = 'theme/' . Config::get('siteStyle') . '/css/summernote/';
        
        return array_merge(
            array(0 => 'extras/summernote-0.9.1/summernote-lite.min.css'),
            $this->getCssFilePaths($cssDir, $extension)
        );
    }

    private function getModuleCss($extension = ".css") {
        $cssDir = Config::get('applicationDirectory') . '/' . $this->getActiveModule() . '/css/';
        return $this->getCssFilePaths($cssDir, $extension);
    }

    private function getMainCss($extension = ".css") {
        $cssDir = 'theme/' . Config::get('siteStyle') . '/css/';
        return $this->getCssFilePaths($cssDir, $extension);
    }

    private function getExtraCss($extension = ".css") {
        $cssDir = 'theme/' . Config::get('siteStyle') . '/css/extra/';
        return $this->getCssFilePaths($cssDir, $extension);
    }

    private function getCssFilePaths($cssDir, $extension) {
        $cssPaths = array();
        if (is_dir($cssDir)) {
            $cssS = scandir($cssDir);
            foreach ($cssS as $css) {
                if (substr($css, 0, 1) != '.' && substr($css, strlen($extension) * -1, strlen($extension)) == $extension) {
                    $cssPaths[] = $cssDir . $css;
                }
            }
        }
        return $cssPaths;
    }

    public function getAdditionalDebugInfo() {
        global $aConfig;
        $debugMessage = array();
        /**
         * add configuration and user settings to debug
         */
        if (Config::get('debugMode')) {

            $debugMessage[] = '<b>Configuration</b>';
            foreach ($aConfig as $key => $value) {
                if ($key == 'dbPassword') {
                    $value = '***';
                }
                if (is_array($value)) {
                    $debugMessage[] = $key . ' => ' . print_r($value, true);
                } else {
                    $debugMessage[] = $key . ' => ' . Input::escape($value);
                }
            }

            if (isset($_COOKIE) && count($_COOKIE) > 0) {
                $debugMessage[] = '<b>Cookies</b>';
                foreach ($_COOKIE as $key => $value) {
                    if (is_array($value)) {
                        $debugMessage[] = $key . ' => ' . print_r($value, true);
                    } else {
                        $debugMessage[] = $key . ' => ' . Input::escape($value);
                    }
                }
            }

            if (isset($_SESSION) && count($_SESSION) > 0) {
                $debugMessage[] = '<b>Session</b>';
                foreach ($_SESSION as $key => $value) {

                    if (is_array($value)) {
                        $debugMessage[] = $key . ' => ' . print_r($value, true);
                    } else {
                        $debugMessage[] = $key . ' => ' . Input::escape($value);
                    }
                }
            }
            if (isset($_POST) && count($_POST) > 0) {
                $debugMessage[] = '<b>POST</b>';
                foreach ($_POST as $key => $value) {
                    if (is_array($value)) {
                        $debugMessage[] = $key . ' => ' . print_r($value, true);
                    } else {
                        $debugMessage[] = $key . ' => ' . Input::escape($value);
                    }
                }
            }
            if (isset($_GET) && count($_GET) > 0) {
                $debugMessage[] = '<b>GET</b>';
                foreach ($_GET as $key => $value) {
                    if (is_array($value)) {
                        $debugMessage[] = $key . ' => ' . print_r($value, true);
                    } else {
                        $debugMessage[] = $key . ' => ' . Input::escape($value);
                    }
                }
            }
        }
        return $debugMessage;
    }

    public function renderLanguageSelection() {
        $langSelect = '';
        if (count(Config::get('enabledLanguages')) > 1) {
            $data['langData'] = array();

            foreach (Config::get('enabledLanguages') as $lang) {

                if (strpos($_SERVER['REQUEST_URI'], "/{$this->getLanguage()}/") === false) {
                    $link = Config::get('relativeUrl') . $lang;
                } else {
                    $link = str_replace("/{$this->getLanguage()}/", "/$lang/", 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
                }
                $data['langData'][] = array('relativeUrl' => Config::get('relativeUrl'), 'lang' => $lang, 'link' => $link);
            }
            $langSelect = $this->getController()->fetchThemeFile('view/menu/lang', $data);
        }
        return $langSelect;
    }

    public function renderNavigation() {
        $menu = Menu::getInstance(); //$this->loadHelper('Menu');
        $sites = array();
        $rs = $this->getController()->select('site', 'id, module, unique_name, role_need', "active=1 AND hide=0 AND (type IN ('d','s'))", '`rank` asc');
        if (is_array($rs)) {
            foreach ($rs as $site) {
                if ($this->getController()->checkUserRights($site['role_need'])) {
                    $rs2 = $this->getController()->select('site_data', '`name`, title', "site_id='" . $site['id'] . "' AND language='" . $this->getLanguage() . "'");
                    if ($rs2 === FALSE) {
                        // fallback if selected language is not fully translated
                        $rs2 = $this->getController()->select('site_data', '`name`, title', "site_id='{$site['id']}' AND language='" . Config::get('defaultLanguage') . "'");
                    }
                    if ($rs2[0] != null) {

                        $sites[] = array_merge($site, $rs2[0]);
                    } else {
                        print_r($site);
                    }
                }
            }
        }
        if (isset($sites) && is_array($sites)) {
            foreach ($sites as $item) {
                $menu->addItem(new MenuItem($item['name'], $item['title'], $item['module'], ($item['module'] == $this->getController()->getActiveModule() . '/' . $this->getController()->getActiveFunction() || ($item['module'] == $this->getController()->getActiveModule() && $this->getController()->getActiveFunction() === 'index') || $item['unique_name'] == $this->getName()), Config::get('showAppIcons')));
            }
        }

        return $menu->renderMenuItems('default');
    }

    public function renderAdminNavigation() {
        $rs = $this->getController()->select('site', 'id, module, role_need', "active=1 AND hide=0 AND (type='a')", '`rank` asc');
        if ($rs == FALSE) {
            return "";
        }
        foreach ($rs as $site) {
            if ($this->getController()->checkUserRights($site['role_need'])) {
                $rs2 = $this->getController()->select('site_data', '`name`, title', "site_id=" . $site['id'] . " AND language='" . $this->getLanguage() . "'");
                if ($rs2 == FALSE) {
                    // fallback if selected language is not fully translated
                    $rs2 = $this->getController()->select('site_data', '`name`, title', "site_id='{$site['id']}' AND language='" . Config::get('defaultLanguage') . "'");
                }
                $sites[] = array_merge($site, $rs2[0]);
            }
        }
        if (!isset($sites) || $sites === null) {
            return "";
        }
        $menu = Menu::getInstance();
        $menuContent = '';

        foreach ($sites as $item) {
            $menu->addItem(new MenuItem($item['name'], $item['title'], $item['module'], ($item['module'] == $this->getController()->getActiveModule() || $item['module'] == $this->getController()->getActiveModule() . '/' . $this->getController()->getActiveFunction()), Config::get('showAppIcons')));
        }
        $menuContent .= $menu->renderMenuItems('admin');
        return $menuContent;
    }

}
