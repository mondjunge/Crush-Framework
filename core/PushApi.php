<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
spl_autoload_register(array(new PushApi(), 'autoload'));

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
/**
 * Description of WebPush
 *
 * @author Tim Wahrendorff
 */
class PushApi {

    public function autoload($class) {
        $filename = "..".DIRECTORY_SEPARATOR."extras". DIRECTORY_SEPARATOR ."web-push-php-6.0.5". DIRECTORY_SEPARATOR ."src". DIRECTORY_SEPARATOR .$class.".php";
        //$filename = $this->path . DIRECTORY_SEPARATOR . str_replace('_', DIRECTORY_SEPARATOR, $class) . '.php';
        include $filename;
    }

    function sendMessage($msg, $recipientArray) {

        // array of notifications
        foreach ($recipientArray as $recipient) {
            $notifications = [
                [
                    'subscription' => Subscription::create([// this is the structure for the working draft from october 2018 (https://www.w3.org/TR/2018/WD-push-api-20181026/) 
                        "endpoint" => $recipient['endpoint'],
                        "keys" => [
                            'p256dh' => $recipient['p256dh'],
                            'auth' => $recipient['auth']
                        ],
                    ]),
                    'payload' => '{"msg":"' . $msg . '"}',
                ]
            ];
        }

        $webPush = new WebPush();

        // send multiple notifications with payload
        foreach ($notifications as $notification) {
            $webPush->queueNotification(
                    $notification['subscription'],
                    $notification['payload'] // optional (defaults null)
            );
        }

        /**
         * TODO: create a log
         */
        /**
         * Check sent results
         * @var MessageSentReport $report
         */
        foreach ($webPush->flush() as $report) {
            $endpoint = $report->getRequest()->getUri()->__toString();

            if ($report->isSuccess()) {
                echo "[v] Message sent successfully for subscription {$endpoint}.";
            } else {
                echo "[x] Message failed to sent for subscription {$endpoint}: {$report->getReason()}";
            }
        }

        /**
         * send one notification and flush directly
         * @var MessageSentReport $report
         */
//        $report = $webPush->sendOneNotification(
//                $notifications[0]['subscription'],
//                $notifications[0]['payload'] // optional (defaults null)
//        );
    }

}
