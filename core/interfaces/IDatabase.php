<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * @package Interface
 */

/**
 * Interface for Database Classes
 *
 * @package Interface
 */
interface IDatabase {

    public function dbExists(): bool;

    public function tableExists($table): bool;

    public function count($table, $where = '');

    public function query($query, $array = false);

    public function select($table, $fields = '*', $where = '', $orderBy = '', $limit = '');
    
    public function selectPrepare($table, $fields = '*', $wherePart = '', $params = array());

    public function insert($table, $fieldsValueArray);

    public function delete($table, $where): bool;

    public function update(string $table, array $fieldsValueArray, string $where, string $limit = ''): bool;
    
    public function updatePrepare(string $table, array $fieldsValueArray, string $wherePart, array $params = []): bool;

    //public function escapeString($string): string;
}
