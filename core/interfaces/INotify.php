<?php

/**
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * @package Interface
 */

/**
 * Interface for Classes which utilize the Notify functions
 *
 * @package Interface
 */

interface INotify {
    /**
     * @return String - HTML String of Settings
     */
    public function getNotificationSettings($data);
    
    public function getNotificationCategories();
    
    public function setNotification(Notification $notification, $usersRsToNotify = null, $tryToSendEmail = true, $tryToPushNotify = true);
}
