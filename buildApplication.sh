#!/bin/bash

Write () { 
	
	DATE=`date +%Y-%m-%d_%H:%M:%S`
	echo "$DATE $1"
}


Write "copying application folder..."
cp -r application build/application

Write "removing custom moduleConfig files..."
find ./build/application -regex '.*moduleConfig_.*\.php' -exec rm {} \+
