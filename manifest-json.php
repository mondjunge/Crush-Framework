<?php
include('core/Config.php');
Config::createConfig();
$module = filter_input(INPUT_GET, 'module', FILTER_UNSAFE_RAW);
$title = filter_input(INPUT_GET, 'title', FILTER_UNSAFE_RAW);
echo '{
  "name": "'.$title.' - '.Config::get('pwaName').'",
  "short_name": "'.$title.' - '.Config::get('pwaShortName').'",
  "icons": [
    {
      "src": "./theme/'.Config::get('siteStyle').'/images/touch-icons/64.png",
      "sizes": "64x64",
      "type": "image/png"
    },
    {
      "src": "./theme/'.Config::get('siteStyle').'/images/touch-icons/128.png",
      "sizes": "128x128",
      "type": "image/png"
    },
    {
      "src": "./theme/'.Config::get('siteStyle').'/images/touch-icons/192.png",
      "sizes": "192x192",
      "type": "image/png",
      "density": 4.0
    },
    {
      "src": "./theme/'.Config::get('siteStyle').'/images/touch-icons/512.png",
      "sizes": "512x512",
      "type": "image/png",
      "density": 4.0
    }
  ],
  "start_url": "./'.Config::get('defaultLanguage')."/".$module.'",
  "display": "standalone"
}';
