/**
 * serviceworker for Crush-Framework
 * 
 * ATTENTION:
 * This file is parsed and served by serviceworker.php
 */

// cache name
var CACHE = 'cache-and-update';
var OFFLINE_URL = '{insertOfflineUrl}';
var SERVER_URL = '{serverUrl}';
var SERVICE_NAME = '{serviceName}';
var SERVICE_ICON = '{serviceIcon}';
var SERVICE_BADGE = '{serviceBadge}';
// ressources to precache (is filled from serviceworker.php)
var ressourcesToPreCache = [
    {insertResourcesToPreCache}
];

// For information on what ServiceWorker-Functionality is supported in which browser check out: 
// https://jakearchibald.github.io/isserviceworkerready/
// To check the current state of the WebPushApi that would make browser-independent pushes possible check out:
// http://www.w3.org/TR/push-api/

var link = "";
self.addEventListener('push', function(event) {
    console.log('Received a push message', event);
    console.log('payload: ', event.data.text());
    var body = event.data.text();


    //get message from the response and convert UTF-8 to something js can manage
    var tag = 'notification-tag';

    //console.log("should now spawn notification with data " , response);

    return self.registration.showNotification(SERVICE_NAME, {
            body: body,
            icon: SERVER_URL + SERVICE_ICON,
            badge: SERVER_URL + SERVICE_BADGE, 
            tag: tag
    }); 
        
        
//	const pushInfoPromise = event.currentTarget.registration.pushManager.getSubscription().then(function(subscription){
//	
//		console.log('Endpoint:',subscription.endpoint);
//		return subscription;
//		
//	}).then(function (subscription){
//		// using fetch to get the message. As this serviceworker basically is isolated javascript from
//		// anything that you have included on your website, the use of jquery is not possible here.
//		var endpointEncoded = encodeURIComponent(subscription.endpoint);
//		console.log('endpointEncoded:', SERVER_URL+"getPushMessageByEndpoint?ep=" + endpointEncoded);
//		
//		return fetch(SERVER_URL+"getPushMessageByEndpoint?ep="+endpointEncoded
//				).then(function(response){
//					
//			return response.json();
//					
//		}).then(function(response){
//			
//			//get message from the response and convert UTF-8 to something js can manage
//			console.log('Server message:'+ response);
//			var message = decodeURIComponent(response).replace(/\+/g, " ");
//			console.log('Server message:', message);
//			if(message===''){
//				message = "You received a push message, but the message was empty.";
//			}
//			console.log(message);		 
//			var body = message;
//			var tag = 'notification-tag';
//			
//			console.log("should now spawn notification with data " , response);
//			
//			return self.registration.showNotification(SERVICE_NAME, {
//				body: body,
//				icon: SERVER_URL + SERVICE_ICON,
//				badge: SERVER_URL + SERVICE_BADGE, 
//				tag: tag
//			}); 
//		});
//		
//	});
//	
//	// create the Promise Chain fuirst and wait for it..
//	// This way you can easily add other Promise Chains in the future
//	const promiseChain = Promise.all([
//		pushInfoPromise
//		//, more Promise chain consts possible.
//	]);
//	
//	event.waitUntil(promiseChain);
		
});


//handle clicks on the notification itself once it is shown
self.addEventListener('notificationclick', function(event) {
	console.log('On notification click: ', event.notification.tag);
	// Android doesn’t close the notification when you click on it
	// See: http://crbug.com/463146
	event.notification.close();
	//console.log(event.origin);
	
	// This looks to see if the current page is already open and
	// focuses if it is. 
	event.waitUntil(clients.matchAll({
		type: "window"
	}).then(function(clientList) {
		for (var i = 0; i < clientList.length; i++) {
			var client = clientList[i];
			if (client.url == SERVER_URL + decodeURIComponent(link).replace(/\+/g, " ") && 'focus' in client){
				client.navigate(SERVER_URL+decodeURIComponent(link).replace(/\+/g, " "));
				return client.focus();
			}
		}
		if (clients.openWindow){
			return clients.openWindow(SERVER_URL+decodeURIComponent(link).replace(/\+/g, " "));
		}
	}));
});




//clear ALL caches on updates
const clearCache = caches.keys().then(function (cacheNames) {
    return Promise.all(
        cacheNames.filter(function (cacheName) {
            // Return true if you want to remove this cache,
            // but remember that caches are shared across
            // the whole origin
            return true;
        }).map(function (cacheName) {
        return caches.delete(cacheName);
    }));
});

// precaches assets on serviceworker activate
function precache() {
    return caches.open(CACHE).then(function (cache) {
        return Promise.all(
            ressourcesToPreCache.map(function (url) {
                return cache.add(url).catch(function (reason) {
                    return console.log(url + "failed: " + String(reason));
                });
            })
            );
    });
}

self.addEventListener('install', function (event) {
    console.log('The service worker is being installed.');
    //shipWaiting on install
    self.skipWaiting();


    // delete outdated cache and precache ressources
    const promiseChain = Promise.all([
        clearCache,
        precache()
            //, more Promise chain consts possible.
    ]);

    event.waitUntil(promiseChain);
});

self.addEventListener('activate', function (event) {
    console.log('The service worker is being activated.');
    // immediately claim the application
    self.clients.claim();
});


self.addEventListener('fetch', function (event) {
    if (event.request.url.includes('extras/') || event.request.url.toLowerCase().includes('upload/')) {
        // do nothing on third party libs
    } else {
        // cache GET requests with image or other static ressources
        if (event.request.method === "GET") {
            if ((event.request.url.toLowerCase().endsWith('.svg')
                    //|| event.request.url.toLowerCase().endsWith('.js')
                    //|| event.request.url.toLowerCase().endsWith('.css')
                    || event.request.url.toLowerCase().endsWith('.png')
                    || event.request.url.toLowerCase().endsWith('.jpg')
                    || event.request.url.toLowerCase().endsWith('.jpeg')
                    || event.request.url.toLowerCase().endsWith('.gif'))
                && !event.request.url.toLowerCase().includes('/de/gallery/index')
                ) {

                // respond from cache if possible, otherwise fetch request, clone response to cache and respond with regular response
                event.respondWith(
                    caches.open(CACHE).then(function (cache) {
                    return cache.match(event.request.url).then(function (response) {
                        return response || fetch(event.request).then(function (response) {
                            console.log('put ' + event.request.url + ' in cache');
                            cache.put(event.request.url, response.clone());
                            return response;
                        });
                    });
                })
                    );
            } 
//            else if (event.request.mode === 'navigate') {
//                // react on offline or server not reachable states while navigating
//                return event.respondWith(
//                    fetch(event.request).catch(() => caches.match(OFFLINE_URL))
//                );
//                
//            }
        }
    }
});