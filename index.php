<?php

/**
 *  Main Script
 * 
 *  Copyright © 2010 Tim Wahrendorff
 *  tim[at]timwahrendorff.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see:
 *  http://www.gnu.org/licenses/gpl-3.0-standalone.html.
 *
 * 	$Id: $
 * 	Files Purpose: main script
 */
/**
 * check for write persmissions in Cache.
 */
if (!is_writeable(__DIR__ . "/cache")) {
    echo "Fatal Error: No write permissions in cache! <br/><strong>Webdaemon needs write access in " . __DIR__ . "/cache</strong><br/><br/>";
    if (!is_writeable(__DIR__ . "/cache/html")) {
         echo "Fatal Error: No write permissions in cache/html! <br/><strong>Webdaemon needs write access in " . __DIR__ . "/cache/html</strong><br/><br/>";
    }
    if (!is_writeable(__DIR__ . "/cache/thumbs")) {
        echo "Fatal Error: No write permissions in cache/thumbs! <br/><strong>Webdaemon needs write access in " . __DIR__ . "/cache/thumbs</strong><br/><br/>";
    }
    if (!is_writeable(__DIR__ . "/cache/updates")) {
        echo "Fatal Error: No write permissions in cache/updates! <br/><strong>Webdaemon needs write access in " . __DIR__ . "/cache/updates</strong><br/><br/>";
    }
    echo "Please set according permissions and do a hard refresh (Shift+F5) to generate css and js files.";
    die;
}
/**
 * include Base and create global config array
 */
/**
 * Core class needed for global configuration
 */
require_once 'core/Base.php';
//Base::includeHelpersAndInterfaces();
spl_autoload_register("Base::autoload");
/**
 * The use of $aConfig = Config::getConfig() is depricated and only available for Legacy reasons.
 * Use Config::get('key'); or $config = Config::getConfig(); instead.
 */
$aConfig = Config::createConfig();

/**
 * set error reporting accordingly: 
 * only errors in production, everything in debug
 */
if(Config::get('debugMode')==true){
    error_reporting(E_ALL);
} else {
    error_reporting(E_ERROR);
}
/**
 * run cron jobs
 */
if(filter_has_var(INPUT_GET, 'cron')){
    Base::doMaintainance();
    exit;
}

/**
 * handle request for minified css file
 */
Base::minifyCss();

/**
 * handle request for minified css file
 */
Base::minifyJs();

/**
 * checks if current Theme has an own Controller and includes it, if existant
 */
//Base::includeTheme($aConfig);

/**
 * Initialize Session
 */
Session::init();

/**
 * load Main Controller
 */
$oController = Base::initController();

/**
 * check and process user input
 */
/**
 * language selection 
 */
$language = Base::handleLanguage($aConfig);

/**
 * module and function selection 
 */
$module = Base::handleModuleSelection($oController, $aConfig, $language);

if (filter_has_var(INPUT_POST, 'AJAX') || filter_has_var(INPUT_GET, 'AJAX')) {
    $module->standalone = true;
}

/**
 * run selected function, buffer direct output of module and include it in the content,
 * if there is any direct output. Else get the content output of the module.
 * There is no other purpose for getting them echos than quick and dirty debugging.
 */
$data['content'] = Base::getModuleContent($module);

/**
 * logout at maintainance mode, 
 */
if (isset($aConfig['maintainanceMode']) && $aConfig['maintainanceMode']) {
    // maintainance mode
    if ((!$oController->checkUserRights(1) && $oController->getActiveModule() != 'login' && $oController->getActiveModule() != 'install') || (!$oController->checkUserRights(1) && $oController->isLoggedIn())) {

        if ($module->standalone) {
            echo "";
            exit;
        }
        // wenn kein Superadmin, logout
        //$module->logout();
        Link::jumpTo("login?logout");
    }
}


/**
 * get System-, Error-, Debug-Messages
 */
$data = Base::getMessages($data, $module);

/**
 * init main theme
 */
$output = new Theme($oController);
/**
 * experimental AJAX fetching sites
 *
 */
if ($module->standalone === true) {
    $data['content'] = $output->getController()->fetchThemeFile('view/messages', $data) . $data['content'];
}

/**
 * TODO: 
 *  * System Settings site shall include a setting to enable a debuglog
 */
//if ($aConfig['debugMode'] && !filter_has_var(INPUT_GET,'AJAX')) {
//    if (is_file("debug.log") && filesize("debug.log") > 2048) {
//        file_put_contents("debug.log", $_SERVER['HTTP_USER_AGENT'] . "\n\r");
//        foreach($data['debugMessages'] as $dm){
//            file_put_contents("debug.log", $dm . "\n\r");
//        }
//        
//        
//    } else {
//        file_put_contents("debug.log", $_SERVER['HTTP_USER_AGENT'] . "\n\r", FILE_APPEND);
//        foreach($data['debugMessages'] as $dm){
//            file_put_contents("debug.log", $dm . "\n\r", FILE_APPEND);
//        }
//        
//    }
//}
/**
 * set Headers
 */
Base::setSecurityHeaders();
// global Cache is only interesting for complete static sites, make configurable.
//Base::setCacheHeader(true);

/**
 * the standalone var signals, that a function in a module have an plain output and does not use the outer template.
 */
if ($module->standalone) {

    echo $data['content'];
    Session::writeClose();
    exit;
}
/**
 * I guess this is needed for loading widgets?
 */
$contentOnly = false;
if (filter_has_var(INPUT_POST, 'contentOnly') || filter_has_var(INPUT_GET, 'contentOnly')) {
    $contentOnly = true;
}
/**
 * process outer template
 */
$output->loadLang('', $language);
$output->render($data, $contentOnly);

/**
 * close the Session for writing
 */
Session::writeClose();
